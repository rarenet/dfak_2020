---
language: fa
layout: organisation
name: Co-Creation Hub
website: https://cchubnigeria.com/
logo: CCHubLogo.png
languages: English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: دوشنبه تا جمعه، به وقت نیجریه (GMT +1)
response_time: "روزهای هفته: ۱ ساعت؛ آخر هفته: ۳-۵ ساعت"
contact_methods: email, pgp, mail, phone
email: digitalsecurity@cchubnigeria.com
pgp_key_fingerprint: 8BCB 3ED5 B64F EBAE 5725 3D63 5E95 2295 42B5 7373
phone: +234 1 295 0555
mail: 294, Herbert Macaulay Way, Sabo, Yaba, Lagos, Nigeria (101212)
initial_intake: yes
---

CcHUB یک مرکز نوآوری است که به تسریع استفاده از سرمایه اجتماعی و فناوری برای رونق اقتصادی اختصاص دارد.
