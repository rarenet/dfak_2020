---
language: fa
layout: organisation
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: ساعات اداری، UTC+2
response_time: چهار ساعت
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL CERT برای بخش خصوصی، کمون‌ها و نهادهای غیر دولتی در لوکزامبورگ است.

CIRCL یک نقطه تماس قابل اعتماد و قابل اعتماد برای هر کاربر، شرکت و سازمان مستقر در لوکزامبورگ برای رسیدگی به حملات و حوادث فراهم می‌کند. تیم متخصص آن مانند یک آتش نشانی عمل می کند و توانایی واکنش سریع و کارآمد را در زمان مشکوک شدن به تهدید، شناسایی یا وقوع حوادث دارد.

هدف CIRCL جمع آوری، بررسی، گزارش و پاسخ به تهدیدات سایبری به شیوه ای سیستماتیک و سریع است.
