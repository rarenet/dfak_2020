---
language: fa
layout: organisation
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: دوشنبه تا جمعه، ساعات اداری، به ساعت شرق آمریکا
response_time: یک روز
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

بنیاد آزادی مطبوعات (FPF) یک سازمان غیرانتفاعی 501(c)3 است که از روزنامه نگاری با منافع عمومی در قرن بیست و یکم محافظت، دفاع و قدرت می‌بخشد. FPF به همه از سازمان‌های رسانه‌ای بزرگ گرفته تا روزنامه‌نگاران مستقل انواع ابزارها و تکنیک‌های امنیتی و حریم خصوصی را برای محافظت بهتر از خود، منابع و سازمان‌هایشان آموزش می‌دهد.
