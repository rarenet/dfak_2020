---
language: fa
layout: organisation
name: Digital Security Lab Ukraine
website: https://dslua.org/
logo: DSL-Ukraine.png
languages: English, Українська, Русский, Français
services: in_person_training, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, legal, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: دوشنبه تا پنجشنبه ۹ صبح تا ۵ بعد از ظهر EET/EEST
response_time: دو روز
contact_methods: email, pgp, signal, whatsapp
email: koushnir@gmail.com, gudvadym@gmail.com
pgp: koushnir@gmail.com - 000F08EA02CE0C81, gudvadym@gmail.com - B20B8DA28B2FA0C3
signal: +380987767783; +380990673853
whatsapp: +380987767783; +380990673853
---

آزمایشگاه امنیت دیجیتال اوکراین یک سازمان غیردولتی مستقر در کیف است که در سال ۲۰۱۷ با ماموریت حمایت از اجرای حقوق بشر در اینترنت با ایجاد ظرفیت سازمان‌های غیردولتی و رسانه‌های مستقل برای رسیدگی به نگرانی‌های امنیتی دیجیتال، سیاست‌های دولت و شرکت‌ها در زمینه حقوق دیجیتال تاسیس شد.
