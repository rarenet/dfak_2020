---
language: fa
layout: organisation
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: ۲۴ ساعته/جهانی
response_time: ۱۲ ساعت
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

اینترنیوز تکمیل کننده کار اصلی اینترنیوز، همچنین با افراد، سازمان‌ها و جوامع در سراسر جهان برای افزایش آگاهی امنیت دیجیتال، حفاظت از دسترسی به اینترنت باز و بدون سانسور و بهبود شیوه‌های ایمنی دیجیتال همکاری می‌کند. اینترنیوز روزنامه نگاران و مدافعان حقوق بشر را در بیش از ۸۰ کشور آموزش داده است و دارای شبکه‌های قوی از مربیان و حسابرسان امنیت دیجیتال محلی و منطقه‌ای است که با چارچوب حسابرسی امنیتی و الگوی ارزیابی برای گروه‌های مدافع (SAFETAG) آشنا هستند ([https://safetag. org](https://safetag.org))، که اینترنیوز توسعه آن را رهبری کرده است. اینترنیوز در حال ایجاد مشارکت‌های قوی و پاسخگو با جامعه مدنی و شرکت‌های اطلاعاتی و تحلیل تهدیدات بخش خصوصی است و می‌تواند به طور مستقیم از شرکای خود در حفظ حضور آنلاین، ایمن و بدون سانسور حمایت کند. اینترنیوز مداخلات فنی و غیرفنی را از ارزیابی‌های امنیتی پایه با استفاده از چارچوب SAFETAG گرفته تا ارزیابی سیاست‌های سازمانی تا استراتژی‌های کاهش تجدیدنظر شده بر اساس تحقیقات تهدید ارائه می‌دهد. آنها مستقیماً از تجزیه و تحلیل فیشینگ و بدافزار برای گروه های حقوق بشری و رسانه ای که حملات دیجیتال هدفمند را تجربه می کنند، پشتیبانی می کنند.
