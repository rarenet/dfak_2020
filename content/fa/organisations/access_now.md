---
language: fa
layout: organisation
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano, Українська, тоҷикӣ
services: grants_funding, in_person_training, org_security, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, harassment, forensic, advocacy
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: جهانی، ۲۴ ساعته
response_time: دو ساعت
contact_methods: email, pgp
email: help@accessnow.org
pgp_key: https://keys.accessnow.org/help.asc
pgp_key_fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC
initial_intake: yes
---

خط راهنمای امنیت دیجیتال Access Now با افراد و سازمان‌ها در سراسر جهان کار می‌کند تا آنها را به صورت آنلاین ایمن نگه دارد. اگر در معرض خطر هستید، می‌توانیم به شما کمک کنیم تا شیوه‌های امنیت دیجیتال خود را بهبود ببخشید تا از آسیب دوری کنید. اگر قبلاً مورد حمله قرار گرفته‌اید، ما کمک‌های اضطراری با واکنش سریع ارائه می‌کنیم.
