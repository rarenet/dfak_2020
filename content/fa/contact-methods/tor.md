---
layout: contact-method
title: تور
author: mfc
language: fa
summary: روش‌های تماس
date: 2018-09
permalink: /fa/contact-methods/tor.md
parent: /fa/
published: true
---

مرورگر تور یک مرورگر وب متمرکز بر حفظ حریم خصوصی است که به شما امکان می‌دهد با به اشتراک گذاشتن موقعیت مکانی خود (از طریق آدرس IP خود) هنگام دسترسی به وب سایت، به طور ناشناس با وب سایت ها تعامل داشته باشید.

منابع: [تور در یک نگاه](https://www.torproject.org/about/overview.html.en).
