---
layout: contact-method
title: اسکایپ
author: mfc
language: fa
summary: روش‌های تماس
date: 2018-09
permalink: /fa/contact-methods/skype.md
parent: /fa/
published: true
---

محتوای پیام شما و همچنین تماس شما با سازمان ممکن است توسط دولت‌ها یا سازمان‌های مجری قانون قابل دسترسی باشد.
