---
layout: contact-method
title: سیگنال
author: mfc
language: fa
summary: روش‌های تماس
date: 2018-09
permalink: /fa/contact-methods/signal.md
parent: /fa/
published: true
---

استفاده از سیگنال تضمین می‌کند که محتوای پیام شما فقط برای سازمان گیرنده رمزگذاری شده است و فقط شما و گیرنده شما از برقراری ارتباط مطلع خواهید شد. توجه داشته باشید که سیگنال از شماره تلفن شما به عنوان نام کاربری شما استفاده می‌کند، بنابراین شما شماره تلفن خود را با سازمانی که با آن تماس می‌گیرید به اشتراک می‌گذارید.

منابع: [نحوه: استفاده از سیگنال برای اندروید](https://ssd.eff.org/en/module/how-use-signal-android)، [نحوه: استفاده از سیگنال برای iOS](https://ssd .eff.org/en/module/how-use-signal-ios)، [چگونه از سیگنال بدون ارائه شماره تلفن خود استفاده کنیم](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)
