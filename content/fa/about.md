---
layout: page
title: "درباره"
language: fa
summary: "درباره بسته کمک‌های اولیه دیجیتال."
date: 2023-05
permalink: about
parent: Home
---

بسته کمک‌های اولیه دیجیتال تلاش مشترک [RaReNet (شبکه واکنش سریع)](https://www.rarenet.org/) و [CiviCERT](https://www.civicert.org/) است.

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Tشبکه واکنش سریع یک شبکه بین‌المللی از پاسخ‌دهندگان سریع و قهرمانان امنیت دیجیتال است که شامل Access Now، امنستی تک، مرکز تاب آوری دیجیتال، CIRCL، فمبلوک، بنیاد مرز الکترونیکی خانه آزادی، فرانت‌لاین دیفندر، گلوبال ویسز، گرین‌ هاست، هیوس و مشارکت مدافعان دیجیتال است. ، اینترنیوز، La Labomedia، صندوق فناوری باز، ویرچوال رود و همچنین کارشناسان امنیتی فردی که در زمینه امنیت دیجیتال و واکنش سریع فعالیت می کنند.

برخی از این سازمان‌ها و افراد بخشی از CiviCERT، یک شبکه بین‌المللی از گروه‌های و سازمان‌های کمک کننده برای امنیت دیجیتال و ارائه‌دهندگان زیرساخت هستند، که عمدتاً بر حمایت از گروه‌ها و افراد در تلاش برای عدالت اجتماعی و دفاع از حقوق بشر و دیجیتال متمرکز هستند. CiviCERT یک چارچوب حرفه‌ای برای تلاش‌های توزیع شده CERT (تیم واکنش اضطراری رایانه‌ای) جامعه واکنش سریع است. CiviCERT توسط Trusted Introducer، شبکه اروپایی تیم‌های واکنش اضطراری رایانه‌ای قابل اعتماد، معتبر است.

کیت کمک های اولیه دیجیتال نیز یک [پروژه منبع باز است که مشارکت های خارجی را می پذیرد](https://gitlab.com/rarenet/dfak_2020).

با تشکر از [بنیاد Metamorphosis](https://metamorphosis.org.mk) برای [بومی سازی آلبانیایی](https://digitalfirstaid.org/sq/) و [EngageMedia](https://engagemedia.org/) برای [برمه](https://digitalfirstaid.org/my/)، [اندونزی](https://digitalfirstaid.org/id/)، و [تایلند](https://digitalfirstaid.org/th/) بومی سازی کیت کمک های اولیه دیجیتال

اگر می‌خواهید از کیت کمک‌های اولیه دیجیتال در شرایطی استفاده کنید که اتصال محدود است، یا یافتن اتصال دشوار است، می‌توانید [نسخه آفلاین](https://digitalfirstaid.org/dfak-offline.zip) را دانلود کنید.

برای هرگونه نظر، پیشنهاد یا سوال در مورد کیت کمک های اولیه دیجیتال، می توانید به dfak @ digitaldefenders مکاتبه کنید. org

GPG - Fingerprint: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
