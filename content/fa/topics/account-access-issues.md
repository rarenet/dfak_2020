---
layout: topic
title: "من نمی‌توانم به حساب کاربری خود دسترسی پیدا کنم"
author: RaReNet
language: fa
summary: "آیا در دسترسی به ایمیل، شبکه های اجتماعی یا حساب وب مشکل دارید؟ آیا حساب کاربری شما فعالیتی را نشان می دهد که شما آن را تشخیص نمی‌دهید؟ کارهای زیادی وجود دارد که می توانید برای بهبود این مشکل انجام دهید."
date: 2023-05
permalink: topics/account-access-issues
parent: /fa/
order: 2
---

# من دسترسی به اکانتم را گم کرده‌ام

رسانه‌های اجتماعی و حساب‌های ارتباطی به طور گسترده توسط اعضای جامعه مدنی برای برقراری ارتباط، اشتراک‌گذاری دانش و حمایت از اهدافشان استفاده می‌شوند. در نتیجه، این حساب‌ها به شدت مورد هدف عوامل مخرب قرار می‌گیرند، که اغلب سعی می‌کنند این حساب‌ها را به خطر بیاندازند و به اعضای جامعه مدنی و مخاطبین آنها آسیب وارد کنند.

این راهنما برای این است که در صورتی که دسترسی به یکی از حساب‌های خود را که در معرض خطر است از دست دادید، به شما کمک کند.

در اینجا یک پرسشنامه برای شناسایی ماهیت مشکل شما و یافتن راه حل های ممکن وجود دارد.

## Workflow

### Password-Typo

#### آیا اشتباه تایپی در رمز عبور شما وجود دارد؟

> گاهی اوقات ممکن است نتوانید به حساب خود وارد شوید زیرا رمز عبور را اشتباه تایپ می کنید، یا به این دلیل که زبان صفحه کلید شما آن چیزی نیست که معمولاً استفاده می‌کنید یا CapsLock را روشن کرده‌اید.

سعی کنید نام کاربری و رمز عبور خود را در یک ویرایشگر متن بنویسید و آنها را از ویرایشگر کپی کنید و در فرم ورود قرار دهید.
آیا راه حل بالا به شما کمک کرد وارد حساب کاربری خود شوید؟

##### Options

- [بله](#resolved_end)
- [خیر](#account-disabled)

### account-disabled

#### حساب غیرفعال شد

> گاهی اوقات ممکن است به دلیل نقض شرایط خدمات یا قوانین پلت فرم توسط پلتفرم حساب کاربری شما مسدود یا غیرفعال شده باشد و شما نتوانید وارد حساب کاربری خود شوید. این می تواند زمانی اتفاق بیفتد که حساب شما به طور گسترده گزارش شود، یا زمانی که مکانیسم های گزارش دهی و پشتیبانی پلت فرم به قصد سانسور محتوای آنلاین مورد سوء استفاده قرار می گیرد.
>
> اگر پیامی مبنی بر قفل شدن، محدود شدن، غیرفعال شدن یا تعلیق شدن حساب خود مشاهده می‌کنید و فکر می‌کنید که این یک اشتباه است، مکانیسم تجدیدنظر ارایه شده همراه با پیام را دنبال کنید. اطلاعات مربوط به نحوه ارسال درخواست تجدید نظر را می‌توانید در لینک های زیر بیابید:
>
> - [فیسبوک](https://www.facebook.com/help/185747581553788)
> - [اینستاگرام](https://help.instagram.com/366993040048856)
> - [توییتر](https://help.twitter.com/en/forms/account-access/appeals/redirect)
> - [یوتویوب](https://support.google.com/youtube/answer/2802168)

آیا راه حل بالا به شما کمک کرد وارد حساب کاربری خود شوید؟

##### Options

- [بله](#resolved_end)
- [خیر](#what-type-of-account-or-service)

### what-type-of-account-or-service

#### دسترسی به کدام حساب یا سرویس را از دست داده اید؟

##### Options

- [فیسبوک](#Facebook)
- [صفحه‌ی فیسبوک](#Facebook-Page)
- [توییتر](#Twitter)
- [گوگل/جیمیل](#Google)
- [یاهو](#Yahoo)
- [هاتمیل/اوت‌لوک/لایو](#Hotmail)
- [پورتون میل](#ProtonMail)
- [اینستاگرام](#Instagram)
- [تیک تاک](#Tiktok)
  <!--- - [AddOtherServiceLink](#service-Name) -->

### Facebook-Page

#### صفحه‌ی فیسبوک

آیا صفحه مدیر دیگری دارد؟

##### Options

- [بله](#Other-admins-exist)
- [خیر](#Facebook-Page-recovery-form)

### Other-admins-exist

#### ادمین های دیگه هم هستن

آیا مدیر (مدیران) دیگر هم همین مشکل را دارند؟

##### Options

- [بله](#Facebook-Page-recovery-form)
- [خیر](#Other-admin-can-help)

### Other-admin-can-help

#### سایر ادمین ها می توانند کمک کنند

> لطفاً از سایر مدیران بخواهید که شما را دوباره به مدیران صفحه اضافه کنند.

آیا این مشکل را حل کرد؟

##### Options

- [بله](#Fb-Page_end)
- [خیر](#account_end)

### Facebook-Page-recovery-form

#### بقیه ادمین ها هم همین مشکل رو دارن

> لطفاً وارد فیس بوک شوید و از [Facebook's form to recover the page](https://www.facebook.com/help/contact/164405897002583). Save translation
> اگر نمی توانید وارد حساب فیس بوک خود شوید، لطفا ازاین طریق بروید [Facebook account recovery workflow](#Facebook)
>
> لطفا توجه داشته باشید که دریافت پاسخ به درخواست‌های شما ممکن است کمی طول بکشد. این صفحه را در نشانک های خود ذخیره کنید و چند روز دیگر دوباره بازگردید.

آیا روش ریکاوری موثر بود؟

##### Options

- [بله](#resolved_end)
- [خیر](#account_end)

<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

#### گوگل

آیا شما به ریکاوری ایمیل/موبایل خود که اکانت شما متصل است دسترسی دارید؟

##### Options

- [بله](#I-have-access-to-recovery-email-google)
- [خیر](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

#### من به ایمیل بازیابی  گوگل دسترسی دارم

> ایمیل ریکاوری خود را چک کنید و ببینید که آیا "Critical security alert for your linked Google Account" از طرف گوگل در ایمیل و یا پیامک دریافت کرده‌اید.
>
> هنگام بررسی ایمیل‌ها، همیشه مراقب فیشینگ باشید. اگر از صحت یک پیام مطمین نیستید، لطفاً آن را بررسی کنید [Suspicious Messages Workflow](/fa/topics/suspicious-messages/).

آیا "Critical security alert for your linked Google Account" از طرف گوگل در ایمیل و یا پیامک دریافت کرده‌اید.

##### Options

- [بله](#Email-received-google)
- [خیر](#Recovery-Form-google)

### Email-received-google

#### من یک ایمیل بازیابی دریافت کرده ام

هنگامی که صحت پیام را تایید کردید، اطلاعات ارائه شده در ایمیل را بررسی کنید. ببینید که آیا لینک "recover your account" وجود دارد. آیا این لینک وجود دارد؟

##### Options

- [بله](#Recovery-Link-Found-google)
- [خیر](#Recovery-Form-google)

### Recovery-Link-Found-google

#### من لینک ریکاوری دارم

> لطفا از لینک"recover your account" برای ریکاور کردن استفاده کنید. هنگامی که لینک را دنبال می کنید، دوباره چک کنید که URL مورد بازدید شما در واقع آدرس "google.com" است.

آیا توانستید که حساب کاربری خود را ریکاور کنید؟

##### Options

- [بله](#resolved_end)
- [خیر](#Recovery-Form-google)

### Recovery-Form-google

#### فرم بازیابی گوگل

> لطفا دستورالعمل زیر را دنبال کنید ["چگونه حساب گوگل و یا جی‌میل خود را ریکاور کنید"](https://support.google.com/accounts/answer/7682439?hl=en).
>
> لطفا توجه داشته باشید که دریافت پاسخ به درخواست‌های شما ممکن است کمی طول بکشد. این صفحه را در نشانک های خود ذخیره کنید و چند روز دیگر دوباره بازگردید.

آیا روش ریکاوری موثر بوده است؟

##### Options

- [بله](#resolved_end)
- [خیر](#account_end)

<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

#### یاهو

آیا شما به ریکاوری ایمیل/موبایل خود که اکانت شما متصل است دسترسی دارید؟

##### Options

- [بله](#I-have-access-to-recovery-email-yahoo)
- [خیر](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

#### من به ایمیل بازیابی دسترسی دارم

> ایمیل ریکاوری را چک کنید تا ببینید آیا ایمیل "Password change for your Yahoo account" را از یاهو دریافت کرده‌اید یا خیر.
>
> هنگام بررسی ایمیل‌ها، همیشه مراقب فیشینگ باشید. اگر از صحت یک پیام مطمین نیستید، لطفاً آن را بررسی کنید [Suspicious Messages Workflow](/fa/topics/suspicious-messages/).

آیا "Password change for your Yahoo account" را از یاهو دریافت کرده‌اید؟

##### Options

- [بله](#Email-received-yahoo)
- [خیر](#Recovery-Form-Yahoo)

### Email-received-yahoo

#### من یک ایمیل بازیابی دریافت کرده ام

هنگامی که صحت پیام را تایید کردید، اطلاعات ارائه شده در ایمیل را بررسی کنید. ببینید که آیا لینک "recover your account" وجود دارد. آیا این لینک وجود دارد؟

##### Options

- [بله](#Recovery-Link-Found-Yahoo)
- [خیر](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

#### من لینک ریکاوری دارم

> لطفا ازلینک "Recover your account here" برای ریکاوری حساب خود استفاده کنید.

آیا شما موفق شدید که حساب خود را ریکاور کنید؟

##### Options

- [بله](#resolved_end)
- [خیر](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

#### فرم بازیابی یاهو

> لطفاً دستورالعمل‌ زیر را دنبال کنید ["رفع مشکلات ورود به حساب یاهو خود"]. (https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html?impressions=true) to recover your account.
>
> لطفا توجه داشته باشید که دریافت پاسخ به درخواست‌های شما ممکن است کمی طول بکشد. این صفحه را در نشانک های خود ذخیره کنید و چند روز دیگر دوباره بازگردید.

آیا روش ریکاوری موثر بود؟

##### Options

- [بله](#resolved_end)
- [خیر](#account_end)

<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

#### توییتر

آیا شما به ریکاوری ایمیل/موبایل خود که اکانت شما متصل است دسترسی دارید؟

##### Options

- [بله](#I-have-access-to-recovery-email-Twitter)
- [خیر](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

#### من به ایمیل بازیابی دسترسی دارم

> ایمیل ریکاوری را چک کنید تا ببینید آیا ایمیل"Your Twitter password has been changed" را از توییتر دریافت کرده‌اید یا خیر.
>
> هنگام بررسی ایمیل‌ها، همیشه مراقب فیشینگ باشید. اگر از صحت یک پیام مطمین نیستید، لطفاً آن را بررسی کنید [Suspicious Messages Workflow](/fa/topics/suspicious-messages/).

آیا ایمیل"Your Twitter password has been changed" را از توییتر دریافت کردید؟

##### Options

- [بله](#Email-received-Twitter)
- [خیر](#Recovery-Form-Twitter)

### Email-received-Twitter

#### من یک ایمیل بازیابی دریافت کرده ام

هنگامی که صحت پیام را تایید کردید، اطلاعات ارائه شده در ایمیل را بررسی کنید. ببینید که آیا لینک "recover your account" وجود دارد. آیا این لینک وجود دارد؟

##### Options

- [بله](#Recovery-Link-Found-Twitter)
- [خیر](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

#### من لینک ریکاوری دارم

> لطفا ازلینک "Recover your account here" برای ریکاوری حساب خود استفاده کنید.

آیا شما موفق شدید که حساب خود را ریکاور کنید؟

##### Options

- [بله](#resolved_end)
- [خیر](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

#### فرم بازیابی توییتر

> اگر فکر می‌کنید حساب توییتر شما به خطر افتاده است، مراحل [Help with my compromised account] را دنبال کنید.
> (https://help.twitter.com/en/safety-and-security/twitter-account-compromised).
>
> اگر حساب شما به خطر نیفتد، یا مشکلات دیگری برای دسترسی به حساب دارید، می‌توانید مراحل را دنبال کنید ["Request help restoring your account"](https://help.twitter.com/forms/restore).
>
> لطفا توجه داشته باشید که دریافت پاسخ به درخواست‌های شما ممکن است کمی طول بکشد. این صفحه را در نشانک های خود ذخیره کنید و چند روز دیگر دوباره بازگردید.

آیا روش ریکاوری موثر بود؟

##### Options

- [بله](#resolved_end)
- [خیر](#account_end)

<!---=========================================================
//Protonmail
//========================================================= -->

### ProtonMail

#### پورتون میل

> لطفا [دستورالعمل بازیابی رمز عبور] ر برای ریکاوری ایمیل دنبال کنید (https://protonmail.com/support/knowledge-base/reset-password/).
>
> لطفا توجه داشته باشید که اگر رمز عبور خود را بازیابی کنید، نمی‌توانید ایمیل‌ها و کانتکت موجود خود را ببنید زیرا آنها با استفاده از رمز عبور رمزگذاری شده‌اند. دنبال کردن مراحل زیر می توان داده های قدیمی را بازیابی کرد[Recover Encrypted Message and Files](https://proton.me/support/recover-encrypted-messages-files).

آیا روش ریکاوری موثر بود؟

##### Options

- [بله](#resolved_end)
- [خیر](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

#### هاتمیل/اوت‌لوک/لایو

آیا شما به ریکاوری ایمیل/موبایل خود که اکانت شما متصل است دسترسی دارید؟

##### Options

- [بله](#I-have-access-to-recovery-email-Hotmail)
- [خیر](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

#### من به ایمیل بازیابی دسترسی دارم

> ایمیل ریکاوری را چک کنید تا ببینید آیا ایمیل "Microsoft account password change" را از ماکروسافت دریافت کرده‌اید یا خیر.
>
> هنگام بررسی ایمیل‌ها، همیشه مراقب فیشینگ باشید. اگر از صحت یک پیام مطمین نیستید، لطفاً آن را بررسی کنید [Suspicious Messages Workflow](/fa/topics/suspicious-messages/).

آیا ایمیل "Microsoft account password change" را از ماکروسافت دریافت کرده‌اید؟

##### Options

- [بله](#Email-received-Hotmail)
- [خیر](#Recovery-Form-Hotmail)

### Email-received-Hotmail

#### من یک ایمیل بازیابی دریافت کرده ام

هنگامی که صحت پیام را تایید کردید، اطلاعات ارائه شده در ایمیل را بررسی کنید. ببینید که آیا لینک "Reset your password" وجود دارد. آیا این لینک وجود دارد؟

##### Options

- [بله](#Recovery-Link-Found-Hotmail)
- [خیر](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

#### من لینک ریکاوری دارم

> لطفا از لینک "Reset your password" برای رمز عبور جدید و ریکاور کردن حساب استفاده کنید

آیا با لینک "Reset your password" توانستید حساب خود را بازیابی کنید؟

##### Options

- [بله](#resolved_end)
- [خیر](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

#### فرم بازیابی هات میل

> لطفا [the "Sign In Helper Tool"](https://go.microsoft.com/fwlink/?linkid=2214157) امتحان کنید. دستورالعمل‌هاز این ابزار را دنبال کنید، از جمله افزودن حسابی که می‌خواهید ریکاور کنید و به سوالات مربوط به اطلاعات موجود برای ریکاوری پاسخ دهید.
>
> لطفا توجه داشته باشید که دریافت پاسخ به درخواست‌های شما ممکن است کمی طول بکشد. این صفحه را در نشانک های خود ذخیره کنید و چند روز دیگر دوباره بازگردید.

آیا روش ریکاوری موثر بود؟

##### Options

- [بله](#resolved_end)
- [خیر](#account_end)

<!---==================================================================
//FacebookMetaFacebookMetaFacebookMetaFacebookMetaFacebookMetaFacebook
//================================================================== -->

### Facebook

#### فیسبوک

آیا شما به ریکاوری ایمیل/موبایل خود که اکانت شما متصل است دسترسی دارید؟

##### Options

- [بله](#I-have-access-to-recovery-email-Facebook)
- [خیر](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

#### من به ایمیل بازیابی دسترسی دارم

> بررسی کنید که آیا ایمیل "?Did you just reset your password" را از فیس بوک دریافت کرده‌اید؟
>
> هنگام بررسی ایمیل‌ها، همیشه مراقب فیشینگ باشید. اگر از صحت یک پیام مطمین نیستید، لطفاً آن را بررسی کنید [Suspicious Messages Workflow](/fa/topics/suspicious-messages/).

آیا ایمیل "?Did you just reset your password" را از فیس بوک دریافت کرده‌اید؟

##### Options

- [بله](#Email-received-Facebook)
- [خیر](#Recovery-Form-Facebook)

### Email-received-Facebook

#### من یک ایمیل بازیابی دریافت کرده ام

هنگامی که صحت پیام را تایید کردید، اطلاعات ارایه شده در ایمیل را بررسی کنید. آیا ایمیل حاوی پیامی با مضمون و لینک "This wasn't me"با پیوند است؟

##### Options

- [بله](#Recovery-Link-Found-Facebook)
- [خیر](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

#### من لینک ریکاوری دارم

> لطفا از لینک "This wasn't me" برای رمز عبور جدید و ریکاور کردن حساب استفاده کنید

آیا توانستید حساب خود را بازیابی کنید؟

##### Options

- [بله](#resolved_end)
- [خیر](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

#### فرم بازیابی فیس بوک

> لطفا [the form to recover your account] را امتحان کنید (https://www.facebook.com/login/identify).
>
> لطفا توجه داشته باشید که دریافت پاسخ به درخواست‌های شما ممکن است کمی طول بکشد. این صفحه را در نشانک های خود ذخیره کنید و چند روز دیگر دوباره بازگردید.

آیا روش ریکاوری موثر بود؟

##### Options

- [بله](#resolved_end)
- [خیر](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== -->

### Instagram

#### اینستاگرام

آیا شما به ریکاوری ایمیل/موبایل خود که اکانت شما متصل است دسترسی دارید؟

##### Options

- [بله](#I-have-access-to-recovery-email-Instagram)
- [خیر](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

#### من به ایمیل بازیابی دسترسی دارم

> ایمیل ریکاوری را چک کنید تا ببینید آیا ایمیل"Your Instagram password has been changed" را از توییتر دریافت کرده‌اید یا خیر.
>
> هنگام بررسی ایمیل‌ها، همیشه مراقب فیشینگ باشید. اگر از صحت یک پیام مطمین نیستید، لطفاً آن را بررسی کنید [Suspicious Messages Workflow](/fa/topics/suspicious-messages/).

آیا ایمیل"Your Instagram password has been changed" را از توییتر دریافت کرده‌اید?

##### Options

- [Yes](#Email-received-Instagram)
- [No](#Recovery-Form-Instagram)

### Email-received-Instagram

#### من یک ایمیل بازیابی دریافت کرده ام

هنگامی که صحت پیام را تایید کردید، اطلاعات ارائه شده در ایمیل را بررسی کنید. ببینید که آیا لینک "secure your account here" وجود دارد. آیا این لینک وجود دارد؟

##### Options

- [بله](#Recovery-Link-Found-Instagram)
- [خیر](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

#### من یک لینک بازیابی پیدا کردم

> لطفا از لینک "secure your account here" برای رمز عبور جدید و ریکاور کردن حساب استفاده کنید.

آیا توانستید حساب خود را بازیابی کنید؟

##### Options

- [بله](#resolved_end)
- [خیر](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

#### فرم بازیابی اینستاگرام

> لطفا دستورالعمل این لینک ["I think my Instagram account has been hacked"] را برای ریکاوری حساب خود دنبال کنید. (https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked)
>
> لطفا توجه داشته باشید که دریافت پاسخ به درخواست‌های شما ممکن است کمی طول بکشد. این صفحه را در نشانک های خود ذخیره کنید و چند روز دیگر دوباره بازگردید.

آیا روش ریکاوری موثر بود؟

##### Options

- [بله](#resolved_end)
- [خیر](#account_end)

<!--- ==================================================================
TiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktok
//================================================================== -->

### Tiktok

#### تیک تاک

آیا شما به ریکاوری ایمیل/موبایل خود که اکانت شما متصل است دسترسی دارید؟

##### Options

- [بله](#I-have-access-to-recovery-email-Tiktok)
- [خیر](#Recovery-Form-Tiktok)

### I-have-access-to-recovery-email-Tiktok

#### من به ایمیل بازیابی دسترسی دارم

> اگر به ایمیل ریکاوری دسترسی دارید، لطفا با دنبال کردن موارد زیر، رمز عبور خود را تغییر دهید [Tiktok Password Reset process](https://www.tiktok.com/login/email/forget-password).

آیا توانستید حساب خود را بازیابی کنید؟

##### Options

- [بله](#resolved_end)
- [خیر](#Recovery-Form-Tiktok)

### Recovery-Form-Tiktok

#### دستورالعمل های بازیابی  تیک تاک

> لطفا دستورالعمل ["My account has been hacked"] را برای ریکاوری حساب خود دنبال کنید. (https://support.tiktok.com/en/log-in-troubleshoot/log-in/my-account-has-been-hacked) to recover your account.

آیا روش ریکاوری موثر بوده است؟

##### Options

- [بله](#resolved_end)
- [خیر](#account_end)

### Fb-Page_end

#### بله مشکلم حل شد

خیلی خوبه که مشکل شما حل شد! لطفا این توصیه ها را بخوانید تا به شما کمک کند احتمال از دست دادن دسترسی به صفحه خود را در آینده به حداقل برسانید:

- احراز هویت دو مرحله‌ای (2FA) را برای همه مدیران صفحه فعال کنید.
- مسوولیت‌های مدیریتی را فقط به افرادی که به آنها اعتماد دارید و پاسخگو هستند اختصاص دهید.
- اگر شخص مورد اعتمادی دارید بهتر است بیش از یک مدیر برای حساب داشته باشید. به خاطر داشته باشید که احراز هویت دومرحله‌ای را برای همه مدیران حساب فعال کنید.
- به طور مرتب دسترسی و مجوزهای صفحه را بررسی کنید. همیشه حداقل سطح دسترسی لازم را به کاربر برای انجام کار خود اختصاص دهید.

### account_end

#### مشکل من حل نشد

اگر رویه‌های پیشنهادی در این جزوه به شما کمک نکرده است تا دسترسی به حسابتان را بازیابی کنید، می‌توانید با سازمان‌های زیر تماس بگیرید و کمک بیشتری بخواهید:

[orgs](:organisations?services=account)

### resolved_end

#### بله مشکلم حل شد

امیدواریم این راهنمای DFAK مفید بوده باشد. لطفا نظرات خود برای ما بنویسید [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

لطفا این توصیه ها را بخوانید تا به شما کمک کند احتمال از دست دادن دسترسی به صفحه خود را در آینده به حداقل برسانید:

-همیشه ایده خوبی است که احراز هویت دو مرحله ای (2FA) را برای حساب های خود در همه پلتفرم هایی که از آن پشتیبانی می کنند فعال کنید.

- هرگز از یک رمز عبور برای بیش از یک حساب استفاده نکنید. اگر در حال حاضر این کار را انجام می دهید، باید آنها را با استفاده از یک رمز عبور منحصر به فرد برای هر یک از حساب های خود تغییر دهید.
- استفاده از ابزارهای مدیریت رمز عبور به شما کمک می کند تا رمزهای عبور منحصر به فرد و قوی را برای همه حساب‌های خود ایجاد کنید.
- هنگام استفاده از شبکه‌های Wi-Fi عمومی غیرقابل اعتماد محتاط باشید و اگر ممکن است هنگام اتصال از طریق آنها از VPN یا Tor استفاده کنید.

### Resources

- [Access Now Helpline Community Documentation: Recommendations on Team Password Managers](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Security Self-Defense: Protecting Yourself on Social Networks](https://ssd.eff.org/en/module/protecting-yourself-social-networks)​​​​​​​
- [Security Self-Defense: Creating Strong Passwords Using Password Managers](https://ssd.eff.org/en/module/creating-strong-passwords#0)

<!--- Edit the following to add another service recovery workflow:
#### service-name

آیا شما به ریکاوری ایمیل/موبایل خود که اکانت شما متصل است دسترسی دارید؟

- [بله](#I-have-access-to-recovery-email-google)
- [خیر](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

> چک کنید که آیا ایمیل "[Password Change Email Subject]" email from service_name. گرفته‌اید؟ آیا دریافت کرده‌اید؟
>
> هنگام بررسی ایمیل‌ها، همیشه مراقب فیشینگ باشید. اگر از صحت یک پیام مطمین نیستید، لطفاً آن را بررسی کنید
[Suspicious Messages Workflow](https://digitalfirstaid.org/en/topics/suspicious-messages/)

- [بله](#Email-received-service-name)
- [خیر](#Recovery-Form-service-name

### Email-received-service-name

چک کنید که آیا لینک "recover your account" وجود دارد. آیا وجود دارد؟

- [بله](#Recovery-Link-Found-service-name)
- [خیر](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> لطفا از لینک[Recovery Link Description](URL) برای ریکاوری حساب خود استفاده کنید.
آیا توانستید با لینک  "[Recovery Link Description]"  حساب خود را ریکاور کنید؟

- [بله](#resolved_end)
- [خیر](#Recovery-Form-service-name)

### Recovery-Form-service-name

>لطفا این فرم ریکاوری را برای ریکاوری حساب امتحان کنید: [Link to the standard recovery form].
>
> لطفا توجه داشته باشید که دریافت پاسخ به درخواست‌های شما ممکن است کمی طول بکشد. این صفحه را در نشانک های خود ذخیره کنید و چند روز دیگر دوباره بازگردید.

آیا روش ریکاوری موثر بوده است؟

- [بله](#resolved_end)
- [خیر](#account_end)

-->
