---
language: ru
layout: organisation
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, по всему миру
response_time: 12 часов
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Internews, помимо прочего, помогает частным лицам, организациям и сообществам по всему миру укреплять цифровую безопасность, защищать доступ к открытому интернету без цензуры. В тренингах Internews участвуют тысячи журналистов и правозащитников из более чем 80 стран. У Internews есть эффективная сеть местных и региональных аудиторов и тренеров по цифровой безопасности, знакомых с методологией SAFETAG (Security Auditing Framework and Evaluation Template for Advocacy Groups, https://safetag.org), разработку которой также возглавляет Internews. В области оперативного реагирования на угрозы мы выстраиваем крепкие партнёрские связи как с участниками гражданского общества, так и с частными компаниями. Мы помогаем партнёрам налаживать их безопасное и неподцензурное представительство в интернете. Internews предлагает техническое и нетехническое содействие — от базовой оценки рисков на основе аудита по методике SAFETAG до разработки организационных политик безопасности и основанных на них стратегий борьбы с угрозами. Чтобы помочь правозащитникам и журналистам, мы поддерживаем проекты анализа фишинга и вредоносных программ.