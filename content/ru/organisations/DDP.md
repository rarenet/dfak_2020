---
language: ru
layout: organisation
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: понедельник — четверг, 9:00 — 17:00 CET
response_time: 4 дня
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Digital Defenders Partnership предлагает правозащитникам поддержку в ситуациях цифровых угроз. DDP старается укреплять местные сети быстрого реагирования. Организация координирует программы поддержки для частных лиц и организаций — правозащитников, журналистов, гражданских активистов, блогеров.

У DDP есть пять разных программ финансирования по инцидентам безопасности, а также долгосрочные гранты, призванные поддерживать ресурсы организаций. Кроме того, мы координируем программу Digital Integrity Fellowship, в рамках которой организации получают персонализированные тренинги по цифровой безопасности и приватности, и программу Сети быстрого реагирования (Rapid Response Network).
