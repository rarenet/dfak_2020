---
language: ru
layout: organisation
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: с понедельника по пятницу в рабочие часы по времени восточного побережья (США)
response_time: 1 день
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp_key: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: да
---

Фонд свободы прессы (Freedom of the Press Foundation, FPF) — некоммерческая организация, зарегистрированная в США (501(c)3). FPF защищает и поддерживает расследовательскую журналистику XXI века. FPF проводит тренинги для всех, от крупных медиацентров до отдельных журналистов, по целому спектру тем, связанных с безопасностью и приватностью, чтобы журналисты могли эффективнее защищать себя, свои организации и свои источники информации.