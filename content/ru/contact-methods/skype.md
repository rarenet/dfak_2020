---
layout: contact-method
title: Skype
author: mfc
language: ru
summary: Способы связи
date: 2018-09
permalink: /ru/contact-methods/skype.md
parent: /ru/
published: true
---

И содержание вашего сообщения, и сам факт того, что вы его отправили конкретной организации, могут стать известны государственным учреждениям и правоохранительным органам.