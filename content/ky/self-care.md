---
layout: page
title: "Өзүңө жана коомго кам көрүү"
author: Peter Steudtner, Flo Pagano
language: ky
summary: "Интернеттеги онлайн куугунтуктар, коркутуулар жана санариптик чабуулдардын кандай гана түрү болбосун, ар кандай терең жана эмоционалдык формаларда оор кыйынчылыктарды пайда кылышы мүмкүн: мунун кесепетинен ар кандай баш аламан сезимдер пайда болушу ыктымал, сиз өзүңүздү күнөөлүү, уялуу, тынчсыздануу, ачуулануу, кыжырдануу, алсыздык, ал тургай психологиялык же физикалык бакубаттуулугуңуз үчүн коркуу сезими менен коштолушу мүмкүн."
date: 2023-04
permalink: self-care
parent: Home
sidebar: >
  <h3>Өзүңүздү жана командаңызды негативдүү, алсыратчу сезимдерден кантип коргоого болот:</h3>

  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Активисттер үчүн өз алдынча жардам: Сиздин эң баалуу ресурсуңузду сактоо</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Адам укуктарын коргоону улантуу үчүн өзүңүзгө кам көрүңүз</a></li>
    <li><a href="https://www.frontlinedefenders.org/en/resources-wellbeing-stress-management">Ден соолукту чыңдоо жана стрессти башкаруу үчүн ресурстар</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Куугунтукка кабылган адамдарга өзүн-өзү жардам берүү</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Кибер аялдар үчүн өзүн-өзү жардам боюнча окуу модулу</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">Амандык жана коом</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Интернетте кодулоого кабылган адамга жардам берүүнүн 20 жолу</a></li>
    <li><a href="https://www.hrresilience.org/">Адам укуктары боюнча туруктуулук долбоору</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_AH_EN+001/about">Totem- долбоору онлайн окутуу курсу: Психикалык ден соолугуңузга кам көрүү (каттоо талап кылынат)</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_PAP_EN+001/about">Totem- долбоору"Онлайн окутуу курсу: Психологиялык биринчи жардам (каттоо талап кылынат)</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-2-individual-responses-to-threat.html">Коркунучка жекече реакция жасоо</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-4-team-and-peer-responses-to-threat.html">Команданын жана кесиптештердин коркунучка реакциясы</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-5-communicating-about-threats-in-teams-and-organisations.html">Коллективдерде жана уюмдардагы баарлашуу</a></li>
    </ul>
---

# Өзүңүздү көңүлсүз сезип жатасызбы?

Интернеттеги онлайн куугунтуктар, коркутуулар жана санариптик чабуулдардын кандай гана түрү болбосун, ар кандай терең жана эмоционалдык формаларда оор кыйынчылыктарды пайда кылышы мүмкүн: мунун кесепетинен ар кандай баш аламан сезимдер пайда болушу ыктымал, сиз өзүңүздү күнөөлүү, уялуу, тынчсыздануу, ачуулануу, кыжырдануу, алсыздык, ал тургай психологиялык же физикалык бакубаттуулугуңуз үчүн коркуу сезими менен коштолушу мүмкүн.Ал эми бул сезимдер коллективде такыр башкача болушу мумкун.

Негизинен алганда, сезимдер "Туура" жана туура эмес деп бөлүнбөйт, анткени адамдагы оор абал жана жеке маалыматтын маанилүүлүгү ар ким үчүн ар башка. Сиздин бүт эмоцияңыз негиздүү жана сиздин реакцияңыз туурабы же туура эмеспи деп тынчсыздануунун кереги жок.

Биринчиден, өзүңүздү же командадагы кимдир бирөөнү күнөөлөгөнүңүз өзгөчө кырдаалдардан чыгууга жардам бербей турганын унутпаңыз. Сиз техникалык жактан да, эмоционалдык жактан да өзгөчө кырдаалды чечүүдө сизге же сиздин командаңызга колдоо көрсөтө турган ишенимдүү кимдир бирөөгө кайрылгыңыз келиши мүмкүн.

Интернеттеги онлайн чабуулдун кесепеттерин азайтуу үчүн, эмне болгондугу жөнүндө маалыматты чогултуу керек, бирок муну сөзсүз түрдө өзүңүз жасашыңыз керек эмес - эгер сиз ишенген адам бар болсо, андан бул сайтта берилген көрсөтмөлөрдү аткарууда колдоо көрсөтүүнү сурансаңыз болот, же ал сиз үчүн маалымат чогултушу үчүн ага сиздин гаджеттериңизге же аккаунттарыңызга кирүү мүмкүнчүлүгүн берсеңиз болот. Ошондой эле, бул процесстин жүрүшүндө сиз эмоционалдык же техникалык жардам сурап кайрылсаңыз да болот.

Эгер сиз же сиздин командаңыз санариптик өзгөчө кырдаалдан чыгуу үчүн эмоционалдык жардамга муктаж болсо (же болбосо,алдын ала чара катары), [Team CommUNITY- коомдук психологиялык саламаттык программасы](https://www.communityhealth.team/) ар кандай курч жана узак мөөнөттүү форматтарда психосоциалдык кызматтарды сунуштайт жана ар кандай тилдерде жана контексттерде колдоо көрсөтө алат.
