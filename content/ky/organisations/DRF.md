---
language: ky
layout: organisation
name: Digital Rights Foundation
website: https://digitalrightsfoundation.pk/
logo: DRF.png
languages: English, اردو
services: grants_funding, in_person_training, org_security, web_hosting, web_protection, digital_support, relocation, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: Жумасына 7 күн, 9:00- 17:00 UTC+5 (PST)
response_time: 56 саатта
contact_methods: email, pgp, mail, phone, whatsapp
email: helpdesk@digitalrightsfoundation.pk
pgp_key_fingerprint: 4D02 8B68 866F E2AD F26F  D7CC 3283 B8DA 6782 7590
mail: 180A, Garden Block, Garden Town, Lahore, Pakistan
phone: +9280039393
whatsapp: +923323939312
---

Digital Rights Foundation (DRF) Санариптик укуктар фонду  Пакистанда катталган, 2012- жылы негиздлеген изилдөөчү бейөкмөт уюм. DRF маалымат жана коммуникация технологиялары адам укуктарын, инклюзивдүүлүктү, демократиялык процесстерди жана санариптик башкарууну  колдоого алып, сөз эркиндиги, купуялуулук, маалыматтарды коргоо жана аялдарга каршы онлайн зомбулук боюнча иш алып барат. 