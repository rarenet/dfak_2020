---
language: ky
layout: organisation
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: office hours, UTC+2
response_time: 4 саат
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL- бул Люксембургдагы жеке ишканалар, жамааттар жана бейөкмөт уюмдар үчүн санариптик коопсуздукка ыкчам жооп берүү тобу.

Эгер сиз Люксембургда жашаган колдонуучу, компания же уюм болсоңуз жана чабуулга кабылып жаткан болсоңуз, CIRCL сиздин ишенимдүү өнөктөшүңүз боло алат. CIRCL адистеринин иши өрт өчүрүүчү бригадага окшош: алар болуп өткөн жана күтүлүп жаткан коркунучтарга, опузалоолорго жана инциденттерге тез жана натыйжалуу жооп беришет.

CIRCL максатына санариптик коркунучтар жана опузалоолор боюнча маалыматтарды системалуу жана оперативдүү чогултуу, талдоо, коомчулукка маалымат берүү жана инциденттерге жооп берүү кирет.
