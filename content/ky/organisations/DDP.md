---
language: ky
layout: organisation
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский, Кыргызча
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: Дүйшөмбү- Жума күндөрү  9:00- 17:00 чейин Борбордук Европа убакыт алкагы боюнча (CET)
response_time: 4 күн
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Digital Defenders Partnership санариптик коркунучка дуушар болгон укук коргоочуларга колдоо көрсөтөт жана жергиликтүү ыкчам жооп берүүчү тармактарды бекемдөө боюнча иш алып барат. DDP укук коргоочулар, журналисттер, жарандык активисттер жана блоггерлер сыяктуу адамдарга жана уюмдарга чукул жана тез арада жардам көрсөтүүнү координациялайт.

DDP ыкчам кырдаалдарды чечүүгө багытталган беш түрдүү каржылоо түрлөрүн, ошондой эле уюмдун потенциалын жогорулатууга багытталган узак мөөнөттүү гранттарды камтыйт. Мындан тышкары, алар уюмдарга жеке санариптик коопсуздукту жана купуялыкты үйрөтүүчү Digital Integrity Fellowship программасын жана Rapid Response Network программасын координациялашат.
