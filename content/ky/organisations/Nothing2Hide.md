---
language: ky
layout: organisation
name: Nothing2Hide
website: https://nothing2hide.org
logo: nothing2hide.png
languages: Français, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: Дүйшөмбүдөн Жумага чейин, 9:00-18:00 CET
response_time: 1 күн
contact_methods: email, pgp, signal, web_form, wire
web_form: https://vault.tech4press.org/#/ (online secured form - based on Globaleaks)
email: help@tech4press.org
pgp_key:  http://tech4press.org/fr/contact-mail/
pgp_key_fingerprint: 2DEB 9957 8F91 AE85 9915 DE94 1B9E 0F13 4D46 224B
signal: +33 7 81 37 80 08 <http://tech4press.org/fr/#signal>
wire: tech4press
---

Nothing2Hide (N2H) – бул журналисттерге, юристтерге, укук коргоочуларга жана “жөнөкөй” жарандарга ар бир контекстке ылайыкташтырылган техникалык чечимдерди жана окутууларды берүү аркылуу алардын маалыматтарын жана коммуникацияларын коргоо каражаттары менен камсыз кылууну максат кылган ассоциация. Биздин максат – дүйнө жүзү боюнча демократияны бекемдөө үчүн маалыматты жайылтуу жана коргоо үчүн технологияны колдонуу болуп эсептелет.
