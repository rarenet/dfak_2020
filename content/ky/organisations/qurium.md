---
language: ky
layout: organisation
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: Дүйшөмбү-Жекшемби 8:00-18:00 CET
response_time: 4 саат
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation - бул укук коргоо уюмдары, иликтөөчү журналисттер жана активисттер үчүн коопсуздук чечимдерин камсыздоочу көз карандысыз медиа. Qurium тобокелдикке кабылган уюмдар жана жеке тараптар үчүн колдоо көрсөтүү менен кесипкөй, индивидуалдуу жана коопсуз чечимдердин портфолиосун сунуштайт, ага төмөнкүлөр кирет:


- тобокелдик жогору болгон сайттар үчүн DDoS коргоосу менен коопсуз хостинг
- Rapid Response түздөн түз коркунучта турган уюмдар жана жеке тараптар үчүн ыкчам жооп берүү 
- веб-сервистердин жана мобилдик тиркемелердин коопсуздук аудити
- интернет -сайттардын бөгөттөлүүсүнөн коргоо чаралары
- Санариптик чабуулдарды, жалган колдонмолорду, зыяндуу программаларды жана жалган маалыматтарды криминалдык иликтөөлөр
