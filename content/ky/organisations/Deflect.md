---
language: ky
layout: organisation
name: Deflect
website: https://www.deflect.ca
logo: deflect.png
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino, Кыргызча
services: ddos, web_hosting, web_protection
beneficiaries: hrds, cso
hours: Дүйшөмбүдөн- Жумага чейин, 24/5, UTC-4
response_time: 6 саат
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

Deflect – бул жарандык активисттердин жана укук коргоочулардын веб-сайттарын санариптик чабуулдардан коргоо үчүн акысыз кызмат.
