---
language: ky
layout: organisation
name: TibCERT - Tibetan Computer Emergency Readiness Team
website: https://tibcert.org/
logo: tibcert-logo.png
languages: English, Tibetan
services: in_person_training, org_security, assessment, digital_support, secure_comms, device_security, advocacy
beneficiaries: journalists, hrds, activists, cso
hours: Дүйшөмбү - Жума, GMT+5.30
response_time: Иш убактысынан тышкары 24 саат, иш убактысында 2 саат
contact_methods: email, pgp, phone, whatsapp, signal, mail
email: info@tibcert.org
pgp: 0xF34C6C41A569F186
pgp_key_fingerprint: D1C5 8DE6 E45B 4DD7 92EF  F970 F34C 6C41 A569 F186
mail: Dhangshar House, Temple Road, McleodGanj, Distt. Kangra, HP - 176219 - India
phone: "Mobile: +919816170738 Office: +911892292177"
whatsapp: +919816170738
signal: +919816170738
initial_intake: yes
---

Тибеттин компьютердик оперативдүү жооп берүү тобу (TibCERT) тибет коомчулугундагы интернет желесиндеги коркунучтарды азайтуу жана айланып өтүүчү, тибеттиктердин диаспорасындагы коркунучтарга жана Тибеттеги көзөмөл/цензурага каршы туруу максатында техникалык жактан күчтүү  коалиция түзүмү болуу  менен жалпы Тибет коомчулугу үчүн  эркиндикти жана коопсуздукту камсыз кылууну көзөмөлдөйт. 

TibCERT миссиясы төмөнкүлөрдү камтыйт:

- Тибет коомчулугунун катышуучуларынын ортосунда санариптик коопсуздук маселелери боюнча узак мөөнөттүү кызматташтык үчүн платформаны түзүү жана колдоо.
- Тибеттиктер менен изилдөөчүлөрдүн ортосунда өз ара пайда алуу үчүн дүйнө жүзү боюнча зыяндуу код жана санариптик коопсуздук боюнча байланыштарды жана кызматташтыкты чыңдоо.
-Интернет желесиндеги онлайн кол салуулардын алдын алуу жана коргоо үчүн тибеттиктерге жеткиликтүү ресурстарды иштеп чыгуу, коомчулукка коркунуч туудурган маалыматтарды жана сунуштарды үзгүлтүксүз жарыяап туруу.
- Тибеттин жашоочулары цензурага жана көзөмөлгө түздөн түз кабылбоосун камсыз кылуу максатында, ошондой эле аны айланып өтүү үчүн жеринде үзгүлтүксүз жана деталдуу маалымат жана аналитика, ошондой эле майнаптуу чечимдерди берүү менен жардам көрсөтүү.
