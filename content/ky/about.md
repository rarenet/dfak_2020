---
layout: page
title: "Жалпы маалымат"
language: ky
summary: "Санариптик биринчи жардам комплекти жөнүндө."
date: 2023-05
permalink: about
parent: Home
---

Санариптик Биринчи Жардам Комплекти - бул [RaReNetтин (Rapid Response Network Ыкчам Жооп Берүү Тармагы)](https://www.rarenet.org/) жана [CiviCERTтин](https://www.civicert.org/) биргелешкен аракети.

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Ыкчам Жооп Берүү Тармагы - бул ыкчам жооп берүүчүлөрдүн жана санариптик коопсуздуктун чемпиондорунун эл аралык тармагы. Анын ичинде Access Now, Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Fembloc, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtualroad, жана ошондой эле санариптик коопсуздугу менен ыкчам жооп берүү тармагында иштеп жаткан жеке коопсуздук эксперттер.

Бул уюмдарынын жана жеке адамдарынын айрымдары CiviCERTтин бир бөлүгү деп саналат. CiviCERT болсо эл аралык санариптик коопсуздугун жардам панелдеринен жана ошондой эле инфраструктура провайдеринен түзүлгөн, алар негизинен социалдык адилетүүлүккө жана адам жана санариптик укуктарын коргоого умтулган топторду жана адамдарды колдоого багытталган. Ошондой эле, CiviCERT - бул тез жооп берүү коомчулугунун бөлүштүрүлгөн CERT (Computer Emergency Response Team Компьютердик Өзгөчө Кырдаалдарга Жооп Берүү Тобу)аракеттери үчүн кесипкөй бир багыты. CiviCERT Trusted Introducer (Европалык ишенимдүү компьютердик өзгөчө кырдаалга жооп берүү топторунун тармагы) тарабынан аккредиттелген.

Ошондой эле, Санариптик Биринчи Жардам Комплекти [тышкы салымдарды кабыл алган ачык булактуу долбоор болуп саналат](https://gitlab.com/rarenet/dfak_2020).

[Албан](https://digitalfirstaid.org/sq/) тилине локалдаштыруу үчүн [Metamorphosis Foundation](https://metamorphosis.org.mk), [армян](https://digitalfirstaid.org/hy/) тилине локалдаштыруу үчүн [Media Diversity Institute](https://mdi.am/en/home) жана санариптик биринчи жардам топтомун бирма, [индонезия](https://digitalfirstaid.org/id/) жана [тай](https://digitalfirstaid.org/th/) тилдеринп локалдаштыруу үчүн [EngageMedia](https://engagemedia.org/) жардамы учун рахмат.

Эгер сиз Санариптик Биринчи Жардам Комплектин туташуу мүмкүнчүлүгү чектелген жерлерде колдонгуңуз келсе, же байланыш жолдорун табуу кыйын болсо, [оффлайн версиясын](https://digitalfirstaid.org/dfak-offline.zip) жүктөсө болот.

Санариптик Биринчи Жардам Комплекти боюнча кандайдыр бир комментарий, сунуш же суроолор болсо, төмөнкү дарекке кайрылсаңыз болот: dfak @ digitaldefenders . org

GPG - Fingerprint: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
