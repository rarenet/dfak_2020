---
layout: contact-method
title: Email
author: mfc
language: ky
summary: Байланыш жолдору
date: 2018-09
permalink: contact-methods/email.md
parent: /ky/
published: true
---

Сиздин кайрылууңуздан мазмуну, ошондой эле кайсы бир уюмга кайрылганыңыз/байланышка чыкканыңыз мамлекеттик органдарга жана укук коргоо органдарына белгилүү болуп калышы толук ыктымал.
