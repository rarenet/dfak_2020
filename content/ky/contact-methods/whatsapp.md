---
layout: contact-method
title: WhatsApp
author: mfc
language: ky
summary: Байланыш жолдору
date: 2018-09
permalink: contact-methods/whatsapp.md
parent: /ky/
published: true
---

WhatsApp тиркемесин колдонууда сүйлөшүүңүз корголуп, эмнени сүйлөшкөндүгүңүз сиз жана сиз сүйлөшкөн тарапка гана көрүү мүмкүн. Бирок, сиз сүйлөшкөн маалымат алмашкан тарап менен байланышка чыкканыңыздын өзү мамлекеттик органдарга жана укук коргоо органдарына белгилүү болуп калышы толук ыктымал.

Пайдалуу ресурстар: [WhatsApp жөнүндө Android тиркемесин колдонгондор үчүн аны кантип колдонуу боюнча багыттама колдонмо](https://ssd.eff.org/en/module/how-use-whatsapp-android) [WhatsApp жөнүндө iOS тиркемесин колдонгондор үчүн аны кантип колдонуу боюнча багыттама колдонмо](https://ssd.eff.org/en/module/how-use-whatsapp-ios).
