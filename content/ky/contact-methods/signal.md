---
layout: contact-method
title: Signal
author: mfc
language: ky
summary: Байланыш жолдору
date: 2018-09
permalink: contact-methods/signal.md
parent: /ky/
published: true
---

Эгерде сиз Signal мессенжерин колдонсоңуз, анда сиздин билдирүүлөрүңүз шифрленип, жашырындуулук сакталат: Сиз жиберген маалыматты/катты кабыл алуучу тарап гана шифрди чечип, окуй алат. Сиз байланышканыңыз тууралуу да сиздин маектешиңизге гана белгилүү бойдон калат. Signalды колдонууда ысым катары телефон номериңиз колдонулаарын эсиңизден чыгарбаңыз, бул деген сиз номериңизди маектешиңизге ачыктап жатасыз дегенди билдирет.

Пайдалуу ресурстар: [Signal жөнүндө Android тиркемесин колдонгондор үчүн аны кантип колдонуу боюнча багыттама колдонмо](https://ssd.eff.org/en/module/how-use-signal-android), [Signal жөнүндө iOS тиркемесин колдонгондор үчүн аны кантип колдонуу боюнча багыттама колдонмо](https://ssd.eff.org/en/module/how-use-signal-ios), [Телефон номериңизди көрсөтпөстөн Signal месседжерин кантип колдонсо болот](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)
