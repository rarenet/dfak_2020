---
layout: contact-method
title: PGP
author: mfc
language: ky
summary: Байланыш жолдору
date: 2020-11
permalink: contact-methods/pgp.md
parent: /ky/
published: true
---

PGP (Pretty Good Privacy) жана ачык булактуу GPG (Gnu Privacy Guard) электрондук каттардын мазмунун шифрлеп, жашыруун кылууга мүмкүндүк берүүчү программалар. Сиздин электрондук почта провайдериңизге жана билдирүүңүзгө кирүү мүмкүнчүлүгү бар башка бирөө да аны окуй албайт. Бирок сиздин башка дарекке кат жөнөткөнүңүздүн өзү мамлекеттик органдарга жана укук коргоо органдарына белгилүү болуп калышы ыктымал. Мунун алдын алуу үчүн, сиз жөнүндө жеке маалыматты камтыбаган башка кошумча электрондук почта ачып алганыңыз оң.

Ресурстар: [Access Now санариптик колдоо көрсөтүү кызматынан коопсуз электрондук почта жөнүндө](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

[Freedom of the Press Foundation: Mailvelope менен электрондук почтаны шифрлөө же болбосо жашыруу: Эми баштап жаткандар үчүн колдонмо](https://freedom.press/training/encrypting-email-mailvelope-guide/)

[Купуялуулук куралдары: Жеке электрондук почта провайдерлери](https://www.privacytools.io/providers/email/)
