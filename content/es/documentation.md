---
layout: page
title: "Documentar ataques digitales"
author: Constanza Figueroa, Patricia Musomba, Candy Rodríguez, Gus Andrews, Alexandra Hache, Nina
language: es
summary: "Consejos para documentar diferentes tipos de emergencias digitales."
date: 2023-05
permalink: documentation
parent: Home
sidebar: >
  <h3>Lee más sobre como documentar emergencias:</h3>
  <ul>
    <li><a href="https://acoso.online/espana/denuncia-a-la-justicia-espana/#consejos-y-conceptos-generales">Acoso.online Recomendaciones generales. Cómo reaccionar si eres víctima de pornografía no consentida</a></li>
    <li><a href="https://www.techsafety.org/sugerencias-sobre-la-documentacion-para-sobrevivientes-del-abuso-tecnolgico-y-acecho">National Network to End Domestic Violence: Sugerencias sobre la Documentación para Sobrevivientes del Abuso Tecnológico y Acecho</a></li>
    <li><a href="https://chayn.gitbook.io/how-to-build-a-domestic-abuse-case-without-a-lawye/english">Chayn: Cómo construir un caso de abuso doméstico sin una persona abogada (en inglés)</a></li>
    </ul>
---

# Documentar ataques digitales

La documentación de los ataques digitales puede servir para una serie de propósitos. Registrar lo que sucede en un ataque puede ayudarte a:

- comprender mejor la situación y tomar medidas para protegerte,
- proporcionar evidencia que las personas que te ayudan técnica o legalmente necesitarán para apoyarte,
- comprender mejor el patrón de ataques e identificar amenazas adicionales,
- respaldar tu propia tranquilidad y comprender cómo los ataques te afectan emocionalmente.

Ya sea que estés documentando para tu propia comprensión o para buscar apoyo legal o técnico, un registro estructurado de ataques puede ayudarte a comprender mejor:

- la escala y el alcance del ataque, ya sea un ataque único o un patrón repetido, dirigido contra ti o un grupo más grande de personas;
- los hábitos de la persona atacante, si está aislada o forma parte de un grupo organizado, si utiliza información disponible en internet o si accede a tu información personal privada, las tácticas y tecnologías que puede utilizar, etc.;
- si las respuestas que planeas implementar disminuirán las amenazas físicas o en línea para ti o las empeorarán.

A medida que empieces a recopilar información, ten en cuenta lo siguiente:

- ¿Estás documentando principalmente para tu propia tranquilidad y/o para comprender lo que está sucediendo?
- ¿Quieres buscar asistencia técnica para detener los ataques? Considera visitar la [sección de soporte del Kit de Primeros Auxilios Digitales](/es/support/) para ver qué organizaciones de CiviCERT pueden ayudarte.
- ¿También deseas iniciar un caso legal?

Dependiendo de tus objetivos, es posible que debas documentar los ataques de formas ligeramente diferentes. Si necesitas pruebas para un caso legal, deberás recopilar tipos específicos de pruebas que sean legalmente admisibles en los tribunales, como números de teléfono y marcas de tiempo. La asistencia técnica necesitará ver evidencia adicional, como direcciones web (URL), nombres de la persona usuaria y capturas de pantalla. Consulta las secciones a continuación para obtener más detalles sobre la información que debes capturar para cada una de estas rutas posibles.

## Protege tu salud mental y bienestar emocional mientras documentas

Lidiar con ataques digitales es estresante, por lo que tu bienestar debe ser la prioridad. Más allá del tipo de documentación desees obtener, recuerda que la documentación de ataques no debe aumentar tu estrés. Cuando comiences a documentar un ataque considera que:

- hay muchas formas de documentar y registrar, elije aquellas con las que sientas comodidad y que respalden tus objetivos de documentación;
- documentar ataques digitales puede ser emocionalmente exigente y desencadenar recuerdos traumáticos, así que si te abruma, considera delegar la documentación a colegas, personas amigas, de confianza o grupo de monitoreo que pueda mantener actualizada la documentación de los ataques sin volver a traumatizarse;
- si el ataque es a un colectivo, considera rotar las responsabilidades de documentación entre diferentes personas;
- el consentimiento es esencial por lo que la documentación debe continuar siempre y cuando la persona o el grupo involucrado esté de acuerdo;
- [aquí hay algunos consejos más sobre cómo hacer que tus afines o familiares te ayuden](https://onlineharassmentfieldmanual.pen.org/es/recomendaciones-amistades-personas-cercanas/).

Documentar ataques digitales o violencia de género en línea significa recopilar información sobre lo que tú o tu colectivo enfrentan. Esta documentación no sólo tiene que ser racional y técnica. También puede ayudarte a procesar la violencia al registrar cómo te sientes acerca de cada ataque en texto, imagen, audio, video o incluso expresión artística. Recomendamos hacerlo sin conexión y seguir los pasos que se describen a continuación para proteger tu privacidad mientras procesas estos sentimientos.

## Prepárate para documentar un ataque

Después de garantizar tu propio bienestar, pero antes de documentar pruebas individuales, **hacer un "mapa" de toda la información relevante** sobre el ataque puede guiarte en la identificación de todas las pruebas que desees recopilar.

La parte más importante de la documentación es **mantener un registro organizado (o "historial")**. Para la documentación legal o técnica, un registro que sólo contenga datos clave es importante para ayudar a demostrar patrones claros de ataques. Cuando documentes para tu propia tranquilidad, un documento de texto regular con menos estructura puede ser suficiente para ti. Un registro más estructurado también podría ayudarte a pensar en los patrones de los ataques que enfrentas. Tu registro digital podría ser un documento de texto o una hoja de cálculo. También puedes llevar un diario en papel. La elección depende de ti.

**Todas las interacciones digitales dejan rastros (metadatos) que los describen**: tiempo, duración, direcciones, cuentas de envío y recepción, etc. Estos rastros se guardan en registros por sus propios dispositivos, por compañías telefónicas y de redes sociales, por proveedores de Internet. , y por más. Analizarlos puede brindar a personas expertas legal y técnicamente información vital sobre quién está detrás de un ataque. Por ejemplo, un registro de los números de teléfono utilizados en 20 llamadas en un período de tiempo determinado podría respaldar un informe de acoso.

Mantener un registro organizado y estructurado de estos datos te ayudará a demostrar **patrones en ataques** de manera que puedas respaldar un caso legal o ayudar a una persona experta en seguridad digital a bloquear los ataques y proteger tus dispositivos.

La siguiente información será útil en la mayoría de los casos, pero cada caso puede variar. Puedes copiar y pegar los campos a continuación en la parte superior de una hoja de cálculo o tabla de documentos para estructurar tus informes sobre cada ataque, agregando más campos según lo consideres necesario.

| Fecha | Tiempo | Direcciones de correo electrónico o números de teléfono utilizados por rivales | Enlaces relacionados con el ataque | Nombres (incluidos los nombres de cuenta) utilizados por rivales | Tipos de ataque digital, acoso, desinformación, etc. | Técnicas utilizadas por rivales |
| ----- | ------ | ------------------------------------------------------------------------------ | ---------------------------------- | ---------------------------------------------------------------- | ---------------------------------------------------- | ------------------------------- |

Estos son algunos ejemplos de otras plantillas de registro de nuestra comunidad que podrías usar:

- [Plantilla de registro de incidentes de la línea de ayuda de seguridad digital de Access Now (en inglés)](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/tree/master/templates/incident_log_template.md)
- [Sursiendo: Registrando Incidentes de Seguridad Digital como Práctica de Mitigación del Riesgo](https://sursiendo.org/2020/10/registro-y-analisis-de-incidentes-de-seguridad-digital/)

Además de un archivo de registro, **almacena todo lo relacionado con el evento o incidente** en una carpeta o carpetas, incluido tu documento de registro y cualquier evidencia digital adicional que hayas exportado, descargado o capturado en un pantallazo. Incluso la evidencia que no tiene peso legal puede ser útil para mostrar la magnitud de un ataque y ayudarte a planificar estrategias de respuesta.

**Almacena la información que recopiles de forma segura**. Es más seguro almacenar copias de seguridad en tus propios dispositivos (no sólo en línea o “en la nube”). Protege estos archivos con cifrado y en carpetas ocultas o particiones de unidades, si es posible. No deseas perder estos registros o, peor aún, que queden expuestos.

En las redes sociales, **evita bloquear, silenciar o denunciar las cuentas que te atacan** hasta que hayas reunido la documentación que necesitas, ya que esto puede impedirte guardar evidencia de esas cuentas. Si ya has bloqueado, silenciado o denunciado cuentas en las redes sociales, no te preocupes. Es posible que puedas desbloquear o dejar de silenciar cuentas que ya no puedes ver, o la documentación aún puede ser posible mirando esa cuenta desde la cuenta de personas de confianza.

## Toma capturas de pantalla

No subestimes la importancia de tomar capturas de pantalla de todos los ataques (o pedirle a alguien en quien confíes que lo haga por ti). Muchos mensajes digitales son fáciles de borrar o perder de vista. Algunos, como los tweets o los mensajes de WhatsApp, sólo se pueden recuperar con la ayuda de las plataformas, que a menudo tardan mucho en responder a los informes de ataques, si es que responden.

Si no sabes cómo hacer una captura de pantalla en tu dispositivo, realiza una búsqueda en línea de "cómo hacer una captura de pantalla" con la marca, el modelo y el sistema operativo de tu dispositivo (por ejemplo, "Samsung Galaxy S21 Android cómo hacer una captura de pantalla"). .

Si estás tomando una captura de pantalla de un mensaje en un navegador web, es importante que **incluya la dirección (URL) de la página en tu imagen capturada**. Está en la barra de direcciones en la parte superior de la ventana del navegador; a veces los navegadores lo ocultan y lo muestran a medida que se desplaza. Las direcciones ayudan a verificar dónde ocurrió el acoso y ayudan a profesionales técnicos y legales a localizarlo más fácilmente. A continuación puedes ver un ejemplo de una captura de pantalla que muestra la URL.

<img src="/images/screenshot_with_URL.png" alt="Éste es un ejemplo de una captura de pantalla que muestra la URL" width="80%" title="Éste es un ejemplo de una captura de pantalla que muestra la URL.">

<a name="legal"></a>

## Documentación para una causa judicial

Considera si deseas emprender acciones legales. ¿Te expondrá a un riesgo adicional? ¿Puedes permitirte el tiempo y el esfuerzo involucrados? Iniciar un caso legal no siempre es obligatorio o necesario. Considera también si la acción legal te resultará reparadora.

Si decides llevar el caso a los tribunales, busca el consejo de una persona profesional en leyes o una organización legal en la que confíes. Un asesoramiento legal deficiente o malo puede ser estresante y perjudicial. No emprendas acciones legales por tu cuenta a menos que realmente sepas lo que estás haciendo.

En la mayoría de las jurisdicciones, la persona profesional en leyes deberá demostrar que la evidencia es relevante para el caso, cómo se obtuvo y verificó, que se recopiló de manera legal, que la recopilación fue necesaria para construir el caso y otros hechos que hacen la evidencia aceptable para un tribunal. Una vez que esta persona lo haya demostrado, la prueba será valorada por un juez o tribunal.

Cosas a considerar al documentar para un caso legal:

- Es crucial preservar la evidencia rápidamente, desde el comienzo de un ataque, porque quien lo envió o un sitio de redes sociales puede eliminar rápidamente el contenido, lo que dificulta guardar evidencia para acciones legales posteriores.
- Incluso si el ataque parece menor, es importante preservar todas las pruebas. Puede ser parte de un patrón que sólo demostrará ser criminal cuando lo veas de principio a fin.
- Es posible que desees solicitar a las plataformas o ISP que mantengan información sobre el ataque digital para ti. Es posible que esto sólo sea posible durante un breve período de tiempo (semanas o un mes) después de que se haya producido el suceso. Consulta [Información de Without My Consent sobre la presentación de una solicitud de retención de litigio (en inglés)](https://withoutmyconsent.org/resources/something-can-be-done-guide/evidence-preservation/#consider-whether-to-include-a-litigation-hold-request) para obtener más información sobre cómo hacerlo.
- Las marcas de tiempo y las direcciones web y de correo electrónico son cruciales para que la evidencia sea aceptada en la corte. Las personas abogadas tienen que demostrar que un mensaje pasó de un dispositivo a otro en un día en particular a una hora en particular utilizando estas herramientas.
- Estas personas también deben verificar que la evidencia no ha sido manipulada, que ha estado en buenas manos desde que la almacenó y que es auténtica. Las capturas de pantalla y las copias impresas de los correos electrónicos no se consideran pruebas suficientemente sólidas a este respecto.

Por estas razones legales, puede ser mejor para ti contratar a una persona experta como notarios/as o una compañía de certificación digital que pueda testificar en la corte si es necesario. Las empresas de certificación digital suelen ser menos caras que los certificados notariales. Cualquier notaría o empresa de certificación que contrates debe tener suficiente formación técnica para realizar tareas como estas si es necesario:

- confirmar marcas de tiempo
- verificar la existencia o inexistencia de cierto contenido, como comparar tus capturas de pantalla y otra evidencia recopilada con la fuente original, para confirmar que tu evidencia no ha sido manipulada
- ofrecer evidencia de que los certificados digitales coinciden con las URLs
- verificar identidades, como verificar si una persona aparece en una lista de perfiles en una plataforma de redes sociales y si posee un perfil con el apodo que te ataca
- confirmar la propiedad de un número de teléfono en una conversación de WhatsApp

Ten en cuenta que no todas las pruebas que recopilaste serán aceptadas por el tribunal. Investiga los procedimientos legales relacionados con las amenazas digitales en tu país y región para decidir lo que recopilas.

### Documentar violaciones de los derechos humanos

Si necesitas documentar violaciones de derechos humanos y descargar o copiar materiales que las plataformas de redes sociales están eliminando activamente, puede ponerte en contacto con organizaciones como [Mnemonic (en inglés)](https://mnemonic.org/en/our-work) que pueden ayudar preservar estos materiales.

## Cómo guardar pruebas para informes legales e informes forenses digitales

Más allá de guardar capturas de pantalla, aquí hay algunas instrucciones adicionales sobre cómo guardar evidencia de manera más completa para respaldar tu caso y el trabajo de alguien que te brinde asistencia técnica o legal.

- **Registros de llamadas.** Los números de las llamadas entrantes y salientes se almacenan en una base de datos en tu teléfono móvil.
  - Puedes tomar capturas de pantalla del registro.
  - También es posible guardar estos registros haciendo una copia de seguridad del sistema de tu teléfono o utilizando un *software* específico para descargar los registros.
- **Mensajes de texto (SMS)**: la mayoría de los teléfonos móviles te permiten hacer una copia de seguridad de tus mensajes SMS en la nube. Alternativamente, puedes tomar capturas de pantalla de ellos. Ninguno de estos métodos cuenta como prueba legal sin una verificación adicional, pero siguen siendo importantes para documentar los ataques.
  - Android: si tu teléfono no viene con la capacidad de hacer una copia de seguridad de los mensajes SMS, puedes usar una aplicación como [Copia de seguridad y restauración de SMS](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore).
  - iOS: activa la copia de seguridad de mensajes en iCloud: Configuración > [tu nombre de persona usuaria] > Administrar almacenamiento > Copia de seguridad. Toca el nombre del dispositivo que estás utilizando y activa las copias de seguridad de mensajes.
- **Grabación de llamada**. Los móviles más nuevos vienen con una grabadora de llamadas instalada. Verifica cuáles son las normas legales en tu país o jurisdicción local sobre la grabación de llamadas con o sin el consentimiento de ambas partes en una llamada; en algunos casos, puede ser ilegal. Si es legal hacerlo, puedes configurar la grabadora de llamadas para que se active cuando conteste la llamada, sin alertar a la persona que llama de que está siendo grabada.
  - Android: en Ajustes > Teléfono > Ajustes de grabación de llamadas puedes activar la grabación automática de todas las llamadas, llamadas de números desconocidos o de números específicos. Si esta opción no está disponible en tu dispositivo, puedes descargar una aplicación como [Grabador de llamadas](https://play.google.com/store/apps/details?id=com.lma.callrecorder).
  - iOs: Apple es más restrictiva con la grabación de llamadas y ha bloqueado la opción por defecto. Deberás instalar una aplicación como [RecMe](https://apps.apple.com/es/app/call-recorder-recme/id1455818490).
- **Correos electrónicos**. Cada correo electrónico tiene un "encabezado" que contiene información sobre quién envió el mensaje y cómo (como las direcciones y el matasellos en una carta en papel).
- Para obtener instrucciones sobre cómo ver y guardar encabezados de correo electrónico, puede usar [esta guía del Centro de respuesta a incidentes informáticos de Luxemburgo (CIRCL)](https://www.circl.lu/pub/tr-07/).
  - Si tiene más tiempo, también puedes configurar tu correo electrónico para descargar todos los mensajes a un gestor de correo electrónico de escritorio, como Thunderbird, usando [POP3](https://support.mozilla.org/es-es/kb/difference- between-imap-and-pop3#w_cambiando-su-cuenta). Las instrucciones para configurar una cuenta de correo electrónico en Thunderbird se pueden encontrar en la [documentación oficial](https://support.mozilla.org/es-es/kb/manual-account-configuration).
- **Fotos**. Todas las imágenes tienen "etiquetas EXIF", metadatos que pueden indicarte dónde y cuándo se tomó una foto.
  - Es posible que puedas guardar imágenes de un sitio web o mensajes que hayas recibido de una manera que se guarden las etiquetas EXIF ​​que se perderían si solo tomara una captura de pantalla. Mantenga presionada la imagen en un dispositivo móvil o haga clic con el botón derecho en la imagen en una computadora (control-clic en una Mac, usa la tecla de menú en Windows).
- **Sitios web**. Descargar o hacer una copia de seguridad de páginas web completas puede ser útil para aquellas personas que intentan ayudarte.
  - Si la página donde ocurrió el acoso es pública (en otras palabras, no tienes que iniciar sesión para verla), puedes ingresar la dirección de la página en [Wayback Machine de Internet Archive (en inglés)](https://web.archive.org) para guardar la página, registrar la fecha en que la guardaste y volver a la página guardada para esa fecha más tarde.
  - Las capturas de pantalla brindan menos detalles, pero también pueden guardar información importante.
- **WhatsApp**. Las conversaciones de WhatsApp se pueden descargar en formato de texto o con medios adjuntos; también se realiza una copia de seguridad de forma predeterminada en iCloud o Google Drive.
  - Sigue [las instrucciones para exportar el historial de un chat individual o grupal](https://faq.whatsapp.com/1180414079177245/?locale=es_LA&helpref=uf_share&cms_platform=android&cms_id=1180414079177245&draft=false).
- **Telegram**. Para exportar conversaciones en Telegram necesitarás usar la aplicación de escritorio. Puedes elegir el formato y el período de tiempo que deseas exportar, y Telegram generará un archivo HTML. Consulta [las instrucciones (en inglés)](https://telegram.org/blog/export-and-more).
- **Facebook Messenger**. Inicia sesión en tu cuenta de Facebook. Ve a Ajustes > Tu información en Facebook > Descarga una copia de tu información. Aparecerán muchas opciones: selecciona "Mensajes". Facebook tarda en procesar el archivo, pero te avisará cuando esté disponible para descargar.
- Si necesitas guardar **videos** como evidencia, ten una unidad externa (un disco duro, una memoria USB o una tarjeta SD) con suficiente espacio de almacenamiento. Es posible que debas usar una herramienta de captura de pantalla de video. La aplicación Camera, XBox Game Bar o Snipping Tool se pueden usar para esto en Windows, Quicktime en Mac, o puedes usar un complemento de navegador como [Video DownloadHelper (en inglés)](https://www.downloadhelper.net/) para guardar videos.
- En el caso de ataques organizados a gran escala, como todo lo publicado con un **hashtag específico o una gran cantidad de comentarios en línea**, es posible que necesites la ayuda de un equipo de seguridad, un laboratorio forense digital o una universidad para reunir y procesar muchos datos. Las organizaciones a las que puedes acudir para obtener ayuda incluyen [Citizen Lab (en inglés)](https://citizenlab.ca/about/), [K-Lab de Fundación Karisma](https://web.karisma.org.co/klab/) ( trabajando en español), [Meedan (en inglés)](https://meedan.com/programs/digital-health-lab), el Observatorio de Internet de Stanford y el Instituto de Internet de Oxford.
- **Los archivos adjuntos a mensajes maliciosos** son evidencia valiosa. Bajo ninguna circunstancia debes hacer clic o abrirlos. Una persona asesora técnico de confianza debería poder ayudarte a manejar estos archivos adjuntos de manera segura y enviarlos a personas que puedan analizarlos.
  - Puedes usar [Danger Zone (en inglés)](https://dangerzone.rocks/) para convertir archivos PDF, documentos de Office o imágenes potencialmente peligrosos en archivos PDF seguros.
- Para amenazas más específicas, como **_software_ espía** en tus dispositivos o **correos electrónicos maliciosos** enviados específicamente a ti, la recopilación de evidencia técnica más profunda puede ayudar en tu caso. Es importante saber cuándo recopilar evidencia tú y cuándo dejar esto en manos de expertos. Si no estás seguro de cómo proceder, comunícate con una [asesoría de confianza](../soporte).
  - **Si el dispositivo está encendido, déjalo encendido. Si está apagado, déjalo apagado.** Al apagarlo o encenderlo, corres el riesgo de perder información importante. Para investigaciones legales y altamente confidenciales, es mejor involucrar a personas expertas forenses antes de apagar o volver a encender el dispositivo.
  - Toma fotografías del dispositivo que crees que contiene *software* malicioso. Documenta tu condición física y el lugar donde lo encontraste cuando comenzaste a sospechar que fue manipulado. ¿Hay abolladuras o rasguños? ¿Está mojado? ¿Hay herramientas cerca que podrían haber sido utilizadas para manipularlo?
  - Mantén tanto el dispositivo como los datos que has copiado en un lugar seguro.
  - El método exacto de extracción de datos depende del dispositivo. Extraer datos de una computadora portátil es diferente de extraer datos de un teléfono inteligente. Cada dispositivo requiere herramientas y conocimientos específicos.
- La recopilación de datos para amenazas dirigidas a menudo implica copiar **archivos de registro del sistema** que tu teléfono o computadora recopilan automáticamente. Las personas expertas técnicamente que te ayudan con la recopilación de datos pueden solicitarlos. Deberían ayudarte a recopilar metadatos y archivos adjuntos de manera segura y enviarlos a personas que puedan analizarlos.
  - Para conservar estos metadatos, mantén el dispositivo aislado de otros sistemas de almacenamiento, apaga Wi-Fi y Bluetooth, desconecta las conexiones de red por cable y nunca edites los archivos de registro.
  - No conectes una memoria USB e intentes copiar archivos de registro en ella.
  - Estos archivos pueden incluir información sobre el estado de los archivos en el dispositivo (como cómo se accedió a los archivos) o sobre el dispositivo (como si se emitió un comando de apagado o eliminación, o si alguien intentó copiar archivos a otro dispositivo).
