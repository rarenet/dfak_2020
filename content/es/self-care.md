---
layout: page
title: "Cuidado del grupo y propio"
author: Peter Steudtner, Flo Pagano
language: es
summary: "El acoso en línea, las amenazas y otros tipos de ataques digitales pueden crear sentimientos abrumadores y estados emocionales muy delicados: puedes sentir culpa, vergüenza, ansiedad, enojo, confusión, desamparo o incluso temor por tu bienestar psicológico o físico."
date: 2023-04
permalink: self-care
parent: Home
sidebar: >
  <h3>Leer más sobre cómo protegerte y proteger a tu grupo contra sentimientos abrumadores:</h3>

  <ul>
    <li><a href="https://cyber-women.com/es/autocuidado/">Cyberwomen Contenidos formativos Autocuidado</a></li>
    <li><a href="https://im-defensoras.org/categoria/autocuidado/">IM-Defensoras Contenidos sobre Autocuidado</a></li>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Autocuidado para activistas: Mantener su recurso más valioso</a> (en inglés)</li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Auto-cuidado para poder seguir defendiendo los derechos humanos</a> (en inglés)</li>
    <li><a href="https://www.frontlinedefenders.org/en/resources-wellbeing-stress-management">Recursos para el bienestar y el manejo del estrés</a> (en inglés)</li>
    <li><a href="https://iheartmob.org/resources/self_care">Autocuidado para personas que sufren acoso</a> (en inglés)</li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">Bienestar y comunidad</a> (en inglés)</li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/es/procurar-autocuidado/">Cómo procurar el autocuidado</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Veinte maneras de ayudar a alguien que sufre acoso en línea</a> (en inglés)</li>
    <li><a href="https://www.hrresilience.org/">Proyecto de Resiliencia de Derechos Humanos</a> (en inglés)</li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_AH_EN+001/about">Curso de aprendizaje en línea de Totem-Project: Cómo cuidar su salud mental (requiere registro)</a> (en inglés)</li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-2-individual-responses-to-threat.html">Respuestas individuales a la amenaza</a> (en inglés)</li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-4-team-and-peer-responses-to-threat.html">Respuestas de equipos y colegas a amenazas</a> (en inglés)</li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-5-communicating-about-threats-in-teams-and-organisations.html">Comunicándose entre equipos y organizaciones</a> (en inglés)</li>
  </ul>
---

# Cuidado del grupo y propio

El acoso en línea, las amenazas y otros tipos de ataques digitales pueden crear sentimientos abrumadores y estados emocionales muy delicados para ti o tu equipo: puedes sentir culpa, vergüenza, ansiedad, enojo, confusión, desamparo o incluso temor por tu bienestar psicológico o físico. Y estos sentimientos pueden ser bastante diferentes en un equipo.

No existe una forma "correcta" de sentirte, ya que tu estado de vulnerabilidad y lo que la información personal significa para ti es diferente de persona a persona. Cualquier emoción está justificada, y no debes preocuparte si sientes que tu reacción es la correcta o no.

Lo primero que debes recordar es que lo que te está sucediendo no es tu culpa y no debes culparte a ti, contacta a alguien de confianza que pueda ayudarte a enfrentar esta emergencia.

Para mitigar un ataque en línea, deberás recopilar información sobre lo que sucedió. No tienes que hacerlo por tu cuenta. Si tienes una persona de confianza, puedes pedirle que te apoye mientras sigue las instrucciones de este sitio web, o darle acceso a tus dispositivos o cuentas para recopilar la información necesaria. O puedes buscar apoyo emocional o técnico durante este proceso.

Si tú o tu equipo necesitan apoyo emocional para enfrentar una emergencia digital (o también como seguimiento), el [Programa de Salud Mental Comunitaria de Team CommUNITY (en inglés)](https://www.communityhealth.team/) ofrece servicios psicosociales en diferentes formatos para casos agudos y a largo plazo y puede brindar soporte en varios idiomas y contextos. Puedes consultar también [FemBloc: Línea feminista de atención y apoyo frente a las violencias digitales](https://fembloc.cat/es).
