---
layout: page
title: "Acerca de"
language: es
summary: "Acerca del Kit de primeros auxilios digitales DFAK."
date: 2019-03-13
permalink: about
parent: Home
---

El Kit de Primeros Auxilios Digitales es un esfuerzo colaborativo de la [Red de Respuesta Rápida (RaReNet)](https://www.rarenet.org/) y de [CiviCERT](https://www.civicert.org/).

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

La Red de Respuesta Rápida es una red internacional de personas que ofrecen respuesta rápida y organizaciones referencia en seguridad digital entre las cuales se encuentran Access Now, Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Fembloc, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos y Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtualroad, así como otras personas colaboradoras expertas que trabajan en este campo.

Algunas de estas organizaciones son parte de CiviCERT, una red internacional de líneas de atención en seguridad digital y proveedores de infraestructura que están enfocados principalmente en apoyar a grupos y personas que trabajan por la justicia social y la defensa de los derechos humanos y digitales. CiviCERT es una figura profesional para los esfuerzos distribuidos de CERTs (Equipos de Respuesta de Emergencias Computacionales o _Computer Emergency Response Team_). CiviCERT está acreditada por Trusted Introducer, la red europea de equipos de respuesta de emergencia computacional de confianza.

Queremos agradecier a [Metamorphosis Foundation](https://metamorphosis.org.mk/en/) por la [traducción albanesa](https://digitalfirstaid.org/sq/), a [EngageMedia](https://engagemedia.org/) por las traducciones [birmana](https://digitalfirstaid.org/my/), [indonesia](https://digitalfirstaid.org/id/), y [tailandesa](https://digitalfirstaid.org/th/), y a [Media Diversity Institute](https://mdi.am/en/home) por la [traducción armenia](https://digitalfirstaid.org/hy/) de este Kit de Primeros Auxilios Digitales.

El Kit de Primeros Auxilios Digitales también es un [proyecto de código abierto que acepta contribuciones externas](https://gitlab.com/rarenet/dfak_2020).

Si deseas utilizar el Kit de Primeros Auxilios Digitales en contextos donde la conectividad a internet es limitada o difícil, puedes descargar [la versión sin conexión](https://www.digitalfirstaid.org/dfak-offline.zip).

Para cualquier comentario, sugerencia o pregunta sobre el Kit de Primeros Auxilios Digitales, puedes escribirnos a: dfak @ digitaldefenders . org

Fingerprint PGP: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
