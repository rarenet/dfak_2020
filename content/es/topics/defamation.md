---
layout: topic
title: "Me están difamando"
author: Inés Binder, Florencia Goldsman, Erika Smith, Gus Andrews
language: es
summary: "Qué hacer cuando alguien está intentando dañar tu reputación en línea."
date: 2023-04
permalink: topics/defamation
parent: Home
order: 9
---

# Estoy siendo objetivo de una campaña de difamación

Cuando alguien crea y difunde deliberadamente información falsa, manipulada o engañosa para dañar la reputación de alguien, eso es una campaña de difamación. Estas campañas pueden estar dirigidas a personas individuales u organizaciones con algún tipo de exposición pública. Pueden ser orquestados por cualquier actor, gubernamental o privado.

Si bien este no es un fenómeno nuevo, el entorno digital difunde estos mensajes rápidamente, aumentando la escala y la velocidad del ataque y profundizando su impacto.

Existe una dimensión de género en las campañas de difamación cuando el objetivo es excluir a las mujeres y a las personas LGBTQIA+ de la vida pública, difundiendo información que "explota las desigualdades de género, promueve la heteronormatividad y profundiza las divisiones sociales" [[1]](#note-1).

Esta sección del Kit de Primeros Auxilios Digitales te guiará a través de algunos pasos básicos para planificar cómo responder a una campaña de difamación. Sigue este cuestionario para identificar la naturaleza de tu problema y encontrar posibles soluciones.

<a name="note-1"></a>
[[1] Contrarrestando la desinformación](https://counteringdisinformation.org/es/topics/gender/0-descripcion-general-genero-y-desinformacion)

## Workflow

### physical-wellbeing

#### ¿Temes por tu seguridad o bienestar físico?

##### Options

- [Sí](#physical-risk_end)
- [No](#no-physical-risk)

### no-physical-risk

#### No, no temo por mi seguridad física o mi bienestar

> Una campaña de difamación tiene como objetivo atacar tu reputación cuestionándola, creando dudas, enmarcándote como objetivo, alegando falsedades o exponiendo contradicciones para erosionar la confianza pública.
>
> Estos ataques afectan especialmente a personas con exposición pública cuyo trabajo depende de su prestigio y confianza: activistas y personas defensoras de derechos humanos, personas en política, periodistas y personas que ponen datos a disposición del público, artistas, etc.

¿Este ataque está tratando de socavar tu reputación?

##### Options

- [Sí](#perpetrators)
- [No](#no-reputational-damage)

### no-reputational-damage

#### No, este ataque no está buscando socavar mi reputación

> Si te enfrentas a un ataque que no está dirigido a socavar tu reputación, es posible que te enfrentes a una emergencia de otra naturaleza que no sea una campaña de difamación.

¿Quieres repasar las preguntas para diagnosticar el acoso en línea?

##### Options

- [Sí, es posible que sufra acoso en línea](/es/topics/harassed-online)
- [No, sigo pensando que es una campaña de difamación](#perpetrators)
- [Necesito ayuda para entender el problema al que me enfrento](/es/support)

### perpetrators

#### Creo que este ataque busca socavar mi reputación

¿Sabes quién está detrás de la campaña de difamación?

##### Options

- [Sí](#respond-defamation)
- [No](#analyze-messages)

### respond-defamation

#### Sí, me gustaría explorar una estrategia de respuesta

> Hay muchas formas de responder a una campaña de difamación, según el impacto y la difusión de los mensajes, los actores involucrados y sus motivaciones. Si la campaña tiene un bajo impacto y difusión, recomendamos ignorarla. Si, por el contrario, tiene un alto impacto y difusión, podrías considerar denunciar y retirar el contenido, aclarar las cosas y silenciarlo.
>
> Ten cuidado de no amplificar los mensajes difamatorios cuando intentes abordar la campaña de difamación. Citar un mensaje, incluso sólo para exponer a las personas o las motivaciones detrás de él, puede aumentar su difusión. Ten en cuenta la ["técnica del sándwich de la verdad" (en inglés)](https://en.wikipedia.org/wiki/Truth_sandwich) para encubrir información falsa sin promover involuntariamente su difusión. Esta técnica "implica presentar la verdad sobre un tema antes de cubrir la información errónea, y luego terminar una historia presentando nuevamente la verdad".
>
> Elije las estrategias que te parezcan adecuadas para contener el ataque, reconstruir la confianza y restablecer la credibilidad en tu comunidad. Recuerda que, independientemente de la estrategia que elijas, tu [autocuidado](/es/self-care/) debe ser la máxima prioridad. Considera también [documentar](/es/documentation) el contenido o los perfiles que te están atacando antes de responder.

¿Cómo quieres responder a la campaña de difamación?

##### Options

- [Quiero notificar a las plataformas de redes sociales y eliminar el contenido difamatorio](#notify-take-down)
- [Quiero ignorarlo y silenciar notificaciones](#ignore-silence)
- [Quiero dejar las cosas claras](#set-record-straight)
- [Quiero presentar una denuncia legal](#legal-queja)
- [Necesito analizar más los mensajes de difamación para poder tomar una decisión](#analyze-messages)

### analyze-messages

#### Necesito analizar más los mensajes de difamación para poder tomar una decisión

> Cuando tú o tu organización sois el objetivo de una campaña de difamación, comprender los métodos estándar utilizados para difundir desinformación puede ayudarte a sopesar los riesgos y orientar tus próximos pasos. Analizar los mensajes que recibes durante este tipo de ataque podría brindarte más información sobre los perpetradores, la motivación y los recursos empleados para socavar tu reputación. Las técnicas de ataque involucran una variedad de herramientas y coordinación en línea.
>
> Es posible que desees evaluar primero el nivel de riesgo al que te enfrentas. En el [Kit de Herramientas para la Interacción en Desinformación (en inglés)](https://www.interaction.org/documents/disinformation-toolkit/), encontrarás una Herramienta de Evaluación de Riesgos para ayudarte a valorar la vulnerabilidad de tu entorno en los medios.
>
> Una campaña de difamación en las redes sociales con frecuencia empleará hashtags para generar mayor interés, pero esto también es útil para monitorear el ataque, ya que puedes buscar por hashtag y evaluar a los perpetradores y los mensajes. Para medir el impacto de los hashtags en las redes sociales, puedes probar estas herramientas:
>
> - [Track my hashtag/Rastrea mi hashtag (en inglés)](https://www.trackmyhashtag.com/)
> - [Brand mentions/Menciones de marca (en inglés)](https://brandmentions.com/hashtag-tracker) agrega menciones en hashtags de Twitter, Instagram y Facebook en un solo flujo de datos.
> - [InVid Project (en inglés)](https://www.invid-project.eu/) es una plataforma de verificación de conocimiento para detectar historias emergentes y evaluar la confiabilidad de los archivos de video de interés periodístico y el contenido difundido a través de las redes sociales.
> - [Metadata2go (en inglés)](https://www.metadata2go.com/view-metadata) descubre metadatos detrás de los archivos que estás analizando.
> - [YouTube DataViewer (en inglés)](https://citizenevidence.amnestyusa.org/) se puede utilizar para realizar una búsqueda inversa de videos de Amnistía Internacional para ver si el video es más antiguo y si se ha modificado.
>
> **Ataques manuales vs. automatizados**
>
> Los actores estatales y otros adversarios emplean con frecuencia botnets coordinados que no son tan costosos ni técnicamente difíciles de configurar. Reconocer que un aluvión de mensajes de ataque no proviene de docenas o cientos de personas, sino de bots automatizados, puede reducir la ansiedad y facilitar la documentación, y te ayudará a decidir cómo quieres defenderte. Utiliza [Botsentinel (en inglés)](https://botsentinel.com/) para verificar la naturaleza de las cuentas que te atacan. Algunos mensajes de bots también pueden ser recogidos y difundidos por individuos, por lo que es posible que la puntuación verifique que algunas cuentas transmiten mensajes de desinformación, es decir, tienen seguidores, una imagen de perfil y contenido únicos, y otros indicadores que significan que hay menos probabilidad de que sean una cuenta bot. Otra herramienta que puedes usar es [Pegabot](https://es.pegabot.com.br/).
>
> **Información interna vs información pública**
>
> La desinformación especializada puede basar el contenido en un núcleo de verdad o hacer que el contenido parezca veraz. Reflexionar sobre el tipo de información que se comparte y sus fuentes puede determinar diferentes caminos de acción.
>
> ¿La información es privada o de fuentes internas? Considera cambiar tus contraseñas e instalar autentificación de dos factores para asegurarte de que nadie más accede a tus cuentas excepto tú. Si se hace referencia a esta información en chats o en conversaciones o archivos compartidos, anima a tus amigues y colegas a revisar su configuración de seguridad. Considera limpiar y cerrar chats antiguos para que la información del pasado no sea fácilmente accesible en tu dispositivo o en el de tus colegas y amigues.
>
> Los adversarios pueden secuestrar las cuentas de una persona u organización para difundir [información errónea (en inglés)](https://en.wikipedia.org/wiki/Misinformation) y [desinformación](https://es.wikipedia.org/wiki/Desinformaci%C3%B3n) de esa cuenta, dando a la información que se comparte mucha más legitimidad. Para recuperar el control de tus cuentas, consulta la sección del Kit de Primeros Auxilios Digitales [No puedo acceder a mi cuenta](/es/topics/account-access-issues#what-type-of-account-or-service/).
>
> ¿La información que se difunde proviene de información pública disponible en línea? Puede ser útil seguir la sección del Kit de Primeros Auxilios Digitales [Me han engañado o alguien está compartiendo imágenes mías no consentidas](/es/topics/doxing) para buscar y delimitar dónde estuvo disponible esta información y lo que se podría hacer.
> <br />
>
> **Creación de material más sofisticado** (videos de YouTube, _deepfakes_, etc.)
>
> A veces, los ataques son más específicos y los perpetradores han invertido tiempo y esfuerzo en crear imágenes y noticias falsas en videos de YouTube o [deepfakes](https://es.wikipedia.org/wiki/Deepfake) para respaldar su historia falsa. La difusión de dicho material puede revelar a los perpetradores y dar más pie para la denuncia legal y pública del ataque y motivos claros para las solicitudes de eliminación. También puede significar que los ataques son altamente personalizados y aumentan los sentimientos de vulnerabilidad y riesgo. Como ante cualquier ataque, el [autocuidado](/es/self-care/) y la seguridad personal son las principales prioridades.
>
> Otro ejemplo es el uso de dominios falsos en los que un adversario crea un sitio web o un perfil de red social que se parece a la cuenta auténtica. Si se enfrenta a un problema de este tipo, consulte la sección Kit de primeros auxilios digital en [suplantación](/es/topics/impersonated/).
>
> Este tipo de ataque también puede indicar que existe una mayor organización, logística, financiación y tiempo dedicado por parte de quien ataca, lo que permite conocer mejor la motivación del ataque y los posibles perpetradores.

¿Ahora tienes una mejor idea de quién te está atacando y cómo lo está haciendo?

##### Options

- [Sí, ahora me gustaría considerar una estrategia para responder](#respond-defamation)
- [No, necesito ayuda para saber cómo responder](#defamation_end)

### notify-take-down

#### Quiero notificar a las plataformas de redes sociales y eliminar el contenido difamatorio

> **_Nota:_** _Considere [documentar ataques](/es/topics/documentation) antes de solicitar que una plataforma elimine contenido difamatorio. Si está considerando emprender acciones legales, debe consultar la información en [documentación legal](/es/documentation#legal)._
>
> Los términos de servicio de las diferentes plataformas explicarán cuándo las solicitudes de eliminación se considerarán legítimas, por ejemplo, contenido que viola de los derechos de autor, para lo que debe ser de tu propiedad. Esta propiedad ocurre por defecto desde el momento en que creas una obra original: tomar una foto, escribir un texto, componer una canción, filmar un video, etc. No es suficiente que aparezcas en la obra; debes haberla creado.

¿Tienes la propiedad de los derechos del contenido utilizado en la campaña de difamación?

##### Options

- [Sí](#copyright)
- [No](#report-defamatory-content)

### set-record-straight

#### Quiero dejar las cosas claras

> Tu versión de los hechos siempre es importante. Dejar las cosas claras puede ser más urgente si la campaña de difamación en tu contra está muy extendida y es muy dañina para tu reputación. Sin embargo, en estos casos, debes evaluar el impacto de la campaña en ti para valorar si una respuesta pública es tu mejor estrategia.
>
> Además de denunciar el contenido y los malos actores a las plataformas, es posible que desees crear una contranarrativa, desacreditar la desinformación, denunciar públicamente a los perpetradores o simplemente resaltar la verdad. La forma en que respondas también dependerá de la comunidad de apoyo que tengas para ayudarte y de tu propia evaluación del riesgo que podrías enfrentar al tomar una postura pública.

¿Tienes un equipo de personas que te apoyen?

##### Options

- [Sí](#campaign-team)
- [No](#self-campaign)

### legal-complaint

#### Quiero presentar una denuncia legal

> Al enfrentarse a un proceso judicial, reunir, organizar y presentar pruebas ante el Tribunal, debes seguir un protocolo específico para que puedan ser aceptadas por el juez. Al presentar pruebas en línea de un ataque, tomar una captura de pantalla no es suficiente. Consulta la sección del kit de primeros auxilios digital sobre [cómo documentar un ataque para presentarlo en un proceso legal](/es/documentation#legal) para obtener más información sobre este proceso antes de comunicarte con un abogado de tu elección.

¿Qué te gustaría hacer?

##### Options

- [Estas recomendaciones fueron útiles, ya sé qué hacer](#resolved_end)
- [Estas recomendaciones fueron útiles, pero me gustaría considerar otra estrategia](#respond-defamation)
- [Necesito apoyo para proceder con mi denuncia legal](#legal_end)

### ignore-silence

#### ¿Quieres silenciar el contenido difamatorio?

> Hay momentos, especialmente en los casos en que la campaña de difamación tiene poco impacto o difusión, en los que puedes preferir ignorar a los perpetradores y simplemente silenciar los mensajes. Esto también puede ser un paso importante para el cuidado personal, y se pueden reclutar personas amigas o aliadas para monitorear el contenido y alertarte si se necesitan otras acciones. Esto no elimina el contenido, en caso de que desees documentar o revisar más adelante la información que se difunde para perjudicarte.

¿Dónde se difunde el contenido difamatorio que te gustaría silenciar?

##### Options

- [Correo electrónico](#silence-email)
- [Facebook](#silence-facebook)
- [Instagram](#silence-instagram)
- [Reddit](#silence-reddit)
- [TikTok](#silence-tiktok)
- [Twitter](#silence-twitter)
- [Whatsapp](#silence-whatsapp)
- [YouTube](#silence-youtube)

### copyright

#### Sí, poseo los derechos del contenido

> Puedes recurrir a las regulaciones de derechos de autor, como la [Ley de derechos de autor del milenio digital (DMCA)](https://es.wikipedia.org/wiki/Digital_Millennium_Copyright_Act), para eliminar contenido. La mayoría de las plataformas de redes sociales proporcionan formularios para denunciar infracciones de derechos de autor y pueden responder mejor a este tipo de solicitudes que a otras consideraciones de eliminación, debido a las implicaciones legales que tienen para ellas.
>
> **_Nota:_** _Siempre [documentar](/es/documentation) antes de solicitar eliminar contenido. Si estás considerando emprender acciones legales, debes consultar la información en [documentación legal](/es/documentation#legal)._
>
> Puedes usar estos enlaces para enviar una solicitud de eliminación por violación de derechos de autor a las principales plataformas de redes sociales:
>
> - [Archive.org (en inglés)](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [Facebook](https://www.facebook.com/help/1020633957973118/?locale=es_LA&helpref=hc_fnav&cms_id=1020633957973118)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://support.tiktok.com/es/safety-hc/account-and-user-safety/copyright)
> - [Twitter](https://help.twitter.com/es/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622?hl=es&sjid=15505274911260004472-EU)
>
> Ten en cuenta que puede llevar algún tiempo recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a este flujo de trabajo en unos días.

¿Se ha eliminado el contenido?

##### Options

- [Sí](#resolved_end)
- [No, necesito apoyo legal](#legal_end)
- [No, necesito ayuda contactando con la plataforma](#harassment_end)

### report-defamatory-content

#### No, no poseo los derechos del contenido

> No todas las plataformas cuentan con procesos especializados para reportar contenido difamatorio y algunas sí requieren un proceso legal previo. Puedes revisar la ruta que establece cada plataforma para denunciar contenido difamatorio a continuación.
>
> - [Archive.org (en inglés)](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [Facebook](https://es-es.facebook.com/help/contact/430253071144967)
> - [Instagram](https://help.instagram.com/contact/653100351788502)
> - [TikTok](https://www.tiktok.com/legal/report/feedback)
> - [Twitch](https://help.twitch.tv/s/article/how-to-file-a-user-report?language=es)
> - [Twitter](https://help.twitter.com/es/rules-and-policies/twitter-report-violation#directly)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885?helpref=search&cms_platform=android&locale=es_LA)
> - [YouTube](https://support.google.com/youtube/answer/6154230)
>
> Ten en cuenta que puede llevar algún tiempo recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a este flujo de trabajo en unos días.

¿El contenido ha sido informado y analizado por el proveedor para su eliminación?

##### Options

- [Sí](#resolved_end)
- [No, necesito apoyo legal](#legal_end)
- [No, necesito ayuda contactando con la plataforma](#harassment_end)

### silence-email

#### Correo electrónico

> La mayoría de los clientes de correo electrónico ofrecen la opción de filtrar mensajes y etiquetarlos, archivarlos o eliminarlos automáticamente para que no aparezcan en tu bandeja de entrada. Por lo general, puedes crear reglas para filtrar mensajes por remitente, destinatario, fecha, tamaño, archivo adjunto, asunto o palabras clave.
>
> Aprende a crear filtros en webmails:
>
> - [Gmail](https://support.google.com/mail/answer/6579?hl=es#zippy=%2Ccreate-a-filter)
> - [Protonmail](https://proton.me/es-es/support/email-inbox-filters)
> - [Yahoo (en inglés)](https://help.yahoo.com/kb/SLN28071.html)
>
> Aprende a crear filtros en clientes de correo electrónico:
>
> - [MacOS Mail](https://support.apple.com/es-es/guide/mail/mlhl1f6cf15a/mac)
> - [Microsoft Outlook](https://support.microsoft.com/es-es/office/configurar-reglas-en-outlook-75ab719a-2ce8-49a7-a214-6d62b67cbd41)
> - [Thunderbird](https://support.mozilla.org/es/kb/organizar-tus-mensajes-utilizando-filtros)

¿Quieres silenciar el contenido difamatorio en otra plataforma?

##### Options

- [Sí](#ignore-silence)
- [No](#damage-control)

### silence-twitter

#### Twitter

> Si no deseas la exposición a mensajes específicos, puedes bloquear personas usuarias o silenciarlas a ellas y sus mensajes. Recuerda que si eliges bloquear a alguien o algún contenido, no tendrás acceso al contenido difamatorio para [documentar el ataque](/es/documentación). En ese caso, es posible que prefieras tener acceso a él a través de otra cuenta o simplemente silenciarlo.
>
> - [Silenciar cuentas para que no aparezcan en tu biografía](https://help.twitter.com/es/using-twitter/twitter-mute)
> - [Silenciar palabras, frases, nombres de usuarie, emojis, hashtags y notificaciones para una conversación](https://help.twitter.com/es/using-twitter/advanced-twitter-mute-options)
> - [Posponer notificaciones para mensajes directos para siempre](https://help.twitter.com/es/using-twitter/direct-messages#snooze)

¿Quieres silenciar el contenido difamatorio en otra plataforma?

##### Options

- [Sí](#ignore-silence)
- [No](#damage-control)

### silence-facebook

#### Facebook

> Aunque Facebook no te permite silenciar personas usuarias o contenido, sí te permite bloquear perfiles y páginas de personas usuarias.
>
> - [Bloquear un perfil de Facebook](https://www.facebook.com/help/168009843260943/?helpref=search&query=bloquear%20perf%C3%ADl%20facebook&search_session_id=71907e89f23e6775b90f6dc6c845fec0&sr=3)
> - [Bloquear mensajes de un perfil en Facebook](https://www.facebook.com/help/1682395428676916?cms_id=1682395428676916)
> - [Bloquear una página de Facebook](https://www.facebook.com/help/395837230605798)
> - [Prohibir o bloquear perfiles de su página de Facebook](https://www.facebook.com/help/185897171460026/)

¿Quieres silenciar el contenido difamatorio en otra plataforma?

##### Options

- [Sí](#ignore-silence)
- [No](#damage-control)

### silence-instagram

#### Instagram

> Aquí hay una lista de consejos y herramientas para silenciar a les usuaries y las conversaciones en Instagram:
>
> - [Silenciar o dejar de silenciar a alguien en Instagram](https://help.instagram.com/469042960409432)
> - [Silenciar o reactivar la historia de Instagram de alguien](https://help.instagram.com/290238234687437)
> - [Restringir o no restringir a alguien](https://help.instagram.com/2638385956221960)
> - [Desactivar sugerencias de cuentas para tu perfil de Instagram](https://help.instagram.com/530450580417848)
> - [Ocultar publicaciones sugeridas en tu feed de Instagram](https://help.instagram.com/423267105807548)

¿Quieres silenciar el contenido difamatorio en otra plataforma?

##### Options

- [Sí](#ignore-silence)
- [No](#damage-control)

### silence-youtube

#### Youtube

> Si accedes a YouTube desde tu navegador de escritorio, puedes eliminar vídeos, canales, secciones y listas de reproducción desde tu página de inicio.
>
> - [Gestiona tus recomendaciones de YouTube](https://support.google.com/youtube/answer/6342839)
>
> También puedes crear una lista de bloqueo en tu perfil de YouTube para bloquear a otras personas en tu chat en vivo de YouTube.
>
> - YouTube: [myaccount.google.com/blocklist](https://myaccount.google.com/blocklist)
> - [Bloquear a otras personas usuarias en un chat en directo de YouTube](https://support.google.com/youtube/answer/7663906)

¿Quieres silenciar el contenido difamatorio en otra plataforma?

##### Options

- [Sí](#ignore-silence)
- [No](#damage-control)

### silence-whatsapp

#### WhatsApp

> WhatsApp es una plataforma común para campañas de difamación. Los mensajes se ejecutan rápido debido a las relaciones de confianza entre personas remitentes y destinatarias.
>
> - [Cómo archivar o desarchivar chats individuales o grupales](https://faq.whatsapp.com/1426887324388733/?locale=es_es&cms_platform=web&cms_id=1426887324388733&draft=false)
> - [Cómo salir de un grupo y eliminarlo](https://faq.whatsapp.com/498814665492149/?locale=es_es&cms_platform=web&cms_id=498814665492149&draft=false)
> - [Cómo bloquear y reportar contactos](https://faq.whatsapp.com/1142481766359885/?locale=es_LA&cms_platform=android&cms_id=1142481766359885&draft=false)
> - [Cómo silenciar o desactivar el silencio de las notificaciones de grupo](https://faq.whatsapp.com/694350718331007/?locale=es_LA&cms_platform=web&cms_id=694350718331007&draft=false)

¿Quieres silenciar el contenido difamatorio en otra plataforma?

##### Options

- [Sí](#ignore-silence)
- [No](#damage-control)

### silence-tiktok

#### TikTok

> TikTok te permite bloquear personas usuarias, evitar que éstas comenten tus videos, crear filtros para comentarios en tus videos y eliminar, silenciar y filtrar mensajes directos.
>
> - [Bloquear a personas usuarias en TikTok](https://support.tiktok.com/es/using-tiktok/followers-and-following/blocking-the-users)
> - [Elige quién puede comentar tus videos en tu configuración](https://support.tiktok.com/es/using-tiktok/messaging-and-notifications/comments)
> - [Cómo activar los filtros de comentarios para tus videos de TikTok](https://support.tiktok.com/es/using-tiktok/messaging-and-notifications/comments#3)
> - [Gestionar la configuración de privacidad de los comentarios para todos tus videos de Tik Tok](https://support.tiktok.com/es/using-tiktok/messaging-and-notifications/comments#4)
> - [Gestionar la configuración de privacidad de los comentarios para uno de tus videos de TikTok](https://support.tiktok.com/es/using-tiktok/messaging-and-notifications/comments#5)
> - [Cómo eliminar, silenciar y filtrar mensajes directos](https://support.tiktok.com/es/account-and-privacy/account-privacy-settings/direct-message#6)
> - [Cómo gestionar quién puede enviarte mensajes directos](https://support.tiktok.com/es/account-and-privacy/account-privacy-settings/direct-message#7)

¿Quieres silenciar el contenido difamatorio en otra plataforma?

##### Options

- [Sí](#ignore-silence)
- [No](#damage-control)

### silence-reddit

#### Reddit

> Si la difamación circula en Reddit, puedes optar por excluir a las comunidades en las que esto sucede para que no aparezcan en tus notificaciones, noticias de Inicio (incluidas las recomendaciones) y noticias Populares en tu sitio de escritorio de Reddit o reddit.com. Si tienes el rol de moderar, también puedes prohibir o silenciar a las personas usuarias que infringen repetidamente las reglas de su comunidad.
>
> - [Silenciamiento de una comunidad de Reddit](https://support.reddithelp.com/hc/es-es/articles/9810475384084-What-is-community-muting-)
> - [Prohibición y silenciamiento de miembros de la comunidad](https://mods.reddithelp.com/hc/es-es/articles/360009161872-User-Management-banning-and-muting)

¿Quieres silenciar el contenido difamatorio en otra plataforma?

##### Options

- [Sí](#ignore-silence)
- [No](#damage-control)

### campaign-team

#### Tengo el apoyo de un equipo

> Si la campaña en tu contra está invirtiendo tiempo y recursos, esto sugiere que los perpetradores tienen una motivación profunda para atacarte. Es posible que debas usar todas las estrategias mencionadas en este flujo de trabajo para mitigar el daño y crear una campaña pública en tu defensa. Las personas amigas y aliadas pueden ser beneficiosas y centrar tu cuidado personal, si lo apoyan, con análisis de mensajes, documentación de ataques e informes de contenido y perfiles.
>
> Un equipo de campaña puede trabajar en conjunto para decidir la mejor manera de cuestionar la narrativa que se usa en tu contra. Los mensajes de desinformación son engañosos y, a menudo, nos dicen que no repitamos los mensajes de los atacantes. Es importante encontrar un equilibrio entre elevar tu historia y refutar afirmaciones falsas.
>
> Es vital hacer un [análisis de evaluación de riesgos (en inglés)](https://www.interaction.org/documents/disinformation-toolkit/) para usted y cualquier persona u organización de apoyo antes de embarcarse en una campaña pública.
>
> Además de analizar los mensajes de ataque, es posible que tú y tu equipo deseéis comenzar [mapeando organizaciones y alianzas (en inglés)](https://holistic-security.tacticaltech.org/exercises/explore/visual-actor-mapping-part-1.html) que reconocen tus antecedentes y se sumarán a una narrativa positiva sobre tu trabajo, o harán suficiente ruido alegre para desviar o ahogar una campaña de desprestigio.
>
> Es posible que tú y tu organización no seáis el único foco de ataque. Considera generar confianza con otras organizaciones y comunidades que han sido discriminadas o atacadas por la campaña de desinformación. Esto podría significar preparar a los miembros de la comunidad para abordar mensajes controvertidos o compartir recursos para que otros puedan defenderse.
>
> Tómate el tiempo en tu equipo para identificar qué quieren provocar sus mensajes y tipos de contenido, también en función de si está refutando la campaña o elevando los mensajes positivos sin hacer referencia a las tácticas de difamación. Es posible que desees que las personas sean más hábiles para diferenciar los rumores de los hechos. Los mensajes simples basados ​​en valores son particularmente efectivos en cualquier campaña pública.
>
> Además de emitir declaraciones públicas o de prensa, es posible que desees seleccionar y preparar diferentes portavoces para tu campaña, lo que también ayuda a desviar los ataques personalizados comunes en las campañas de difamación, especialmente en casos de desinformación de género.
>
> Establecer un sistema de monitoreo de medios para rastrear tu campaña de mensajes y la de sus atacantes te ayudará a ver qué mensajes dan mejor en el blanco para afinar su campaña.
>
> Si el contenido difamatorio también se publica en un periódico o revista, es posible que su equipo quiera exigir [Derecho de respuesta](#self-strategies) activar su caso con [sitios de verificación de hechos](#self-strategies).

¿Cree que ha podido controlar el daño a su reputación?

##### Options

- [Sí](#resolved_end)
- [No](#defamation_end)

### self-campaign

#### No, no tengo el apoyo de un equipo

> Si no cuentas con el apoyo de un equipo, debes tener en cuenta cualquier limitación de capacidad o recursos que tengas al planificar tus próximos pasos, priorizando tu [autocuidado](/es/self-care). Es posible que desees publicar una declaración en tu perfil de redes sociales o sitio web. Asegúrate de tener [control total](/es/topics/account-access-issues) de esas cuentas antes de hacerlo. En tu declaración, es posible que desees referirte a tu trayectoria establecida y fuentes confiables que reflejen esa trayectoria. Tu [análisis de la campaña de difamación](#analyze-messages) te dará una idea del tipo de declaración que quieres hacer. Por ejemplo, puede revisar si se hace referencia a alguna fuente de noticias en el ataque.

¿El contenido difamatorio también se publica en un periódico o revista?

##### Options

- [Sí](#self-strategies)
- [No](#damage-control)

### self-strategies

#### Sí, el contenido difamatorio está publicado en otros lugares

> Si se publica información difamatoria, la política editorial o incluso la ley de un país en particular puede otorgarle el [derecho de respuesta o derecho de corrección (en inglés)](https://en.wikipedia.org/wiki/Right_of_reply). Esto no sólo te brinda otra plataforma para publicar tu declaración, más allá de tus propios medios, sino que también proporciona un lugar para vincular a otras declaraciones que circulen.
>
> Si los medios de comunicación que contienen información difamatoria no son una fuente de noticias creíble o con frecuencia son una fuente de noticias sin fundamento, puede ser útil señalarlo en cualquier declaración.
>
> Hay muchos verificadores de noticias locales, regionales y globales que pueden ayudarte a desacreditar la información que circula sobre ti.
>
> - [DW Fact Check (en inglés)](https://www.dw.com/en/fact-check/t-56584214)
> - [France24 LesObservateurs (en inglés)](https://observers.france24.com/en/)
> - [AFP FactCheck (en inglés)](https://factcheck.afp.com/)
> - [EUvsDisinfo](https://euvsdisinfo.eu/es/)
> - [Latam Chequea](https://chequeado.com/latamchequea)
> - [Europa fact check (en inglés)](https://eufactcheck.eu/)

¿Sientes que necesitas una mayor respuesta a la campaña de difamación?

##### Options

- [Sí](#campaign-team)
- [No](#damage-control)

### damage-control

#### No, me gustaría hacer un poco de control de daños

> Tu análisis de la campaña de difamación te brindará información sobre el tipo de acciones que deseas realizar. Por ejemplo, si se están empleando mensajes de bot en tu contra, es posible que desees señalarlo en una declaración pública o sugerir motivos ocultos de los perpetradores detrás de la campaña si los conoces. Sin embargo, establecer tu propia trayectoria y obtener ayuda de personas amigas y aliadas para promover y elevar tu verdad puede ser tu preferencia, en lugar de interactuar y reaccionar ante contenido difamatorio.

¿Crees que ha podido controlar el daño a tu reputación?

##### Options

- [Sí](#resolved_end)
- [No](#defamation_end)

### physical-risk_end

#### Sí, temo por mi seguridad física y mi bienestar

> Si estás en riesgo físico, comunícate con las organizaciones a continuación que pueden ayudarte.

[orgs](:organisations?services=physical_security)

### defamation_end

#### Necesito más apoyo

> Si aún experimentas daños a tu reputación debido a una campaña de difamación, comunícate con las organizaciones a continuación que pueden ayudarte.

[orgs](:organisations?services=advocacy&services=individual_care&services=legal)

### harassment_end

#### Necesito más apoyo

> Si necesitas ayuda para eliminar contenido, puedes comunicarse con las organizaciones a continuación que pueden ayudarte.
> Si estás en riesgo físico, comunícate con las organizaciones a continuación que pueden ayudarte.

[orgs](:organisations?services=physical_security) Si estás en riesgo físico, comunícate con las organizaciones a continuación que pueden ayudarte.

[orgs](:organisations?services=harassment)

### legal_end

#### Necesito apoyo legal

> Si aún sufres daños a tu reputación debido a una campaña de difamación y necesita apoyo legal, comunícate con las organizaciones a continuación.

[orgs](:organisations?services=legal)

### resolved_end

#### Mi problema ha sido resuelto

Esperamos que esta guía de solución de problemas haya sido útil. Envía tus comentarios [vía correo electrónico](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

Una forma importante de controlar el daño de un plan de difamación es tener buenas prácticas de seguridad para mantener tus cuentas a salvo de actores malintencionados y tomar el pulso de la información que circula sobre ti o tu organización. Es posible que desees considerar algunos de los siguientes consejos preventivos:

- Mapea tu presencia en línea. El autodoxing consiste en explorar la inteligencia de código abierto sobre uno mismo para evitar que los actores maliciosos encuentren y usen esta información para hacerse pasar por ti.
- Configura alertas de Google. Puedes recibir correos electrónicos cuando aparecen nuevos resultados para un tema en la Búsqueda de Google. Por ejemplo, puedes obtener información sobre las menciones a tu nombre o de tu organización/nombre colectivo.
- Activa la autentificación de dos factores (2FA) para tus cuentas más importantes. 2FA ofrece una mayor seguridad de la cuenta al requerir que uses más de un método para iniciar sesión en tus cuentas. Esto significa que incluso si alguien consiguiera tu contraseña principal, no podría acceder a tu cuenta, a menos que también tuviera tu teléfono móvil u otro medio secundario de autentificación.
- Verificar tus perfiles en las plataformas de redes sociales. Algunas plataformas ofrecen una función para verificar tu identidad y vincularla a tu cuenta.
- Captura tu página web tal como aparece ahora para usarla como prueba en el futuro. Si su sitio web permite rastreadores, puede usar [Wayback Machine](https://archive.org/web), ofrecido por [Archive.org](https://arhive.org). Visita Internet Archive Wayback Machine, ingresa el nombre de tu sitio web en el campo debajo del encabezado "Guardar página ahora" y haz clic en el botón "Guardar página ahora".

### Resources

- [Unión de Científicos Preocupados: Cómo contrarrestar la desinformación: estrategias de comunicación, mejores prácticas y trampas para evitar (en inglés)](https://www.ucsusa.org/resources/how-counter-disinformation)
- [Abordar la misoginia en línea y la desinformación de género: una guía práctica, por ND (en inglés)](https://www.ndi.org/sites/default/files/Addressing%20Gender%20%26%20Disinformation%202%20%281%29.pdf)
- [Manual de verificación para desinformación y manipulación de medios (en inglés)](https://datajournalism.com/read/handbook/verification-3)
- [Una guía para prebunking: una forma prometedora de inocular contra la desinformación (en inglés)](https://firstdraftnews.org/articles/a-guide-to-prebunking-a-promising-way-to-inoculate-against-misinformation/)
- [OverZero: Comunicarse en tiempos contenciosos: qué hacer y qué no hacer para elevarse por encima del ruido (en inglés)](https://overzero.ghost.io/communicating-during-contentious-times-dos-and-donts-to-rise-above-the-noise/)
- [Mapeo de actores visuales (en inglés)](https://holistic-security.tacticaltech.org/exercises/explore/visual-actor-mapping-part-1.html)
- [Conjunto de herramientas de desinformación (en inglés)](https://www.interaction.org/documents/disinformation-toolkit/)
- [Libro de ejercicios para desactivar el odio (en inglés)](https://www.ushmm.org/m/pdfs/20160229-Defusing-Hate-Workbook-3.pdf)
- [Fanzines Dra. Odio contra la desinformación y los discursos de odio, por Fábrica Memética](https://memetic.media/fabrica/proyecto/herramientas-para-desarmar-el-odio/)
