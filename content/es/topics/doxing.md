---
layout: topic
title: "Alguien está compartiendo mi información personal"
author: Ahmad Gharbeia, Michael Carbone, Gus Andrews, Flo Pagano, Constanza Figueroa
language: es
summary: "Se está distribuyendo información privada o media sobre mí que muestran mi semejanza. Es angustioso para mí o podría ser potencialmente dañino."
date: 2023-05
permalink: topics/doxing
parent: Home
order: 10
---

# Me han doxeado. Alguien está compartiendo mi información o media privadas sin mi consentimiento

Las personas que acosan a veces pueden publicar información que te identifica personalmente o expone aspectos de tu vida personal que has elegido mantener en privado. Esto a veces se denomina _"doxxing"_ o _"doxing"_ (un término abreviado para "publicar documentos").

El objetivo del _doxing_ es avergonzar al objetivo o agitar contra él. Esto puede tener graves efectos negativos en tu bienestar psicosocial, seguridad personal, relaciones y trabajo.

El _doxing_ se utiliza para intimidar a figuras públicas, periodistas, personas defensoras de los derechos humanos y activistas, pero también se dirige con frecuencia a personas pertenecientes a minorías sexuales o poblaciones marginadas. Está estrechamente relacionado con otras formas de violencia en línea y de género.

La persona que te engaña puede haber obtenido tus documentos de varias fuentes. Esto podría incluir entrar en tus cuentas y dispositivos privados. Pero también es posible que estén exponiendo y sacando de contexto información o medios que pensabas que estabas compartiendo en privado con personas amigas, socias, conocidas o con las que trabajas. En algunos casos, quien acosa puede agregar información disponible públicamente sobre ti en las redes sociales o registros públicos. Reunir esta información en formas nuevas y perjudicialmente malinterpretadas también podría constituir _doxing_.

Si te han engañado, puedes usar el siguiente cuestionario para identificar dónde la persona acosadora ha encontrado su información y encontrar formas de limitar el daño, incluida la eliminación de los documentos de los sitios web.

## Workflow

### be_supported

#### ¿Sientes que tienes a alguien de confianza que te puede ayudar en caso de que lo necesites?

> La documentación es fundamental para evaluar los impactos, identificar el origen de estos ataques y desarrollar respuestas que restablezcan su seguridad. Dependiendo de lo que haya sucedido, pasar por el proceso de documentación podría aumentar los impactos emocionales que esta agresión tiene sobre ti. Para disminuir la carga de este trabajo y reducir el estrés emocional potencial en ti, es posible que necesites el apoyo de una persona cercana o de confianza mientras documentas estos ataques.

¿Sientes que tienes a alguien de confianza que te puede ayudar en caso de que lo necesites?

##### Options

- [Sí, una persona de confianza está lista para ayudarme](#physical_risk)

### physical_risk

#### Sí, una persona de confianza está lista para ayudarme

> Reflexiona: ¿Quién crees que puede ser responsable de estos ataques? ¿Cuáles son sus capacidades? ¿Qué tan decididos están a hacerte daño? ¿Qué otras partes pueden estar motivadas para hacerte daño? Estas preguntas te ayudarán a evaluar las amenazas que están en curso y valorar si tienes exposición a riesgos físicos.

¿Temes por tu integridad física o bienestar o que puedas estar en riesgo legal?

##### Options

- [Sí, creo que tengo exposición a riesgos físicos](#yes_physical_risk)
- [Pienso que puedo estar en riesgo legal](#legal_risk)
- [No, me gustaría solucionar mi problema en el ámbito digital](#takedown_content)
- [No tengo seguridad. Necesito ayuda para evaluar mis riesgos.](#assessment_end)

### yes_physical_risk

#### Sí, creo que tengo exposición a riesgos físicos

> Si crees que podrías ser blanco de ataques en persona o que tu integridad física o tu bienestar están en riesgo, puedes consultar los siguientes recursos para pensar en tus necesidades inmediatas de seguridad física.
>
> - [Front Line Defenders - Manual Sobre Seguridad´](https://www.frontlinedefenders.org/es/manual-sobre-seguridad) (multiple idiomas)
> - [National Domestic Violence Hotline - Create a Safety Plan (en inglés)](https://www.thehotline.org/plan-for-safety/create-a-safety-plan/)
> - [Coalition against Online Violence - Physical Security Support (en inglés)](https://onlineviolenceresponsehub.org/physical-security-support)
> - [Cheshire Resilience Forum - How to Prepare for an Emergency (en inglés)](https://cheshireresilience.org.uk/how-to-prepare-for-an-emergency/)
> - [FEMA Form P-1094 Create Your Family Emergency Communication Plan (en inglés)](https://www.templateroller.com/group/12262/create-your-family-emergency-communication-plan.html)
> - [Reolink - How to Cleverly Secure Your Home Windows — Top 9 Easiest Security Solutions (en inglés)](https://reolink.com/blog/top-7-easy-diy-ways-to-secure-your-home-windows/)
> - [Reolink - How to Make Your Home Safe from Break-ins (en inglés)](https://reolink.com/blog/make-home-safe-from-break-ins/)
> - [Seguridad integral para periodistas - Seguridad Física](https://seguridadintegral.articulo19.org/categorias-prevencion/seguridad-fisica/)

¿Necesitas más ayuda para asegurar tu integridad física y bienestar?

##### Options

- [Sí](#physical-risk_end)
- [No, pero creo que también estoy en riesgo legal](#legal_risk)
- [No, me gustaría solucionar mi problema en el ámbito digital](#takedown_content)
- [No tengo seguridad, necesito ayuda para evaluar mis riesgos](#assessment_end)

### legal_risk

#### Pienso que puedo estar en riesgo legal

> Si estás considerando emprender acciones legales, será muy importante conservar pruebas de los ataques de los que fuiste objeto. Por lo tanto, se recomienda encarecidamente seguir las [recomendaciones de la página del Kit de Primeros Auxilios Digitales sobre el registro de información sobre ataques](/es/documentation).

¿Necesitas apoyo legal inmediato?

##### Options

- [Sí](#legal_end)
- [No, me gustaría solucionar mi problema en el ámbito digital](#takedown_content)
- [No](#resolved_end)

### takedown_content

#### Me gustaría solucionar mi problema en el ámbito digital

¿Quieres trabajar para que los documentos compartidos de manera inapropiada se eliminen de la vista del público?

##### Options

- [Sí](#documenting)
- [No](#resolved_end)

### documenting

#### Retirar documentos de la vista del público

> Es muy importante que conserves tu propia documentación del ataque al que has sido sometido. Hace que sea más fácil para quien te ayude legal o técnicamente cuando te brinde apoyo. Consulta la [guía del Kit de Primeros Auxilios Digitales sobre la documentación de ataques en línea](/es/documentation) para obtener información sobre cómo recopilar mejor la información para tus necesidades particulares.
>
> También puedes pedirle a una persona de tu confianza que te ayude a buscar pruebas del ataque del que has sido objeto. Esto puede ayudar a garantizar que no te vuelvas a traumatizar al volver a mirar los materiales publicados.

¿Crees que tienes preparación para documentar el ataque de forma segura?

##### Options

- [Sí, estoy documentando los ataques](#where_published)

### where_published

#### Estoy documentando los ataques

> Dependiendo de dónde quien ataca haya compartido públicamente su información, es posible que desee realizar diferentes acciones.
>
> Si tu información ha sido publicada en plataformas de redes sociales basadas en países con sistemas legales que definen las responsabilidades legales de las corporaciones hacia las personas que usan sus servicios (por ejemplo, la Unión Europea o los Estados Unidos), es más probable que quien modera en la corporación tendrá más disposición a ayudarte. Necesitará que [documentes](/es/documentation) las pruebas de los ataques. Es posible que puedas buscar el país donde se encuentra una empresa en Wikipedia.
>
> A veces, los adversarios pueden publicar tu información en sitios más pequeños y menos conocidos y enlazarlos desde las redes sociales. A veces, estos sitios están alojados en países donde no existe protección legal, o son administrados por individuos o grupos que practican, respaldan o simplemente no se oponen a comportamientos dañinos. En este caso, puede ser más difícil ponerse en contacto con los sitios donde se publica el material o averiguar quién los administra.
>
> Pero incluso cuando quien ataca haya publicado tus documentos en otro lugar que no sean las redes sociales populares (que suele ser el caso), trabajar para eliminar esos materiales o enlaces en ellos puede reducir en gran medida la cantidad de personas que ven tu información. También puedes reducir la cantidad y la gravedad de los ataques que puedes enfrentar.

Mi información fue publicada en...

##### Options

- [Una plataforma o servicio regulado por la ley](#what_info_published)
- [Una plataforma o servicio sin posibilidad de responder a solicitudes de eliminación](#legal_advocacy)

### what_info_published

#### Una plataforma o servicio regulado por la ley

¿Qué información se ha compartido sin consentimiento?

##### Options

- [Información personal que me identifica o es privada: dirección, número de teléfono, DNI, cuenta bancaria, etc.](#personal_info)
- [Contenido que no acepté compartir, incluido material íntimo (imágenes, videos, audio, textos)](#media)
- [Un seudónimo que utilizo se ha relacionado con mi identidad en la vida real](#linked_identities)

### linked_identities

#### Un seudónimo que utilizo se ha relacionado con mi identidad en la vida real

> Si un agresor vincula públicamente tu nombre o identidad de la vida real con un seudónimo, apodo o identificador que utilizas cuando te expresas, haces pública tu opinión, organizas o participas en activismo, considera cerrar las cuentas relacionadas con esa identidad como una manera de reducir el daño.

¿Necesitas continuar usando esta identidad/persona para lograr tus objetivos, o puedes cerrar la(s) cuenta(s)?

##### Options

- [Necesito seguir usándolo](#defamation_flow_end)
- [Puedo cerrar la(s) cuenta(s)](#close)

### close

#### Puedo cerrar la(s) cuenta(s)

> Antes de cerrar las cuentas asociadas con la identidad o persona afectada, considera los riesgos de cerrar la cuenta. Puedes perder el acceso a servicios o datos, enfrentar riesgos para tu reputación, perder contacto con tus asociados en línea, etc.

##### Options

- [He cerrado cuentas relevantes](#resolved_end)
- [He decidido no cerrar las cuentas relevantes en este momento](#resolved_end)
- [Necesito ayuda para entender lo que debo hacer](#harassment_end)

### personal_info

#### ¿Dónde se ha publicado tu información personal?

¿Dónde se ha publicado tu información personal?

##### Options

- [En una plataforma de redes sociales](#doxing-sn)
- [En un sitio web](#doxing-web)

### doxing-sn

#### En una plataforma de redes sociales

> Si tu información privada se ha publicado en una plataforma de redes sociales, puedes denunciar una violación de los estándares de la comunidad siguiendo los procedimientos de denuncia proporcionados a las personas usuarias por los sitios web de redes sociales.
>
> **_Nota:_** _Siempre [documentar](/es/documentation) antes de realizar acciones como eliminar mensajes o registros de conversaciones o bloquear perfiles. Si estás considerando emprender acciones legales, debes consultar la información en [documentación legal](/es/documentation#legal)._
>
> Encontrarás instrucciones para informar de una violación de los estándares de la comunidad a las principales plataformas en la siguiente lista:
>
> - [Google (en inglés)](https://cybercivilrights.org/google-2/)
> - [Faceboo (en inglés)k](https://www.cybercivilrights.org/facebook)
> - [Twitter (en inglés)](https://www.cybercivilrights.org/twitter)
> - [Tumblr (en inglés)](https://www.cybercivilrights.org/tumblr)
> - [Instagram (en inglés)](https://www.cybercivilrights.org/instagram)
> - [Muchas plataformas](https://docs.fembloc.cat/plataformas-doxing-outing-publicacion-informacion-personal-sin-consentimiento.html) y [especializada en sexting (en español)](https://docs.fembloc.cat/plataformas-reportar-sexpreding-difusion-imagenes-videos-intimos.html)
>
> Ten en cuenta que puede llevar algún tiempo recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a este flujo de trabajo en unos días.

¿Se ha borrado la información?

##### Options

- [Sí](#harassment_end)
- [No](#platform_help_end)

### doxing-web

#### En un sitio web

> Puedes intentar denunciar el sitio web a quien provee de alojamiento o tiene registrados los dominios y pedir que lo eliminen.
>
> **_Nota:_** _Siempre [documenta](/es/documentation) antes de realizar acciones como eliminar mensajes o registros de conversaciones o bloquear perfiles. Si estás considerando emprender acciones legales, debes consultar la información en [documentación legal](/es/documentation#legal)._
>
> Para enviar una solicitud de eliminación, también deberás recopilar información del sitio web donde se publicó tu información:
>
> - Ve al [servicio NSLookup de Network Tools (en inglés)](https://network-tools.com/nslookup/) y descubre la dirección IP (o direcciones) del sitio web falso ingresando su URL en el formulario de búsqueda.
> - Anota la dirección o direcciones IP.
> - Ve al [Servicio de búsqueda Whois de Domain Tools (en inglés)](https://whois.domaintools.com/) o [servicio en Whoisdominio (en español)](https://www.whoisdominio.com/) y busca tanto el dominio como la(s) dirección(es) IP del sitio web falso.
> - Registra el nombre y la dirección de correo electrónico de abuso del proveedor de alojamiento y el servicio de dominio. Si se incluye en los resultados de tu búsqueda, registra también el nombre de quien tiene la propiedad del sitio web.
> - Escribe a quien provee de alojamiento y también a quien registra los dominios del sitio web falso para solicitar su eliminación. En tu mensaje, incluye información sobre la dirección IP, la URL y la propiedad del sitio web suplantador, así como las razones por las que es abusivo.
>
> Ten en cuenta que puede llevar algún tiempo recibir una respuesta a tus solicitudes. Guarda esta página en sus marcadores y vuelve a este flujo de trabajo en unos días.

¿Se ha eliminado el contenido?

##### Options

- [Sí](#resolved_end)
- [No, la plataforma no respondió o ayudó](#platform_help_end)
- [Necesito ayuda para saber cómo enviar una solicitud de eliminación](#platform_help_end)

### media

#### Contenido que no consentí compartir, incluido material íntimo

> Si has creado el medio tú, por lo general serás quien tiene propiedad de los derechos de autoría del media. En algunas jurisdicciones, las personas también tienen derechos sobre los medias en los que aparecen (conocidos como derechos de publicidad).

¿Tienes los derechos de autoría de los medias?

##### Options

- [Sí](#intellectual_property)
- [No](#nude)

### intellectual_property

#### Tengo

> Puede recurrir a las regulaciones de derechos de autor, como la [Ley de derechos de autor del milenio digital (DMCA)](https://es.wikipedia.org/wiki/Digital_Millennium_Copyright_Act), para eliminar la información protegida por derechos de autoría. La mayoría de las plataformas de redes sociales proporcionan formularios para denunciar infracciones de estos derechos y pueden responder mejor a este tipo de solicitudes que a otras consideraciones de eliminación, debido a las implicaciones legales que tienen para ellas.
>
> **_Nota:_** _Siempre [documenta](/es/documentation) antes de solicitar eliminar contenido. Si estás considerando emprender acciones legales, debes consultar la información en [documentación legal](/es/documentation#legal)._
>
> Puedes usar estos enlaces para enviar una solicitud de eliminación por violación de derechos de autoría a las principales plataformas de redes sociales:
>
> - [Facebook](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://support.tiktok.com/es/safety-hc/account-and-user-safety/copyright#4)
> - [Twitter](https://help.twitter.com/es/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622?sjid=1561215955637134274-SA)
> - [Archive.org (en inglés)](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
>
> Ten en cuenta que puede llevar algún tiempo recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a este flujo de trabajo en unos días.

¿Se ha eliminado el contenido?

##### Options

- [Sí](#resolved_end)
- [No, la plataforma no respondió o ayudó](#platform_help_end)
- [No encontré las plataformas que buscaba en la lista de recursos](#platform_help_end)

### nude

#### No tengo los derechos sobre el contenido

> Los medias que muestran cuerpos desnudos o detalles de los mismos a veces están cubiertos por políticas de plataformas específicas.

¿Los medias incluyen cuerpos desnudos o detalles de cuerpos desnudos?

##### Options

- [Sí](#intimate_media)
- [No](#nonconsensual_media)

### intimate_media

#### Sí, el contenido tiene material íntimo

> Para obtener información sobre cómo denunciar una violación de las reglas de privacidad o el intercambio no consentido de medias íntimos/contenido dañino en las plataformas más populares, puedes consultar los siguientes recursos:
>
> - [Stop NCII](https://stopncii.org/?lang=es-mx) (Facebook, Instagram, TikTok y Bumble - en todo el mundo)
> - [Revenge Porn Helpline - Ayuda para víctimas fuera del Reino Unido (en inglés)](https://revengepornhelpline.org.uk/how-can-we-help/if-we-can-t-help-who-can/help-for-victims-outside-the-uk/)
> - [FemBloc - Reportar sexpreading](https://docs.fembloc.cat/plataformas-reportar-sexpreding-difusion-imagenes-videos-intimos.html)
>
> **_Nota:_** _Siempre [documenta](/es/documentation) antes de solicitar la eliminación de contenido. Si estás considerando emprender acciones legales, debes consultar la información en [documentación legal](/es/documentation#legal)._
>
> Ten en cuenta que puede llevar algún tiempo recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a este flujo de trabajo en unos días.

¿Se ha eliminado el contenido?

- [Sí](#resolved_end)
- [No, la plataforma no respondió / ayudó](#platform_help_end)
- [No encontré las plataformas que buscaba en la lista de recursos](#platform_help_end)

### nonconsensual_media

#### No, el contenido no tiene material íntimo

¿Dónde se han publicado tus medias?

##### Options

- [En una plataforma de redes sociales](#NCII-sn)
- [En un sitio web](#NCII-web)

### NCII-sn

#### En una plataforma de redes sociales

> Si tus medias se han publicado en una plataforma de redes sociales, puede denunciar una violación de los estándares de la comunidad siguiendo los procedimientos de denuncia proporcionados a las personas usuarias por los sitios web de redes sociales.
>
> **_Nota:_** _Siempre [documenta](/es/documentation) antes de realizar acciones como eliminar mensajes o registros de conversaciones o bloquear perfiles. Si estás considerando emprender acciones legales, debes consultar la información en [documentación legal](/es/documentation#legal)._
>
> Encontrarás instrucciones para informar una violación de los estándares de la comunidad a las principales plataformas en la siguiente lista:
>
> - [Google (en inglés)](https://cybercivilrights.org/google-2/)
> - [Faceboo (en inglés)k](https://www.cybercivilrights.org/facebook)
> - [Twitter (en inglés)](https://www.cybercivilrights.org/twitter)
> - [Tumblr (en inglés)](https://www.cybercivilrights.org/tumblr)
> - [Instagram (en inglés)](https://www.cybercivilrights.org/instagram)
> - [Muchas plataformas](https://docs.fembloc.cat/plataformas-doxing-outing-publicacion-informacion-personal-sin-consentimiento.html) y [especializada en sexting (en español)](https://docs.fembloc.cat/plataformas-reportar-sexpreding-difusion-imagenes-videos-intimos.html)
>
> Ten en cuenta que puede llevar algún tiempo recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a este flujo de trabajo en unos días.

¿Se ha eliminado el contenido?

##### Options

- [Sí](#resolved_end)
- [No, la plataforma no respondió o ayudó](#platform_help_end)
- [No encontré las plataformas que buscaba en la lista de recursos](#platform_help_end)

### NCII-web

#### En un sitio web

> Sigue las instrucciones en ["Without My Consent - Take Down" (en inglés)](https://withoutmyconsent.org/resources/take-down) para eliminar contenido de un sitio web.
>
> **_Nota:_** _Siempre [documenta](/es/documentation) antes de realizar acciones como eliminar mensajes o registros de conversaciones o bloquear perfiles. Si estás considerando emprender acciones legales, debes consultar la información en [documentación legal](/es/documentation#legal)._
>
> Tenga en cuenta que puede llevar algún tiempo recibir una respuesta a sus solicitudes. Guarde esta página en sus marcadores y vuelva a este flujo de trabajo en unos días.

¿Se ha eliminado el contenido?

##### Options

- [Sí](#resolved_end)
- [No, la plataforma no respondió o ayudó](#platform_help_end)
- [No encontré las plataformas que buscaba en la lista de recursos](#platform_help_end)

### legal_advocacy

#### Una plataforma o servicio sin probabilidad de responder a las solicitudes de retirada de contenidos

> En algunos casos, las plataformas donde se lleva a cabo el _doxing_ o la publicación no consentida de medias íntimos no cuentan con políticas o un proceso de moderación lo suficientemente maduro para gestionar las solicitudes relacionadas con el _doxing_, el acoso o la difamación.
>
> A veces, el material ofensivo se publica o aloja en plataformas o aplicaciones que son administradas por personas o grupos que practican, respaldan o simplemente no se oponen a comportamientos dañinos.
>
> También hay lugares en el ciberespacio donde las leyes y reglamentos habituales difícilmente se pueden hacer cumplir, porque se crearon específicamente para operar de forma anónima y no dejar rastro. Uno de ellos es lo que se conoce como _la dark web_. Hay beneficios al hacer que espacios anónimos como estos estén disponibles, aunque algunas personas abusan de ellos.
>
> En situaciones como estas, cuando la acción legal no está disponible, considera si estás dispuesto a recurrir a la defensa. Esto podría incluir arrojar luz sobre tu caso públicamente, generar un debate público al respecto y posiblemente sacar a la luz casos similares. Existen organizaciones y grupos de derechos humanos que pueden ayudarte con eso. Pueden ayudarte a establecer expectativas para los resultados y darte una idea de las ramificaciones.
>
> Buscar asistencia legal puede ser el último recurso si la comunicación con la plataforma, la aplicación o el proveedor de servicios falla o es imposible.
>
> El camino legal por lo general toma tiempo, cuesta dinero y puede requerir que hagas público que te has sufrido _doxing_ o que tus medias íntimos se publicaron sin tu consentimiento. Un caso legal con éxito depende de muchos factores, incluida la [documentación](/es/documentation#legal) de la agresión de una manera que los tribunales consideren aceptable, y el marco legal que rige el caso. Identificar a quien perpetra o determinar quienes fueron responsables también puede ser difícil de probar. También puede ser un desafío establecer la jurisdicción en la que se debe juzgar el caso.

¿Qué te gustaría hacer?

##### Options

- [Necesito apoyo para planificar una campaña de promoción](#advocacy_end)
- [Necesito apoyo legal](#legal_end)
- [Cuento con el apoyo de mi comunidad local](#resolved_end)

### advocacy_end

#### Necesito apoyo para planificar una campaña de defensa

> Si deseas contrarrestar la información sobre ti que se ha difundido en línea sin tu voluntad, te sugerimos que sigas las recomendaciones del [flujo de trabajo del Kit de Primeros Auxilios Digitales sobre campañas de difamación](/es/topics/defamation).
>
> Si deseas obtener apoyo para lanzar una campaña para exponer a tus atacantes, puedes ponerte en contacto con organizaciones que pueden ayudar con los esfuerzos de defensa:

[orgs](:organisations?services=advocacy)

### legal_end

#### Necesito apoyo legal

> Si necesitas apoyo legal, comunícate con las organizaciones siguientes que pueden ayudarte.
>
> Si estás pensando en emprender acciones legales, es muy importante que conserves pruebas de los ataques de los que fuiste objeto. Por lo tanto, se recomienda encarecidamente seguir las [recomendaciones del Kit de Primeros Auxilios Digitales sobre el registro de información sobre ataques](/es/documentation).

[orgs](:organisations?services=legal)

### physical-risk_end

#### Necesito apoyo para asegurar mi integridad física y mi bienestar

> Si estás en riesgo físico y necesitas ayuda inmediata, comunícate con las organizaciones siguientes que pueden ayudarte.

[orgs](:organisations?services=physical_security)

### assessment_end

#### Necesito apoyo para hacer un análisis de riesgos

> Si crees que necesitas ayuda para evaluar los riesgos que enfrentas porque tu información o medias se han compartido sin tu permiso, comunícate con las organizaciones siguientes que pueden ayudarte.

[orgs](:organisations?services=harassment&services=assessment)

### resolved_end

#### Mi problema ha sido resuelto

> Esperemos que esta guía de solución de problemas haya sido útil. Envía tus comentarios [por correo electrónico](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### defamation_flow_end

#### Necesito seguir utilizando la identidad en cuestión

> Si deseas contrarrestar la información sobre ti que se ha difundido en línea sin tu voluntad, te sugerimos que sigas las recomendaciones del [Flujo de trabajo del Kit de Primeros Auxilios Digitales sobre campañas de difamación](/es/topics/defamation).
>
> Si necesitas apoyo especializado, la siguiente lista incluye organizaciones que pueden ayudarte a abordar el daño a tu reputación.

[orgs](:organisations?services=advocacy&services=legal)

### platform_help_end

#### El contacto con la plataforma no prosperó

> A continuación se muestra una lista de organizaciones que pueden ayudarte a informar a plataformas y servicios.

[orgs](:organisations?services=harassment&services=legal)

### harassment_end

#### Necesito ayuda para saber qué debo hacer

> Si necesitas apoyo para abordar tu situación, comunícate con las siguientes organizaciones que pueden brindarte apoyo.

[orgs](:organisations?services=harassment&services=triage)

### Final Tips

- Mapea tu presencia en línea. El autodoxing consiste en explorar la inteligencia de código abierto sobre si para evitar que los actores maliciosos encuentren y usen esta información para hacerse pasar por ti. Obten más información sobre cómo buscar tus rastros en línea en [Guía de la línea de ayuda de Access Now para prevenir el _doxing_ (en inglés)](https://guides.accessnow.org/self-doxing.html) y [FemBloc: _Doxing_ y _outing_](https://docs.fembloc.cat/plataformas-doxing-outing-publicacion-informacion-personal-sin-consentimiento.html)
- Si el doxxing involucró información o documentos que solo tú, o unas pocas personas de confianza deberíais haber conocido o tenido acceso, entonces también puedes reflexionar sobre cómo accedió. Revisa tus prácticas de seguridad con la información personal para mejorar la seguridad de tus dispositivos y cuentas.
- Es posible que desees prestar atención en un futuro cercano, en caso de que el mismo material, o material relacionado, vuelva a aparecer en Internet. Una forma de protegerse es buscar tu nombre o los seudónimos que utilizaste en los motores de búsqueda para ver dónde pueden aparecer también. Puedes configurar una Alerta de Google para ti, o usar Meltwater o Mention. Estos servicios también te notificarán cuando tu nombre o seudónimo aparezca en Internet.

### Resources

- [Crash Override Network - Así que te han doxeado: una guía de mejores prácticas (en inglés)](https://crashoverridenetwork.tumblr.com/post/114270394687/so-youve-been-doxed-a-guide-to-best-practices)
- [Access Now Helpline Community Documentation: Guía para prevenir el _doxing_ (en inglés)](https://guides.accessnow.org/self-doxing.html)
- [Equality Labs: Guía anti-doxing para activistas que enfrentan ataques de la extrema derecha (en inglés)](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Bloqueo de tu identidad digital (en inglés)](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Ken Gagne, Doxxing defense: Retira tu información personal de los intermediarios de datos (en inglés)](https://www.computerworld.com/article/2849263/doxxing-defense-remove-your-personal-info-from-data-brokers.html)
- [Totem Project - curso Manténgalo privado (en inglés)](https://learn.totem-project.org/courses/course-v1:IWMF+IWMF_KP_EN+001/about)
- [Totem Project - curso Cómo proteger tu identidad en línea (en inglés)](https://learn.totem-project.org/courses/course-v1:Totem+TP_IO_EN+001/about)
- [Coalition against Online Violence - Me han doxeado (en inglés)](https://onlineviolenceresponsehub.org/for-journalists#doxxed)
- [PEN America: Manual contra el acoso en línea](https://onlineharassmentfieldmanual.pen.org/es/)
- [FemBloc: _Doxing_ y _outing_](https://docs.fembloc.cat/plataformas-doxing-outing-publicacion-informacion-personal-sin-consentimiento.html)
