---
layout: topic
title: "Recibí un mensaje sospechoso"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: es
summary: "Recibí un mensaje, un enlace o correo electrónico sospechoso, ¿qué debo hacer al respecto?"
date: 2023-05
permalink: topics/suspicious-messages
parent: /es/
order: 4
---

# Recibí un mensaje sospechoso

La forma más común de correos electrónicos sospechosos son los correos electrónicos de _phishing_, pero también puedes recibir mensajes _sospechosos_ en cuentas de redes sociales y/o aplicaciones de mensajería. Los mensajes de _phishing_ tienen como objetivo engañarte para que proporciones tu información personal, financiera o de cuenta. Es posible que te pidan que visites un sitio web falso o que llames a un número de servicio al cliente falso. Los mensajes de _phishing_ también pueden contener archivos adjuntos que instalan _software_ malicioso en tu dispositivo cuando se abren.

Si no tienes seguridad de la autenticidad del mensaje que recibes, o qué hacer al respecto, puedes usar el siguiente cuestionario como herramienta para diagnosticar la situación o compartir el mensaje con organizaciones externas de confianza que te proporcionarán un análisis más detallado del mensaje.

Ten en cuenta que recibir un correo electrónico sospechoso no significa necesariamente que tu cuenta haya sido comprometida. Si crees que un correo electrónico o un mensaje es sospechoso, no lo abras. No respondas al mensaje, no hagas clic en ningún enlace y no descargues ningún archivo adjunto.

## Workflow

### start

#### ¿Has realizado alguna acción en el mensaje o enlace?

##### Options

- [Hice clic en un enlace](#link-clicked)
- [Ingresé mis credenciales](#account-security_end)
- [Descargué un archivo](#device-security_end)
- [Respondí con información](#reply-personal-info)
- [No he tomado ninguna acción todavía](#do-you-know-sender)

### do-you-know-sender

#### He tomado ninguna acción todavía

¿Reconoces el remitente del mensaje? Ten en cuenta que el remitente del mensaje puede ser [falsificado](https://es.wikipedia.org/wiki/Suplantaci%C3%B3n) para parecer alguién en quien confías.

##### Options

- [Es una persona u organización que conozco](#known-sender)
- [Es un proveedor de servicios (como un proveedor de correo electrónico, _hosting_, redes sociales o bancos)](#service-provider)
- [El mensaje fue enviado por una persona u organización desconocida](#share)

### known-sender

#### Es una persona u organización que conozco

> ¿Puedes comunicarte con el remitente utilizando otro canal de comunicación? Por ejemplo, si recibiste un correo electrónico, ¿puedes verificarlo directamente con el remitente por Signal, WhatsApp o por teléfono? Asegúrate de utilizar un método de contacto existente. No necesariamente puedes confiar en un número de teléfono que aparece en el mensaje sospechoso.

¿Confirmaste que el remitente es quien te envió este mensaje?

##### Options

- [Sí](#resolved_end)
- [No](#share)

### service-provider

#### Es un proveedor de servicios (de correo electrónico, alojamiento, redes sociales, banco, etc.)

> En este escenario, un proveedor de servicios es cualquier compañía o marca que brinde servicios que usas o estás suscrito o suscrita. Esta lista puede incluir a tu proveedor de correo electrónico (Google, Yahoo, Microsoft, Prontonmail, etc.), tu proveedor de redes sociales (Facebook, Twitter, Instagram...), o plataformas en línea que tienen tu información financiera (Paypal, Amazon, bancos, Netflix, etc.).
>
> ¿Hay alguna forma de verificar si el mensaje es auténtico? Muchos proveedores de servicios también proporcionarán copias de notificaciones u otros documentos en la página de su cuenta. Por ejemplo, si el mensaje es de Facebook, debe incluirse en su [lista de correos electrónicos de notificación](https://www.facebook.com/settings?tab=security&section=recent_emails), o si es de tu banco, puedes llamar al servicio al cliente.

Elije una de las siguientes opciones:

##### Options

- [Pude verificar que es un mensaje legítimo de mi proveedor de servicios](#resolved_end)
- [No pude verificar el mensaje](#share)
- [No me suscribí a este servicio o estoy esperando un mensaje de ellos](#share)

### link-clicked

#### Hice clic en un enlace

> En algunos mensajes sospechosos, los enlaces pueden llevarte a páginas de inicio de sesión falsas que robarán tus credenciales, o incluso otro tipo de páginas que podrían robar tu información personal o financiera. Algunas veces, el enlace puede pedirte que descargues archivos adjuntos que instalan _software_ malicioso en tú computadora cuando se abren. El enlace también puede llevarte a un sitio web especialmente preparado que puede intentar infectar tú dispositivo con _software_ malicioso o espía.

¿Puedes decir qué es lo que pasó después de hacer clic en el enlace?

##### Options

- [Me pidió ingresar credenciales](#account-security_end)
- [Descargó un archivo](#device-security_end)
- [No pasó nada, pero no tengo certeza](#clicked-but-nothing-happened)

### clicked-but-nothing-happened

#### No pasó nada, pero no tengo certeza

> El hecho de que hayas hecho clic en un enlace sospechoso y no hayas notado ningún comportamiento extraño no significa que no se haya realizado ninguna acción maliciosa en segundo plano. Existen algunos escenarios que deberías evaluar. En el mejor de los escenarios, el mensaje que recibiste fue _spam_ utilizado con fines publicitarios. En este caso, algunos anuncios puede que hayan aparecido. En otros casos, los menos, estos anuncios podrían también ser maliciosos.

- [Aparecieron algunos anuncios. No sé si son maliciosos o no.](#suspicious-device_end)

> En el peor de los casos, al hacer clic en el enlace se pudo haber producido una vulnerabilidad al ejecutar un comando malicioso en el sistema operativo de tú dispositivo. Si esto sucediera, podría deberse a que tu navegador no está actualizado y tenga una vulnerabilidad que permite la ejecución de este _exploit_. En casos excepcionales en los que tú navegador está actualizado y ocurre esta situación, es posible que no se conozca dicha vulnerabilidad. En ambos casos, tú dispositivo podría comenzar a actuar de manera sospechosa.

> En otros escenarios, al visitar este enlace, es posible que hayas sido víctima de un [ataque de código de referencia cruzada (_cross-site script attack_ o XSS)](https://es.wikipedia.org/wiki/Cross-site_scripting). El resultado de este ataque será el robo de alguna _cookie_ usada para autenticarte en el sitio web que visitaste, por lo que quien ataca podrá iniciar sesión en el sitio con tus credenciales. Dependiendo de la seguridad del sitio, quien ataca podrá o no cambiar la contraseña. Esto se vuelve más serio si el sitio web que es vulnerable a ataques XSS es un sitio que tu administras, porque quien ataca también podrá autenticarse como administrador de dicho sitio web. Para identificar un ataque XSS, verifica si el enlace en el que hiciste clic contiene un [texto codificado (en inglés)](https://owasp.org/www-community/attacks/xss/). Esto también podría estar codificado en hexadecimal o Unicode.

##### Options

- [Aparecieron algunos anuncios. No tengo seguridad si son maliciosos o no](#suspicious-device_end)
- [Sí, mi navegador no está actualizado y/o mi dispositivo comenzó a actuar de manera sospechosa después de hacer clic en el enlace](#suspicious-device_end)
- [Hay un _script_ en el enlace o está parcialmente codificado](#cross-site-script_1)
- [No se pudo identificar ningún _script_](#suspicious-device_end)

### cross-site-script_1

#### ¿El sitio que visitaste es un sitio web en el que tienes una cuenta?

##### Options

- [Sí](#account-security_end)
- [No](#cross-site-script-2)

### cross-site-script-2

#### ¿El sitio al que llegaste es un sitio web que administras?

##### Options

- [Sí](#cross-site-script-admin-compromised)
- [No](#cross-site-script-3)

### cross-site-script-admin-compromised_end

#### Administro el sitio web al que dirigía el enlace sobre el que hice clic

> En este caso, quien ataca puede tener una _cookie_ válida que le permitirá acceder a tu cuenta de administración. Lo primero es iniciar sesión en tú interfaz de administración y eliminar cualquier sesión activa o simplemente cambiar la contraseña. También debes verificar si quien ataca cargó algún artefacto en tú sitio y/o publicó algún contenido malicioso y, de ser así, elimínalo.

Las siguientes organizaciones te pueden ayudar a investigar y responder a este incidente:

[orgs](:organisations?services=forensic)

### cross-site-script-3

#### No, no administro el sitio web

> Con esto deberías estar bien. Sin embargo, en algunos casos raros, un ataque XSS podría emplearse para usar tu navegador para lanzar otros ataques.

##### Options

- [Quiero evaluar si mi dispositivo se infectó](/es/topics/device-acting-suspiciously)
- [Creo que estoy bien](#final_tips)

### reply-personal-info

#### Respondí con información

> Dependiendo del tipo de información que compartiste, es posible que debas tomar medidas inmediatas.

¿Qué tipo de información compartiste?

##### Options

- [Compartí información confidencial de una cuenta](#account-security_end)
- [Compartí información pública](#share)
- [No tengo seguridad de cuán sensible era la información y necesito ayuda](#help_end)

### share

#### Compartir la información o mensaje sospechoso

> Compartir el mensaje sospechoso puede ayudar a proteger a tus colegas y a las comunidades que también podrían verse afectadas. También sería ideal pedir ayuda a alguien de tú confianza para que te confirme si es que el mensaje sospechoso es realmente peligroso. Considera compartir el mensaje con organizaciones que puedan analizarlos.
>
> Para compartir el mensaje sospechoso, asegúrate de incluir tanto el mensaje en sí, como la información del remitente. Si el mensaje era un correo electrónico, asegúrate de incluir el correo electrónico completo, con los encabezados, utilizando la [siguiente guía del centro de respuesta a incidentes informáticos de Luxemburgo (CIRCL) (en inglés)](https://www.circl.lu/pub/tr-07/).

¿Necesitas más ayuda?

##### Options

- [Sí, necesito mas ayuda](#help_end)
- [No, he resuelto mi problema](#resolved_end)

### device-security_end

#### Descargué un archivo

> En caso de que algunos archivos se hayan descargado en tu dispositivo, ¡la seguridad de este puede correr peligro!

Por favor, contacta a las siguientes organizaciones que pueden ayudarte. Luego, [comparte el mensaje sospechoso](#share).

[orgs](:organisations?services=device_security)

### account-security_end

#### He introducido o facilitado información sobre la cuenta

> En caso de que hayas ingresado tus credenciales o hayas sido víctima de un ataque de código de referencia cruzada (XSS o _cross-site script attack_), ¡tus cuentas pueden estar en riesgo!
>
> Si crees que tú cuenta está comprometida, se recomienda que también sigas el flujo de trabajo del Kit de Primeros Auxilios Digitales en [cuentas comprometidas](/es/topics/account-access-issues).
>
> Se sugiere que también informes a tu comunidad sobre esta campaña de _*phishing*_ y compartas el mensaje sospechoso con organizaciones que puedan analizarlos.

¿Deseas compartir información sobre el mensaje que recibiste o necesitas ayuda adicional?

##### Options

- [Quiero compartir el mensaje sospechoso](#share)
- [Necesito más ayuda para asegurar mi cuenta](#account_end)
- [Necesito mas ayuda para analizar el mensaje](#analysis_end)

### suspicious-device_end

#### No tengo claro qué ha ocurrido

> Si haces clic en un enlace y no estás seguro de lo que sucedió, es posible que tu dispositivo se haya infectado sin que te hayas dado cuenta. Si deseas explorar esta posibilidad, o tienes la sensación de que tú dispositivo puede estar infectado, intenta seguir los pasos de: ["Mi dispositivo está actuando de forma sospechosa"](/es/topics/device-acting-suspiciously).

Si necesitas ayuda inmediata porque tu dispositivo está actuando de manera sospechosa, puedes comunicarte con las organizaciones siguientes que pueden ayudarte. Después, [comparte tu mensaje sospechoso](#share).

[orgs](:organisations?services=device_security)

### help_end

#### Necesito más ayuda

> Debes buscar ayuda de tus colegas u otras personas para comprender mejor los riesgos de compartir dicha información. Otras personas en tu organización o red también pueden haber recibido mensajes similares.

Por favor, contacta a las siguientes organizaciones que pueden ayudarte. Luego, [comparte el mensaje sospechoso](#share).

[orgs](:organisations?services=digital_support)

### account_end

#### Necesito más ayuda para proteger mi cuenta

Si tu cuenta ha sido comprometida y necesitas ayuda para protegerla, comunicate con las organizaciones a continuación descritas que pueden ayudarte.

[orgs](:organisations?services=account)

### analysis_end

#### Necesito más ayuda para analizar el mensaje sospechoso

Las siguientes organizaciones pueden recibir tu mensaje sospechoso e investigar a fondo en tu nombre:

[orgs](:organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

#### Sí, mi problema ha sido resuelto

Con suerte esta guía del Kit de Primeros Auxilios Digitales te ha sido útil. Por favor danos tu opinión [vía correo electrónico](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

### Final Tips

La primera regla a recordar: nunca entregues información personal en correos electrónicos u otras herramientas de mensajería. Ninguna institución, banco u otras organizaciones solicitarán esta información a través de este medio. Puede que no siempre sea fácil reconocer si un correo electrónico, mensaje o sitio web es legítimo, pero hay algunos consejos que pueden ayudarte a evaluar lo que recibiste.

- Sentido de urgencia: los correos electrónicos sospechosos generalmente advierten de un cambio repentino en una cuenta y te piden que actúes de inmediato para verificar tu cuenta.
- En el cuerpo de un correo electrónico, puedes ver preguntas que te piden que "verifiques" o "actualices tu cuenta" o que "la falta de actualización de tus registros conlleva la suspensión de la cuenta". Por lo general, es seguro asumir que ninguna organización creíble a la que hayas proporcionado tu información te pedirá que la vuelvas a ingresar, no caigas en la trampa.
- Cuidado con los mensajes no solicitados, archivos adjuntos, enlaces y páginas de inicio de sesión.
- Sospecha si ves errores de ortografía y gramática.
- En emails, haz clic para ver la dirección completa del remitente, no solo el nombre que se muestra.
- Ten precaución con los enlaces acortados, ya que no puedes comprobar el destino final, te podrían llevar a un sitio web malicioso.
- Cuando pasas el ratón sobre un enlace, la URL real a la que te dirigen se muestra en una ventana emergente o en la parte inferior de la ventana de tu navegador.
- Los encabezados de correo electrónico que incluyan el parámetro "De:" (_from_:) podrían haber sido elaborados cuidadosamente para que parezcan legítimos. Al examinar los encabezados SPF y DKIM, puedes saber, respectivamente, si una dirección IP puede (o no) enviar correos electrónicos en nombre del dominio del remitente, y si los encabezados o el contenido se han modificado en tránsito. En un correo electrónico legítimo, el [SPF (en inglés)](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-_phishing_-the-definitive-guide#what-is-spf) y [DKIM (en inglés)](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-_phishing_-the-definitive-guide#what-is-dkim) los valores siempre deben ser 'PASS', de lo contrario, un correo electrónico no puede ser de confianza. La razón es que el correo electrónico está falsificado o, en raras ocasiones, el servidor de correo no está configurado correctamente.
- [Las firmas digitales (en inglés)](https://www.gnupg.org/gph/es/manual/c16.html) pueden decirnos si un correo electrónico ha sido enviado por el remitente legítimo y si ha sido modificado o no en el camino. Si el correo electrónico está firmado con OpenPGP, verifica si la firma está verificada o no. Para verificar una firma, necesitarás instalar OpenPGP y importar la clave pública asociada con el ID en la firma del mensaje. La mayoría de los clientes de correo electrónico modernos que admiten firmas digitales automatizarán la verificación por ti y te dirán a través de tu interfaz de usuarie si una firma está verificada o no.
- Una cuenta comprometida podría emitir un correo electrónico o mensaje malicioso con todas las condiciones anteriores y parecer legítimo. Sin embargo, normalmente el contenido del mensaje será inusual. Si el contenido del mensaje de correo electrónico parece extraño, siempre es una buena idea consultar con el remitente legítimo a través de un canal de comunicación diferente antes de realizar cualquier acción.
- Siempre es una buena práctica leer y escribir los correos electrónicos en texto simple. Los correos electrónicos basados en HTML se pueden procesar de forma que oculten códigos maliciosos o direcciones URL. Puedes encontrar instrucciones sobre cómo deshabilitar HTML en diferentes clientes de correo electrónico en [este post (en inglés)](https://useplaintext.email/).
- Usa la última versión del sistema operativo en tu teléfono móvil y computadora (mira las últimas versiones para [Android](https://es.wikipedia.org/wiki/Anexo:Historial_de_versiones_de_Android), [iOS](https://es.wikipedia.org/wiki/Anexo:Historial_de_versiones_de_iOS), [macOS](https://es.wikipedia.org/wiki/Historia_de_macOS) y [Windows](https://es.wikipedia.org/wiki/Microsoft_Windows).
- Actualiza lo antes posible tú sistema operativo y todas las aplicaciones y programas que hayas instalado, especialmente aquellos que reciben información (navegadores, mensajería instantánea y chat, clientes de correo electrónico, etc.). Elimina todas el _software_ que no necesites.
- Utiliza un navegador confiable (por ejemplo, Mozilla Firefox). Aumenta la seguridad de tu navegador revisando las extensiones complementos instalados. Deja solo aquellos en los que confíes (por ejemplo: [Privacy Badger](https://privacybadger.org/es/), [uBlock Origin (en inglés)](https://ublockorigin.com), [Facebook Container](https://addons.mozilla.org/es-es/firefox/addon/facebook-container/), [Cookie AutoDelete (en inglés)](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete), [NoScript (en inglés)](https://noscript.net/)).
- Realiza copias de seguridad periódicas y seguras de tu información.
- Protege tus cuentas con contraseñas seguras, autenticación de dos factores (2fa) y configuraciones seguras.

### Resources

Aquí van una serie de recursos para detectar mensajes sospechosos y evitar ser engañados.

- [Citizen Lab: Comunidades en riesgo - Amenazas digitales dirigidas contra la sociedad civil (en inglés))](https://targetedthreats.net).
- [Surveillance Self-Defense: Cómo evitar los ataques de _*phishing*_](https://ssd.eff.org/es/module/c%C3%B3mo-evitar-los-ataques-de-_phishing_-o-suplantaci%C3%B3n-de-identidad).
- [Herramienta de análisis de cabeceras de Google (en inglés)](https://toolbox.googleapps.com/apps/messageheader/).
