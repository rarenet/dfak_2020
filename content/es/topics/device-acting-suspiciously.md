---
layout: topic
title: "Mi dispositivo está actuando de forma sospechosa"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, Rarenet
language: es
summary: "Si tu computadora o teléfono actúan de manera sospechosa, puede haber software no deseado o malicioso en tu dispositivo."
date: 2023-05
permalink: topics/device-acting-suspiciously
parent: /es/
order: 3
---

# Mi dispositivo está actuando de forma sospechosa

Los ataques de software malicioso (_malware_) han evolucionado y se han sofisticados con los años. Estos ataques representan múltiples amenazas y pueden tener serias implicaciones a nivel de infraestructura y datos personales como organizacionales.

Los ataques de _malware_ vienen en diferentes formas como virus, _phishing_, _ransomware_, troyanos y _rootkits_. Algunas de las amenazas son: fallas en los equipos, robo de datos (credenciales confidenciales de una cuenta, información financiera, inicios de sesión de cuentas bancarias), un atacante que os chantajea para pagar un rescate al tomar el control de tu dispositivo, o la toma de control de tu dispositivo y utilizándolo para espiar en cualquier cosa que haces en línea para lanzar ataques de Denegación Distribuida de Servicio (DDoS).

Algunos métodos utilizados comúnmente por los atacantes para comprometerte a ti y a tus dispositivos pueden parecen actividades regulares, como:

- Un correo electrónico o una publicación en las redes sociales que tentarán les usuaries a abrir un archivo adjunto o a hacer clic en un enlace.
- Impulsar a las personas a descargar e instalar _software_ desde una fuente no confiable.
- Presionar a alguien para que ingrese su nombre de usuarie y contraseña en un sitio web cuyo aspecto parece legítimo, a pesar de no serlo.
- Instalando un software espía comercial en tu dispositivo cuando lo dejas desatendido y desbloqueado.

Esta sección del Kit de Primeros Auxilios Digitales te guiará a través de algunos pasos básicos para determinar si tu dispositivo está probablemente infectado o no.

Si crees que tu computadora o dispositivo móvil han comenzado a actuar de manera sospechosa, primero debes identificar los síntomas.

Los síntomas que comúnmente se pueden interpretar como actividad sospechosa del dispositivo, aunque a menudo no sean motivo suficiente para preocuparse, incluyen:

- Sonido de cliqueos durante llamadas telefónicas.
- Gasto de batería inesperado.
- Recalentamiento mientras el dispositivo no está en uso.
- Funcionamiento lento del dispositivo.

Estos síntomas a menudo se consideran erróneamente como indicadores confiables de actividad maliciosa en el dispositivo. Sin embargo, cualquiera de ellos por sí sólo no es motivo suficiente para preocuparse.

Los síntomas más confiables de un dispositivo comprometido por lo general son:

- El dispositivo se reinicia frecuentemente por sí sólo.
- Las aplicaciones fallan, especialmente después de la acción de entrada.
- Las actualizaciones del sistema operativo y/o los parches de seguridad fallan repetidamente.
- La luz indicadora de actividad de la cámara web está encendida mientras la cámara web no está en uso.
- Aparecen ["pantallas azules de la muerte"](https://es.wikipedia.org/wiki/Pantalla_azul_de_la_muerte) o pánicos del kernel de forma muy repetida.
- Ventanas con intermitencias.
- Advertencias de antivirus.

## Workflow

### start

#### ¿Crees que tu dispositivo está comprometido?

Dada la información proporcionada en la introducción, si aún sientes que tu dispositivo puede estar comprometido, la siguiente guía puede ayudarte a identificar el problema.

##### Options

- [Creo que mi dispositivo móvil está actuando de manera sospechosa](#phone-intro)
- [Creo que mi computadora está actuando de manera sospechosa](#computer-intro)
- [Ya no creo que mi dispositivo esté comprometido](#device-clean)

### device-clean

#### No, ya no creo que mi dispositivo esté comprometido

> ¡Genial! Sin embargo, ten en cuenta que estas instrucciones te ayudarán a realizar apenas un análisis rápido. Si bien debería ser suficiente para identificar anomalías visibles, un _malware_ más sofisticado podría ser capaz de esconderse de una manera más efectiva.

Si aún sospechas que el dispositivo podría estar comprometido, es posible que desees:

##### Options

- [Buscar ayuda adicional](#malware_end)
- [Proceder directamente a un reinicio del dispositivo](#reset)

### phone-intro

#### Sí, creo que mi dispositivo móvil está actuando de forma sospechosa

> Es importante tener en cuenta cómo tu dispositivo pudo haberse comprometido:
>
> - ¿Hace cuánto tiempo comenzaste a sospechar que tu dispositivo estaba actuando de manera sospechosa?
> - ¿Recuerdas haber hecho clic en algún enlace de fuentes desconocidas?
> - ¿Has recibido mensajes de personas que no reconoces?
> - ¿Instalaste algún _software_ sin firmar?
> - ¿El dispositivo ha estado fuera de tu posesión?

Reflexiona sobre estas preguntas para intentar identificar las circunstancias, si es que las hubiera, que llevaron a que tu dispositivo se vea comprometido.

##### Options

- [Tengo un dispositivo Android](#android-intro)
- [Tengo un dispositivo iOS](#ios-intro)

### android-intro

#### Tengo un dispositivo Android

> Primero verifica si hay aplicaciones desconocidas instaladas en tu dispositivo Android.
>
> Puedes encontrar una lista de aplicaciones en la sección "Aplicaciones" del menú de configuración. Identifica cualquier aplicación que no venga preinstalada con tu dispositivo y que no recuerdes haber descargado.
>
> Si sospechas de alguna de las aplicaciones en la lista, realiza una búsqueda en la web e identifica recursos para ver si hay informes que cataloguen a la aplicación como maliciosa.

¿Encontraste alguna aplicación sospechosa?

##### Options

- [No, no lo hice](#android-unsafe-settings)
- [Sí, identifiqué aplicaciones potencialmente maliciosas](#android-badend)

### android-unsafe-settings

#### No, no encuentro ninguna aplicación sospechosa

> Android ofrece la opción de controlar aspectos avanzados del dispositivo. Esto puede ser útil para quienes desarrollan _software_, pero también puede exponer a los dispositivos a ataques adicionales. Debes revisar estos ajustes y asegurarte de que estén configurados con las opciones más seguras. Los fabricantes pueden enviar dispositivos con valores inseguros por defecto, por lo que estas configuraciones deben revisarse incluso si no has hecho ningún cambio en tu dispositivo.
>
> **Aplicaciones no firmadas**
>
> Android normalmente bloquea la instalación de aplicaciones que no se cargan desde la tienda de aplicaciones de Google (Google Play Store). Google tiene procesos para revisar e identificar aplicaciones maliciosas en esta tienda. Los atacantes a menudo intentan evitar estas comprobaciones al enviar aplicaciones maliciosas directamente la persona usuaria a través de un mensaje que contiene o enlaza a un fichero de instalación. Asegúrate de que tu dispositivo no permita la instalación de aplicaciones desde fuentes no confiables.
>
> Revisa la sección "Seguridad" de la configuración de Android y asegúrate de que la opción _Instalar aplicaciones desde origenes desconocidos_ esté deshabilitada.
>
> **Modo de desarrollador y acceso ADB (Android Debug Bridge)**
>
> Android permite a quienes desarrollan _software_ ejecutar comandos directamente en el sistema operativo mientras se encuentra en "Modo de desarrollador". Cuando está habilitado, este modo expone a los dispositivos a ataques físicos. Alguien con acceso físico al dispositivo podría usar el Modo desarrollador para descargar copias de datos privados del dispositivo o para instalar aplicaciones maliciosas.
>
> Si ves un menú del Modo desarrollador en la configuración de tu dispositivo, debes asegurarse de que el acceso mediante ADB esté deshabilitado.
>
> **Google Play Protect**
>
> El servicio [Google Play Protect](https://www.android.com/intl/es_es/play-protect/) está disponible en todos los dispositivos Android recientes. Este realiza análisis regulares de todas las aplicaciones instaladas en tu dispositivo. Play Protect también puede eliminar automáticamente cualquier aplicación maliciosa conocida de tu dispositivo. Al activar este servicio se envía a Google información sobre tu dispositivo.
>
> Google Play Protect se puede habilitar en la configuración de seguridad de tu dispositivo. Hay más información disponible en el sitio [Play Protect](https://www.android.com/intl/es_es/play-protect/).

¿Identificaste alguna configuración insegura?

##### Options

- [No, no lo hice](#android-bootloader)
- [Sí, identifiqué configuraciones potencialmente inseguras](#android-badend)

### android-bootloader

#### No, no identifiqué ninguna configuración insegura

> El _bootloader_ de Android es un _software_ clave que se ejecuta tan pronto como enciendes tu dispositivo. Es un gestor de arranque que permite al sistema operativo iniciar y utilizar el hardware correspondiente. Un _bootloader_ comprometido le da a un atacante acceso completo al hardware del dispositivo. La mayoría de los fabricantes envían sus dispositivos con el _bootloader_ bloqueado. Una forma común de identificar si el _bootloader_ firmado por el fabricante ha sido modificado es reiniciar tu dispositivo y buscar el logotipo de arranque. Si aparece un triángulo amarillo con un signo de exclamación, entonces el _bootloader_ original fue reemplazado. Tu dispositivo también puede verse comprometido si muestra una pantalla de advertencia de _bootloader_ desbloqueado y no lo habías desbloqueado previamente para instalar una ROM de Android personalizada, como LineageOS or GrapheneOS. Deberías realizar un restablecimiento de fábrica de tu dispositivo si muestra una pantalla de advertencia de bootloader desbloqueada que no esperas.

¿Está el _bootloader_ comprometido o su dispositivo está usando el _bootloader_ original?

##### Options

- [El _bootloader_ de mi dispositivo está comprometido](#android-badend)
- [Mi dispositivo está usando su _bootloader_ original](#android-goodend)

### android-goodend

#### Mi dispositivo está utilizando el gestor de arranque original (_bootloader_)

> No parece que tu dispositivo haya sido comprometido.

¿Sigues sospechando de que tu dispositivo esté comprometido?

##### Options

- [Sí, me gustaría buscar ayuda profesional](#malware_end)
- [No, he resuelto mis problemas](#resolved_end)

### android-badend

#### Sí, creo que mi dispositivo está comprometido

> Tu dispositivo puede estar comprometido. Una configuración de restablecimiento de fábrica probablemente eliminará cualquier amenaza presente en tu dispositivo. Sin embargo, no siempre es la mejor solución. Además, es posible que desees investigar más a fondo esta alternativa para identificar tu nivel de exposición y la naturaleza precisa del ataque que has sufrido.
>
> Probablemente desees utilizar una herramienta para el autodiagnóstico llamada [VPN de emergencia (en inglés)](https://www.civilsphereproject.org/emergency-vpn) o [PiRogue (en inglés)](https://pts-project.org/), o buscar ayuda de una organización que pueda asistirte.

¿Te gustaría buscar más ayuda?

##### Options

- [Me gustaría restaurar mi dispositivo a la configuración de fábrica](#reset)
- [Me gustaría buscar ayuda profesional](#malware_end)
- [Tengo una red de asistencia local a la que puedo contactar para soporte profesional](#resolved_end)

### ios-intro

#### Tengo un dispositivo iOS

> Sigue los pasos para [ver quien ha accedido a tu iPhone o iPad](https://support.apple.com/es-es/guide/personal-safety/ipsb8deced49/web). Check the iOS settings to see if there is anything unusual. Comprueba la configuración de iOS para ver si hay algo inusual.
>
> En la configuración de la aplicación, verifica que tu dispositivo esté vinculado a tu ID de Apple. El primer elemento del menú en el lado izquierdo debe ser tu nombre o el nombre que usas para tu ID de Apple. Haz clic en él y verifica que se muestre la dirección de correo electrónico correcta. En la parte inferior de esta página, verás una lista con los nombres y modelos de todos los dispositivos iOS vinculados a este ID de Apple.
>
> Comprueba que tus dispositivos no están vinculados a un sistema de Gestión de Dispositivos Móviles (Mobile Device Management - MDM) que no reconoces. Ve a Configuración > General > VPN & Gestión de Dispositivos y comprueba si tienes una seccion para Perfiles. Puedes [borrar la configuración de perfiles desconodidos](https://support.apple.com/es-es/guide/personal-safety/ips41ef0e8c3/1.0/web/1.0#ips68379dd2e). Si no tienes perfiles listados, no tienes registro en MDM.

##### Options

- [Toda la información es correcta y todavía tengo el control de mi ID de Apple](#ios-goodend)
- [El nombre u otros detalles son incorrectos o veo dispositivos o Perfiles en la lista que no son míos](#ios-badend)

### ios-goodend

#### Toda la información es correcta y sigo en control de mi Apple ID

> No parece que tu dispositivo haya sido comprometido.

¿Sigues sospechando que tu dispositivo esté comprometido?

##### Options

- [Sí, me gustaría buscar ayuda profesional](#malware_end)
- [No, he resuelto mis problemas](#resolved_end)

### ios-badend

#### El nombre u otros detalles son incorrectos o veo dispositivos en la lista que no son míos

> Tu dispositivo puede estar comprometido. Un restablecimiento de la configuración de fábrica probablemente eliminará cualquier amenaza que esté presente en tu dispositivo. Sin embargo, no siempre es la mejor solución. Además, es posible que desees investigar más a fondo esta alternativa para identificar tu nivel de exposición y la naturaleza precisa del ataque.
>
> Probablemente desees utilizar una herramienta para el autodiagnóstico llamada [VPN de emergencia (en inglés)](https://www.civilsphereproject.org/emergency-vpn) o [PiRogue (en inglés)](https://pts-project.org/), o buscar ayuda de una organización que pueda asistirte.

¿Te gustaría buscar más ayuda?

##### Options

- [Me gustaría restaurar mi dispositivo a la configuración de fábrica](#reset)
- [Me gustaría buscar ayuda profesional](#malware_end)
- [Tengo una red de asistencia local a la que puedo contactar para soporte profesional](#resolved_end)

### computer-intro

#### Creo que mi computadora está actuando de manera sospechosa

> **Nota: En el caso de que estés bajo un ataque de _ransomware_, ve directamente a [No More Ransom!](https://www.nomoreransom.org/es/index.html).**
>
> Este cuestionario te ayudará a investigar actividades sospechosas en tu computadora. Si estás ayudando a una persona de forma remota, puedes intentar seguir los pasos descritos en los enlaces a continuación utilizando un software de escritorio remoto como [TeamViewer (en inglés)](https://www.teamviewer.com), o puedes revisar marcos de trabajo forenses remotos como el [GRR Rapid Response (en inglés)](https://github.com/google/grr). Ten en cuenta que la latencia y la confiabilidad de la red serán esenciales para llevar a cabo este proceso correctamente.

Por favor selecciona tu sistema operativo:

##### Options

- [Tengo una computadora con Windows](#windows-intro)
- [Tengo una computadora MacOS](#mac-intro)
- [Tengo una computadora con GNU/Linux](#linux-intro)

### windows-intro

#### Windows

> Puedes seguir esta guía introductoria para investigar actividades sospechosas en dispositivos con Windows:
>
> - [How to Live Forensic on Windows por Te-k (en inglés)](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md).

¿Te ayudaron estas instrucciones a identificar alguna actividad maliciosa?

##### Options

- [Sí, creo que la computadora está infectada](#device-infected)
- [No, no se identificó ninguna actividad maliciosa](#device-clean)

### mac-intro

#### MacOS

> Para identificar una posible infección en una computadora macOS, debes seguir estos pasos:
>
> 1. Comprobar si hay programas sospechosos que se inician automáticamente.
> 2. Comprobar si hay procesos en ejecución sospechosos.
> 3. Comprobar si hay extensiones sospechosas del kernel.
>
> El sitio web [Objective-See (en inglés)](https://objective-see.com) proporciona varias utilidades gratuitas que facilitan este proceso:
>
> - [KnockKnock (en inglés)](https://objective-see.com/products/knockknock.html) se puede usar para identificar todos los programas que están registrados para iniciarse de forma automática.
> - [TaskExplorer (en inglés)](https://objective-see.com/products/taskexplorer.html) puede ser usado para verificar procesos en ejecución e identificar aquellos que parecen sospechosos (por ejemplo, porque no están firmados, o porque están marcados por VirusTotal) .
> - [KextViewr (en inglés)](https://objective-see.com/products/kextviewr.html) puede ser usado para identificar cualquier extensión sospechosa del kernel que se carga en la computadora macOS.
>
> En caso de que no revelen nada sospechoso de inmediato y deseas realizar un seguimiento adicional, puedes usar [pcQF (en inglés)](https://github.com/botherder/pcqf). pcQF es una utilidad que simplifica el proceso de recopilación de información sobre el sistema y toma una instantánea completa de la memoria.
>
> Una herramienta adicional que podría ser útil para recopilar más detalles (pero que requiere cierta familiaridad con los comandos del terminal) es [AutoMacTC (en inglés)](https://www.crowdstrike.com/blog/automating-mac-forensic-triage/) de la empresa estadounidense de ciberseguridad CrowdStrike.

¿Te ayudaron estas instrucciones a identificar alguna actividad maliciosa?

##### Options

- [Sí, creo que la computadora está infectada](#device-infected)
- [No, no se identificó ninguna actividad maliciosa](#device-clean)

### device-infected

#### Sí, creo que la computadora está infectada

Ay, ¡no! Tu dispositivo parece estar infectado. Para deshacerte de la infección es posible que desees:

##### Options

- [Buscar ayuda adicional](#malware_end)
- [Proceder directamente restablecer los datos de fábrica del dispositivo](#reset)

### reset

#### Reestalecer los datos de fábrica del dispositivo

> Es posible que desees considerar restablecer de fábrica tu dispositivo como una medida de precaución adicional. Las guías a continuación te proporcionarán las instrucciones apropiadas para tu tipo de dispositivo:
>
> - [Android](https://support.google.com/android/answer/6088915?hl=es)
> - [iOS](https://support.apple.com/es-es/HT201252)
> - [Windows](https://support.microsoft.com/es-es/windows/reinstalar-windows-d8369486-3e33-7d9c-dccc-859e2b022fc7)
> - [MacOS](https://support.apple.com/es-es/HT201314)
> - [GNU/Linux (Ubuntu y Mint)](https://www.solvetic.com/tutoriales/article/3686-resetear-de-fabrica-sistema-ubuntu-linux-mint/)

¿Sientes que necesitas ayuda adicional?

##### Options

- [Sí](#malware_end)
- [No](#resolved_end)

### malware_end

#### Sí, creo que necesito más ayuda

Si necesitas ayuda adicional para tratar un dispositivo infectado, puedes comunicar con las organizaciones listadas a continuación.

[orgs](:organisations?services=vulnerabilities_malware)

### resolved_end

#### No, creo que no necesito más ayuda

Esperamos que este flujo de trabajo de la guía del Kit de Primeros Auxilios Digitales te haya sido útil. Envíanos tus comentarios [vía correo electrónico](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

### Final Tips

Estos son algunos consejos para evitar que tus dispositivos y datos se vean comprometidos:

- Siempre comprueba la legitimidad de cualquier correo electrónico que recibas, un archivo que hayas descargado o un enlace que solicite los detalles de inicio de sesión de tu cuenta.
- Lee más sobre cómo proteger tu dispositivo contra infecciones de _malware_ en las guías vinculadas en los recursos.

### Resources

- [Security in a Box. Protege tu dispositivo de _malware_ y ataques de _phishing_ (en inglés)](https://securityinabox.org/es/phones-and-computers/malware/).
  - Más pistas para proteger tus dispositivos (en inglés) [Android (en inglés)](https://securityinabox.org/en/phones-and-computers/android), [iOS (en inglés)](https://securityinabox.org/en/phones-and-computers/ios), [Windows (en inglés)](https://securityinabox.org/en/phones-and-computers/windows), [MacOS (en inglés)](https://securityinabox.org/en/phones-and-computers/mac), y [Linu (en inglés)x](https://securityinabox.org/en/phones-and-computers/linux).
- [Security Self-Defense: cómo evitar los ataques de _phishing_ o suplantación de identidad](https://ssd.eff.org/es/module/c%C3%B3mo-evitar-los-ataques-de-phishing-o-suplantaci%C3%B3n-de-identidad).
- [Security Without Borders - Guía sobre _phishing_ (en inglés)](https://guides.securitywithoutborders.org/guide-to-phishing/).
- [Xataka - Malware: qué es, qué tipos hay y cómo evitarlos](https://www.xataka.com/basics/malware-que-que-tipos-hay-como-evitarlos).
