---
layout: contact-method
title: Formulario de Contacto
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/contact-form.md
parent: /es/
published: true
---

Un formulario de contacto probablemente preservará la privacidad de tu mensaje a la organización receptora, de modo que solo tú y la organización receptora puedan leerlo. Este será el caso sólo si el sitio web que aloja el formulario de contacto implementa las medidas de seguridad adecuadas, como el cifrado [TLS/SSL](https://ssd.eff.org/es/glossary/secure-sockets-layer-ssl), entre otros. Tal es el caso de las organizaciones pertenecientes a CiviCERT.

Sin embargo, el simple hecho de haber visitado el sitio web de la organización donde está alojado el formulario de contacto puede ser conocido por gobiernos, fuerzas de seguridad u otras instituciones con acceso a la infraestructura de vigilancia local, regional o global. Visitar el sitio web de la organización puede indicar que te has puesto en contacto con con ella.

Si deseas mantener esta información en privado, entonces es preferible acceder a su sitio web a través del [navegador Tor](https://www.torproject.org/es/) o una VPN o proxy de confianza. Antes de hacerlo, considera el contexto legal donde resides y si necesitas ofuscar el uso del navegador Tor [configurándolo](https://tb-manual.torproject.org/es/running-tor-browser/) con un *[pluggable transport](https://tb-manual.torproject.org/es/circumvention/)*. Si estás considerando usar una VPN o un proxy, [investiga dónde se encuentra ese servidor o proxy VPN (en inglés)](https://protonvpn.com/blog/vpn-servers-high-risk-countries/) y [su confiabilidad como entidad VPN](https://ssd.eff.org/es/module/escogiendo-el-vpn-apropiado-para-usted).
