---
layout: contact-method
title: Llamada telefónica
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/phone-number.md
parent: /es/
published: true
---

Las comunicaciones por teléfonos de línea y móviles no están cifradas, por lo que los gobiernos, las fuerzas de seguridad u otros actores con el equipo técnico necesario podrían acceder al contenido de tu conversación y saber a quién llamas.
