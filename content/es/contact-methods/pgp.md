---
layout: contact-method
title: PGP
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/pgp.md
parent: /es/
published: true
---

PGP (*Pretty Good Privacy*, por sus siglas en inglés) y su equivalente de código abierto, GPG (*Gnu Privacy Guard*, por sus siglas en inglés), te permite cifrar el contenido de los correos electrónicos para proteger tu mensaje y evitar que lo vea tu proveedor de correo electrónico o cualquier otro actor que pueda tener acceso a tus correos. Sin embargo, el hecho de que hayas enviado un mensaje a la organización receptora puede ser accesible por los gobiernos o fuerzas de seguridad. Para evitar esto, puedes crear una dirección de correo electrónico alternativo no asociada a tu identidad.

#### Recursos

- [Documentación para la comunidad de la línea de ayuda en seguridad digital de Access Now: correo electrónico seguro (en inglés)](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html).
- [Freedom of the Press Foundation: Cifrar el correo electrónico con Mailvelope: una guía para principiantes](https://freedom.press/training/encrypting-email-mailvelope-guide/) y [Mailvelope: tutorial](https://mailvelope.com/es/help).
- [Herramientas de privacidad: proveedores de correo electrónico privados (en inglés)](https://www.privacytools.io/providers/email/).
