---
layout: contact-method
title: Correo electrónico
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/email.md
parent: /es/
published: true
---

Tanto contenido de tu mensaje como el hecho de que te contactaste con la organización pueden ser accesibles para gobiernos o fuerzas de seguridad.
