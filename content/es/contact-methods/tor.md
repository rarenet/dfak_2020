---
layout: contact-method
title: Tor
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/tor.md
parent: /es/
published: true
---

Tor es un navegador web enfocado en la privacidad que te permite interactuar con sitios web de forma anónima al no compartir tu ubicación (a través de tu dirección IP) cuando accedes al sitio web.

#### Recursos

- [Descripción general de Tor (en inglés)](https://www.torproject.org/about/overview.html.en).
- [Vídeo del funcionamiento en español](https://media.torproject.org/video/2015-03-animation/HQ/Tor_Animation_es.mp4).
