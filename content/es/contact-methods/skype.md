---
layout: contact-method
title: Skype
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/skype.md
parent: /es/
published: true
---

Los gobiernos o fuerzas de seguridad podrían acceder al contenido de tu mensaje, así como al hecho de que te contactaste con la organización.
