---
layout: page
title: "Arrestaron a alguien que conozco"
author: Peter Steudtner, Shakeeb Al-Jabri
language: es
summary: "Las fuerzas de seguridad han arrestado a alguien cercano, colega o familiar. Quisiera limitar el impacto del arresto en el grupo y cualquiera que tenga implicación."
date: 2019-03-13
permalink: arrested
parent: Home
sidebar: >
  <h3>Lee más sobre qué hacer si alguien que conoces ha sido arrestade:</h3>

  <ul>
    <li><a href="https://coping-with-prison.org">Inspiración y orientación para personas encarceladas y sus familias, abogados y simpatizantes (en inglés)</a></li>
    <li><a href="https://www.newtactics.org/search/solr/arrest">Obtener ayuda para hacer campaña en favor de personas detenidas (en inglés)</a></li>
  </ul>
---

# Arrestaron a alguien que conozco

El arresto de personas defensoras de derechos humanos, periodistas y activistas ponen a estas personas y a aquellos con los que trabajan en un gran riesgo.

Esta guía está orientada especialmente a países con carencias en términos de derechos humanos y debido proceso, o en donde las autoridades pueden sortear los caminos legales, o en donde actores paraestatales operan con libertad. En estos casos los arrestos representan un gran riesgo para las víctimas, sus colegas y familiares.

Con esta guía pretendemos mitigar el peligro que enfrentan las personas arrestadas y a limitar el acceso de quienes los arrestaron a información sensible que pueda incriminar a las víctimas y a sus colegas, o que pueda ser usada para comprometer otras operaciones.

Cuidar a quienes han sido detenides así como vigilar los impactos de la detención en términos digitales puede ser agotador y retador. Intenta buscar el apoyo de otras personas y coordina tus acciones con la comunidad afectada.

También es importante que te cuides a ti misme y a otres afectades por este arresto:

- [Cuidando tus necesidades psicosociales y tu bienestar](/es/self-care).
- Cuidando de la dimensión legal en el trabajo de apoyo (puedes encontrar organizaciones que ofrecen apoyo legal [aquí](/es/support)).
- [Obteniendo ayuda para hacer campaña en favor de personas detenidas (en inglés)](https://www.newtactics.org/search/solr/arrest)

Si esta situación te abruma en términos técnicos o emocionales o, por la razón que sea, no te sientes en posición de seguir los pasos descritos a continuación, por favor busca ayuda y guía con [organizaciones que ofrecen evaluaciones iniciales](/es/support) entre sus servicios.

## Diseña un plan

Antes de actuar con la información de las secciones siguientes, por favor sigue estos pasos:

- Intenta leer la guía completa y elabora un panorama de todas las áreas críticas de impacto antes de tomar decisiones en aspectos específicos. La razón de esto es porque las diferentes secciones se enfocan en diferentes escenarios de riesgo que pueden solaparse, por lo que necesitarás diseñar tu propia secuencia de acciones.
- Tómate un tiempo con tu equipo para hacer los análisis de riesgo que sean necesarios.

<a name="harm-reduction"></a>

## Prepárate antes de actuar

¿Tienes razones para creer que este arresto puede llevar a repercusiones para familiares, personas afines o colegas, incluyéndote?

Esta guía te llevará por una serie de pasos para ofrecerte soluciones que puedan ayudar a reducir la exposición de las personas detenidas y de cualquier otra persona que esté involucrada.

### Consejos para una respuesta coordinada

En todas las situaciones en donde una persona ha sido detenida, la primera cosa que debemos notar es que con frecuenta cuando ocurren este tipo de incidentes, muchas personas afines o colegas reaccionan al momento, resultando en esfuerzos duplicados e incluso contradictorios. Por esto es importante recordar que las acciones coordinadas y concertadas, tanto a nivel local como internacional, son esenciales para ayudar a quien está bajo detención y para apoyar a sus redes de apoyo, familia y afines.

- Establece un equipo de crisis que coordinará todas las actividades de apoyo, cuidado, campaña, etc.
- Involucra a familiares, pareja y otras personas cercanas tanto como sea posible (respetando sus límites si, por ejemplo, están con sobrecarga).
- Establece metas claras para tu campaña de apoyo (y revísalas frecuentemente): por ejemplo, puedes plantear como una meta la liberación de la persona detenida, garantizar su bienestar o proteger a su familia y afines, y asegurar su bienestar como la meta más inmediata.
- Acuerda usar canales seguros de comunicación así como la frecuencia y límites (por ejemplo que no haya comunicaciones entre las 22:00 y 08:00, excepto para emergencias o noticias de último momento).
- Distribuye tareas entre las personas del equipo y busca ayuda con terceras partes (análisis, incidencia, trabajo de medios, documentación, etc.).
- Pide más ayuda fuera del "equipo de crisis" para cosas desde comidas o compras hasta necesidades básicas (por ejemplo comidas regulares, etc.).

### Precauciones de seguridad digital

Si tienes razones para temer repercusiones para ti u otras personas, antes de lidiar con cualquier emergencia digital conectada a la detención, es crucial que tomes algunas medidas preventivas a nivel de seguridad digital para protegerte a ti y al resto de amenazas inmediatas.

- Acuerda cuáles serán los canales seguros de comunicación con que tú y tu red usarán para coordinar la mitigación de la emergencia y comunicaros sobre la persona detenida y repercusiones posteriores. Comparte permisos de administración con múltiples personas usuarias en los grupos de mensajería u otros grupos en herramientas de comunicación que estén usando. Puede que quieras considerar habilitar los mensajes temporales para una seguridad adicional.
- Reduce la información en tus dispositivos al mínimo necesario y protege los datos alojados en tus dispositivos [cifrándolos](https://ssd.eff.org/es/module/manteniendo-sus-datos-seguros).
- Crea [copias de seguridad seguras y cifradas](https://docs.fembloc.cat/dispositivos-respaldo-copia-seguridad-informacion-hacer-recuperar.html) de toda tu información y mantenlas en un lugar que no sea encontrado durante redadas o potenciales detenciones adicionales.
- Comparte las contraseñas de dispositivos, cuentas de internet, etc., con una persona de confianza que no esté bajo peligro inmediato. Los [gestores de contraseñas](https://www.incibe.es/ciudadania/blog/gestores-de-contrasenas-como-funcionan) te pueden ayudar con esta seguridad.
- Acuerda las acciones a tomar (como suspensión de cuentas, borrado remoto de dispositivos, etc.) como una primera reacción a cualquier detención adicional.

Esta página te guiará por una serie de pasos para encontrar soluciones que puedan ayudar a reducir la exposición de la persona detenida y de cualquier otra relacionada a esta, por ejemplo porque la información de contactos no se pueda encontrar en los dispositivos, cuentas de redes sociales o correos electrónicos de la persona detenida.

## Análisis de riesgos

### Reducir daños causados por nuestras propias acciones

En general, deberías intentar de diseñar tus acciones a partir de la siguiente pregunta:

- ¿Cuáles son los impactos de las acciones únicas y combinadas de la persona detenida, pero también de sus comunidades, colegas activistas, amigues, familia, etc., incluyéndote?

Cada una de las siguientes secciones describirán aspectos especiales de este análisis de riesgo.

Algunas consideraciones básicas son:

- Antes de borrar cuentas, datos, hilos de redes sociales, etc., asegúrate de que tienes documentada la información que se está borrando, especialmente si se tendrá que restaurar esa información o la necesitarás como futura evidencia.
- Si borras o eliminas cuentas o archivos, ten en cuenta que:
  - Las autoridades pueden interpretar esto como destrucción de evidencia.
  - Esto puede hacer más difícil la situación de la persona detenida en el caso en que las autoridades buscan el acceso a las cuentas y no lo consigan, haciendo ver a la persona detenida como no creíble y pudiendo resultar en acciones negativas producto de la eliminación de las cuentas.
- Si informas a otros que su información personal está almacenada en dispositivos o cuentas tomadas por las autoridades y estas comunicaciones son interceptadas, pueden representar evidencia adicional de la relación con la persona detenida.
- Los cambios en los procedimientos de comunicación (incluyendo el borrado de cuentas, etc.) pueden atraer la atención de las autoridades.

### Informa a los contactos

En general, es imposible determinar si las autoridades encargadas de la detención tienen la capacidad de mapear la red de contactos de la persona detenida, y si lo han hecho o no. Así que tenemos que asumir el peor escenario, que es que ya lo hicieron o lo harán en el futuro.

Antes de comenzar a informar a la red de contactos de la persona detenida, por favor evalúa el riesgo de informarlos:

- ¿Tienes una copia de la red de contactos de la persona detenida? ¿Puedes chequear quién está en su lista de contactos tanto en sus dispositivos, cuentas de correo y plataformas de redes sociales? Recopila una lista de posibles contactos para tener un panorama de a quiénes les pudiera afectar. [Almacena](https://securityinabox.org/en/files/secure-file-storage/) y [transfiere](https://securityinabox.org/en/communication/private-communication/) esta lista de la manera más segura posible y compártela bajo el criterio de que sólo quien la necesite conocer tenga acceso.
- ¿Existe un riesgo de que informar a los contactos los pueda relacionar más cercanamente con la persona detenida y esto pueda ser usado por las autoridades en contra de ellos?
- ¿Deberían ser informadas todas la personas en la lista de contactos o sólo un grupo específico?
- ¿Quién informará a los contactos? ¿Quién ya esta en contacto con ellos? ¿Cuál es el impacto de esta decisión?
- Establece el canal más seguro de comunicación, incluyendo reuniones físicas en espacios en donde no haya vigilancia, por ejemplo mediante circuito cerrado de televisión (CCTV), para informar a los contactos implicados.

### Documenta para recabar evidencia

Para más información en como guardar evidencia judicial, ve a la [sección sobre documentación incidentes digitales](/es/documentation).

Antes de borrar cualquier contenido de sitios web, redes sociales, hilos, etc., asegúrate de tenerlos documentados de antemano. Una razón para almacenar esta información es capturar cualquier señal o prueba de que las cuentas han sido infiltradas (cómo contenidos agregados o impersonados) o para conservar contenidos que puedas necesitar como evidencia legal.

Dependiendo del sitio web o plataforma de redes sociales desde donde quieras documentar información, se pueden tomar diferentes vías:

- Puedes tomar capturas de pantalla de partes relevantes (asegúrate de que aparezcan horas de publicación, URLs, etc.).
- Puedes comprobar que sitios web o blogs relevantes están indexados en [Wayback Machine](https://archive.org/web), o descargar estas páginas web en tu computadora.

**Puedes encontrar una información más completa en como almacenar información en línea en la [guía de documentación de DFAK](/es/documentation).**

_Recuerda que es importante mantener la información que descargues en un dispositivo seguro almacenado en un lugar seguro._

### Bloqueo de dispositivos

Si cualquier dispositivo de la persona detenida fue confiscado durante o después del arresto, por favor lee la guía [Perdí mis dispositivos](/es/topics/lost-device), en particular la sección de [borrado remoto](/es/topics/lost-device/questions/find-erase-device), incluyendo recomendaciones en caso de detección.

### Datos y cuentas en línea incriminatorios

Si los dispositivos de la persona detenida tienen o están conectados a cuentas en línea que contienen información que pueda perjudicarla, a sí misma o a otras personas), intenta limitar el acceso a esta información.

Antes de hacerlo, compara el riesgo asociado a esta información con el riesgo asociado a la reacción de las fuerzas de seguridad al no tener acceso a la información (o a la acción legal que pueden tomar basándose en destrucción de evidencia). Si el riesgo asociado a la información es mayor, puedes proceder a borrar información relevante, cerrar o suspender las cuentas, o desvincularlas del dispositivo:

#### Suspender o cerrar cuentas

Si tienes acceso a las cuentas que deseas cerrar, puedes seguir el proceso detallado por cada una de las diferentes plataformas para cerrar una cuenta. ¡Por favor asegúrate de tener una copia de seguridad del contenido borrado! Ten en cuenta que después de cerrar una cuenta el contenido no será inmediatamente inaccesible: en el caso de Facebook, por ejemplo, puede tomar hasta dos semanas para que el contenido sea borrado de todos los servidores.

Considera que existen diferentes opciones para suspender o cerrar cuentas, que tienen diferentes impactos en el contenido y el acceso:

- Bloqueo. El bloqueo de una cuenta evita que cualquier persona inicie sesión en la cuenta. Cuando se bloquea una cuenta, se eliminan todas las credenciales y tokens de aplicaciones, lo que significa que no es posible iniciar sesión en la cuenta, pero la cuenta se mantiene activa y cualquier contenido público seguirá siendo visible.

- Suspensión temporal. Al solicitar una suspensión temporal, las plataformas harán que la cuenta no esté disponible temporalmente. Esto significa que nadie podrá iniciar sesión en la cuenta, y la cuenta también aparecerá como suspendida para cualquier persona que intente interactuar con ella, mostrando un mensaje de "cuenta suspendida" cuando la vea o la contacte.

- Terminación. Se refiere al cierre de la cuenta, de forma que no sea recuperable en el futuro. Este suele ser el mecanismo de contención menos preferido, pero es el único disponible para algunos servicios y plataformas. Dada la imposibilidad de recuperar la cuenta correspondiente, es necesario el consentimiento del representante legal o de la familia antes de solicitar la cancelación de la cuenta.

Si no tienes acceso a las cuentas de la persona detenida o necesitas una acción más urgente sobre las cuentas de redes sociales por favor busca ayuda en las organizaciones listadas [aquí](/es/support) que pueden ofrecerte ayuda con cuentas entre sus servicios.

#### Desvincular cuentas de dispositivos

Algunas veces, es posible que desees desvincular cuentas en dispositivos, ya que este vínculo puede dar acceso a datos confidenciales a cualquier persona que controle el dispositivo conectado. Para hacer esto, puedes seguir las instrucciones en [Perdí mi dispositivo](/es/topics/lost-device#accounts).

No olvides desvincular las [cuentas de banco](#online_bank_accounts) de los dispositivos.

#### Cambiar contraseñas

Si decides no cerrar o suspender cuentas, puede resultar útil cambiar sus contraseñas siguiendo las instrucciones de [Perdí mi dispositivo](/es/topics/lost-device#passwords).

Considera también habilitar la autenticación en 2 factores para aumentar la seguridad de las cuentas de la persona detenida siguiendo las instrucciones de [Perdí mi dispositivo](/es/topics/lost-device#2fa).

En el caso de que la misma contraseña hubiese sido usada en diferentes cuentas, deberías cambiarlas todas ya que también pueden estar comprometidas.

**Otros consejos sobre el cambio de contraseñas**

- Usa [un gestor de contraseñas (en inglés)](https://securityinabox.org/en/passwords/passwords-and-2fa/#use-a-password-manager) (como [KeepassXC](https://keepassxc.org) y [¿Cómo sincronizar contraseñas de KeePass entre dispositivos?](https://protege.la/como-sincronizar-contrasenas-de-keepass-entre-dispositivos/) para documentar las contraseñas cambiadas para su futuro uso o para entregárselas a la persona detenida luego de su liberación.
- Asegúrate de contarle a la persona detenida, como muy tarde justo después de su liberación, acerca de las contraseñas cambiadas y devuélvele la posesión de sus cuentas.

#### Eliminar membresías de grupos y dejar de compartirarpetas

Si la persona detenida está en grupos de Facebook, WhatsApp, Signal o Wire, o pueden acceder a carpetas compartidas en línea, y su presencia en estos grupos le da a la persona detenida acceso a información privilegiada y potencialmente peligrosa, puedes querer sacarla de los grupos y de las carpetas u otros espacios compartidos.

**Instrucciones para quitar membresías de grupo en diferentes servicios de mensajería instantánea:**

- [WhatsApp](https://faq.whatsapp.com/841426356990637/?cms_platform=web&cms_id=841426356990637&draft=false).
- Telegram - La persona que creó el grupo puede eliminar participantes seleccionando "Información del grupo" y deslizando a la izquierda a aquellos usuarios que desee eliminar.
- [Wire (en inglés)](https://support.wire.com/hc/en-us/articles/203526400-How-can-I-remove-someone-from-a-group-conversation-).
- [Signal](https://support.signal.org/hc/es/articles/360050427692-Administrar-un-grupo#remove), sólo si administras el grupo.

**Instrucciones para dejar de compartir carpetas en diferentes servicios en línea:**

- [Facebook](https://www.facebook.com/help/211909018842184/).
- [Google Drive](https://support.google.com/drive/answer/2494893?hl=es&co=GENIE.Platform&sjid=9897576644868797276-EU). Primero busca los archivos de interés usando el operador de búsqueda "to:" (`to:usario@gmail.com`), luego selecciona todos los resultados y has clic en el ícono de compartir. Luego has clic en "Avanzado", elimina la dirección de correo de la ventana que aparece y guarda.
- [Dropbox](https://help.dropbox.com/es-es/files-folders/share/unshare-folder).
- [iCloud](https://support.apple.com/es-lamr/HT201081).

### Eliminar contenidos de redes sociales

En algunos casos querrás remover contenidos de las líneas de tiempo en las cuentas de redes sociales de la persona detenida u otras publicaciones asociadas a sus cuentas, ya que pueden ser usadas como evidencia en contra de la persona detenida o para generar confusión y conflicto dentro de su comunidad o desacreditarle.

Algunos servicios permiten borrar líneas de tiempo y publicaciones de formas más sencillas que otros. Guías para Twitter, Facebook e Instagram están disponibles abajo. Por favor asegúrate de que has documentado el contenido que quieres borrar, en el caso de necesitarlo como evidencia en caso de manipulación, etc.

- Para Twitter, puedes usar [Tweet Deleters](https://tweetdeleter.com/es/home).
- Para Facebook, puedes seguir la guía de Louis Barclays ["How to delete your Facebook News Feed" (en inglés)](https://medium.com/@louisbarclay/how-to-delete-your-facebook-news-feed-6c99e51f1ef6), basada en la aplicación para el navegador Chrome llamada [Nudge](https://chrome.google.com/webstore/detail/nudge/dmhgdnbkjkejeddddlklojinngaideac).
- Para Instagram, puedes seguir [las instrucciones en la página de Instagram sobre la edición y eliminación de publicaciones](https://help.instagram.com/997924900322403?cms_id=997924900322403). Ten en cuenta que las personas con acceso a la configuración de la cuenta en cuestión podrán ver el historial de la mayoría de interacciones, incluso después de eliminar el contenido.

### Eliminar referencias perjudiciales

Si existe cualquier información en línea en donde la persona detenida es nombrada y que pueda tener implicaciones negativas para esta o sus contactos, es una buena idea borrarla siempre y cuando esto no perjudique más a la persona detenida.

- Haz una lista de los espacios en línea y la información que necesitaría ser removida o cambiada.
- Si has identificado contenido para ser eliminado o cambiado, puedes querer hacer una copia de seguridad de este antes de proceder con su eliminación o solicitudes de bajada de información.
- Evalúa si la eliminación del nombre de la persona detenida puede tener algún impacto negativo en su situación (por ejemplo, al eliminar su nombre de una lista de miembros de una organización puede proteger a esta última, pero puede también dejar a la persona detenida sin justificación de que estaba trabajando para esa organización).
- Si tienes acceso a los respectivos sitios o cuentas, cambia o elimina la información sensible.
- Si no tienes acceso, pídele a personas que lo tengan que cambien o eliminen la información sensible.
- Puedes encontrar instrucciones para eliminar contenidos en servicios de Google [aquí](https://support.google.com/webmasters/answer/6332384?hl=es#get_info_off_web)
- Revisa si los sitios que contienen información sensible han sido indexados por [Wayback Machine (en inglés)](https://web.archive.org) o [Google Cache](https://cachedview.com/index.php?lang=es). Si es el caso, este contenido debe ser eliminado también.
  - [Cómo pedir la eliminación del Wayback Machine (en inglés)](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
  - [Eliminar una página alojada en tu web de Google](https://developers.google.com/search/docs/crawling-indexing/remove-information?hl=es)

<a name="online_bank_accounts"></a>

### Cuentas de banco

Muy frecuentemente las cuentas son gestionadas en línea y una verificación vía dispositivos móviles puede ser necesaria para realizar transacciones o incluso solamente para acceder a ella. Si una persona detenida no accede a sus cuentas bancarias por un período largo de tiempo, esto puede tener consecuencias sobre su situación financiera y su posibilidad de acceder a dicha cuenta en el futuro. En estos casos asegúrate de:

- Desvincular las cuentas bancarias de los dispositivos incautados a la persona detenida.
- Obtén una autorización y poder legal de la persona detenida para poder operar sus cuentas bancarias en su lugar (en coordinación con sus familiares).

## Consejos finales

- Asegúrate de devolver todo el control de todos los datos a la persona detenida después de su liberación.
- Lee los consejos de [Perdí mi dispositivo](/es/topics/lost-device#device-returned) sobre cómo tratar los dispositivos incautados después de que las autoridades los devuelvan.
