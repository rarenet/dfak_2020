---
language: es
layout: organisation
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: Lunes-Viernes, horas de oficina, horario de zona este, USA
response_time: 1 día
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

Freedom of the Press Foundation (FPF) es una organización sin fines de lucro 501(c)3 que protege, defiende y empodera al periodismo de interés público en el siglo XXI. FPF capacita a quien lo quiera, desde grandes organizaciones de medios hasta periodistas independientes, en una variedad de herramientas y técnicas de seguridad y privacidad para que se protejan mejor y también lo hagan con sus fuentes y a sus organizaciones.