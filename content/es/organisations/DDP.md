---
language: es
layout: organisation
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: lunes-jueves 9:00-17:00h CET
response_time: 4 días
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Digital Defenders Partnership ofrece apoyo a defensores de derechos humanos bajo amenaza digital y trabaja para fortalecer redes locales de respuesta rápida. DDP coordina apoyo de emergencia para individuos y organizaciones defensoras de derechos humanos, periodistas, activistas de sociedad civil y blogueros.

DDP tiene cinco tipos diferentes de financiación que abordan situaciones de emergencia urgentes, así como subvenciones a largo plazo centradas en la creación de capacidades dentro de una organización. Coordinan una Beca de Integridad Digital (Digital Integrity Fellowship) en donde las organizaciones reciben capacitaciones personalizadas en seguridad digital y privacidad, además de un programa de Red de Respuesta Rápida.
