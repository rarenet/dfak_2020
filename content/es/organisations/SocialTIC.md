---
language: es
layout: organisation
name: SocialTIC
website: https://socialtic.org/
logo: ST_logo-es.png
languages: English, Español
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, censorship
beneficiaries: journalists, hrds, cso, activists, lgbti, land, women, youth, other
hours: Monday to Friday, 8am to 6pm, GMT+1
response_time: From 12 hours
contact_methods: web form, email
web_form: https://socialtic.org/contacto/
email: seguridad@socialtic.org , contacto@socialtic.org
initial_intake: yes
---

SocialTIC es una ONG mexicana con una perspectiva latinoamericana, se enfoca en habilitar a los agentes de cambio (activistas, defensores, organizaciones de la sociedad civil y medios de comunicación independientes comprometidos con la justicia social) fortaleciendo sus capacidades de incidencia, alcance e influencia a través del uso estratégico y seguro de la tecnología digital y los datos. Nuestra visión apunta a fortalecer estratégicamente a los defensores que trabajan día a día para construir sociedades del siglo XXI más igualitarias, justas, abiertas, diversas e inclusivas.

SocialTIC ofrece formación especializada en seguridad digital, auditorías organizativas y respuesta de emergencia a activistas, organizaciones de la sociedad civil y periodistas. También trabajamos junto a estos grupos para que puedan abordar, comunicar y responder mejor a los retos a los que se enfrentan en materia de privacidad digital, violencia y ciberseguridad. SocialTIC ha formado a agentes de cambio latinoamericanos y ha desarrollado un plan de formación y auditorías de seguridad organizativa que combina los conocimientos de otras organizaciones internacionales especializadas y nuestra experiencia sobre el terreno.
