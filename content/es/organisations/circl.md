---
language: es
layout: organisation
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: office hours, UTC+2
response_time: 4 horas
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL es el CERT para el sector privado, comunes y entidades no gubernamentales en Luxemburgo.

CIRCL proporciona un punto de contacto fiable y confiable para cualquier persona usuaria, empresa y organización con sede en Luxemburgo, para el manejo de ataques e incidentes. Su equipo de expertos actúa como un cuerpo de bomberos, con la capacidad de reaccionar con prontitud y eficacia ante la sospecha, la detección de amenazas o la ocurrencia de incidentes.

El objetivo de CIRCL es recopilar, revisar, informar y responder a las amenazas cibernéticas de manera sistemática y rápida.