---
language: es
layout: organisation
name: Media Diversity Institute - Armenia
website: https://mdi.am/
logo: MDI_Armenia_Logo.png
languages: հայերեն, Русский, English
services: in_person_training, org_security, web_hosting, web_protection, digital_support, triage, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, censorship, physical_sec
beneficiaries: journalists, hrds, activists, lgbti, women, cso
hours: 24/7, GMT+4
response_time: 3 horas
contact_methods: email, pgp, phone, whatsapp, signal, telegram
email: help@mdi.am
pgp_key_fingerprint: D63C EC9C D3AD 51BE EFCB  683F F2B8 6042 F9D9 23A8
phone: "+37494938363"
whatsapp: "+37494938363"
signal: "+37494938363"
telegram: "+37494938363"
initial_intake: yes
---

Media Diversity Institute - Armenia es una organización sin ánimo de lucro, no gubernamental que busca equilibrar el poder de los medios tradicionales, redes sociales y nuevas tecnologías para salvaguardar los derechos humanos, ayudar a construir una sociedad civil democrática, dar voz a las personas sin voz y profundizar en el entendimiento colectivo de diferentes tipos de diversidad social.

MDI Armenia está afiliada con el Media Diversity Institute, pero es una entidad independiente.
