---
layout: page
title: "Rreth nesh"
language: sq
summary: "Rreth Veglërisë së Ndihmës së Parë Digjitale"
date: 2023-05
permalink: about
parent: Home
---

Veglëria e Ndihmës së Parë Digjitale është një përpjekje bashkëpunuese e [RaReNet (Rapid Response Network)](https://www.rarenet.org/) dhe [CiviCERT](https://www.civicert.org/).

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Rrjeti i Reagimit të Shpejtë (Rapid Response Network) është një rrjet ndërkombëtar i reaguesve të shpejtë dhe mbështetësve të sigurisë digjitale që përfshin Access Now, Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Fembloc, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtualroad, si dhe ekspertë individualë të sigurisë që punojnë në fushën e sigurisë digjitale dhe të reagimit të shpejtë.

Disa nga këto organizata dhe individë janë pjesë e CiviCERT, një rrjet ndërkombëtar për ndihmë për sigurinë digjitale dhe i ofruesve të infrastrukturës që janë të përqendruar kryesisht në mbështetjen e grupeve dhe individëve që synojnë drejtësinë sociale dhe mbrojtjen e të drejtave të njeriut dhe digjitale. CiviCERT është një kornizë profesionale për përpjekjet e shpërndara të komunitetit të reagimit të shpejtë CERT (Ekipi i Reagimit ndaj Emergjencave Kompjuterike - Computer Emergency Response Team). CiviCERT është akredituar nga Trusted Introducer, rrjeti evropian i ekipeve të besueshme për reagim ndaj emergjencave kompjuterike.

Veglëria e Ndihmës së Parë Digjitale është gjithashtu një [projekt me burim të hapur që pranon kontribute të jashtme](https://gitlab.com/rarenet/dfak_2020).

E falënderojmë [Fondacionin Metamorfozis](https://metamorphosis.org.mk) për lokalizimin [në gjuhën shqipe](https://digitalfirstaid.org/sq/), [EngageMedia](https://engagemedia.org/) për lokalizimin [në gjuhën birmaneze](https://digitalfirstaid.org/my/), [në gjuhën indoneziane](https://digitalfirstaid.org/id/) dhe [në gjuhën tailandeze](https://digitalfirstaid.org/th/), dhe [Media Diversity Institute](https://mdi.am/en/home) për lokalizimin [në gjuhën armene](https://digitalfirstaid.org/hy/) a të Veglërisë së Ndihmës së Parë Digjitale

Nëse dëshironi të përdorni Veglërinë e Ndihmës së Parë Digjitale në situata ku lidhja në internet është e kufizuar ose ku gjetja e një lidhjeje është e vështirë, mund të shkarkoni [një version offline](https://digitalfirstaid.org/dfak-offline.zip).

Për çdo koment, sugjerim apo pyetje në lidhje me Veglërinë e Ndihmës së Parë Digjitale, mund të shkruani në: dfak @ digitaldefenders . org

GPG - Fingerprint: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
