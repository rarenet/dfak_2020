---
language: sq
layout: organisation
name: TibCERT - Tibetan Computer Emergency Readiness Team
website: https://tibcert.org/
logo: tibcert-logo.png
languages: English, Tibetan
services: in_person_training, org_security, assessment, digital_support, secure_comms, device_security, advocacy
beneficiaries: journalists, hrds, activists, cso
hours: Monday - Friday, GMT+5.30
response_time: 24 hours during off hours, 2 hours during our working time
contact_methods: email, pgp, phone, whatsapp, signal, mail
email: info@tibcert.org
pgp: 0xF34C6C41A569F186
pgp_key_fingerprint: D1C5 8DE6 E45B 4DD7 92EF  F970 F34C 6C41 A569 F186
mail: Dhangshar House, Temple Road, McleodGanj, Distt. Kangra, HP - 176219 - India
phone: "Mobile: +919816170738 Office: +911892292177"
whatsapp: +919816170738
signal: +919816170738
initial_intake: yes
---

Ekipi Tibetian i Gatishmërisë për Emergjencat Kompjuterike (TibCERT) synon të krijojë një strukturë formale, të bazuar në koalicion për reduktimin dhe zbutjen e kërcënimeve në internet në komunitetin tibetian, si dhe të zgjerojë kapacitetin hulumtues teknik të tibetianëve për kërcënimet në diasporë dhe mbikëqyrjen dhe censurën brenda Tibetit, duke siguruar në fund liri dhe siguri më të madhe në internet për shoqërinë tibetiane në tërësi.

Misioni i TibCERT përfshin:

- Krijimin dhe mbajtjen e një platforme për bashkëpunim afatgjatë midis palëve të interesuara në komunitetin tibetian për çështjet dhe nevojat e sigurisë dixhitale,
- Thellimin e lidhjeve dhe zhvillimin e një procesi formal për bashkëpunim midis tibetianëve dhe studiuesve globalë të "malware" dhe sigurisë kibernetike për të siguruar ndarje reciproke të dobishme,
- Rritjen e burimeve në dispozicion të tibetianëve për t'u mbrojtur kundër dhe për të zbutur sulmet në internet duke publikuar rregullisht informacione dhe rekomandime mbi kërcënimet me të cilat përballet komuniteti,
- Ndihmën për tibetianët në Tibet të anashkalojnë censurën dhe mbikëqyrjen duke ofruar informacione dhe analiza të rregullta, të detajuara, si dhe zgjidhje të mundshme.