---
language: sq
layout: organisation
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: Monday-Thursday 9am-5pm CET
response_time: 4 days
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Digital Defenders Partnership (DDP) ofron mbështetje për mbrojtësit e të drejtave të njeriut nën kërcënimin digjital dhe punon për të forcuar rrjetet lokale të reagimit të shpejtë. DDP koordinon mbështetjen emergjente për individë dhe organizata siç janë mbrojtësit e të drejtave të njeriut, gazetarët, aktivistët e shoqërisë civile dhe blogerët.

DDP ka pesë lloje të ndryshme financimi që adresojnë situata urgjente emergjente, si dhe grante afatgjata të fokusuara në ngritjen e kapaciteteve brenda një organizate. Për më tepër, ata koordinojnë Bursën për Integritetin Digjital ku organizatat marrin trajnime të personalizuara për sigurinë dhe privatësinë digjitale, dhe programin Rrjeti i Reagimit të Shpejtë.