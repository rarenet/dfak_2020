---
language: sq
layout: organisation
name: Fondacioni i të Drejtave Digjitale (Digital Rights Foundation)
website: https://digitalrightsfoundation.pk/
logo: DRF.png
languages: English, اردو
services: grants_funding, in_person_training, org_security, web_hosting, web_protection, digital_support, relocation, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: 7 days a week, 9 AM-5 PM UTC+5 (PST)
response_time: 56 hours
contact_methods: email, pgp, mail, phone, whatsapp
email: helpdesk@digitalrightsfoundation.pk
pgp_key_fingerprint: 4D02 8B68 866F E2AD F26F  D7CC 3283 B8DA 6782 7590
mail: 180A, Garden Block, Garden Town, Lahore, Pakistan
phone: +9280039393
whatsapp: +923323939312
---

Fondacioni i të Drejtave Digjitale (DRF) është një organizatë joqeveritare e regjistruar e bazuar në hulumtime me seli në Pakistan. E themeluar në vitin 2012, DRF fokusohet në TIK-un për të mbështetur të drejtat e njeriut, gjithëpërfshirjen, proceset demokratike dhe qeverisjen digjitale. DRF punon për çështjet e fjalës së lirë në internet, privatësisë, mbrojtjes së të dhënave dhe dhunës në internet kundër grave.