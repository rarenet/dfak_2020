---
language: sq
layout: organisation
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, global
response_time: 12 hours
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Duke plotësuar punën thelbësore të Internews, Internews gjithashtu punon me individë, organizata dhe komunitete anembanë botës për të rritur ndërgjegjësimin për sigurinë digjitale, për të mbrojtur qasjen në një internet të hapur dhe të pacensuruar dhe për të përmirësuar praktikat e sigurisë digjitale. Internews ka trajnuar gazetarë dhe mbrojtës të të drejtave të njeriut në mbi 80 vende, dhe ka rrjete të forta të trajnerëve dhe auditorëve lokalë dhe rajonalë të sigurisë digjitale të njohur me Kornizën e Auditimit të Sigurisë dhe Modelin e Vlerësimit për Grupet e Avokimit (SAFETAG) ([https://safetag.org](https://safetag.org)), zhvillimin e të cilës e ka udhëhequr Internews. Internews dërton partneritete të forta dhe të përgjegjshme si me shoqërinë civile ashtu edhe me firmat e inteligjencës dhe analizës së kërcënimeve të sektorit privat, dhe mund të mbështesë partnerët drejtpërdrejt në mbajtjen e një pranie online, të sigurt dhe të pacensuruar. Internews ofron ndërhyrje teknike dhe jo-teknike nga vlerësimet bazë të sigurisë duke përdorur kornizën SAFETAG tek vlerësimet e politikave të organizatave e deri te strategjitë e rishikuara të zbutjes duke u bazuar në hulumtimin e kërcënimeve. Ata mbështesin drejtpërdrejt analizën e "phishing" dhe "malware" për të drejtat e njeriut dhe grupet e medias që përballen me sulme digjitale të synuara.