---
language: sq
layout: organisation
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: Monday to Friday, office hours, Eastern Time Zone, USA
response_time: 1 day
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

Fondacioni Freedom of the Press (FPF) është një organizatë jofitimprurëse që përkrah, mbron dhe fuqizon gazetarinë me interes publik në shekullin e 21-të. FPF trajnon të gjithë, nga organizatat e mëdha mediatike deri te gazetarët e pavarur për një sërë mjetesh dhe teknikash të sigurisë dhe privatësisë për të mbrojtur më mirë veten, burimet dhe organizatat e tyre.