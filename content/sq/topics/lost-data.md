---
layout: topic
title: "Kam humbur të dhënat e mia"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: sq
summary: "Çfarë të bëni në rast se keni humbur disa të dhëna"
date: 2023-04
permalink: topics/lost-data
parent: Home
order: 8
---

# Kam humbur të dhënat e mia

Të dhënat digjitale mund të jenë shumë kalimtare dhe të paqëndrueshme dhe ka shumë mënyra për t'i humbur ato. Dëmtimi fizik i pajisjeve tuaja, mbyllja e llogarive tuaja, fshirja e gabuar, përditësimet e softuerit dhe ndërprerjet aksidentale të programeve kompjuterike mund të shkaktojnë humbjen e të dhënave. Përveç kësaj, ndonjëherë mund të mos jeni të vetëdijshëm se si ose nëse funksionon sistemi juaj i kopjeve rezervë, ose thjesht i keni harruar kredencialet tuaja ose mënyrën për të gjetur ose rikuperuar të dhënat tuaja.

Ky seksion i Veglërisë së Ndihmës së Parë Digjitale do t'ju njoftojë me disa hapa themelorë për të diagnostikuar se si mund të keni humbur të dhënat dhe strategjitë e mundshme zbutëse për t'i rikuperuar ato.

_Në këtë seksion do të fokusohemi në të dhënat e vendosura në pajisje. Për përmbajtjen online dhe kredencialet, mund të ndiqni [rrjedhën e punës për çështjet e qasjes në llogari të Veglërisë së Ndihmës së Parë Digjitale](/sq/topics/account-access-issues)._

Në vazhdim keni një pyetësor për të identifikuar natyrën e problemit tuaj dhe për të gjetur zgjidhjet e mundshme.

## Workflow

### entry-point

#### Çfarë lloji të të dhënave keni humbur?

> Në këtë seksion do të fokusohemi kryesisht në të dhënat e vendosura në pajisje. Për përmbajtjen online dhe kredencialet, do t'ju drejtojmë në seksione të tjera të Veglërisë së Ndihmës së Parë Digjitale.
>
> Pajisjet përfshijnë kompjuterët, pajisjet celulare, hard disqe të jashtme, memorie USB dhe karta SD.

Çfarë lloji të të dhënave keni humbur?

##### Options

- [Përmbajtje online (në internet)](/sq/topics/account-access-issues)
- [Kredencialet](/sq/topics/account-access-issues)
- [Përmbajtje në pajisje](#content-on-device)

### content-on-device

#### Përmbajtje në pajisje

> Shpesh të dhënat tuaja "të humbura" janë të dhëna që thjesht janë fshirë - ose aksidentalisht ose me qëllim. Në kompjuterin tuaj, kontrolloni koshin tuaj të riciklimit (Recycle Bin) ose Trash. Në pajisjet Android, të dhënat e humbura mund t'i gjeni në drejtorinë LOST.DIR. Nëse skedarët e zhdukur ose të humbur nuk mund të gjenden në koshin e riciklimit (Recycle Bin) ose në Trash të sistemit tuaj operativ, është e mundur që ato të mos jenë fshirë, por të jenë në një vend tjetër nga ai që ju e prisni. Provoni funksionin e kërkimit në sistemin tuaj operativ për t'i gjetur ato.
>
> Provoni të kërkoni për emrin e saktë të skedarit tuaj të humbur. Nëse kjo nuk funksionon, ose nëse nuk jeni të sigurt për emrin e saktë, mund të provoni "kërkimin me xhoker" (d.m.th. nëse keni humbur një skedar docx, por nuk jeni i sigurt për emrin, mund të kërkoni "*.docx") që do t'i nxjerrë vetëm skedarët me shtesën `.docx`. Renditni sipas *Datës së ndryshimit\* në mënyrë që të gjeni shpejt skedarët më të fundit kur përdorni opsionin e “kërkimit me xhoker”.
>
> Përveç kërkimit të të dhënave të humbura në pajisjen tuaj, pyesni veten nëse keni një kopje rezervë të atyre të dhënave ose nëse i keni dërguar ato me e-mail ose i keni ndarë me dikë (duke përfshirë veten tuaj) ose nëse i keni shtuar ato në hapësirën tuaj në re (cloud) në çfarëdo momenti. Nëse është kështu, mund të kryeni një kërkim atje dhe mund të jeni në gjendje t'i riktheni ato ose ndonjë version të tyre.
>
> Për të rritur shanset për të rikuperuar të dhënat, ndaloni menjëherë ndryshimin dhe shtimin e informacionit në pajisjet tuaja. Nëse mundeni, lidhni diskun si pajisje të jashtme (p.sh. përmes USB) me një kompjuter të veçantë dhe të sigurt për të kërkuar dhe rikuperuar informacionin. Regjistrimi në disk (p.sh. shkarkimi ose krijimi i dokumenteve të reja) mund të zvogëlojë shanset për të gjetur dhe rikuperuar të dhënat e humbura.
>
> Kontrolloni gjithashtu skedarët dhe dosjet e fshehura, pasi të dhënat që keni humbur mund të jenë aty. Ja se si mund ta bëni atë në [Mac](https://www.macworld.co.uk/how-to/mac-software/show-hidden-files-mac-3520878/) dhe [Windows](https://support.microsoft.com/en-us/windows/view-hidden-files-and-folders-in-windows-97fbc472-c603-9d90-91d0-1166d1d9f4b5). Për Linux, shkoni te drejtoria juaj kryesore në një menaxher skedarësh dhe shtypni Ctrl + H.

Si i keni humbur të dhënat tuaja?

##### Options

- [Pajisja ku janë ruajtur të dhënat pësoi një dëmtim fizik](#tech-assistance_end)
- [Pajisja ku janë ruajtur të dhënat është vjedhur/humbur](#device-lost-theft_end)
- [Të dhënat u fshinë](#where-is-data)
- [Të dhënat u zhdukën pas një përditësimi të softuerit](#software-update)
- [Sistemi ose mjeti softuerik pësoi ndërprerje aksidentale dhe të dhënat u zhdukën](#where-is-data)

### software-update

#### Të dhënat u zhdukën pas një përditësimi të softuerit

> Ndonjëherë, kur përditësoni ose përmirësoni një vegël softuerike ose të gjithë sistemin operativ, mund të ndodhin probleme të papritura, duke bërë që sistemi ose softueri të mos funksionojë siç duhet, ose të ndodhë ndonjë sjellje e gabuar, duke përfshirë dëmtimin e të dhënave. Nëse keni vënë re një humbje të të dhënave menjëherë pas një përditësimi të softuerit ose sistemit, ia vlen të mendoni të ktheheni në një gjendje të mëparshme. Kthimet në gjendje të mëparshme janë të dobishme sepse ato nënkuptojnë që softueri juaj ose baza juaj e të dhënave mund të rikthehen në një gjendje të pastër dhe të qëndrueshme edhe pasi të kenë ndodhur operacione të gabuara ose ndërprerje aksidentale të softuerit.
>
> Metoda për të rikthyer një pjesë të softuerit në një version të mëparshëm varet nga mënyra se si është ndërtuar ai softuer - në disa raste kjo mund të bëhet lehtësisht, në raste të tjera nuk është aq lehtë, ndërsa disa të tjerë kërkojnë pak punë në lidhje me të. Kështu që do t'ju duhet të bëni një kërkim në internet për t'u kthyer në versionet më të vjetra për atë softuer të caktuar. Ju lutemi vini re se kthimi mbrapa njihet gjithashtu si "downgrading", kështu që bëni një kërkim edhe me këtë term. Për sistemet operative të tilla si Windows ose Mac, secili i ka metodat e veta për kthimin në versionin e mëparshëm.
>
> - Për sistemet Mac, vizitoni [bazën e njohurive të Qendrës së Mbështetjes Mac](https://support.apple.com) dhe përdorni fjalën kyçe "downgrading" me versionin tuaj macOS për të lexuar më shumë dhe për të gjetur udhëzime.
> - Për sistemet Windows, procedura ndryshon në varësi të asaj se a dëshironi ta anuloni ndonjë azhurnim specifik ose ta riktheni gjithë sistemin. Në të dyja rastet mund të gjeni udhëzime në [Microsoft Support Center](https://support.microsoft.com), për shembull për Windows 11 mund të kërkoni "How do I remove an installed update" në këtë [faqe të Pyetjeve të Shpeshta](https://support.microsoft.com/en-us/help/12373/windows-update-faq).

A ishte i dobishëm rikthimi i softuerit?

##### Options

- [Po](#resolved_end)
- [Jo](#where-is-data)

### where-is-data

#### Të dhënat u fshinë

> Bërja e rregullt e kopjeve rezervë të të dhënave është një praktikë e mirë dhe e rekomanduar. Ndonjëherë harrojmë se kemi aktivizuar krijimin automatik të kopjeve rezervë, kështu që ia vlen të kontrolloni në pajisjet tuaja nëse e keni aktivizuar atë opsion dhe ta përdorni atë kopje rezervë për të rikthyer të dhënat tuaja. Në rast se nuk bëni kopje rezervë tani, rekomandohet planifikimi për të bërë kopje rezervë në të ardhmen dhe mund të gjeni më shumë këshilla se si të krijoni kopje rezervë në [këshillat përfundimtare të kësaj rrjedhe pune](#resolved_end).

Për të kontrolluar nëse keni një kopje rezervë të të dhënave që i keni humbur, filloni të pyesni veten se ku janë ruajtur të dhënat e humbura.

##### Options

- [Në pajisjet të ruajtjes (hard disk i jashtëm, memorie USB, kartat SD)](#storage-devices_end)
- [Në një kompjuter](#computer)
- [Në një pajisje celulare](#mobile)

### computer

#### Në një kompjuter

Çfarë lloj sistemi operativ keni në kompjuterin tuaj?

##### Options

- [MacOS](#macos-computer)
- [Windows](#windows-computer)
- [Gnu/Linux](#linux-computer)

### macos-computer

#### MacOS

> Për të kontrolluar nëse pajisja juaj MacOS kishte të aktivizuar opsionin për të krijuar kopje rezervë dhe për ta përdorur atë kopje rezervë për t'i rikthyer të dhënat tuaja, kontrolloni opsionet tuaja në [[iCloud](https://support.apple.com/en-us/HT208682) ose [Time Machine](https://support.apple.com/en-za/HT201250) për të parë nëse ka ndonjë kopje rezervë të disponueshme.
>
> Një vend për të parë është lista "Recent items", që i ndjek aplikacionet, skedarët dhe serverët që i keni përdorur gjatë disa seancave tuaja të kaluara në kompjuter. Për të kërkuar skedarin dhe për ta rihapur atë, shkoni te menyja Apple në këndin e sipërm të majtë, zgjidhni "Recent items" dhe shfletoni listën e skedarëve. Më shumë detaje mund të gjenden në [Udhëzuesin zyrtar të përdoruesit të macOS](https://support.apple.com/en-za/guide/mac-help/mchlp2305/mac).

A arritët t'i gjeni të dhënat tuaja ose t'i riktheni ato?

##### Options

- [Po](#resolved_end)
- [Jo](#macos-restore)

### macos-restore

#### Jo, nuk munda të gjeja dhe rivendosja të dhënat e mia

> Në disa raste ka mjete falas dhe me burim të hapur që mund të jenë të dobishme për të gjetur përmbajtjen që mungon dhe për ta rikuperuar atë. Ndonjëherë ato janë të kufizuara në përdorim dhe shumica e mjeteve të njohura kushtojnë para.
>
> Për shembull, mund të provoni [EaseUS Data Recovery Wizard](https://www.easeus.com/mac-data-recovery-software/drw-mac-free.htm) ose [CleverFiles Disk Drill Data Recovery Software](https ://www.cleverfiles.com/).

A ishte ky informacion i dobishëm për rikuperimin e të dhënave tuaja (plotësisht ose pjesërisht)?

##### Options

- [Po](#resolved_end)
- [Jo](#tech-assistance_end)

### windows-computer

#### Windows

> Për të kontrolluar nëse keni një kopje rezervë të aktivizuar në kompjuterin tuaj Windows, shihni [Informacioni i mbështetjes së Microsoft: Bërja e kopjes rezervë dhe rikthimi i të dhënave në Windows](https://support.microsoft.com/en-us/windows/backup-and-restore-in-windows-352091d2-bb9d-3ea3-ed18-52ef2b88cbef).
>
> Windows përfshin veçorinë **Timeline**, i cili ka për qëllim të përmirësojë produktivitetin tuaj duke ruajtur një shënim të skedarëve që keni përdorur, faqeve të internetit që keni shfletuar dhe veprimeve të tjera që i keni bërë në kompjuterin tuaj. Nëse nuk e mbani mend se ku e keni ruajtur një dokument, mund të klikoni në ikonën "Timeline" në shiritin e detyrave në Windows 10 për të parë një regjistër vizual të organizuar sipas datës, dhe të ktheheni përsëri në atë që ju nevojitet duke klikuar ikonën e duhur të pamjes paraprake. Kjo mund t'ju ndihmojë të gjeni një skedar që është riemërtuar. Nëse "Timeline" është aktivizuar, disa nga aktivitetet e kompjuterit tuaj - si skedarët që redaktoni në Microsoft Office - mund të sinkronizohen edhe me pajisjen tuaj celulare ose me një kompjuter tjetër që përdorni, kështu që mund të keni një kopje rezervë të të dhënave tuaja të humbura në një pajisje tjetër. Lexoni më shumë për “Timeline” dhe si ta përdorni atë në [udhëzuesin zyrtar](https://support.microsoft.com/en-us/windows/get-help-with-timeline-febc28db-034c-d2b0-3bbe-79aa0c501039) .

A arritët t'i gjeni të dhënat tuaja ose t'i riktheni ato?

##### Options

- [Po](#resolved_end)
- [Jo](#windows-restore)

### windows-restore

#### Jo, nuk isha në gjendje të gjeja dhe rivendosja të dhënat e mia

> Në disa raste ka mjete falas dhe me burim të hapur që mund të jenë të dobishme për të gjetur përmbajtjen që mungon dhe për ta rikuperuar atë. Ndonjëherë ato janë të kufizuara në përdorim dhe shumica e mjeteve të njohura kushtojnë para.
>
> Për shembull, mund të provoni [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (shihni [udhëzimet hap pas hapi](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/datarecoverywizard/free-data-recovery-software.htm), [CleverFiles Disk Drill Recovery Software](https://www.cleverfiles.com/ data-recovery-software.html), ose [Recuva](http://www.ccleaner.com/recuva).

A ishte ky informacion i dobishëm për rikuperimin e të dhënave tuaja (plotësisht ose pjesërisht)?

##### Options

- [Po](#resolved_end)
- [Jo](#tech-assistance_end)

### linux-computer

#### GNU/Linux

> Disa nga versionet më të njohura Linux, si Ubuntu, kanë një mjet të integruar për të bërë kopje rezervë, për shembull [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) në Ubuntu. Nëse një mjet i integruar për të bërë kopje rezervë është i përfshirë në sistemin tuaj operativ, mund t'ju jetë kërkuar që të aktivizoni bërjen e kopjes rezervë në mënyrë automatike kur keni filluar ta përdorni kompjuterin për herë të parë. Kërkoni versionin tuaj të Linux për të parë nëse përfshin një mjet të integruar për të bërë kopje rezervë dhe cila është procedura për të kontrolluar nëse është i aktivizuar dhe për t'i rikthyer të dhënat nga kopjet rezervë
>
> Lexoni [Ubuntu & Deja Dup - Get up, backup](https://www.dedoimedo.com/computers/ubuntu-deja-dup.html) për të kontrolluar nëse e keni të aktivizuar bërjen automatike të kopjeve rezervë në kompjuterin tuaj. Lexoni ["Rivendosja e plotë e sistemit me Déjà Dup"](https://wiki.gnome.org/Apps/DejaDup/Help/Restore/Full) për udhëzime se si të riktheni të dhënat tuaja të humbura nga një kopje rezervë ekzistuese.

A ishte ky informacion i dobishëm për rikuperimin e të dhënave tuaja (plotësisht ose pjesërisht)?

##### Options

- [Po](#resolved_end)
- [Jo](#tech-assistance_end)

### mobile

#### Në një pajisje celulare

Çfarë sistemi operativ ka celulari juaj?

##### Options

- [iOS](#ios-phone)
- [Android](#android-phone)

### ios-phone

#### IOS

> Ju mund të keni aktivizuar bërjen automatike të kopjes rezervë me iCloud ose iTunes. Lexoni udhëzuesin ["Rivendosni ose konfiguroni pajisjen tuaj nga një kopje rezervë në iCloud"](https://support.apple.com/en-us/HT204184) për të kontrolluar nëse keni ndonjë kopje rezervë ekzistuese dhe për të mësuar se si t'i riktheni të dhënat tuaja.

A e keni gjetur kopjen rezervë tuaj dhe a i keni rikuperuar të dhënat tuaja?

##### Options

- [Po](#resolved_end)
- [Jo](#phone-which-data)

### phone-which-data

#### Cila nga deklaratat e mëposhtme vlen për të dhënat që keni humbur?

##### Options

- [Janë të dhëna të krijuara nga aplikacionet, p.sh. kontakte, burime, etj.](#app-data-phone)
- [Janë të dhëna të krijuara nga përdoruesi, p.sh. foto, video, audio, shënime](#ugd-phone)

### app-data-phone

#### Janë të dhëna të krijuara nga aplikacionet, p.sh. kontakte, burime, etj.

> Një aplikacion celular është një aplikacion softuerik i krijuar për t'u ekzekutuar në pajisjen tuaj celulare. Megjithatë, në shumicën e këtyre aplikacioneve mund të hyhet edhe përmes një shfletuesi desktop. Nëse keni humbur të dhënat e krijuara nga aplikacionet në celularët tuaj, përpiquni të hyni në atë aplikacion përmes shfletuesit tuaj në desktop, duke hyrë në ndërfaqen e aplikacionit në internet me kredencialet tuaja për atë aplikacion. Ndoshta mund t'i gjeni të dhënat që keni humbur në ndërfaqen e shfletuesit.

A ishte ky informacion i dobishëm për rikuperimin e të dhënave tuaja (plotësisht ose pjesërisht)?

##### Options

- [Po](#resolved_end)
- [Jo](#tech-assistance_end)

### ugd-phone

#### Janë të dhëna të krijuara nga përdoruesi, p.sh. foto, video, audio, shënime

> Të dhënat e krijuara nga përdoruesi janë lloj i të dhënave që i krijoni ose i gjeneroni përmes një aplikacioni specifik, dhe në rast të humbjes së të dhënave do të dëshironi të kontrolloni nëse ai aplikacion i ka cilësimet për të krijuar kopje rezervë të aktivizuara si parazgjedhje ose nëse mundëson një mënyrë për ta rikuperuar atë. Për shembull, nëse përdorni WhatsApp në celularin tuaj dhe bisedat u janë humbur ose ka ndodhur ndonjë gabim, mund t'i rikuperoni bisedat tuaja nëse e keni aktivizuar cilësimin e WhatsApp-it për bërje të kopjes rezervë, ose nëse përdorni një aplikacion për të krijuar dhe mbajtur shënime me informacione të ndjeshme ose personale, në të gjithashtu mund të jetë aktivizuar opsioni për të bërë kopje rezervë pa e vërejtur ju këtë ndonjëherë.
>
> Shumica e të dhënave të krijuara nga përdoruesi në aplikacione që janë ruajtur në telefon do të kopjohen në kopjen tuaj rezervë, qoftë në re (cloud) ose në kopjen rezervë në diskun lokal. Shihni këshillat në fund të kësaj rrjedhe pune për udhëzime se si të bëni një kopje rezervë të pajisjes suaj.

A ishte ky informacion i dobishëm për rikuperimin e të dhënave tuaja (plotësisht ose pjesërisht)?

##### Options

- [Po](#resolved_end)
- [Jo](#tech-assistance_end)

### android-phone

#### Android

> Google ka një shërbim të integruar në Android të quajtur Android Backup Service. Si parazgjedhje, ky shërbim bën kopje rezervë të disa llojeve të të dhënave dhe i lidh ato me shërbimin e duhur të Google, ku gjithashtu mund t'u qaseni në internet. Në shumicën e pajisjeve Android, mund të shihni cilësimet tuaja të sinkronizimit duke shkuar te Cilësimet > Llogaritë > Google dhe më pas duke zgjedhur adresën tuaj të Gmail. Nëse keni humbur të dhëna të llojit që jeni duke i sinkronizuar me llogarinë tuaj të Google, me gjasë do të jeni në gjendje t'i rikuperoni ato duke hyrë në llogarinë tuaj të Google përmes një shfletuesi të internetit.

A ishte ky informacion i dobishëm për rikuperimin e të dhënave tuaja (plotësisht ose pjesërisht)?

##### Options

- [Po](#resolved_end)
- [Jo](#phone-which-data)

### tech-assistance_end

#### Pajisja ku janë ruajtur të dhënat pësoi një dëmtim fizik

> Nëse humbja e të dhënave tuaja është shkaktuar nga dëmtime fizike, si p.sh. rënia e pajisjeve tuaja në dysheme ose në ujë, ekspozimi ndaj ndërprerjes së energjisë elektrike ose probleme të tjera, situata më e mundshme është se do t'ju duhet të bëni një rikuperim të të dhënave të ruajtura në pajisjen tuaj. Nëse nuk dini si t'i kryeni këto operacione, duhet të kontaktoni një person të TI që ka mjete për mirëmbajtjen e pajisjeve elektronike, i cili mund t'ju ndihmojë. Megjithatë, në varësi të kontekstit tuaj dhe ndjeshmërisë së të dhënave që duhet të rikuperoni, nuk këshillohet të kontaktoni ndonjë dyqan të TI-së, por rekomandohet t'u jepni përparësi personave të TI-së që i njihni dhe u besoni.

### device-lost-theft_end

#### Pajisja ku janë ruajtur të dhënat është vjedhur/humbur

> Në rast të pajisjeve të humbura, të vjedhura ose të konfiskuara, sigurohuni që të ndryshoni të gjitha fjalëkalimet tuaja sa më shpejt dhe të vizitoni [burimin tonë specifik se çfarë të bëni nëse keni humbur ndonjë pajisje](/sq/topics/I lost my devices).
>
> Nëse jeni anëtar i shoqërisë civile dhe keni nevojë për mbështetje për të blerë një pajisje të re për ta zëvendësuar pajisjen e humbur, mund të kontaktoni organizatat e shënuara më poshtë.

[orgs](:organisations?services=equipment_replacement)

### storage-devices_end

#### Do të doja të merrja më shumë ndihmë

> Për të rikuperuar të dhënat që keni humbur, mbani mend se koha është e rëndësishme. Për shembull rikuperimi i një skedari që e keni fshirë aksidentalisht disa orë ose një ditë më parë mund të ketë gjasa më të mëdha për t'u rikuperuar sesa një skedar që e keni humbur muaj më parë.
>
> Konsideroni të përdorni një vegël softuerike për të provuar të rikuperoni të dhënat që keni humbur (për shkak të fshirjes ose dëmtimit) siç është p.sh. [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (për Windows, Mac ose Linux - shikoni [udhëzimet hap pas hapi](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/data-recovery-software/) (për Windows, Mac, Android ose iOS), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/) (për Windows ose Mac), ose [Recuva](http://www.ccleaner.com/recuva) (për Windows). Kini parasysh që këto vegla nuk funksionojnë gjithmonë, sepse sistemi juaj operativ mund të ketë shkruar të dhëna të reja mbi informacionin tuaj të fshirë. Për shkak të kësaj, duhet të përdorni sa më pak të jetë e mundur kompjuterin tuaj nga momenti i fshirjes së një skedari deri në momentin e përpjekjes për ta rikthyer atë me një nga vegla e përmendura më sipër.
>
> Për të rritur shanset për të rikuperuar të dhënat, ndaloni menjëherë përdorimin e pajisjeve tuaja. Shkrimi i vazhdueshëm në hard disk mund të zvogëlojë shanset për gjetjen dhe rikthimin e të dhënave.
>
> Nëse asnjë nga opsionet e mësipërme nuk ka funksionuar për ju, situata më e mundshme është se do t'ju duhet të bëni një rikuperim të të dhënave të ruajtura në hard disk. Nëse nuk dini si t'i kryeni këto operacione, duhet të kontaktoni një person të TI që ka mjete për mirëmbajtjen e pajisjeve elektronike, i cili mund t'ju ndihmojë. Megjithatë, në varësi të kontekstit tuaj dhe ndjeshmërisë së të dhënave që duhet të rikuperoni, nuk këshillohet të kontaktoni ndonjë dyqan të TI-së, por rekomandohet t'u jepni përparësi personave të TI-së që i njihni dhe u besoni.

### resolved_end

#### My issue has been resolved

> Shpresojmë se ky udhëzues i DFAK ishte i dobishëm. Ju lutemi na jepni komente [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

- Kopjet rezervë - Është gjithmonë një ide e mirë të siguroheni se keni kopje rezervë - kopje rezervë të ndryshme, që i ruani diku tjetër nga vendi ku janë të dhënat tuaja! Në varësi të kontekstit tuaj, zgjidhni të ruani kopjen tuaj rezervë në shërbimet në re (cloud) dhe në pajisjet fizike të jashtme që i mbani të shkëputura nga kompjuteri juaj derisa jeni të lidhur në internet.
- Për të dy llojet e kopjeve rezervë, duhet t'i mbroni të dhënat tuaja me enkriptim. Bëni kopje rezervë të rregullt dhe të vazhdueshme të të dhënave më të rëndësishme dhe kontrolloni nëse i keni ato gati dhe provoni nëse jeni në gjendje t'i riktheni ato përpara se të kryeni përditësime të softuerit ose sistemit operativ.
- Krijoni një sistem të strukturuar dosjesh - Çdo person ka mënyrën e vet të organizimit të të dhënave dhe të informacioneve të rëndësishme të tij, d.t.th. nuk ka një mënyrë që u përshtatet të gjithëve. Megjithatë, është e rëndësishme që të merrni në konsideratë krijimin e një sistemi dosjesh që i përshtatet nevojave tuaja. Duke krijuar një sistem të qëndrueshëm dosjesh, mund ta bëni jetën tuaj më të lehtë duke e ditur më mirë se për cilat dosje dhe cilët skedarë duhet të bëhen kopje rezervë të shpeshta, për shembull ku gjenden informacionet e rëndësishme me të cilat punoni, ku duhet t'i mbani të dhënat që përmbajnë informacion personal ose të ndjeshëm për ju dhe bashkëpunëtorët tuaj, e kështu me radhë. Si zakonisht, merrni frymë thellë dhe ndani kohë për të planifikuar llojin e të dhënave që prodhoni ose menaxhoni, dhe mendoni për një sistem dosjesh që mund ta bëjë atë më të qëndrueshëm dhe më të rregulluar.

### Resources

- [Access Now Helpline Community Documentation: Secure back up](https://communitydocs.accessnow.org/182-Secure_Backup.html) -[Siguria në një kuti: taktikat për të bërë kopje rezervë](https://securityinabox.org/en/files/backup/)
- [Faqja zyrtare për kopjet rezervë e Mac](https://support.apple.com/mac-backup)
- [Si të bëni kopje rezervë të të dhënave rregullisht në Windows](https://support.microsoft.com/en-us/windows/backup-and-restore-in-windows-352091d2-bb9d-3ea3-ed18-52ef2b88cbef)
- [Si ta aktivizoni bërjen e rregullt të kopjeve rezervë në iPhone](https://support.apple.com/en-us/HT203977)
- [Si ta aktivizoni bërjen e rregullt të kopjeve rezervë në Android](https://support.google.com/android/answer/2819582?hl=en)
- [Bëni kopje rezervë të të dhënave tuaja në mënyrë të sigurt](https://johnopdenakker.com/securily-backup-your-data/)
- [Si të bëni kopje rezervë të jetës suaj digjitale](https://www.wired.com/story/how-to-back-up-your-digital-life/)
- [Wikipedia - Rikuperimi i të dhënave](https://en.wikipedia.org/wiki/Data_recovery)
