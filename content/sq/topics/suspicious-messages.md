---
layout: topic
title: "Kam marrë një mesazh të dyshimtë"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: sq
summary: "Kam marrë një mesazh, lidhje ose email të dyshimtë, çfarë duhet të bëj?"
date: 2023-05
permalink: topics/suspicious-messages
parent: Home
order: 4
---

# Kam marrë një mesazh të dyshimtë

Forma më e zakonshme e email-eve të dyshimta janë e-mail-et “phishing”, por ju mund të merrni mesazhe të dyshimta në llogaritë e mediave sociale e poashtu edhe/ose në aplikacionet për dërgimin e mesazheve. Mesazhet “phishing” synojnë t'ju mashtrojnë që të jepni informacionin tuaj personal, financiar ose atë të llogarisë. Ata mund t'ju kërkojnë të vizitoni një faqe interneti të rreme ose të telefononi një numër të rremë shërbimi ndaj klientit. Mesazhet “phishing” mund të përmbajnë gjithashtu bashkëngjitje që instalojnë softuer keqdashës në kompjuterin tuaj kur të hapen.

Nëse nuk jeni të sigurt për vërtetësinë e mesazhit që keni marrë, ose çfarë të bëni për të, mund të përdorni pyetësorin e mëposhtëm si një mjet udhëzues për të diagnostikuar më tej situatën ose për të ndarë mesazhin me organizata të jashtme të besuara që do t'ju ofrojnë një analizë më të detajuar të mesazhit tuaj.

Mbani në mend se marrja e një e-maili të dyshimtë nuk do të thotë domosdoshmërisht se llogaria juaj është komprometuar. Nëse mendoni se një e-mail ose mesazh është i dyshimtë, mos e hapni. Mos iu përgjigjni mesazhit, mos klikoni asnjë lidhje dhe mos shkarkoni asnjë bashkëngjitje.

## Workflow

### intro

#### A keni kryer ndonjë veprim në atë mesazh ose lidhje?

##### Options

- [Kam klikuar në lidhje](#link-clicked)
- [Kam futur kredencialet](#account-security_end)
- [Shkarkova një skedar](#device-security_end)
- [U përgjigja me informacion](#reply-personal-info)
- [Nuk kam marrë ende asnjë veprim](#do-you-know-sender)

### do-you-know-sender

#### Nuk kam marrë ende asnjë veprim

A e njihni dërguesin e mesazhit? Kini parasysh se dërguesi i mesazhit mund të jetë [i rremë](https://en.wikipedia.org/wiki/Spoofing_attack) që të duket se është dikush që ju i besoni.

##### Options

- [Është një person ose organizatë që e njoh](#known-sender)
- [Është një ofrues shërbimesh (si p.sh. ofrues i e-mailit, hostingut, media sociale ose bankë)](#service-provider)
- [Mesazhi është dërguar nga një person apo organizatë e panjohur](#share)

### known-sender

#### Është një person ose organizatë që e njoh

> A mund ta kontaktoni dërguesin duke përdorur një kanal tjetër komunikimi? Për shembull, nëse keni marrë një e-mail, a mund ta verifikoni atë drejtpërdrejt me dërguesin përmes aplikacionit Signal, WhatsApp ose përmes telefonit? Sigurohuni që të përdorni një metodë ekzistuese kontakti. Nuk mund t'i besoni domosdoshmërisht një numri telefoni të shënuar në mesazhin e dyshimtë.

A e keni konfirmuar që dërguesi është ai që ua ka dërguar këtë mesazh?

##### Options

- [Po](#resolved_end)
- [Jo](#share)

### service-provider

#### Është një ofrues shërbimesh (si p.sh. ofrues i e-mailit, hostingut, media sociale ose bankë)

> Në këtë skenar, një ofrues shërbimi është çdo kompani ose markë që ofron shërbime që ju i përdorni ose ku jeni abonuar. Kjo listë mund të përmbajë ofruesin tuaj të e-mailit (Google, Yahoo, Microsoft, ProtonMail...), ofruesin tuaj të mediave sociale (Facebook, Twitter, Instagram...) ose platformat online që kanë informacionin tuaj financiar (Paypal, Amazon, bankat, Netflix...).
>
> A keni ndonjë mënyrë për të kontrolluar nëse mesazhi është autentik? Shumë ofrues shërbimesh do të ofrojnë gjithashtu kopje të njoftimeve ose dokumente të tjera në faqen e llogarisë tuaj. Për shembull, nëse mesazhi është nga Facebook, ai duhet të përfshihet në [listën e e-maileve të njoftimit](https://www.facebook.com/settings?tab=security§ion=recent_emails), ose nëse është nga banka juaj, mund të telefononi shërbimin e klientit të bankës suaj.

Zgjidhni një nga opsionet e mëposhtme:

##### Options

- [Arrita ta verifikoj se është një mesazh legjitim nga ofruesi im i shërbimit](#resolved_end)
- [Nuk arrita ta verifikoja mesazhin](#share)
- [Nuk jam abonuar në këtë shërbim dhe/ose nuk pres mesazh prej tyre](#share)

### link-clicked

#### Kam klikuar në lidhje

> Në disa mesazhe të dyshimta, lidhjet mund t'ju dërgojnë në faqe të rreme identifikimi që do t'jua vjedhin kredencialet ose lloje të tjera faqesh që mund t'ua vjedhin informacionin tuaj personal ose financiar. Ndonjëherë, lidhja mund t'ju kërkojë të shkarkoni bashkëngjitjet që instalojnë keqdashës në kompjuterin tuaj kur të hapen. Lidhja gjithashtu mund t'ju çojë në një faqe interneti të përgatitur posaçërisht që mund të përpiqet të infektojë pajisjen tuaj me softuer keqdashës ose spiunues.

A mund të tregoni se çfarë ndodhi pasi klikuat në lidhjen?

##### Options

- [Më kërkoi të fusja kredencialet](#account-security_end)
- [Shkarkoi një skedar](#device-security_end)
- [Asgjë nuk ndodhi, por nuk jam i sigurt](#clicked-but-nothing-happened)

### clicked-but-nothing-happened

#### Asgjë nuk ndodhi, por nuk jam i sigurt

> Fakti që keni klikuar në një lidhje të dyshimtë dhe nuk keni vënë re ndonjë sjellje të çuditshme nuk do të thotë që asnjë veprim keqdashës nuk ka ndodhur në sfond. Ka disa skenarë për të cilët duhet të mendoni. Më pak shqetësues është rasti kur mesazhi që keni marrë ishte “spam” i përdorur për qëllime reklamimi. Në këtë rast mund të jenë shfaqur disa reklama. Në disa raste, këto reklama mund të jenë gjithashtu keqdashëse.
>
> Në skenarin më të keq, duke klikuar në lidhje është shfrytëzuar rasti për të ekzekutuar një komandë keqdashëse në sistemin tuaj. Nëse kjo ka ndodhur, kjo mund të jetë për shkak të faktit se shfletuesi juaj nuk është i përditësuar dhe ka një cenueshmëri që e lejon këtë shfrytëzim. Në raste të rralla kur shfletuesi juaj është i përditësuar dhe ky skenar ka ndodhur, dobësia e shfrytëzuar nuk mund të dihet. Në të dyja rastet, pajisja juaj mund të fillojë të veprojë në mënyrë të dyshimtë.
>
> Në skenarë të tjerë, duke vizituar këtë lidhje, ju mund të keni qenë viktimë e [sulmit të quajtur “cross-site script attack# (ose XSS)](https://en.wikipedia.org/wiki/Cross-site_scripting). Rezultati i këtij sulmi do të jetë vjedhja e kukive tuaj të përdorur për t'ju vërtetuar në ueb-faqen e vizituar, kështu që sulmuesi do të jetë në gjendje të hyjë në ueb-faqet me emrin tuaj të përdoruesit. Në varësi të sigurisë së ueb-faqes, sulmuesi mund të jetë ose të mos jetë në gjendje ta ndryshojë fjalëkalimin. Kjo bëhet edhe më serioze nëse faqja e internetit që është e pambrojtur ndaj XSS është një faqe që ju e menaxhoni, sepse në raste të tilla sulmuesi do të jetë në gjendje të vërtetohet si administrator i faqes suaj të internetit. Për të identifikuar një sulm XSS, kontrolloni nëse lidhja që keni klikuar përmban një [varg kodi programues](https://owasp.org/www-community/attacks/xss/). Kjo mund të jetë gjithashtu e koduar në HEX ose Unicode.

##### Options

- [U shfaqën disa reklama. Nuk jam i sigurt nëse janë keqdashëse apo jo](#suspicious-device_end)
- [Po, shfletuesi im nuk është i përditësuar dhe/ose pajisja ime filloi të veprojë në mënyrë të dyshimtë pasi klikova lidhjen](#suspicious-device_end)
- [Ka një skript (kod) në lidhje ose është pjesërisht i koduar](#cross-site-script_1)
- [Asnjë skript (kod) nuk mund të identifikohej](#suspicious-device_end)

### cross-site-script_1

#### A është faqja që keni vizituar një faqe interneti ku keni një llogari?

##### Options

- [Po](#account-security_end)
- [Jo](#cross-site-script-2)

### cross-site-script-2

#### A është faqja që keni përfunduar në një faqe interneti që ju menaxhoni?

##### Options

- [Po](#cross-site-script-admin-compromised_end)
- [Jo](#cross-site-script-3)

### cross-site-script-admin-compromised_end

#### Unë menaxhoj faqen e internetit në të cilën çoi lidhja që klikova

> Në këtë rast, sulmuesi mund të ketë një kuki të vlefshme që i lejon atij të hyjë në llogarinë tuaj të administratorit. Gjëja e parë është të hyni në ndërfaqen tuaj të administratorit dhe të mbyllni çdo seancë aktive ose thjesht të ndryshoni fjalëkalimin. Gjithashtu duhet të kontrolloni nëse sulmuesi ka ngarkuar ndonjë objekt në faqen tuaj dhe/ose nëse ka postuar ndonjë përmbajtje keqdashëse, dhe nëse po, hiqeni atë.

Organizatat e mëposhtme mund të ndihmojnë në hetimin dhe reagimin ndaj këtij incidenti:

[orgs](:organisations?services=forensic)

### cross-site-script-3

#### Jo, nuk e menaxhoj këtë faqe interneti

> Kjo duhet të jetë mirë. Megjithatë, në disa raste të rralla, XSS mund të përdoret për të përdorur shfletuesin tuaj për të filluar sulme të tjera.

##### Options

- [Dëshiroj të vlerësoj nëse pajisja ime është infektuar](/sq/topics/device-acting-suspiciously)
- [Mendoj se nuk kam problem](#final_tips)

### reply-personal-info

#### U përgjigja me informacion

> Në varësi të llojit të informacionit që keni ndarë, mund t'ju duhet të merrni masa të menjëhershme.

Çfarë lloj informacioni keni ndarë?

##### Options

- [Kam ndarë informacionin konfidencial të llogarisë](#account-security_end)
- [Kam ndarë informacionin publik](#share)
- [Nuk jam i sigurt se sa i ndjeshëm ishte informacioni dhe kam nevojë për ndihmë](#help_end)

### share

#### Ndarja e informacionit ose mesazhit tuaj të dyshimtë

> Ndarja e mesazhit tuaj të dyshimtë mund të ndihmojë në mbrojtjen e kolegëve dhe komunitetit tuaj, të cilët gjithashtu mund të preken. Gjithashtu mund të dëshironi t'i drejtoheni për ndihmë dikujt që keni besim për t'ju këshilluar nëse mesazhi është i rrezikshëm. Merrni parasysh ndarjen e mesazhit me organizatat që mund t'i analizojnë ato.
>
> Për të ndarë mesazhin tuaj të dyshimtë, sigurohuni që të përfshini vetë mesazhin si dhe informacionet rreth dërguesit. Nëse mesazhi ishte një e-mail, ju lutemi sigurohuni që të përfshini e-mailin e plotë, duke përfshirë titujt duke përdorur [udhëzuesin vijues nga Qendra e Reagimit ndaj Incidenteve Kompjuterike në Luksemburg (CIRCL)](https://www.circl.lu/pub/tr-07/).

Keni nevojë për ndihmë të mëtejshme?

##### Options

- [Po, kam nevojë për më shumë ndihmë](#help_end)
- [Jo, e kam zgjidhur problemin tim](#resolved_end)

### device-security_end

#### Shkarkova një skedar

> Në rast se disa skedarë janë shkarkuar në pajisjen tuaj, pajisja juaj mund të jetë në rrezik!

Ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin. Më pas, ju lutemi [ndani mesazhin tuaj të dyshimtë](#share).

[orgs](:organisations?services=device_security)

### account-security_end

#### Kam futur kredencialet

> Në rast se keni futur kredencialet tuaja, ose keni qenë viktimë e një sulmi të ashquajtur “cross-site script attack”, llogaritë tuaja mund të jenë në rrezik!
>
> Nëse mendoni se llogaria juaj është e komprometuar, rekomandohet që të ndiqniedhe sekuencën e veprimeve të Veglërisë së Ndihmës së Parë Digjitale në [llogaritë e komprometuara](/sq/topics/account-access-issues).
>
> Ju gjithashtu duhet të informoni komunitetin tuaj për këtë fushatë “phishing” dhe të ndani mesazhin e dyshimtë me organizatat që mund ta analizojnë atë.

A dëshironi të ndani informacion mbi mesazhin që keni marrë apo keni nevojë për ndihmë të mëtejshme më parë?

##### Options

- [Dua të ndaj mesazhin e dyshimtë](#share)
- [Kam nevojë për më shumë ndihmë për të siguruar llogarinë time](#account_end)
- [Kam nevojë për më shumë ndihmë për të analizuar mesazhin](#analysis_end)

### suspicious-device_end

#### Nuk jam i sigurt se çfarë ndodhi

> Nëse keni klikuar në një lidhje dhe nuk jeni të sigurt se çfarë ka ndodhur, pajisja juaj mund të jetë infektuar pa e vënë re ju. Nëse dëshironi të eksploroni këtë mundësi, ose nëse keni ndjenjën se pajisja juaj mund të jetë e infektuar, ndiqni sekuencën e veprimeve ["Pajisja ime po reagon në mënyrë të dyshimtë"](/sq/topics/device-acting-suspiciously).

Nëse keni nevojë për ndihmë të menjëhershme sepse pajisja juaj po reagon në mënyrë të dyshimtë, mund të kontaktoni organizatat e mëposhtme që mund t'ju mbështesin. Më pas, ju lutemi [ndani mesazhin tuaj të dyshimtë](#share).

[orgs](:organisations?services=device_security)

### help_end

#### Unë kam nevojë për më shumë ndihmë

> Duhet të kërkoni ndihmë nga kolegët tuaj ose të tjerët për të kuptuar më mirë rreziqet nga informacioni që keni ndarë. Njerëz të tjerë në organizatën ose rrjetin tuaj mund të kenë marrë gjithashtu kërkesa të ngjashme.

Ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin. Më pas, ju lutemi [ndani mesazhin tuaj të dyshimtë](#share).

[orgs](:organisations?services=digital_support)

### account_end

#### Kam nevojë për më shumë ndihmë për të siguruar llogarinë time

Nëse llogaria juaj është komprometuar dhe keni nevojë për ndihmë për ta siguruar atë, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

[orgs](:organisations?services=account)

### analysis_end

#### Më duhet më shumë ndihmë për analizimin e mesazhit

Organizatat e mëposhtme mund të marrin mesazhin tuaj të dyshimtë dhe ta hetojnë atë më tej për ju:

[orgs](:organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

#### Po, çështja ime është zgjidhur

Shpresojmë se ky udhëzues i DFAK ishte i dobishëm. Ju lutemi jepni komente [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

### Final Tips

Rregulli i parë që duhet mbajtur mend: Asnjëherë mos jepni asnjë informacion personal në e-mail ose mjete të tjera për shkëmbim mesazhesh. Asnjë institucion, bankë apo ndonjë institucion tjetër nuk do ta kërkojë kurrë këtë informacion përmes e-mailit, platformave të rrjeteve sociale ose aplikacioneve për dërgimin e mesazheve. Mund të mos jetë gjithmonë e lehtë të dallosh nëse një e-mail, mesazh ose faqe interneti është legjitime, por ka disa këshilla që mund t'ju ndihmojnë të vlerësoni e-mailin që keni marrë.

- Ndjenja e urgjencës: e-mailet e dyshimta zakonisht paralajmërojnë për një ndryshim të papritur në llogari dhe ju kërkojnë të veproni menjëherë për të verifikuar llogarinë tuaj.
- Në pjesën kryesore të një e-maili, mund të shihni pyetje që ju kërkojnë të "verifikoni" ose "përditësoni llogarinë tuaj" ose "Mospërditësimi i të dhënave tuaja do të rezultojë me pezullimin e llogarisë". Zakonisht është e sigurt të supozohet se asnjë organizatë e besueshme, së cilës i keni dhënë informacionin tuaj, nuk do t'ju kërkojë kurrë ta rifusni atë, ndaj mos bini pre e këtij kurthi.
- Kini kujdes nga mesazhet, bashkëngjitjet, lidhjet dhe faqet e hyrjes të panjohura.
- Kini kujdes nga gabimet drejtshkrimore dhe gramatikore.
- Në e-maile, klikoni për të parë adresën e plotë të dërguesit, jo vetëm emrin e shfaqur.
- Kini parasysh lidhjet e shkurtuara - meqenëse nuk mund të kontrolloni destinacionin përfundimtar, ato mund t'ju çojnë në një faqe interneti keqdashëse.
- Kur e vendosni treguesin e miut mbi një lidhje, URL-ja aktuale tek e cila ju dërgon lidhja shfaqet në një dritare të vogël mbi lidhjen ose në fund të dritares së shfletuesit tuaj.
- Titujt e e-mail-eve duke përfshirë vlerën "Nga:" mund të manipulohen me kujdes që të duken sikur janë të ligjshme. Duke ekzaminuar titujt SPF dhe DKIM, ju mund të tregoni përkatësisht nëse një adresë IP lejohet (ose nuk lejohet) të dërgojë e-mail në emër të domenit të dërguesit dhe nëse titujt ose përmbajtja janë ndryshuar gjatë tranzitit. Në një e-mail të ligjshëm, vlerat e [SPF](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what- is-spf) dhe [DKIM](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is -dkim) duhet të jenë gjithmonë 'PASS', përndryshe e-mail-it nuk mund të besohet. Arsyeja është se e-maili është i falsifikuar ose, në raste të rralla, serveri i e-mail-it nuk është konfiguruar saktë.
- [Nënshkrimet dixhitale](https://www.gnupg.org/gph/en/manual/x135.html) mund të na tregojnë nëse një e-mail është dërguar nga dërguesi legjitim dhe nëse është modifikuar apo jo gjatë rrugës. Nëse e-maili është nënshkruar me OpenPGP, kontrolloni nëse nënshkrimi është verifikuar ose jo. Për të verifikuar një nënshkrim, do t'ju duhet të instaloni OpenPGP dhe të importoni çelësin publik të lidhur me ID-në në nënshkrimin e mesazhit. Shumica e klientëve modernë të e-mailit që mbështesin nënshkrimet digjitale do të automatizojnë verifikimin për ju dhe do t'ju tregojnë përmes ndërfaqes së tyre të përdoruesit nëse një nënshkrim është verifikuar ose jo.
- Një llogari e komprometuar mund të dërgojë një e-mail ose mesazh keqdashës që mund të verifikojë të gjitha kushtet e mësipërme dhe të duket legjitim. Megjithatë, zakonisht përmbajtja e mesazhit do të jetë e pazakontë. Nëse përmbajtja e mesazhit të e-mailit duket e çuditshme, është gjithmonë një ide e mirë të kontrolloni këtë me dërguesin legjitim përmes një kanali tjetër komunikimi përpara se të ndërmerrni ndonjë veprim.
- Është gjithmonë një praktikë e mirë që e-mailet tuaja t’i lexoni dhe t’i shkruani si tekst i thjeshtë. E-mailet e bazuara në HTML mund të shfaqen në një mënyrë që e fsheh kodin ose URL-të keqdashëse. Mund të gjeni udhëzime se si të çaktivizoni HTML në klientë të ndryshëm të e-mail-it në [këtë faqe interneti](https://useplaintext.email/).
- Përdorni versionin e fundit të sistemit operativ në telefonin ose kompjuterin tuaj (kontrolloni versionin për [Android](https://en.wikipedia.org/wiki/Android_version_history), [iOS](https://en.wikipedia.org). /wiki/iOS_version_history), [macOS](https://en.wikipedia.org/wiki/MacOS_version_history) dhe [Windows](https://en.wikipedia.org/wiki/Microsoft_Windows)).
- Përditësoni sa më shpejt të jetë e mundur sistemin tuaj operativ dhe të gjitha aplikacionet/programet që i keni instaluar, veçanërisht ato që marrin informacion (shfletuesit, aplikacionet/programet e mesazheve dhe bisedave, klientët e e-mail-it, etj.). Hiqni të gjitha aplikacionet/programet që nuk ju nevojiten.
- Përdorni një shfletues të besueshëm (p.sh. Mozilla Firefox). Rritni sigurinë e shfletuesit tuaj duke rishikuar zgjerimet/shtesat e instaluara në shfletuesin tuaj. Lërini vetëm ata që të cilave u besoni (për shembull: [Privacy Badger](https://privacybadger.org/), [uBlock Origin](https://ublockorigin.com), [Facebook Container](https://addons.mozilla.org/en-US/firefox/addon/facebook-container/), [Cookie AutoDelete](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete), [NoScript](https://noscript.net/)).
- Bëni kopje rezervë të rregullt të sigurt të informacionit tuaj.
- Mbroni llogaritë tuaja me fjalëkalime të forta, me vërtetim me 2 faktorë dhe me cilësime të sigurta.

### Resources

Këtu janë një sërë burimesh për të dalluar mesazhet e dyshimta dhe për të shmangur mashtrimin.

- [Citizen Lab: Komunitetet në rrezik - Kërcënimet digjitale të synuara kundër shoqërisë civile](https://targetedthreats.net)
- [Vetë-mbrojtja nën mbikëqyrje: Si të shmangni sulmet “phishing”](https://ssd.eff.org/en/module/how-avoid-phishing-attacks)
- [Mjet i analizës së pjesës kryesore të mesazheve i Google](https://toolbox.googleapps.com/apps/messageheader/)
