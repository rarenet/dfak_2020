---
layout: topic
title: "Unë mendoj se jam duke u mbikëqyrur"
author: Carlos Guerra, Peter Steudtner
language: sq
summary: "Nëse mendoni se mund të jeni shënjestër i mbikëqyrjes digjitale, kjo rrjedhë pune do t'ju udhëheqë përmes pyetjeve për të identifikuar treguesit e mundshëm të mbikëqyrjes."
date: 2023-05
permalink: topics/surveilled
parent: Home
order: 11
---

# Mendoj se jam duke u mbikëqyrur

Kohët e fundit, kemi parë shumë raste të reja të mbikëqyrjes ndaj aktorëve të hapësirës civile, nga përdorimi i programeve për përndjekje (“stalkerware”) nga partnerët abuzues dhe mbikëqyrje të punonjësve nga kompanitë, deri tek mbikëqyrja masive nga shteti dhe fushatat e synuara të spiunimit (“spyware”) kundër aktivistëve dhe gazetarëve. Duke pasur parasysh spektrin e gjerë të teknikave dhe të precedentëve të vëzhguar, është e pamundur të mbulohet çdo aspekt i mbikëqyrjes, veçanërisht për mbështetjen emergjente.

Thënë kështu, kjo rrjedhë pune do të përqendrohet në rastet e zakonshme që shihen në sferën digjitale dhe që lidhen me pajisjet e përdoruesve fundorë dhe llogaritë në internet që përfaqësojnë urgjencat me përgjigje vepruese. Kur nuk ka një zgjidhje të mundshme, atëherë nuk do ta konsiderojmë atë një urgjencë dhe nuk do të mbulohet gjerësisht (në vend të kësaj, do të jepen referenca).

Disa nga rastet që nuk i mbulon kjo rrjedhë pune janë:

- Mbikëqyrja fizike. Nëse keni frikë se jeni në shënjestër të mbikëqyrjes fizike dhe dëshironi mbështetje, mund të kontaktoni organizatat që fokusohen në sigurinë fizike në [faqen mbështetëse](/sq/support) të Veglërisë së Ndihmës së Parë Digjitale.
- Mbikëqyrja masive me pajisje si kamerat CCTV në hapësira publike, korporative dhe private
- Defektet e harduerit që nuk varen nga lidhja e internetit

Një mosmarrje e përgjegjësisë e dytë e rëndësishëm është se në ditët moderne të gjithë po mbikëqyren si parazgjedhje në një farë mase:

- Infrastruktura e celularëve është krijuar për të mbledhur një sasi masive të dhënash për veten tonë, të cilat mund të përdoren nga disa aktorë për të mësuar detaje si vendndodhjen tonë, rrjetin e kontakteve, etj.
- Shërbimet online si rrjetet sociale dhe ofruesit e postës elektronike (e-mailit) mbledhin gjithashtu një sasi të madhe të dhënash rreth nesh, të cilat mund të lidhen për shembull me fushatat reklamuese për të na shfaqur reklama që lidhen me temat e postimeve tona apo edhe mesazhe të drejtpërdrejta (një-për-një).
- Gjithnjë e më shumë vende kanë shumë sisteme të ndërlidhura që përdorin identitetin tonë dhe mbledhin informacion mbi ndërveprimet tona me institucione të ndryshme publike, duke mundësuar krijimin e një “profili” duke u bazuar në kërkesat e dokumenteve për informacionin tatimor apo shëndetësor.

Këta shembuj të mbikëqyrjes masive, plus precedentët e shumtë të operacioneve të synuara, krijojnë ndjenjën se të gjithë (veçanërisht në hapësirën e aktivistëve) po mbikëqyren, gjë që mund të çojë në paranojë dhe të ndikojë në aftësinë tonë emocionale për t'u përballur me kërcënimet aktuale me të cilat mund të përballemi. Thënë kështu, ka raste specifike ku - në varësi të profilit tonë të rrezikut, punës që bëjmë dhe kapaciteteve të kundërshtarëve tanë të mundshëm - ne mund të kemi krijuar dyshime se jemi duke u mbikëqyrur. Nëse rasti juaj është i tillë, vazhdoni të lexoni.

Një mosmarrje e përgjegjësisë e tretë, shumë skema mbikëqyrjeje (veçanërisht ato më të synuara), zakonisht lënë pak ose aspak tregues të qasshëm për viktimat, duke e bërë zbulimin të vështirë dhe ndonjëherë të pamundur. Ju lutemi, mbani këtë në mend gjatë lundrimit në këtë rrjedhë punë dhe nëse ende besoni se rasti juaj nuk është i mbuluar ose është më kompleks, ju lutemi kontaktoni organizatat e shënuara në fund të rrjedhës së punës.

Si konsideratë e fundit, ju lutemi mbani në mend se përpjekjet e mbikëqyrjes mund të jenë masive ose të synuara. Operacionet masive të mbikëqyrjes zakonisht mbulojnë popullsi të tëra ose pjesë të mëdha të tyre, si p.sh. të gjithë përdoruesit e një platforme, të gjithë njerëzit nga një rajon specifik, etj. Këto zakonisht mbështeten më shumë në teknologji dhe zakonisht janë më pak të rrezikshme sesa mbikëqyrja e synuar, e cila synon një individ ose një numër të vogël individësh, kërkon burime të dedikuara (para, kohë dhe/ose kapacitet) dhe zakonisht është më e rrezikshme se mbikëqyrja masive, duke pasur parasysh motivimet pas vendosjes së burimeve për të monitoruar aktivitetin dhe komunikimin e personave të veçantë.

Nëse keni gjetur ndonjë tregues se jeni të mbikëqyrur, klikoni fillo për të filluar me rrjedhën e punës.

## Workflow

### start

#### What kind of issue are you experiencing?

> Nëse jeni këtu, me siguri keni arsye të mendoni se komunikimi juaj, vendndodhja ose aktivitete të tjera tuaja në internet mund të mbikëqyren. Më poshtë do të gjeni një sërë treguesish të zakonshëm të aktivitetit të dyshimtë në lidhje me mbikëqyrjen. Klikoni në rastin tuaj për të vazhduar.

Duke pasur parasysh informacionin e dhënë në hyrje, me çfarë lloj problemi po përballeni?

##### Options

- [Kam gjetur një pajisje gjurmuese afër meje](#tracking-device-intro)
- [Pajisja ime po reagon në mënyrë të dyshimtë](#device-behaviour-intro)
- [Po ridrejtohem në faqet e internetit të pambrojtura (http) për instalimin e përditësimeve ose aplikacioneve](#suspicious-redirections-intro)
- [Po marr mesazhe sigurie dhe alarme nga shërbimet e besuara](#security-alerts-intro)
- [Kam gjetur një pajisje të dyshimtë të lidhur me kompjuterin ose rrjetin tim](#suspicious-device-intro)
- [Kundërshtari i di informacionet që i kam ndarë privatisht përmes e-mailit, aplikacionet për dërgimin e mesazheve ose të ngjashme](#leaked-internet-info-intro)
- [Informacionet konfidenciale të ndara nëpërmjet telefonatave ose mesazheve SMS duket se kanë rrjedhur te kundërshtari (veçanërisht gjatë ngjarjeve të ndjeshme)](#leaked-phone-info-intro)
- [Njerëzit përreth jush janë shënjestruar me sukses nga masat e mbikëqyrjes](#peer-compromised-intro)
- [Diçka tjetër / Nuk e di](#other-intro)

### tracking-device-intro

#### Kam gjetur një pajisje gjurmuese afër meje

> Nëse keni gjetur një pajisje të papritur pranë jush dhe besoni se është një pajisje mbikëqyrjeje, hapi i parë do të ishte konfirmimi se me të vërtetë pajisja është aty për t'ju gjurmuar dhe se nuk kryen funksione të tjera përkatëse. Me prezantimin e pajisjeve të Internetit të Gjërave (IoT), është më e zakonshme të gjejmë pajisje që kryejnë një sërë veprimesh që mund t'i harrojmë pasi t'i përdorim për një kohë. Një nga hapat e parë për t'u siguruar që është një pajisje mbikëqyrjeje është inspektimi i pajisjes për informacion mbi markën, modelin, etj. Disa pajisje të zakonshme gjurmuese që janë në dispozicion me çmime të ulëta në treg përfshijnë Apple Airtags, Samsung Galaxy SmartTag ose gjurmuesit Tile. Këto pajisje shiten për qëllime legjitime, si p.sh. për të ndihmuar në gjetjen e sendeve personale. Megjithatë, aktorët keqdashës mund t'i abuzojnë ato për të gjurmuar njerëz të tjerë pa pëlqimin e tyre.
>
> Për të kontrolluar nëse jemi duke u ndjekur nga një gjurmues i papritur, përveç gjetjes së vendndodhjes së tyre fizike, mund të jetë e dobishme të shqyrtojmë disa zgjidhje specifike:
>
> - Për Apple Airtags, në iOS [duhet të merrni një njoftim automatik pasi pajisja e panjohur të jetë zbuluar për ca kohë](https://support.apple.com/en-us/HT212227).
> - Për Android, ekziston një [aplikacion për të kontrolluar Airtags-at që janë në afërsi](https://play.google.com/store/apps/details?id=com.apple.trackerdetect).
> - Për pajisjet Tile, [aplikacioni Tile](https://www.tile.com/download) ka një veçori për të kontrolluar për gjurmues të panjohur.
> - Për Samsung Galaxy SmartTags, mund të përdorni [aplikacionin SmartThings](https://www.samsung.com/us/smartthings/) për një veçori të ngjashme.
>
> Një lloj tjetër pajisjeje që mund të gjeni janë gjurmuesit GPS. Këto regjistrojnë shpesh vendndodhjen e objektivit dhe në disa raste mund të transmetojnë informacionin përmes një linje telefonike, ose mund t'i ruajnë vendndodhjet në një formë të memories së brendshme që mund të merret fizikisht më vonë për t'u shkarkuar dhe analizuar. Nëse gjeni një pajisje si kjo, të kuptuarit e mënyrës se si funksionon pajisja është thelbësore për të pasur një kuptim më të mirë se kush e vendosi pajisjen dhe se si po e marrin informacionin.
>
> Për çdo lloj pajisje mbikëqyrjeje që gjeni, duhet të dokumentoni gjithçka rreth saj: markën, modelin, nëse lidhet me rrjetin pa tel (wireless), ku saktësisht e keni gjetur, ndonjë emër të pajisjes në aplikacionin përkatës, etj.

Pas identifikimit të pajisjes, ka disa strategji që mund të dëshironi të ndiqni. Çfarë doni të bëni me pajisjen?

##### Options

- [Dua ta çaktivizoj](#tracking-disable-device)
- [Do të doja ta përdorja për ta mashtruar operatorin](#tracking-use-device)

### tracking-disable-device

#### Dua ta çaktivizoj

> Në varësi të llojit dhe markës së pajisjes që keni gjetur, mund të jeni në gjendje ta fikni ose ta çaktivizoni plotësisht. Në disa raste kjo mund të bëhet përmes aplikacioneve për gjurmim si ato të përshkruara në hapin e mëparshëm, por edhe përmes ndërprerësve fizikë të energjisë, apo edhe duke dëmtuar pajisjen. Ju lutemi, mbani në mend se nëse doni të kryeni një hetim më të thellë rreth këtij incidenti, çaktivizimi i pajisjes mund të ndryshojë provat përkatëse që janë ruajtur në pajisje.

Dëshiron të ndërmarrësh veprime të mëtejshme për të identifikuar pronarin e pajisjes? (_ju lutemi vini re se do të duhet ta mbani pajisjen gjurmuese online për ta bërë këtë_)

##### Options

- [Po](#identify-device-owner)
- [Jo](#pre-closure)

### tracking-use-device

#### Do të doja ta përdorja për ta mashtruar operatorin

> Një strategji e zakonshme për t'u marrë me pajisjet gjurmuese është t'i bëni ato të gjurmojnë diçka ndryshe nga ajo për të cilën ishin paraparë. Për shembull, mund ta lini atë në një pozicion statik të sigurt ndërsa shkoni në vende të tjera, ose madje mund ta bëni atë të gjurmojë një objektiv tjetër në lëvizje që nuk ka lidhje me ju. Ju lutemi mbani në mend se në disa nga këta skenarë mund të humbni qasjen në pajisje, e po ashtu nëse e bëni gjurmuesin të ndjekë një person tjetër ose të gjurmojë një vendndodhje specifike mund të ketë pasoja sigurie dhe ligjore.

Dëshiron të ndërmarrësh veprime të mëtejshme për të identifikuar pronarin e pajisjes? (_ju lutemi vini re se do të duhet ta mbani pajisjen gjurmuese online për ta bërë këtë_)

##### Options

- [Po](#identify-device-owner)
- [Jo](#pre-closure)

### identify-device-owner

#### Unë dua të identifikoj pronarin e pajisjes

> Identifikimi i pronarit të pajisjes gjurmuese mund të jetë i vështirë në disa skenarë dhe kjo do të varet kryesisht nga lloji i pajisjes së përdorur, andaj ju rekomandojmë të bëni kërkime në mënyrë specifike për pajisjen gjurmuese që keni gjetur. Disa shembuj janë:
>
> - Për gjurmuesit me valë të kategorisë së konsumatorit: - Si shfaqen në aplikacionet e kontrollit? - Kur keni marrë një njoftim? - A ka ndonjë mënyrë për të nxjerrë më shumë të dhëna nga pajisja që mund të përmbajnë informacione relevante?
> - Për gjurmues më të dedikuar, si gjurmuesit GPS: - A kanë memorie të brendshme? Disa modele kanë një kartë SD që mund të nxirret për të parë se kur filloi kapja e të dhënave, ose ndonjë informacion tjetër relevant. - A kanë linjë telefonike? A mund të nxjerrësh një kartë SIM nga pajisja dhe të shikosh nëse mund ta provosh në një telefon tjetër për të marrë numrin? Me këtë informacion, a mund të zbuloni se kush është pronari i asaj linje telefonike?
> - Në përgjithësi - Kërkoni për çdo shenjë fizike, si p.sh. emrat e shkruar në pajisje, numrat e inventarit, etj. - Nëse pajisja lidhet me rrjetin, a ka ndonjë emër specifik pajisjeje që mund të përfshijë të dhëna identifikimi? - Kush mund të hyjë në vendin ku e gjetët pajisjen? Duke ditur vendin, kur mund të ishte vendosur aty?

Dëshiron të ndërmarrësh veprime ligjore kundër pronarit të pajisjes gjurmuese?

##### Options

- [Po](#legal-action-device-owner)
- [Jo](#pre-closure)

### legal-action-device-owner

#### Unë dua të ndërmarr veprime ligjore kundër pronarit të pajisjes

> Në rast se dëshironi të ndërmerrni veprime ligjore kundër pronarit të pajisjes për mbikëqyrje, do t'ju duhet të kontrolloni se si duket procesi në juridiksionin tuaj. Në disa raste do të nevojitet kërkohet një avokat, në raste të tjera do të mjaftojë të shkoni në stacionin e policisë për të nisur procesin. Këshilla e vetme e përbashkët për të gjitha rastet është se çdo rast do të jetë po aq i fortë sa provat që i mblidhni rreth tij. Një fillim i mirë është të ndiqni këshillat e dhëna në hapat e mëparshëm të kësaj rrjedhe pune se çfarë të dokumentoni. Më shumë informacion dhe shembuj mund të gjeni në udhëzuesin e Veglërisë së Ndihmës së Parë Digjitale për [dokumentimin e urgjencave digjitale](/sq/documentation).

A keni nevojë për ndihmë për të ndërmarrë veprime ligjore kundër pronarit të pajisjes gjurmuese?

##### Options

- [Po, kam nevojë për mbështetje ligjore](#legal_end)
- [Jo, mendoj se i kam zgjidhur problemet e mia](#resolved_end)
- [Jo, por do të doja të konsideroja skenarë të tjerë të mbikëqyrjes](#pre-closure)

### device-behaviour-intro

#### Pajisja ime po reagon në mënyrë të dyshimtë

> Një nga teknikat më të zakonshme të mbikëqyrjes së synuar deri më sot është infektimi i telefonave ose kompjuterëve me “spyware” - “malware” të krijuar për të monitoruar dhe transmetuar aktivitetin te të tjerët. Programet e spiunimit (“Spyware”) mund të vendosen nga aktorë të ndryshëm nëpërmjet metodave të ndryshme, si për shembull nga partnerët abuzues që shkarkojnë manualisht një aplikacion spiunimi (zakonisht i referuar si “stalkerware” ose “spouseware”) në pajisje, ose nga krimi i organizuar duke dërguar lidhje “phishing” për të shkarkuar një aplikacion.
>
> Disa shembuj të treguesve të mbikëqyrjes mund të përfshijnë aplikacione të dyshimta që keni gjetur në pajisjen tuaj me leje për mikrofonin, kamerën, qasjen në rrjet, etj. Nëse vëreni se është ndezur treguesi i kamerës së internetit pa përdorur ndonjë aplikacion që duhet ta përdorë atë, ose nëse vëreni se “rrjedh” përmbajtja e skedarëve.
>
> Për raste të tilla, mund t'i referoheni rrjedhës së punës së Veglërisë së Ndihmës së Parë Digjitale [Pajisja ime po reagon në mënyrë të dyshimtë](/sq/topics/device-acting-suspiciously).

Çfarë do të dëshironit të bënit?

##### Options

- [Më dërgo te rrjedha e punës për pajisjen që reagon në mënyrë të dyshimtë](/sq/topics/device-acting-suspiciously)
- [Më dërgo në hapin tjetër këtu](#pre-closure)

### suspicious-redirections-intro

#### Po ridrejtohem në faqet e internetit të pambrojtura (http) për instalimin e përditësimeve ose aplikacioneve

> Edhe nëse kjo është e pazakontë, ndonjëherë ne shohim alarme sigurie kur përpiqemi të përditësojmë aplikacionet apo edhe sistemin operativ të pajisjeve tona. Ka shumë arsye për këtë, dhe ato mund të jenë legjitime ose keqdashëse. Disa pyetje udhëzuese që duhet t'ia bëjmë vetes janë:
>
> - A ka pajisja ime datë dhe orë të saktë? Mënyra se si pajisjet tona u besojnë serverëve për të funksionuar më të sigurt varet nga kontrollimi i certifikatave të sigurisë që janë të vlefshme brenda një afati kohor të caktuar. Nëse, për shembull, kompjuteri juaj është konfiguruar me një vit më të hershëm, këto kontrolle sigurie do të dështojnë për një numër faqesh interneti dhe shërbimesh. Nëse data dhe ora juaj janë të sakta, mund të sugjerojë gjithashtu se ofruesi nuk i ka përditësuar certifikatat e tij ose ka diçka të dyshimtë që po ndodh. Sidoqoftë, është një rregull i mirë që të mos përditësoni ose të përdorni kurrë aplikacionet në pajisjen tuaj nëse i shihni këto paralajmërime sigurie ose ridrejtime në faqe të tjera.
> - A u shfaq problemi pas një ngjarjeje të pazakontë, si p.sh. pas klikimit të një reklame, instalimit të një aplikacioni ose hapjes së një dokumenti? Shumë sulme mbështeten në imitimin e proceseve si instalimet dhe përditësimet legjitime të softuerit, kështu që në këtë rast ndoshta pajisja juaj e kupton këtë dhe jep një paralajmërim ose e ndalon plotësisht procesin.
> - A ndodh procesi me të cilin keni probleme në një mënyrë të pazakontë, p.sh. normalisht procesi i përditësimit të atij aplikacioni ndjek një proces të ndryshëm nga ai që po shihni?
>
> Sidoqoftë, mos harroni të dokumentoni gjithçka në mënyrën më të detajuar të mundshme, veçanërisht nëse dëshironi të merrni ndihmë të specializuar, të kryeni një hetim më të thellë ose të ndërmerrni ndonjë veprim ligjor.
>
> Pothuajse në të gjitha rastet, adresimi i shkakut rrënjësor të problemit do të jetë i mjaftueshëm përpara se ta ktheni pajisjen dhe funksionimin në normalitet. Megjithatë, mund të ketë probleme të mëtejshme nëse vërtet keni instaluar ose përditësuar një aplikacion (ose sistemin operativ) në rrethana të dyshimta.

A keni klikuar dhe shkarkuar ose instaluar ndonjë softuer ose përditësim në kushte të dyshimta?

##### Options

- [Po](#suspicious-software-installed)
- [Jo](#pre-closure)

### suspicious-software-installed

#### Kam shkarkuar ose instaluar softuer në kushte të dyshimta

> Në shumicën e rasteve, sulmet që rrëmbejnë proceset e instalimit ose përditësimit të aplikacioneve ose sistemeve operative kanë për qëllim instalimin e programeve keqdashëse (“malware”) në sistem. Nëse dyshoni se skenari që po ndodh me ju është i tillë, ju rekomandojmë të shkoni te rrjedha e punës "Pajisja ime po reagon në mënyrë të dyshimtë” të Veglërisë së Ndihmës së Parë Digjitale.

Çfarë do të dëshironit të bënit?

##### Options

- [Më dërgo te rrjedha e punës për pajisjen që reagon në mënyrë të dyshimtë](/sq/topics/device-acting-suspiciously)
- [Më dërgo në hapin tjetër këtu](#pre-closure)

### security-alerts-intro

#### Po marr mesazhe sigurie dhe alarme nga shërbimet e besuara

> nëse po merrni mesazhe ose sinjalizime njoftimesh nga shërbimet e postës elektronike (e-mailit), platformat e rrjeteve sociale ose ndonjë shërbim tjetër në internet që përdorni në të vërtetë dhe ato kanë të bëjnë me çështje sigurie, si hyrje të reja të dyshimta nga pajisje ose vendndodhje të reja, fillimisht mund të kontrolloni nëse mesazhi është legjitim ose nëse është një mesazh “phishing”. Për ta bërë këtë, ju rekomandojmë të kontrolloni rrjedhën e punës [Kam marrë një mesazh të dyshimtë](/sq/topics/suspicious-messages) së Veglërisë së Ndihmës së Parë Digjitale përpara se të shkoni më tej.
>
> Nëse mesazhi është legjitim, pyetja tjetër që duhet t'i bëni vetes është nëse ka një arsye pse ky mesazh mund të jetë legjitim. Për shembull, nëse përdorni një shërbim VPN ose Tor dhe lundroni në versionin ueb të një shërbimi, ai do të mendojë se gjendeni në vendin e serverit tuaj VPN ose të nyjës së daljes së Tor, duke shkaktuar një alarm për një hyrje të re të dyshimtë, kur në fakt jeni ju që përdor një zgjidhje anashkaluese.
>
> Një gjë tjetër e dobishme për të pyetur veten është se çfarë lloj alarmi sigurie po marrim: a është për dikë që përpiqet të hyjë në llogarinë tuaj apo se ka pasur sukses në këtë? Në varësi të përgjigjes, përgjigja e kërkuar mund të jetë shumë e ndryshme.
>
> Nëse kemi marrë një njoftim për përpjekje të pasuksesshme të hyrjes dhe kemi një fjalëkalim të mirë (të gjatë, që nuk e kemi ndarë me shërbime të tjera të pasigurta, etj.) dhe përdorim vërtetimin me shumë faktorë (i njohur edhe si MFA ose 2FA), nuk duhet të shqetësohemi shumë se llogaria jonë mund të komprometohet. Pavarësisht se sa serioz është kërcënimi për komprometimin e llogarisë, është gjithmonë mirë të kontrolloni çdo regjistër (log) për aktivitetet e fundit ku zakonisht mund të rishikojmë historikun tonë të identifikimit, duke përfshirë vendndodhjen dhe llojin e pajisjes.
>
> Nëse shihni ndonjë gjë të dyshimtë në regjistrat e aktiviteteve tuaja, strategjitë më të zakonshme, në varësi të llogarisë, përfshijnë:
>
> - Ndryshimin e fjalëkalimeve
> - Mundësimin e MFA-së
> - Përpjekjen për të shkëputur të gjitha pajisjet nga llogaria
> - Dokumentimin e treguesve të tentativës së hakimit (në rast se duam të kërkojmë ndihmë, të hetojmë më tej ose të fillojmë një proces ligjor)
>
> Një pyetje tjetër e rëndësishme për të ndihmuar në vlerësimin e këtyre njoftimeve është nëse i ndajmë pajisjet ose qasjen në llogari me dikë tjetër që mund të ketë shkaktuar sinjalizimin. Nëse është kështu, kini kujdes me nivelin e qasjes që u jepni të tjerëve në informacionin tuaj dhe përsëritni hapat e mësipërm nëse doni të revokoni qasjen për këtë person të tretë.

Nëse besoni se ka kërcënime të tjera të mundshme të përmendura në fillimin e rrjedhës së punës të cilat dëshironi t’i kontrolloni, ose nëse keni një rast më kompleks dhe dëshironi të kërkoni ndihmë, klikoni “Më tutje”.

##### Options

- [Më tutje](#pre-closure)

### suspicious-device-intro

#### Kam gjetur një pajisje të dyshimtë të lidhur me kompjuterin ose rrjetin tim

> Nëse kemi gjetur një pajisje të dyshimtë, gjëja e parë që duhet të pyesim veten është nëse kjo pajisje është keqdashëse apo nëse gjendet aty ku duhe të jetë. Kjo sepse në vitet e fundit numri i pajisjeve me aftësi komunikimi në shtëpitë dhe vendet tona të punës është rritur ndjeshëm: shtypës, drita inteligjente, lavatriçe, termostate, etj. Duke pasur parasysh numrin e madh të këtyre pajisjeve, mund të shqetësohemi për një pajisje që nuk e mbajmë mend se e kemi konfiguruar ose që është konfiguruar nga dikush tjetër, por nuk është me qëllim të keq.
>
> Në rast se nuk e dini nëse pajisja që e keni gjetur është me qëllim të keq, një nga gjërat e para që duhet të bëni është të merrni informacione rreth saj: markën, modelin, nëse është e lidhur me rrjetin me tel ose me atë me valë, nëse thotë çfarë bën, nëse është e ndezur, etj. Nëse ende nuk e dini se çfarë bën pajisja, mund ta përdorni këtë informacion për të kërkuar në ueb dhe për të mësuar më shumë se çfarë është dhe çfarë bën ajo pajisje.

Pas kontrollimit të pajisjes, a mendoni se është e pakomprometuar)?

##### Options

- [Ishte një pajisje e pakomprometuar](#pre-closure)
- [Kam kontrolluar, por ende nuk e di se çfarë bën pajisja ose nëse është me qëllim të keq](#suspicious-device2)

### suspicious-device2

#### Kam kontrolluar, por ende nuk e di se çfarë bën pajisja ose nëse është me qëllim të keq

> Në rast se jeni ende i shqetësuar për pajisjen pas një kontrolli fillestar, duhet të merrni parasysh mundësinë shumë të vogël që ajo të jetë një pajisje mbikëqyrjeje. Zakonisht këto pajisje monitorojnë aktivitetin në rrjetin me të cilin janë lidhur, ose madje mund të regjistrojnë dhe të transmetojnë audio dhe/ose video ashtu siç bëjnë programet “spyware”. Është e rëndësishme të kuptohet se ky skenar është shumë i pazakontë dhe zakonisht lidhet me profile me rrezik shumë të lartë. Megjithatë, nëse kjo ju shqetëson, disa gjëra që mund të bëni përfshijnë:
>
> - Shkëputjen e pajisjes nga rrjeti/energjia dhe kontrollimin nëse të gjitha shërbimet e parapara përreth funksionojnë pa probleme. Pas kësaj mund pajisjen tuaj mund ta analizojë dikush me përvojë.
> - Hulumtimin e mëtejshëm dhe kërkimin e ndihmës (kjo në përgjithësi përfshin teste më të avancuara si hartëzimin e rrjetit ose kërkimin e sinjaleve të pazakonta).
> - Si një masë parandaluese për të zbutur çdo mbikëqyrje të rrjetit nëse e lini pajisjen të lidhur, duke përdorur VPN, Tor ose një mjet të ngjashëm që kodon shfletimin tuaj në ueb, si dhe mjete komunikimi me kriptim nga skaji në skaj (end-to-end).
> - Si masë parandaluese për të zbutur regjistrimin e mundshëm audio/video nëse e lini pajisjen të lidhur, duke e izoluar fizikisht pajisjen në mënyrë që të mos regjistrojë ndonjë audio ose video të vlefshme.

Nëse besoni se ka kërcënime të tjera të mundshme të përmendura në fillim që dëshironi të kontrolloni, ose dëshironi të kërkoni ndihmë, klikoni “Më tutje”.

##### Options

- [Më tutje](#pre-closure)
- [Mendoj se i kam zgjidhur problemet e mia](#resolved_end)

### leaked-internet-info-intro

#### Kundërshtari i di informacionet që i kam ndarë privatisht përmes e-mailit, aplikacionet për dërgimin e mesazheve ose të ngjashme

> Kjo degë e rrjedhës së punës do të mbulojë të dhënat që kanë “rrjedhur” nga shërbimet dhe aktiviteti në internet. Për informacionin që ka “rrjedhur” nga komunikimet telefonike, ju lutemi referojuni seksionit ["Informacioni konfidencial i ndarë nëpërmjet telefonatave ose mesazheve SMS duket se ka rrjedhur te kundërshtari"](#leaked-phone-info-intro).
>
> Në përgjithësi, rrjedhjet e informacionit ga shërbimet e bazuara në internet mund të ndodhin për tre arsye:
>
> 1. Informacioni në radhë të parë ka qenë publik, dhe këtë rast nuk do ta mbulojmë këtu.
> 2. Informacioni ka rrjedhë drejtpërdrejt nga llogaria (më e zakonshme për kundërshtarët "më të vegjël" më afër viktimës).
> 3. Informacioni ka rrjedhë nga platforma ku u ngarkua në radhë të parë (më e zakonshme për kundërshtarët "e mëdhenj" si qeveritë dhe kompanitë e mëdha).
>
> Do t’i trajtojmë dy rastet e fundit, pasi ato kanë të bëjnë më shumë me emergjencat e zakonshme që shihen në komunitetin e hapësirës qytetare. Ju mund të ndiqni rrjedhën e punës duke filluar nga pyetja e mëposhtme:

A ndani pajisje ose llogari me kundërshtarin tuaj?

##### Options

- [Po](#shared-devices-or-accounts)
- [Jo](#leaked-internet-info2)

### shared-devices-or-accounts

#### Po, unë ndaj pajisje ose llogari

> Në disa raste, ndarja e qasjes në llogari me të tjerët mund t'u japë palëve të padëshiruara qasje në informacione të ndjeshme, të cilat më pas mund të “rrjedhin” lehtësisht. Gjithashtu, në përgjithësi kur i ndajmë llogaritë me të tjerët, duhet ta bëjmë procesin praktik, duke ndikuar shpesh në sigurinë e mekanizmave të qasjes në llogari, si për shembull duke i bërë fjalëkalimet më të dobëta për t'u ndarë me lehtësi dhe duke e çaktivizuar vërtetimin me shumë faktorë (MFA ose 2FA) për të lejuar shumë njerëz të hyjnë në të njëjtën llogari.
>
> Një këshillë e parë do të ishte të vlerësohet se kush duhet të ketë qasje në llogaritë ose burimet përkatëse (si p.sh. dokumentet që ruhen në re - “cloud”) dhe të kufizohet kjo në sa më pak njerëz që të jetë e mundur. Një parim tjetër i dobishëm është të ketë një llogari për çdo person, duke i lejuar të gjithë të konfigurojnë cilësimet e tyre të qasjes në mënyrën më të sigurt të mundshme.
>
> Rekomandohet gjithashtu të kontrolloni seksionin ["Po marr mesazhe sigurie dhe sinjalizime nga shërbimet e besuara"](#security-alerts-intro), ku mund të gjeni këshilla për të forcuar llogaritë në rast të qasjes së dyshimtë.

Klikoni “Më tutje” për të eksploruar situatën tuaj më në detaje.

##### Options

- [Më tutje](#leaked-internet-info2)

### leaked-internet-info2

#### Eksploroni në më shumë detaje

A mundet kundërshtari juaj të kontrollojë ose të ketë qasje në shërbimet online që i përdorni dhe të ruajë informacionin e rrjedhur? (_Kjo zakonisht vlen për qeveritë, agjencitë e zbatimit të ligjit ose vetë ofruesit e shërbimeve_)

##### Options

- [Po](#control-of-online-services)
- [Jo](#leaked-internet-info3)

### control-of-online-services

#### Po, kundërshtari im mund të kontrollojë ose të ketë akses në shërbimet

> Ndonjëherë platformat që përdorim janë lehtësisht të qasshem ose madje kontrollohen nga kundërshtarët. Disa pyetje të zakonshme që mund t'i bëni vetes janë:
>
> - Kush është pronari i shërbimit?
> - A është një kundërshtar?
> - A ka kundërshtari qasje në serverë ose të dhëna përmes kërkimeve ligjore të të dhënave?
>
> Në atë rast, këshillohet që informacioni të zhvendoset në një platformë tjetër të pa kontrolluar nga kundërshtari për të shmangur rrjedhjet e ardhshme.
>
> Tregues të tjerë të dobishëm mund të jenë:
>
> - Aktiviteti i hyrjes (duke përfshirë vendndodhjet gjeografike), nëse është i disponueshëm
> - Sjellja e çuditshme e elementeve të lexuara dhe të palexuara
> - Konfigurimet që ndikojnë në atë se çfarë bëhet me mesazhet ose informacionet, si p.sh. ridrejtimet, rregullat për të kthyer e-mailet, etj., kur janë të disponueshme
>
> Mund të kontrolloni gjithashtu nëse mesazhet tuaja lexohen nga dikush i padëshiruar duke përdorur tokenët Canary, si ato që mund të gjenerojmë në [canarytokens.org](https://canarytokens.org/generate)

Klikoni “Më tutje” për të eksploruar situatën tuaj më në detaje.

##### Options

- [Më tutje](#leaked-internet-info3)

### leaked-internet-info3

#### Eksploroni në më shumë detaje

A ka kundërshtari juaj qasje në pajisjet e mbikëqyrjes dhe aftësi për ta vendosur atë pranë jush?

##### Options

- [Po](#leaked-internet-info4)
- [Jo](#pre-closure)

### leaked-internet-info4

#### Po, ata kanë akses në pajisjet e vëzhgimit aty pranë

> Kundërshtarët më të fuqishëm mund të kenë aftësinë për të vendosur pajisje të specializuara mbikëqyrjeje rreth viktimave të tyre, si p.sh. vendosjen e pajisjeve gjurmuese ose infektimin e pajisjeve tuaja me "spyware”.

A mendoni se mund të gjurmoheni nga një pajisje gjurmuese apo se kompjuteri ose celulari juaj mund të jetë infektuar me “malware” për t'ju spiunuar?

##### Options

- [Do të doja të mësoja më shumë rreth pajisjeve gjurmuese](#tracking-device-intro)
- [Do të doja të mësoja më shumë rreth mundësisë që pajisja ime të komprometohet me “malware”](#device-behaviour-intro)

### leaked-phone-info-intro

#### Informacionet konfidenciale të ndara nëpërmjet telefonatave ose mesazheve SMS duket se kanë rrjedhur te kundërshtari (veçanërisht gjatë ngjarjeve të ndjeshme

> Siç përshkruhet në hyrjen e kësaj rrjedhe të punës, mbikëqyrja masive telefonike është një kërcënim i gjithëpranishëm në shumicën e rajoneve dhe vendeve (nëse jo në të gjitha). Si informacion bazë, duhet të jemi të vetëdijshëm se është vërtet e lehtë për operatorët (dhe këdo që ka kontroll ose qasje në to) të kontrollojë informacione si:
>
> - Vendndodhjen historike (sa herë që telefoni është i ndezur dhe i lidhur me rrjetin), të dhënat nga kullat e rrjetit të telefonisë celulare me të cilat është lidhur telefoni
> - Regjistrat e thirrjeve (kush i telefonon kujt, kur dhe për sa kohë)
> - Mesazhet SMS (si regjistrat ashtu edhe përmbajtja aktuale e mesazheve)
> - Trafiku i internetit (kjo është më pak e zakonshme dhe më pak e dobishme për kundërshtarët, por nga kjo do të ishte e mundur të nxirret përfundimi se cilat aplikacione përdoren dhe kur, cilat faqe interneti konsultohen, etj.)
>
> Një skenar tjetër që ende vërehet në disa kontekste, megjithëse i pazakontë, është përdorimi i pajisjeve të përgjimit fizik të quajtura “stingrays” ose “IMSI-Catchers”. Këto pajisje imitojnë një kullë legjitime të rrjetit celular për të ridrejtuar përmes tyre trafikun telefonik të njerëzve në një zonë fizike të afërt. Megjithëse dobësitë që i mundësojnë këto pajisje po zgjidhen ose zbuten me protokolle më të reja (4G/5G), prap se prapë është ende e mundur të bllokohet sinjali për t'i bërë telefonat të besojnë se i vetmi protokoll i disponueshëm është 2G (më i keqpërdoruari nga këto pajisje). Për këtë rast, ka disa tregues me njëfarë besueshmërie, veçanërisht duke "zbritur" në 2G në zonat ku duhet të ketë 4G ose 5G ose ku njerëzit e tjerë përreth janë të lidhur me protokollet më të reja pa probleme. Për më shumë informacion mund të kontrolloni [faqen e internetit të Projektit FADe](https://fadeproject.org/?page_id=36). Gjithashtu, mund të pyesni veten nëse kundërshtari juaj ka dëshmuar të ketë qasje në këtë lloj pajisjeje, ndër të tjera.
>
> Si rekomandim i përgjithshëm për këta skenarë, është e këshillueshme që individët në rrezik të migrojnë komunikimet në kanale të enkriptuara duke u mbështetur në internet në vend të thirrjeve telefonike dhe SMS-ve konvencionale. E njëjta gjë vlen edhe për përdorimin e VPN ose Tor për trafikun e ndjeshëm të internetit (duke përfshirë aplikacionet). Aspekti i madh i pazgjidhur do të ishte gjurmimi i vendndodhjes së përdoruesit, i cili është i pashmangshëm gjatë qëndrimit të lidhur me rrjetin telefonik.
>
> Lidhur me mbledhjen masive të të dhënave përmes operatorëve, në disa juridiksione përdoruesit mund të kërkojnë se cilat të dhëna rreth tyre janë ndarë në të kaluarën. Mund të hulumtoni nëse kjo vlen për ju dhe nëse ai informacion do të jetë i besueshëm për të zbuluar mbikëqyrjen e mundshme. Mbani në mend se kjo zakonisht përfshin një hetim ligjor.

Nëse dëshironi të kontrolloni kërcënime të tjera të mundshme ose nëse keni një rast më kompleks dhe dëshironi të kërkoni ndihmë, klikoni Më tutje.

##### Options

- [Më tutje](#pre-closure)

### peer-compromised-intro

#### Njerëzit përreth jush janë shënjestruar me sukses nga masat e mbikëqyrjes

> Në rast se ndonjë person ose organizatë e lidhur me ju ka qenë viktimë e mbikëqyrjes, ju rekomandojmë që të përpiqeni të kuptoni teknikat që janë përdorur në to dhe të zgjidhni në listën e mëparshme ato që janë më të afërtat, në mënyrë që të merrni një kontekst më të përshtatshëm dhe të vendosni nëse edhe ju mund të jeni viktimë e teknikave të ngjashme.

Çfarë do të dëshironit të bënit?

##### Options

- [Më dërgo në hapin e mëparshëm](#start)
- [Nuk jam i sigurt](#pre-closure)

### other-intro

#### Diçka tjetër / Nuk e di

> Lista që patë më parë përfshinte rastet më të shpeshta që shihen në hapësirën civile nga ndihmësit, por situata juaj mund të mos jetë mbuluar aty. Nëse ky është rasti juaj, klikoni "Kam nevojë për ndihmë"; përndryshe, mund të ktheheni te lista e rasteve më të zakonshme dhe të zgjidhni skenarin më të afërt të zbatueshëm për ju për të marrë udhëzime më specifike.

Çfarë do të dëshironit të bënit?

##### Options

- [Më dërgo në listën e kërcënimeve të mundshme](#start)
- [Kam nevojë për ndihmë](#help_end)

### pre-closure

#### por do të doja të konsideroja skenarë të tjerë të mbikëqyrjes

> Shpresojmë që kjo rrjedhë pune të ketë qenë e dobishme deri më tani. Ju mund të zgjidhni të ktheheni në listën fillestare të skenarëve për të kontrolluar kërcënimet e tjera për të cilat mund të jeni të shqetësuar, ose të shkoni te lista e organizatave që mund të kontaktoni për t'ju ndihmuar me raste më komplekse.

Çfarë do të dëshironit të bënit?

##### Options

- [Do të doja të mësoja rreth teknikave të tjera të mbikëqyrjes](#start)
- [Mendoj se nevoja ime nuk mbulohet në këtë seksion ose është shumë komplekse](#help_end)
- [Mendoj se e kam të qartë se çfarë po ndodh me mua](#resolved_end)

### help_end

#### Mendoj se nevoja ime nuk mbulohet në këtë seksion ose është shumë komplekse

> Nëse keni nevojë për ndihmë shtesë në trajtimin e rasteve të mbikëqyrjes, mund të kontaktoni organizatat e shënuara më poshtë.
>
> _Shënim: kur kontaktoni ndonjë organizatë për të kërkuar ndihmë, ju lutemi tregojuni për rrugën që keni ndjekur këtu dhe çdo tregues që keni gjetur për ta lehtësuar çdo hap tjetër._

[orgs](:organisations?services=legal&services=vulnerabilities_malware&services=forensic)

### legal_end

#### kam nevojë për mbështetje ligjore

> Nëse keni nevojë për mbështetje për të paditur dikë për spiunim të paligjshëm ndaj jush, mund të kontaktoni një nga organizatat e mëposhtme.

[orgs](:organisations?services=legal)

### resolved_end

#### mendoj se i kam zgjidhur problemet e mia

Shpresojmë se ky udhëzues për zgjidhjen e problemeve ishte i dobishëm. Ju lutemi jepni komente [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

- Migroni çdo komunikim nga kanale të pasigurta si telefonatat dhe SMS-të në kanalet e komunikimit të koduar që mbështeten në internet, si aplikacionet për dërgimin e mesazheve të enkriptuara nga skaji në skaj (end-to-end) dhe faqet e internetit të mbrojtura përmes HTTPS.
- Nëse keni frikë se trafiku juaj i internetit po monitorohet dhe besoni se ky mund të jetë një rrezik sigurie, merrni parasysh mundësinë për t’u lidhur në internet përmes një mjeti kriptimi si VPN ose Tor.
- Mbani gjurmët e të gjitha pajisjeve të pranishme në hapësirat tuaja, veçanërisht ato të lidhura në internet dhe përpiquni të dini se çfarë bëjnë.
- Shmangni ndarjen e informacionit të ndjeshëm përmes shërbimeve të kontrolluara nga kundërshtari.
- Minimizoni ndarjen e llogarive ose pajisjeve tuaja sa më shumë që të mundeni.
- Jini të vetëdijshëm për mesazhet, faqet e internetit, shkarkimet dhe aplikacionet e dyshimta.

### Resources

- [Aplikacioni Android i Apple për të skanuar për AirTag është një hap i domosdoshëm përpara, por nevojiten më shumë zbutje kundër përndjekjes](https://www.eff.org/es/deeplinks/2021/12/apples-android-app-scan-airtags-necessary-step-forward-more-anti-stalking).
- [Privatësia për nxënësit/studentët](https://ssd.eff.org/module/privacy-students): Me referenca të teknikave të mbikëqyrjes në shkolla që mund të zbatohen për shumë skenarë të tjerë. -[Siguria në një kuti: Mbroni privatësinë e komunikimit tuaj online](https://securityinabox.org/en/communication/private-communication/) -[Siguria në një kuti: Vizitoni faqet e bllokuara të internetit dhe shfletoni në mënyrë anonime](https://securityinabox.org/en/internet-connection/anonymity-and-circumvention/) -[Siguria në një kuti: Rekomandime për mjetet e enkriptuara të bisedës (çatit)](https://securityinabox.org/en/communication/tools/#more-secure-text-voice-and-video-chat-applications)
