---
layout: topic
title: "Dikush po ndan informacionin tim personal"
author: Ahmad Gharbeia, Michael Carbone, Gus Andrews, Flo Pagano, Constanza Figueroa
language: sq
summary: "Po qarkullojnë informacione private për mua ose materiale që shfaqin ngjashmëri me mua. Është shqetësuese për mua ose mund të jetë potencialisht e dëmshme"
date: 2023-05
permalink: topics/doxing
parent: Home
order: 10
---

# Dikush po shpërndan informacionin ose mediat e mia private pa pëlqimin tim

Ngacmuesit ndonjëherë mund të publikojnë informacione që ju identifikojnë personalisht ose që ekspozojnë aspekte të jetës suaj personale që keni zgjedhur t'i mbani private. Kjo nganjëherë quhet "doxxing", ose "doxing" (një term i shkurtër për "lëshimin e dokumenteve").

Qëllimi i doxxing-ut është të turpërojë personin e shënjesturar ose të agjitojë kundër tyre. Kjo mund të ketë efekte të rënda negative në mirëqenien tuaj psiko-sociale, sigurinë personale, marrëdhëniet dhe punën.

Doxxing-u përdoret për të frikësuar figurat publike, gazetarët, mbrojtësit e të drejtave të njeriut dhe aktivistët, por gjithashtu shpesh synon njerëzit që u përkasin pakicave seksuale ose popullatave të margjinalizuara. Ai është i lidhur ngushtë me forma të tjera të dhunës në internet dhe dhunës gjinore.

Personi që ju bën doxxing mund t'i ketë marrë dokumentet tuaja nga burime të ndryshme. Kjo mund të përfshijë hakimin e llogarive dhe pajisjeve tuaja private. Por, është gjithashtu e mundur që ata po ekspozojnë dhe nxjerrin jashtë kontekstit informacione ose materiale që keni menduar se po i ndani privatisht me miqtë, partnerët, të njohurit ose bashkëpunëtorët tuaj. Në disa raste ngacmuesit mund të grumbullojnë informacione të disponueshme publikisht për ju nga mediat sociale ose të dhënat publike. Kombinimi i këtij informacioni në mënyra të reja, të keqinterpretuara në mënyrë të dëmshme, mund të përbëjë gjithashtu doxxing.

Nëse keni qenë cak i doxxing-ut, ju mund të përdorni pyetësorin e mëposhtëm për të identifikuar se ku e ka gjetur ngacmuesi informacionin tuaj dhe për të gjetur mënyra për të kufizuar dëmin, duke përfshirë heqjen e dokumenteve nga faqet e internetit.

## Workflow

### be_supported

#### A mendoni se keni dikë të cilit i besoni, i cili mund t'ju ndihmojë në rast se keni nevojë?

> Dokumentimi është thelbësor për vlerësimin e ndikimeve, identifikimin e origjinës së këtyre sulmeve dhe zhvillimin e përgjigjeve që rikthejnë sigurinë tuaj. Në varësi të asaj që ka ndodhur, kalimi nëpër procesin e dokumentimit mund të rrisë ndikimet emocionale që ky agresion ka tek ju. Për të lehtësuar barrën e kësaj pune dhe për të reduktuar stresin e mundshëm emocional mbi veten tuaj, mund t'ju duhet mbështetje nga një mik i ngushtë ose një person tjetër i besuar gjatë dokumentimit të këtyre sulmeve.

A mendoni se keni dikë të cilit i besoni, i cili mund t'ju ndihmojë në rast se keni nevojë?

##### Options

- [Po, një person i besuar është i gatshëm të më ndihmojë](#physical_risk)

### physical_risk

#### një person i besuar është i gatshëm të më ndihmojë

> Reflektim: Kush mendoni se mund të jetë përgjegjës për këto sulme? Cilat janë aftësitë e tyre? Sa të vendosur janë ata për t'ju dëmtuar? Cilat palë të tjera mund të jenë të motivuara për t'ju dëmtuar? Këto pyetje do t'ju ndihmojnë të vlerësoni kërcënimet që janë duke u zhvilluar dhe të vlerësoni nëse jeni të ekspozuar ndaj rreziqeve fizike.

A keni frikë për integritetin apo mirëqenien tuaj fizike apo se mund të jeni në rrezik ligjor?

##### Options

- [Po, mendoj se jam i ekspozuar ndaj rreziqeve fizike](#yes_physical_risk)
- [Mendoj se mund të jem në rrezik ligjor](#legal_risk)
- [Jo, do të doja të zgjidhja problemin tim në sferën digjitale](#takedown_content)
- [Nuk jam i/e sigurt. Më duhet ndihmë për të vlerësuar rreziqet e mia.](#assessment_end)

### yes_physical_risk

#### Po, mendoj se jam i ekspozuar ndaj rreziqeve fizike

> Nëse mendoni se mund të jeni në shënjestër të sulmeve fizike personale ose se integriteti juaj fizik ose mirëqenia juaj është në rrezik, mund t'i referoheni burimeve të mëposhtme për të menduar për nevojat tuaja të menjëhershme të sigurisë fizike.
>
> - [Front Line Defenders - Fletore Pune për Sigurinë](https://www.frontlinedefenders.org/en/workbook-security) (në gjuhë të shumta)
> - [Linja telefonike kombëtare për dhunën në familje - Krijo një plan sigurie](https://www.thehotline.org/plan-for-safety/create-a-safety-plan/)
> - [Koalicioni kundër dhunës në internet - Mbështetje për sigurinë fizike](https://onlineviolenceresponsehub.org/physical-security-support)
> - [Forumi i Qëndrueshmërisë Cheshire - Si të përgatitemi për një urgjencë](https://cheshireresilience.org.uk/how-to-prepare-for-an-emergency/)
> - [FEMA Form P-1094 Krijo planin e komunikimit të urgjencës familjare](https://www.templateroller.com/group/12262/create-your-family-emergency-communication-plan.html)
> - [Reolink - Si të siguroni me zgjuarsi dritaret e shtëpisë tuaj — 9 zgjidhjet më të lehta të sigurisë](https://reolink.com/blog/top-7-easy-diy-ways-to-secure-your-home-windows/)
> - [Reolink - Si ta bëni shtëpinë tuaj të sigurt nga vjedhjet](https://reolink.com/blog/make-home-safe-from-break-ins/)
> - [Seguridad integral para periodistas - Seguridad Física](https://seguridadintegral.articulo19.org/categorias-prevencion/seguridad-fisica/) (në spanjisht)

Keni nevojë për më shumë ndihmë për të siguruar integritetin dhe mirëqenien tuaj fizike?

##### Options

- [Po](#physical-risk_end)
- [Jo, por mendoj se jam edhe në rrezik ligjor](#legal_risk)
- [Jo, do të doja të zgjidhja problemin tim në sferën digjitale](#takedown_content)
- [Nuk jam i/e sigurt. Më duhet ndihmë për të vlerësuar rreziqet e mia.](#assessment_end)

### legal_risk

#### Mendoj se mund të jem në rrezik ligjor

> Nëse po mendoni të ndërmerrni veprime ligjore, do të jetë shumë e rëndësishme të mbani provat për sulmet ndaj të cilave jeni nënshtruar. Prandaj, rekomandohet fuqimisht të ndiqni [rekomandimet në faqen e Veglërisë së Ndihmës së Parë Digjitale për regjistrimin e informacionit mbi sulmet](/sq/documentation).

A keni nevojë për mbështetje të menjëhershme ligjore?

##### Options

- [Po](#legal_end)
- [Jo, do të doja të zgjidhja problemin tim në sferën digjitale](#takedown_content)
- [Jo](#resolved_end)

### takedown_content

#### Jo, do të doja të zgjidhja problemin tim në sferën digjitale

A dëshironi të punoni për heqjen e dokumenteve të shpërndara në mënyrë të papërshtatshme nga publiku?

##### Options

- [Po](#documenting)
- [Jo](#resolved_end)

### documenting

#### Hiqni dokumentet nga pamja publike

> Mbajtja e dokumentacionit tuaj të sulmit të cilit i jeni nënshtruar është shumë e rëndësishme. Kjo e bën më të lehtë për ndihmësit ligjorë ose teknikë t'ju mbështesin. Ju lutemi shikoni [udhëzuesin e Veglërisë së Ndihmës së Parë Digjitale për dokumentimin e sulmeve në internet](/sq/documentation) për të mësuar se si të mblidhni më mirë informacionin për nevojat tuaja të veçanta.
>
> Ju gjithashtu mund të dëshironi të kërkoni nga një person të cilit i besoni t'ju ndihmojë të gjeni prova për sulmin të cilit i jeni nënshtruar. Kjo mund të ndihmojë të siguroheni që të mos ri-traumatizoheni duke i parë përsëri materialet e publikuara.

A mendoni se jeni gati për të dokumentuar sulmin në një mënyrë të sigurt?

##### Options

- [Po, po i dokumentoj sulmet](#where_published)

### where_published

#### Po, po i dokumentoj sulmet

> Në varësi të vendit ku sulmuesi e ka ndarë publikisht informacionin tuaj, mund të dëshironi të ndërmerrni veprime të ndryshme.
>
> Nëse informacioni juaj është postuar në platformat e mediave sociale të bazuara në vende me sisteme ligjore të cilat përcaktojnë përgjegjësitë ligjore të korporatave ndaj njerëzve që përdorin shërbimet e tyre (për shembull, Bashkimi Evropian ose Shtetet e Bashkuara të Amerikës), ka më shumë gjasa që moderatorët e korporatës do të jenë të gatshëm t'ju ndihmojnë. Ata do të kenë nevojë që ju të [dokumentoni](/sq/documentation) dëshmitë e sulmeve. Ju mund të jeni në gjendje të kërkoni vendin ku është selia e një kompanie në Wikipedia.
>
> Ndonjëherë kundërshtarët mund t'i publikojnë informacionet tuaja në ueb-faqe më të vogla, më pak të njohura, dhe të lidhen me to nga mediat sociale. Ndonjëherë këto faqe hostohen në vende ku nuk ka mbrojtje ligjore, ose ato menaxhohen nga individë ose grupe që praktikojnë, mbështesin ose thjesht nuk e kundërshtojnë sjelljen e dëmshme. Në këtë rast mund të jetë më e vështirë të kontaktoni me faqet ku është postuar materiali, ose të zbuloni se kush i udhëheq ato.
>
> Por edhe kur sulmuesi i ka postuar dokumentet tuaja diku tjetër, e jo në mediat sociale të njohura (gjë që ndodh shpesh), puna për të hequr ato materiale ose lidhje me to mund të zvogëlojë ndjeshëm numrin e njerëzve që e shohin informacionin tuaj. Kjo mund të zvogëlojë gjithashtu numrin dhe ashpërsinë e sulmeve me të cilat mund të përballeni.

Informacioni im u postua në

##### Options

- [një platformë ose shërbim të rregulluar sipas ligjit](#what_info_published)
- [një platformë ose shërbim pa shumë gjasa për t'iu përgjigjur kërkesave për heqje të përmbajtjes](#legal_advocacy)

### what_info_published

#### një platformë ose shërbim të rregulluar sipas ligjit

Çfarë informacioni është shpërndarë pa pëlqim?

##### Options

- [të dhënat personale që më identifikojnë ose që janë private: adresa, numri i telefonit, numri i identifikimit personal, llogaria bankare, etj.](#personal_info)
- [materiale që nuk pranova t’i shpërndaja, duke përfshirë materiale intime (fotografi, video, audio, tekste)](#media)
- [një pseudonim që përdor është i lidhur me identitetin tim të jetës reale](#linked_identities)

### linked_identities

#### një pseudonim që përdor është i lidhur me identitetin tim të jetës reale

> Nëse një agresor e lidh publikisht emrin ose identitetin tuaj të jetës reale me një pseudonim, nofkë ose llagap që e përdorni kur shpreheni, jepni mendimin tuaj publik, organizoni ose përfshiheni në aktivizëm, konsideroni mbylljen e llogarive që lidhen me atë identitet si një mënyrë për të reduktuar dëmin.

A ju duhet të vazhdoni të përdorni këtë identitet/personalitet për të arritur qëllimet tuaja, apo mund të mbyllni llogarinë(të)?

##### Options

- [Më duhet të vazhdoj ta përdor](#defamation_flow_end)
- [Mund ta mbyll llogarinë(t)](#close)

### close

#### Mund ta mbyll llogarinë(t)

> Para se të mbyllni llogaritë e lidhura me identitetin ose personalitetin e prekur, merrni parasysh rreziqet e mbylljes së llogarisë. Mund të humbni qasjen në shërbime ose të dhëna, mund të përballeni me rreziqe për reputacionin tuaj, të humbni kontaktin me bashkëpunëtorët tuaj në internet, etj.

##### Options

- [I kam mbyllur llogaritë përkatëse](#resolved_end)
- [Kam vendosur të mos i mbyll llogaritë përkatëse për momentin](#resolved_end)
- [Kam nevojë për ndihmë për të kuptuar se çfarë duhet të bëj](#harassment_end)

### personal_info

#### Ku janë publikuar të dhënat tuaja personale?

##### Options

- [Në një platformë të rrjeteve sociale](#doxing-sn)
- [Në një faqe interneti](#doxing-web)

### doxing-sn

#### Në një platformë të rrjeteve sociale

> Nëse informacioni juaj privat është publikuar në një platformë të mediave sociale, ju mund të raportoni një shkelje të standardeve të komunitetit duke ndjekur procedurat e raportimit të ofruara për përdoruesit nga faqet e internetit të rrjeteve sociale.
>
> **_Shënim:_** _Gjithmonë [dokumentoni](/sq/documentation) përpara se të ndërmerrni veprime të tilla si fshirja e mesazheve ose e regjistrave të bisedave ose bllokimi i profileve. Nëse shqyrtoni mundësinë për veprime ligjore, duhet të konsultoni informacionin mbi [dokumentacionin ligjor](/sq/documentation#legal)._
>
> Udhëzimet për raportimin e një shkeljeje të standardeve të komunitetit në platformat kryesore do t'i gjeni në listën e mëposhtme:
>
> - [Google](https://cybercivilrights.org/google-2/)
> - [Facebook](https://www.cybercivilrights.org/facebook)
> - [Twitter](https://www.cybercivilrights.org/twitter)
> - [Tumblr](https://www.cybercivilrights.org/tumblr)
> - [Instagram](https://www.cybercivilrights.org/instagram)
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A është fshirë informacioni?

##### Options

- [Po](#harassment_end)
- [Jo](#platform_help_end)

### doxing-web

#### Në një faqe interneti

> Mund të provoni të raportoni faqen e internetit te ofruesi i hostingut ose regjistruesi i domenit, duke kërkuar heqjen e përmbajtjes.
>
> **_Shënim:_** _Gjithmonë [dokumentoni](/sq/documentation) përpara se të ndërmerrni veprime të tilla si fshirja e mesazheve ose e regjistrave të bisedave ose bllokimi i profileve. Nëse shqyrtoni mundësinë për veprime ligjore, duhet të konsultoni informacionin mbi [dokumentacionin ligjor](/sq/documentation#legal)._
>
> Për të dërguar një kërkesë për heqje të përmbajtjes, do t'ju duhet gjithashtu të grumbulloni informacion në faqen e internetit ku është publikuar informacioni juaj:
>
> - Shkoni te [Shërbimi NSLookup i Network Tools](https://network-tools.com/nslookup/) dhe zbuloni adresën (ose adresat) IP të faqes së rreme të internetit duke futur URL-në e saj në formularin e kërkimit.
> - Shkruani adresën ose adresat IP.
> - Shkoni te [Shërbimi Whois Lookup i Domain Tools](https://whois.domaintools.com/) dhe kërkoni si domenin ashtu edhe adresën/at IP të faqes së rreme të internetit.
> - Regjistroni emrin dhe adresën e e-mailit të abuzimit të ofruesit të hostingut dhe shërbimit të domenit. Nëse përfshihet në rezultatet e kërkimit tuaj, regjistroni edhe emrin e pronarit të faqes së internetit.
> - Shkruani ofruesit të hostingut dhe regjistruesit të domenit të faqes së rreme të internetit për të kërkuar heqjen e saj. Në mesazhin tuaj, përfshini informacion mbi adresën IP, URL-në dhe pronarin e faqes së internetit që ju imiton, si dhe arsyet pse është abuzive.
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A është hequr përmbajtja?

##### Options

- [Po](#resolved_end)
- [Jo, platforma nuk u përgjigj / ndihmoi](#platform_help_end)
- [Kam nevojë për ndihmë për të kuptuar se si të dërgoj një kërkesë për heqje të përmbajtjes](#platform_help_end)

### media

#### Mediat që refuzova të ndaja, përfshirë materialet intime

> Nëse e keni krijuar vetë materialin mediatik, zakonisht do t’i zotëroni të drejtat e autorit për atë material. Në disa juridiksione, individët gjithashtu kanë të drejta për materialet mediatike në të cilat shfaqen (të njohura si të drejtat e publicitetit).

A i zotëroni të drejtat e autorit për materialet mediatike?

##### Options

- [Po](#intellectual_property)
- [Jo](#nude)

### intellectual_property

#### Unë zotëroj të drejtën e autorit të medias

> Ju mund të përdorni rregulloret për të drejtat e autorit, të tilla si [Ligji i të Drejtave të Autorit i Mijëvjeçarit Digjital](https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act), për të hequr informacionin me të drejtë autori. Shumica e platformave të mediave sociale ofrojnë forma për të raportuar shkeljen e të drejtave të autorit dhe mund të jenë më të gatshme të përgjigjen ndaj këtij lloji të kërkesave sesa opsionet e tjera për heqjen e përmbajtjes, për shkak të implikimeve ligjore për to.
>
> **_Shënim:_** _Gjithmonë [dokumentoni](/sq/documentation) përpara se të kërkoni të hiqet përmbajtja. Nëse shqyrtoni mundësinë për veprime ligjore, duhet të konsultoni informacionin mbi [dokumentacionin ligjor](/sq/documentation#legal)._
>
> Ju mund t'i përdorni këto lidhje për të dërguar një kërkesë për heqjen e përmbajtjes për shkak të shkeljes së të drejtave të autorit në platformat kryesore të rrjeteve sociale:
>
> - [Facebook](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://www.tiktok.com/legal/report/Copyright)
> - [Twitter](https://help.twitter.com/en/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622?sjid=1561215955637134274-SA)
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A është hequr përmbajtja?

##### Options

- [Po](#resolved_end)
- [Jo, platforma nuk u përgjigj / ndihmoi](#platform_help_end)
- [Nuk i gjeta platformat përkatëse në listën e burimeve](#platform_help_end)

### nude

#### Unë nuk zotëroj të drejtat e autorit të medias

> Materialet mediatike që shfaqin trupa të zhveshur ose detaje të tyre ndonjëherë mbulohen nga politika specifike të platformave.

A përfshin materiali mediatik trupa të zhveshur apo detaje të trupave të zhveshur?

##### Options

- [Po](#intimate_media)
- [Jo](#nonconsensual_media)

### intimate_media

#### Po, media përmban media intime

> Për të mësuar se si të raportoni një shkelje të rregullave të privatësisë ose ndarje pa pëlqim të materialeve mediatike intime/përmbajtjes së dëmshme në platformat më të njohura, mund të shikoni burimet e mëposhtme:
>
> - [Stop NCII](https://stopncii.org/) (Facebook, Instagram, TikTok dhe Bumble - në mbarë botën)
> - [Revenge Porn Helpline - Ndihmë për viktimat jashtë MB](https://revengepornhelpline.org.uk/how-can-we-help/if-we-can-t-help-who-can/help-for-victims-outside-the-uk/)
>
> **_Shënim:_** _Gjithmonë [dokumentoni](/sq/documentation) përpara se të kërkoni të hiqet përmbajtja. Nëse shqyrtoni mundësinë për veprime ligjore, duhet të konsultoni informacionin mbi [dokumentacionin ligjor](/sq/documentation#legal)._
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A është hequr përmbajtja?

##### Options

- [Po](#resolved_end)
- [Jo, platforma nuk u përgjigj / ndihmoi](#platform_help_end)
- [Nuk i gjeta platformat përkatëse në listën e burimeve](#platform_help_end)

### nonconsensual_media

#### Jo, nuk përmban media intime

Ku janë publikuar materialet tuaja mediatike?

##### Options

- [Në një platformë të rrjeteve sociale](#NCII-sn)
- [Në një faqe interneti](#NCII-web)

### NCII-sn

#### Në një platformë të rrjeteve sociale

> Nëse materiali juaj mediatik është publikuar në një platformë të mediave sociale, ju mund të raportoni një shkelje të standardeve të komunitetit duke ndjekur procedurat e raportimit të ofruara për përdoruesit nga faqet e internetit të rrjeteve sociale.
>
> **_Shënim:_** _Gjithmonë [dokumentoni](/sq/documentation) përpara se të ndërmerrni veprime të tilla si fshirja e mesazheve ose e regjistrave të bisedave ose bllokimi i profileve. Nëse shqyrtoni mundësinë për veprime ligjore, duhet të konsultoni informacionin mbi [dokumentacionin ligjor](/sq/documentation#legal)._
>
> Udhëzimet për raportimin e një shkeljeje të standardeve të komunitetit në platformat kryesore do t'i gjeni në listën e mëposhtme:
>
> - [Google](https://cybercivilrights.org/google-2/)
> - [Facebook](https://www.cybercivilrights.org/facebook)
> - [Twitter](https://www.cybercivilrights.org/twitter)
> - [Tumblr](https://www.cybercivilrights.org/tumblr)
> - [Instagram](https://www.cybercivilrights.org/instagram)
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A është hequr përmbajtja?

##### Options

- [Po](#resolved_end)
- [Jo, platforma nuk u përgjigj / ndihmoi](#platform_help_end)
- [Nuk i gjeta platformat përkatëse në listën e burimeve](#platform_help_end)

### NCII-web

#### Në një faqe interneti

> Ndiqni udhëzimet në ["Pa pëlqimin tim - Hiq përmbajtjen"](https://withoutmyconsent.org/resources/take-down) për të hequr përmbajtjen nga një faqe interneti.
>
> **_Shënim:_** _Gjithmonë [dokumentoni](/sq/documentation) përpara se të ndërmerrni veprime të tilla si fshirja e mesazheve ose e regjistrave të bisedave ose bllokimi i profileve. Nëse shqyrtoni mundësinë për veprime ligjore, duhet të konsultoni informacionin mbi [dokumentacionin ligjor](/sq/documentation#legal)._
>
> Ju lutemi vini re se mund të duhet pak kohë për të marrë një përgjigje ndaj kërkesave tuaja. Ruajeni këtë faqe në faqerojtësin (bookmarks) tuaj dhe kthehuni në këtë rrjedhë pune pas disa ditësh.

A është hequr përmbajtja?

##### Options

- [Po](#resolved_end)
- [Jo, platforma nuk u përgjigj / ndihmoi](#platform_help_end)
- [Kam nevojë për ndihmë për të kuptuar se si të dërgoj një kërkesë për heqje të përmbajtjes](#platform_help_end)

### legal_advocacy

#### Një platformë ose shërbim që nuk ka gjasa t'i përgjigjet kërkesave për heqje

> Në disa raste, platformat ku ndodh doxxing-u ose publikimi pa pëlqim i materialeve mediatike intime nuk kanë politika ose një proces moderimi që është mjaftueshëm i përgatitur për të trajtuar kërkesat në lidhje me doxxing-un, ngacmimin ose shpifjet.
>
> Ndonjëherë materiali fyes publikohet ose vendoset në platforma ose aplikacione që drejtohen nga individë ose grupe që praktikojnë, mbështesin ose thjesht nuk e kundërshtojnë sjelljen e dëmshme.
>
> Ka edhe vende në hapësirën kibernetike ku ligjet dhe rregulloret vështirë se janë të zbatueshme, sepse ato janë krijuar posaçërisht për të funksionuar në mënyrë anonime dhe për të mos lënë gjurmë. Një nga këto është ajo që njihet si _rrjeti i errët_ (_the dark web_). Ka përfitime nga vënia në dispozicion e hapësirave anonime si këto, edhe pse disa njerëz i abuzojnë ato.
>
> Në situata si këto, kur veprimet ligjore nuk janë të disponueshme, merrni parasysh mundësinë për të përdorur avokimin. Kjo mund të përfshijë hedhjen publike të dritës mbi rastin tuaj, gjenerimin e një debati publik në lidhje me të, e ndoshta edhe nxjerrjen në sipërfaqe të rasteve të ngjashme. Ka organizata dhe grupe të të drejtave që mund t'ju ndihmojnë me këtë. Ato mund t'ju ndihmojnë të vendosni pritshmëri për rezultatet dhe t'ju japin një ndjenjë të pasojave.
>
> Kërkimi i ndihmës juridike mund të jetë zgjidhja e fundit nëse komunikimi me platformën, aplikacionin ose ofruesin e shërbimit dështon ose është i pamundur.
>
> Rruga ligjore zakonisht kërkon kohë, kushton para dhe mund të kërkojë që të publikoni se doxxing-u ju ka ndodhur ose se materialet tuaja intime mediatike janë publikuar pa pëlqimin tuaj. Një rast ligjor i suksesshëm varet nga shumë faktorë, duke përfshirë [dokumentimin](/sq/documentation#legal) e agresionit në një mënyrë që konsiderohet e pranueshme nga gjykatat dhe korniza ligjore që e rregullon çështjen. Identifikimi i autorëve ose përcaktimi se ata ishin përgjegjës mund të jetë gjithashtu i vështirë për t'u dëshmuar. Mund të jetë gjithashtu sfiduese të përcaktohet juridiksioni në të cilin duhet të gjykohet rasti.

Çfarë do të dëshironit të bënit?

##### Options

- [Kam nevojë për mbështetje për të planifikuar një fushatë avokimi](#advocacy_end)
- [Kam nevojë për mbështetje ligjore](#legal_end)
- [Kam mbështetje nga komuniteti im lokal](#resolved_end)

### advocacy_end

#### Kam nevojë për mbështetje për të planifikuar një fushatë avokimi

> Nëse dëshironi të kundërshtoni informacionin për ju që është përhapur në internet pa vullnetin tuaj, ju sugjerojmë të ndiqni rekomandimet në [rrjedhën e punës së Veglërisë së Ndihmës së Parë Digjitale për fushatat shpifëse](/sq/topics/defamation).
>
> Nëse dëshironi mbështetje për të nisur një fushatë për të ekspozuar sulmuesit tuaj, mund të kontaktoni me organizatat që mund të ndihmojnë me përpjekjet e avokimit:

[orgs](:organisations?services=advocacy)

### legal_end

#### Kam nevojë për mbështetje ligjore

> Nëse keni nevojë për mbështetje ligjore, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.
>
> Nëse mendoni të ndërmerrni veprime ligjore, do të jetë shumë e rëndësishme të mbani prova për sulmet të cilave u jeni nënshtruar. Prandaj, rekomandohet fuqimisht të ndiqni [rekomandimet në faqen e Veglërisë së Ndihmës së Parë Digjitale për regjistrimin e informacionit mbi sulmet](/sq/documentation).

[orgs](:organisations?services=legal)

### physical-risk_end

#### Kam nevojë për mbështetje për të siguruar integritetin dhe mirëqenien time fizike

> Nëse jeni në rrezik fizik dhe keni nevojë për ndihmë të menjëhershme, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

[orgs](:organisations?services=physical_security)

### assessment_end

#### Nuk jam i/e sigurt. Më duhet ndihmë për të vlerësuar rreziqet e mia.

> Nëse mendoni se keni nevojë për ndihmë në vlerësimin e rreziqeve me të cilat përballeni sepse informacioni ose materiali juaj mediatik është shpërndarë pa lejen tuaj, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

[orgs](:organisations?services=harassment&services=assessment)

### resolved_end

#### Problemi im është zgjidhur

> Shpresojmë se ky udhëzues për zgjidhjen e problemeve ishte i dobishëm. Ju lutemi jepni komente [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### defamation_flow_end

#### Më duhet të vazhdoj të përdor identitetin

> Nëse dëshironi të kundërshtoni informacionin për ju që është përhapur në internet pa vullnetin tuaj, ju sugjerojmë të ndiqni rekomandimet në [rrjedhën e punës së Veglërisë së Ndihmës së Parë Digjitale për fushatat shpifëse](/sq/topics/defamation).
>
> Nëse keni nevojë për mbështetje të specializuar, lista e mëposhtme përfshin organizata që mund t'ju ndihmojnë të trajtoni dëmtimin e reputacionit.

[orgs](:organisations?services=advocacy&services=legal)

### platform_help_end

#### Platforma nuk u përgjigj dhe nuk ndihmoi

> Më poshtë është një listë e organizatave që mund t'ju mbështesin në raportimin në platforma dhe shërbime.

[orgs](:organisations?services=harassment&services=legal)

### harassment_end

#### Unë kam nevojë për ndihmë për të kuptuar se çfarë duhet të bëj

> Nëse keni nevojë për mbështetje për të adresuar situatën tuaj, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

[orgs](:organisations?services=harassment&services=triage)

### Final Tips

- Hartëzoni praninë tuaj në internet. Vetë-doksimi konsiston në eksplorimin e inteligjencës me burim të hapur mbi veten për të parandaluar aktorët keqdashës që të gjejnë dhe përdorin këtë informacion për t'ju imituar. Mësoni më shumë se si të kërkoni gjurmët tuaja në internet [Access Now Helpline's Guide's Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- Nëse doxxing-u përfshinte informacione ose dokumente që vetëm ju, ose disa njerëz të besuar, duhet t'i kishin ditur ose të kishin qasje, atëherë mund të dëshironi të reflektoni gjithashtu edhe për atë se si personi që ka bërë doxxing-un ka pasur qasje në to. Rishikoni praktikat tuaja të sigurisë së informacionit personal për të përmirësuar sigurinë e pajisjeve dhe llogarive tuaja.
- Është mirë të jeni vigjilentë në të ardhmen e afërt, në rast se i njëjti material, ose materiale të ngjashme, rishfaqen në internet. Një mënyrë për të mbrojtur veten është të kërkoni emrin tuaj ose pseudonimet që keni përdorur në motorët e kërkimit, për të parë se ku mund të shfaqen ato. Mund të konfiguroni një Google Alert për vete, ose të përdorni Meltwater ose Mention. Këto shërbime gjithashtu do t'ju njoftojnë kur emri ose pseudonimi juaj të shfaqet në internet.

### Resources

- [Crash Override Network - So You’ve Been Doxed: Një udhëzues për praktikat më të mira](https://crashoverridenetwork.tumblr.com/post/114270394687/so-youve-been-doxed-a-guide-to-best-practices)
- [Access Now Helpline Community Documentation: Udhëzues për parandalimin e doxxing-ut](https://guides.accessnow.org/self-doxing.html)
- [Equality Labs: Udhëzues kundër doxxing-ut për aktivistët që përballen me sulme nga Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Mbyllja e identitetit tuaj digjital](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Ken Gagne, Doxxing defense: Hiqni të dhënat tuaja personale nga ndërmjetësit e të dhënave](https://www.computerworld.com/article/2849263/doxxing-defense-remove-your-personal-info-from-data-brokers.html)
- [Projekti Totem - Mbajeni private](https://learn.totem-project.org/courses/course-v1:IWMF+IWMF_KP_EN+001/about)
- [Projekti Totem - Si të mbroni identitetin tuaj në internet](https://learn.totem-project.org/courses/course-v1:Totem TP_IO_EN 001/about)
- [Koalicioni kundër dhunës në internet - Kam qenë viktimë e doxxing-ut](https://onlineviolenceresponsehub.org/for-journalists#doxxed)
- [PEN America: Doracak për ngacmimin në internet](https://onlineharassmentfieldmanual.pen.org/)
