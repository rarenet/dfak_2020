---
layout: topic
title: "Ueb-faqja ime ka rënë, çfarë po ndodh?"
author: Rarenet
language: sq
summary: "Një kërcënim me të cilin përballen shumë OJQ, media të pavarura dhe blogerë është që zëri i tye të heshtet sepse ueb-faqja e tyre ka rënë ose është hakuar."
date: 2023-05
permalink: topics/website-not-working
parent: /sq/
order: 5
---

# Ueb-faqja ime ka rënë, çfarë po ndodh?

Një kërcënim me të cilin përballen shumë OJQ, media të pavarura dhe blogerë është që zëri i tye të heshtet sepse ueb-faqja e tyre ka rënë ose është hakuar. Ky është një problem zhgënjyes dhe mund të ketë shumë shkaqe si mirëmbajtja e keqe e faqes në internet, hostimi jo i besueshëm, [script-kiddies](https://en.wikipedia.org/wiki/Script_kiddie), ngatërresa me një faqe interneti, një sulm DDOS ose hakimi i ueb-faqes. Ky seksion i Veglërisë së Ndihmës së Parë Digjitale do t'ju njoftojë me disa hapa themelorë për t'i diagnostikuar problemet e mundshme duke përdorur materiale nga [My Website is Down](https://github.com/OpenInternet/MyWebsiteIsDown/blob/master/MyWebsiteIsDown.md).

Është e rëndësishme të dini se ka shumë arsye pse faqja juaj e internetit mund të bjerë. Ato mund të jenë probleme teknike në
kompaninë që e hoston ueb-faqen ose në një sistem të menaxhimit të përmbajtjes (CMS), si Joomla ose Wordpress, që nuk është përditësuar. Gjetja e problemit dhe zgjidhjeve të mundshme për problemin e faqes suaj të internetit mund të jetë e rëndë. Është praktikë e mirë të **kontaktoni webmasterin tuaj dhe ofruesin e shërbimit të hostimit të faqes në internet** pasi të keni diagnostikuar këto probleme të zakonshme më poshtë. Nëse asnjë nga këto opsione nuk është e disponueshme për ju, [kërkoni ndihmë nga një organizatë që keni besim](#website-down_end).

## Consider

Për fillim, merrni parasysh:

- Kush e zhvilloi faqen tuaj të internetit? A janë në dispozicion për të ndihmuar?
- A është zhvilluar duke përdorur WordPress apo një platformë tjetër të njohur CMS?
- Kush është ofruesi juaj i shërbimit të hostimit të faqes në internet (web-hosting)? Nëse nuk e dini, mund të përdorni një [shërbim në internet WHOIS](https://whois.domaintools.com) për t’ju ndihmuar.

## Workflow

### error-message

#### A shihni mesazhe gabimi?

##### Options

- [Po, po shoh mesazhe gabimi](#error-message-yes)
- [Jo](#hosting-message)

### hosting-message

#### A po shihni një mesazh nga ofruesi juaj i shërbimit të hostimit të faqes në internet?

##### Options

- [Po, shoh një mesazh nga ofruesi im i shërbimit të hostimit të faqes në internet](#hosting-message-yes)
- [Jo](#site-not-loading)

### site-not-loading

#### A nuk hapet aspak faqja juaj?

##### Options

- [Po, faqja nuk hapet aspak](#site-not-loading-yes)
- [Jo, faqja e internetit hapet](#hosting-working)

### hosting-working

#### A funksionon faqja e internetit të ofruesit tuaj të hostimit, por faqja e juaj e internetit nuk hapet?

##### Options

- [Po, mund të hyj në faqen e internetit të ofruesit tim të hostimit](#hosting-working-yes)
- [Jo](#similar-content-censured)

### similar-content-censored

#### A jeni në gjendje të vizitoni faqe të tjera me përmbajtje si faqja juaj e internetit?

##### Options

- [Nuk mund të vizitoj as faqe të tjera me përmbajtje të ngjashme](#similar-content-censored-yes)
- [Faqet e tjera funksionojnë mirë. Vetëm nuk mund ta vizitoj faqen time](#loading-intermittently)

### loading-intermittently

#### A po hapet faqja juaj me ndërprerje, apo jashtëzakonisht ngadalë?

##### Options

- [Po, faqja ime po hapet me ndërprerje ose është e ngadaltë](#loading-intermittently-yes)
- [Jo, faqja ime e internetit po hapet, por mund të jetë hakuar](#website-defaced)

### website-defaced

#### A hapet faqja juaj e internetit, por pamja dhe përmbajtja nuk janë ato që prisni të shihni?

##### Options

- [Po, faqja ime nuk e ka përmbajtjen/paraqitjen e pritur](#defaced-attack-yes)
- [Jo](#website-down_end)

### error-message-yes

#### Po, po shoh mesazhe gabimi

> Ky mund të jetë një **_problem softuerik_** - duhet të reflektoni mbi çdo ndryshim të fundit që ju ose ekipi juaj mund të keni bërë dhe të kontaktoni web-masterin tuaj. Po t'i dërgoni web-masterit një pamje nga ekrani, lidhje të faqes me të cilën keni probleme, dhe çdo mesazh gabimi që shihni, do t'u ndihmojë atyre të kuptojnë se cili mund të jetë shkaku i problemit. Ju gjithashtu mund të kopjoni mesazhet e gabimit në një motor kërkimi për të parë nëse ka ndonjë mënyrë për rregullim të lehtë të problemit.

A ndihmoi kjo?

##### Options

- [Po](#resolved_end)
- [Jo](#website-down_end)

### hosting-message-yes

#### Po, shoh një mesazh nga ofruesi im i pritjes

> Faqja juaj mund të jetë hequr (offline) për arsye ligjore, [të drejtat e autorit](https://www.eff.org/issues/bloggers/legal/liability/IP), për arsye faturimi ose arsye të tjera. Kontaktoni ofruesin tuaj të hostimit për detaje të mëtejshme se pse ata e kanë pezulluar hostimin e faqes suaj në internet.

A ndihmoi kjo?

##### Options

- [Po](#resolved_end)
- [Jo, kam nevojë për mbështetje ligjore](#legal_end)
- [Jo, kam nevojë për mbështetje teknike](#website-down_end)

### site-not-loading-yes

#### Faqja nuk po ngarkohet fare

> Kompania juaj e hostimit mund të ketë probleme, dhe në këtë rast ju mund të përballeni me **_problem të hostimit_**. A mund të vizitoni faqen e internetit të kompanisë suaj të hostimit? Vini re se kjo **nuk** është seksioni i administratorit të faqes suaj të internetit, por kompania ose organizata e cila e hoston faqen tuaj.
>
> Shikoni ose kërkoni blogun "status" (p.sh. status.dreamhost.com), e gjithashtu kërkoni në twitter.com përdoruesit e tjerë që diskutojnë për probleme të ngjashme në kompaninë e hostimit - një kërkim i thjeshtë si "(emri i kompanisë) down" shpesh mund të zbulojë nëse edhe shumë të tjerë e kanë të njëjtin problem.

A ndihmoi kjo?

##### Options

- [Po](#resolved_end)
- [Jo, faqja e internetit e ofruesit tim të hostimit nuk ka rënë](#hosting-working-yes)
- [Jo, kam nevojë për mbështetje teknike](#website-down_end)

### hosting-working-yes

#### Faqja e internetit e ofruesit tim të hostimit nuk ka rënë

> Kontrolloni nëse faqja e internetit [Ka rënë tek të gjithë apo vetëm tek unë](https://downforeveryoneorjustme.com/) - faqja juaj mund të jetë aktive, por ju nuk mund ta shihni atë.
>
> Nëse faqja juaj është në rregull, por ju nuk mund ta shihni, kjo me gjasë është **_problem i rrjetit_**: lidhja juaj e internetit mund të ketë probleme ose mund ta bllokojë hyrjen tuaj në faqen tuaj.

Keni nevojë për ndihmë të mëtejshme?

##### Options

- [Jo, nuk kam problem](#resolved_end)
- [Kam nevojë për ndihmë për të rivendosur lidhjen time të rrjetit](#website-down_end)
- [Ky nuk është një problem rrjeti dhe faqja ime e internetit nuk funksionon për të gjithë](#similar-content-censored)

### similar-content-censored-yes

#### Ky nuk është një problem rrjeti dhe faqja ime e internetit nuk funksionon për të gjithë

> Provoni të vizitoni faqet e internetit me përmbajtje të ngjashme me faqen tuaj të internetit. Provoni gjithashtu të përdorni [Tor](https://www.torproject.org/projects/gettor.html), [Psiphon](https://psiphon.ca/en/index.html) ose një VPN për të hyrë në faqen tuaj.
>
> Nëse mund të vizitoni faqen tuaj nëpërmjet Tor, Psiphon ose një VPN, keni një **_problem censurimi_** - jeni ende online për pjesët tjera të botës, por jeni duke u censuruar në vendin tuaj.

Dëshironi të bëni diçka për këtë censurë?

##### Options

- [Po, do të doja ta raportoja këtë publikisht dhe kam nevojë për mbështetje për fushatën time të avokimit](#advocacy_end)
- [Po, do të doja të gjeja një zgjidhje për ta bërë faqen time të aksesueshme](#website-down_end)
- [Jo](#resolved_end)

### loading-intermittently-yes

#### Po, faqja ime po ngarkohet me ndërprerje ose është e ngadaltë

> Faqja juaj mund të jetë stërngarkuar nga numri dhe shpejtësia e kërkesave për qasje në të në një kohë të shkurtër - ky është një **_problem i performancës_**.
>
> Kjo mund të jetë "mirë" pasi faqja juaj është bërë më e popullarizuar dhe thjesht ka nevojë për disa përmirësime për t'iu përgjigjur sa më shumë lexuesve - kontrolloni analitikën e faqes tuaj për një model afatgjatë të rritjes. Kontaktoni web-masterin tuaj ose ofruesin e shërbimit të hostimit për udhëzime. Shumë sisteme të njohura të blogimit dhe CMS (Joomla, Wordpress, Drupal ...) kanë shtojca për të ndihmuar në ruajtjen e faqes suaj lokalisht dhe për të integruar [Rrjetet e shpërndarjes së përmbajtjes](https://en.wikipedia.org/wiki/Content_delivery_network), që mund ta përmirësojë në mënyrë dramatike performancën dhe qëndrueshmërinë e faqes. Shumë nga zgjidhjet e mëposhtme mund të ndihmojnë gjithashtu në zgjidhjen e problemeve të performancës.
>
> Nëse jeni duke u përballur me një **problem të rëndë të performancës**, faqja juaj mund të jetë viktimë e një [**sulmi "distributed denial of service"**](https://ssd.eff.org/en/glossary/distributed-denial-service-attack) (ose DDoS). Ndiqni hapat e mëposhtëm për të zbutur një sulm të tillë:
>
> - Hapi 1: Kontaktoni një person të besuar që mund të ndihmojë me faqen tuaj të internetit (web-masterin tuaj, njerëzit që ju ndihmuan të krijoni faqen tuaj, punonjësit tuaj të brendshëm ose ofruesin tuaj të hostimit).
>
> - Hapi 2: Kontaktoni kompaninë nga e cila e keni blerë emrin e domenit dhe ndryshoni "Time to Live" ose TTL në 1 orë (mund të gjeni udhëzime se si ta bëni këtë në faqet e internetit të shumë ofruesve të hostimit, si për shembull [GoDaddy](https://www.godaddy.com/garage/configuring-and-working-with-domains-dns/) ose [Gandi.net](https://docs.gandi.net/en/domain_names/faq/dns_records.html)). Kjo mund t'ju ndihmojë të ridrejtoni faqen tuaj shumë më shpejt pasi të sulmohet (parazgjedhja është 72 orë ose tre ditë). Ky cilësim do të gjendet shpesh në veçoritë "advanced" të domenit tuaj, e ndonjëherë pjesë e të SRV ose Service records.
>
> - Hapi 3: Zhvendosni faqen tuaj në një shërbim për zbutjen e sulmeve DDoS. Filloni me:
>
> - [Deflect.ca](https://deflect.ca/)
> - [Projektin Mburoja (Shield) të Google](https://projectshield.withgoogle.com/landing)
> - [Projektin Galileo të CloudFlare](https://www.cloudflare.com/galileo)
>
> Për një listë të plotë të organizatave të besuara që mund të ndihmojnë në zbutjen e një sulmi DDoS, kaloni [në seksionin e fundit të kësaj sekuence ë veprimeve që adreson sulmet DDoS](../ddos_end).
>
> - Hapi 4: Sapo të keni rifituar kontrollin, rishikoni nevojat tuaja dhe vendosni midis një ofruesi të sigurt të hostimit ose thjesht të vazhdoni me shërbimin tuaj për zbutjen e sulmeve DDoS.

##### Options

- [Për një listë të plotë të organizatave të besuara që mund të ofrojnë hostim të sigurt, kaloni në hapin e fundit të kësaj sekuence të veprimeve që trajton çështjet e hostimit të faqeve në internet](#web-hosting_end).

### defaced-attack-yes

#### Po, faqja ime nuk ka përmbajtjen ose pamjen e pritur

> Shpërfytyrimi i faqes në internet është një praktikë ku një sulmues zëvendëson përmbajtjen ose pamjen vizuale të faqes me përmbajtjen e tij. Këto sulme zakonisht kryhen ose duke shfrytëzuar dobësitë në platformat CMS të pa mirëmbajtura, pa përditësimet më të fundit të sigurisë ose duke përdorur emrat e përdoruesve/fjalëkalimet e vjedhura të llogarisë së hostimit.
>
> - Hapi 1: Verifikoni që kjo është një marrje keqdashëse e faqes suaj të internetit. Një praktikë fatkeqe, por ligjore, është blerja e emrave të domenit të skaduar së fundmi për të "marrë përsipër" trafikun që ata kishin për qëllime reklamimi. Është shumë e rëndësishme të bëni në rregull/ në kohë pagesat për emrin e domenit.
> - Hapi 2: Nëse faqja juaj e internetit është shpërfytyruar, së pari rifitoni kontrollin e llogarisë tuaj të hyrjes në ueb-faqen tuaj dhe rivendosni fjalëkalimin e saj, shihni seksionin [Problemet e qasjes në llogari të Ndihmës së Parë Digjitale](/sq/topics/account-access-issues) për ndihmë.
> - Hapi 3: Bëni një kopje rezervë të faqes së internetit të shpërfytyruar, që mund të përdoret më vonë për hetimin e shpërfytyrimit.
> - Hapi 4: Shkyçeni përkohësisht faqen tuaj të internetit - përdorni një faqe të thjeshtë për të informuar se jeni duke "punuar në rregullimin e faqes".
> - Hapi 5: Përcaktoni se si është hakuar faqja juaj. Ofruesi juaj i hostimit mund të jetë në gjendje të ndihmojë. Problemet e zakonshme janë pjesë më të vjetra të faqes tuaj me skripte/mjete të personalizuara që ekzekutohen në to, sistemet e vjetëruara të administrimit të përmbajtjes dhe programimi sipas kërkesës me të meta të sigurisë.
> - Hapi 6: Rivendosni faqen tuaj origjinale nga kopjet rezervë. Nëse as ju, e as kompania juaj e hostimit nuk ka kopje rezervë, mund t'ju duhet të rindërtoni faqen tuaj të internetit nga e para! Vini re gjithashtu që nëse kopjet rezervë të vetme gjenden tek ofruesi juaj i hostimit, sulmuesi mund të jetë në gjendje t'i fshijë ato kur ta marrë kontrollin e faqes suaj!

A u ndihmuan këto rekomandime?

##### Options

- [Po](#resolved_end)
- [Jo](#website-down_end)

### website-down_end

#### Kam ende nevojë për ndihmë

> Nëse keni ende nevojë për ndihmë pas të gjitha pyetjeve që iu përgjigjët, mund të kontaktoni një organizatë të besuar dhe të kërkoni mbështetje.
>
> Para se t’i kontaktoni, ju lutemi bëni vetes pyetjet e mëposhtme:
>
> - Si është e strukturuar dhe si mirëmbahet kompania/organizata? Çfarë lloje të verifikimit ose raportimit kërkohet të bëjnë, nëse bëjnë?
> - Konsideroni se në cilin vend/shtet ata kanë prani ligjore, dhe cilat ligje dhe kërkesa të tjera ligjore duhet t'i respektojnë ato.
> - Çfarë regjistrash krijohen dhe për sa kohë janë në dispozicion?
> - A ka kufizime në lidhje me llojin e përmbajtjes që shërbimi do ta hostojë/përcjellë dhe a mund të kenë ato ndikim në faqen tuaj?
> - A ka kufizime se në cilat vende ato mund t’i ofrojnë shërbimet e tyre?
> - A pranojnë ata një formë pagese që mund të përdorni? A mund të përballoni shërbimin e tyre?

- Komunikimet e sigurta - duhet të jeni në gjendje të identifikoheni në mënyrë të sigurt dhe të komunikoni privatisht me ofruesin e shërbimit.
  > - A ekziston mundësi për vërtetimin me dy faktorë, për të përmirësuar sigurinë e qasjes së administratorit? Kjo ose politikat e ngjashme për qasje të sigurt mund të ndihmojë në zvogëlimin e kërcënimit nga format e tjera të sulmeve kundër faqes suaj të internetit.
  > - Në cilin lloj mbështetjeje të vazhdueshme do të keni qasje? A ka ndonjë kosto shtesë për mbështetje, dhe/ose a do të merrni mbështetje të mjaftueshme nëse përdorni nivelin 'falas' të mbështetjes?
  > - A mund ta 'testoni' ueb-faqen tuaj në version testues te hosti përpara se ta bëni publike?

Këtu është një listë e organizatave që mund të ndihmojnë me problemin tuaj:

[orgs](:organisations?services=web_protection)

### legal_end

#### Jo, kam nevojë për mbështetje ligjore

> Nëse ueb-faqja juaj ka rënë për shkak të arsyeve ligjore dhe keni nevojë për mbështetje ligjore, ju lutemi kontaktoni një organizatë që mund të ndihmojë:

[orgs](:organisations?services=legal)

### advocacy_end

#### Po, do të doja ta raportoja këtë publikisht dhe kam nevojë për mbështetje për fushatën time të avokimit

> Nëse dëshironi mbështetje për të filluar një fushatë kundër censurës, ju lutemi kontaktoni organizatat që mund të ndihmojnë me përpjekjet e avokimit:

[orgs](:organisations?services=advocacy)

### ddos_end

#### Kam nevojë për ndihmë me një sulm DDoS

Nëse keni nevojë për ndihmë për të zbutur një sulm DDoS kundër faqes tuaj të internetit, ju lutemi referojuni organizatave të specializuara në zbutjen e DDoS:

[orgs](:organisations?services=ddos)

### web-hosting_end

#### Kam nevojë për ndihmë me hostimin e faqes në internet

> Nëse jeni duke kërkuar një organizatë të besuar për të hostuar faqen tuaj të internetit në një server të sigurt, ju lutemi shikoni listën më poshtë.
>
> Përpara se të vini në kontakt me këto organizata, ju lutemi mendoni për këto pyetje:
>
> - A ofrojnë ata mbështetje të plotë për ta zhvendosur faqen tuaj në shërbimin e tyre?
> - A janë shërbimet të barabarta ose më të mira se ofruesi juaj aktual i hostimit, të paktën për mjetet/shërbimet që përdorni? Gjërat kryesore që duhet të kontrolloni janë:
> - Pultet e menaxhimit si cPanel
> - Llogaritë e e-maile-ve (sa, kuotat, qasja përmes SMTP, IMAP)
> - Bazat e të dhënave (sa, llojet, qasja)
> - Qasja në distancë nëpërmjet SFTP/SSH
> - Mbështetje për gjuhën e programimit (PHP, Perl, Ruby, akses në cgi-bin access...) ose CMS (Drupal, Joomla, Wordpress…) që përdor faqja juaj

Këtu është një listë e organizatave që mund t'ju ndihmojnë me hostimin e faqes në internet:

[orgs](:organisations?services=web_hosting)

### resolved_end

#### Çështja ime është zgjidhur

Shpresojmë se ky udhëzues i DFAK ishte i dobishëm. Ju lutemi na jepni komente [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

- **Kopjet rezervë** - Përveç shërbimeve dhe sugjerimeve më poshtë, është gjithmonë ide e mirë të siguroheni se keni kopje rezervë (që i ruani diku tjetër nga vendi ku është faqja juaj e internetit!). Shumë hostë dhe platforma të ueb-faqeve e përfshijnë këtë, por është mirë që të keni kopje shtesë, off-line.

- ** Përditësoni softuerin rregullisht** - Nëse jeni duke përdorur një Sistem të Menaxhimit të Përmbajtjes (CMS) si WordPress ose Drupal, sigurohuni që teknologjia e faqes suaj të internetit të përditësohet me softuerin më të fundit, veçanërisht nëse ka pasur përditësime të sigurisë .

- **Monitorimi** - Ka shumë shërbime që mund të kontrollojnë vazhdimisht faqen tuaj dhe t'ju dërgojnë e-mail ose SMS nëse ato nuk janë funksionale. [Ky artikull](https://www.websiteplanet.com/blog/website-down-top-website-monitoring-tools/) ofron një listë prej 9 shërbimeve më të popullarizuara.. Kini parasysh se e-maili ose numri i telefonit që përdorni për monitorim do të lidhet qartë me menaxhimin e faqes së internetit.

### Resources

- [EFF: Mirëmbajta funksionale e ueb-faqes suaj](https://www.eff.org/keeping-your-site-alive)
- [CERT.be: Masat proaktive dhe reaktive kundër sulmeve DDoS](https://ccb.belgium.be/sites/default/files/DDoS-proactive-reactive.pdf)
- [Sucuri: Çfarë është një sulm DDoS?](https://sucuri.net/guides/what-is-a-ddos-attack/)
