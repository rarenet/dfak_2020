---
layout: page
title: "Kujdesi për veten dhe komunitetin"
author: FP, Metamorphosis Foundation
language: sq
summary: "Ngacmimet në internet, kërcënimet dhe llojet e tjera të sulmeve digjitale mund të krijojnë ndjenja të mbingarkimit dhe gjendje emocionale shumë delikate: mund të ndjeheni fajtorë, të turpëruar, të shqetësuar, të zemëruar, të hutuar, të pafuqishëm, apo edhe mund të keni frikë për mirëqenien tuaj psikologjike ose fizike."
date: 2018-09-05
permalink: self-care
parent: Home
sidebar: >
  <h3>Lexoni më shumë se si të mbroni veten dhe ekipin tuaj nga ndjenjat mbingarkuese:</h3>

  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Vetë-kujdesi për aktivistët: Ruajtja e burimit tuaj më të vlefshëm</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Vetë-kujdesi që të mund të vazhdoni të mbroni të drejtat e njeriut</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Vetë-kujdesi për personat që përjetojnë ngacmim</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Moduli i trajnimit të grave për vetë-kujdes ndaj sigurisë digjitale</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">Mirëqenia dhe komuniteti</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Njëzet mënyra për të ndihmuar dikë që është duke u ngacmuar në internet</a></li>
    <li><a href="https://www.hrresilience.org/">Projekti i Qëndrueshmërisë së të Drejtave të Njeriut</a></li>

    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_AH_EN+001/about">Kursi i mësimit në internet i Totem-Project: Kujdesi për shëndetin tuaj mendor (kërkon regjistrim)</a></li>
     <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_PAP_EN+001/about">Kursi i mësimit në internet i Totem-Project: Ndihma e Parë Psikologjike (kërkon regjistrim)</a></li>
     <li><a href="https://holistic-security.org/chapters/prepare/1-2-individual-responses-to-threat.html">Reagimet individuale ndaj kërcënimit</a></li>
     <li><a href="https://holistic-security.org/chapters/prepare/1-4-team-and-peer-responses-to-threat.html">Reagimet e ekipit dhe të kolegëve ndaj kërcënimit</a></li>
     <li><a href="https://holistic-security.org/chapters/prepare/1-5-communicating-about-threats-in-teams-and-organisations.html">Komunikimi në ekipe dhe organizata</a></li>
  </ul>
---

# Ndiheni të mbingarkuar?

Ngacmimet në internet, kërcënimet dhe llojet e tjera të sulmeve digjitale mund të krijojnë ndjenja të mbingarkimit dhe gjendje emocionale shumë delikate për ju ose ekipin tuaj: mund të ndiheni fajtorë, të turpëruar, të shqetësuar, të zemëruar, të hutuar, të pafuqishëm, apo edhe mund të keni frikë për mirëqenien tuaj psikologjike ose fizike. Dhe këto ndjenja mund të jenë mjaft të ndryshme në një ekip.

Nuk ka një mënyrë "të duhur" se si duhet ndier, pasi gjendja juaj e cenueshmërisë dhe çfarë do të thotë informacioni juaj personal për ju është e ndryshme nga një person te tjetri. Çdo emocion është i justifikuar, dhe nuk duhet të shqetësoheni nëse reagimi juaj është ai i duhuri apo jo.

Gjëja e parë që duhet të mbani mend është se fajësimi i vetes ose i dikujt tjetër brenda një ekipi nuk ndihmon në reagimin ndaj emergjencave. Mund të dëshironi të kontaktoni dikë të besuar, i cili mund t'ju mbështesë ju ose ekipin tuaj në adresimin e një emergjence nga pikëpamja teknike dhe emocionale.

Për të zbutur një sulm në internet, do t'ju duhet të mblidhni informacion për atë që ka ndodhur, por këtë nuk keni nevojë ta bëni vetë - nëse keni një person të cilit i besoni, mund t'i kërkoni që t'ju mbështesë duke i ndjekur udhëzimet në këtë ueb-faqe, ose t'u jepni atyre qasje në pajisjet ose llogaritë tuaja për të mbledhur informacion për ju. Ose mund të kërkoni mbështetje emocionale ose teknike gjatë këtij procesi.

Nëse ju ose ekipi juaj keni nevojë për mbështetje emocionale për trajtimin e një urgjence digjitale (ose gjithashtu për mbështetje të mëtejshme), [Programi i Shëndetit Mendor në Komunitet i Team ComUNITY](https://www.communityhealth.team/) ofron shërbime psiko-sociale në formate të ndryshme për rastet akute dhe në afat të gjatë dhe mund të ofrojë mbështetje në gjuhë dhe kontekste të ndryshme.
