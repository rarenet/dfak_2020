---
layout: page
title: "Self & Community Care"
author: Peter Steudtner, Flo Pagano
language: en
summary: "Online harassment, threats, and other kinds of digital attacks can create overwhelming feelings and very delicate emotional states: you might feel guilty, ashamed, anxious, angry, confused, helpless, or even fear for your psychological or physical well-being."
date: 2018-09-05
permalink: self-care
parent: Home
sidebar: >
  <h3>Read more about how to protect yourself and your team against overwhelming feelings:</h3>

  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Self-Care for Activists: Sustaining Your Most Valuable Resource</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Caring for yourself so you can keep defending human rights</a></li>
    <li><a href="https://www.frontlinedefenders.org/en/resources-wellbeing-stress-management">Resources for Well-being & Stress Management</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Self-Care for People Experiencing Harassment</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Cyberwomen self-care training module</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">Wellness and Community</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Twenty ways to help someone who's being bullied online</a></li>
    <li><a href="https://www.hrresilience.org/">Human Rights Resilience Project</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_AH_EN+001/about">Totem-Project Online Learning Course: Taking care of your mental health (requires registration)</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_PAP_EN+001/about">Totem-Project Online Learning Course: Psychological First Aid (requires registration)</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-2-individual-responses-to-threat.html">Individual Responses to Threat</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-4-team-and-peer-responses-to-threat.html">Team and Peer Responses to Threat</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-5-communicating-about-threats-in-teams-and-organisations.html">Communicating ain Teams and Organisations</a></li>
    </ul>
---

# Feeling Overwhelmed?

Online harassment, threats, and other kinds of digital attacks can create overwhelming feelings and very delicate emotional states for you or your team: you might feel guilty, ashamed, anxious, angry, confused, helpless, or even fear for your psychological or physical well-being. And these feelings can be quite different in a team.

There is no "right" way to feel, as your state of vulnerability and what your personal information means to you is different from person to person. Any emotion is justified, and you shouldn't worry about whether or not your reaction is right.

The first thing you should remember is that blaming oneself or anyone else within a team does not help react to emergencies. You might want to reach out to someone trusted who can support you or your team in addressing an emergency from a technical as well as an emotional perspective.

To mitigate an online attack, you will need to gather information on what has happened, but you don't have to do it on your own. If you have a person you trust, you can ask them to support you while following the instructions in this website, or give them access to your devices or accounts to gather information for you. Or you can reach out for emotional or technical support during this process.

If you or your team need emotional support for dealing with a digital emergency (or also as a follow-up), the [Community Mental Health Program of Team CommUNITY](https://www.communityhealth.team/) offers psychosocial services in different formats for acute cases and in the long run and can provide support in various languages and contexts.
