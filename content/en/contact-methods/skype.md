---
layout: contact-method
title: Skype
author: mfc
language: en
summary: Contact methods
date: 2018-09
permalink: /contact-methods/skype.md
parent: /en/
published: true
---

The content of your message as well as the fact that you contacted the organization may be accessible by governments or law enforcement agencies.