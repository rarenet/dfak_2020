---
layout: topic
title: "I received a suspicious message"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: en
summary: "I received a suspicious message, link or email, what should I do about it?"
date: 2019-03-12
permalink: topics/suspicious-messages
parent: Home
order: 4
---

# I Received A Suspicious Message

The most common form of suspicious emails is phishing emails, but you may receive suspicious messages to social media accounts and/or messaging applications too. Phishing messages aim to trick you into giving up your personal, financial, or account information. They may ask you to visit a fake website or call a fake customer service number. Phishing messages can also contain attachments that install malicious software on your computer when opened.

If you are not certain about the authenticity of the message you received or what to do about it, you can use the following questionnaire as a guiding tool to further diagnose the situation or to share the message with external trusted organizations that will provide you with a more detailed analysis of your message.

Keep in mind that receiving a suspicious email does not necessarily mean that your account has been compromised. If you think an email or message is suspicious, don't open it. Don't reply to the message, don't click any links, and don't download any attachments.

## Workflow

### start

#### Have you performed any action on the message or link?

##### Options

- [I clicked a link](#link-clicked)
- [I entered credentials](#account-security_end)
- [I downloaded a file](#device-security_end)
- [I replied with information](#reply-personal-info)
- [I have taken no action yet](#do-you-know-sender)

### do-you-know-sender

#### I have taken no action yet

Do you recognise the sender of the message? Be aware that the sender of the message may be [spoofed](https://en.wikipedia.org/wiki/Email_spoofing) to appear to be someone you trust.

##### Options

- [It is a person or organization that I know](#known-sender)
- [It is a service provider (such as an email provider, hosting, social media or bank)](#service-provider)
- [The message was sent by an unknown person or organization](#share)

### known-sender

#### It is a person or organization that I know

> Can you reach the sender using another communication channel? For example if you received an email, can you verify it directly with the sender by Signal, WhatsApp or phone? Be sure to use an existing contact method. You cannot necessarily trust a phone number listed in the suspicious message.

Did you confirm that the sender is the one that sent you this message?

##### Options

- [Yes](#resolved_end)
- [No](#share)

### service-provider

#### It is a service provider (such as an email provider, hosting, social media or bank

> In this scenario, a service provider is any company or brand that provides services you use or are subscribed to. This list can contain your email provider (Google, Yahoo, Microsoft, ProtonMail), your social media provider (Facebook, Twitter, Instagram), or online platforms that have your financial information (Paypal, Amazon, banks, Netflix).
>
> Is there a way that you can check if the message is authentic? Many service providers will also provide copies of notifications or other documents on your account page. For example, if the message is from Facebook, it should be included in your [list of notification emails](https://www.facebook.com/settings?tab=security&section=recent_emails), or if it is from your bank, you can call your customer service.

Choose one of the below options:

##### Options

- [I was able to verify it is a legitimate message from my service provider](#resolved_end)
- [I wasn't able to verify the message](#share)
- [I am not subscribed to this service and/or expecting a message from them](#share)

### link-clicked

#### I clicked a link

> In some suspicious messages, the links can take you to fake login pages that will steal your credentials or other type of pages that might steal your personal or financial information. Sometimes, the link might ask you to download attachments that install malicious software on your computer when opened. The link can also take you to a specially prepared website that may try to infect your device with malicious or spying software.

Can you tell what happened after you clicked on the link?

##### Options

- [It asked me to enter credentials](#account-security_end)
- [It downloaded a file](#device-security_end)
- [Nothing happened, but I am not sure](#clicked-but-nothing-happened)

### clicked-but-nothing-happened

#### Nothing happened, but I am not sure

> The fact that you clicked on a suspicious link and you didn't notice any strange behaviour doesn't mean that no malicious action took place in the background. There are some scenarios that you should think about. The least concerning is that the message you received was spam and used for advertising purposes. In this case, some ads may have popped up. In some cases, these ads could be malicious, too.
>
> In the worst-case scenario, by clicking on the link an exploit took place to execute a malicious command on your system. If this happened, this may be due to the fact that your browser is not up-to-date and has a vulnerability allowing this exploit. In rare cases where you browser is up-to-date and this scenario happened, the exploited vulnerability could not be known. In both cases, your device could start to act suspiciously.
>
> In other scenarios, by visiting this link, you may have been a victim of a [cross-site script attack (or XSS)](https://en.wikipedia.org/wiki/Cross-site_scripting). The result of this attack will be the stealing of your cookie used to authenticate you to the visited website, so the attacker will be able to log into the site with your username. Depending on the security of the site, the attacker may or may not be able to change the password. This becomes more serious if the website that is vulnerable to XSS is a site you manage, because, in such cases, the attacker will be able to authenticate as the admin of your website. In order to identify an XSS attack, check if the link you clicked contains a [script string](https://owasp.org/www-community/attacks/xss/). This could also be encoded in HEX or Unicode.

##### Options

- [Some ads popped up. I'm not sure if they are malicious or not](#suspicious-device_end)
- [Yes, my browser is not up to date and/or my device started acting suspiciously after I clicked the link](/topics/device-acting-suspiciously)
- [There is a script in the link or it is partially encoded](#cross-site-script_1)
- [No script could be identified](#suspicious-device_end)

### cross-site-script_1

#### Is the site you visited a site you have an account in?

##### Options

- [Yes](#account-security_end)
- [No](#cross-site-script-2)

### cross-site-script-2

#### Is the site you ended up in a website you manage?

##### Options

- [I manage the website that the link I clicked on led to](#cross-site-script-admin-compromised_end)
- [No](#cross-site-script-3)

### cross-site-script-admin-compromised_end

#### I manage the website that the link I clicked on led to

> In this case, the attacker may have a valid cookie that allows them to access your admin account. The first thing is to log into your administration interface and kill any active session or simply change the password. Also, you should check if the attacker uploaded any artifacts to your site and/or posted any malicious content, and if so remove it.

The following organizations can help investigate and respond to this incident:

[orgs](:organisations?services=forensic)

### cross-site-script-3

#### No, I do not manage this website

> This should be fine. However, in some rare cases, XSS could be used to use your browser to launch other attacks.

##### Options

- [I want to assess if my device got infected](/topics/device-acting-suspiciously)
- [I think I'm fine](#resolved_end)

### reply-personal-info

#### I replied with information

> Depending on the type of information you shared, you may need to take immediate action.

What kind of information did you share?

##### Options

- [I shared confidential account information](#account-security_end)
- [I shared public information](#share)
- [I'm not sure how sensitive the information was and I need help](#help_end)

### share

#### Sharing your information or the suspicious message

> Sharing your suspicious message can help protect your colleagues and community who may also be affected. You may also want to reach out for help to someone you trust to advise you if the message is dangerous. Consider sharing the message with organizations that can analyse them.
>
> To share your suspicious message, make sure to include the message itself as well as information about the sender. If the message was an email, please make sure to include the full email including headers using the [following guide by the Computer Incident Response Center Luxembourg (CIRCL)](https://www.circl.lu/pub/tr-07/).

Do you need further help?

##### Options

- [Yes, I need more help](#help_end)
- [No, I've solved my problem](#resolved_end)

### device-security_end

#### I downloaded a file

> In case some files were downloaded to your device, your device may be at risk!

Please contact the organizations below who can support you. Afterward, please [share your suspicious message](#share).

[orgs](:organisations?services=device_security)

### account-security_end

#### I entered or provided account information

> In case you entered your credentials, or you were the victim of a cross-site script attack, your accounts may be at risk!
>
> If you think your account is compromised, it is recommended you also follow the Digital First Aid Kit workflow on [compromised accounts](/topics/account-access-issues).
>
> You should also inform your community about this phishing campaign, and share the suspicious message with organizations that can analyse them.

Would you like to share information on the message you received or do you need further assistance before?

##### Options

- [I want to share the suspicious message](#share)
- [I need more help to secure my account](#account_end)
- [I need more help to analyze the message](#analysis_end)

### suspicious-device_end

#### I am not sure what happened

> If you clicked on a link and aren't sure what happened, your device may have been infected without you noticing. If you want to explore this possibility or have the feeling your device may be infected, go to the workflow ["My device is acting suspiciously"](/topics/device-acting-suspiciously).

If you need immediate help because your device is acting suspiciously, you can contact the organizations below who can support you. Afterward, please [share your suspicious message](#share).

[orgs](:organisations?services=device_security)

### help_end

#### I need more help

> You should seek help from your colleagues or others to better understand the risks of the information you shared. Other people in your organization or network may also have received similar requests.

Please contact the organizations below who can support you. Afterward, please [share your suspicious message](#share).

[orgs](:organisations?services=digital_support)

### account_end

#### I need more help to secure my account

If your account has been compromised and you need help to secure it, please contact the organizations below who can support you.

[orgs](:organisations?services=account)

### analysis_end

#### I need more help to analyze the message

The following organizations can receive your suspicious message and investigate it further for you:

[orgs](:organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

#### Yes, My issue has been resolved

Hopefully, this DFAK guide was useful. Please give feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

The first rule to remember: Never give out any personal information in emails or other messaging tools. No institution, bank or otherwise will ever ask for this information via email, social networking platforms or messaging apps. It may not always be easy to tell whether an email, message or website is legitimate, but there are some tips that can help you assess the email you have received.

- Sense of urgency: suspicious emails typically warn of a sudden change to an account and ask you to act immediately to verify your account.
- In the body of an email, you might see questions asking you to “verify” or “update your account” or “failure to update your records will result in account suspension”. It is usually safe to assume that no credible organization to which you have provided your information will ever ask you to re-enter it, so do not fall for this trap.
- Beware of unsolicited messages, attachments, links and login pages.
- Watch out for spelling and grammar errors.
- In emails, click to view the full sender address, not only the displayed name.
- Be mindful of shortened links - since you can't check the final destination, they could take you to a malicious website.
- When you hover your mouse over a link, the actual URL you are being directed to is displayed in a popup or at the bottom of your browser window.
- Email headers, including the "from:" value, could be carefully crafted to look like they are legitimate. By examining the SPF and DKIM headers, you can tell respectively if an IP address is (or is not) allowed to send emails on behalf of the sender's domain, and if the headers or the content have been changed in transit. In a legitimate email, the [SPF](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-spf) and [DKIM](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-dkim) values should always be 'PASS', otherwise an email cannot be trusted. The reason is that the email is spoofed or, in rare cases, the mail server is not configured correctly.
- [Digital signatures](https://www.gnupg.org/gph/en/manual/x135.html) can tell us if an email has been sent by the legitimate sender and if it has been modified or not along the way. If the email is signed with OpenPGP, check if the signature is verified or not. In order to verify a signature, you will need to install OpenPGP and import the public key associated with the ID in the signature of the message. Most of the modern email clients that support digital signatures will automate the verification for you and tell you through their user interface if a signature is verified or not.
- A compromised account could issue a malicious email or message that could verify all the conditions above and look legitimate. However, usually, the content of the message will be unusual. If the content of the email message seems strange, it is always a good idea to check with the legitimate sender through a different channel of communication before taking any action.
- It is always a good practice to read and write your emails in plain text. HTML-based emails could be rendered in a way that hides malicious code or URLs. You can find instructions on how to disable HTML in different email clients on [this website](https://useplaintext.email/).
- Use the latest version of the operating system on your phone or computer (check version for [Android](https://en.wikipedia.org/wiki/Android_version_history), [iOS](https://en.wikipedia.org/wiki/IOS_version_history), [macOS](https://en.wikipedia.org/wiki/MacOS_version_history) and [Windows](https://en.wikipedia.org/wiki/Microsoft_Windows)).
- Update as soon as possible your operating system and all the apps/programs you have installed, especially those which receive information (browsers, messaging and chatting apps/programs, email clients, etc.). Remove all apps/programs you do not need.
- Use a reliable browser (e.g. Mozilla Firefox). Increase your browser security by reviewing the extensions/add-ons installed in your browser. Leave only those you trust (for example: [Privacy Badger](https://privacybadger.org/), [uBlock Origin](https://ublockorigin.com), [Facebook Container](https://addons.mozilla.org/en-US/firefox/addon/facebook-container/), [Cookie AutoDelete](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete), [NoScript](https://noscript.net/)).
- Make regular secure backups of your information.
- Protect your accounts with strong passwords, 2-factor authentication and secure settings.

### Resources

Here are a number of resources to spot suspicious messages and avoid being phished.

- [Citizen Lab: Communities at risk - Targeted Digital Threats Against Civil Society](https://targetedthreats.net)
- [Surveillance Self-Defense: How to Avoid Phishing Attacks](https://ssd.eff.org/en/module/how-avoid-phishing-attacks)
- [Google message header analysis tool](https://toolbox.googleapps.com/apps/messageheader/)
