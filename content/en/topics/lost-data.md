---
layout: topic
title: "I lost my data"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: en
summary: "What to do in case you lost some data."
date: 2019-03-12
permalink: topics/lost-data
parent: Home
order: 8
---

# I Lost My Data

Digital data can be very ephemeral, and unstable and there are many ways you can lose it. Physical damage to your devices, termination of your accounts, erroneous deletion, software updates and software crashes can all propel a loss of data. Besides, sometimes you might not be aware of how or if your backup system works or simply have forgotten about your credentials or the route to find or recover your data.

This section of the Digital First Aid Kit will walk you through some basic steps to diagnose how you might have lost data and potential mitigation strategies to recover it.

_In this section, we are focusing on device-based data. For online content and credentials, you can follow the [Digital First Aid Kit workflow on account access issues](topics/account-access-issues)._

Here is a questionnaire to identify the nature of your problem and find possible solutions.

## Workflow

### entry-point

#### What type of data did you lose?

> In this section, we are mostly focusing on device-based data. For online content and credentials, we will be directing you to other sections of the Digital First Aid Kit.
>
> Devices include computers, mobile devices, external hard disks, USB sticks, and SD cards.

What type of data did you lose?

##### Options

- [Online content](/topics/account-access-issues)
- [Credentials](/topics/account-access-issues)
- [Content on devices](#content-on-device)

### content-on-device

#### Content on devices

> Often, your "lost" data is data that has simply been deleted - either accidentally or intentionally. On your computer, check your Recycle Bin or Trash. On Android devices, you might find the lost data in the LOST.DIR directory. If the missing or lost files cannot be located in the Recycle Bin or Trash of your operating system, it is possible that they haven't been deleted but are in a different location than you expected. Try the search function in your operating system to locate them.
>
> Try searching for the exact name of your lost file. If that does not work, or if you are not sure of the exact name, you can try a wildcard search (i.e. if you lost a docx file but aren't sure of the name, you can search `*.docx`), which will pull up only files with the `.docx` extension. Sort by the _Date Modified_ in order to quickly locate the most recent files when using the wildcard search option.
>
> In addition to searching for the lost data on your device, ask yourself if you have a backup of that data or if you have emailed it to or shared it with someone (including yourself) or added it to your cloud storage at any point. If that is the case, you can perform a search there and may be able to retrieve it or some version of it.
>
> To increase your chances of recovering data, stop editing and adding information to your devices immediately. If you can, connect the drive as an external device (e.g. over USB) to a separate, secure computer to search and restore information. Writing to the drive (e.g. downloading or creating new documents) might decrease the chances of finding and restoring lost data.
>
> Check also hidden files and folders, as the data you have lost might be there. Here is how you can do it on [Mac](https://www.macworld.co.uk/how-to/mac-software/show-hidden-files-mac-3520878/) and [Windows](https://support.microsoft.com/en-us/windows/view-hidden-files-and-folders-in-windows-97fbc472-c603-9d90-91d0-1166d1d9f4b5). For Linux, go to your home directory in a file manager and press Ctrl + H.

How did you lose your data?

##### Options

- [The device where the data was stored suffered physical damage](#tech-assistance_end)
- [The device where the data was stored was stolen or lost](#device-lost-theft_end)
- [The data was deleted](#where-is-data)
- [The data disappeared after a software update](#software-update)
- [The system or a software tool crashed, and the data was gone](#where-is-data)

### software-update

#### The data disappeared after a software update

> Sometimes, when you update or upgrade a software tool or the entire operating system, unexpected problems could happen, causing the system or software to stop working as it should, or some faulty behavior to happen, including data corruption. If you noticed a data loss just after a software or system update, it is worth considering rolling back to some previous state. Rollbacks are useful because they mean that your software or database can be restored to a clean and consistent state even after erroneous operations or software crashes have happened.
>
> The method to roll back a piece of software to a previous version depends on how that software is built - in some cases it is possible easily, in others it is not, and with some it would need some work around it. So you will need to do an online search on rolling back to older versions for that given software. Please note that rolling back is also known as "downgrading", so do a search with this term too. For operating systems such as Windows or Mac, each has their own methods for downgrading to the previous version.
>
> - For Mac systems, visit the [Mac Support Center knowledge base](https://support.apple.com) and use the "downgrade" keyword with your macOS version to read more and find instructions.
> - For Windows systems, the procedure varies depending on whether you want to undo a specific update or roll back the whole system. In both cases, you can find instructions in the [Microsoft Support Center](https://support.microsoft.com), for example for Windows 11 you can look for the "How do I remove an installed update" question in this [FAQ page](https://support.microsoft.com/en-us/help/12373/windows-update-faq).

Was the software rollback useful?

##### Options

- [Yes](#resolved_end)
- [No](#where-is-data)

### where-is-data

#### The system or a software tool crashed and the data was gone

> Regular data backup is a good and recommended practice. Sometimes, we forget that we have automatic backup enabled, so it is worth checking on your devices to see if you have that option enabled and use that backup to restore your data. In case you don't do backup now, planning for future backups is recommended, and you can find more tips on how to set up backups in the [final tips of this workflow](#resolved_end).

To check if you have a backup of the data you lost, start asking yourself where the lost data was stored.

##### Options

- [In storage devices (external hard disk, USB sticks, SD cards)](#storage-devices_end)
- [In a computer](#computer)
- [In a mobile device](#mobile)

### computer

#### In a computer

What kind of operating system runs on your computer?

##### Options

- [MacOS](#macos-computer)
- [Windows](#windows-computer)
- [Gnu/Linux](#linux-computer)

### macos-computer

#### MacOS

> To check if your MacOS device had a backup option enabled, and use that backup to restore your data, check your [iCloud](https://support.apple.com/en-us/HT208682) or [Time Machine](https://support.apple.com/en-za/HT201250) options to see if there is any available backup.
>
> One place to look at is the running list of Recent Items, which keeps track of the apps, files and servers you have used during your past few sessions on the computer. To look for the file and reopen it, go to the Apple Menu in the upper-left corner, select Recent Items and browse the list of files. More details can be found in [the official macOS User Guide](https://support.apple.com/en-za/guide/mac-help/mchlp2305/mac).

Were you able to locate your data or restore it?

##### Options

- [Yes](#resolved_end)
- [No](#macos-restore)

### macos-restore

#### No, I was not able to locate and restore my data

> In some cases, there are free and open-source tools that can be helpful to find missing content and recover it. Sometimes, they are limited in use, and most of the well-known tools cost some money.
>
> For example, you can try [EaseUS Data Recovery Wizard](https://www.easeus.com/mac-data-recovery-software/drw-mac-free.htm) or [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/).

Was this information helpful in recovering your data (fully or partially)?

##### Options

- [Yes](#resolved_end)
- [No](#tech-assistance_end)

### windows-computer

#### Windows

> To check if you have a backup enabled in your Windows machine, visit [Microsoft Support information: Backup and Restore in Windows](https://support.microsoft.com/en-us/windows/backup-and-restore-in-windows-352091d2-bb9d-3ea3-ed18-52ef2b88cbef).
>
> Windows includes the **Timeline** feature, which is meant to enhance your productivity by storing a record of the files you used, sites you browsed and other actions you made on your computer. If you can’t remember where you stored a document, you can click the Timeline icon in the Windows 10 taskbar to see a visual log organized by date and jump back to what you need by clicking the appropriate preview icon. This might help you locate a file that has been renamed. If Timeline is enabled, some of your PC activity — like files you edit in Microsoft Office — can also sync with your mobile device or another computer you use, so you might have a backup of your lost data on another device. Read more on Timeline and how to use it in [the official guide](https://support.microsoft.com/en-us/windows/get-help-with-timeline-febc28db-034c-d2b0-3bbe-79aa0c501039).

Were you able to locate your data or restore it?

##### Options

- [Yes](#resolved_end)
- [No](#windows-restore)

### windows-restore

#### No, I was not able to locate and restore my data

> In some cases, there are free and open-source tools that can be helpful to find missing content and recover it. Sometimes, they are limited in use, and most of the well-known tools cost some money.
>
> For example, you can try [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (see [step-by-step instructions](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/datarecoverywizard/free-data-recovery-software.htm), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/data-recovery-software.html), or [Recuva](http://www.ccleaner.com/recuva).

Was this information helpful in recovering your data (fully or partially)?

##### Options

- [Yes](#resolved_end)
- [No](#tech-assistance_end)

### linux-computer

#### GNU/Linux

> Some of the most popular Linux distributions, like Ubuntu, have an in-built backup tool, for example [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) on Ubuntu. If a built-in backup tool is included in your operating system, you might have been prompted to enable automatic backups when you first started using your computer. Search your Linux distribution to see if it includes a built-in backup tool, and what the procedure is to check if it is enabled and restore data from backups.
>
> Read [Ubuntu & Deja Dup - Get up, backup](https://www.dedoimedo.com/computers/ubuntu-deja-dup.html) to check whether you have automatic backups enabled in your computer. Read ["Full System Restore with Déjà Dup"](https://wiki.gnome.org/Apps/DejaDup/Help/Restore/Full) for instructions on how to restore your lost data from an existing backup.

Was this information helpful in recovering your data (fully or partially)?

##### Options

- [Yes](#resolved_end)
- [No](#tech-assistance_end)

### mobile

#### In a mobile device

What operating system runs on your mobile?

##### Options

- [iOS](#ios-phone)
- [Android](#android-phone)

### ios-phone

#### IOS

> You might have enabled an automatic backup with iCloud or iTunes. Read ["Restore or set up your device from an iCloud backup"](https://support.apple.com/en-us/HT204184) to check if you have any existing backups and to learn how to restore your data.

Have you found your backup and recovered your data?

##### Options

- [Yes](#resolved_end)
- [No](#phone-which-data)

### phone-which-data

#### No, I have not found my backup and recovered my data

Which of the following applies to the data you lost?

##### Options

- [It is app-generated data, e.g. contacts, feeds, etc.](#app-data-phone)
- [It is user-generated data, e.g. photos, videos, audio, notes](#ugd-phone)

### app-data-phone

#### It is app-generated data, e.g. contacts, feeds, etc.

> A mobile app is a software application designed to run on your mobile device. However, most of these apps can also be accessed through a desktop browser. If you have experienced a loss of app-generated data in your mobile phones, try to access that app in your desktop browser, by logging to the app's web interface with your credentials for that app. You might find the data you lost in the browser interface.

Was this information helpful in recovering your data (fully or partially)?

##### Options

- [Yes](#resolved_end)
- [No](#tech-assistance_end)

### ugd-phone

#### It is user-generated data, e.g. photos, videos, audio, notes

> User-generated data is the kind of data you create or generate through a specific app, and in case of data loss you will want to check if that app has backup settings enabled by default or enables a way to recover it. For example, if you use WhatsApp on your mobile and the conversations go missing or something faulty happened, you can recover your conversations if you enabled WhatsApp's recovery setting, or if you are using an app to create and keep notes with sensitive or personal information, it might also have a backup option enabled without you noticing sometimes.
>
> Most of the apps' user-generated data stored on the phone will be copied into your backup, either into the cloud or local disk backup. See tips at the end of this workflow for instructions on how to do a backup of your device.

Was this information helpful in recovering your data (fully or partially)?

##### Options

- [Yes](#resolved_end)
- [No](#tech-assistance_end)

### android-phone

#### Android

> Google has a service built into Android called Android Backup Service. By default, this service backs up several types of data and associates it with the appropriate Google service, where you can also access it on the web. In most Android devices, you can see your Sync settings by heading into Settings > Accounts > Google, then selecting your Gmail address. If you lost data of a kind that you were synchronizing with your Google account, you will probably be able to recover it by logging into your Google account through a web browser.

Was this information helpful in recovering your data (fully or partially)?

##### Options

- [Yes](#resolved_end)
- [No](#phone-which-data)

### tech-assistance_end

#### I would like to get some more help

> If your data loss has been caused by physical damage such as your devices falling on the floor or in the water, being exposed to an electricity power outage or other issues, the most probable situation is that you will need to perform a recovery of the data stored in your hardware. If you do not know how to perform these operations, you should contact an IT person with hardware and electronic equipment maintenance that could help you. However, depending on your context and the sensitivity of the data you need to recover, contacting any IT shop is not advised. Rather it is recommended you give preference to IT persons you know and trust.

### device-lost-theft_end

#### I would like to get some more help

> In case of lost, stolen, or seized devices, make sure to change all your passwords as soon as possible and to visit our specific [resource on what to do if a device is lost](../../../lost-device).
>
> If you are a member of civil society and you need support to acquire a new device to replace the lost one, you can reach out to the organizations listed below.

[orgs](:organisations?services=equipment_replacement)

### storage-devices_end

#### I would like to get some more help

> In order to recover the data you have lost, remember that timing is important. For example, recovering a file you accidentally deleted a few hours or a day before might have higher success rates than a file you lost months before.
>
> Consider using a software tool for trying to recover the data you have lost (because of deletion or corruption) such as [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (for Windows, Mac, or Linux - see [step-by-step instructions](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/data-recovery-software/) (for Windows, Mac, Android, or iOS), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/) (for Windows or Mac), or [Recuva](http://www.ccleaner.com/recuva) (for Windows). Take into account that these tools do not always work, because your operating system may have written new data over your deleted information. Because of this, you should do as little as possible with your computer between deleting a file and attempting to restore it with one of the tools listed above.
>
> To increase your chances of recovering data, stop using your devices immediately. Continuously writing on the hard drive might decrease the chances of finding and restoring data.
>
> If none of the above options has worked for you, the most probable situation is that you will need to perform a recovery of the data stored in the hard drive. If you do not know how to perform these operations, you should contact an IT person with hardware and electronic equipment maintenance skills who could help you. However, depending on your context and the sensitivity of the data you need to recover, contacting any IT shop may not be advised, and it is recommended you give preference to IT persons you know and trust.

### resolved_end

#### My issue has been resolved

We hope this DFAK guide was useful. Please give us feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

- Backups - It is always a good idea to make sure you have backups - a lot of different backups, that you store somewhere other than the same place your data is! Depending on your context, opt for storing your backups in cloud services and in physical external devices you keep disconnected from your computer while connecting to the internet.
- For both types of backups, you should protect your data with encryption. Perform regular and incremental backups for your most important data, check that you have them ready, and test that you are able to restore them before performing software or operating system updates.
- Set up a structured folder system - Every person has their own way of organizing important data and information, there is no one size fits all. Nonetheless, it is important that you consider setting up a folder system that suits your needs. By creating a consistent folder system, you can make your life easier by better knowing which folder and files should be frequently backed up, for instance, where important information you are working on is located, where you should keep data containing personal or sensitive information about you and your collaborators, and so on. As usual, take a deep breath and some time to plan the type of data you produce or manage, think of a folder system that can make it more consistent and tidy up.

### Resources

- [Access Now Helpline Community Documentation: Secure back up](https://communitydocs.accessnow.org/182-Secure_Backup.html)
- [Security in a Box: Back-up Tactics](https://securityinabox.org/en/files/backup/)
- [Official page on Mac backups](https://support.apple.com/mac-backup)
- [How to back up data regularly on Windows](https://support.microsoft.com/en-us/windows/backup-and-restore-in-windows-352091d2-bb9d-3ea3-ed18-52ef2b88cbef)
- [How to enable regular backups on iPhone](https://support.apple.com/en-us/HT203977)
- [How to enable regular backups on Android](https://support.google.com/android/answer/2819582?hl=en)
- [Securily backup your data](https://johnopdenakker.com/securily-backup-your-data/)
- [How to Back Up Your Digital Life](https://www.wired.com/story/how-to-back-up-your-digital-life/)
- [Wikipedia - Data recovery](https://en.wikipedia.org/wiki/Data_recovery)
