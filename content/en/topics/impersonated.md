---
layout: topic
title: "Someone is impersonating me online"
author: Floriana Pagano, Alexandra Hache
language: en
summary: "Someone is being impersonated through a social media account, email address, PGP key, fake website or app."
date: 2019-04-01
permalink: topics/impersonated
parent: Home
order: 6
---

# Someone Is Impersonating Me Online

A threat faced by many activists, human rights defenders, NGOs, independent media, and bloggers is to be impersonated by adversaries that will create false profiles, websites, or emails in their names. Sometimes, this is meant to create smear campaigns, misleading information or social engineering, or to steal someone's identity in order to create noise, trust issues, and data breaches that impact the reputation of the individuals or collectives being impersonated. In other cases, an adversary may impersonate someone's online identity for financial motivations such as raising funds, stealing payment credentials, receiving payments, etc.

This is a frustrating problem that can, on different levels, affect your capacity to communicate and inform. It can also have different causes depending on where and how you are being impersonated.

It is important to know that there are many ways to impersonate someone (fake profiles on social media, cloned websites, spoofed emails, and non-consensual publication of personal images and videos). Strategies may range from submitting takedown notices to proving original ownership, claiming copyright of the original website or information, or warning your networks and personal contacts through public or confidential communications. Diagnosing the problem and finding possible solutions to impersonation can be hard. Sometimes it will be close to impossible to push a small hosting company to take down a website, and legal action may become necessary. It is good practice to set up alerts and monitor the internet to find out if you or your organization are being impersonated.

This section of the Digital First Aid Kit will walk you through some basic steps to diagnose potential ways of impersonating and potential mitigation strategies to remove accounts, websites and emails impersonating you or your organization.

If you are being impersonated, follow this questionnaire to identify the nature of your problem and find possible solutions.

## Workflow

### urgent-question

#### Do you fear for your physical integrity or well-being?

##### Options

- [Yes](#physical-sec_end)
- [No](#diagnostic-start1)

### diagnostic-start1

#### No, I do not fear for my physical integrity or well-being

Is the impersonation affecting you as an individual (someone is using your legal name and surname or the nickname you base your reputation on) or as an organization/collective?

##### Options

- [As an individual](#individual)
- [As an organization](#organization)

### individual

#### As an individual

> If you are being affected as an individual, you might want to alert your contacts. Take this step using a mail account, profile or website that is fully under your control.

##### Options

- [Once you have informed your contacts that you're being impersonated, proceed to the next step](#diagnostic-start2).

### organization

#### As an organization

> If you are being affected as a group, consider doing public communication. Take this step by using a mail account, profile or website that is fully under your control.

##### Options

- [Once you have informed your community that you're being impersonated, proceed to the next step](#diagnostic-start2).

### diagnostic-start2

#### How are you being impersonated?

##### Options

- [A fake website is impersonating me or my group](#fake-website)
- [Through a social network account](#social-network)
- [Through non-consensual sharing of videos or images](#doxing)
- [Through my email address or a similar address](#spoofed-email1)
- [Through a PGP key connected to my email address](#PGP)
- [Through a fake app that is imitating my app](#app1)

### social-network

#### Through a social network account

> You can decide to report the impersonating account or content to the relevant platform where the impersonation is happening.
>
> **_Note:_** _Always [document](/documentation) before taking actions such as deleting messages or conversation logs or blocking profiles. If you are considering legal action, you should consult information on [legal documentation](/documentation#legal)._

On which social networking platform are you being impersonated?

##### Options

- [Facebook](#facebook)
- [Instagram](#instagram)
- [TikTok](#tiktok)
- [Twitch](#twitch)
- [Twitter](#twitter)
- [YouTube](#youtube)

### facebook

#### Facebook

> Follow the instructions in ["How do I report a Facebook account or Page that’s pretending to be me or someone else?"](https://www.facebook.com/help/174210519303259) to request the impersonating account to be deleted.
>
> Please note that it might take some time to receive an answer to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

##### Options

- [Yes](#resolved_end)
- [No](#account_end)

### twitter

#### Twitter

> Follow the steps in ["Report an account for impersonation"](https://help.twitter.com/forms/impersonation) to request the impersonating account to be deleted.
>
> Please note that it might take some time to receive an answer to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

##### Options

- [Yes](#resolved_end)
- [No](#account_end)

### instagram

#### Instagram

> Follow the instructions in ["Impersonation Accounts"](https://help.instagram.com/446663175382270) to request the impersonating account to be deleted.
>
> Please note that it might take some time to receive an answer to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

##### Options

- [Yes](#resolved_end)
- [No](#account_end)

### tiktok

#### TikTok

> Follow the instructions in ["Report an impersonation account"](https://support.tiktok.com/en/safety-hc/report-a-problem/report-an-impersonation-account) to request the impersonating account to be deleted.
>
> Please note that it might take some time to receive an answer to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

##### Options

- [Yes](#resolved_end)
- [No](#account_end)

### youtube

#### YouTube

> Follow the instructions in ["Report inappropriate videos, channels, and other content on YouTube"](https://support.google.com/youtube/answer/2802027) to report the impersonating account. Choose "Impersonation" among the possible reasons for reporting.
>
> Please note that it might take some time to receive an answer to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

##### Options

- [Yes](#resolved_end)
- [No](#account_end)

### twitch

#### Twitch

> Follow the instructions in ["How to File a User Report"](https://help.twitch.tv/s/article/how-to-file-a-user-report) to report the impersonating account and request it to be deleted. Choose "Impersonation" among the possible categories included in the form.
>
> Please note that it might take some time to receive an answer to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

##### Options

- [Yes](#resolved_end)
- [No](#account_end)

### fake-website

#### A fake website is impersonating me or my group

> Check if this website is known as malicious by looking for its URL in the following online services:
>
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)

Is the domain known to be malicious?

##### Options

- [Yes](#malicious-website)
- [No](#non-malicious-website)

### malicious-website

#### The domain is known to be malicious

> Report the URL to Google Safe Browsing by filling out the ["Report malicious software" form](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Please note that it might take some time to make sure that your report was successful. Meanwhile, you can proceed to the next step for sending a takedown request to the hosting provider and domain registrar, or save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

##### Options

- [Yes](#resolved_end)
- [No](#non-malicious-website)

### non-malicious-website

#### The domain is not known to be malicious

> You can try reporting the website to the hosting provider or domain registrar, asking for a takedown.
>
> **_Note:_** _Always [document](/documentation) before taking actions such as deleting messages or conversation logs or blocking profiles. If you are considering legal action, you should consult information on [legal documentation](/../../documentation#legal)._
>
> If the website you want to report is using your content, one thing you may need to prove is that you are the legitimate owner of the original content. You can show this by presenting your original contract with the domain registrar and/or hosting provider, but you can also do a search on the [Wayback Machine](https://archive.org/web/), looking for both the URL of your website and the fake website. If the websites have been indexed there, you will find a history that may make it possible to show that your website existed before the fake website was published.
>
> To send a takedown request, you will also need to gather information on the fake website:
>
> - Go to [Network Tools' NSLookup service](https://network-tools.com/nslookup/) and find out the IP address (or addresses) of the fake website by entering its URL in the search form.
> - Write down the IP address or addresses.
> - Go to [Domain Tools' Whois Lookup service](https://whois.domaintools.com/) and search both for the domain and the IP address/es of the fake website.
> - Record the name and abusive email address of the hosting provider and domain service. If included in the results of your search, also record the name of the website owner.
> - Write to the hosting provider and domain registrar of the fake website to request its takedown. In your message, include information on the IP address, URL, and owner of the impersonating website, as well as the reasons why it is abusive.
> - You can use [Access Now Helpline's Template to report cloned websites to a hosting provider](https://communitydocs.accessnow.org/352-Report_Fake_Domain_Hosting_Provider.html) to write to the hosting provider.
> - You can use [Access Now Helpline's template to report impersonations or cloning to a domain provider](https://communitydocs.accessnow.org/343-Report_Domain_Impersonation_Cloning.html) to write to the domain registrar.
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has this worked?

##### Options

- [Yes](#resolved_end)
- [No](#web-protection_end)

### spoofed-email1

#### Through my email address or a similar address

> For underlying technical reasons, it is quite difficult to authenticate emails. This is also why it is very easy to create forged sender addresses and spoofed emails.

Are you being impersonated through your email address, or a similar one, for example, with the same user name but a different domain?

##### Options

- [I'm being impersonated through my email address](#spoofed-email2)
- [I'm being impersonated through a similar email address](#similar-email)

### spoofed-email2

#### I'm being impersonated through my email address

> The person who is impersonating you might have hacked into your email account. To rule out this possibility, try changing your password.

Are you able to change your password?

##### Options

- [Yes](#spoofed-email3)
- [No](#hacked-account)

### hacked-account

#### I was not able to change my password

> If you can't change your password, your email account has probably been hacked into. You can follow the ["I cannot access my account" workflow](../../../account-access-issues) to solve this issue.

Has this helped solve your issue?

##### Options

- [Yes](#resolved_end)
- [No](#account_end)

### spoofed-email3

#### I was able to change my password

> Email spoofing consists of email messages with a forged sender address. The message appears to have originated from someone or somewhere other than the actual source.
>
> Email spoofing is common in phishing and spam campaigns because people are more likely to open an email when they think it is coming from a legitimate source.
>
> If someone is spoofing your email, you should inform your contacts to warn them about the danger of phishing (do it from a mail account, profile or website that is fully under your control).
>
> If you think the impersonation was aimed at phishing or other malicious intents, you might also want to read the [I have received suspicious messages](../../../suspicious_messages) section.

Did the emails stop after you changed the password to your email account?

##### Options

- [Yes](#compromised-account)
- [No](#secure-comms_end)

### compromised-account

#### The emails stopped after I changed my password

> Probably, your account was hacked into by someone who used it to send out emails to impersonate you. As your account was compromised, you might also want to read the [I lost access to my accounts](../../../account-access-issues/) troubleshooter.

Has this helped solve your issue?

##### Options

- [Yes](#resolved_end)
- [No](#account_end)

### similar-email

#### I'm being impersonated through a similar email address

> If the impersonator is using an email address that is similar to yours but with a different domain or user name, it's a good idea to warn your contacts about this attempt at impersonating you (do it from a mail account, profile or website that is fully under your control).
>
> You might also want to read the [I have received suspicious messages](../../../suspicious-messages) section, as this impersonation might be aimed at phishing.

Has this helped solve your issue?

##### Options

- [Yes](#resolved_end)
- [No](#secure-comms_end)

### PGP

#### Through a PGP key connected to my email address

Do you think your private PGP key might have been compromised, for example, because you lost control of the device where it was stored?

##### Options

- [Yes](#PGP-compromised)
- [No](#PGP-spoofed)

### PGP-compromised

#### My PGP key might have been compromised

> - Create a new key pair and have it signed by people you trust.
> - Inform your contacts through a trusted channel you control (such as Signal or another [end-to-end encrypted tool](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools)) that they should use your new key and stop using the old one. Tell them that they can recognize your actual key based on the fingerprint of your real key. You can also send them directly your new public key through the same trusted channel you're using to inform them.

Do you need more help solving your problem?

##### Options

- [Yes](#secure-comms_end)
- [No](#resolved_end)

### PGP-spoofed

#### My PGP key might have been spoofed

> - Inform your contacts through a trusted channel you control, such as Signal or another [end-to-end encrypted tool](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools), that someone is trying to impersonate you. Tell them that they can recognize your actual key based on the fingerprint of your real key. You can also send them directly your public key through the same trusted channel you're using to inform them.

Do you need more help solving your problem?

##### Options

- [Yes](#secure-comms_end)
- [No](#resolved_end)

### doxing

#### Through non-consensual sharing of videos or images

> If someone is sharing your personal information or private videos or images, we recommend you follow the Digital First Aid Kit workflow on [Doxing and Non-Consensual Sharing of Private Media](../../../doxing).

What would you like to do?

##### Options

- [Take me to the Digital First Aid Kit section on Doxing and Non-Consensual Sharing of Private Media](/doxing)
- [I need support to address my issue](#harassment_end)

### app1

#### Through a fake app that is imitating my app

> If someone is spreading a malicious copy of your app or other software, it's a good idea to do public communication to warn users to only download the legitimate version.
>
> You should also report the malicious app and request its takedown.

Where is the malicious copy of your app being distributed?

##### Options

- [On Github](#github)
- [On Gitlab.com](#gitlab)
- [On Google Play Store](#playstore)
- [On Apple App Store](#apple-store)
- [On another website](#fake-website)

### github

#### Github

> If the malicious software is hosted on Github, read Github's [Guide to Submitting a Digital Millennium Copyright Act (DMCA) Takedown Notice](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) for taking down content that violates the copyright.
>
> It might take some time to wait for a response to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this helped solve your issue?

##### Options

- [Yes](#resolved_end)
- [No](#app_end)

### gitlab

#### Gitlab

> If the malicious software is hosted on Gitlab.com, read Gitlab's [Digital Millennium Copyright Act (DMCA) takedown request requirements](https://about.gitlab.com/handbook/dmca/) for taking down content that violates copyright.
>
> It might take some time to wait for a response to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this helped solve your issue?

##### Options

- [Yes](#resolved_end)
- [No](#app_end)

### playstore

#### Playstore

> If the malicious app is hosted on Google Play Store, follow the steps in ["Removing Content From Google"](https://support.google.com/legal/troubleshooter/1114905) to take down content that violates copyright.
>
> It might take some time to wait for a response to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this helped solve your issue?

##### Options

- [Yes](#resolved_end)
- [No](#app_end)

### apple-store

#### Apple store

> If the malicious app is hosted on the App Store, fill in the ["Apple App Store Content Dispute" form](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts) to take down content that violates copyright.
>
> It might take some time to wait for a response to your request. Save this page in your bookmarks and come back to this workflow in a few days.

Has this helped solve your issue?

##### Options

- [Yes](#resolved_end)
- [No](#app_end)

### physical-sec_end

#### I fear for my physical integity and well-being

> If you are fearing for your physical well-being, please contact the organizations below who can support you.

[orgs](:organisations?services=physical_sec)

### harassment_end

#### I need support to address my issue

> If your private information or media have been published without your consent and you need support to troubleshoot your issue, you can get in touch with the organizations below who can support you.
>
> Before you reach out to an organization, we strongly recommend you follow the Digital First Aid Kit [troubleshooter on doxing and non-consensual publication of private media](../../../doxing), as this will help you figure out what is exactly happening to you.

[orgs](:organisations?services=harassment)

### account_end

#### I am still experiencing impersonation with my account(s)

> If you are still experiencing impersonation or your account is still compromised, please contact the organizations below who can support you.

[orgs](:organisations?services=account&services=legal)

### app_end

#### I would like more support with fake apps

> If the fake app hasn't been taken down, please contact the organizations below who can support you.

[orgs](:organisations?services=account&services=legal)

### web-protection_end

#### I would like more support with takedown requests

> If your takedown requests have not been successful, you can try reaching out to the organizations below for further support.

[orgs](:organisations?services=web_protection)

### secure-comms_end

#### I would like more support with secure communications

> If you need help or recommendations on phishing, email security and encryption, or secure communications in general, you can reach out to these organizations:

[orgs](:organisations?services=secure_comms)

### resolved_end

#### My problem has been resolved

We hope this DFAK guide was useful. Please give us feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

To prevent any further attempts at impersonating you, read the tips below.

### Final Tips

- Create strong, complex and unique passwords for all your accounts.
- Consider using a password manager for creating and storing passwords so you can use a unique password on each site and service without having to memorize all of them.
- Activate two-factor authentication (2FA) for your most important accounts. 2FA offers greater account security by requiring you to use more than one method to log into your accounts. This means that even if someone were to get hold of your primary password, they could not access your account unless they also had your mobile phone or another secondary means of authentication.
- Verify your profiles on social networking platforms. Some platforms offer a feature for verifying your identity and linking it to your account.
- Set up Google alerts. You can get emails when new results for a topic show up in Google Search. For example, you can get information about mentions of your name or your organization/collective name.
- Capture your web page as it appears now for use as evidence in the future. If your website allows crawlers, you can use the Wayback Machine, offered by the [Internet Archive](https://archive.org). Visit the [Internet Archive Wayback Machine](https://archive.org/web/), enter your website's name in the field under the "Save Page Now" header, and click the "Save Page" button.

### Resources

- [Security Self-Defense: Create Strong and Unique Passwords](https://ssd.eff.org/en/module/creating-strong-passwords)
- [Security Self-Defense: Animated Overview Using Password Managers](https://ssd.eff.org/en/module/animated-overview-using-password-managers-stay-safe-online)
- [Surveillance Self-Defense: How to Enable Two-Factor Authentication](https://ssd.eff.org/module/how-enable-two-factor-authentication)
- [Archive.org: Archive your Website](https://archive.org/web/)
- [Security Self-Defense: How to use KeePassXC - A Secure Open-Source Password Manager](https://ssd.eff.org/en/module/how-use-keepassxc)
- [Access Now Helpline Community Documentation: Choosing a Password Manager](https://communitydocs.accessnow.org/295-Password_managers.html)
