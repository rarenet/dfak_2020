---
layout: topic
title: "I'm being targeted by online harassment"
author: Floriana Pagano
language: en
summary: "Are you being targeted by online harassment?"
date: 2019-04-01
permalink: topics/harassed-online
parent: Home
order: 7
---

# I'm being targeted by online harassment

The Internet, and social media platforms in particular, have become a critical space for civil society members and organizations, especially for women, LGBTQIA+ persons, and other marginalized groups, to express themselves and make their voices heard. But at the same time, they have also become spaces where these groups are easily targeted for expressing their views. Online violence and abuse deny women, LGBTQIA+ persons, and many other unprivileged people the right to express themselves equally, freely, and without fear.

Online violence and abuse have many different forms, and malicious entities can often rely on impunity due to a lack of laws that protect victims of harassment in many countries, and mostly because there are no universal protection strategies: they need to be tweaked creatively depending on what kind of attack is being launched. It is therefore important to identify the typology of the attack targeting you to decide what steps can be taken.

This section of the Digital First Aid Kit will walk you through some basic steps to understand what kind of attack you are suffering and plan how to get protected against it, or find a digital security help desk that can support you.

If you are targeted by online harassment, we suggest you read recommendations on [how to take care of yourself](/self-care) and [how to document the attacks](/documentation), and then follow this questionnaire to identify the nature of your problem and find possible solutions.

## Workflow

### physical-wellbeing

#### Do you fear for your physical integrity or well-being?

##### Options

- [Yes](#physical-risk_end)
- [No](#location)

### location

#### Does the attacker appear to know your physical location?

##### Options

- [Yes](#location_tips)
- [No](#device)

### location_tips

#### The attacker appears to know my physical location

> Check your recent posts on social media: do they include your exact location? If so, disable GPS access for the social media apps and other services on your phone so that when you post your updates, your location doesn't show.
>
> Check pictures you posted of yourself online: do they include details of places that are clearly recognizable and can clearly show where you are? To protect yourself from potential stalkers, it's better not to show your exact location when posting photos or videos of yourself.
>
> As an additional precautionary measure, it is also a good idea to keep GPS off all the time – except for briefly when you really need to find your position on a map.

Could the attacker have followed you through the information you published online? Or do you still feel the attacker knows your physical location through other means?

##### Options

- [No, I think I've solved my issues](#resolved_end)
- [Yes, I still think the attacker knows where I am](#device)
- [No, but I have other issues](#account)

### device

#### Do you think the attacker has accessed or is accessing your device?

##### Options

- [Yes](#device-compromised)
- [No](#account)

### device-compromised

#### The attacker has access to my device

> Change the password to access your device to a unique, long and complex one:
>
> - [Mac OS](https://support.apple.com/en-us/HT202860)
> - [Windows](https://support.microsoft.com/en-us/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/en-us/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=en)

Do you still have the feeling that the attacker may be controlling your device?

##### Options

- [No, I think I've solved my issues](#resolved_end)
- [No, but I have other issues](#account)
- [Yes](#info_stalkerware)

### info_stalkerware

#### The attacker appears to control my device

> [Stalkerware](https://en.wikipedia.org/wiki/Stalkerware) is any software used to monitor a person's activity or location with the purpose of stalking or controlling them.
>
> If you think that someone may be spying on you through an app they installed in your mobile device, [this Digital First Aid Kit workflow will help you decide whether your device is infected with malware and how to take steps to clean it](/topics/device-acting-suspiciously).

Do you need more help to understand the attack you are suffering?

##### Options

- [No, I think I've solved my issues](#resolved_end)
- [Yes, I still have some issues I would like to troubleshoot](#account)

### account

#### No, but I have other issues

> Whether someone got access to your device or not, there is a possibility that they have accessed your online accounts by hacking into them or because they knew or cracked your password.
>
> If someone has access to one or more of your online accounts, they could read your private messages, identify your contacts, publish your private posts, photos, or videos, or start impersonating you.
>
> Review your online accounts activity and mailbox (including the Sent and Trash folders) for possible suspicious activity.

Have you noticed posts or messages disappearing, or other activities that give you good reason to think your account may have been compromised?

##### Options

- [Yes](#change_passwords)
- [No](#private-contact)

### change_passwords

#### Change password

> Try changing the password to each of your online accounts with a strong and unique password.
>
> You can learn more on how to create and manage passwords in this [Surveillance Self-Defense guide](https://ssd.eff.org/module/creating-strong-passwords).
>
> It is also a very good idea to add a second layer of protection to your online accounts by enabling 2-factor authentication (2FA) whenever possible.
>
> Find out how to enable 2-factor authentication in this [Surveillance Self-Defense guide](https://ssd.eff.org/module/how-enable-two-factor-authentication).

Do you still have the feeling that someone may have access to your account?

##### Options

- [Yes](#hacked-account)
- [No](#private-contact)

### hacked-account

#### Yes, I think someone may still have access to my account

> If you are unable to access your account, there is a chance that your account was hacked and the hacker changed your password.

If you think your account may have been hacked into, try following the Digital First Aid Kit workflow that can help you troubleshoot account access issues.

##### Options

- [Please take me to the workflow to troubleshoot account access issues](/topics/account-access-issues)
- [I think I've solved my issues](#resolved_end)
- [My account is fine, but I have other issues](#private-contact)

### private-contact

#### My account is fine, but I have other issues

Are you receiving unwanted phone calls or messages on a messaging app?

##### Options

- [Yes](#change_contact)
- [No](#threats)

### change_contact

#### I am still receiving unwanted phone calls or messages on a messaging app

> If you are receiving unwanted phone calls, SMS messages or messages on apps that are associated with your telephone number, email, or other private contact information, you can try changing your number, SIM card, email, or other contact information associated with the account.
>
> You should also consider reporting and blocking the messages and the associated account to the relevant platform.

Have you successfully stopped the unwanted calls or messages?

##### Options

- [Yes](#legal)
- [No, I have other issues](#threats)
- [No, I need assistance](#harassment_end)

### threats

#### I am not receiving unwanted phone calls or messages on a messaging app

Are you being blackmailed or receiving threats through emails or messages on a social media account?

##### Options

- [Yes](#threats_tips)
- [No](#impersonation)

### threats_tips

#### I am being blackmailed or receiving threats

> If you are receiving messages containing threats, including threats of physical or sexual violence, or blackmail, you should document what happened as much as possible, including recording any links and screenshots, reporting the person to the relevant platform or service provider, and blocking the attacker.

Have you successfully stopped the threats?

##### Options

- [Yes](#legal)
- [Yes, but I have more issues](#impersonation)
- [No, I need legal support](#legal-warning)

### impersonation

#### I might be subject to impersonation

> Another form of harassment you could have been subjected to is impersonation.
>
> For example, someone may have created an account under your name and is sending private messages or publicly attacking someone on social media platforms, spreading disinformation or hate speech, or acting in other ways to make you, your organization or others close to you vulnerable to reputational and security risks.
>
> If you think someone is impersonating you or your organization, you can follow this Digital First Aid Kit workflow, guiding you through the various forms of impersonation to identify the best possible strategy to face your issue.

What would you like to do?

##### Options

- [I am being impersonated and would like to find a solution](/topics/impersonated)
- [I'm facing a different form of harassment](#defamation)

### defamation

#### I'm facing a different form of harassment

Is someone trying to damage your reputation by spreading false information?

##### Options

- [Yes](#defamation-yes)
- [I'm facing a different form of harassment](#doxing)

### defamation-yes

#### Someone is trying to damage my reputation by spreading false information

> Defamation is generally defined as making a statement that injures someone's reputation. Defamation can be spoken (slander) or written (libel). Laws in different countries may differ on what constitutes defamation for legal action. For example, some countries have laws that are very protective of freedom of expression which allow media and private individuals to say things about public officials that may be embarrassing or damaging as long as they believe such information is true. Other countries may allow you to sue others more easily for spreading information about you that you don't like.
>
> If someone is trying to damage your or your organization's reputation, you can follow the Digital First Aid Kit workflow on defamation, guiding you through various strategies to address defamation campaigns.

What would you like to do?

##### Options

- [I would like to find a strategy against a defamation campaign](/topics/defamation)
- [I'm facing a different form of harassment](#doxing)

### doxing

#### I'm facing a different form of harassment

Has someone published private information or media about you without your consent?

##### Options

- [Yes](#doxing-yes)
- [No](#hate-speech)

### doxing-yes

#### Someone published private information or media about me without my consent

> If someone has published private information about you or is circulating videos, photos, or other media about you, you can follow the Digital First Aid Kit workflow that can help you understand what is happening and how to respond to such an attack.

What would you like to do?

##### Options

- [I'd like to understand what is happening and what I can do about it](/topics/doxing)
- [I would like to receive support](#harassment_end)

### hate-speech

#### Someone is spreading hate messages against me

Is someone spreading hate messages against you based on attributes like race, gender, or religion?

##### Options

- [Yes](#one-more-persons)
- [No](#harassment_end)

### one-more-persons

#### One or more persons is spreading hate messages against me

Have you been attacked by one or more persons?

##### Options

- [I've been attacked by one person](#one-person)
- [I've been attacked by more than one person](#campaign)

### one-person

#### I've been attacked by one person

> If hate speech comes from a single person, the easiest and quickest way to contain the attack and prevent the user from continuing to send you hateful messages is to report and block them. Remember that if you block the user, you cannot access its content to document it. Before blocking, read our [tips on documenting digital attacks](/documentation).
>
> Whether you know who your harasser is or not, it is always a good idea to block them on social networking platforms whenever possible.
>
> - [Facebook](https://www.facebook.com/help/168009843260943)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=en)
> - [Instagram](https://help.instagram.com/426700567389543)
> - [TikTok](https://support.tiktok.com/en/using-tiktok/followers-and-following/blocking-the-users)
> - [Twitter](https://help.twitter.com/en/using-twitter/blocking-and-unblocking-accounts)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885/?cms_platform=android)

Have you blocked your harasser effectively?

##### Options

- [Yes](#legal)
- [No](#campaign)

### legal

#### I need legal assistance

> If you know who is harassing you, you can report them to your country's authorities if you consider it safe and appropriate in your context. Each country has different laws for protecting people from online harassment, and you should explore the legislation in your country or ask for legal advice to help you decide what to do.
>
> If you don't know who is harassing you, in some cases, you could trace back the attacker's identity through a forensic analysis of the traces they may have left behind.
>
> In any case, if you are considering taking legal action, keeping evidence of the attacks you were subjected to will be very important. It is therefore strongly recommended to follow the [recommendations in the Digital First Aid Kit page on recording information on attacks](/documentation).

What would you like to do?

##### Options

- [I would like to receive legal help to sue my attacker](#legal_end)
- [I think I've solved my issues](#resolved_end)

### campaign

#### I need help to campaign to stop the harassment

> If you are being attacked by more than one person, you might be the target of a hate speech or harassment campaign, and you will need to reflect on what is the best strategy that applies to your case.
>
> To learn about all the possible strategies, read [Take Back The Tech's page on strategies against hate speech](https://www.takebackthetech.net/be-safe/hate-speech-strategies).

Have you identified the best strategy for you?

##### Options

- [Yes](#resolved_end)
- [No, I need help to troubleshoot more](#harassment_end)
- [No, I need legal assistance](#legal_end)

### harassment_end

#### I need help to troubleshoot more

> If you are still under harassment and need a customized solution, please contact the organizations below who can support you.

[orgs](:organisations?services=harassment)

### physical-risk_end

#### I fear for my physical integrity and well-being

> If you are at physical risk and need immediate help, please contact the organizations below who can support you.

[orgs](:organisations?services=physical_security)

### legal_end

#### I need legal assistance

> If you need legal support, please contact the organizations below who can support you.
>
> If you are thinking of taking legal action, it will be very important to keep evidence of the attacks you were subjected to. It is therefore strongly recommended to follow the [recommendations in the Digital First Aid Kit page on recording information on attacks](/documentation).

[orgs](:organisations?services=legal)

### resolved_end

#### My issue has been resolved

Hopefully, this troubleshooting guide was useful. Please give feedback [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

- **Document harassment.** It is helpful to document the attacks or any other incident you may witness: take screenshots, save the messages you receive from harassers, etc. If possible, create a journal to systematize this documentation, recording dates, times, platforms and URLs, user ID, screenshots, description of what happened, etc. Journals can help you detect possible patterns and indications about your possible attackers. If you feel overwhelmed, think of someone you trust who could document the incidents for you for a while. You should deeply trust the person managing this documentation, as you must hand them over credentials to your personal accounts. Before sharing your password with this person, change it to something different and share it via a secure means - such as using a tool with [end-to-end encryption](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools). Once you feel you can regain control of the account, remember to change your password back to something unique, [secure](https://ssd.eff.org/en/module/creating-strong-passwords), and that only you know.

  - Follow the [recommendations on the Digital First Aid Kit page on recording information on attacks](/documentation) to store information regarding your attack in the best possible way.

- **Set up 2-factor authentication** on all your accounts. 2-factor authentication can be very effective for stopping someone from accessing your accounts without your permission. If you can choose, don't use SMS-based 2-factor authentication and choose a different option, based on a phone app or on a security key.

  - If you don't know what solution is best for you, you can check out [Access Now's "What is the best type of multi-factor authentication for me?" infographic](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) and [EFF's "Guide to Common Types of Two-Factor Authentication on the Web"](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
  - You can find instructions for setting up 2-factor authentication on the major platforms in [The 12 Days of 2FA: How to Enable Two-Factor Authentication For Your Online Accounts](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Map your online presence**. Self-doxing consists of exploring open-source intelligence on oneself to prevent malicious actors from finding and using this information to impersonate you. Learn more on how to search your online traces in [Access Now Helpline's Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- **Do not accept messages from unknown senders.** Some messaging platforms, like WhatsApp, Signal or Facebook Messenger, allow you to preview messages before accepting the sender as trusted. Apple iMessage also enables you to change settings to filter messages from unknown senders. Never accept the message or the contact if they look suspicious or you don't know the sender.

### Resources

- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- [Access Now Helpline Community Documentation: FAQ - Online Harassment Targeting a Civil Society Member](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)
- [PEN America: Online Harassment Field Manual](https://onlineharassmentfieldmanual.pen.org/)
- [Equality Labs: Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [National Network to End Domestic Violence: Documentation Tips for Survivors of Technology Abuse & Stalking](https://www.techsafety.org/documentationtips)
