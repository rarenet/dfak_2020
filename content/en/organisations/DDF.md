---
language: en
layout: organisation
name: Digital Defense Fund
website: https://digitaldefensefund.org
logo: DDF.png
languages: English, Español, Tiếng Việt
services: grants_funding, in_person_training, org_security, assessment, secure_comms, device_security, secure_comms, device_security, account, harassment
beneficiaries: hrds, activists, hros, cso, lgbti, women, youth, other
hours: Monday-Friday, 9am - 5pm, covers UTC-5 to UTC-8 (North America)
response_time: 1-2 days
contact_methods: web_form, email, pgp 
web_form: https://digitaldefensefund.org/contact-us
email: team@digitaldefensefund.org
pgp_key: https://keys.openpgp.org/vks/v1/by-fingerprint/D732ADCB38E62105054B5B418C529D41F978BF35
pgp_key_fingerprint: D732 ADCB 38E6 2105 054B 5B41 8C52 9D41 F978 BF35
initial_intake: yes

---

The Digital Defense Fund provides grants, technical assistance, and training for groups, organizations, or movements working on autonomy and liberation. We particularly focus on folks working in the abortion access and bodily autonomy space.
