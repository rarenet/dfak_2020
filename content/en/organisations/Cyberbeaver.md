---
language: en
layout: organisation
name: CyberBeaver
website: https://cyberbeaver.help
logo: Copy_of_CyberBeaver_Dark_Logo-06.png
languages: Беларуская, Русский
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, browsing, account, individual_care, censorship
beneficiaries: journalists, hrds, cso, activists, lgbti, women, youth
hours: Monday–Friday, 09:00–18:00 CET; during Saturday and Sunday requests are checked and responded in case of a security incident
response_time: within 2 hours
contact_methods: email, telegram
email: info@cyberbeaver.help
telegram: "https://t.me/cyberbeaver"
initial_intake: yes
---

CyberBeaver is a belarusian cybersecurity initiative, which provides advice and consultation relevant for local context to individuals, organizations, or communities facing digital security threats. CyberBeaver is a trusted initiative within the circles of Belarusian civic and media actors, which provides both direct cybersecurity through a chatbot and comprehensive cybersecurity audit to organizations.

CyberBeaver is also a platform with wide outreach on the topics of digital security, capable of reaching approximately 75,000 users.
