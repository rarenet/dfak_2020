---
language: en
layout: organisation
name: Tech4Peace
website: https://t4p.co/
logo: tech4peace.png
languages: English, العربية
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, harassment, advocacy, other
beneficiaries: journalists, hrds, activists, land defenders, women, youth, cso
hours: Sunday-Thursday, 09:00-17:00 UTC+3. for urgent cases the team does work on weekends.
response_time: Less than 24 hours.
contact_methods: email, mobile_app
email: info@t4p.co
pgp_key_fingerprint:
mobile_app: (for helpline) for iOS <https://apps.apple.com/us/app/tech4peace-news-image-video/id1318896273> and Android <https://play.google.com/store/apps/details?id=co.t4p.app>
initial_intake: yes
---

Building awareness through capacity building, campaigns, advocacy, research, fact-checking, technical innovation, empowering people's safety, promoting human rights, and increasing collective efforts with the aim of making everyone Tech4Peace.
