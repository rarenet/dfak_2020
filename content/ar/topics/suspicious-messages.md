---
layout: topic
title: "وصلتني رسالة مريبة"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: ar
summary: "وصلتني رسالة مريبة، عبر البريد الإلكتروني أو فيها رابط، فماذا أفعل بها؟"
date: 2023-07-15
permalink: topics/suspicious-messages
parent: /ar/
order: 4
---

# وصلتني رسالة مريبة

أكثر الرسائل المريبة رسائل التصيُّد، لكنّك قد تتلقى أحيانًا رسالة _مريبة_ في صندوق بريدك الإلكتروني، أو عبر وسائل التواصل الاجتماعي أو تطبيق تراسل . رسائل التصُّيد الهدف منها خداعك للإفصاح عن بيانات خاصة، أو مالية أو مسوِّغات الولوج إلى حساباتك. قد يُطلب منك في تلك الرسائل زيارة موقع زائف أو طلبُ رقم خدمة عملاء كاذب، كما قد تحوي رسائل التصيُّد مرفقات تزرَعُ عند فتحها برمجيات خبيثة في حاسوبك أو هاتفك.

إذا كنت غير واثقة مِنْ أصالة رسالة تلقيتها، أو محتارة بشأن كيفية التصرُّف فيها فالمسار التالي سيدلُّك لتشخيص الموقف أو الاستعانة بمنظّمة ثقة يمكنها تقديم العون لك بإجراء فحص أعمق.

تذكَّر أنَ تلقي رسالة مريبة لا يعني بالضرورة أنَّ حسابك قد اختُرِق. إذا وجدت رسالة مريبة فلا تفتحها، و&nbsp;لا تردّ عليها، و&nbsp;لا تتبع أيّ روابط فيها، و&nbsp;لا تنزِّل أيّة مرفقات منها.

## Workflow

### start

#### هل تعاملت مع الرسالة أو الروابط فيها أو مرفقاتها؟

##### Options

- [نقرتُ رابطًا](#link-clicked)
- [أدخلتُ بياناتي في صفحة](#account-security_end)
- [نزّلتُ ملفًا](#device-security_end)
- [رددتُ على الرسالة بمعلومات تخصّني](#reply-personal-info)
- [لَمْ أفعل شيئًا لحد الآن](#do-you-know-sender)

### do-you-know-sender

#### لم أتخذ أي إجراء حتى الآن

هل تعرف الشخص الذي تبدو الرسالة واردة منه؟ لاحظ أنَّ اسم مرسل الرسالة و&nbsp;عنوانه قد يكونا [منتحلين](https://ar.wikipedia.org/wiki/انتحال_بريد_إلكتروني).

##### Options

- [من شخص أو منظّمة أعرفها](#known-sender)
- [مِنْ مقدِّم خدمة (مثل البريد أو الاستضافة أو منصة تواصل اجتماعي أو بنك)](#service-provider)
- [مِنْ شخص أو منظمة لا أعرفها](#share)

### known-sender

#### إنه شخص أو منظمة أعرفها

> هل يمكنك الاتصال بالمُرسل عبر قناة اتّصال أخرى؟ مثلا إذا وصلتك الرسالة بالبريد، فهل تستطيع التحقُّق مع مُرسلها بالهاتف أو بمحادثة عبر واتسأب أو ما شابهه؟ لا تتّصل بالشخص إلا عبر وسيلة اتّصال معروفة لك مسبقًا، و&nbsp;ليس برقم مذكور في الرسالة المريبة نفسها.

هل تحقّقت من كون المرسل الظاهر في ترويسة الرسالة هو مرسلها الفعلي؟

##### Options

- [نعم](#resolved_end)
- [لا](#share)

### service-provider

#### هو مزود خدمة (مثل مزود البريد الإلكتروني أو الاستضافة أو وسائل التواصل الاجتماعي أو البنك)

> في هذا السيناريو يُعدُّ مُقدِّم الخدمة أيَّة جهة تقدِّم خدمة لك، بما فيها مقدِّمو خدمات حسابات البريد الإلكتروني (مثل گوگل أو ياهو أو ميكروسفت أو پروتونميل، إلخ) و&nbsp;منصات التواصل الاجتماعي (مثل فيسبوك و&nbsp;تويتر و&nbsp;إنستَگرام، إلخ) أو منصات مالية مثل (پايپال أو أمازون أو نِتفلِكس أو بنوكًا إلخ).
>
> هل توجد وسيلة يمكنك بها التوثُّق مِنْ الرسالة؟ كثير مِنْ مقدِّمي الخدمات يوردون البيانات المرسلة بالبريد في قسمٍ ما في صفحة حسابك لديهم. مثلا، إذا كانت الرسالة مِنْ فيسبوك فستجدها كذلك في قسم [قائمة التنويهات البريدية](https://www.facebook.com/settings?tab=security&section=recent_emails)، أو إذا كانت مِنْ البنك فيمكنك الاتّصال بخدمة العملاء.

فماذا كانت نتيجة تقصِّيك؟

##### Options

- [استوثقتُ مِنْ الرسالة بالاتصال بمقدِّم الخدمة](#resolved_end)
- [لم أستطع الاستيثاق مِنْ الرسالة](#share)
- [أنا أصلا ليس لدي حساب في تلك الخدمة و&nbsp;لا أنتظر رسائل منهم!](#share)

### link-clicked

#### لقد نقرت على ارتباط

> بعض الرسائل المريبة يمكن أن تقودك الروابط إلى صفحة ولوج زائفة الغرض منها سرقة مسوِّغات ولوجك أو إلى نوع آخر مِنْ الصفحات التي قد تسرق بياناتك الشخصية أو المالية. كما قد يوجِّهك الرابط أحيانًا إلى تنزيل مرفقات تزرع برمجيات خبيثة في حاسوبك عند فتحها.

ماذا حدث عندما نقرت الرابط؟

##### Options

- [طُلِب مني إدخال مسوِّغات ولوج](#account-security_end)
- [نَزَل ملف](#device-security_end)
- [لَمْ يحدث شيء لكني لست واثقة](#clicked-but-nothing-happened)

### clicked-but-nothing-happened

#### لم يحدث شيء لكنني لست متأكدا

> كونك لم تلاحظ حدوث شيئ عند نقرك الرابط لا يعني أنَّ ما من أمر ضار قد حدث خفية، لذا عليّك النظر في بعض السيناريوات المحتملة. أقل دواعي القلق أن تكون الرسالة التي وصلتك رسالة سُخامية لأغراض إعلانية، و&nbsp;أن يكون الغرض من الرابط توكيد عنوانك البريدي. إلا أنّ بعض الرسائل السخامية قد يكون خبيثًا أيضًا.
>
> أسوأ السيناريوات أن نقرك الرابط فعّل ثغرة أمنية في إحدى البرمجيات أو نظام التشغيل مما أدى إلى تنصيب برمجية خبيثة. في الحالات النادرة التي يحدث فيها ذلك بالرغم من كون البرمجيات مُحدَّثة تكون الثغرة المستغلّة غير معروفة.
>
> و&nbsp;في سيناريوات أخرى فباتّباعكم الرابط تقعون ضحيّة لنوع من الهجمات السبرانية اسمه [هجوم الاسكربتات العابر للمواقع XSS](https://ar.wikipedia.org/wiki/%D8%A8%D8%B1%D9%85%D8%AC%D8%A9_%D8%B9%D8%A7%D8%A8%D8%B1%D8%A9_%D9%84%D9%84%D9%85%D9%88%D8%A7%D9%82%D8%B9). مُحصَّلة هذا الهجوم سرقة الكوكيز من متصفّحك المُستخدمة لاستيثاق حساباتك في المواقع، فيمكن عندها للمهاجم الولوج إلى حساباتك. حسب منظومة الأمان في الموقع قد يتمكّن المهاجم من تغيير كلمة سرّ حسابك في الموقع أو لا. تزداد فداحة الأمر إذا كان الموقع المُعرَّض لهجمات XSS هو موقع تديرينه أنت، لأن المهاجم في هذه الحالة سيتمكّن من الولوج إليه بحساب ذي صلاحيات إدارية. لاكتشاف تعرّضك لهجوم XSS افحص الرابط الذي نقرته لترى إن كان يضمّ [عبارة برمجية](https://owasp.org/www-community/attacks/xss/)، و&nbsp;قد تكون تلك مرمَّزة.

ما الذي حدث عندما نقرت على الرابط في الرسالة؟

##### Options

- [بعض الإعلانات ظهرت. لست متأكدة إن كانت خبيثة](#suspicious-device_end)
- [متصفّحي ليس مُحدَّثا و&nbsp;قد بدأ حاسوبي أو هاتفي بالتصرف على نحو مريب](#suspicious-device_end)
- [يوجد سكربت (كود برمجي) في الرابط أو يبدو أنه مُرمّز جزئيًا](#cross-site-script)
- [لم ألحظ سكربتات](#suspicious-device_end)

### cross-site-script_1

#### هل الموقع الذي طالعك باتّباعك الرابط موقع لك فيه حساب؟

##### Options

- [نعم](#account-security_end)
- [لا](#cross-site-script-2)

### cross-site-script-2

#### هل الموقع الذي طالعك باتّباعك الرابط موقع تديره؟

##### Options

- [نعم](#cross-site-script-admin-compromised_end)
- [لا](#cross-site-script-3)

### cross-site-script-admin-compromised_end

#### أدير موقع الويب الذي أدى إليه الرابط الذي نقرت عليه

> في هذه الحالة فإنَّ المهاجم قد تكون في حوزته كوكي صحيحة تمكّنه من الولوج إلى حسابك. أوذل ما ينبغي فعله هو الولوج فورًا إلى واجهة الإدارة في موقك هذا و&nbsp;إنهاء أيّة جلسات نشطة و&nbsp;تغيير كلمة السرّ. ينبغي عليك كذلك فحص الموقع لمعرفة ما إن كان المهاجم قد رفع أي شيء خبيث، و&nbsp;إزالتها.

المنظّمات التالية يمكنها مساعدتك على فحص الموقع و&nbsp;الخادوم و&nbsp;الاستجابة إلى ذلك التهديد:

[orgs](:organisations?services=forensic)

### cross-site-script-3

#### لا، أنا لا أدير هذا الموقع

> ليس الوضع سيئا، لكن في بعض الحالات يمكن لهجمات XSS أن تستغلّ المتصفّح لشنّ هجمات أخرى.

##### Options

- [أريد تقييم ما إنْ كانت نبيطتي أصيبت بعدوى](/ar/topics/device-acting-suspiciously)
- [أظنّ كلّ شيء على ما يُرام](#final_tips)

### reply-personal-info

#### أجبت بالمعلومات

> حسب نوع البيانات التي شاركتها قد يكون التصرُّف الفوري لازمًا.

ما نوع البيانات التي شاركتها؟

##### Options

- [بيانات تخصّ حسابي](#account-security_end)
- [بيانات علنية](#share)
- [لستُ متأكدًا مِنْ درجة حساسية البيانات و&nbsp;أريد مساعدة](#help_end)

### share

#### مشاركة معلوماتك أو الرسالة المشبوهة

> إذا مرّرت إلينا الرسالة المريبة فقد يساهم ذلك في حماية زملائك و&nbsp;آخرين في مجتمعك ممّن قد يتعرَّضون لهجمات مماثلة. تُستحسن كذلك استشارة شخص موثوق به بخصوص ما إذا كانت الرسالة خطيرة.
>
> لكي ترسلي إلينا الرسالة احرصي على تضمين الرسالة نفسها و&nbsp;كذلك معلومات عن المُرسل. إن كانت الرسالة بريدية فاحرصي على تضمين الرسالة كاملة بما فيها الترويسات [بالكيفية المشروحة في دليل مركز الاستجابة للطوارئ الحاسوبية في لوكسمبورج](https://www.circl.lu/pub/tr-07).

هل تحتاج مساعدة بعد؟

##### Options

- [نعم، أحتاج مساعدة](#help_end)
- [لا، لقد انحلّت المشكلة](#resolved_end)

### device-security_end

#### لقد قمت بتنزيل ملف

> إنْ كانت بعض الملفات قد أُنزِلت إلى جهازك فقد يكون جهازك في خطر!

تواصل مع المنظمات التالية لطلب الدّعم. بعدها [شارك معنا الرسالة المريبة](#share)

[orgs](:organisations?services=device-security)

### account-security_end

#### لقد أدخلت أو قدمت معلومات الحساب

> إنْ كنت قد أدخلت مسوِّغاتك فقد يكون حسابك في خطر!
>
> إذا كنت تعتقد أنَّ حسابك مُخترَق فنحثُّك على اتباع [مسار عدِّة الإسعاف الأوّلي الرقمي بشأن الحسابات المخترقة](/ar/topics/account-access-issues).
>
> كما نحثُّك على إبلاغ الذين حولك بحملة التصيُّد هذه و&nbsp;مشاركة الرسائل المريبة مع المنظمَّات التي تحلِّلها.

هل تودين مشاركة معلومات عن الرسالة التي تلقَّيتها أم هل تريدين مساعدة قبل ذلك؟

##### Options

- [أريد مشاركة الرسالة المريبة](#share)
- [أريد مساعدة في تأمين حسابي](#account_end)
- [أريد مساعدة في فحص الرسالة](#analysis_end)

### suspicious-device_end

#### انا غير متاكد ماذا حدث

> إذا اتّبعت رابطًا و&nbsp;لست متأكدة مما حدث فقد تكون نبيطتك قد أصيبت ببرمجية خبيثة دون أن تلاحظي. إنّ أردت تحرّي هذا الاحتمال أو كان لديك شعور بأنّ نبيطتك مصابة فنحن نحثّك على اتّباع تحليل "[جهازي يتصرّف على نحو مريب](/ar/topics/device-acting-suspiciously)".
>
> إن كنت بحاجة إلى مساعدة عاجلة بسبب تصرّف نبيطتك على نحو غريب فتواصل مع إحدى المنظمّات المذكورة فيما يلي، ثم [مرّر الرسالة المريبة](#share) بعدها.

[orgs](:organisations?services=device_security)

### help_end

#### انا بحاجة الى مزيد من المساعدة

> استعن بزملائك أو بآخرين للإلمام بالمخاطر المحتملة مِنْ جَرّاء مشاركتك تلك المعلومات. فقد يكون آخرون في منظَّمتك أو شبكتك قد تلقَّوا رسائل مشابهة.

تواصل مع المنظمات التالية لطلب الدّعم. بعدها [مرذر إلينا الرسالة المريبة](#share)

[orgs](:organisations?services=digital-support)

### account_end

#### أحتاج إلى مزيد من المساعدة لتأمين حسابي

إذا كان حسابك مخترقًا و&nbsp;تحتاجين مساعدة في تأمينه فتواصلي مع المنظَّمات التالية لطلب الدعم:

[orgs](:organisations?services=account)

### analysis_end

#### أحتاج إلى مزيد من المساعدة لتحليل الرسالة

المنظَّمات التالية يمكنها تلقِّي رسائلك المريبة و&nbsp;فحصها لأجلك:

[orgs](:organisations?services=forensic&services=vulnerabilities-malware)

### resolved_end

#### نعم، لقد تم حل مشكلتي

عسى أنَّ دليل عدَّة الإسعاف اﻷولي الرقمي هذا قد أفادك، و&nbsp;نحبُّ معرفة رأيك [بالبريد الإلكتروني](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

أهمُّ ما ينبغي تذكُّره: لا تُعْطِ بيانات خاصَّة أبدا في البريد الإلكتروني. المؤسسات و&nbsp;البنوك و&nbsp;غيرها لَنْ تطلب أبدًا مثل تلك المعلومات في البريد و&nbsp;لا عبر الشبكات الاجتماعية و&nbsp;لا تطبيقات التراسل. قد يصعب أحيانًا التوثّق مِنْ صحّة رسالة بريدية أو موقع، لكن توجد نصائح تساعدك في تقييم الرسالة التي تتلقاها.

- الإلحاح: الرسائل المريبة عادة ما تحذِّرك مِنْ تغيُّر مفاجئ في حسابك و&nbsp;تطلب منك توكيد حسابك فورًا.
- في متن الرسالة البريدية قد ترى أسئلة توجِّهك إلى توكيد حسابك أو تحديث بياناتك و&nbsp;تنذرك بأنَّ تأخَّرك في فعل ذلك سيترتّب عليه وقف الحساب. المؤسَّسات ذات المصداقية التي تمدُّها ببياناتك لن تطلب منك إعادة إدخالها، فلا تقع في هذا الفخّ.
- احذر الرسائل غير المطلوبة، و&nbsp;المرفقات و&nbsp;الروابط و&nbsp;صفحات الولوج.
- انتبه إلى الأخطاء في النحو و&nbsp;الإملاء
- استعرض تفاصيل الرسالة ليظهر لك عنوان المرسل كاملًا، لا اسمه فقط
- انتبه إلى الروابط المختزلة فقد تؤدِّي إلى روابط خبيثة
- عندما تحوِّم بمؤشِّر الفأرة فوق رابط في مَتَنِ الرسالة فإنَّ المسار الفعلي الذي يربط إليه سيبين في بالون يظهر إلى جواره أو في الشريط السفلي في نافذة المتصفِّح
- ترويسات رسائل البريد الإلكتروني، بما فيها الحقل "FROM:" يمكن تزييفها. بملاحظة ترويسات SPF و&nbsp;DKMI تمكن معرفة ما إذا كان عنوان آي​پي الذي صدرت منه الرسالة مخوّل بإرسال بريد الإلكتروني من اسم النطاق هذا، و&nbsp;كذلك معرفة ما إن كان المحتوى أو الترويسات قد جرى التلاعب بها. في رسالة البريد البريد الطبيعية تكون قيمتا حقلي [SPF](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-spf) و&nbsp;[DKIM](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-dkim) دوما "PASS"، و&nbsp;إلا انتفت الثقة في صحّة الرسالة. و&nbsp;يكون السبب في ذلك إما أنّ الرسالة مزيّفة أو أن خادوم البريد ليس مضبوطًا كما ينبغي.
- [التوقيع الرقمي](https://www.gnupg.org/gph/en/manual/x135.html) يمكن أنْ يعرّفنا إن كانت الرسالة قد صدرت ممن يدّعي أنه مرسلها، و&nbsp;كذلك إن كان قد تمّ التلاعب بمضمونها. إذا كانت رسالة البريد الإلكتروني موقّعة وفق بروتوكول OpenPGP فيمكن التحقّق من صحّة التوقيع. و&nbsp;لعمل ذلك ينبغي تنصيب برمجية OpenPGP و&nbsp;استيراد المفتاح العلني للمُرسٍل. تدعم معظم تطبيقات البريد الإلكتروني الحديثة التي تدعم التوقيعات الرقمية تجري هذا تلقائيّا و&nbsp;تعلمك عبر واجهة المستخدم ما إذاكان التوقيع صحيح أم لا.
- يمكن أن يُرسِل حسابُ بريد مُخترَق رسائل صحيحة حسب المؤشّرات السابق ذكرها، إلا أنّ محتوى الرسالة في مثل تلك الحالات عادة ما يكون غريبًا. إذا بدا لك محتوى رسالة تلقيتيها غريبًا فمن الأفضل التواصل مع المثرسل عبر قناة اتّصال غير البريد للتيقّن منه قبل اتّخاذ أيّ إجراء.
- من المُستحسن دومًا كتابة و&nbsp;تأليف رسائل البريد الإلكتروني في صيغة النصوص البسيطة (plain text أو simple text) عوضًا عن صيغة النصّ الغني التي تسمح بتنسيق النصوص، و&nbsp;ذلك لأن الرسائل في صيغة HTML يمكن أن تُصنع بحيث تُخفي أكوادًا خبيثة أو مسارات غير الظاهرة. توجد [إرشادات إلى كيفية تعطيل التنسيق الغني في تطبيقات عديدة للبريد الإلكتروني](https://useplaintext.email/).
- احرصي على استخدام أحدث إصدارة من نظام التشغيل لحاسوبك أو هاتفك. التمسي الإصدارة الأحدث [لأندرويد](https://en.wikipedia.org/wiki/Android_version_history) و&nbsp;[iOS](https://en.wikipedia.org/wiki/IOS_version_history) و&nbsp;[macOS](https://en.wikipedia.org/wiki/MacOS_version_history) و&nbsp;[Windows](https://en.wikipedia.org/wiki/Microsoft_Windows)).
- حدّث نظام التشغيل في أقرب وقت ممكن و&nbsp;كلّ التطبيقات و&nbsp;البرمجيات الي نصّبتها، بخاصّة التي تستخدمها في تلقّي المعلومات (المتصفحّات و&nbsp;تطبيقات التراسل و&nbsp;عملاء البريد الإلكتروني، إلخ) أزيلي من النظام كلًّ التطبيقات التي لا تحتاجينها.
- استخدم متصفّحًا جيّدًا، مثل موزِلا فَيَرْفُكْس، وزِد أمانه بمراجعة المُلحقات المنصّبة فيه، و&nbsp;لا تترك غير ما تثق فيه منها، مثل [Privacy Badger](https://privacybadger.org/) و&nbsp;[uBlock Origin](https://ublockorigin.com) و&nbsp;[Facebook Container](https://addons.mozilla.org/en-US/firefox/addon/facebook-container/) و&nbsp;[Cookie AutoDelete](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete) و&nbsp;[NoScript](https://noscript.net/)).
- احفظي نسخًا احتياطيّة من معلوماتك
- احمِ حساباتك بكلمات سرٍّ قوية و&nbsp;بوظيفة الاستيثاق بمعاملين و&nbsp;بتضبيطات آمنة.

### Resources

فيما يلي مصادر تعينك على التعرُّف على الرسائل المريبة لتفادي الوقوع ضحيّة للتصيُّد.

- [Citizen Lab: Communities at risk - Targeted Digital Threats Against Civil Society](https://targetedthreats.net)
- [Security Self-Defense: How to Avoid Phishing Attacks](https://ssd.eff.org/ar/module/%D8%AF%D9%84%D9%8A%D9%84-%D9%83%D9%8A%D9%81%D9%8A%D8%A9-%D8%AA%D8%AC%D9%86%D8%A8-%D9%87%D8%AC%D9%85%D8%A7%D8%AA-%D8%A7%D9%84%D8%AA%D8%B5%D9%8A%D8%AF-phishing)
- Google message header analysis tool](https://toolbox.googleapps.com/apps/messageheader/)
- [Security without borders: Guide to Phishing](https://guides.securitywithoutborders.org/guide-to-phishing/)
