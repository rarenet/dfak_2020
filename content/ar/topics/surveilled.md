---
layout: topic
title: "أظنّ أنّ ثمّة من يُراقبني"
author: Carlos Guerra, Peter Steudtner, Ahmad Gharbeia
language: ar
summary: "إذا كنت تظنّ أنّك مُستهدف بالمراقبة السبرانية فهذا التحليل قد يساعدك على فهم ما يجري"
date: 2023-08
permalink: topics/surveilled
parent: Home
order: 11
---

# أظنُّ أنّ ثمّة مَنْ يراقبني

نشهد مؤخّرًا حالات يتعرَّض فيها أشخاص مدنيون للمراقبة، باستخدام أدوات التتبُّع من قِبَل أزواج\قُرناء و&nbsp;كذلك أصحاب الأعمال و&nbsp;المديرين، أو المراقبة الشاملة من قِبَل الحكومات و&nbsp;حملات استهداف النشطاء و&nbsp;الصحافيين ببرمجيات التَّجسُّس. و&nbsp;بالنظر إلى الطيف العريض من التقنيات و&nbsp;الوقائع فمن غير الممكن تناول كُلِّ جواتنب المراقبة هنا، بخاصة لحالات الطوارئ.

و مع هذا فإنَّ هذا التحليل يتناول الحالات الشائعة و&nbsp;تلك المتعلّقة بالنبائط و&nbsp;خدمات الإنترنت التي يستخدمها الأفراد، و&nbsp;التي تُشكِّل طوارئ تُمكن الاستجابة لها. أما عندما لا توجد حلول قابلة للتطبيق فلن نعتبر تلك الحالة طارئًا ولن نتعرّض لها بالتفصيل، بل سنشير إلى مصادر أخرى.

بعض الحالات التي لا يشملها هذا التحليل:

- المراقبة الماديّة. إذا كنت تظنّين أنّك مستهدفة بالمراقبة الماديّة و&nbsp;تحتاجين مساعدة فلك أن تتواصلي مع [المنظمات التي تٌقدِّم الدعم في مثل هذه الحالات](/ar/support).
- المراقبة الشاملة بكميرات المراقبة في الأماكن العامّة و&nbsp;المساحات الخاصّة و&nbsp;مقار الجهات المختلفة.
- نبائط التجسّس غير المعتمدة على الاتصال بالإنترنت.

و إبراءً للذمة، تنبغي الإشارة هنا كذلك إلى حقيقة خضوع الناس كافةً إلى المراقبة على نحو أو آخر في زماننا هذا:

- شبكات الهواتف المحمولة مُصمَّمة بحيث تجمع كموما هائلة من البيانات عن مستخدميها و&nbsp;يمكن لجهات عديدة أن تحصل منها على معلومات منها مواقع تواجدنا و&nbsp;شبكة معارفنا و&nbsp;غيرها.
- الخدمات على الإنترنت مثل شبكات التواصل الاجتماعي و&nbsp;مُقدِّمي خدمات البريد الإلكتروني يجمعون عنّا بيانات كثيرة يستغّلونها لتوجيه الإعلانات التجارية.
- يتزايد تطوير حكومات دول العالم نُظمًا مترابطة تجمع و&nbsp;تُصنِّف معلومات عن الأفراد تربطها بهوياتهم الرسمية القانونية صانعةً ملفات تعريف لكُلِّ شخص.

هذه الأمثلة من المراقبة الشاملة، و&nbsp;الحالات العديدة المرصودة من الاستهداف بالمراقبة، تجعلنا نظُنُّ أنّ كُلَّ الناس مُراقبون عن كثب، بخاصة النشطاء و&nbsp;العاملين في المجتمع المدني، و&nbsp;هو ما يتسبب في ضغوط نفسية و&nbsp;يزيد الحمل العاطفي علينا بما يُعيق قدرتنا على مواجهة المخاطر الفعلية التي قد نتعرّض لها. و&nbsp;مع ذلك ففي حالات معيّنة، و&nbsp;حسب طبيعة عملنا و&nbsp;طبيعة المخاطر التي نتعرّض لها و&nbsp;كذلك حسب قُدرات خصومنا، فقد توجد قرائن معقولة على كوننا مراقبين.

و من باب إبراء الذّمة أيضًا ينبغي القول إنَّ كثيرًا من وسائل المراقبة، بخاصة الموجّهة عادة لا تترك أثرًا يمكن للمُراقَب إدراكه. ينبغي أخذ هذا في الحُسبان أثناء اتّباع هذا التحليل، و&nbsp;إذا رأيت أن حالتك غير مشمولة أو هي مُعقَّدة أكثر مما يُتناول هُنا، فتواصل مع إحدى المؤسّسات المذكورة في آخر التحليل.

ينبغي أيضًا الانتباه إلى أنّ المراقبة قد تكون شاملة أو موجّهة. المراقبة الشاملة تستهدف عادة جماعة كبيرة من الناس، مثل كُلِّ مستخدمي منصة تواصل اجتماعي بعينها، أو كُلَّ مواطني دولة ما، أو سكان إقليم ما، و&nbsp;هذا النوع من المراقبة يوظّف التقنية و&nbsp;عادة ما يكون أقلّ خطرًا في المدى المنظور من المراقبة المُوجّهة التي تستهدف شخصًا أو زُمرة من الأفراد و&nbsp;تتطلّب تخصيص موارد أكثر نسبيًا، و&nbsp;عادة ما تكون أخطر بالنظر إلى الدافع.

## Workflow

### start

#### ما نوع المشكلة التي تواجهها؟

> أي الشواهد التالية على المُراقبة في رأيك أقرب إلى وصف حالتك مع أخذ ما ورد في المقدِّمة في الاعتبار ؟

##### Options

- [وجدتُ نبيطة تتبُّعٍ في نطاقِ وجودي](#tracking-device-intro)
- [جهازي يتصرَّف على نحو مريب](#device-behaviour-intro)
- [يُعاد توجيه متصفّحي إلى مواقع وِب ببروتوكول HTTP غبر المؤمّن بالتعمية](#suspicious-redirections-intro)
- [تصلني تنويهات أمنية من خدمات موثوق فيها على الوِب](#security-alerts-intro)
- [وجدتُ نبيطة مريبة مشبوكة بحاسوبي أو شبكتي المحليّة](#suspicious-device-intro)
- [وصلت إلى خصومي معلومات كنت قد شاركتها عبر قنوات اتّصال خاصّة](#leaked-internet-info-intro)
- [وصلت إلى خصومي معلومات خاصة كنت قد شاركتها بطريق محادثات هاتفيّة أو رسائل SMS](#leaked-phone-info-intro)
- [بعض من حولي جرى استهدافهم بالمراقبة](#peer-compromised-intro)
- [شيء غير ذلك \ لا أعلم](#other-intro)

### tracking-device-intro

#### لقد وجدت جهاز تتبع بالقرب مني

> إذا وجدت نبيطة غير متوقّعة في محيط تواجدك و&nbsp;تظنّ أنها موجودة لمراقبتك على نحو ما، فالخطوة الأولى هي التيقُّن من كونها نبيطة مراقبة و&nbsp;لا وظيفة أخرى لها. فبزيادة تطبيقات "إنترنت أﻷشياء" (Internet of Things المعروفة بالاختصار IoT) أصبح من الشائع وجود نبائط تؤدّي وظائف عديدة قد ننسى بعد حين وجودها. و&nbsp;أولى خطوات معرفة إذا كانت النبيطة المشكوك في أمرها نبيطة تجسُّس فحصها بحثًا عن علامات تدلُّ على صانعها و&nbsp;موديلها و&nbsp;ما إلى ذلك من العلامات الموجودة عادة على النبائط التجارية. نبائط التتبع التجارية المتاحة في الأسواق بأسعار متوسّطة تشمل Apple Airtags و&nbsp;Samsung Galaxy SmartTag أو Tile trackers. تُباع هذه النبائط بغرض مساعدة الناس على تعيين مواضع أغراضهم الشخصية، إلّا أنَّها يمكن أن تُستخدم في تتبع الأشخاص دون رغبتهم.

> يمكن باستخدام الوسائل التالية الكشف عن وجود بعض تلك النبائط التجارية مخفية في محيط وجودك:

> - إذا كنت تستخدم نظام التشغيل iOS أو نبيطة عاملة بالنظام iPhone [فستتلقّى تنويها عند تحسّس نظامك تكرار وجود نبيطة تتبُّعٍ من طراز Apple Airtags في محيطك لا تخُصُّك](https://support.apple.com/ar-eg/HT212227).
> - يوجد [تطبيق لنظام التشغيل أندرويد لتحسُس وجود نبائط Airtags في محيطك](https://play.google.com/store/apps/details?id=com.apple.trackerdetect).
> - توجد في [Tile app](https://www.tile.com/download) وظيفة للكشف عن وجود نبائط تتبّع غير معروفة من طراز Tile.
> - يمكن باستعمال [SmartThings app](https://www.samsung.com/us/smartthings/) الكشف عن وجود نبائط التتبُّع SmartTags.

> ضمن أنواع نبائط التجسُّس التي قد تجدها مقتفيات GPS، و&nbsp;هي تسجِّل موضع وجودها دوريًا ليمكن بعد استعادتها استنباط مسارات تحرُّكها خلال مدة زمنيّة معيّنة، و&nbsp;بعض أنواعها يمكنه إرسال بيانات الموضع عبر خطوط الهاتف المحمول. إن وجدت نبيطة كهذه ففهم أساس عملها يفيد في معرفة مَنْ ورائها.

> و&nbsp;أيٌّا كان نوع نبيطة التَّبُّع المُكتشَفَة فيجب تسجيل كُلِّ ما تُمكنُ معرفته عنها: صانعها، و&nbsp;طرازها، و&nbsp;إن كانت تتَّصِل بشبكة لاسلكية و&nbsp;موضع اكتشافها بالتحديد، و&nbsp;الأسماء المتعلِّقة بها في التطبيق الذي كشفها، إن كانت هذه هي وسيلة كشفها.

بعد التعرُّف على النبيطة يُمكن فعل أحد أمرين:

##### Options

- [تعطيل نبيطة التتبُّع](#tracking-disable-device)
- [استخدامها لتضليل مشغِّلها](#tracking-use-device)

### tracking-disable-device

#### أريد تعطيله

قد يُمكن إطفاء النبيطة المعثور عليها أو تعطيلها نهائيا، و&nbsp;ذلك حسب طرازها. و&nbsp;في بعض الحالات قد يمكن فعل ذلك باستخدام تطبيقات التتبُّع مثل الموصوفة في الخطوة السابقة، أو بأزرار التشغيل و&nbsp;الإطفاء في جسمها، أو بتخريبها. مع ملاحظة أنَّك إنْ أردت تحرّي مسألة تتبّعك على نحو أعمق فإنَّ تعطيل النبيطة قد يغيِّر حالة القرائن المعلوماتية المُخزّنة فيها.

هل تريد التحرّي عن هوية مَنْ يشغِّل النبيطة؟
(_مع ملاحظة أن ذلك يستلزم إبقاءها عاملة_)

##### Options

- [نعم](#identify-device-owner)
- [لا](#pre-closure)

### tracking-use-device

#### أرغب في استخدامه لتضليل المشغل

> إحدى الاستراتيجيات الشائعة للتعامل مع نبائط التّتبُّع جعلها تتتبّع شيئًا غير المعني بعملية التتبُّع. مثلا، يمكنك تركها في موضع آمن أثناء انتقالك إلى مواضع أخرى، أو يمكن جعلها تتتبّع هدفًا مُتحرِّكًا غيرك. مع ملاحظة أنَّك في مثل هذه الحالة قد تفقد النبيطة و&nbsp;أنَّ جعل النبيطة تقتفي شخصًا غيرك أو وضعها في بعض المواضِع قد تكون له تبِعات أمنية أو قانونية غير مرغوبة.

هل تريدين التحرّي عن هوية مَنْ يشغِّل النبيطة؟
(_مع ملاحظة أن ذلك يستلزم إبقاءها عاملة_)

##### Options

- [نعم](#identify-device-owner)
- [لا](#pre-closure)

### identify-device-owner

#### أريد التعرف على صاحب الجهاز

> تحديد هويّة مشغِّل نبيطة التتبُّع قد يكون صعبًا في بعض الحالات، و&nbsp;هو يتوقَّف كُلِّيَّةً على نوعها، لذا نوصي بمعرفة كُلٍِ ما تمكن معرفته تفصيلًا عن النبيطة المُكتشفَة، و&nbsp;بعض ما تنبغي معرفته:

> - فيما يتعلَّقُ بالنبائط التجارية للمستخدمين العاديين:
>   - كيف تظهر في تطبيق الفحص؟
>   - كيف وصلك التنويه؟
>   - هل ثمّة وسيلة لاستخراج مزيد من المعلومات من النبيطة؟
>   - هل تحوي مخزن معلومات داخلي؟ بعض الطُرُز تحوي شرائح ذاكرة SD يمكن نزعها لاستخراج البيانات المُخَزَّنة عليها
>   - هل بها خط هاتف؟ هل توجد بها شريحة SIM يمكن نزعها و&nbsp;وضعها في هاتف آخر لمعرفة رقمها؟ بهذه المعلومة قد تُمكن معرفة مَنْ سُجِّل الخطّ باسمه.
>   - هل توجد علامات ماديّة، مثل اسم مكتوب عليها أو رقمٍ مخزني؟
>   - إذا كانت النبيطة تتَّصلُ بشبكة WiFi المحلية، فهل تظهرُ في اسمها مقاطع دالة؟
>   - مَنْ يستطيع النفاذ إلى الموضع الذي اكتُشِفَت فيه نبيطة التتبُّع؟

هل تريد اتّخاذ إجراء قانوني ضدَّ مُشغِّل نبيطة التتبُّع؟

##### Options

- [نعم](#legal-action-device-owner)
- [لا](#pre-closure)

### legal-action-device-owner

#### أريد اتخاذ الإجراءات القانونية ضد صاحب الجهاز

> إذا أردت اتّخاذ إجراء قانوني في حقِّ مُشغِّل نبيطة التتبُّع فينبغي عليك معرفة كيفية سير مثل هذه الإجراءات في القضاء الذي تعيش فيه. في بعض الحالات قد يستلزم ذلك توكيل محامٍ، و&nbsp;في أحيان أخرى قد يكفي إبلاغ الشرطة. و&nbsp;في كُلِّ الأحوال فإنَّ درجة إحكام القضية ستكون بدرجة بإحكام القرائن المُقدَّمة، لذا يُستحسن اتِّباع الإرشادات المُقدَّمة في الخطوات السابقة من هذا التحليل، و&nbsp;كذلك مطالعة إرشادات [توثيق الطوارئ السبرانيّة](/ar/documentation).

هل تريد مساعدة لاتّخاذ إجراء قانوني ضد مُشغِّل نبيطة التتبُّع؟

##### Options

- [نعم، أريد مساعدة](#legal_end)
- [لا، أظنّ مشكلتي انحلّت](#resolved_end)
- [لا، لكنني أريد أن آخذ في الحسبان احتمالات أخرى للمراقبة](#pre-closure)

### device-behaviour-intro

#### جهازي يتصرف بشكل مريب

> إحدى أكثر وسائل المراقبة شيوعًا زرع برمجيات تجسّس في الهواتف المحمولة أو الحواسيب، و&nbsp;هي برمجيات خبيثة مُصمَّمة بغرض جمع بيانات من النبيطة المنزرعة فيها ثُمَّ إرسالها إلى من يتحكَّم فيها. يمكن أن تُزرع برمجيات التجسُّس بأساليب متباينة، مثل أنْ يُنصِّبها الأزواج\الأقران في نبائط يستخدمها أزواجهم، و&nbsp;هي ما تُعرف ببرمجيّات التجسُّس الزوجي، أو بإرسال روابط إنترنت في رسائل تدفع متلقيها إلى تنزيل و\أو تنصيب البرمجيّة الخبيثة، أو باستغلال ثغرات أمنية في نظم التشغيل و\أو التطبيقات، و&nbsp;هو أسلوب تتبّعه الجماعات الإجرامية و&nbsp;كذلك أجهزة المخابرات الحكومية.

> بعض أعراض التعرُّض للمراقبة تشمل عثورك على تطبيقات غريبة مُنصَّبة في نبيطتك لها صلاحيّات استغلال بعض أو كلٍّ من: الميكروفون، و&nbsp;الكامرا و&nbsp;الشبكة، أو ملاحظة ظهور إشارة كون الكامرا عاملة بالرغم من عدم تشغيلك أي تطبيق يستعملها، أو اكتشافك تسرُّب مُحتوى بعض الملفات.

> في هذه الحالات ننصح بالرجوع إلى تحليل "جهازي يتصرّف على نحو مريب" من عُدَّة الإسعاف الأوّلي الرقمي.

ماذا تريدين أن تفعلي؟

##### Options

- [أريد مطالعة تحليل "جهازي يتصرّف على نحو مريب"](/ar/topics/device-acting-suspiciously)
- [أريد المواصلة إلى الخطوة التالية](#pre-closure)

### suspicious-redirections-intro

#### تتم إعادة توجيهي إلى مواقع http غير المحمية لتثبيت التحديثات أو التطبيقات

> أحيانًا يعرض المتصفِّح تحذيرات عندما نشرع في أنْ نُنزِّل يدويًا عبر مواقع الوِب تحديثًا لتطبيق نستخدمه أو لنظام التشغيل، تلك التحذيرات عادة ما تتعلّق بالشهادات الرقمية، مثل "شهادة غير مطابقة لاسم النطاق" أو "شهادة منتهية صلاحيتها" أو الغامضة "تعذّر الاستيثاق".

> و&nbsp;لذلك أسباب متنوّعة، و&nbsp;قد تكون مشروعة أو خبيثة، لذا ينبغي علينا التفكير في المسائل التالية:
>
> - هل تاريخ و&nbsp;ساعة الجهاز مضبوطان؟ الكيفية التي تعمل بها آلية الاستيثاق ما بين النظم الحاسوبية، و&nbsp;منها متصفحات الوِب و&nbsp;خواديمها، تعتمد على التحقُّق من صحَّة شهادات رقمية لا تكون صالحة إلّا لفترة زمنية محدودة، لذا، فإذا كانت نبيطتك تاريخها مضبوطًا على السنة الماضية فإنَّ الاتّصال الآمن بالمواقع و&nbsp;الخدمات سيتعذَّر. أمّا إذا كان التاريخ و&nbsp;الساعة مضبوطين فقد يكون معنى رسالة التنويه أنّ مدير الموقع أو الخدمة لَمْ يجدِّد الشهادة الرقمية في نظامه. إمّا ذلك أو أنّ أمرًا مريبًا يجري. في كُلِّ الحالات يجب تحديث البرمجيات أو نظام التشغيل دومًا باتّصال آمن، و&nbsp;الامتناع عن مواصلة إجراء التحديث إنْ ظهرت تنويهات مريبة.
> - هل بدأت الأعراض في الظهور بعد واقعة غير معتادة؟ مثل النقر على إعلان أو تنصيب تطبيقٍ ما أو فتح ملف وثيقة تلقيتها بإحدى السبُل؟ الكثير من الهجمات يعتمد على تمويه البرمجية الخبيثة في مظهر تطبيق اعتيادي مشروع، مثل مُنضِّبات التطبيقات و&nbsp;التحديثات، لذا فقد يكون نظامك قد تحسّس الفخّ فأظهر لك تنويها أو أوقف الإجراء.
> - هل لاحظت اختلافات في كيفية جريان الصيرورة التي تسبّبت في التنبيه؟ كأن تختلف صيرورة التحديث الجارية التي ظهر بشأنها التنويه عن الصيرورة المعتاد جريانها عند تحديث التطبيق.

> في كُلِّ الأحوال يجب الاعتناء بتوثيق كُلِّ ما يجري بِدِقَّةٍّ، خصوصًا إذا ما كنت تنوي اللجوء إلى مساعدة من مُتخصِّصين، أو تسعى لتحرّي الأمر على نحو أعمق، أو اتّخاذ إجراء قانوني.

> في أغلب الحالات يكفي تدارك سبب تعذّر الاتصّال الآمن لتنحلَّ المشكلة و&nbsp;تعود الأمور إلى نصابها، لكن الأمر قد يكون أكثر تعقيدًا و&nbsp;تبعاته أخطر، إن كنت قد نصّبت بالفعل تطبيقات أو تحديثات نزّلتها في تلك الملابسات المريبة.

هل نزّلت أو نصّبت برمجيات أو تحديثات في ملابسات مريبة؟

##### Options

- [نعم](#suspicious-software-installed)
- [لا](#pre-closure)

### suspicious-software-installed

#### لقد قمت بتنزيل البرنامج أو تثبيته في ظل ظروف الشكوك

> في أغلب الأحيان تهدف الهجمات التي تحدث بطريق تنصيب أو تحديث التطبيقات أو نظام التشغيل إلى تنصيب برمجية خبيثة في النظام. فإذا شككت في أنَّ هذا هو الحاصل فننصحك باتّباع تحليل عُدَّة الإسعاف الأولي الرقمي "جهازي يتصرَّف على نحو مريب".

ماذا تريد أن تفعل؟

##### Options

- [أريد مطالعة تحليل "جهازي بتصرَّف على نحو مريب"](/ar/topics/device-acting-suspiciously)
- [أريد المواصلة إلى الخطوة التالية](#pre-closure)

### security-alerts-intro

#### أتلقى رسائل وتنبيهات أمنية من الخدمات الموثوقة

> إذا تلقَّيت رسائل تنويه أو تحذير من مُقدِّم خدمة البريد الإلكتروني أو منصَّة التواصل الاجتماعي أو خدمة أخرى على الإنترنت تستخدمها، و&nbsp;كانت تلك التنويهات متعلّقة بمسئلة أمنية، مثل محاولات الولوج من نبائط أو مواضع غير مشهودة من قبل، فينبغي عليك أوّلا التحقّق من مصداقيتها. قد يساعدك تحليل "[تلقيّت رسالة مريبة](/ar/topics/suspicious-messages)" في عمل ذلك، قبل مواصلة هذا التحليل.

> إذا كانت الرسالة صادقة، فعليك إذن التفكير في سببها. على سبيل المثال، إذا كنت قد استخدمت مؤخّرًا VPN أو تور للنفاذ إلى الخدمة المعنية بالرسالة فقد تظنُّ نُظُم الأمان فيها أنّك تتصلُّ من موضع غير المعتادة عليه منه.

> من المفيد كذلك التمعُّن في نوع الإنذار الذي وصلك: هل يتعلّق بمحاولة للولوج إلى حسابك؟ أم بقيام شخص بالولوج إلى الحساب بالفعل؟ فكلُّ حالة تستتبع تصرُّفًا مختلفًا بالكُلِّيَّة.

> إذا وجدت ما يريب في سجّلات النشاط المتعلّقة بحسابك في الخدمة المعنيّة فقد يتطلب ذلك بعض أو كُلَّ ما يلي:
>
> - تغيير كلمات السرّ
> - تفعيل وظيفة الاستيثاق بمعاملين
> - فصل كُلُّ النبائط المُتًّصلة بالحساب
> - توثيق الدلائل على محاولة الاختراق

> من المحدِّدات الهامة المساعِدة على تقييم درجة إلحاح مثل هذه التنويهات كوننا نتشارك في استخدام هذا الحساب مع آخرين قد تكون تصرُّفاتهم تسبَّبت في التحذير. إذا كان الحال كذلك فتنبغي مراجعة الصلاحيات الممنوحة للآخرين على حساباتك، و&nbsp;كذلك بروتوكولاتكم لاستعمال الحسابات المشتركة.

إذا كنت تعتقد بوجود مخاطر أخرى محتملة ضمن المسرودة في رأس هذا التحليل، أو كانت حالتك مُعقَّدة أكثر و&nbsp;تريد مساعدة، فاضغط "واصل"

##### Options

- [واصل](#pre-closure)

### suspicious-device-intro

#### لقد وجدت جهازًا مشبوهًا مرتبطًا بجهاز الكمبيوتر أو الشبكة

> إذا اكتشفت وجود نبيطة مريبة فأوّل ما ينبغي التفكير فيه هو إنْ كانت هذه النبيطة خبيثة، أو يُفترض وجودها حيث اكتُشِفَت. ففي السنوات القليلة الماضية ازداد عدد النبائط الشبكيّة المتواجدة في محيط المنزل و&nbsp;العمل: الطابعات و&nbsp;الإضاءات الذكيّة و&nbsp;الغسّالات و&nbsp;منظّمات التدفئة، و&nbsp;غيرها. و&nbsp;بسبب هذه الزيادة في عددها فقد ننسى أحيانًا وجود بعضها فنظّنُ أنّها مدسوسة علينا.

> إذا لم تكوني متيقّنة من كون النبيطة التي اكتشفتها خبيثة فحاولي الحصول على معلومات عنها، صانعها و&nbsp;طرازها و&nbsp;ما إذا كانت مُتَّصلة بالشبكة المحليّة سلكيًّا أو لا سلكيًّا، و&nbsp;ما إذا كانت عاملة، و&nbsp;ما إذا كانت مكتوبًا عليها معلومات قد تفيد في التعرُّف على وظيفتها.

بعد فحص النبيطة المُكتشَفة، هل استنتجت كونها مشروعة؟

##### Options

- [هي نبيطة مشروعة](#pre-closure)
- [فحصت النبيطة لكنّني لم أتمكن من استنتاج الغرض منها و&nbsp;ما إذا كانت خبيثة](#suspicious-device2)

### suspicious-device2

#### لقد قمت بالتحقق ولكن ما زلت لا أعرف ما يفعله الجهاز أو ما إذا كان ضارًا

> إذا كنت لا تزال قلِقًا بشأن النبيطة المُكتشَفَة بعد الفحص الأوّلي، فثمَّة احتمال بكونها نبيطة تجسًّس. بعض هذه النبائط يراقب الشبكات الحاسوبية المتّصل بها، أو قد يُسجِّل الصوت و&nbsp;الصورة من محيطه و&nbsp;يُرسلهما عبر الشبكة أو لا سلكيًّا كالبرمجيّات الخبيثة. لكن تنبغي ملاحظة أنَّ هذا السيناريو غير شائع و&nbsp;عادة ما يتعلّق بالأشخاص المُعرّضين لمخاطر كبيرة، فإذا رأيت أنَّ هذا ينطبق عليك، فإليك بعض ما يمكن فعله:
>
> - فصل الشبكة عن الشبكة الحاسوبية و\أو شبكة الطاقة الكهربية ثُمَّ التحقُّ من كون كُلِّ النبائط الأخرى المشروعة تعمل كما ينبغي. بعدها يمكن لخبير فحص النبيطة المُكتشَفَة.
> - البحث بتعمُّق أكثر و&nbsp;طلب مساعدة، و&nbsp;هذا يتضمَّن عادة اختبارات تقنية مثل تحليل الاتصالات الشبكية أو تحسُّس الإشارات اللاسلكية.
> - يمكن استخدام VPN أو تور أو ما شابهها من وسائل لحماية الاتصالات بالإنترنت بطريق التعمية من الطرف إلى الطرف، و&nbsp;ذلك كإجراء وقائي في حالة قرَّرت الإبقاء على النبيطة المَكتَشَفة متّصلة بالشبكة لحين دراسة الموقف.
> - عزل النبيطة ماديًّا بحيث لا تعود قادرة على التقاط الصوت و&nbsp;الصورة من محيطها.

إذا كنت تعتقد بوجود مخاطر أخرى محتملة ضمن المسرودة في رأس هذا التحليل، أو كانت حالتك مُعقَّدة أكثر و&nbsp;تريد مساعدة، فاضغط "واصل"

##### Options

- [واصل](#pre-closure)
- [أظن مشكلتي قد انحلّت](#resolved_end)

### leaked-internet-info-intro

#### المعلومات التي قمت بمشاركتها بشكل خاص عبر البريد الإلكتروني أو المراسلة أو ما شابه ذلك معروفة للخصم

> هذا الفرع من التحليل يتناول تسريب البيانات من الخدمات على الإنترنت و&nbsp;الأنشطة المرتبطة بها. فيما بتعلَق بتسريب البيانات من التلفونات المحمولة ارجعوا إلى [القسم الذي يتناول تسرٌّب بيانات خاصّة تشاركتها عبر مكالمات هاتفية أو رسائل SMS](#leaked-phone-info-intro).

> تقع التسريبات من خدمات الإنترنت على الأغلب لأحد الأسباب التالية:

> 1. المعلومات كانت علنية في الأصل،و هنا قد تفيدك مطالعة [التحليل المتعلّق بتسريب البيانات الشخصية(/ar/topics/doxing)
> 1. المعلومات تسرّبت مباشرة من الحساب، و&nbsp;هو الأسلوب الذي يتِّبعه عادة الخصوم الأقرب إلى الشخص المُستَهدَف.
> 1. المعلومات تسرَّبت من ناحية المنصّة أو مُقدِّم الخدمة التي حُفِظَت فيها، و&nbsp;هذا الأسلوب تقدر عليه حكومات الدول الكبرى و&nbsp;الشركات الكبرى.

> في هذا التحليل نتناول الحالتين الأخيرتين.

هل تتشارك في النبائط أو الحسابات مع آخرين؟

##### Options

- [نعم](#shared-devices-or-accounts)
- [لا](#leaked-internet-info2)

### shared-devices-or-accounts

#### نعم، أشارك الأجهزة أو الحسابات

> في بعض الحالات يؤدّي التشارك في الحسابات و&nbsp;النبائط مع آخرين إلى قدرة أشخاص ليسوا في الحسبان على النفاذ إلى بيانات حسّاسة، مما قد يؤدّي إلى تسرُّبها. كما أنًّنا عندما نتشارك في الحسابات فإنّنا عادة نلجأ إلى تسهيل ذلك بما يُقلِّل من أمانها، مثل وضع كلمة سرِّ سهلة التذكُّر، و&nbsp;من ثمَّ التخمين، و&nbsp;تعطيل الاستيثاق عديد المعاملات (2FA أو MFA) لتسهيل ولوج أكثر من شخص إلى الحساب.

> لذا فمن أوّل ما يجب فعله مراجعة مَنْ لهم نفاذ إلى الحسابات و&nbsp;الوثائق (مثل مخازن الملفات السحابية و&nbsp;تطبيقات العمل التشاركية) و&nbsp;قصر ذلك على الأشخاص الذين يلزمهم ذلك. من المبادئ الهامّة أيضًا وجود حساب لكُلِّ شخص، مما يتيح تفعيل وسائل الأمان المناسبة، و&nbsp;تحديد صلاحيات الأشخاص حسب أدوارهم، و&nbsp;المحاسبة بطريق وضوح تصرفات كل مستخدم في سجّلات النظام.

> من المُحبَّذ كذلك مطالعة [القسم الذي يتناول رسائل التحذير من الخدمات الموثوق بها](#security-alerts-intro).

في القسم التالي نستكشف الموقف على نحو أعمق.

##### Options

- [واصل](#leaked-internet-info2)

### leaked-internet-info2

#### اكتشف المزيد بالتفصيل

هل في وسع المهاجم التحكم في حساباتك على الإنترنت أو النفاذ إليها؟

##### Options

- [نعم](#control-of-online-services)
- [لا](#leaked-internet-info3)

### control-of-online-services

#### نعم، يستطيع خصمي التحكم في الخدمات أو الوصول إليها

> في بعض الحالات قد يتمكّن الخصوم من النفاذ إلى مخازن بيانات المنصّات التي نستعملها أو التحكُّم فيها، و&nbsp;بعض الجوانب التي ينبغي التفكير فيها في هذه المسألة:

> - من يملك الخدمة
> - هل المالك نفسه خصم
> - هل للخصم نفاذ إلى البنية التحتية التقنية للخدمة، بخواديمها و&nbsp;شبكتها، بطريق قضائي؟

> في تلك الحالات من المستحسن الامتناع عن استعمال الخدمة المعنيّة و&nbsp;الانتقال إلى غيرها.

> من المؤشِّرات المفيدة أيضًا:

> - المواضع الجغرافية التي يلج منها المهاجمون إلى الحسابات المُستهدفة
> - التغيّر غير المتوقّع في حالة الوثائق و&nbsp;الرسائل، من حيث كونها قد سبقت قراءتها من عدمه، أو آخر تحرير أو حفظ لها
> - الإعدادات التي تؤثِّر على كيفية معالجة الرسائل أو الملفات، مثل إعادة توجيهها أو تمريرها إلى آخرين، أو مشاركتها مع حسابات أخرى، أو وسمها، إلخ.

> يمكنك كذلك التحقُّق من كون الرسائل تُقرأ من قِبَل شخص آخر بطريق استخدام أمارات الكناري، مثل التي يُمكن توليدها في [canarytokens.org](https://canarytokens.org/generate) أو ما يُشبهها.

فيما يلي نواصل استكشاف الموقف.

##### Options

- [واصل](#leaked-internet-info3)

### leaked-internet-info3

#### اكتشف المزيد بالتفصيل

هل لدى خصمك القدرة على الحصول على عتاد المراقبة و&nbsp;استعماله و&nbsp;إيصاله إلى محيطك؟

##### Options

- [نعم](#leaked-internet-info4)
- [لا](#pre-closure)

### leaked-internet-info4

#### نعم، لديهم إمكانية الوصول إلى معدات المراقبة القريبة

> الخصوم ذوي الإمكانات الكبيرة قد يكونون قادرين على إيصال عتاد مراقبة مُتخصِّص إلى محيط مَنْ يستهدفون، مثل زرع نبائط التتبُّع أو زرع البرمجيات الخبيثة في الحواسيب و&nbsp;النبائط المحمولة الأخرى من تلفونات وغيرها.

هل تظنّ أنَّك مُراقَب باستخدام نبيطة أو أنَّ حاسوبك أو تلفونك المحمول قد تكون مزروعة فيه برمجيات تتجسَّس عليك؟

##### Options

- [أريد معرفة المزيد عن نبائط التتبُّع](#tracking-device-intro)
- [أريد أن أعرف ما إذا كانت نبيطتي الحاسوبية مختَرَقة ببرمجيات خبيثة](#device-behaviour-intro)

### leaked-phone-info-intro

#### أرغب في معرفة المزيد عن أجهزة التتبع

> كما بيّنا في مقدِّمة هذا التحليل فإنَّ المراقبة الشاملة للاتصالات الهاتفية منتشرة في العالم بحيث يمكننا القول أنَّها تمارس في أغلب بلاد العالم، إنْ لَمْ يكن جميعها. في الحدَّ الأدنى ينبغي علينا إدراك السهولة البالغة التي يمكن بها لمُشغّلي الشبكات الهاتفية، و&nbsp;كذلك من له سُلطة عليهم، الوصول إلى معلومات من قبيل:

> - تاريخ تحرُّكات الشخص، بطريق معرفة أبراج شبكة الاتّصالات التي انشبك بها الهاتف المحمول.
> - سجلّات المكالمات الواردة و&nbsp;الصادرة، تواريخها و&nbsp;أطرافها و&nbsp;مُدَدُها
> - محتوى الرسائل النصية و&nbsp;بياناتها الفوقية، من متراسلين و&nbsp;تواريخها
> - تدفقات الاتّصال بالإنترنت، بياناتها الفوقية، و&nbsp;كذلك فحوى الاتّصالات في حال كونها غير مُعمّاة من الطرف إلى الطرف.

> ضمن وسائل المراقبة غير الشائعة نَجِدُ استخدام أجهزة خاصة لاعتراض الاتصالات الهاتفية تُعرف باسم stingrays أو IMSI-catchers، تنتحل دور أبراج الاتصالات مما يدفع الهواتف المحمولة إلى الاتصال بها عوضًا عن الأبراج المشروعة، و&nbsp;من ثمّ تمرُّ عبرها كُلُّ تدفّقات الاتصالات. و&nbsp;برغم مساعي مصمّمي بروتوكولات الاتصالات الهاتفية المحمولة الحديثة 4G و&nbsp;5G إلى سدّ الثغرات التي يعتمد عليها هذا النوع من التنصُّت إلا أنّه لا تزال توجد وسائل للتشويش على إشارات الأبراج المشروعة لدفع الهواتف إلى الظن بعدم وجود إمكانية للاتصال إلّا ببروتوكول 2G الأقدم الذي تستخدمه غالبية هذه الأجهزة. لذا فملاحظة اتّصال الهاتف بهذا البروتوكول في السياقات التي يفترض فيها وجود إمكانية الاتّصال بالبروتوكولات الأحدث هو من دلائل احتمال وجود المراقبة. المزيد عن هذا الموضوع في [FADe Project website](https://fadeproject.org/?page_id=36).

> و&nbsp;بوجه عام، فإنّنا ننصح الأشخاص المعرّضين إلى هذه السيناريوات من المراقبة قصر اتّصالاتهم على القنوات المُعمّاة من الطرف إلى الطرف عبر الإنترنت، عوضا عن المكالمات الصوتية و&nbsp;رسائل SMS النصية القصيرة، و&nbsp;كذلك باستخدام VPN أو تور للاتصالات بالإنترنت عمومًا. الجانب غير المحلول الباقي هو إمكانية تتبُّع موضع مستخدمي شبكات الهواتف المحمولة، و&nbsp;هو الذي لا يمكن التغلّب عليه طالما كان الاتّصال بالشبكة قائمًا.

> في بعض القضاءات يكون من حقِّ الناس من مُستخدمي خدمات التلفونات المحمولة أنْ يطلبوا من مُشغِّلي تلك الخدمات إمدادهم بمعلومات عن المعلومات التي سبق و&nbsp;أنْ جُمعت عنهم فيما مضى.

نواصل فيما يلي النظر في المخاطر المتوقّعة الأخرى أو الحالات الأعقد.

##### Options

- [واصل](#pre-closure)

### peer-compromised-intro

#### لقد تم استهداف الأشخاص من حولك بنجاح من خلال إجراءات المراقبة

> في حال تعرُّض شخصٍ من معارفك أو زملائك أو مُنظَّمةٍ لك علاقة بها إلى المراقبة فننصح بالسعي إلى فهم الأسلوب الذي اتُبِع لمراقبتهم و&nbsp;اختيار الحالة الأنسب من القائمة السابقة.

ماذا تريدين أن تفعلي؟

##### Options

- [أريد الرجوع إلى القائمة السابقة](#start)
- [لستُ متأكدة!](#pre-closure)

### other-intro

#### شيء آخر، لا أعرف

> القائمة السابقة تضمُّ حالات المراقبة التي تشيع في وسط المجتمع المدني، إلّا أنّها لا تشمل كُلَّ الحالات، و&nbsp;حالتك قد لا تكون مشمولة هنا. فإذا كان الأمر كذلك يمكنك طلب المساعدة من مختصّين، وإلّا فبوسعك الرجوع إلى قائمة الحالات الشائعة و&nbsp;اختيار الأقرب منها.

ماذا تريد أن تفعل؟

##### Options

- [أريد الرجوع إلى القائمة السابقة](#start)
- [أريد مساعدة](#help_end)

### pre-closure

#### أريد استكشاف الخطوات التالية

> عسى أنْ يكون هذا التحليل قد أفادك. بوسعك الرجوع إلى القائمة الافتتاحية لمخاطر المراقبة المحتملة المتناولة هنا، أو إلى قائمة المنظّمات التي يمكنك التواصل معها لطلب المساعدة في الحالات الأعقد.

ماذا تريدين أن تفعلي؟

##### Options

- [أريد معرفة المزيد عن تقنيات و&nbsp;ممارسات المراقبة الأخرى](#start)
- [أظنُّ أنّ حالتي ليست مشمولة هنا أو هي أعقد من هذا](#help_end)
- [أظنُّ أنني صرت مُلمًّا بما يجري](#resolved_end)

### help_end

#### أعتقد أن حاجتي لم تتم تغطيتها في هذا القسم أو أنها معقدة للغاية

> إذا كنت تحتاجين إلى مساعدة في تحليل حالة المراقبة التي تتعرضين إليها لفهمها و&nbsp;اختيار السبيل الأفضل للتصرُّف حيالها فيمكنك اللجوء إلى إحدى المنظَّمات المسرودة أسماؤها فيما يلي.

[orgs](:organisations?services=legal&services=vulnerabilities_malware&services=forensic)

### legal_end

#### أحتاج إلى الدعم القانوني

> إذا كنت تحتاج إلى مساعدة في اتّخاذ إجراء قانوني في حقِّ شخص أو جهة تتجسًّس عليك فيمكنك الاتّصال بالمنظَّمات التالية.

[orgs](:organisations?services=legal)

### resolved_end

#### لقد تم حل مشكلتي

عسى أنَّ هذا الدليل قد أفادك. و&nbsp;نحبُّ معرفة رأيك و&nbsp;مقترحاتك [بالبريد الإلكتروني](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

- يُستسحن استبدال قنوات الاتّصال غير الآمنة مثل الاتصالات الهاتفية و&nbsp;رسائل SMS بقنوات الاتّصال عبر الإنترنت المُعمّاة من الطرف إلى الطرف.
- إذا كنت تظنّين أن اتّصالك بالإنترنت مُراقَب فالجئي إلى استخدام VPN أو تور.
- احرص على معرفة كُلِّ النبائط الموجودة في محيطك، بخاصة المتّصلة منها بالإنترنت، و&nbsp;على معرفة وظيفة كُلٍّ منها.
- تجنَّبي مشاركة المعلومات الحسّاسة عبر خدمات يتحكَّم فيها خصومك.
- تجنّب التشارك في الحسابات و&nbsp;النبائط بقدر الإمكان.
- تحوَّطي من الرسائل المريبة و&nbsp;المواقع و&nbsp;التطبيقات و&nbsp;التنزيلات.

### Resources

- [Apple’s Android App to Scan for AirTags is a Necessary Step Forward, But More Anti-Stalking Mitigations Are Needed](https://www.eff.org/ar/deeplinks/2021/12/apples-android-app-scan-airtags-necessary-step-forward-more-anti-stalking).
- [الخصوصية للطلاب](https://ssd.eff.org/ar/module/privacy-students): مع مراجع لتقنيات المراقبة في المدارس التي يمكن تطبيقها على العديد من السيناريوهات الأخرى.
- [عُدَّة الأمان: حماية الاتّصالات عبر الإنترنت](https://securityinabox.org/ar/communication/private-communication/)
- [عُدَّة الأمان: النفاذ إلى المواقع المحجوبة و&nbsp;تصفُّح الوِب بمجهوليّة](https://securityinabox.org/ar/internet-connection/anonymity-and-circumvention/)
- [عُدَّة الأمان: تطبيقات آمن للتواصل النصي و&nbsp;الصوتي و&nbsp;المحادثات](https://securityinabox.org/ar/communication/tools/#more-secure-text-voice-and-video-chat-applications)
