---
layout: topic
title: "جهازي يتصرّف على نحو مريب"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, RaReNet
language: ar
summary: "إذا كان حاسوبك أو&nbsp;هاتفك أو&nbsp;نبيطتك الذكية تتصرّف على نحو مريب فقد تكون مصابة ببرمجية خبيثة."
date: 2023-06-30
permalink: topics/device-acting-suspiciously
parent: /ar/
order: 3
---

# جهازي يتصرَّف على نحو مريب

تطوَّرت الهجمات بالبرمجيّات الخبيثة و&nbsp;زاد تعقيدها على مرِّ السنين. تلك الهجمات تُشَكِّلُ تهديدات عديدة متنوِّعة و&nbsp;قد تكون لها تبعات خطيرة على البنية التحتيّة المعلوماتيّة لكم و&nbsp;لمؤسستكم و&nbsp;على سلامة بياناتكم.

هجمات البرمجيات لها أشكال عديدة، منها العدوى بالڤيروسات و&nbsp;التصيُّد و&nbsp;برمجيات الفدية و&nbsp;أحصنة طروادة و&nbsp;غيرها. و&nbsp;مِنْ أعراض الإصابة: انهيار الحواسيب، سرقة البيانات (مثل مسوِّغات الولوج إلى الحسابات، و&nbsp;المعلومات الماليّة، و&nbsp;غيرها)، الابتزاز بطلب دفع فدية نظير الإفراج عن بياناتك الرهينة، و&nbsp;التجسّس على كل تصرفاتك عبر الإنترنت، و&nbsp;استغلال الجهاز في شنِّ هجمات موزّعة للحرمان مِنْ الخدمة على آخرين.

بعض الأساليب التي يتّبعها المهاجمون لاختراق جهازك تبدو أنشطة معتادة، مثل:

- رسالة&nbsp;بريد أو&nbsp;منشور في الوسائط الاجتماعية يغريك بفتح ملف مرفق أو&nbsp;اتّباع رابط.

- دفع الناس إلى تنزيل و&nbsp;تشغيل برمجيّة مِنْ مصدر غير موثوق فيه

- دفع الشخص المستهدف إلى إدخال مسوِّغات الولوج (مثل اسم المستخدم و&nbsp;كلمة السرِّ) في موقع اصطُنع بحيث يحاكي موقعًا آخر.

- تنصيب برمجيّة تجسّس تجارية في جهازك إذا تركته غير مقفول.

هذا التحليل في عدَّة الإسعاف الأولي الرقمي سيدلُّك على خطوات أساسيّة لمعرفة ما إذا كانت نبيطتك مصابة بعدوى أم لا.

إنْ كنت تعتقد أنَّ حاسوبك أو&nbsp;هاتفك آخذ في التصرُّف على نحو مريب فيجب عليك الانتباه إلى الأعراض.

بعض الأعراض تدلُّ على سلوك غريب للجهاز إلا أنّها ليست مدعاة للقلق:

- أصوات طقطقة في الهاتف أثناء المحادثات
- الاستنزاف السريع للبطارية
- السخونة الزائدة عندما لا يكون الجهاز مستعملًا
- بطء الجهاز

هذه الأعراض عادة ما توصف بأنَّها دلائل على نشاط للجهاز مريب، إلا أنَّ أيًّا منها بمفرده لا يُعدُّ مدعاة للقلق.

مَنْ الأعراض الدالة على جهاز مخترق:

- الجهاز يُعيد تشغيل نفسه تلقائيًا على نحو متواتر
- انهيار التطبيقات بمعدل متزايد، بالذات عند إدخال بيانات
- تعطُّل تحديث نظام التشغيل أو&nbsp;رقعات الأمان
- ضوء اشتغال كاميرا الوب يضيء بغير تشغيل الكاميرا
- تواتر ظهور [شاشة الهلاك الزرقاء]( https://ar.wikipedia.org/wiki/%D8%B4%D8%A7%D8%B4%D8%A9_%D8%A7%D9%84%D9%85%D9%88%D8%AA_%D8%A7%D9%84%D8%B2%D8%B1%D9%82%D8%A7%D8%A1) أو&nbsp;أحداث الذعر في النواة (kernek panic)
- وميض نوافذ التطبيقات
- تحذيرات مضادات الڤيروسات

## Workflow

### start

#### هل تشعر أن جهازك معرض للخطر؟

بناء على المعلومات المعطاة في المقدِّمة إذا كنت لاتزالين تعتقدين أنَّ جهازك قد يكون مخترقًا فالدليل التالي قد يساعدك على تحديد المشكلة.

##### Options

- [أعتقد أنَّ جهازي المحمول يتصرَّف على نحو مريب](#phone-intro)
- [أعتقد أنَّ حاسوبي يتصرَّف على نحو مريب](#computer-intro)
- [لا أعتقد أنَّ حاسوبي مُخترَق](#device-clean)

### device-clean

#### لا، لم أعد أشعر أن جهازي قد يتعرض للخطر

> هذا عظيم! لكنْ لاحظ أنَّ هذه الإرشادات ستساعدك على إجراء تحليل سطحي. و&nbsp;مع أنَّ هذا ينبغي أنْ يكفي لاكتشاف الشذوذ الظاهر فإنَّ البرمجيّات الخبيثة المعقَّدة تستطيع التخَفّي بفعالية.

فإذا كنت لاتزال تظنّ أنّ جهازك مخترق فلربّما توجّب عليك:

##### Options

- [طلب مساعدة إضافية](#malware_end)
- [التقدّم إلى خطوات تصفير الجهاز](#reset)

### phone-intro

#### نعم ، أعتقد أن جهازي المحمول يتصرف بشكل مريب

> مِنْ المهمّ الأخذ في الاعتبار كيفيّة اختراق الجهاز
>
> - متى بدأت تلاحظين التصرفات الغريبة للجهاز؟
> - هل تذكر اتّباع روابط مِنْ مصادر غير معلومة؟
> - هل تلقَّيت رسائل مِنْ أشخاص لا تعرفينهم؟
> - هل نصَّبت برميجات نزَّلتها مِنْ مصدر غير موثوق فيه؟
> - هل كان الجهاز في حوزة غيرك؟

تفكّر في الأسئلة السابقة و&nbsp;حدّد الملابسات التي يمكن أنْ تكون قد أدَّت إلى اختراق جهازك.

##### Options

- [لدي جهاز يعمل على أندرُويد](#android-intro)
- [لدي جهاز يعمل على آي‌أو‌إس](#ios-intro)

### android-intro

#### لدي جهاز أندرويد

> انظر في وجود تطبيقات غير مألوفة مُنَصَّبَةٍ في النظام.
>
> يمكنك إيجاد حصر التطبيقات المُنَصَّبَةٍ في قائمة الإعدادات بعنوان "التطبيقات"
>
> إذا شككت في أيِّ تطبيق فابحث في الوِب باسمه عن تقارير ذات مصداقية تَذكُرُ التطبيق على أنَّه خبيث.

هل وجدت أيَّ تطبيق مريب؟

##### Options

- [لا، لم أجد](#android-unsafe-settings)
- [نعم، وجدت تطبيقات قد تكون خبيثة](#android-badend)

### android-unsafe-settings

#### لا، لم أجد أي تطبيقات مشبوهة

> يتيح أندرُويْد إمكانية تفعيل النفاذ خفيض المستوى إلى نبائطهم، و&nbsp;هو&nbsp;ما يفيد مطوّري البرمجيات، إلّا أنّه مِنْ ناحية أخرى يفتح المجال أمام هجمات إضافية. لذا تجب عليك مراجعة إعدادات الأمان تلك و&nbsp;التأكد مِنْ أنَّها مضبوطة على الخيارات الآمَن. أحيانًا يبيع المصنعون الأجهزة بإعدادات استبدائيّة غير آمِنة، لذا تجب مراجعتها حتّى إذا لَمْ تكن قد غيَّرتها بنفسك منذ حصلت على الجهاز.
>
> **تنصيب التطبيقات مِنْ مصادر غير موثوق فيها**
>
> في المعتاد يمنع نظام التشغيل أندرُويْد تنصيب التطبيقات مِنْ غير متجر گُوگِل أو المتجر المرتبط بصانع الهاتف. لدى گُوگِل صيرورة لمراجعة التطبيقات المعروضة في السوق لكشف الخبيث منها و&nbsp;إزالته. لذلك يسعى المهاجمون إلى تجاوز هذه العقبة بطريق إيصال تطبيقاتهم الخبيثة إلى الأشخاص المستهدفين بإرسال رابط أو&nbsp;ملف مباشرة إليهم. مِنْ الضروري التحقّق من أنَّ جهازك لا يسمح بتنصيب تطبيقات مِنْ مصادر غير موثوق فيها.

> تحقّقي من أنَّ تنصيب التطبيقات من المصادر غير الموثوق بها مُعطّل في جهازك. في العديد مِنْ إصدارات أندرُويْد توجد تحكّمات تنصيب البرمجيات ضمن خيارات الأمان، إلّا أنَّها قد تكون في موضع آخر، حسب تنويعة أندرُيد التي تشتغل على جهازك. و&nbsp;في الإصدارات الحديثة قد يوجد هذا التّحكم ضمن صلاحيات كُلِّ تطبيق.
>
> **طَوْر التطوير**
>
> بوضع أندرويد في "طَوْر التطوير" (Developer mode) يمكن لمطوِّري البرمجيات تشغيل أوامر على نظام التشغيل مباشرة، و&nbsp;هذا يعرِّض الجهاز للهجمات العتادية، بحيث أنَّ مَنْ يستطيع وضع يده على الجهاز ماديًّا يمكنه نسخ البيانات الخاصة المحفوظة فيه، كما يمكنه تنصيب برمجيات فيه.
>
> إذا كان "طور التطوير" ظاهرا في قائمة إعدادات جهازك فتحقّق من كونه معطّلا.
>
> **Google Play Protect**
>
> خدمة Google Play Protect متاحة في إصدارات أندرويد في الأجهزة الحديثة، و&nbsp;هي تفحص دوريًا التطبيقات المنصَّبة في الجهاز، و&nbsp;يمكن ضبطها لتزيل تلقائيًا أيّة برمجية خبيثة تكتشفها. مع ملاحظة أنَّ تفعيل هذه الخدمة يُرسل إلى گُوگِل معلومات عن الجهاز، مِنْ بينها التطبيقات المنصَّبة فيه.
>
> يمكن تفعيل خدمة Google Play Protect مِنْ إعدادات الأمان في الجهاز. [المزيد مِنْ المعلومات عن Google Play Protect](https://www.android.com/play-protect).

بفحص جهازك هل وجدت فيه تضبيطات غير آمنة؟

##### Options

- [لا، لم أجد](#android-bootloader)
- [نعم، وجدت](#android-badend)

### android-bootloader

#### لا، لم أتمكن من تحديد أي إعدادات غير آمنة

> مقلاع أندرويد (Android bootloader) برمجية هامة للغاية تبدأ عملها فور تشغيل الجهاز، و&nbsp;هو&nbsp;الذي يبتدئ تشغيل نظام التشغيل لإدارة العتاد. لذا فوجود مقلاع مُخترَق يمكِّن المهاجم مِنْ النفاذ الكامل إلى عتاد الجهاز. أغلب صانعي أجهزة الهاتف يبيعون الأجهزة بمقلاع مقفول، أيْ لا يُمكن تغييره و&nbsp;لا تعديل تضبيطاته. لمعرفة ما إذا كان المقلاع الأصلي المُوَقّعُ رقميًا مِنْ قِبَل الصانع قد تمّ تغييره، شغّل الجهاز و&nbsp;الحَظْ شعار الإقلاع، فإذا ظهرت إلى جواره شارة مثلث أصفر فيه علامة تعجّب فهذا يعني أنَّ المقلاع العامل حاليًّا قد تبدّل عن المقلاع الذي كان الصانع قد نصّبه في الجهاز. كذلك قد يكون جهازك مخترقًا إذا اكتشفت أنَّ المقلاع قد تبدَّل و&nbsp;لَم تكن أنت الذي أزال عنه القَفْل سابقًا، مثلا لتنصيب برمجية ما تتطلّب ذلك أو&nbsp;لتنصيب نظام تشغيل مختلف، مثل LineageOS أو GrapheneOS. في هذه الحالة ينبغي إجراء تصفير المصنع (factory reset).

هل مقلاع أندرويد في جهازك مخترق أم أنَّه يُشغِّل المقلاع الأصلي؟

##### Options

- [المقلاع مخترق](#android-badend)
- [جهازي يشتغل فيه المقلاع الأصلي](#android-goodend)

### android-goodend

####  الأصلي bootloader جهازي يستخدم

> الظاهر أنَّ جهازك ليس مخترقًا.

فهل لا تزالين قلقة مِنْ كونه مخترقًا؟

##### Options

- [نعم، أريد مساعدة مِنْ متخصِّصين](#malware_end)
- [لا، لقد انحلَّت المشكلة](#resolved_end)

### android-badend

#### نعم، أعتقد أن جهازي معرض للخطر

> قد يكون جهازك مخترقًا. إجراء تصفير المصنع للجهاز (factory reset) على الأرجح سيزيل مصادر الخطر الموجودة فيه. لكنَّ هذا ليس الحلَّ الأنسب على الدوام. علاوة على ذلك فقد تريد سَبْرَ المسألة بعمق أكبر لفهم درجة الاختراق و&nbsp;طبيعته.
>
> بوسعك تجربة أداة للفحص الذاتي اسمها [Emergency VPN](https://www.civilsphereproject.org/emergency-vpn)، أو [PiRogue](https://pts-project.org/) أو&nbsp;اطلب المساعدة مِنْ منظّمة تُقدِّم ذلك.

كيف تودين الاستمرار؟

##### Options

- [أريد تصفير جهازي إلى إعدادات المصنع](#reset)
- [أريد مساعدة مِنْ متخصِّصين](#malware_end)
- [في شبكتي الداعمة المحلية من يقدّم المساعدة التقنية و&nbsp;أستطيع اللجوء إليها](#resolved_end)

### ios-intro

#### IOSلدي جهاز  

> اتّبع الخطوات التالية [لمعرفة من لديهم نفاذ إلى جهازك الآيفون أو الآيپاد](https://support.apple.com/en-gb/guide/personal-safety/ipsb8deced49/web). راجع نظام آي‌أو‌إس لملاحظة إن طان يوجد شيء غير مألوف.
>
> في تطبيق الإعدادات تأكَّد مِنْ أنَّ جهازك مربوط بمعرِّف Apple ID الخاص بك. العنصر الأوَّل في القائمة على الجانب الأيسر ينبغي أنْ يكون اسمك أو&nbsp;الاسم الذي تستعمله في Apple ID. انقر على الاسم و&nbsp;تحقّق مِنْ أنَّ عنوان البريد الإلكتروني صحيح. في ذيل الصفحة يوجد حصر لأسماء و&nbsp;طُرُز أجهزة آي‌أو‌إس المربوطة بمعرّف Apple ID هذا.
>
> تحقّق من كون جهازك غير مربوط بنظام لإدارة النبائط المحمولة (Mobile Device Management System) غير مألوف. تحت الإعدادات > العامة > إدارة الجهاز و&nbsp;الحظي إن كان يوجد قسم بعنوان الإضبارات. يمكنك [حذف أي إضبارات تضبيطات](https://support.apple.com/ar/guide/personal-safety/ips41ef0e8c3/1.0/web/1.0#ips68379dd2e). إذا لم توجد إضبارات مسرودة في هذا الموضع فهذا يعني عدم وجود نظام MDM.

##### Options

- [كلّ المعلومات صحيحة و&nbsp;أنا المتحكّم في حساب Apple ID](#ios-goodend)
- [الاسم أو&nbsp;بيانات أخرى غير صحيحة أو&nbsp;تظهر في القائمة أجهزة أو إضبارات لا تخصّني](#ios-badend)

### ios-goodend

#### جميع المعلومات صحيحة وما زلت مسيطرا على معرف Apple الخاص بي

> الظاهر أنَّ جهازك ليس مخترقًا.

فهل لا تزالين قلقة مِنْ كونه مخترقًا؟

##### Options

- [نعم، أريد مساعدة مِنْ متخصِّصين](#malware_end)
- [لا، لقد انحلّت المشكلة](#resolved_end)

### ios-badend

#### الاسم أو تفاصيل الآخرين غير صحيحة أو أرى أجهزة في القائمة ليست تابعة لي

> قد يكون جهازك مخترقًا. إجراء تصفير المصنع للجهاز (factory reset) على الأرجح سيزيل مصادر الخطر الموجودة فيه. لكنّ هذا ليس الحلّ الأنسب على الدوام. علاوة على ذلك فقد تريد سَبْرَ المسألة بعمق أكبر لفهم درجة الاختراق و&nbsp;طبيعته.
>
> بوسعك تجربة أداة للفحص الذاتي اسمها [Emergency VPN](https://www.civilsphereproject.org/emergency-vpn)، أو [PiRogue](https://pts-project.org/) أو&nbsp;اطلبي المساعدة مِنْ منظّمةٍ تُقدِّم ذلك.

كيف تودين الاستمرار؟

##### Options

- [أريد تصفير جهازي إلى إعدادات المصنع](#reset)
- [أريد مساعدة مِنْ متخصِّصين](#malware_end)
- [في شبكتي الداعمة المحلية من يقدّم المساعدة التقنية و&nbsp;أستطيع اللجوء إليها](#resolved_end)

### computer-intro

#### أعتقد أن جهاز الكمبيوتر الخاص بي يتصرف بشكل مثير للريبة

> **ملاحظة: إذا وقعت ضحيّة هجوم فدية فاتجّه فورا إلى موقع [nomoreransom.org](https://www.nomoreransom.org)**
>
> هذا المسار سيعينك على تقصِّي النشاط المريب في حاسوبك. إذا كنت تساعد شخصًا ما عن بُعْدٍ فجرِّب اتِّباع الخطوات الموصوفة في الروابط التالية عبر تطبيق للتحكم عن بعد مثل [TeamViewer](https://www.teamviewer.com)، أو&nbsp;انظر في استخدام إطار للتشخيص عن بُعد مثل [Google Rapid Response (GRR)](https://github.com/google/grr). و&nbsp;تذكَّر أنَّ سرعة الشبكة و&nbsp;اعتماديَّتها عاملان حاكمان.

ما نظام التشغيل الذي تريدين تشخيصه؟

##### Options

- [حاسوب يشغّل وِندوز](#windows-intro)
- [حاسوب يشغّل ماك](#mac-intro)

### windows-intro

#### Windows

> اتّبع الدليل المبسّط التالي لتقصّي النشاط المريب في الأجهزة المشتغلة بوِندوز:
>
> - [How to Live Forensic on Windows by Tek](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md)

هل ساعدتك تلك الإرشادات في تشخيص المشكلة؟

##### Options

- [نعم، أظنّ الحاسوب مصابًا](#device-infected)
- [لا، لم أتعرّف على أيِّ نشاط مريب](#device-clean)

### mac-intro

#### MacOS

> للتعرّف على إصابة محتملة في حاسوب ماك اتَّبعي الخطوات التالية:
>
> 1. افحص البرمجيات المريبة التي تشتغل تلقائيًا بعد تشغيل النظام
> 2. افحصي السيرورات العاملة المريبة
> 3. افحص ملحقات النُّواة المريبة
>
> موقع [Objective-See](https://objective-see.com) يقدّم أدوات مجانيّة عدَّة تسهّل هذه الفحوص:
>
> - [KnockKnock](https://objective-see.com/products/knockknock.html) لإظهار التطبيقات المسجَّلة للاشتغال تلقائيًّا.
> - [TaskExplorer](https://objective-see.com/products/taskexplorer.html) لفحص السيرورات العاملة و&nbsp;التعرُّف على المريب منها، مثلا غير الموقَّعة، أو&nbsp;الموصومة في VirusTotal.
> - [KextViewr](https://objective-see.com/products/kextviewr.html) للتعرُّف على ملحقات النُّواة المريبة المُنصَّبة في النظام.
>
> إذا لَمْ تكشف هذه الأدوات شيئًا مريبًا و&nbsp;أردت التعمُّق في الفحص فجرِّبي [pcQF](https://github.com/botherder/pcqf))، و&nbsp;هي أداة تُيَسِّرُ جمع معلومات عن النِّظام و&nbsp;أخذ لقطة مِنْ الذاكرة.
>
> توجد أداة أخرى لجمع معلومات إضافيّة (إلَّا أنَّها تتطلَّب قدرة على التعامل مع الطرفيّة) و&nbsp;هي [AutoMacTC](https://www.crowdstrike.com/blog/automating-mac-forensic-triage).

هل أفادتك تلك الأدوات في التعرُّف على نشاط مريب؟

##### Options

- [نعم، أظنّ الحاسوب مصابًا](#device-infected)
- [لا، لَمْ أتعرّف على أيِّ نشاط مريب](#device-clean)

### device-infected

#### نعم، أعتقد أن الكمبيوتر مصاب

يبدو أن جهازك مصابًا بالعدوى. للتخلُّص مِنْ العدوى يمكنك:

##### Options

- [طلب مساعدة](#malware_end)
- [تصفير النظام](#reset)

### reset

#### إعادة ضبط الجهاز

> خذ في اعتبارك تصفير الجهاز كإجراء احترازي. الأدلَّة التالية تحوي الإرشادات اللازمة لكلِّ نظام تشغيل:
>
> - [أندرُويْد](https://support.google.com/android/answer/6088915?hl=ar)
> - [آي‌أو‌إس](https://support.apple.com/ar-eg/HT201252)
> - [وِندوز](https://support.microsoft.com/en-us/windows/reinstall-windows-d8369486-3e33-7d9c-dccc-859e2b022fc7)
> - [ماك](https://support.apple.com/ar-eg/HT201314)

هل تحتاجين مساعدة بعد؟

##### Options

- [نعم](#malware_end)
- [لا](#resolved_end)

### malware_end

####  نعم، أشعر أنني بحاجة إلى مساعدة إضافية

إذا أردت مساعدة إضافية مع جهاز مصاب فاتَّصل بالمنظَّمات التالية:

[orgs](:organisations?services=vulnerabilities-malware)

### resolved_end

#### لا، أشعر أنني لا أحتاج إلى مساعدة إضافية

عسى أنَّ دليل عدَّة الإسعاف اﻷولي الرقمي هذا قد أفادك، و&nbsp;نحبُّ معرفة رأيك [بالبريد الإلكتروني](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

النصائح التالية تفيد في حمايتك مِنْ الوقوع ضحيّة هجوم غرضه اختراق جهازك و&nbsp;بياناتك:

- تحقَّقي دومًا مِنْ رسائل البريد التي تتلقينها، و&nbsp;مِنْ الملفات التي تنزلينها و&nbsp;مسارات الصفحات التي تطلب منك إدخال مسوِّغات الولوج.
- اقرأ المزيد عن كيفية حماية جهازك مِنْ البرمجيات الخبيثة في الأدلَّة المذكورة في المصادر

### Resources

- [عُدّة الأمان: الحماية من البرمجيات الخبيثة](https://securityinabox.org/ar/guide/malware/)
  - و&nbsp;إرشادات لحماية نبائط [أندرويد](https://securityinabox.org/ar/phones-and-computers/android) و&nbsp;[آي⁨أوإس](https://securityinabox.org/ar/phones-and-computers/ios) و&nbsp;[وِندوز](https://securityinabox.org/ar/phones-and-computers/windows) و&nbsp;[ماك⁨أوإس](https://securityinabox.org/ar/phones-and-computers/mac) و&nbsp;[لينُكس](https://securityinabox.org/ar/phones-and-computers/linux)
- [Security Self-Defense: دليل: كيفية تجنب هجمات التصيد](https://ssd.eff.org/ar/module/%D8%AF%D9%84%D9%8A%D9%84-%D9%83%D9%8A%D9%81%D9%8A%D8%A9-%D8%AA%D8%AC%D9%86%D8%A8-%D9%87%D8%AC%D9%85%D8%A7%D8%AA-%D8%A7%D9%84%D8%AA%D8%B5%D9%8A%D8%AF-phishing)​​​​​​​
  ​​​​​- [Security Security without borders: Guide to Phishing](https://guides.securitywithoutborders.org/guide-to-phishing/)
