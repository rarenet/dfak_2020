---
layout: topic
title: "لا أستطيع الولوج إلى حسابي"
author: RaReNet
language: ar
summary: "هل تواجهين صعوبة في الولوج إلى حساب بريدك أو&nbsp;حسابك على منصّة تواصل اجتماعي أو&nbsp;موقع ما على الوِب؟ هل يبدي الحساب نشاطًا لم تقم أنت به؟ توجد حلول لتدارك هذه المشكلة."
date: 2023-06-29
permalink: topics/account-access-issues
parent: /ar/
order: 10
---

# لا أستطيع الولوج إلى حسابي

يستعمل أفراد المجتمع المدني حسابات التواصل الاجتماعي بكثافة لنشر المعلومات و&nbsp;لمناصرة قضاياهم، و&nbsp;هذا يؤدي إلى استهداف حساباتهم مِنْ قِبَل الفاعلين المُعادين الذين يسعون إلى اختراق تلك الحسابات، ممّا يضرُّ أفراد المجتمع المدني و&nbsp;الذين يتواصلون معهم.

الغرض مِنْ هذا الدليل مساعدتك في حال ما تعذََّر عليك النفاذ إلى أحد حساباتك بسبب اختراقه.

المسار التالي الغرض مِنْه تحديد طبيعة المشكلة و&nbsp;إيجاد الحلول المحتملة.

## Workflow

### Password-Typo

#### هل تحتوي كلمة السر الخاصة بك على خطأ مطبعي؟

> أحيانا يتعذَّر عليك الولوج إلى حساباتك لأنَّك تُدخِلين كلمة سرٍّ خاطئة دون أنْ تعي، أو&nbsp;لأنّ لغة لوحة المفاتيح مضبوطة على غير ما تستعملينه عادة أو&nbsp;لأنّ زرّ الأحرف اللاتينيّة الكبيرة CAPS مضغوط.
>
> جرِّبي كتابة اسم المستخدم و&nbsp;كلمة السرِّ في مُحرِّر نصوص صافية (plaintext) ثم انسخيها ثم ألصقيها في استمارة الولوج. و&nbsp;تحقّق أيضًا مِنْ إعدادات لغة لوحة المفاتيح، و&nbsp;إذا ما كان زرُّ CAPS مضغوطا.

هل حلَّ أيُّ من المقترحات السالفة المشكلة و&nbsp;تمكَّنت مِنْ الولوج إلى حسابك؟

##### Options

- [نعم](#resolved_end)
- [لا](#account-disabled)

### account-disabled

#### الحساب معطل

> أحيانًا يتعذَّر الولوج إلى الحساب لكونه مقفولا أو مُعطّلا من طرف مُشغِّل الخدمة بسبب مخالفة شروط تقديم الخدمة أو قواعد المنصّة. يمكن أن يحدث هذا عندما ترد بلاغات كثيرة عن إساءة استخدام الحساب أو عندما يجري التلاعب بآلية الدعم و المراقبة في المنصّة بغرض حجب المحتوى أو كتم الآراء.
>
> إذا وصلك إخطار يفيد تجميد حسابك أو تعطيله أو توقيفه، و في اعتقادك أنها ذلك غير مُبَرَّر، فاتّبعوا آليات التظلّم المذكورة في الإخطار. يمكنكم فيما يلي إيجاد معلومات عن كيفية تقديم التظلّمات:
>
> - [فيسبوك](https://www.facebook.com/help/185747581553788)
> - [إنستَگرام](https://help.instagram.com/366993040048856)
> - [تويتر](https://help.twitter.com/en/forms/account-access/appeals/redirect)
> - [يوتيوب](https://support.google.com/youtube/answer/2802168)

هل حلَّ أيُّ من المقترحات السالفة المشكلة و&nbsp;تمكَّنت مِنْ الولوج إلى حسابك؟

##### Options

- [نعم](#resolved_end)
- [لا](#what-Type-of-Account-or-Service)

### what-Type-of-Account-or-Service

#### أيُّ حساب أو&nbsp;خدمة يتعذَّر الولوج إليها؟

##### Options

- [فيسبوك](#Facebook)
- [صفحة فيسبوك](#Facebook-Page)
- [تويتر](#Twitter)
- [گُوگِل\جيميل](#Google)
- [ياهو](#Yahoo)
- [هُتميل\أوْتلُك\لايڤ](#Hotmail)
- [پروتونميل](#ProtonMail)
- [إنستَگرام](#Instagram)
<!--- - [AddOtherServiceLink](#service-Name) -->

### Facebook-Page

#### صفحة فيسبوك

هل للصفحة مديرون آخرون؟

##### Options

- [نعم](#Other-admins-exist)
- [لا](#Facebook-Page-recovery-form)

### Other-admins-exist

#### يوجد إداريين آخرين

هل يواجه المديرون الآخرون نفس المشكلة؟

##### Options

- [نعم](#Facebook-Page-recovery-form)
- [لا](#Other-admin-can-help)

### Other-admin-can-help

#### يمكن للمسؤولين الآخرين المساعدة

> اطلب مِنْ مدير آخر إعطاءك صلاحيات الإدارة مجدَّدًا.

هل يحلُّ هذا المشكلة؟

##### Options

- [نعم](#Fb-Page_end)
- [لا](#account_end)

### Facebook-Page-recovery-form

#### المسؤولين الآخرين لديهم نفس المشكلة

> عاود الولوج إلى فيسبوك و&nbsp;استعملي [استمارة الإبلاغ عن مشاكل الصفحات](https://www.facebook.com/help/contact/164405897002583). إذا كان الولوج إلى حسابك في فيسبوك متعذَّرًا، فاتّبعي [إجراء استعداة حسابات فيسبوك](#Facebook)
>
> اعلمي أنَّ الاستجابة للبلاغات عبر إجراءات الوِب قد تستغرق عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاستعادة؟

##### Options

- [نعم](#resolved_end)
- [لا](#account_end)

<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

#### گُوگِل\جيميل

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

##### Options

- [نعم](#I-have-access-to-recovery-email-google)
- [لا](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

#### Google لدي حق الوصول إلى البريد الإلكتروني المخصص لاسترداد

> انظر في صندوق الوارد في بريدك الإلكتروني المُربوط بحسابك بحثًا عن إخطارات بشأن أمان حسابك لدى گوگل، و كذلك في رسائل SMS الواردة.
>
> عند مطالعة رسائل الإخطارات من الخدمات المختلفة تحوّطي من محاولات التصيُّد. إذا شككت في صحّة رسالة فطالعي التحليل [وصلتني رسالة مريبة](../../../suspicious-messages/).

هل وصلتك رسالة فيها إخطار بشأن أمان حسابك لدى گوگل؟

##### Options

- [نعم](#Email-received-google)
- [لا](#Recovery-Form-google)

### Email-received-google

#### لقد تلقيت رسالة استرداد عبر البريد الإلكتروني

بعد التحقق من صحّة الرسالة، انظر إن كان في الرسالة رابط لاسترداد حسابك؟

##### Options

- [نعم](#Recovery-Link-Found-google)
- [لا](#Recovery-Form-google)

### Recovery-Link-Found-google

#### لدي رابط الاسترداد

> اتبع رابط استرداد حسابك. عند اتّباع الرايط تحقّقي من كون مسار الصفحة التي وصلت إليها في المتصفح يحتوي اسم النطاق google.com.

هل تمكّنت مِنْ استرداد الحساب؟

##### Options

- [نعم](#resolved_end)
- [لا](#Recovery-Form-google)

### Recovery-Form-google

#### Google نموذج الاسترداد من]

> جرب اتّباع الإجراء في استمارة "[كيفية استرداد حسابك على Google أو Gmail](https://support.google.com/accounts/answer/7682439?hl=ar)"
>
> اعلمي أنَّ الاستجابة للبلاغات عبر إجراءات الوِب قد تستغرق عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمُعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

##### Options

- [نعم](#resolved_end)
- [لا](#account_end)

<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

#### Yahoo

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

##### Options

- [نعم](#I-have-access-to-recovery-email-yahoo)
- [لا](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

#### لدي إمكانية الوصول إلى البريد الإلكتروني المخصص للاسترداد

> انظر في صندوق الوارد في بريدك الإلكتروني المُربوط بحسابك بحثًا عن إخطارات بشأن تغيير كلمة سرّ حسابك لدى ياهو.
>
> عند مطالعة رسائل الإخطارات من الخدمات المختلفة تحوّطي من محاولات التصيُّد. إذا شككت في صحّة رسالة فطالعي التحليل [وصلتني رسالة مريبة](../../../suspicious-messages/).

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حسابك لدى ياهو؟

##### Options

- [نعم](#Email-received-yahoo)
- [لا](#Recovery-Form-Yahoo)

### Email-received-yahoo

#### لقد تلقيت رسالة استرداد عبر البريد الإلكتروني

بعد التحقق من صحّة الرسالة، انظر إن كان في الرسالة رابط لاسترداد حسابك؟

##### Options

- [نعم](#Recovery-Link-Found-Yahoo)
- [لا](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

#### لدي رابط الاسترداد

> اتبع رابط استرداد حسابك.

هل تمكَّنت مِنْ استرداد الحساب؟

##### Options

- [نعم](#resolved_end)
- [لا](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

#### Yahoo نموذج الاسترداد

> جرّبي اتّباع الإجراء في [استمارة استرداد الحسابات](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html).
>
> اعلمي أنَّ الاستجابة للبلاغات عبر إجراءات الوِب قد تستغرق عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

##### Options

- [نعم](#resolved_end)
- [لا](#account_end)

<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

#### Twitter

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

##### Options

- [نعم](#I-have-access-to-recovery-email-Twitter)
- [لا](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

#### لدي إمكانية الوصول إلى البريد الإلكتروني المخصص للاسترداد

> انظر في صندوق الوارد في بريدك الإلكتروني المُربوط بحسابك بحثًا عن إخطارات بشأن تغيير كلمة سرّ حسابك لدى تويتر.
>
> عند مطالعة رسائل الإخطارات من الخدمات المختلفة تحوّطي من محاولات التصيُّد. إذا شككت في صحّة رسالة فطالعي التحليل [وصلتني رسالة مريبة](../../../suspicious-messages/).

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حسابك لدى تويتر؟

##### Options

- [نعم](#Email-received-Twitter)
- [لا](#Recovery-Form-Twitter)

### Email-received-Twitter

#### لقد تلقيت رسالة استرداد عبر البريد الإلكتروني

بعد التحقق من صحّة الرسالة، انظر إن كان في الرسالة رابط لاسترداد حسابك؟

##### Options

- [نعم](#Recovery-Link-Found-Twitter)
- [لا](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

#### لدي رابط الاسترداد

> اتبع رابط استرداد حسابك.

هل تمكَّنت مِنْ استرداد الحساب؟

##### Options

- [نعم](#resolved_end)
- [لا](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

#### Twitter نموذج الاسترداد

> إذا كنت تعتقدين أنّ حسابك في تويتر مخترق، فجرِّب اتّباع الإجراء في [استمارة استرداد الحسابات](https://help.twitter.com/ar/safety-and-security/twitter-account-compromised)
>
> أما إذا لم يكن حسابك مخترقًا أو إذا تعذّر عليك الولوج إلى حسابك فجرِّب اتّباع الإجراء في [استمارة استرداد الحسابات](https://help.twitter.com/forms/restore)
>
> اعلمي أنَّ الاستجابة للبلاغات عبر إجراءات الوِب قد تستغرق عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

##### Options

- [نعم](#resolved_end)
- [لا](#account_end)

<!---=========================================================
//Protonmail
//========================================================= -->

### ProtonMail

#### Protonmail

> جرّب اتّباع إجراء [تصفير كلمة السرّ](https://protonmail.com/support/knowledge-base/reset-password)
> علما بأنّك إذا صفّرت كلمة السرّ فلن يمكنك قراءة رسائل البريد الواردة قبل هذا الفعل و لا مدخلات دفتر العناوين التي سبق حفظها لأنها كلّها مُعمّاة بمفتاح محمي بكلمة السرّ. يُمكن استرجاع البيانات السابقة إذا كنت قد حفظت ملف الاسترجاع أو عبارة الاسترجاع، و ذلك [بطريق الإجراء الموصوف](https://proton.me/support/recover-encrypted-messages-files).

هل أفلح إجراء الاسترجاع؟

##### Options

- [نعم](#resolved_end)
- [لا](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

#### Hotmail

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

##### Options

- [نعم](#I-have-access-to-recovery-email-Hotmail)
- [لا](#Recovery-Form-Hotmail)

##### Options

### I-have-access-to-recovery-email-Hotmail

#### لدي إمكانية الوصول إلى البريد الإلكتروني المخصص للاسترداد

> انظر في صندوق الوارد في حسابك بريدك الإلكتروني المُربوط بحسابك بحثًا عن إخطارات بشأن تغيير كلمة سرّك لدى ميكروسوفت.
>
> عند مطالعة رسائل الإخطارات من الخدمات المختلفة تحوّطي من محاولات التصيُّد. إذا شككت في صحّة رسالة فطالعي التحليل [وصلتني رسالة مريبة](../../../suspicious-messages/).

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حسابك لدى ميكروسوفت؟

##### Options

- [نعم](#Email-received-Hotmail)
- [لا](#Recovery-Form-Hotmail)

### Email-received-Hotmail

#### لقد تلقيت رسالة استرداد عبر البريد الإلكتروني

بعد التحقق من صحّة الرسالة، انظر إن كان في الرسالة رابط لاسترداد حسابك؟

##### Options

- [نعم](#Recovery-Link-Found-Hotmail)
- [لا](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

#### لدي رابط الاسترداد

> اتبع رابط تصفير كلمة السرّ، لوضع كلمة سرّ جديدة لحسابك.

هل تمكَّنت مِنْ استرداد الحساب؟

##### Options

- [نعم](#resolved_end)
- [لا](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

#### Hotmail نموذج الاسترداد

> جرِّبي أداة "[Sign-in Helper Tool](https://go.microsoft.com/fwlink/?linkid=2214157)"، متّبعة الإجراءات الموصوفة فيها، بما في ذلك تعيين الحساب الذي تسعين إلى استرجاعه و إجابة الأسئلة.
>
> اعلمي أنَّ الاستجابة للبلاغات عبر إجراءات الوِب قد تستغرق عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

##### Options

- [نعم](#resolved_end)
- [لا](#account_end)

### Facebook

#### فيسبوك

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيَّنين لاسترداد هذا الحساب؟

##### Options

- [نعم](#I-have-access-to-recovery-email-Facebook)
- [لا](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

#### لدي إمكانية الوصول إلى البريد الإلكتروني المخصص للاسترداد

> انظر في صندوق الوارد في حسابك بريدك الإلكتروني المُربوط بحسابك بحثًا عن إخطارات بشأن تغيير كلمة السرّ في فيسبوك.
>
> عند مطالعة رسائل الإخطارات من الخدمات المختلفة تحوّطي من محاولات التصيُّد. إذا شككت في صحّة رسالة فطالعي التحليل [وصلتني رسالة مريبة](../../../suspicious-messages/).

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حسابك لدى فيسبوك؟

##### Options

- [نعم](#Email-received-Facebook)
- [لا](#Recovery-Form-Facebook)

### Email-received-Facebook

#### لقد تلقيت رسالة استرداد عبر البريد الإلكتروني

بعد التحقق من صحّة الرسالة، انظر إن كان في الرسالة تنويه مفاده "لم يكن ذلك بطلب مني" مع رابط؟

##### Options

- [نعم](#Recovery-Link-Found-Facebook)
- [لا](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

#### لدي رابط الاسترداد

> اتبع رابط "لم يكن ذلك بطلب مني" لاسترجاع الحساب.

هل تمكَّنت مِنْ استرداد الحساب؟

##### Options

- [نعم](#resolved_end)
- [لا](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

#### نموذج استرداد الفيسبوك

> جرِّبي الإجراء في استمارة "[البحث عن حسابك](https://www.facebook.com/login/identify)"
>
> اعلمي أنَّ الاستجابة للبلاغات عبر إجراءات الوِب قد تستغرق عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

##### Options

- [نعم](#resolved_end)
- [لا](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

#### انستغرام

هل تستطيع النفاذ إلى حساب البريد أو&nbsp;رقم الهاتف المعيّنين للاسترداد؟

##### Options

- [نعم](#I-have-access-to-recovery-email-Instagram)
- [لا](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

#### لدي إمكانية الوصول إلى البريد الإلكتروني المخصص للاسترداد

> انظر في صندوق الوارد في حسابك بريدك الإلكتروني المُربوط بحسابك بحثًا عن إخطارات بشأن تغيير كلمة سرّ حسابك لدى إنستگرام؟
>
> عند مطالعة رسائل الإخطارات من الخدمات المختلفة تحوّطي من محاولات التصيُّد. إذا شككت في صحّة رسالة فطالعي التحليل [وصلتني رسالة مريبة](../../../suspicious-messages/).

هل تلقّيت مؤخرًا إخطارًا بتغيير كلمة سرِّ حسابك لدى إنستگرام؟

##### Options

- [نعم](#Email-received-Instagram)
- [لا](#Recovery-Form-Instagram)

### Email-received-Instagram

#### لقد تلقيت رسالة استرداد عبر البريد الإلكتروني

بعد التحقق من صحّة الرسالة، انظر إن كان في الرسالة تنويه بشأن تأمين حسابك؟

##### Options

- [نعم](#Recovery-Link-Found-Instagram)
- [لا](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

#### لقد وجدت رابط الاسترداد

> اتبع رابط استرداد حسابك.

هل تمكَّنت مِنْ استرداد الحساب؟

##### Options

- [نعم](#resolved_end)
- [لا](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

#### نموذج استرداد إنستغرام

> جرِّبي اتّباع الإجراء في "[أعتقد أن حسابي على Instagram قد تعرّض للاختراق](https://help.instagram.com/149494825257596)"
>
> اعلم أنَّ الاستجابة للبلاغات عبر الوِب قد تستغرق عدَّة أيام. احفظ هذه الصفحة في مفضلاتك للرجوع إليها لاحقًا لمعاودة مسار حلِّ المشكلة.

هل أفلح إجراء الاسترداد؟

##### Options

- [نعم](#resolved_end)
- [لا](#account_end)

### Fb-Page_end

#### نعم، لقد تم حل مشكلتي

عظيم أنّ مشكلتك انحلّت. طالع التوصيات التالية لتقليل احتمال فقدناك النفاذ إلى حسابك في المستقبل.

- فعّل الاستيثاق بمعاملين (2FA) لكل مديري الصفحة
- لا تسند صلاحيات الإدارة لغير الأشخاص الموثوق فيهم و&nbsp;يردون عند الحاجة
- احرص على وجود أكثر من مدير للصفحة، من الأشخاص الموثوق فيهم، على أن يفعّلوا جميعا الاستيثاق بمعاملين في حساباتهم
- راجعي دوريًّا الصلاحيات و الأدوار المسندة إلى مديري و ميسّري الصفحة، و يُستحسن إسناد أقل الصلاحيات اللازمة ﻷاداء المهام المطلوبة.

### account_end

#### لم يتم حل مشكلتي

إذا لم يفلح الإجراء الموصوف هنا في استرداد حسابك فيمكنك التواصل مع المنظَّمات التالية لطلب العون:

[orgs](:organisations?services=account)

### resolved_end

#### نعم، لقد تم حل مشكلتي

عسى أن تكون عدَّة الإسعاف اﻷولي الرقمي قد أفادتك، و&nbsp;نحبُّ معرفة رأيك و&nbsp;مقترحاتك [بالبريد الإلكتروني](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

طالعي التوصيات التالية لتقليل احتمال فقدانك النفاذ إلى حسابك في المستقبل.

- مِنْ المفيد دوما تفعيل الاستيثاق بمعاملين (2FA) في كلّ الحسابات التي تدعم هذه الوظيفة.
- لا تضع أبدا كلمة السّر نفسها لأكثر مِنْ حساب. و&nbsp;إذا كنت قد فعلت ذلك بالفعل فغيّريها في أقرب فرصة واضعة كلمة سرّ فريدة في كلّ حساب. حتى ما إذا تسرّبت معلومات مِنْ إحدى الخدمات لَمْ يستطع أحد استعمالها للنفاذ إلى حساباتك الأخرى.
- استعمال مدير لكلمات السرّ سيعينك على وضع كلمات سرّ فريدة قوية لكلّ حساباتك دون الاعتماد على ذاكرتك
- احتَط عند الاتصال عبر شبكات وايفاي عمومية، و&nbsp;يستحسن استعمال تور أو VPN في هذه الحالة.

### Resources

- [وضع كلمات سر قوية | الدفاع عن النفس ضد الرقابة](https://ssd.eff.org/ar/module/%D9%88%D8%B6%D8%B9-%D9%83%D9%84%D9%85%D8%A7%D8%AA-%D8%B3%D8%B1-%D9%82%D9%88%D9%8A%D8%A9)
- [حماية نفسك على مواقع التواصل الاجتماعي | الدفاع عن النفس ضد الرقابة](https://ssd.eff.org/ar/module/%D8%AD%D9%85%D8%A7%D9%8A%D8%A9-%D9%86%D9%81%D8%B3%D9%83-%D8%B9%D9%84%D9%89-%D9%85%D9%88%D8%A7%D9%82%D8%B9-%D8%A7%D9%84%D8%AA%D9%88%D8%A7%D8%B5%D9%84-%D8%A7%D9%84%D8%A7%D8%AC%D8%AA%D9%85%D8%A7%D8%B9%D9%8A)
- [عدّة الأمان: إنشاء كلمات مرور قويّة وحفظها](https://securityinabox.org/ar/guide/passwords/)
- [Access Now Helpline Community Documentation: Recommendations on Team Password Managers](https://communitydocs.accessnow.org/295-Password_managers.html)

<!--- Edit the following to add another service recovery workflow:
#### service-name

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-google)
- [No](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Check if you received a "[Password Change Email Subject]" email from service_name. Did you receive it?

- [Yes](#Email-received-service-name)
- [No](#Recovery-Form-service-name

### Email-received-service-name

> Please check if there is a "recover your account" link. Is it there?

- [Yes](#Recovery-Link-Found-service-name)
- [No](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> Please use the [Recovery Link Description](URL) link to recover your account.

Were you able to recover your account with "[Recovery Link Description]" link?

- [Yes](#resolved_end)
- [No](#Recovery-Form-service-name)

### Recovery-Form-service-name

> Please try this recovery form to recover this account: [Link to the standard recovery form].
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

-->
