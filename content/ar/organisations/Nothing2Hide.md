---
language: ar
layout: organisation
name: Nothing2Hide
website: https://nothing2hide.org
logo: nothing2hide.png
languages: Français, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: من الاثنين إلى الجمعة، من 09:00 إلى 18:00، بتوقيت وسط أوربا
response_time: يوم
contact_methods: email, pgp, signal, web_form, wire
web_form: https://vault.tech4press.org/#/ (online secured form - based on Globaleaks)
email: help@tech4press.org
pgp_key:  http://tech4press.org/fr/contact-mail/
pgp_key_fingerprint: 2DEB 9957 8F91 AE85 9915 DE94 1B9E 0F13 4D46 224B
signal: +33 7 81 37 80 08 <http://tech4press.org/fr/#signal>
wire: tech4press
---

Nothing2Hide (N2H) رابطة تهدف إلى إمداد الصحافيين و&nbsp;المحامين و&nbsp;المدافعين عن حقوق الإنسان و&nbsp;المواطنين "ال
عاديين" بوسائل حماية بياناتهم و&nbsp;اتّصالاتهم بتقديم الحلول تقنيّة و&nbsp;التدريبات المطوّعة لسياقاتهم المختلفة. رؤية الرابطة وضع االتقنية في خدمة نشر البيانات و&nbsp;الحماية بغرض تقوية الديمقراطية حول العالم.