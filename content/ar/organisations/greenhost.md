---
language: ar
layout: organisation
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: 24/7, UTC+2
response_time: 4 ساعات
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: ‭37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066‬
phone: ‭+31 20 489 04 44‬
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

گريْنهُسْت تقدّم خدماتِ تقنية المعلوماتيَّة بمدخل أخلاقي و&nbsp;مستدام و&nbsp;خدماتها تتضمَّن استضافة مواقع الوِب، و&nbsp;الخدمات السحابيَّة، و&nbsp;عروضًا أخرى في مجال أمن المعلومات. و&nbsp;المنظَّمة ضالعة في مشروعات تطوير برمجيات حُرَّة مفتوحة المصدر، كما تنخرط في مشروعات عديدة في مجالات تقنية المعلوماتيَّة و&nbsp;الصحافة و&nbsp;الثقافة و&nbsp;التعليم و&nbsp;الاستدامة و&nbsp;حرّية الإنترنت.
