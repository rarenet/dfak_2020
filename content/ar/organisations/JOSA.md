---
language: ar
layout: organisation
name: JORDAN OPEN SOURCE ASSOCIATION (JOSA)
website: https://www.josa.ngo/
logo: JOSA_logo.png
languages: English, العربية
services: in_person_training, org_security, web_hosting, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, advocacy, censorship
beneficiaries: hrds, activists
hours:
response_time:
contact_methods:
initial_intake: yes
---

وتسعى "جوسا" لتعزيز الوعي في المجتمع الأردني وتثقيف أهله بأهمية مبادئ المصدر المفتوح وقيمتها في رفعة المجتمع. وينبع عمل جوسا هذا من إيمانها بأن المعلومات غير الشخصية، مثل الرموز المصدرية والتصاميم وجميع أنواع البيانات والمحتوى، يجب أن تنشر علناً للانتفاع منها واستخدامها ونشرها وتطويرها. كما تؤمن "جوسا" بضرورة حماية البيانات الشخصية ضمن أطر قانونية وتقنية، وبأن تتوفر شبكة الإنترنت للجمي
