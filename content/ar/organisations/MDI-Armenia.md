---
language: ar
layout: organisation
name: Media Diversity Institute - Armenia
website: https://mdi.am/
logo: MDI_Armenia_Logo.png
languages: հայերեն, Русский, English
services: in_person_training, org_security, web_hosting, web_protection, digital_support, triage, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, censorship, physical_sec, physical_sec
beneficiaries: journalists, hrds, activists, lgbti, women, cso
hours: 24/7، GMT+4
response_time: 3 ساعات
contact_methods: email, pgp, phone, whatsapp, signal, telegram
email: help@mdi.am
pgp_key_fingerprint: D63C EC9C D3AD 51BE EFCB  683F F2B8 6042 F9D9 23A8
phone: "+37494938363"
whatsapp: "+37494938363"
signal: "+37494938363"
telegram: "+37494938363"
initial_intake: yes
---

معهد التعددية في الإعلام - أرمينيا مؤسسة أهلية غير هادفة للربح تسعى إلى زيادة أثر الإعلام التقليدي و&nbsp;المنصات الاجتماعية و&nbsp;التقنيات الحديثة في حماية حقوق الإنسان و&nbsp;المساهمة في بناء الديمقراطية و&nbsp;المجتمع المدني، و&nbsp;تمكين غير القادرين من التعبير عن أنفسهم و&nbsp;تعميق الفهم الجمعي للأنواع المختلفة من التعددية المجتمعية.

و&nbsp;المعهد في أرمينيا مرتبط بمعهد التعددية في الإعلام في لندن إلا أنّه كيان مستقلّ.
