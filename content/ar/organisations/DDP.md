---
language: ar
layout: organisation
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: من الإثنين إلى الخميس من 09:00 إلى 17:00 بتوقيت وسط أوروبا
response_time: 4 أيّام
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: ‭+31 070 376 5500‬
---

شراكة المدافعين الرقميّين DDP تسعى إلى دعم المدافعين عن حقوق الإنسان الذين يتعرّضون لمخاطر سبرانيّة، كما تسعى إلى تقوية شبكات الاستجابة المحلية للطوارئ. و&nbsp;هي تُنسِّق الإغاثة الطارئة للأفراد و&nbsp;المنظّمات، من المدافعين عن حقوق الإنسان، و&nbsp;الصحافيّين و&nbsp;نشطاء المجتمع المدني و&nbsp;المدوّنين.

لدى DDP خمسة فئات مِنْ الدعم المالي في الحالات الطارئة، و&nbsp;كذلك منحات أطول مدىً تُركِّز على بناء القدرات في المنظّمات. علاوة على ذلك فإنّها تنظِّم زمالة السلامة الرقميَّة التي تتلقّى المنظُّمات بموجبها تدريبات في الأمان الرقمي و&nbsp;الخصوصية مفصّلة لها، كما تنظّم برنامج شبكة الاستجابة السريعة.

