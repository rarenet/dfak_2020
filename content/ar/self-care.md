---
layout: page
title: "الاعتناء بالنفس"
author: Peter Steudtner, Flo Pagano
language: ar
summary: "التحرُّش عبر الإنترنت و&nbsp;التهديد و&nbsp;غيرهما من هجمات عبر وسائل الاتّصال يمكنها إثقالنا بالمشاعر و&nbsp;زيادة حملنا العاطفي: فقد نشعر بالذّنب أو الخزي أو القلق أو الغضب أو الحيرة أو قلّة الحيلة، كما قد نخشى على سلامتنا السيكولوجية أو الجسدية."
date: 2018-09-05
permalink: self-care
parent: Home
sidebar: >
  <h3>الموارد التالية تحوي المزيد عن كيفية حماية نفسك مِنْْ المشاعر الطاغية:</h3>

  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">الرعاية الذاتية للناشطين: الحفاظ على الموارد الأكثر قيمة لديك</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">اهتم بنفسك حتى تتمكن من الاهتمام بمواصلة الدفاع عن حقوق الإنسان</a></li>
    <li><a href="https://www.frontlinedefenders.org/en/resources-wellbeing-stress-management">موارد للرفاهية وإدارة الإجهاد</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">الرعاية الذاتية للأشخاص الذين يتعرضون للتحرش</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">وحدة تدريب على الرعاية الذاتية للنساء في  فضاء الإنترنت</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care/">العافية والمجتمع</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">عشرون طريقة لمساعدة شخص يتعرض للتنمر عبر الإنترنت</a></li>
    <li><a href="https://www.hrresilience.org/">مشروع المرونة في مجال حقوق الإنسان</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_AH_EN+001/about">العناية بصحتك العقلية (يتطلب التسجيل) :Totem-Project دورة تعليمية عبر الإنترنت لمشروع</a></li>
    <li><a href="https://learn.totem-project.org/courses/course-v1:IWPR+IWPR_PAP_EN+001/about">(الإسعافات الأولية النفسية (يتطلب التسجيل) :Totem-Project دورة تعليمية عبر الإنترنت لمشروع</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-2-individual-responses-to-threat.html">الاستجابات الفردية للتهديدات</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-4-team-and-peer-responses-to-threat.html">استجابات الفريق والأقران للتهديدات</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-5-communicating-about-threats-in-teams-and-organisations.html">التواصل حول الأمن في الفرق والمنظمات</a></li>
    </ul>
---

# أصابك الإنهاك؟

التحرُّش عبر الإنترنت و&nbsp;التهديد و&nbsp;غيرهما مِنْ هجمات عبر وسائل الاتّصال قد تثقل شعورنا و&nbsp;تزيد عبئنا العاطفي نحن و&nbsp;زملاءنا و&nbsp;معارفنا: فقد نشعر بالذّنْب أو الخزي أو القلق أو الغضب أو الحيرة أو قلَّة الحيلة، كما قد نخشى على سلامتنا السيكولوجيّة أو الجسديّة.

لا يوجد شعور واحد صحيح في مثل تلك الظروف. فالقابلية للتأثُّر و&nbsp;فداحة ما تعنيه البيانات الشخصية تتفاوتان مِنْ شخصٍ لآخر. كلُّ المشاعر حقَّة، و&nbsp;يجب ألّا تشغل نفسك بما إذا كان ردُّ فعلك صائبًا.

ما ينبغي تذكُّره دومًا أنَّ ما أصابك ليس ذنبك و&nbsp;يجب ألّا تلقي باللائمة على نفسك، و&nbsp;تواصلي مع شخص تثقين فيه يستطيع دعمك و&nbsp;فريقك في مواجهة الأزمة، دعمًا تقنيًّا و&nbsp;نفسيًّا.

تدارُك عواقب الهجمات عبر الإنترنت تلزمه معلومات عمَّا حدث، لكنَّ جمعها لا يجب أنْ يقع على عاتقك أنت وحدك. فإذا كان لديك شخص تثقين به فاطلبي المساعدة مِنْه و&nbsp;اتِّباع الإرشادات في هذا الموقع، أو مكنِّيها مِنْ النفاذ إلى أجهزتك و&nbsp;حساباتك لجمع المعلومات لك.

إذا كنت أنت أو فريقك بحاجة إلى دعم نفسي عاطفي للتعاطي مع طارئ رقمي (أو في أعقاب ذلك) فإنَّ [برنامج الصحة النفسية الذي تديره مجموعة CommUNITY](https://www.communityhealth.team/) تُقدِّم خدمات سيكولوجية بأشكال متباينة للحالات الحادة و&nbsp;أيضًا على المدى الطويل بلغات عِدَّة و&nbsp;في سياقات متنوّعة.
