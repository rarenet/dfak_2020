---
layout: contact-method
title: البريد التقليدي
author: mfc
language: ar
summary: وسائل الاتصال
date: 2018-09
permalink: /ar/contact-methods/postal-mail.md
parent: /ar/
published: true
---

البريد التّقليدي وسيلة تواصل بطيئة لا تلائم الظروف المُلحَّة، و&nbsp;حسب القضاءات التي تنتقل عبرها الرسالة فقد تفتح السلطات الرسائل و&nbsp;تطالع فحواها، كما أنَّها عادة ما تتبَّع المُرسِل و&nbsp;موضع الإرسال و&nbsp;المُتلقّي و&nbsp;الوجهة.

