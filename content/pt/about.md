---
layout: page
title: "Sobre"
language: pt
summary: "Sobre o Kit de Primeiros Socorros Digitais."
date: 2023-05
permalink: about
parent: Home
---

O Kit de Primeiros Socorros Digitais é um trabalho colaborativo entre [RaReNet (Rapid Response Network)](https://www.rarenet.org/) e [CiviCERT](https://www.civicert.org/).

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

A Rede de Resposta Rápida é uma rede internacional de socorristas digitais e defensores de direitos humanos que atuam na segurança digital e é composta por Access Now, Amnesty Tech/Anistia Internacional, Center for Digital Resilience, CIRCL, EFF, Fembloc, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtualroad, e também analistas de segurança independentes que atuam conjuntamente no campo da segurança digital e da resposta rápida.

Algumas destas organizações e indivíduos fazem parte da CiviCERT, uma rede internacional de provedoras de infraestrutura e atendimento em segurança digital que foca, principalmente, no apoio a grupos e organizações que lutam em favor da justiça social e na defesa dos direitos humanos e digitais. A CiviCERT reúne diferentes comunidades de times de resposta a emergências digitais (conhecidos como CERT), e é certificada pela Trusted Introducer, rede européia de certificação de comunidades CERT.

O Kit de Primeiros Socorros Digitais é [um projeto de código aberto, que aceita contribuições de pessoas interessadas em colaborar](https://gitlab.com/rarenet/dfak_2020).

Agradecemos à [Metamorphosis Foundation](https://metamorphosis.org.mk/) pela [localização em albanês](https://digitalfirstaid.org/sq/), à [EngageMedia](https://engagemedia.org/) pela localização em [birmanês](https://digitalfirstaid.org/my/), [indonésio](https://digitalfirstaid.org/id/) e [tailandês](https://digitalfirstaid.org/th/), e á [Media Diversity Institute](https://mdi.am/en/home) pela localização em [armênio](https://digitalfirstaid.org/hy/) do Digital First Aid Kit.

Se você deseja utilizar o Kit de Primeiros Socorros Digitais em contextos nos quais a conectividade é limitada ou inexistente, você pode baixar [uma versão offline aqui](https://digitalfirstaid.org/dfak-offline.zip).

Para quaisquer comentários, sugestões e questões sobre o Kit de Primeiros Socorros Digitais, você pode enviar um e-mail para: dfak @ digitaldefenders . org.

GPG - Impressão digital: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
