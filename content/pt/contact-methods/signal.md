---
layout: contact-method
title: Signal
author: mfc
language: pt
summary: Métodos de contato
date: 2018-09
permalink: /pt/contact-methods/signal.md
parent: /pt/
published: true
---

Usar o Signal garante que o conteúdo de suas mensagens trocadas com a organização que irá recebê-las estará criptografado e apenas você e a pessoa destinatária saberão que as comunicações existiram. Esteja ciente que o Signal usa seu número de telefone como identificação de usuárie, e assim sendo, estará compartilhando o seu número com a organização com a qual você está entrando em contato.

Leia mais: [Como Usar Signal para Android](https://ssd.eff.org/pt-br/module/how-use-signal-android), [Como Usar Signal para iOS](https://ssd.eff.org/pt-br/module/how-use-signal-ios), [Como conversar com desconhecidos no Signal sem revelar seu número de telefone](https://theintercept.com/2017/10/12/signal-batepapo-sem-revelar-seu-numero/)
