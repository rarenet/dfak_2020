---
layout: contact-method
title: WhatsApp
author: mfc
language: pt
summary: Métodos de contato
date: 2018-09
permalink: /pt/contact-methods/whatsapp.md
parent: /pt/
published: true
---

Usar o WhatsApp garante que suas conversas serão protegidas de forma que apenas você e a pessoa a quem as mensagens são destinadas possam ler as comunicações, no entanto, esta troca de mensagens entre vocês pode ser notificada à governos e autoridades tanto na sua jurisdição como da pessoa receptora das mensagens.

Leia mais: [Como Usar WhatsApp no Android](https://ssd.eff.org/pt-br/module/how-use-whatsapp-android) [Como Usar WhatsApp no iOS](https://ssd.eff.org/pt-br/module/how-use-whatsapp-ios).
