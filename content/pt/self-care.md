---
layout: page
title: "Autocuidado e Cuidado Coletivo"
author: FP
language: pt
summary: "Assédio online, ameaças e outros tipos de ataques digitais podem criar sentimentos avassaladores e estados emocionais muito delicados: você pode sentir-se culpado, envergonhado, ansioso, zangado, confuso, desamparado ou até mesmo com medo de seu bem-estar psicológico ou físico."
date: 2023-04
permalink: self-care
parent: Home
sidebar: >
  <h3>Leia mais sobre como proteger você e seu grupo de sobrecarga emocional:</h3>

  <ul>
    <li><a href="https://www.eff.org/pt-br/deeplinks/2015/01/enfrentando-o-desafio-do-assedio-online">Enfrentando o Desafio do Assédio Online</a></li>
    <li><a href="https://mariadajuda.org/">Maria d'Ajuda, a primeira linha de ajuda em segurança digital feita por feministas do Brasil voltada à mulheres, pessoas não bináries, LGBTQIAP+ e organizações da América Latina.</a></li>
    <li><a href="https://escoladeativismo.org.br/wp-content/uploads/2022/09/LabCuidados_Zine2_Ansiedade_WEB_pag_soltas.pdf">Lab Cuidados de Escola de Ativismo</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Praticar o autocuidado para que você possa continuar defendendo os direitos humanos</a></li> (em inglês)
    <li><a href="https://iheartmob.org/resources/self_care">Autocuidado para pessoas com experiência em assédio</a></li> (em inglês)
    <li><a href="https://cyber-women.com/en/self-care/">Módulo de treinamento de autocuidado de uma mulher na era digital</a></li> (em inglês e espanhol)
    <li><a href="https://onlineharassmentfieldmanual.pen.org/self-care">Bem-estar e Comunidade</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Vinte maneiras de ajudar alguém que está sofrendo intimidação on-line</a></li>

  </ul>
---

# Lidando com sobrecarga emocional?

Assédio online, ameaças e outros tipos de ataques digitais podem criar sentimentos avassaladores e estados emocionais muito delicados **_tanto para você quanto para seu grupo_**: você pode sentir culpa, vergonha, ansiedade, raiva, dúvida, desamparo ou até mesmo medo de seu bem-estar psicológico ou físico. **_E esses sentimentos podem ser muito diversos dentro de um grupo_**.

Não existe um modo "certo" de sentir, pois o seu estado de vulnerabilidade e o que sua informação pessoal significa para você será diferente de pessoa para pessoa. Qualquer emoção é justificada e você não deve se preocupar se sua reação é ou não a correta.

A primeira coisa que você deve se lembrar é que o que está acontecendo com você não é sua culpa, portanto, culpar a si ou a qualquer outra pessoa dentro de um grupo não vai ajudar a reagir a emergência. O melhor a ser feito pode ser contactar alguém de confiança que possa dar apoio e ajudar a encontrar a melhor forma de responder a situação.

Para diminuir os danos de um ataque online, você precisará coletar informações sobre o que aconteceu, mas não precisa fazer isso só. Peça ajuda de alguém de sua confiança seguindo as instruções deste site ou permitindo que essa pessoa tenha acesso a seus dispositivos ou contas para coletar estas informações para você. **_Você também pode pedir apoio técnico e/ou emocional durante este processo_**.

Se você ou seu grupo precisarem de apoio emocional para lidar com uma emergência digital (ou também precisarem de acompanhamento), o [Programa de Saúde Mental Comunitária da Team CommUNITY](https://www.communityhealth.team/) oferece serviços psicossociais em diferentes formatos para casos agudos, e também a longo prazo, podendo prestar esse apoio em algumas línguas e diferentes contextos.
