---
layout: page
title: "Documentando Ataques Digitais"
author: Constanza Figueroa, Patricia Musomba, Candy Rodríguez, Gus Andrews, Alexandra Hache, Nina
language: pt
summary: "Dicas de como documentar ataques digitais."
date: 2023-05
permalink: documentation
parent: Home
sidebar: >
  <h3>Leia mais sobre como documentar ataques online:</h3>
  <ul>
    <li><a href="https://acoso.online/brasil/denunciar-a-justica/">Acoso.online dicas gerais, comunicar um caso e como conservar as provas</a></li>
    <li><a href="https://www.techsafety.org/documentationtips">National Network to End Domestic Violence: Dicas de documentação para vítimas de abuso tecnológico e perseguição</a></li> (em inglês)
    <li><a href="https://chayn.gitbook.io/how-to-build-a-domestic-abuse-case-without-a-lawye/portugues">Chayn: Como construir um caso de abuso doméstico sem ter o apoio de advogades</a></li>
    </ul>
---

# Documentando Ataques Digitais

Documentar os ataques digitais pode ser útil em diversos aspectos. Registrar o que está acontecendo em um ataque pode te ajudar das seguintes maneiras:

- entender melhor a situação para te ajudar a agir no sentido de se proteger
- fornecer evidências que os especialistas técnicos e legais irão precisar para te ajudar
- entender melhor os padrões de ataque, te ajudando a identificar outras possíveis ameaças
- trazer mais paz de espírito e compreensão de como os ataques te impactam emocionalmente

Independente de estar documentando os ataques para si, ou para buscar apoio técnico e legal, registros bem estruturados podem te ajudar a entender melhor:

- O tamanho e o objetivo do ataque: um ataque pontual ou um padrão que vem sendo repetido, mirando apenas em você ou em um grupo de pessoas.
- O comportamento de quem faz os ataques: se essas pessoas agem só, se fazem parte de um grupo organizado, se estão usando informações disponíveis na internet ou mesmo acessando suas informações pessoais privadas, as táticas e tecnologias que podem estar usando, etc.
- Se as medidas que você irá tomar podem oferecer riscos online ou físicos, ou se podem piorar a situação.

Já que você está reunindo informações, é importante considerar as seguintes situações:

- Você está documentando para sua tranquilidade e/ou para entender melhor o que está acontecendo?
- Você deseja buscar apoio técnico para que os ataques parem? Nesse caso, considere visitar a sessão de suporte do [Kit de Primeiros Socorros Digitais](https://digitalfirstaid.org/pt/support/) e veja qual organização da CiviCERT pode te ajudar.
- Além disso, você também deseja acionar a justiça?

Dependendo dos seus objetivos, você pode precisar documentar os ataques de diferentes maneiras. Se você precisar levantar evidências para um processo na justiça, deve reunir tipos específicos de informações que serão válidas no âmbito da justiça. Acompanhantes técnicos vão precisar de evidências adicionais como endereços da web (URLs), nomes de usuárie e capturas de tela. Veja abaixo mais detalhes sobre as informações que você deve coletar em cada uma dessas circunstâncias.

## Cuide do seu bem-estar mental e emocional enquanto estiver fazendo a documentação

Lidar com ataques virtuais é estressante e o seu bem-estar físico e emocional deve ser uma prioridade, independente do tipo de documentação que está realizando. O processo de documentação não pode contribuir para piorar o seu estress.
Ao começar a documentar um ataque considere o seguinte:

- Existem várias maneiras de documentar e registrar. Escolha aquela com a qual você se sinta mais confortável e que faz mais sentido para seus objetivos.
- Documentar ataques digitais pode demandar bastante do seu emocional e pode acessar memórias traumáticas. Sentiu uma sobrecarga? Considere delegar esse processo para colegues, amigues e pessoas de confiança ou grupos de monitoramento que possam manter a documentação em dia.
- Se o ataque aconteceu direcionado a um coletivo, considere alternar as responsabilidades da documentação entre diferentes pessoas.
- Consentimento é fundamental. A documentação deve continuar apenas se você ou o grupo concordar.
- [Aqui estão algumas dicas de como conseguir a ajuda de amigos e familiares](https://onlineharassmentfieldmanual.pen.org/guidelines-for-talking-to-friends-and-loved-ones/) (em inglês)

Documentar ataques digitais ou violência de gênero significa reunir informações sobre o que você ou seu coletivo estão enfrentando. Essa documentação não precisa ser apenas técnica e racional. Ela também pode te ajudar à processar a violência sofrida, através de registros de como você se sentiu em cada ataque. Esses registros podem ser em formato de texto, imagem, áudio, vídeo e até mesmo expressões artísticas. Recomendamos que você faça isso offline e siga os passos abaixo para proteger sua privacidade enquanto lida com esses sentimentos.

## Se preparando para documentar um ataque digital

Após se certificar do seu próprio bem-estar é importante que, antes de começar a documentar peças soltas das evidências, **faça um "mapa" de todas as informações relevantes** sobre o ataque. Isso vai te ajudar a identificar a completude das evidências que você pode querer e precisar reunir.

A parte mais importante da documentação é **manter um registro organizado (ou "log") do processo**. Para uma documentação com fins técnicos e/ou jurídicos, fazer um registro que contenha peças-chave dos dados é importante para demonstrar padrões explícitos de ataque. Quando está documentando para seu próprio registro, um texto com uma estrutura simples pode ser o suficiente, mas um registro ("log") bem estruturado pode te ajudar a identificar melhor os padrões nos ataques enfrentados por você. Seu registro pode ser um documento de texto ou uma planilha, ambos digitais. Você também pode manter um registro físico ou impresso. A escolha é toda sua.

**Todas as interações digitais deixam rastros (metadados) compostos por**: Horário, duração, endereços, contas de envio e recebimento, etc. Esses rastros são salvos em logs pelos seus próprios dispositivos, por empresas de telefonia e redes sociais, por provedores de internet e outros. Analisar esses rastros pode fornecer para especialistas técnicos e legais informações centrais sobre quem está por trás do ataque. Por exemplo, um registro do número de telefone usado em 20 chamadas em determinado período pode sustentar uma acusação de assédio.

Manter um registro organizado e estruturado desses dados vai te ajudar a demonstrar **padrões nos ataques** de maneiras que podem sustentar um processo na justiça ou ajudar um especialista em segurança digital a interrompê-los e proteger seus dispositivos.

Os critérios a seguir são bastante úteis na maioria dos casos, mas podem variar em diferentes situações. Você pode copiar e colar os campos abaixo em uma planilha ou tabela para estruturar seus relatórios de cada ataque. Adicione mais campos se achar necessário.

| Data | Horário | Endereço de e-mail ou número de telefone usado por quem está atacando | Links relacionados ao ataque | Nome e/ou nome de usuárie utilizado por quem está atacando | Tipos de ataque digital (assédio, discurso de ódio, desinformação, etc.) | Técnicas utilizadas por quem está atacando |
| ---- | ------- | --------------------------------------------------------------------- | ---------------------------- | ---------------------------------------------------------- | ------------------------------------------------------------------------ | ------------------------------------------ |

Veja nos links a seguir alguns exemplos de outros modelos de registro que você também pode utilizar. São modelos fornecidos e utilizados pela comunidade:

- [Template para registro (log) de incidente utilizado pela Linha de Ajuda em Segurança Digital da Access Now](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/tree/master/templates/incident_log_template.md) (em inglês)
- [Modelo de template em caso de violência de gênero da Acoso.online](https://acoso.online/site2022/wp-content/uploads/2019/01/reactingNCP.pdf) (em inglês)

Além do arquivo de registro, **salve em uma ou mais pastas tudo que você tiver relacionado ao ataque**, toda e qualquer evidência que você tenha exportado, feito download e capturas de tela. Mesmo que aquela evidência não tenha valor legal, pode ser útil para demonstrar o tamanho do ataque e te ajudar a planejar estratégias de resposta.

**Armazene com segurança as informações que você está reunindo**. É mais seguro armazenar backups em seus dispositivos pessoais, e não só online ou em alguma "nuvem". Proteja esses arquivos com criptografia e em pastas escondidas ou repartições do HD, se possível. Você não quer perder os registros, ou pior, ter eles expostos por aí.

**Evite bloquear, silenciar ou reportar as contas que estão te atacando** nas redes sociais até que você tenha reunido as informações que precisa, isso pode te impedir de conseguir as provas relacionadas à essas contas. Se você já fez isso, não se preocupe, poderá desfazer, ou poderá coletar as informações que precisa através da conta de amigues e colegues.

## Faça capturas de tela

Não subestime a importância de fazer capturas de tela de todos os ataques (se necessário, peça para alguém que você confia que faça isso por você). Muitas mensagens digitais são fáceis de serem deletadas ou se perderem. Algumas delas, como tweets os mensagens no Whatsapp, só podem ser resgatadas com a ajuda das plataformas, que normalmente demoram para responder as solicitações e, em muitos casos, nem chegam a responder.

Se você não sabe como fazer uma captura de tela no seu dispositivo, faça uma pesquisa online para aprender, informando o modelo e sistema operacional, por exemplo: "Samsung Galaxy S21 Android como fazer captura de tela".

Se você precisa fazer uma captura de tela em um navegador web, **lembre-se de incluir o endereço da página (URL) na imagem, ele estará no topo da janela**. Pode acontecer da barra de endereço desaparecer com a rolagem da página, então se atente. Endereços podem ajudar especialistas legais e técnicos a identificarem mais rápido os locais dos ataques. Veja a seguir um exemplo de uma captura de tela em que a URL é mostrada:

<img src="/images/screenshot_with_URL.png" alt="Faça capturas de tela com o URL" width="80%" title="Faça capturas de tela com URL.">

<a name="legal"></a>

## Documentação para um processo judicial

Reflita se você deseja tomar medidas legais. Isso vai te expor a riscos? Você pode arcar com o tempo e o esforço que será necessário? Iniciar um processo judicial nem sempre é necessário. Avalie se isso será benéfico para você.

Se decidir levar seu caso para a justiça, procure a orientação de advogades ou uma organização que você confia. Orientações legais ruins podem ser estressantes e causar transtornos. Não tome providências legais por conta própria, a menos que você realmente saiba o que está fazendo.

Na maioria das jurisdições, advogades precisam demonstrar que as evidências são relevantes para que o processo prossiga. Se elas foram obtidas de maneira legal, como elas foram obtidas, verificadas, que a sua coleta foi necessária para a construção do caso, e outros critérios que as tornam válidas para a justiça. Uma vez que advogades demonstram isso, as evidências serão julgadas pela justiça.

O que você precisa considerar ao fazer uma documentação para um processo legal:

- É fundamental que as evidências sejam recolhidas rapidamente, desde o início do ataque. Conteúdos podem ser apagados com rapidez por usuáries ou pela própria rede social, tornando mais difícil que sejam acessadas futuramente.
- Mesmo se o ataque parecer pequeno, é importante preservar todas as evidências. Elas podem fazer parte de um padrão que só poderá ser comprovado como criminoso se acontecer do início ao fim.
- Você pode pedir às plataformas ou que guardem as informações do ataque para você. Isso pode ser possível apenas durante um período de tempo (semanas ou 1 mês) desde o ataque. Para mais informações sobre como fazer isso, consulte as informações do site [Without My Consent sobre a apresentação de um pedido de retenção de litígio](https://withoutmyconsent.org/resources/something-can-be-done-guide/evidence-preservation/#consider-whether-to-include-a-litigation-hold-request) (em inglês).
- Os carimbos de data e hora, os endereços de e-mail e sites, são cruciais para que as provas sejam aceitas em tribunal. Advogades têm de demonstrar que uma mensagem passou de um dispositivo para outro em um determinado dia e a uma determinada hora utilizando estas informações.
- Advogades também precisam verificar se as provas não foram adulteradas, se estiveram em boas mãos desde que foram armazenadas e se são autênticas. As capturas de tela e as impressões de mensagens de e-mail não são consideradas provas suficientemente fortes nesse contexto.

Por estas razões legais, pode ser melhor contratar um perito, como um notário ou uma empresa de certificação digital, que possa testemunhar em tribunal se necessário. As empresas de certificação digital são muitas vezes mais acessíveis financeiramente do que notários certificados.Qualquer notário ou empresa de certificação que você contrate deve ter conhecimentos técnicos suficientes para efetuar tarefas como estas, se necessário:

- confirmar carimbos de data e hora;
- verificar a existência ou não de determinado conteúdo. Por exemplo: comparando suas capturas de tela, e outras evidências levantadas, com as fontes originais, confirmando que não foram adulteradas.
- confirmar que certificados digitais batem devidamente com URLs.
- verificar identidades. Por exemplo: verificar se uma pessoa aparece em uma lista de perfis de uma plataforma de redes sociais e se possui um perfil com o apelido que realiza os ataques.
- confirmar a propriedade de um número em uma conversa de Whatsapp.

Lembre-se de que nem todas as evidências levantadas serão aceitas pela justiça. Pesquise os procedimentos legais sobre ameaças digitais no seu país e região para decidir o que irá recolher ou não.

### Documentação de violações de direitos humanos

Se precisar documentar violações de direitos humanos, fazer download ou copiar materiais que as plataformas das redes sociais tendem a remover, você pode entrar em contato com organizações como a [Mnemonic](https://mnemonic.org/en/our-work) (em inglês) que ajudam a preservar esses materiais.

## Como guardar provas para relatórios jurídicos e forenses

Além de guardar capturas de tela, aqui estão mais algumas instruções sobre como guardar provas de forma mais completa para o seu caso e para o trabalho de alguém que lhe dê apoio jurídico ou técnico:

- **Registos de chamadas.** Os números das chamadas recebidas e efetuadas são guardados em uma base de dados no seu telefone.
  - É possível fazer capturas de tela desses registos.
  - Também é possível guardar esses registos fazendo uma cópia de segurança do sistema do seu telefone ou utilizando um software específico para descarregar essas informações.
- **Mensagens de texto (SMS)**: a maioria dos celulares permitem fazer cópias de segurança das suas mensagens SMS para a nuvem. Em alternativa, você pode fazer também capturas de tela. Nenhum destes métodos conta como prova legal sem verificação adicional, mas continuam sendo importantes para documentar ataques.
  - Android: se o seu celular não tiver a capacidade de fazer cópias de segurança de mensagens SMS, utilize um aplicativo como o [SMS Backup & restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore).
  - iOS: ative a cópia de segurança das mensagens seguindo o caminho: iCloud em Definições > [nome de utilizador] > Gerir armazenamento > Cópia de segurança. Toque no nome do dispositivo e ative as cópias de segurança de mensagens.
- **Gravação de chamadas**. Os celulares mais recentes vêm com um gravador de chamadas instalado. Verifique quais são as regras legais no seu país ou jurisdição local sobre a gravação de chamadas com ou sem o consentimento de ambas as partes. Em alguns casos pode ser ilegal. Se for legal, pode configurar o gravador de chamadas para ser ativado quando atender a chamada, sem alertar o remetente.
  - Android: em Definições > Telefone > Definições de gravação de chamadas. Ative a gravação automática de todas as chamadas, chamadas de números desconhecidos ou de números específicos. Se essa opção não estiver disponível, baixe um aplicativo como o [Gravador de chamadas](https://play.google.com/store/apps/details?id=com.lma.callrecorder).
  - iOs: A Apple é mais restritiva em relação à gravação de chamadas e bloqueou a opção por padrão. Você terá que instalar um aplicativo como o [RecMe](https://apps.apple.com/us/app/call-recorder-recme/id1455818490) (em inglês).
- **Emails**. Todas as mensagens de e-mail têm um "cabeçalho" com informações sobre quem enviou a mensagem e quando, assim como os endereços e o carimbo postal de uma carta em papel.
  - Veja instruções sobre como visualizar e guardar cabeçalhos de e-mail, você pode utilizar [este guia do Computer Incident Response Center Luxembourg (CIRCL)](https://www.circl.lu/pub/tr-07/) (em inglês).
  - Se tiver mais tempo, pode também configurar seu e-mail para descarregar todas as mensagens para um gerenciador de e-mails no desktop, como o Thunderbird ou Outlook, utilizando [POP3](https://support.mozilla.org/pt-BR/kb/diferenca-entre-imap-e-pop3#w_changing-your-account). As instruções para configurar uma conta de correio eletrônico no Thunderbird podem ser encontradas na [documentação oficial](https://support.mozilla.org/pt-BR/kb/configuracao-manual-de-conta).
- **Fotografias**. Todas as fotografias têm "etiquetas EXIF", metadados que podem indicar onde e quando uma fotografia foi tirada.
  - Pode ser possível guardar imagens de um site ou de mensagens que tenha recebido de forma que guarde as etiquetas EXIF que se perderiam se fizesse apenas uma captura de tela. Clique e segure em cima na imagem em um dispositivo móvel ou clique com o botão direito do mouse na imagem em um computador. Clique no botão de control no Mac, utilize a tecla de menu no Windows.
- **Websites**. Fazer o download ou backup de páginas da web pode ser útil para quem irá te ajudar.
  - Se a página onde ocorreu o assédio for pública, basta introduzir o endereço da página no site Internet Archive's [Wayback Machine](https://web.archive.org/) (em inglês) para salvá-la. Guarde a data que armazenou no site e retorne mais tarde à página guardada nessa data.
  - Capturas de tela oferecem menos detalhes, mas podem também salvar informações importantes.
- **WhatsApp**. Conversas do WhatsApp podem ser baixadas em formato de texto ou como mídias anexadas. Também é feita uma cópia de segurança por padrão, para o iCloud ou o Google Drive.
  - Siga as instruções para exportar o histórico de mensagens de uma conversa individual ou em grupo [aqui](https://faq.whatsapp.com/1180414079177245/?helpref=uf_share&cms_platform=android).
- **Telegram**. Para exportar conversas do Telegram, utilize sua versão para desktop. Escolha o formato e o período de tempo que deseja exportar, e o Telegram irá gerar um ficheiro HTML. Veja as instruções [aqui](https://telegram.org/blog/export-and-more) (em inglês).
- **Facebook Messenger**. Inicie a sessão na sua conta do Facebook. Prossiga para Definições > Suas informações no Facebook > Fazer o download de uma cópia das suas informações. Aparecerão várias opções: selecione "Mensagens". O Facebook demora algum tempo para processar o arquivo, mas te avisa quando estiver disponível para baixar.
- Se precisar salvar **vídeos** como prova, tenha uma unidade externa como um HD, um pen drive ou um cartão SD, com espaço de armazenamento suficiente. Poderá ser necessário utilizar uma ferramenta de captura de vídeo. A aplicação Câmera, a XBox Game Bar ou a Snipping Tool podem ser utilizadas para isso no Windows, o Quicktime no Mac, ou você pode utilizar um plug-in do browser, como o [Video DownloadHelper](https://www.downloadhelper.net/) (em inglês) para guardar vídeos.
- Em caso de ataques organizados em larga escala - por exemplo, tudo postado com uma **hashtag específica ou um grande número de comentários** - você pode precisar da ajuda de uma equipe de segurança, um laboratório de perícia digital ou uma universidade para recolher e processar uma grande quantidade de dados. Entre as organizações que você pode entrar em contato para obter ajuda estão [Citizen Lab](https://citizenlab.ca/about/) (em inglês), [Fundacion Karisma’s K-Lab](https://web.karisma.org.co/klab/) (em espanhol), [Meedan](https://meedan.com/programs/digital-health-lab) (em inglês), the Stanford Internet Observatory and the Oxford Internet Institute.
- **Arquivos anexados à mensagens maliciosas** são evidências valiosas. Mas lembre-se! Em nenhuma circunstância você deve abrir esses arquivos. Conte com a ajuda de alguém com conhecimento técnico para armazenar esses anexos de maneira segura e enviar para pessoas capazes de analisá-los.
  - Você pode usar o [Danger Zone](https://dangerzone.rocks/) (em inglês) para converter PDFs, arquivos de texto ou imagens para arquivos seguros.
- Para ameaças mais diretas como **spywares** em seus dispostivos ou **e-mails maliciosos** enviados diretamente para você, recolher evidências mais técnicas e profundas pode ser relevante para seu caso. É importante avaliar quando recolher essas evidências por conta própria ou designar essa tarefa para especialistas. Se não souber como proceder, acione [especialistas de confiança](https://digitalfirstaid.org/pt/support/).
  - **Se o dispositivo estiver ligado, deixe ligado. Se estiver desligado, deixe desligado**. Ao ligar ou desligar dispositivos, corremos o risco de perder informações importantes. Em situações mais sensíveis e em caso de investigações legais, pode ser melhor contar com especialistas forenses antes de ligar ou desligar o dispositivo.
  - Tire fotos do dispositivo que pode estar infectado com softwares maliciosos, documentando sua condição física e o lugar em que ele estava quando começaram as suspeitas. Ele apresenta algum dano ou avaria? Estava molhado? Perto dele estava alguma ferramenta com a qual ele pode ter sido alterado?
  - Guarde em um lugar seguro o dispositivo e os dados que você recolheu.
  - O método de extração de dados mais indicado vai depender do dispositivo. A extração de dados de um notebook é diferente de um smartphone. Cada dispositivo requer ferramentas e conhecimentos específicos.
- Coleta de dados relacionados à uma ameaça direcionada envolve **copiar arquivos do registro do sistema** do celular ou computador. Esses registros são coletados automaticamente pelos dispositivos e você pode solicitá-los. Esses registros ajudam no armazenamento de metadados e anexos com segurança. Envie-os para analistas.
  - Para preservar esses metadados, mantenha o dispositivo isolado de outros sistemas de armazenamento, desligue o Wi-Fi e o Bluetooth, desconecte cabos de rede e nunca edite os ficheiros de registo.
  - Lembre-se de não plugar no dispositivo um pen drive para copiar esses arquivos.
  - Esses registros podem conter informações sobre o estados dos arquivos no dispositivo. Por exemplo: a maneira como foram acessados, sobre o estado do dispositivo, se foi acionado um comando de encerramento das ações ou se alguém pode ter tentado copiar arquivos para outro dispositivo.
