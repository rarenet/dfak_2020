---
layout: topic
title: "Meu dispositivo está com um comportamento estranho"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, Rarenet
language: pt
summary: "Se o seu computador ou celular está agindo de forma suspeita, pode haver um programa malicioso ou indesejado no dispositivo"
date: 2023-04
permalink: topics/device-acting-suspiciously
parent: /pt/
order: 3
---

# Meu dispositivo está com um comportamento estranho

Ataques de malware (software malicioso) evoluíram e se tornaram mais sofisticados ao longo dos anos. Essas ameaças representam diversos perigos a sua vida digital e podem acarretar sérias consequências tanto para você quanto para os dados de sua organização.

Malwares existem em diferentes formatos, como vírus, _phising_, sequestro digital (ransomware), cavalos-de-tróia e controle de dispositivos (rootkits ou C&C). Algumas das ameaças são: comprometimento do sistema, roubo de dados (como senhas de serviços pessoais e de contas bancárias, informações financeiras e cadastrais, etc), sequestro de dados (_ransomware_) com finalidade de chantagem ou controlando seu dispositivo e **_espiando tudo o que você faz nele_**, ou ainda com a finalidade de usar seus dados como parte de um ataque distribuído de negação de serviço (DDoS).

Alguns métodos usados frequentemente por malfeitores comprometem a você e aos seus dispositivos através de atividades cotidianas, por exemplo:

- Um e-mail ou post em rede social que seduz você a abrir um link ou baixar um arquivo voluntariamente.

- Forçar pessoas a baixar e instalar um programa ou app de fonte duvidosa.

- Forçar alguém a colocar usuário e senha em um site feito para parecer verdadeiro, mas que é uma fraude.

Esta seção do Kit de Primeiros Socorros Digitais irá conduzir você através de simples passos para determinar se seu dispositivo pode ou não estar infectado.

Se você acredita que seu computador ou telefone está funcionando de maneira suspeita, comece tentando entender os indicativos que este dispositivo apresenta.

Indicativos que normalmente são interpretados como atividade suspeita, mas que geralmente não são motivos para preocupação, incluem:

- Ouvir cliques durante uma ligação;
- Consumo de bateria acima do normal;
- Dispositivo esquentando mesmo sem uso;
- Dispositivo funcionando muito devagar.

Estes sintomas geralmente são mal interpretados como motivos para se preocupar com o comportamento do dispositivo. Ainda assim, nenhum destes sintomas sozinhos são indicadores confiáveis de atividade suspeita.

Os sintomas abaixo são um pouco mais confiáveis para determinar se um dispositivo pode estar comprometido:

- O dispositivo reinicia sozinho sem causa ou defeito aparente;
- Aplicativos param de funcionar depois de dar um comando;
- Atualizações de segurança falham repetidamente na instalação;
- A luz da câmera acende sozinha quando não deveria estar em uso;
- Falhas críticas frequentes, como a ["Tela Azul"](https://pt.wikipedia.org/wiki/Tela_azul_da_morte), Kernel Panic (Linux e Android) ou telas "vazias" em outros dispositivos;
- Janelas piscando ou aparecendo de repente;
- Alertas de vírus.

## Workflow

### start

#### Você sente que seu dispositivo está comprometido?

Se você acredita que mesmo após ler a introdução seus dispositivos possam estar comprometidos, o guia a seguir pode ajudar a identificar o problema.

##### Options

- [Acredito que meu telefone esteja comprometido](#phone-intro)
- [Acredito que meu computador esteja comprometido](#computer-intro)
- [Acredito que meu dispositivo não esteja mais comprometido](#device-clean)

### device-clean

#### Acredito que meu dispositivo não esteja mais comprometido

> Muito bom! No entanto, tenha em mente que estas instruções ajudam apenas a fazer um diagnóstico rápido. Ainda que ele possa ser suficiente para identificar anormalidades mais visíveis, programas espiões melhor elaborados conseguiriam burlar estas regras e se ocultar no sistema de forma eficiente.

Se você ainda suspeita que o dispositivo está comprometido as alternativas mais interessantes seriam:

##### Options

- [Buscar ajuda externa](#malware_end)
- [Tentar uma formatação completa do dispositivo](#reset).

### phone-intro

#### Acredito que meu telefone esteja comprometido

> É importante considerar como o seu dispositivo pode estar comprometido.
>
> - Há quanto tempo você começou a suspeitar que seu dispositivo estava agindo de forma suspeita?
> - Você se lembra de clicar em algum link de fontes desconhecidas?
> - Você recebeu mensagens de pessoas que não reconhece?
> - Você instalou algum software não oficial?
> - O aparelho esteve fora de sua posse?

Refletir sobre as circunstâncias que levaram o comprometimento de um dispositivo pode ajudar a identificar possíveis pontos vulneráveis e melhorar a segurança digital.

##### Options

- [Tenho um dispositivo Android](#android-intro)
- [Tenho um dispositivo iOS](#ios-intro)

### android-intro

#### Tenho um dispositivo Android

> Primeiro, verifique se tem algum aplicativo desconhecido instalado no seu dispositivo Android.
>
> Você pode encontrar uma lista na seção "Apps" entrando no ícone de configurações. Identifique todos os aplicativos que não tenham vindo instalados de fábrica com o seu dispositivo e que não se lembre de ter instalado.
>
> Se você suspeitar de algum aplicativo na lista, é uma boa prática buscar informações adicionais para determinar se ele é malicioso ou seguro.

Você encontrou algum aplicativo suspeito?

##### Options

- [Não encontrei](#android-unsafe-settings)
- [Sim, identifiquei um app possivelmente malicioso](#android-badend)

### android-unsafe-settings

#### Não, não encontrei nenhum aplicativo suspeito

>O Android oferece aos usuários a opção de ativar o acesso de nível inferior aos seus dispositivos. Isso pode ser útil para desenvolvedores de aplicativos, mas também pode expor o dispositivos a ataques adicionais. Você deve revisar essas configurações de segurança para ter certeza que estão marcadas com as opções mais seguras. Os fabricantes podem vender dispositivos com padrões inseguros. Essas configurações devem ser revisadas mesmo que você não tenha feito alterações.
>
> **Apps de fontes desconhecidas**
>
> O Android normalmente bloqueia a instalação de aplicativos que não foram baixados da loja da Google (Play Store), que possui processos de revisão e identifição de aplicativos maliciosos para verificar os desenvolvedores. Invasores geralmente tentam burlar estas verificações tentando compartilhar apps com usuáries diretamente **_através de links ou arquivos vindos de fontes não confiáveis_**. É importante confirmar que seu dispositivo não permita instalação de apps por este método.
>
> Vá à seção "Segurança" nas configuração de seu Android e confirme que a opção "Instalar apps de fontes desconhecidas" está desabilitada. Em muitas versões de Android você encontrará essa opção nas configurações de Segurança, no entanto, dependendo da versão do Android que você está utilizando e em aparelhos celulares mais recentes, isso pode estar em outro lugar, como nas permissões dos apps.
>
> **Modo desenvolvedor e acesso ao ADB (Android Debug Bridge)**
>
> No Android, desenvolvedores são permitidos rodar comandos diretamente na camada do sistema quando estão no "modo desenvolvedor". Quando ele está habilitado, pode expor os aparelhos a ataques físicos caso alguns cuidados não sejam tomados. Alguém com acesso físico ao dispositivo pode usar o modo desenvolvedor para baixar cópias de dados sensíveis do dispositivo com alguma facilidade, ou mesmo instalar apps maliciosos.
>
> Se você ver um modo desenvolvedor aparecendo nas suas configurações, deve-se assegurar de que o acesso ao ADB (aparece assim mesmo) está desabilitado.
>
> **Google Play Protect**
>
> O serviço de proteção do Google Play está disponível em todos os aparelhos Android recentes. Ele vai fazer verificações regularmente em todos os aplicativos e tem a capacidade de remover automaticamente qualquer aplicativo malicioso conhecido do seu dispositivo. Habilitar o Play Protect faz com que não envie informações sobre o seu dispositivo automaticamente para o Google (tais como os aplicativos que estão instalados).
>
> A proteção pode ser habilitada nas opções de segurança do seu aparelho. Mais informações estão disponíveis no site [Play Protect](https://developers.google.com/android/play-protect?hl=pt-br).

Você identificou alguma configuração insegura no seu dispositivo?

##### Options

- [Não identifiquei](#android-bootloader)
- [Sim, identifiquei possíveis inseguranças](#android-badend)

### android-bootloader

#### Não, não identifiquei nenhuma configuração insegura

> O gerenciador de inicialização do Android (também conhecido como bootloader), é um componente chave do sistema que é executado assim que você liga o aparelho. O bootloader permite que o sistema operacional inicie e use o hardware. Um bootloader comprometido dá a pessoa que está invadindo o acesso completo ao hardware do dispositivo. A maioria dos fabricantes vende seus dispositivos com um bootloader bloqueado contra alterações. Uma forma comum de identifcar se a assinatura da fabricante foi modificada é reiniciar o aparelho e observar o logotipo do boot. Se aparecer um triângulo com uma exclamação, o bootloader original foi alterado. Seu dispositivo também pode ter sido comprometido se estiver sendo mostrada uma tela de alerta de desbloqueio e você deve desconfiar caso não tenha feito o desbloqueio para instalar uma versão alternativa do Android, como por exemplo o **_CyanogenMod (atual LineageOS)_**. Faça uma redefinição de fábrica (reset) de seu aparelho se estiver mostrando um desbloqueio inesperado do bootloader.

O seu aparelho está usando o bootloader original, ou ele está comprometido?

##### Options

- [O bootloader do meu aparelho está comprometido](#android-badend)
- [O meu aparelho está usando o bootloader original](#android-goodend)

### android-goodend

#### O meu aparelho está usando o bootloader original

> Seu aparelho não parece estar comprometido.

Você ainda tem receio de que seu dispositivo esteja comprometido?

##### Options

- [Sim, gostaria de buscar auxílio profissional](#malware_end)
- [Não, consegui resolver meus problemas](#resolved_end)

### android-badend

#### Sim, identifiquei um app possivelmente malicioso

> Seu dispositivo pode estar comprometido. Uma [**_redefinição de fábrica_**](#reset) possivelmente irá remover qualquer ameaça presente no seu aparelho. No entanto, nem sempre é a melhor solução. Além disso, você pode querer investigar mais a fundo para identificar o nível de exposição e a natureza exata do ataque que você sofreu.
>
> Se quiser, existem duas ferramentas de auto-diagnóstico chamadas [Emergency VPN](https://www.civilsphereproject.org/emergency-vpn) (em inglês) e **_[PiRogue](https://pts-project.org/)_** (em inglês) podem ser usada para auxiliar na resposta, ou você pode buscar assistência através de uma organização parceira.

Você gostaria de buscar assistência posterioriormente?

##### Options

- [Sim, gostaria de buscar auxílio profissional](#malware_end)
- [Não, eu tenho uma rede de suporte local que pode me auxiliar](#resolved_end)

### ios-intro

#### Eu tenho um dispositivo IOS

> **_Siga essas etapas para [ver quem tem acesso ao seu IPhone ou IPad](https://support.apple.com/pt-br/guide/personal-safety/ipsb8deced49/web)._** Olhe as configurações do iOS para ver se tem alguma coisa estranha.
>
> No app de Configurações, verifique se seu dispositivo está conectado com o seu ID Apple. O primeiro item do menu no lado da sua mão esquerda deverá ser o seu nome ou o nome que você utiliza com a sua conta Apple. Clique nele e verifique se está mostrando o seu endereço de e-mail correto. No final da tela, na parte de baixo, você verá uma lista com nomes e modelos de todos os aparelhos conectados com o seu ID Apple.
>
> Verifique se o seu dispositivo está ligado a um sistema de gestão de dispositivos móveis (MDM) que você não reconhece. Vá em Definições > Geral > VPN e Gestão de Dispositivos e verifique se existe uma seção para Perfis. Pode [eliminar quaisquer perfis de configuração desconhecidos](https://support.apple.com/pt-br/guide/personal-safety/ips41ef0e8c3/1.0/web/1.0). Se não tiver Perfis listados, não está inscrito na MDM.

##### Options

- [Todas as informações estão corretas e eu ainda estou no controle do meu ID Apple](#ios-goodend)
- [O meu nome ou outros detalhes estão incorretos, ou estou vendo dispositivos na lista que não são meus](#ios-badend)

### ios-goodend

#### Todas as informações estão corretas e eu ainda estou no controle do meu ID Apple

> Seu aparelho não parece estar comprometido.

Você ainda tem receio de que seu dispositivo esteja comprometido?

##### Options

- [Sim, gostaria de buscar auxílio profissional](#malware_end)
- [Não, consegui resolver meus problemas](#resolved_end)

### ios-badend

#### O meu nome ou outros detalhes estão incorretos, ou estou vendo dispositivos na lista que não são meus

> Seu dispositivo pode estar comprometido. Uma **_[redefinição de fábrica](#reset)_** possivelmente irá remover qualquer ameaça presente no seu aparelho. No entanto, nem sempre é a melhor solução. Além disso, você pode querer investigar mais a fundo para identificar o nível de exposição e a natureza exata do ataque que você sofreu.
>
> Se quiser, existem duas ferramentas de auto-diagnóstico chamadas [Emergency VPN ](https://www.civilsphereproject.org/emergency-vpn) (em inglês) e **_[PiRogue](https://pts-project.org/)_** (em inglês) podem ser usada para auxiliar na resposta, ou você pode buscar assistência através de uma organização parceira.

O que você quer fazer?

##### Options

- [Quero resetar meu dispositivo para configurações de fábrica](#reset)
- [Quero ajuda profissional especializada](#malware_end)
- [Tenho uma rede de apoio local onde posso procurar por ajuda profissional](#resolved_end)

### computer-intro

#### Acredito que meu computador esteja comprometido

> **Nota: caso você esteja sob ataque de ransomware, siga diretamente para o site [No More Ransom!](https://www.nomoreransom.org/pt/index.html) e [Proteja-se de ransomware](ransomware.png).**
>
> Este diagnóstico irá ajudar a investigar atividade suspeita em seu computador. Se você estiver auxiliando uma pessoa remotamente pode tentar seguir os passos descritos nos links a seguir sobre como usar uma ferramenta de acesso remoto como o **_[TeamViewer](https://www.teamviewer.com/pt-br/)_**, ou pode pesquisar uma infraestrutura forense remota como o [Google Rapid Response (GRR)](https://github.com/google/grr) (em inglês). Leve em consideração que as condições de conectividade de rede de ambas as pessoas determinam a possibilidade deste tipo de procedimento.

Por favor, selecione seu sistema operacional:

##### Options

- [Estou usando um computador Windows](#windows-intro)
- [Estou usando um computador Mac](#mac-intro)

### windows-intro

#### Estou usando um computador Windows

> Você pode seguir este guia inicial para investigar atividades suspeitas em dispositivos Windows:
>
> - [Como fazer investigação em sistemas Windows ativos](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md) (em inglês)

Estas instruções ajudaram a identificar alguma atividade maliciosa?

##### Options

- [Sim, acredito que meu computador esteja infectado](#device-infected)
- [Não identifiquei atividade maliciosa em meu computador](#device-clean)

### mac-intro

#### Estou usando um computador Mac

> Para identificar uma potencial infecção em um computador **_Mac_**, os passos são os seguintes:
>
> 1. Verifique programas suspeitos iniciando automaticamente
> 2. Verifique processos suspeitos
> 3. Verifique extensões de kernel suspeitas
>
> O site [Objective-See ](https://objective-see.com) (em inglês) oferece diversas ferramentas livres que facilitam este processo:
>
> - [KnockKnock](https://objective-see.com/products/knockknock.html) (em inglês) pode ser usado para identificar todos os programas registrados para iniciar automaticamente.
> - [TaskExplorer](https://objective-see.com/products/taskexplorer.html) (em inglês) pode ser usado para verificar processos em execução e identificar aqueles que parecem se comportar de maneira suspeita (por exemplo os que não possuem assinatura de desenvolvedor, ou são marcados suspeitos no site VirusTotal).
> - [KextViewr](https://objective-see.com/products/kextviewr.html) (em inglês) pode ser usado para identificar qualquer extensão de kernel (que atuam basicamente como os drivers do sistema no Windows ou módulos no Linux e Android) que possam ser suspeitas no funcionamento do **_Mac_**.
>
> Em caso de nenhuma delas revelar nada imediatamente suspeito e você desejar fazer uma análise posterior mais elaborada, você pode usar a ferramenta **_[pcqf](https://github.com/botherder/pcqf). pcqf_** é um utilitário que simplifica a coleta de informação do sistema e tira uma imagem completa da memória do sistema.
>
> Uma ferramenta adiciona que pode ser útil para coletar detalhes mais imersivos (mas que requer familiaridade com terminal de linha de comando) é a [AutoMacTC](https://www.crowdstrike.com/blog/automating-mac-forensic-triage/) (em inglês), da empresa de cibersegurança estadunidense CrowdStrike.

Estas instruções ajudaram a identificar alguma atividade maliciosa?

##### Options

- [Sim, acredito que meu computador esteja infectado](#device-infected)
- [Não identifiquei atividade maliciosa em meu computador](#device-clean)

### device-infected

#### Sim, acredito que meu computador esteja infectado

Ih, azedou! **_Seu dispositivo parece estar infectado_**. Pra se livrar da infecção você precisará:

##### Options

- [Buscar ajuda adicional](#malware_end)
- [Seguir com a formatação do aparelho](#reset).

### reset

#### Seguir com a formatação do aparelho

> Você pode optar por resetar ou formatar seu dispositivo como uma medida cautelar extraordinária. Os guias abaixo irão trazer instruções apropriadas para seu tipo de dispositivo:
>
> - **_[Android](https://tecnoblog.net/215758/formatar-resetar-android/)_**
> - [iOS](https://support.apple.com/pt-br/HT201252)
> - **_[Windows](https://support.microsoft.com/pt-br/help/4000735/windows-10-reinstall)_**
> - [Mac](https://support.apple.com/pt-br/HT201314)

Sente que irá precisar de apoio adicional?

##### Options

- [Sim](#malware_end)
- [Não](#resolved_end)

### malware_end

#### Sim, sinto que preciso de ajuda adicional

Se você precisa de informação adicional ao lidar com um dispositivo infectado, pode contatar uma das organizações listadas abaixo.

[orgs](:organisations?services=vulnerabilities_malware)

### resolved_end

#### Não, sinto que não preciso de ajuda adicional

Esperamos que o Kit de Primeiros Socorros Digitais tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

### Final Tips

Algumas dicas para se prevenir contra os esforços de invasores em comprometer seus dados e dispositivos:

- Verifique com atenção dobrada a legitimidade de qualquer e-mail que receber, de arquivos que tiverem sido baixados ou links que exijam seu login e informações de contas
- Leia mais sobre como proteger seus dispositivos de infecções de malware nos guias listados em recursos

### Resources

- [Evitar e denunciar ataques de phishing"](https://support.google.com/websearch/answer/106318?hl=pt)
- [Dicas para prevenção de phishing](https://www.kaspersky.com.br/resource-center/preemptive-safety/phishing-prevention-tips)
- [O que é um malware e como se proteger](https://www.kaspersky.com.br/resource-center/preemptive-safety/what-is-malware-and-how-to-protect-against-it)
- [Autodefesa contra Vigilância: Como evitar ataques de Pesca (Phishing)](https://ssd.eff.org/pt-br/module/como-evitar-ataques-de-pesca-phishing)
- [Avast: Guia Essencial sobre Phishing](https://www.avast.com/pt-br/c-phishing)
