---
layout: topic
title: "Alguém está se passando por mim online"
author: Flo Pagano, Alexandra Hache
language: pt
summary: "Uma pessoa está tendo sua identidade roubada através de uma conta de mídia social, endereço de email, chave PGP, site ou blog falso ou app"
date: 2023-04
permalink: topics/impersonated
parent: /pt/
order: 6
---

# Outra pessoa está se passando por mim online

Uma ameaça enfrentada por muitas pessoas ativistas, defensores de direitos humanos, ONGs e profissionais de mídia e conteúdo independente, é ter a identidade roubada por adversários que criam perfis falsos, sites ou e-mails em seus nomes. O objetivo dessas ações geralmente é difamar, disseminar notícias falsas, realizar práticas de engenharia social, roubo de identidade ou simplesmente gerar ruído, desconfiança e violação de dados que afetam a reputação das pessoas e grupos envolvidos. Em outras situações, estes adversários podem roubar a identidade digital de alguém por motivações financeiras, como criar campanhas falsas de financiamento coletivo, roubar credenciais de serviços de pagamento e cartões, se passar pela pessoa para receber dinheiro de contatos dela etc.

Esse problema pode ter diversos impactos emocionais e suas consequências podem variar dependendo de como afeta as comunicações e em quais meios as identidades estão sendo roubadas.

É importante ter em mente que existem muitas maneiras de alguém se passar por outra pessoa (perfis falsos nas redes sociais, sites clonados, e-mails falsificados, publicação não-consensual de imagens pessoais e vídeos). As estratégias podem variar desde o envio de ordens de remoção do site, disputa de propriedade do domínio, a reivindicação de direitos autorais do site ou das informações postadas, até ameaças às suas redes e contatos pessoais através de mensagens públicas ou confidenciais. Diagnosticar o problema e encontrar possíveis soluções para personificação pode ser complicado. Em certos casos será quase impossível convencer uma pequena empresa de hospedagem a derrubar um site, e mover uma ação legal pode se tornar necessário. É uma boa prática configurar alertas e monitorar a internet para descobrir se você ou sua organização estão sendo personificados.

Esta seção do Kit de Primeiros Socorros Digitais irá te guiar através de alguns passos básicos para diagnosticar potenciais formas de personificação e potenciais estratégias de mitigação para remover contas, sites e emails que se passam por você ou sua organização.

Se alguém está tomando sua identidade online, siga o questionário para identificar a natureza do problema e tentar encontrar possíveis soluções.

## Workflow

### urgent-question

#### Você teme por sua integridade física e bem estar?

##### Options

- [Sim](#physical-sec_end)
- [Não](#diagnostic-start1)

### diagnostic-start1

#### Não, não temo pela minha integridade física ou bem-estar

A personificação está te afetando como indivíduo (alguém está usando seu nome legal, social ou notório) ou como coletivo/organização?

##### Options

- [Como indivíduo](#individual)
- [Como organização](#organization)

### individual

#### Como indivíduo

Se estão afetando você como indivíduo, considere alertar seus contatos. Faça isso usando uma conta de email, perfil ou site que esteja sob seu total controle.

##### Options

- [Uma vez que você informe seus contatos que estão se pasando por você, siga para o próximo passo](#diagnostic-start2).

### organization

#### Como organização

> Se estão afetando você como grupo, é importante fazer uma anúncio público. Faça isso usando uma conta de email, perfil ou site que esteja sob seu total controle.

##### Options

- [Uma vez que tenha informado sua comunidade que estão se pasando por você, siga para o próximo passo](#diagnostic-start2).

### diagnostic-start2

#### De que forma estão se passando por você?

##### Options

- [Um site fake está usando meu nome ou o da minha organização/coletivo](#fake-website)
- [Através de uma conta de rede social](#social-network)
- [Compartilhando vídeos e imagens minhas sem consentimento](#other-website)
- [Através do meu email ou endereço similar](#spoofed-email1)
- [Através de uma chave PGP conectada ao meu email](#PGP)
- [Através de um app falso que imita meu app](#app1)

### social-network

#### Através de uma conta de rede social

Em qual plataforma de mídia social estão se passando por você?

##### Options

- [Facebook](#facebook)
- [Twitter](#twitter)
- [Google](#google)
- [Instagram](#instagram)

### facebook

#### Facebook

> Siga as instruções em ["Como faço para denunciar uma conta ou Página do Facebook que está fingindo ser eu ou outra pessoa?"](https://www.facebook.com/help/174210519303259) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

##### Options

- [Sim](#resolved_end)
- [Não](#account_end)

### twitter

#### Twitter

> Siga as instruções em ["Denunciar uma conta por falsa identidade"](https://help.twitter.com/forms/impersonation) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

##### Options

- [Sim](#resolved_end)
- [Não](#account_end)

### google

#### Google

> Siga as instruções em [Denunciar falsificação de identidade](https://support.google.com/plus/troubleshooter/1715140) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

##### Options

- [Sim](#resolved_end)
- [Não](#account_end)

### instagram

#### Instagram

> Siga as instruções em ["Contas falsas"](https://help.instagram.com/446663175382270) para solicitar que a conta falsa seja deletada.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

A solicitação funcionou para você?

##### Options

- [Sim](#resolved_end)
- [Não](#account_end)

### fake-website

#### Um site fake está usando meu nome ou o da minha organização/coletivo

> Verifique se o site já é reconhecido como malicioso buscando a URL nos serviços a seguir:
>
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)

O domínio é reconhecido como malicioso?

##### Options

- [Sim](#malicious-website)
- [Não](#non-malicious-website)

### malicious-website

#### O domínio é conhecido por ser malicioso

> Denuncie ao Google Safe Browsing o link da página maliciosa preenchendo o formulário ["Report malicious software" (em inglês)](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Note que pode levar algum tempo para garantir que sua denúncia teve sucesso. Neste tempo, você pode seguir para os próximos passos enviando uma ordem de remoção para os serviços de hospedagem e domínio, ou se preferir apenas salve este procedimento nos seus favoritos para que possa retornar e nos dizer se funcionou ou se precisa de outra solução.

O procedimento funcionou para você?

##### Options

- [Sim](#resolved_end)
- [Não](#non-malicious-website)

### non-malicious-website

#### O domínio não é conhecido por ser malicioso

> Você pode tentar denunciar o site para os serviços de hospedagem e domínio solicitando sua retirada.
>
> Se o site que você precisa denunciar está usando o seu conteúdo, uma coisa que você deverá provar é a propriedade do conteúdo postado. Você pode comprovar isso apresentando o contrato original com o serviço de registro de domínio ou de hospedagem, mas também prover resultados de arquivo da [Wayback Machine](https://archive.org/web/), buscando pela URL de ambos o site original em sua propriedade e o site falso. Se os sites foram indexados lá em algum momento, você terá um histórico que fará possível comprovar a existência de seu site antes da publicação do site falso.
>
> Para enviar uma solicitação de remoção do conteúdo, você precisará coletar a seguinte informação sobre o site falso:
>
> - Entre no serviço [Network Tools' NSLookup](https://network-tools.com/nslookup/) e insira o link da página falsa no campo "Host" do formulário para encontrar os endereços IP ligados a ela.
> - Anote os endereços IP.
> - Entre no serviço [Domain Tools' Whois Lookup](https://whois.domaintools.com/) e faça uma busca usando o link da página e, a seguir, pelos endereços IP do site falso.
> - Guarde o nome e endereço de email de notificação dos serviços de hospedagem e domínio. Caso esteja incluso nos resultados, guarde o nome do dono do site.
> - Escreva para ambos os serviços de hospedagem e domínio do site falso, solicitando a remoção. Em sua mensagem, inclua as informações coletadas, bem como os motivos pelos quais a existência deste site é abusiva.
> - Você pode usar este [modelo de denúncia de sites clonados fornecido pela Access Now Helpline (em inglês, com possibilidade de tradução)](https://accessnowhelpline.gitlab.io/community-documentation/352-Report_Fake_Domain_Hosting_Provider.html) para enviar ao serviço de hospedagem.
> - Você pode usar este [modelo de denúncia de domínios clonados ou forjados, fornecido pela Access Now Helpline (em inglês, com possibilidade de tradução)](https://accessnowhelpline.gitlab.io/community-documentation/343-Report_Domain_Impersonation_Cloning.html) para enviar ao serviço de registro de domínios.
>
> Note que esta solicitação pode levar algum tempo até ser respondida. Salve este procedimento nos seus favoritos para que possa retornar e nos dizer se os procedimentos funcionaram.

Estes procedimentos funcionaram para você?

##### Options

- [Sim](#resolved_end)
- [Não](#web-protection_end)

### spoofed-email1

#### Através do meu endereço de e-mail ou endereço semelhante

> Autenticar e-mails é uma tarefa tecnicamente complicada por uma questão de engenharia. Por conta desta dificuldade, é fácil abusar dos protocolos existentes e criar endereços forjados e mensagens iscas.

Alguém tomou para si o endereço de email que você usa frequentemente, ou estão usando um similar, por exemplo com o mesmo nome (escrito igual) mas com outro domínio (outro endereço/@)?

##### Options

- [Tomaram o endereço de email que eu uso frequentemente](#spoofed-email2)
- [Estão usando um endereço similar para se passar por mim](#similar-email)

### spoofed-email2

#### Tomaram o endereço de email que eu uso frequentemente

> A pessoa que está se passando por você pode ter invadido sua conta de email. Para descartar esta possibilidade, troque imediatamente sua senha.

Você conseguiu trocar a sua senha?

##### Options

- [Sim](#spoofed-email3)
- [Não](#hacked-account)

### hacked-account

#### Se você não conseguiu trocar a sua senha, sua conta de email provavelmente foi comprometida.

##### Options

- [Você poderá resolver seu problema seguindo o fluxo "Não consigo acessar minhas contas"](/pt/topics/account-access-issues).

### spoofed-email3

#### Consegui alterar minha senha

> Fraude de emails (spoofing) é uma prática que consiste em forjar um endereço de envio. A intenção é fazer parecer que a mensagem foi enviada por um remetente confiável, disfarçando sua origem falsa.
>
> Esta técnica é comum em campanhas de spam e golpes digitais (phishing), pelo fato de as pessoas estarem mais suscetíveis a abrir um e-mail quando imaginam ter vindo de uma fonte legítima.
>
> Se alguém está fraudando seu email, você deve informar seus contatos para alertá-los sobre o risco de serem atraídos para um phishing (faça isso através de outra conta de email confiável, perfil, ou site que estejam sob seu total controle).
>
> Se você acredita que a personificação da sua identidade foi focada em usos maliciosos e phishing, você pode querer ler também a seção [Recebi mensagens suspeitas](/pt/topics/suspicious_messages).

Os emails pararam após trocar a senha da sua conta?

##### Options

- [Sim](#compromised-account)
- [Não](#secure-comms_end)

### compromised-account

#### Os e-mails pararam depois que mudei minha senha

> Provavelmente sua conta foi invadida por alguém que está enviando emails para se passar por você. Como sua conta foi comprometida, você pode querer ler também a seção [Perdi o acessso às minhas contas](/pt/topics/account-access-issues/).

Estas informações ajudaram a resolver o seu problema?

##### Options

- [Sim](#resolved_end)
- [Não](#account_end)

### similar-email

#### Estão usando um endereço similar para se passar por mim

> Se estão se passando por você através de um endereço de email similar ao seu mas de um domínio diferente ou com outro nome de usuário modificado, é uma boa ideia alertar seus contatos sobre a tentativa de fraude (faça isso através de outra conta de email confiável, perfil, ou site que estejam sob seu total controle).
>
> Você também pode querer ler a seção [Recebi mensagens suspeitas](/pt/topics/suspicious-messages), uma vez que esta personificação pode ser uma tentativa de fisgar contatos para um golpe (phishing).

Estas informações ajudaram a resolver o seu problema?

##### Options

- [Sim](#resolved_end)
- [Não](#secure-comms_end)

### PGP

#### Através de uma chave PGP conectada ao meu email

Você acredita que sua chave PGP privada possa ter sido comprometida, por exemplo por perda ou acesso indevido ao dispositivo onde estava armazenada?

##### Options

- [Sim](#PGP-compromised)
- [Não](#PGP-spoofed)

### PGP-compromised

#### Minha chave PGP pode ter sido comprometida

Você ainda possui acesso à sua chave privada?

##### Options

- [Sim](#access-to-PGP)
- [Não](#lost-PGP)

### access-to-PGP

#### Acesso à chave PGP

> - Revogue sua chave.
>   - [Instruções para o Enigmail (em inglês)](https://www.enigmail.net/index.php/en/user-manual/key-management#Revoking_your_key_pair)
> - Crie um novo par de chaves e peça a pessoas que você confia para assiná-la.
> - Utilizando um canal de comunicação confiável, como o Signal ou outra [ferramenta com criptografia ponta-a-ponta](https://www.frontlinedefenders.org/pt/resource-publication/guide-secure-group-chat-and-conferencing-tools), informe seus contatos que você revogou a sua chave atual e gerou uma nova.

Você necessita de maior apoio para resolver este problema?

##### Options

- [Sim](#secure-comms_end)
- [Não](#resolved_end)

### lost-PGP

#### Chave PGP perdida

Você tem um certificado de revogação?

##### Options

- [Sim](#access-to-PGP)
- [Não](#no-revocation-cert)

### no-revocation-cert

#### Sem certificado de revogação

> - Crie um novo par de chaves e peça a pessoas que você confia que assinem-a.
> - Informe seus contatos utilizando um canal de comunicação confiável, como o Signal ou outra [ferramenta com criptografia ponta-a-ponta](https://www.frontlinedefenders.org/pt/resource-publication/guide-secure-group-chat-and-conferencing-tools), para que deixem de utilizar sua chave antiga e passem a usar a nova chave.

Você necessita de maior apoio para resolver este problema?

##### Options

- [Sim](#secure-comms_end)
- [Não](#resolved_end)

### PGP-spoofed

#### Minha chave PGP pode ter sido falsificada

Sua chave está assinada por pessoas de sua confiança?

##### Options

- [Sim](#signed-key)
- [Não](#non-signed-key)

### signed-key

#### Chave assinada

> Informe seus contatos utilizando um canal de comunicação confiável, como o Signal ou outra [ferramenta com criptografia ponta-a-ponta](https://www.frontlinedefenders.org/pt/resource-publication/guide-secure-group-chat-and-conferencing-tools), que alguém está tentando se passar por você. Informe seus contatos que é possível confirmar a veracidade de sua chave (1) conferindo as assinaturas de chave feitas por pessoas próximas de sua confiança, ou (2) através do [fingerprint](http://g1.globo.com/Noticias/Tecnologia/0,,MUL982184-6174,00-SAIBA+COMO+FUNCIONA+A+CRIPTOGRAFIA+DE+DADOS+NOS+EMAILS.html) da sua chave.

Você necessita de maior apoio para resolver este problema?

##### Options

- [Sim](#secure-comms_end)
- [Não](#resolved_end)

### non-signed-key

#### Chave não assinada

> - Tenha sua chave [assinada](https://communitydocs.accessnow.org/243-PGP_keysigning.html#comments) por pessoas que você confia.
> - Informe seus contatos utilizando um canal de comunicação confiável, como o Signal ou outra [ferramenta com criptografia ponta-a-ponta](https://www.frontlinedefenders.org/pt/resource-publication/guide-secure-group-chat-and-conferencing-tools), que alguém está tentando se passar por você. Informe seus contatos que é possível confirmar a veracidade de sua chave (1) conferindo as assinaturas de chave feitas por pessoas próximas de sua confiança, ou (2) através do [fingerprint](http://g1.globo.com/Noticias/Tecnologia/0,,MUL982184-6174,00-SAIBA+COMO+FUNCIONA+A+CRIPTOGRAFIA+DE+DADOS+NOS+EMAILS.html) da sua chave.

Você necessita de maior apoio para resolver este problema?

##### Options

- [Sim](#secure-comms_end)
- [Não](#resolved_end)

### other-website

#### Compartilhando vídeos e imagens minhas sem consentimento

> Se alguém está usando sua identidade em um site, a primeira coisa que você precisa entender é onde este site está hospedado, quem está gerenciando, e quem está provendo o domínio. Tal pesquisa se destina a identificar a melhor forma de solicitar a remoção do conteúdo malicioso.
>
> Antes de prosseguir com a investigação, caso possua cidadania da União Européia, você pode requerer ao Google a remoção deste site de buscas relacionadas a seu nome.

Você possui cidadania da União Européia?

##### Options

- [Sim](#EU-privacy-removal)
- [Não](#doxing-question)

### EU-privacy-removal

#### Remoção de privacidade na UE

> Preencha o formulário do Google [Personal Information Removal Request Form](https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=rtbf&hl=en&rd=1) para remover o site das buscas relacionadas ao seu nome feitas no Google.
>
> Você irá precisar de:
>
> - Uma cópia digital de um documento de identificação (se estiver enviando a solicitação em intermédio de outra pessoa, precisará enviar o documento de identificação desta pessoa)
> - As URLs dos conteúdos que deseja que sejam removidos das buscas
> - Para cada URL fornecida, você deve explicar:
>   1. como a informação pessoal contida se relaciona com a pessoa que está solicitando a remoção
>   2. porque você acredita que tal conteúdo deve ser removido
>
> Note que será necessário autenticar com sua Conta Google, e o envio do formulário será associado a ela.
>
> Após submeter o formulário, você deverá aguardar a resposta do Google para verificar se os resultados foram devidamente removidos.

Você gostaria de submeter também uma solicitação de remoção do site com informações falsas sobre você?

##### Options

- [Sim](#doxing-question)
- [Não, preciso de outras formas de apoio](#account_end)

### doxing-question

#### A pessoa que está se passando por você publicou informações pessoais, ou fotos e videos íntimos sobre você?

##### Options

- [Sim](/pt/topics/doxing)
- [Não](#fake-website)

### app1

#### Através de um app falso que imita meu app

> Se alguém está espalhando uma cópia de aplicativo modificada ou um app malicioso similar ao seu, é uma boa ideia fazer um anúncio público e alertar usuários sobre estas modificações e os meios certificados e seguros de baixar e instalar seu aplicativo oficial.
>
> Também é essencial denunciar os apps maliciosos e solicitar a remoção deles.

Em quais meios as cópias maliciosas do seu app estão sendo distribuídas?

##### Options

- [no Github](#github)
- [no Gitlab.com](#gitlab)
- [na Google Play Store](#playstore)
- [na Apple App Store](#apple-store)
- [em outro site](#fake-website)

### github

#### no Github

> Se o software malicioso estiver hospedado no Github, leia [Guia de envio do aviso de retirada DMCA](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) para informações sobre como derrubar conteúdos que violem direitos autorais.
>
> É possível que leve algum tempo até que sua solicitação seja respondida. Salve esta página nos seus favoritos e retorne ao fluxo em alguns dias, após obter a resposta.

A solicitação funcionou para você?

##### Options

- [Sim](#resolved_end)
- [Não](#app_end)

### gitlab

#### no Gitlab

> Se o software malicioso estiver hospedado no Gitlab, leia [Digital Millennium Copyright Act (DMCA) takedown request requirements](https://about.gitlab.com/handbook/dmca/) para informações sobre como derrubar conteúdos que violem direitos autorais.
>
> É possível que leve algum tempo até que sua solicitação seja respondida. Salve esta página nos seus favoritos e retorne ao fluxo em alguns dias, após obter a resposta.

A solicitação funcionou para você?

##### Options

- [Sim](#resolved_end)
- [Não](#app_end)

### playstore

#### no Google play store

> Se o software malicioso estiver hospedado na Google Play Store, siga as instruções em ["Remoção de conteúdo do Google"](https://support.google.com/legal/troubleshooter/1114905) para informações sobre como derrubar conteúdos que violem direitos autorais.
>
> É possível que leve algum tempo até que sua solicitação seja respondida. Salve esta página nos seus favoritos e retorne ao fluxo em alguns dias, após obter a resposta.

A solicitação funcionou para você?

##### Options

- [Sim](#resolved_end)
- [Não](#app_end)

### apple-store

#### no Apple store

> Se o software malicioso estiver hospedado na Apple App Store, siga as instruções em ["Apple App Store Content Dispute" form](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts?lang=pt) para informações sobre como derrubar conteúdos que violem direitos autorais.
>
> É possível que leve algum tempo até que sua solicitação seja respondida. Salve esta página nos seus favoritos e retorne ao fluxo em alguns dias, após obter a resposta.

A solicitação funcionou para você?

##### Options

- [Sim](#resolved_end)
- [Não](#app_end)

### physical-sec_end

#### Temo pela minha integridade física e bem-estar

> Se você está tendo sua integridade e bem estar físicos ameaçados, por gentileza contate as organizações a seguir que poderão auxiliar melhor nesta situação.

[orgs](:organisations?services=physical_sec)

### account_end

#### Ainda estou enfrentando falsificação de identidade em minhas contas

> Se você ainda está tendo suas contas e identidades comprometidas, por gentileza contate as organizações a seguir que poderão apoiar com esta situação.

[orgs](:organisations?services=account&services=legal)

### app_end

#### Eu gostaria de mais suporte com aplicativos falsos

> Se o app falso não foi removido, por gentileza contate as organizações a seguir que poderão apoiar com esta situação.

[orgs](:organisations?services=account&services=legal)

### web-protection_end

#### Gostaria de mais suporte com solicitações de remoção

> Se as solicitações de remoção não foram bem sucedidas, por gentileza contate as organizações a seguir que poderão auxiliar melhor nesta situação.

[orgs](:organisations?services=web_protection)

### secure-comms_end

#### Gostaria de mais suporte com comunicações seguras

> Se você precisa de apoio ou recomendações adicionais sobre _phishing_, segurança de email, criptografia, e comunicações seguras como um todo, você pode entrar em contato com uma das organizações a seguir.

[orgs](:organisations?services=secure_comms)

### resolved_end

#### Meu problema foi resolvido

Esperamos que o Kit de Primeiros Socorros Digitais tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

Para prevenier futuras tentativas de fraude da sua identidade, leia as dicas abaixo.

### Final Tips

- Crie senhas fortes: prefira usar uma frase com palavras aleatórias, que não digam respeito a algo pessoal, usando espaços e caracteres especiais fáceis de lembrar. Mais dicas nos recursos.
- Considere usar um gerenciador de senhas para criar e guardar suas novas senhas, assim você pode manter senhas diferentes para cada site e serviço que usar sem ter que memorizar tudo ou anotar no papel.
- Configure a autenticação em dois fatores em todas as suas contas. A autenticação em dois fatores pode ser muito efetiva para barrar alguém de acessar suas contas sem a sua permissão. Se você puder escolher, não use a autenticação baseada em SMS, prefira usar opções baseadas em aplicativos ou em uma chave de segurança.
- Faça o processo de verificação de conta em suas plataformas de rede social. Algumas plataformas oferecem a possibilidade de verificar sua identidade e sinalizar sua conta de acordo.
- Mapeie sua presença online. Use a inteligência de código aberto disponível na internet para buscar informações disponíveis sobre você e interprete de forma estratégica para conseguir prevenir atores maliciosos de usarem estas informações para se passar por você.
- Configure alertas do Google. Você pode receber emails quando novos resultados para um tópico aparecerem na busca do Google. Por exemplo, você pode receber informação sobre menções do seu nome ou da sua organização/coletivo.
- Salve e tire prints da condição atual do seu site, para que possam ser utilizados como evidência mais adiante. Se o seu site permite robôs de classificação (crawlers), você pode utilizar a Wayback Machine, oferecida pelo archive.org Visite a [Internet Archive Wayback Machine](https://archive.org/web/), insira o link da sua página (URL) no campo de pesquisa abaixo do banner "Save page now", e depois disso clique no botão "Save page now".

### Resources

- [Gerenciador de Senhas: Saiba quais as 5 melhores ferramentas (recomendamos usar modo leitura para melhorar a experiência do site)](https://geekblog.com.br/gerenciador-de-senhas-saiba-quais-as-5-melhores-ferramentas/)
- [Segurança e a reciclagem de números: por que você nunca deve depender do SMS](https://olhardigital.com.br/2020/11/19/noticias/seguranca-e-a-reciclagem-de-numeros-por-que-voce-nunca-deve-depender-do-sms/)
- [Autodefesa contra Vigilância: Criando senhas fortes](https://ssd.eff.org/pt-br/module/criando-senhas-fortes)
- [Autodefesa contra Vigilância: Como utilizar o KeePassXC](https://ssd.eff.org/pt-br/module/como-utilizar-o-keepassxc)
- [Access Now: Two-factor authentication (2FA)](https://www.accessnow.org/need-talk-sms-based-two-step-authentication/)
- [Access Now Helpline Community Documentation: Choosing a password manager](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Archive.org: Archive your website](https://archive.org/web/)
