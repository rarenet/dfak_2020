---
layout: topic
title: "Acho que estão me vigiando"
author: Carlos Guerra, Peter Steudtner
language: pt
summary: "Se você acredita que pode ser alvo de vigilancia digital, esse questionário vai te guiar através de questões para identificar possiveis indicadores de vigilância."
date: 2023-05
permalink: topics/surveilled
parent: Home
order: 11
---

# Acho que estão me vigiando

Recentemente, temos nos deparado com um crescente surgimento de casos de vigilância contra a sociedade civil, que vão desde o uso de stalkerware (aplicações maliciosas de vigilância) no campo corporativo, para espionar funcionáries, até vigilância em massa e direcionada contra ativistas e jornalistas. Dada a amplitude e diversidade de técnicas e precedentes observadas nesse tipo de ataque, torna-se impossível cobrir todos os aspectos da vigilância, especialmente para um apoio de emergência.

Dito isso, este tópico está focado em casos comuns observados na esfera digital, relacionados a dispositivos e contas dos usuáries-finais, que representam emergências nas quais uma resposta é possível. Quando não houver uma solução viável, não vamos considerar a situação uma emergência e não vamos nos aprofundar nela. Mas, para esses casos, vamos compartilhar referências úteis.

Alguns casos não cobertos por este tópico:

- Vigilância física. Se você teme estar sendo alvo de uma vigilância física e precisa de apoio, acione organizações focadas em segurança física, aqui na [página de suporte do Kit de Primeiros Socorros Digitais](/pt/support).
- Vigilância em massa com hardwares do tipo CCTV com câmeras em público, em ambientes corporativos e privados.
- Hardwares maliciosos que não dependem de conexão com internet.

Também é importante destacar que, atualmente, todes estamos sendo vigiades de alguma forma, em diferentes níveis:

- A infraestrutura de telefones celulares é desenhada e configurada para coletar uma imensa quantidade de dados sobre nós. Dados que podem ser utilizados por diversos agentes para descobrir detalhes como locais, rede de contatos, etc.
- Serviços online como redes sociais e provedores de e-mail também coletam uma grande quantidade de dados que podem ser utilizados para a segmentação de campanhas de anúncios relacionados aos nossos posts, ou até mesmo mensagens diretas.
- Cada vez mais países tem adotado sistemas que usam nossa identidade e coletam informações através de nossas interações com serviços e instituições públicas como documentos pessoais, nossas interações com a Receita, nossa relação com o SUS, etc.

Esses exemplos de vigilância em massa, além de outros diversos precedentes de operações mais direcionadas, criam um senso de que todes (principalmente no campo ativista) estamos sendo vigiades, o que pode levar à paranoia e afetar nossa capacidade emocional de lidar com ameaças reais. Dito isso, existem casos mais específicos que, dependendo do nosso perfil de risco, do trabalho que realizamos, e as capacidades de potenciais adversaries, podemos suspeitar que estamos sendo vigiades. Se esse é seu caso, continue para os próximos passos.

Um terceiro aviso! Muitos esquemas de vigilância (especialmente aqueles mais direcionados), geralmente deixam poucos em nenhum rastro a mostra para vítimes, tornando a detecção muito difícil ou até mesmo impossível. Não se esqueça disso enquanto estiver navegando por esse fluxo de trabalho, e se ainda acredita que seu caso não está contemplado ou que seja mais complexo do que isso, entre em contato com as organizações listadas no final.

Consideração final: lembre-se de que esforços de vigilância podem ser massivos ou direcionados. Operações de vigilância massiva normalmente abraçam populações inteiras ou grande parte delas, como todos os usuáries de uma plataforma, todas as pessoas de uma determinada região, etc. Estes estão mais relacionados à tecnologia e são menos perigosos do que uma vigilância direcionada, que mira uma ou um número menor de pessoas, que demanda um maior investimento (dinheiro, tempo e/ou capacidade). A vigilância direcionada é mais perigosa do que a vigilância em massa, dadas as motivações por trás de tanto investimento para monitorar as atividades e comunicações de pessoas específicas.

Encontrou algum indicativo de que está sendo vigiade? Clique no começar para iniciar esse questionário.

## Workflow

### start

#### Que tipo de experiência você está enfrentando?

> Se você está aqui, provavelmente tem motivos para achar que suas comunicações, suas localizações ou outras atividades online estão sendo vigiadas. Abaixo você encontrará uma série de indicativos comuns de atividades suspeitas relacionadas à vigilância. Para continuar, clique no caso com o qual mais se identifica.

Dadas as informações da introdução, que tipo de experiência você está enfrentando?

##### Options

- [Achei um dispositivo de rastreamento perto de mim ](#tracking-device-intro)
- [Meu dispositivo está agindo de forma suspeita ](#device-behaviour-intro)
- [Estou sendo redirecionade para sites http (desprotegidos) quando instalo atualizações ou aplicativos](#suspicious-redirections-intro)
- [Estou recebendo mensagens de segurança de serviços que confio](#security-alerts-intro)
- [Achei um dispositivo suspeito conectado ao meu computador ou rede](#suspicious-device-intro)
- [Informações que compartilhei de forma privada por e-mail, mensagens ou similares estão sendo descobertas](#leaked-internet-info-intro)
- [Informações confidenciais que compartilhei por chamadas de telefone ou SMS foram vazadas (especialmente durante eventos sesíveis) ](#leaked-phone-info-intro)
- [As pessoas à minha volta foram alvo de medidas de vigilância bem sucedidas](#peer-compromised-intro)
- [Alguma coisa desse tipo / eu não sei](#other-intro)

### tracking-device-intro

#### Achei um dispositivo de rastreamento perto de mim

> Se encontrou algum dispositivo inesperado próximo de você, e acredita que seja de vigilância, o primeiro passo é confirmar essa suspeita de monitoramento. Com a popularização dos dispositivos relacionados à "internet das coisas", está cada vez mais comum encontrarmos dispositivos com uma variedade grande de funções que podemos acabar esquecendo ao utilizá-los por algum tempo. Um dos primeiros passos para termos certeza que aquele é um dispositivo de vigilância é checar informações sobre sua marca, modelo, etc. Alguns dos dispositivos de vigilância mais comuns disponíveis a preços acessíveis no mercado são Airtags da Apple, Samsung Galaxy SmartTags ou rastreadores Tile. Esses dispositivos são vendidos com objetivos legítimos como rastrear objetos pessoais, mas agentes de má-fé podem se apropriar deles para rastrear outras pessoas sem permissão.
>
> Para checar se está sendo monitorade por um rastreador inesperado, além de encontrá-lo fisicamente, é importante considerar essas medidas:
>
> - Para Airtags da Apple, [normalmente você recebe no iOS uma notificação automática quando o dispositivo desconhecido for detectado por algum tempo.](https://support.apple.com/en-us/HT212227).
> - Para o Android, há um [aplicativo para checar Airtags por perto.](https://play.google.com/store/apps/details?id=com.apple.trackerdetect).
> - Para dispositivos Tile, o [app Tile](https://www.tile.com/download) tem um recurso para detectar rastreadores desconhecidos.
> - Para o Samsung Galaxy SmartTags, você pode usar o [app SmartThings](https://www.samsung.com/us/smartthings/) para fazer o mesmo.
>
> Outro tipo de dispositivo que você pode encontrar são os rastreadores GPS. Eles registram com frequência a localização dos alvos e, em alguns casos, podem transmitir essas informações por linhas telefônicas, ou podem armazená-las em dispositivos físicos que podem ser resgatados mais tarde e destinados para análise. Se encontrou um dispositivo como esses, entender como ele opera é a chave para ter uma melhor noção de quem o plantou e como está obtendo as informações.
>
> Para qualquer tipo de dispositivo de vigilância que você encontre, documente tudo sobre ele: marca, modelo, se pode se conectar a redes wireless, onde exatamente você o encontrou, se seu nome está relacionado alguma aplicação relevante, etc.

Após identificar o dispositivo, existem algumas estratégias que você pode querer adotar. O que você quer fazer com o dispositivo?

##### Options

- [Quero desativá-lo ](#tracking-disable-device)
- [Quero utilizá-lo para despistar seu operador](#tracking-use-device)

### tracking-disable-device

#### Quero desativá-lo

> Dependendo do tipo e da marca do dispositivo que você encontrou, você deve ser capaz de desligá-lo ou desativá-lo por completo. Em alguns casos isso pode ser feito através das aplicações dos próprios dispositivos, aquelas que citamos anteriormente, mas também através por outros dispositivos que forçam o desligamento, ou até mesmo o danificando. Lembre-se de que se você deseja conduzir uma investigação mais a fundo sobre o incidente, se desativar o dispositivo, poderá perder evidências relevantes que podem estar armazenadas nele.

Deseja tomar outras medidas para identificar o responsável pelo dispositivo? (_para isso você deverá manter o dispositivo que está realizando o rastreio online todo o tempo do rastreio_)

##### Options

- [Sim](#identify-device-owner)
- [Não](#pre-closure)

### tracking-use-device

#### Quero utilizá-lo para despistar seu operador

> Uma estratégia comum para lidar com dispositivos de rastreamento é fazer com eles algo diferente do que lhe foi proposto. Por exemplo, você deixá-lo parado em algum lugar seguro enquanto você vai para outros lugares, ou até mesmo fazê-lo rastrear outras rotas diferentes da sua. Fazendo isso, é importante lembrar que você pode perdê-lo e não conseguir mais resgatá-lo, e assim ele estará rastreando outras pessoas ou lugares, o que pode gerar alguma implicação de segurança ou legal.

Deseja tomar outras medidas para identificar o responsável pelo dispositivo? (_para isso você deverá manter o dispositivo online todo o tempo do rastreio_)

##### Options

- [Sim](#identify-device-owner)
- [Não](#pre-closure)

### identify-device-owner

#### Quero identificar o proprietário do dispositivo

> Identificar o responsável pelo dispositivo de rastreamento pode ser difícil em alguns cenários, vai depender muito do tipo do dispositivo, então recomendamos que pesquise especificamente sobre o dispositivo que encontrou. Alguns exemplos:
>
> - Para rastreadores wireless acessíveis para consumo:
>   - Como eles aparecerem nos apps de checagem?
>   - Quando você recebeu uma notificação?
>   - Existe a possibilidade de extrair do dispositivo mais dados que contenham informações relevantes?
> - Para rastreadores mais dedicados, como GPS:
>   - Eles possuem armazenamento interno? Alguns modelos possuem cartões SD de memória que podem ser extraídos para podermos analisar quando começou a captura de dados ou qualquer outra informação relevante.
>   - Eles possuem linha telefônica? Você consegue extrair um cartão SIM e checar em outro telefone se consegue descobrir o número? Com essa informação, você consegue obter a identidade do dono daquela linha telefônica?
> - Em geral
>   - Procure pistas físicas do tipo nomes escritos no dispositivo, número de série, etc.
>   - Se o dispositivo tem conexão de rede, ele possui algum nome específico que pode identificá-lo?
>   - Quem tem acesso ao local onde você achou? Conhecendo o lugar, quando ele pode ter sido plantado?

Deseja tomar providências legais contra o proprietário do dispositivo?

##### Options

- [Sim](#legal-action-device-owner)
- [Não](#pre-closure)

### legal-action-device-owner

#### Quero tomar medidas legais contra o proprietário do dispositivo

> Caso deseje tomar alguma providência legal contra proprietárie do dispositivo de vigilância, você vai precisar checar como funciona esse processo na jurisdição do seu país. Em alguns casos vai precisar de advogade ou apenas comparecer a uma delegacia para iniciar o processo. O único conselho comum em todas as circunstâncias é que a força do seu caso é diretamente relacionada à quantidade de evidências que você possui para sustentá-lo. Um bom começo é seguir o conselho dado no passo anterior deste fluxo de trabalho, sobre o que documentar. Você encontra mais informações e exemplos no guia[ Documentando Ataques Digitais, aqui do Kit de Primeiros Socorros Digitais](/pt/documentation).

Precisa de ajuda para tomar providências legais contra o proprietário do dispositivo?

##### Options

- [Sim, preciso de apoio legal](#legal_end)
- [Não, acho que resolvi meus problemas](#resolved_end)
- [Não, mas gostaria de considerar outros cenários de vigilância](#pre-closure)

### device-behaviour-intro

#### Meu dispositivo está agindo de forma suspeita

> Uma das técnicas mais comuns de vigilância direcionada até hoje é a infecção de celulares ou computadores com _spywares_ - softawres maliciosos desenhados para monitorar e transmitir sua atividade para outras pessoas. Spywares podem ser plantados por diferentes agentes por diferentes métodos, como, por exemplo, parceires abusives que podem fazer download manualmente de uma aplicação espiã no dispositivo, ou o criminosos enviando links de phishing para que você baixe um app.
>
> Alguns exemplos de indicativos de vigilância são apps suspeitos que você encontra no seu celular com permissões de acesso ao microfone, câmera, acesso à internet, etc. Além disso, observe se o sinal de uso da webcam está aceso sem que nenhuma aplicação que pode operá-la está funcionando, e se foi vazado o conteúdo de arquivos do seu dispositivo.
>
> Nesses casos, você pode acessar o fluxo de trabalho [Meu dispositivo está com um comportamento estranho](/pt/topics/device-acting-suspiciously), aqui do Kit de Primeiros Socorros Digitais.

O que você gostaria de fazer?

##### Options

- [Me leve para o tópico "Meu dispositivo está com um comportamento estranho"](/pt/topics/device-acting-suspiciously)
- [Me leve para a próxima etapa](#pre-closure)

### suspicious-redirections-intro

#### Estou sendo redirecionade para sites http (desprotegidos) quando instalo atualizações ou aplicativos

> Mesmo que isso não seja comum, às vezes nos deparamos com alertas de segurança quando tentamos atualizar aplicações ou mesmo o sistema operacional de nossos dispositivos. Existem vários motivos para isso, e eles podem ser legítimos ou maliciosos. Essas são algumas questões que precisamos fazer para nós mesmes nesses casos:
>
> - Seu dispositivo está com a data e hora corretas? A forma com que nossos dispositivos confiam em servidores para operar com mais ou menos segurança depende da checagem de certificados de segurança válidos em determinados fuzos. Se, por exemplo, seu computador está configurado em um ano anterior, essas checagens de segurança vão falhar em vários websites e serviços. Se a sua data e hora estiverem corretas, pode significar que o provedor não atualizou seus certificados ou que algo suspeito está acontecendo. De qualquer forma, é uma boa prática nunca atualizar ou operar aplicações no seu dispositivo se aparecerem esses avisos de segurança ou redirecionamentos.
> - O problema apareceu depois de algum evento incomum, tipo clicar em algum anúncio, instalar algum aplicativo ou abrir algum documento? Muitos ataques acontecem através de processos que parecem legítimos como a instalação ou atualização de softwares, então isso pode ser seu dispositivo reconhecendo a possibilidade de um ataque, te avisando, ou até mesmo interrompendo todo o processo.
> - Você está percebendo que o processo está acontecendo de uma maneira diferente do que normalmente acontece?
>
> De toda forma, não se esqueça de documentar tudo, da maneira mais detalhada possível, especialmente se você deseja receber ajuda especializada, conduzir uma investigação mais aprofundada ou tomar qualquer providência legal.
>
> Em quase todos os casos, descobrir a raiz do problema vai ser o suficiente antes de retornar o dispositivo e a navegação para o normal. Apesar disso, futuramente podem acontecer outros problemas se você realmente instalou ou atualizou alguma aplicação ou sistema operacional em circunstâncias suspeitas.

Você clicou e fez o download, ou instalou, qualquer software ou atualizações em circunstâncias suspeitas?

##### Options

- [Sim](#suspicious-software-installed)
- [Não](#pre-closure)

### suspicious-software-installed

#### Baixei ou instalei software sob condições suspeitas

> Geralmente, ataques que sequestram instalações ou atualizações de aplicações, ou sistemas operacionais tem o objetivo de instalar malwares no sistema. Se está suspeitando que este é seu caso, recomendamos que acesse o tópico "Meu dispositivo está com um comportamento estranho", do Kit de Primeiros Socorros Digitais.

O que você gostaria de fazer?

##### Options

- [Me leve para o tópico "Meu dispositivo está com um comportamento estranho"](/pt/topics/device-acting-suspiciously)
- [Me leve para a próxima etapa](#pre-closure)

### security-alerts-intro

#### Estou recebendo mensagens de segurança de serviços que confio

> Se está recebendo mensagens ou notificações de seus serviços de e-mail, plataformas de redes sociais ou qualquer outro serviço online que você de fato utiliza, e eles estão acusando questões de segurança como tentativas de login em novos dispositivos ou lugares, você deve primeiro checar se a mensagem é verdadeira ou se pode ser uma tentativa de phishing. Para fazer isso, recomendamos que acesse o tópico ["Recebi uma Mensagem Suspeita"](/pt/topics/suspicious-messages), do Kit de Primeiros Socorros Digitais, antes de continuar.
>
> Se a mensagem for verdadeira, a próxima pergunta que você deveria se fazer é se há algum motivo para que isso esteja acontecendo de forma legítima. Por exemplo, se você utiliza algum serviço de VPN ou Tor e está navegando pela versão web do serviço, ele vai entender que você está localizado no país do seu servidor de VPN ou o nó de saída do Tor, acionando um alerta de novo login suspeito, quando, na verdade é você mesmo tentando acessar.
>
> Outra coisa interessante de se perguntar é que tipo de alerta é esse que está recebendo: alguém está tentando acessar sua conta, ou já conseguiu? Dependendo da resposta, você deverá agir diferente.
>
> Se tiver recebido uma notificação sobre uma tentativa de login que não deu certo, e tiver uma boa senha (longa, não compartilhada como outros serviços inseguros, etc.) e utiliza uma autenticação de dois fatores (também conhecida como MFA ou 2FA), não precisa se preocupar tanto. Independente da seriedade da ameaça, é sempre importante checar registros de atividades recentes, onde vai encontrar seu histórico de login, incluindo a localização e o tipo de dispositivo utilizado.
> Se deparou com algo suspeito nos registros de atividades? As estratégias mais comuns, dependendo da conta, são:
>
> - Mudar a senha
> - Ativar autenticação de dois fatores
> - Tentar desconectar todos os dispositivos da conta
> - Documentar os indicadores da tentativa de invasão (caso queira procurar ajuda, investigar mais a fundo ou iniciar um processo legal)
>
> Outra questão importante para ajudar a avaliar estas notificações é se você partilha dispositivos ou acesso à conta com outra(s) pessoa(s) que possa(m) ter acionado o alerta. Se for esse o caso, tenha cuidado com o nível de acesso que dá a outras pessoas às suas informações e repita os passos acima se quiser revogar o acesso dessa terceira pessoa.

Se acredita que está lidando com alguma das potenciais ameaças listadas no início deste questionário e queira checar, ou está passando por um caso ainda mais complexo e precisa de ajuda, clique em continuar.

##### Options

- [Próximo](#pre-closure)

### suspicious-device-intro

#### Achei um dispositivo suspeito conectado ao meu computador ou rede

> Ao encontrarmos um dispositivo suspeito, a primeira coisa que devemos nos perguntar é se ele é de fato malicioso ou se deveria mesmo estar ali? Isso por que ao longo dos anos a quantidade de dispositivos com capacidade de comunicação vem aumentando em nossas casas e locais de trabalho: impressoras, luzes inteligentes, lavadoras, termostatos, etc. Dada a quantidade de dispositivos, podemos acabar nos preocupando com algum que nem sequer lembramos de ter configurado, ou de ter sido configurado por outra pessoa, e que pode não ser malicioso.
>
> Se você não sabe se o dispositivo que encontrou é ou não malicioso, uma das primeiras coisas que precisa fazer é reunir informações sobre ele: marca, modelo, se está conectado à rede, seja à cabo ou wireless, se ele diz o que faz, se está ligado, etc. Se você ainda não sabe o que o dispositivo faz, pelo menos use essas informações para pesquisar na web e descobrir mais sobre ele.

Após avaliar o dispositivo, você acha que ele é legítimo?

##### Options

- [Acho que é legítimo ](#pre-closure)
- [Após checar, ainda não sei o que ele faz ou se é maligno](#suspicious-device2)

### suspicious-device2

#### Após checar, ainda não sei o que ele faz ou se é maligno

> Se depois de uma avaliação inicial, continua preocupade, considere a possibilidade remota de ser um dispositivo de vigilância. Normalmente esses dispositivos monitoram atividades na rede em que estão conectados, ou podem gravar e transmitir áudio e/ou vídeo, assim como um spyware. É importante que entenda que esse cenário é bastante incomum e ligado a perfis de alto risco. No entanto, se essa é uma preocupação, veja algumas medidas que pode tomar:
>
> - Desconectar dispositivo da rede ou da energia e checar se todos recursos e serviços a sua volta estão funcionando sem problemas. Depois disso, pode levar para ser analisado por alguém com essa experiência.
> - Pesquisar ainda mais sobre ele, procurando por ajuda (isso normalmente envolve testes mais avançados como mapeamento da rede ou a detecção de sinais estranhos).
> - Como uma medida de prevenção para mitigar qualquer vigilância através da rede se dispositivo permanecer ligado, utilize serviços de VPN, Tor, ou similares, que criptografam sua navegação na web, assim como ferramentas de comunicação com criptografia de ponta a ponta.
> - Também como medida preventiva, dessa vez para reduzir os riscos de gravação de áudio/vídeo se o dispositivo permanecer ligado, considere isolá-lo fisicamente.

Se acredita que há outras potenciais ameaças daquelas listadas anteriormente, e deseja checar ou procurar ajuda, clique em continuar.

##### Options

- [Continuar](#pre-closure)
- [Acho que resolvi meu problema ](#resolved_end)

### leaked-internet-info-intro

#### Informações que compartilhei de forma privada por e-mail, mensagens ou similares estão sendo descobertas

> Essa parte do tópico contemplará o vazamento de dados e atividades, provenientes de serviços na internet. Sobre informações vazadas de comunicações pelo telefone, visite a seção ["Informações confidenciais compartilhadas via ligações telefônicas ou SMS parecem ter sido vazadas para adversários"](#leaked-phone-info-intro).
>
> No geral, vazamentos provenientes de serviços na internet acontecem por três razões:
>
> 1. As informações eram públicas, então esse tipo de situação não estará contemplada aqui.
> 2. As informações foram vazadas diretamente da conta (mais comum em ataques promovidos por adversários mais próximos da vítima).
> 3. As informações foram vazadas pelas plataformas (mais comum em ataques promovidos por adversários "maiores" como governos e grandes empresas).
>
> Vamos cobrir os dois últimos casos, já que estão mais relacionados às emergências mais vistas na sociedade civil. Acompanhe o fluxo de trabalho começando pela seguinte pergunta:

Você compartilha dispositivos ou contas com adversáries?

##### Options

- [Sim](#shared-devices-or-accounts)
- [Não](#leaked-internet-info2)

### shared-devices-or-accounts

#### Sim, compartilho dispositivos ou contas

> Em alguns casos, compartilhar contas com outras pessoas pode dar acesso às informações sensíveis que podem ser facilmente vazadas. Além disso, quando compartilhamos contas, precisamos simplificar os processos de login, como tornar as senhas mais fracas e desativar a autenticação de dois fatores (2AF), o que pode afetar a segurança da conta.
>
> Um primeiro conselho seria avaliar bem quem deve ter acesso às contas ou materiais importantes, como documentos salvos em nuvem, e limitar esse acesso ao menor número de pessoas. Outra recomendação é ter uma conta para cada pessoa, permitindo que todas possam configurar suas próprias definições de acesso, da forma mais segura possível.
>
> Recomendamos também que consulte a seção ["Estou recebendo mensagem e alertas de segurança de dispositivos confiáveis"](#security-alerts-intro), onde você encontra dicas para endurecer as contas em caso de tentativas suspeitas de acesso.

Clique em continuar para explorar sua situação com mais detalhes

##### Options

- [Continuar](#leaked-internet-info2)

### leaked-internet-info2

#### Explorar mais detalhadamente

Seu adversárie pode controlar ou acessar os serviços online que você utiliza e armazenar informações? (_Isso se aplica normalmente a governos, órgãos de segurança pública ou até mesmo os próprios fornecedores_)

##### Options

- [Sim](#control-of-online-services)
- [Não](#leaked-internet-info3)

### control-of-online-services

#### Sim, meu adversário pode controlar ou acessar os serviços

> Às vezes as plataformas que usamos são simples de serem acessadas ou até mesmo controladas. Algumas questões que devemos nos fazer:
>
> - Quem tem a propriedade do serviço?
> - É um adversárie?
> - Tem acesso aos servidores ou dados através de solicitações legais?
>
> Nesse caso, é recomendado que transfira as informações para uma plataforma diferente que não seja controlada por adversaries, evitando futuros vazamentos.
>
> Outros indicadores de atenção:
>
> - Atividade de login, incluindo localizações geográficas, se estiver disponível
> - Comportamento estranho nos indicadores de leitura
> - Configurações afetando o que acontece com mensagens ou informações, como redirecionamentos, regras para encaminhamento de e-mails, etc, quando isso for possível
>
> Você também pode checar se as suas mensagens estão sendo lidas sem a sua permissão usando tokens Canários, como esses que podemos gerar em [canarytokens.org](https://canarytokens.org/generate)

Clique em "Continuar" para explorar sua situação com mais detalhes

##### Options

- [Continuar](#leaked-internet-info3)

### leaked-internet-info3

#### Explorar mais detalhadamente

Adversáries tem acesso a equipamentos de vigilância e a capacidade de plantá-los próximos de você?

##### Options

- [Sim](#leaked-internet-info4)
- [Não](#pre-closure)

### leaked-internet-info4

#### Sim, eles têm acesso a equipamentos de vigilância próximos

> Adversáries mais poderosos podem ser capazes de instalar equipamentos de vigilância ao redor de suas vítimas, como plantar dispositivos de localização ou infectar seus dispositivos com spywares.

Você acha que estão te rastreando por um dispositivo de localização, ou que seu computador ou celular pode ter sido infectado com um malware para te espionar?

##### Options

- [Gostaria de aprender mais sobre dispositivos de rastreamento](#tracking-device-intro)
- [Gostaria de aprender mais sobre a possibilidade do meu dispositivo ser infectado com um malware](#device-behaviour-intro)

### leaked-phone-info-intro

#### Informações confidenciais que compartilhei por chamadas de telefone ou SMS foram vazadas (especialmente durante eventos sesíveis)

> Como descrito na introdução deste tópico, a vigilância em massa de telefones é uma ameaça onipresente na maioria das regiões e países (se não em todos). Com ponto de partida, precisamos ter consciência de que é muito fácil para as operadoras (e qualquer pessoa com acesso) checar informações como:
>
> - Histórico de localização (sempre que o telefone está ligado e conectado à internet). Dados da torre de celular na qual o aparelho está conectado.
> - Registros de chamadas (quem ligou para quem, quando e por quanto tempo)
> - SMS (registros de até mesmo o conteúdo das mensagens)
> - Navegação na Internet (menos comum e menos útil para adversáries, mas pode identificar quais aplicações foram acessadas e quando, quais websites e quando, etc.)
>
> Outro cenário que ainda vem sendo observado em alguns contextos, mesmo que incomum, é o uso de dispositivos físicos de interceptação, chamados "grampos", ou IMSI-Catchers. Esses dispositivos simulam torres legítimas de conexão telefônica para redirecionar chamadas de pessoas em determinado raio de distância. Mesmo que as vulnerabilidades relacionadas a esse método já estejam sendo mitigadas pelos novos protocolos (4G/5G), ainda é possível interferir no sinal para fazer o telefone pensar que o único protocolo disponível é o 2G (o mais vulnerável). Em casos como esse, existem alguns indicadores que demonstram riscos, principalmente o fato do seu celular estar utilizando o protocolo 2G em áreas com 4G ou 5G disponível, ou se outras pessoas estão conectadas a eles sem problemas. Para mais informações você pode acessar o site do [Projeto FADe](https://fadeproject.org/?page_id=36). Além disso, você pode se perguntar se seu adversárie teria acesso a esse tipo de dispositivo, entre outras coisas.
>
> Uma recomendação geral para cenários como esse é: se você está sob esse risco, é recomendado migrar suas comunicações para canais criptografados baseados na internet, ao invés de chamadas telefônicas ou SMS's tradicionais, e também utilizar VPN e Tor, em caso de uma navegação web mais sensível. O grande aspecto não resolvido seria o rastreio da localização do aparelho, que é inevitável quando se mantém ligado à rede telefônica.
>
> No que diz respeito à coleta massiva de dados através das operadoras, em algumas jurisdições, usuáries podem solicitar um relatório de quais de seus dados já foram compartilhados. Você pode procurar saber se essa medida se aplica a sua realidade e se as informações podem ser relevantes para descobrir uma potencial vigilância. Lembre-se de que isso geralmente envolve alguma medida judicial.

Gostaria de checar outras potenciais ameaças ou seu caso é ainda mais complexo e deseja procurar ajuda, clique em "Continuar".

##### Options

- [Continuar](#pre-closure)

### peer-compromised-intro

#### As pessoas à minha volta foram alvo de medidas de vigilância bem sucedidas

> Caso alguma pessoa ou organização relacionada com você tenha sido vítima de vigilância, recomendamos que tente compreender as técnicas que foram utilizadas e selecione na lista anterior as mais próximas, para poder receber um explorar um contexto mais relevante e decidir se também pode ser vítima de técnicas semelhantes.

O que gostaria de fazer?

##### Options

- [Me leve para o passo anterior](#start)
- [Não tenho certeza](#pre-closure)

### other-intro

#### Alguma coisa desse tipo / eu não sei

> A lista que viu anteriormente incluiu os casos mais comuns observados na esfera civil pelas pessoas que atendem aos casos, mas a sua situação pode não ter sido contemplada. Se for esse o caso, clique em "Preciso de ajuda". Caso contrário, pode voltar à lista dos casos mais comuns e escolher o cenário mais próximo de você para obter orientações mais específicas.

O que gostaria de fazer?

##### Options

- [Me leve para a lista de potenciais ameaças](#start)
- [Preciso de ajuda](#help_end)

### pre-closure

#### Quero explorar as próximas etapas

> Esperamos que este fluxo de trabalho tenha sido útil até agora. Você pode escolher voltar para a lista inicial de cenários para verificar outras ameaças que possam te preocupar, ou ir para a lista de organizações que podem te ajudar em casos mais complexos.

O que gostaria de fazer?

##### Options

- [Gostaria de saber mais sobre outras técnicas de vigilância](#start)
- [Penso que a minha necessidade não foi contemplada nessa seção ou é ainda mais complexa](#help_end)
- [Acho que tenho já sei o que está acontecendo comigo](#resolved_end)

### help_end

#### Penso que a minha necessidade não foi contemplada nessa seção ou é ainda mais complexa

> Se precisa de mais ajuda para lidar com casos de vigilância, pode entrar em contato com as organizações abaixo.
>
> _Nota: quando contactar qualquer organização para procurar ajuda, informe o caminho que seguiu aqui e qualquer indicador que tenha encontrado para facilitar os próximos passos._

[orgs](:organisations?services=legal&services=vulnerabilities_malware&services=forensic)

### legal_end

#### Preciso de apoio jurídico

> Se precisa de apoio para processar alguém por ter te espionado ilegalmente, pode contactar uma das organizações abaixo.

[orgs](:organisations?services=legal)

### resolved_end

#### Meu problema foi resolvido

Esperamos que o Kit de Primeiros Socorros Digitais tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

### Final Tips

- Migre todas suas comunicações de canais não seguros, como chamadas telefônicas e SMS, para canais de comunicação criptografados que dependam da Internet, como aplicações de mensagens criptografadas de ponta a ponta e sites protegidos por HTTPS.
- Se está achando que o seu tráfego na internet está sendo monitorado e acredita que isso pode ser um risco para sua segurança, considere a possibilidade de se conectar à Internet através de uma ferramenta de criptografia como uma VPN ou Tor.
- Mantenha um registo de todos os dispositivos presentes nos seus espaços, especialmente os que estão ligados à Internet, e tente saber o que fazem.
- Evite compartilhar informações sensíveis através de serviços controlados por adversáries.
- Minimize o compartilhamento das suas contas ou dispositivos.
- Esteja atento a mensagens, sites, downloads e aplicações suspeitas.

### Resources

- [A aplicação Android da Apple para procurar AirTags é um passo necessário, mas são necessárias mais medidas anti-perseguição](https://www.eff.org/es/deeplinks/2021/12/apples-android-app-scan-airtags-necessary-step-forward-more-anti-stalking). (em inglês)
- [Privacidade para Estudantes](https://ssd.eff.org/module/privacy-students): Com referências a técnicas de vigilância nas escolas que se podem aplicar a muitos outros cenários. (em inglês)
- [Security in a Box: Proteja a privacidade das suas comunicações online](https://securityinabox.org/en/communication/private-communication/) (em inglês)
- [Security in a Box: Visite siter bloqueados e navegue anonimamento](https://securityinabox.org/en/internet-connection/anonymity-and-circumvention/) (em inglês)
- [Security in a Box: Recomendações para ferramentas de conversa criptografadas](https://securityinabox.org/en/communication/tools/#more-secure-text-voice-and-video-chat-applications) (em inglês)
