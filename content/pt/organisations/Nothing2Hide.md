---
language: pt
layout: organisation
name: Nothing2Hide
website: https://nothing2hide.org
logo: nothing2hide.png
languages: Français, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: "Segunda à Sexta, 9h às 18h, CET"
response_time: 1 dia
contact_methods: email, pgp, signal, web_form, wire
web_form: https://vault.tech4press.org/#/ (formulário online seguro - baseado no Globaleaks)
email: help@tech4press.org
pgp_key:  http://tech4press.org/fr/contact-mail/
pgp_key_fingerprint: 2DEB 9957 8F91 AE85 9915 DE94 1B9E 0F13 4D46 224B
signal: +33 7 81 37 80 08 <http://tech4press.org/fr/#signal>
wire: tech4press
---

A Nothing2Hide (N2H) é uma associação que tem como objetivo fornecer aos jornalistas, advogades, ativistas defensores de direitos humanos e cidadãos "comuns" os meios para protegerem seus dados e comunicações, fornecendo soluções técnicas e formações adaptadas à cada contexto. A nossa visão é colocar a tecnologia à serviço da difusão e da proteção da informação, a fim de reforçar as democracias em todo o mundo.
