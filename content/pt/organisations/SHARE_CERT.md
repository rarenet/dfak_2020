---
language: pt
layout: organisation
name: SHARE CERT
website: https://www.sharecert.rs/
logo: SHARECERT_Logo.png
languages: Srpski, Македонски, English, Español
services: in_person_training, org_security, assessment, secure_comms, vulnerabilities_malware, browsing, account, harassment, forensic, legal, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: "Segunda às Sexta, 9h às 17h, GMT+1/GMT+2"
response_time: "Em até um dia"
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.sharecert.rs/prijava-incidenta/
email: info@sharecert.rs, emergency@sharecert.rs
pgp_key_fingerprint: info@sharecert.rs - 3B89 7A55 8C36 2337 CBC2 C6E9 A268 31E2 0441 0C10
mail: Kapetan Mišina 6A, Office 31, 11000 Belgrade, Serbia
phone: +381 64 089 70 67
initial_intake: yes
---

A missão da SHARE CERT é fortalecer o nosso conselho deliberativo para que possamos responder de forma eficiente a ataques e tornarmo-nos mais resilientes para a prevenção de ataques futuros.
