---
language: pt
layout: organisation
name: Media Diversity Institute - Armenia
website: https://mdi.am/
logo: MDI_Armenia_Logo.png
languages: հայերեն, Русский, English
services: in_person_training, org_security, web_hosting, web_protection, digital_support, triage, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, censorship, physical_sec
beneficiaries: journalists, hrds, activists, lgbti, women, cso
hours: 24/7, GMT+4
response_time: 3 horas
contact_methods: email, pgp, phone, whatsapp, signal, telegram
email: help@mdi.am
pgp_key_fingerprint: D63C EC9C D3AD 51BE EFCB  683F F2B8 6042 F9D9 23A8
phone: "+37494938363"
whatsapp: "+37494938363"
signal: "+37494938363"
telegram: "+37494938363"
initial_intake: yes
---

O Media Diversity Institute - Armenia é uma organização sem fins lucrativos e não-governamental que busca catalizar o poder das mídias tradicionais, mídias sociais e novas tecnologias buscando assegurar os direitos humanos, ajudar a construir uma sociedade civil democrática de direito, dar voz aos silenciados e aprofundar o entendimento coletivo de diferentes tipos de diversidade social.

O MDI Armenia está afiliado ao Media Diversity Institute baseado em Londres, mas é uma entidade independente.
