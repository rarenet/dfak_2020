---
layout: contact-method
title: Tor դիտարկիչ
author: mfc
language: hy
summary: Կապ հաստատելու միջոցներ
date: 2018-09
permalink: contact-methods/tor
parent: /hy/
published: true
---

Tor դիտարկիչը գաղտնիության վրա հիմված վեբ բրաուզեր է, որը թույլ է տալիս անանունություն պահել կայքեր այցելելիս՝ առանց ձեր տեղայնքի հասցեն (IP հասցեն) կիսելու։

Ռեսուրսներ. [Tor-ի մասին](https://www.torproject.org/about/overview.html.en)։
