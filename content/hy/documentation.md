---
layout: page
title: “Թվային հարձակումների վավերագրում”
author: Constanza Figueroa, Patricia Musomba, Candy Rodríguez, Gus Andrews, Alexandra Hache, Nina
language: hy
summary: “Ինչպես դոկումենտացնել տարբեր տեսակի թվային արտակարգ իրավիճակներ։”
date: 2023-05
permalink: documentation
parent: Home
sidebar: >
  <h3>Կարդացեք ավելի՝ թե ինչպես վավերագրել տարբեր տեսակի թվային արտակարգ իրավիճակներ.</h3>
  <ul>
    <li><a href="https://acoso.online/barbados/report-to-law-enforcement/#general-tips">Acoso.online. ընդհանուր խորհուրդներ, դեպքի մասին զեկույցի ներկայացում և ինչպես պահել ապացույցները </a></li>
    <li><a href="https://www.techsafety.org/documentationtips">Ընտանեկան բռնությանը վերջ դնելու միջազգային կոմիտե. Ինչպես վավերագրել թվային չարաշահումից և հետապնդումից փրկվածների դեպքերը</a></li>
    <li><a href="https://chayn.gitbook.io/how-to-build-a-domestic-abuse-case-without-a-lawye/english">Chayn: Ինչպես սկսել ընտանեկան բռնության դատական գործ առանց փաստաբանի </a></li>
    </ul>
---

# Թվային հարձակումների վավերագրում

Թվային հարձակնումների վավերագրումը կարող է ունենալ մի շարք տարբեր նպատակներ։ Գրանցելով այն ամենը, ինչ հարձակման ընթացքում տեղի է ունենում, կարող եք.

- ավելի լավ հասկանալ իրավիճակն ու պաշտպանել ինքներդ ձեզ։
- հավաքել ապացույցներ, որոնք տեխնիկական և օրինական մասնագետներին կօգնեն՝ օգնել ձեզ։
- գտնել օրինաչափություններ հարձակումների մեջ և կանխատեսել հարձակվողի հաջորդ քայլերը։
- հասկանալ, թե ինչպես են թվային հարձակումներն էմոցիոնալ առումով ազդում ձեզ վրա։

Անկախ նրանից, հարձակումները վավերագրում եք ինքներդ ձեր, թե օրինական և տեխնիկական օգնություն հայցելու համար, դրանց կառուցվածքային գրանցումը կօգնի ավելի լավ հասկանալ.

- Հարձակումների մասշտաբը. արդյոք խոսքը գնում է եզակի թե կրկնվող հարձակումների շարքի մասին։ Արդյոք թիրախը անձամբ դուք եք, թե՝ մարդկանց ավելի մեծ խումբ։
- Հարձակվողի պահելակերպը. արդյոք նա ինքնուրույն գործող անհատ է, թե կազմակերպված խմբի մաս։ Օգտագործում է ձեր մասին համացանցում հասանելի տվյալնե՞րը, թե՞ գաղտնի, անձնական ինֆորմացիա, ինչ մարտավարություն ու տեխնոլոգիաներ կարող է ունենալ ձեռքի տակ, և այլն։
- Ինչպես մշակել պատասխան քայլեր կամ խուսափել դրանցից, որպեսզի նվազեցնեք ձեր հասցեին ուղղված ֆիզիկական ու թվային սպալնալիքները։

Ինֆորմացիա հավաքելու ընթացքում, հաշվի առեք հետևյալը.

- Վավերագրում եք առաջին հերթին ձեր հանգստությա՞ն համար, թե՞ հասկանալու, թե ինչ է կատարվում։
- Ցանկանո՞ւմ եք դիմել մասնագետների օգնության։ [Թվային առաջին օգնության հավաքածուի աջակցության բաժնում](/hy/support/) կգտնեք CiviCERT կազմակերպությունների շարք, որոնք կարող են ձեզ օգնել։
- Ցանկանո՞ւմ եք նաև սկսել դատական գործ։

Կախված ձեր նպատակներից, կարող է անհրաժեշտ լինի գրանցել հարձակումները փոքր-ինչ տարբեր ձևերով։ Եթե ձեզ անհրաժեշտ են պաշտոնական ապացույցներ դատական գործ սկսելու համար, ապա կարիք կլինի հավաքել որոշակի կոնկրետ տվյլաներ, որոնք օրինականորեն ընդունելի կլինեն դատարանի կողմից (օրինակ՝ հեռախոսի համարներ և ժամանակային դրոշմանիշներ)։ Տեխնիկական օգնություն հայցելիս, ձեզանից նույնպես հավելյալ ինֆորմացիա կպահանջեն. ինտերնետային հասցեներ (URL-ներ), մուտքանուններ, սքրինշոթեր։ Ստորև կգտնեք ավելի մանրամասն նկարագիր, տարբեր պրոցեսների համար անհրաժեշտ ինֆորմացիայի վերաբերյալ։

## Պաշտպանեք ձեր հոգեբանական և նյարդային առողջությունը՝ հարձակումները փասթաթղթավորելիս

Օնլայն հարձակումների դեմ պայքարելը սթրեսային է։ Ձեր ֆիզիկական և հոգեբանական առողջությունը պետք է լինի առաջնահերթություն՝ անկախ նրանից, թե ինչպես եք հարցին մոտենալու։ Հարձակումների գրանցումը չպետք է ձեզ հավելյալ սթրեսի ենթարկի։ Այդ իսկ պատճառով՝ հիշեք, որ.

- Գրանցումներ կատարելու տարբեր ձևեր կան։ Ընտրեք այն ձևերը, որոնք ձեզ անհարմարություն չեն պատճառում և կծառայեն ձեր նպատակին։
- Թվային հարձակումների վավերագրությունը կարող է ձեզ վրա ազդել էմոցիոնալ առումով և տրավմատիկ հիշողությունների տեղիք տալ։ Եթե ձեզ ճնշված եք զգում, ապա միգուցե արժե վստահել վավերագրման պրոցեսը որևէ գործընկերոջ, ընկերոջ, վստահված անձի կամ վերասհսկողության խմբի, որը կարող է գրանցել հարձակումներն առանց նման զգացումներ ապրելու։
- Եթե հարձակումը ոչ թե անհատի այլ խմբի կողմից է, միգուցե արժե դելեգացնել գրանցման պատասխանատվությունը տարբեր անձանց։
- Հարձակումները պետք է շարունակվեն գրանցվել միայն այնքան ժամանակ, քանի դեռ ներգրավված անձը կամ խումբը համաձայն է դրան։
- [Ավելին՝ թե ինչպես հայցել ձեր ընկերների և ընտանիքի աջակցությունը։](https://onlineharassmentfieldmanual.pen.org/guidelines-for-talking-to-friends-and-loved-ones/)

Գրանցել թվային հարձակումներ կամ օնլայն գենդերային բռնության դեպքեր, նշանակում է նաև վերլուծել ձեր և ձեր կոլեկտիվի ապրումները։ Ուստի պարտադիր չէ հարցի միայն ռացիոնալ ու տեխնիկական կողմը դիտարկել։ Կարող եք գրանցել ձեր զգացումները՝ տեքստով, պատկերներով, ձայնագրություններով, վիդեոյով կամ անգամ արտահայտել դրանք գեղարվեստական լուծումներով։ Մենք խրախուսում ենք այս ամենն անել օֆլայն, հետևելով ստորև նշված քայլերին՝ ձեր տվյալների գաղտնիության համար։

## Պատրաստվեք հարձակումը վավերագրելու

Երբ արդեն վստահ եք, որ էմոցիոնալ առումով բավականաչափ կայուն եք, սակայն մինչև անհատական դեպքերի գրանցումը, վերլուծեք, թե կոնկրետ ինչ ինֆորմացիա է ձեզ պետք հավաքագրել՝ **«քարտեզագրելով» այն ամենը, ինչը կարող է պետք գալ**։

Դոկումենտացիայի ամենակարևոր հատվածը **գրառումների կազմակերպված մատյանի (լոգի) ստեղծումն է**։ Օրինական և տեխնիկական դոկումնետացիայի համար կարևոր է նման մատյանը, որում գրանցված են միայն ամենահիմնական կետերը. հարձակումների բնույթի մասին ընդհանուր պատկեր կազմելու համար։ Եթե դոկումենտացիան անում եք ձեր իսկ հանգստության համար, միգուցե կարիք չլինի այս աստիճան մեթոդիկ գրանցումների։ Սխեմատիկ գրանցումն, այնուամենայնիվ, ձեզ էլ կարող է օգնել՝ նկատելու հարձակումների օրինաչափությունները։ Մատյանը կարող է լինել տեքստային դոկումենտի, էլեկտրոնային աղյուսակի, կամ անգամ թղթային տարբերակով։ Որոշումը՝ ձերն է։

**Բոլոր թվային գործողություններն իրենց բնորոշ հետքերը (մետա-դատաներն) են թողնում**. ժամ, տևողություն, հասցեներ և այլն։ Այս հետքերը պահպանվում են ձեր էլ.սարքի մատյաններում, ինչպես նաև օրինակ հեռախոսային կամ սոց.ցանցային կազմակերպությունների մոտ։ Վերլուծելով այս մետա-դատաները, տեխնիկական մասնագետները կարող են էական ինֆորմացիա ստանալ հարձակվողի ինքնության վերաբերյալ։ Օրինակ հեռախոսազանգի մատյանը, որտեղ երևում է, որ նույն համարից քսան անգամ զանգ եք ստացել, կարող է ոտնձգության գործում կարևոր դեր խաղալ։

Գրանցելով այս ամենը՝ կազմակերպված, սխեմատիկ կերպով, դուք կարող եք գտնել **հարձակումների օրինաչափությունները**։ Միգուցե գտնեք անգամ այնպիսի օրինաչափություններ, որոնք կարոք եք օգտագործել դատական գործի համար, կամ որոնք կարող են պետք գալ թվային անվտանգության մասնագետին՝ հարձակումները բլոկելու և ձեր սարքերը պաշտպանելու համար։

Ինֆորմացիայի պահպանման հետևյալ սխեման օգտակար է դեպքերի մեծ մասի համար, բայց իհարկե՝ ամեն դեպքն առանձնահատուկ է։ Կարող եք պատճենել ստորև թվարկած վանդակները էլեկտրոնային աղյուսակի կամ տեքստային դոկումենտի վերևում, դասակարգելու համար ձեր զեկույցները։ Կարող եք նաև ավելացնել հավելյալ կետեր՝ ըստ անհրաժեշտության։

| Օր  | Ժամ | Էլ.փոստի հասցեներ և հեռախոսահամարներ, որ հարձակվողն օգտագործել է | Հարձակման հետ կապված հղումներ | Անուններ (ներառյալ մուտքանուններ), որոնք հարձակվողն օգտագործել է | Թվային հարձակման տեսակներ. ոտնձգություն, ապատեղեկատվություն, և այլն | Հարձակման մեթոդներ |
| --- | --- | ---------------------------------------------------------------- | ----------------------------- | ---------------------------------------------------------------- | ------------------------------------------------------------------- | ------------------ |

Ահա թվային մատյանների (լոգերի) մի քանի այլ ձևանմուշներ, որոնք կարող եք օգագործել.

- [Միջադեպերի մատյանի ձևանմուշ՝ Access Now-ի Digital Security Helpline-ից](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/tree/master/templates/incident_log_template.md)
- [Գենդերային բռնության համար ստեղծված ձևանմուշ Acoso.online-ից](https://acoso.online/site2022/wp-content/uploads/2019/01/reactingNCP.pdf)

Բացի գրանցման մատյանից, **միջադեպի հետ կապված ամեն բան պահեք** (էլ.)թղթապանակում կամ թղթապանակներում. ներառյալ մատյաններում գրանցած դոկումենտները և մնացած բոլոր հավելյալ թվային ապացույցները, որոնք վերբեռնել, ներբեռնել, կամ սքրինշոթել եք։

**Պաշտպանեք ձեր հավաքագրած ինֆորմացիան**։ Դատան չկորցնելու համար ավելի ապահով է կրկնօրինակներ պահել նաև ձեր էլ.սարքի վրա (և ոչ միայն օնլայն դատաբազաներում)։ Պաշտպանեք այն գաղտնագրեր ստեղծելով ու տեղադրելով դրանք գաղտնի թղթապանակներում կամ մասնատված դիսկերում, եթե դա հնարավոր է։

Սոցիալական մեդիաներում **խուսափեք բլոքելուց, համրեցնելուց (մյութելուց) կամ բողոքարկելուց ձեր վրա հարձակում գործած էջերը**, քանի դեռ չեք հավաքել ձեզ անհրաժեշտ դոկումենտացիան։ Հարձակվողի էջը բլոքելով, կարող եք ինքներդ ձեզ զրկել անհրաժեշտ ինֆորմացիայից։ Եթե անգամ արդեն բլոքել, համրեցրել կամ բողոքարկել եք նրանց էջերը՝ հնարավոր է, որ կարողանաք ձեզ պետքական դոկումենտացիան հավաքել մեկ այլ անձի (ընկերոջ կամ կոլեգայի) էջից։

## Էկրանի պատկերներ (սքրինշոթեր) արեք

Մի թերագնահատեք բոլոր հարձակումները սթրինշոթերով պահելու կարևորությունը (կարող եք այս աշխատանքը թողնել նաև ձեր վստահված անձին)։ Շատ թվային նամակներ հեշտությամբ կարելի է կորցնել կամ ջնջել։ Որոշ հրապարակումներ, օրինակ՝ Tweet-եր կամ WhatsApp-ի նամակներ, հնարավոր է վերականգնել միայն այնպիսի հարթակների օգնությամբ, որոնք թվային հարձակման կանչին պատասխանում են ուշացմամբ՝ եթե ընդհանրապես արձագանքում են։

Եթե չգիտեք ինչպես անել էկրանի պատկերը ձեր գործիքով, ապա կարող եք հրահանգները գտնել համացանցում՝ փնտրելով, «ինչպես անել սքրինշոթ» ու նշել ձեր գործիքի արտադրողի, մոդելի և օպերացիոն համակարգի անունը (օրինակ. «Սամսունգ Գալաքսի S21 Android, ինչպես անել սքրինշոթ (հայերենով արդյունքի չհասնելու դեպքում, փորձեք նույնը փնտրել անգլերենով կամ մեկ այլ լեզվով))։

Եթե սքրինշոթ եք անում դիտարկիչի (բրաուզերի) միջից, ապա կարևոր է **ներառել էջի էլ.հասցեն (URL-ը)**։ Խոսքը դիտարկիչի վերևի հատվացում գտնվող էլ.հասցեի մասին է։ Մեկմեկ բրաուզերներն այն թաքցնում են և ցույց են տալիս միայն երբ մկնիկով էջի վրայով վերև-ներքև եք անում։ Հասցեների միջոցով պարզ է դառնում, թե որտեղից է թվային հարձակումը կատարվել և օգնում եք տեխնիկական կամ օրենսդիր մասնագետներին ավելի հեշտությամբ նման կայքերն ախտորոշել։ Ստորև տեսե՛ք URL կոդը ներառող սքրինշոթ՝ որպես օրինակ։

<img src="/images/screenshot_with_URL.png" alt="Here is a sample view of a screenshot showing the URL." width="80%" title="Here is a sample view of a screenshot showing the URL.">

<a name="legal"></a>

## Դոկումենտացիա՝ դատական գործի համար

Ուզում եք դատական գո՞րծ սկսել։ Արդյո՞ք այն ձեզ ավելի մեծ ռիսկի տակ չի դնի։ Ունե՞ք այդքան ժամանակ և էներգիա։ Դատական գործ սկսելը միշտ չէ, որ պարտադիր է կամ անհրաժեշտ։ Ինքներդ ձեզ հարցրեք նաև, թե արդյոք իրավական գործընթացը ձեզ համար կազդուրիչ կլինի։

Եթե որոշել եք գործը տանել դատարան, դիմեք փաստաբանի կամ օրենսդիր կազմակերպության, որին վստահում եք։ Վատ կամ սխալ իրավական խորհուրդը կարող է օգուտի փոխարեն վնաս պատճառել։ Չարժե անել իրավական քայլեր ինքուրույն, եթե վստահ չեք, որ գիտեք, թե ինչ եք անում։

Փաստաբանին սովորաբար պետք է տրամադրել պատշաճ ապացույցներ գործը պաշտպանելու համար։ Նա պետք է նաև ներկայացնի, թե ինչպես է ինֆորմացիան ստացվել և ապացուցի, որ այն հավաքվել է օրինական ճանապարհով. որ ինֆորմացիայի ստանձնումն անհրաժեշտ էր դատական գործ սկսելու համար և այլ փաստարկներ ներկայացնել, որոնք հավաքագրված տվյալները դատարանի կողմից ընդունելի կդարձնեն։ Այնուհետև դատավորը կամ դատարանը կգնահատեն փաստաբանի առաջադրած փաստարկները։

Դատական գործ սկսելու համար ինֆորմացիա հավաքելիս, արժե.

- Սկսել ապացույց հավաքել որքան հնարավոր է շուտ՝ պրոցեսի ամենասկզբից։ Այն հետո կարող է ջնջվել հրապարակողի կամ սոց.ցանցի կողմից։ Սա կբարդացնի հետագայում համապատասխան փաստերի հավաքագրումը։
- Եթե անգամ հարձակումը չնչին է թվում, կարևոր է պահել այն։ Դա կարող է լինել ընդհանուր փազլի մի կտոր, որի հանցավոր բնույթը կերևա միայն գործը սկզբից մինչև վերջ զննելիս։
- Կարող եք հարթակներին կամ ISP-ներին (ինտերնետ պրովայդերներին) հայցել, որպեսզի ձեզ համար պահեն հարձակման թվային զեկույցը։ Սա հնարավոր է միայն կարճաժամկետ՝ դեպքից շաբաթներ կամ ամիսներ անց։ Տեսե՛ք [Without My Consent-ի դատական տվյալների պահպանման հայտ ներկայացնելու վերաբերյալ ինֆորմացիան](https://withoutmyconsent.org/resources/something-can-be-done-guide/evidence-preservation/#consider-whether-to-include-a-litigation-hold-request), այս քայլի մասին ավելին իմանալու համար։
- Պահել ժամային դրոշմակնիքները, էլ.փոստի ու վեբ հասցեները։ Դրանք վճռորոշ նշանակություն ունեն դատարանում ապացույցների ընդունման համար։ Փաստաբանները պետք է կարողանան ցույց տալ, որ այսինչ տեղից հաղորդագրությունը այնինչ տեղն է ուղարկվել՝ այս կոնկրետ օրվա այս ժամին։
- Փաստաբանները նաև պետք է ստուգեն, որ ապացույցները չեն կեղծվել, որ դրանք իրական են և ապահով ձեռքերում են եղել ստեղծվելու պահից։ Նամակների սքրինշոթերն ու տպված մեյլերն այս առումով բավականաչափ կայուն փաստեր չեն համարվում։

Այս իրավական պատճառներից ելնելով, ձեզ համար լավագույն լուծումը կարող է լինել նոտարի կամ թվային հավաստագրման ընկերություն վարձելը, որը կարող է անհրաժեշտության դեպքում ցուցմունք տալ դատարանում։ Թվային սերտիֆիկացման ընկերությունները հաճախ ավելի էժան են, քան նոտարական ծառայությունները։ Ցանկացած նոտար կամ սերտիֆիկացման ընկերություն, որին կվարձեք, պետք է ունենա բավարար տեխնիկական նախապատմություն՝ անհրաժեշտության դեպքում նման առաջադրանքներ կատարելու համար.

- հաստատել ժամանակային դրոշմանիշները
- հաստատել հավաքած ապացույցների հավաստիությունը, օրինակ ձեր սքրինշոթերը սկզբնական աղբյուրի հետ համեմատելով և հաստատելով, որ դրանք չեն կեղծվել
- ապացույց ներկայացնել, որ թվային վկայականները համապատասխանում են URL-ներին
- նույնականացում անել. օրինակ հաստատել, որ x անձի պրոֆիլը իրոք գրանցված է այսինչ սոցիալական մեդիայում և որ այդ պրոֆիլից ձեզ վրա իսկապես հարձակում է կատարվել
- հաստատել հեռախոսահամարի և ձեր դեմ օգտագործած WhatsApp-ի էջի կապը

Իմացեք, որ ձեր հավաքած ոչ բոլոր ապացույցներն են ընդունվելու դատարանի կողմից։ Ուսումնասիրեք ձեր երկրում ու տարածաշրջանում թվային սպառնալիքների վերաբերյալ իրավական ընթացակարգերը՝ որոշելու համար, թե ինչ է պետք հավաքել։

### Մարդու իրավունքների խախտումների փաստագրում

Եթե ձեզ անհրաժեշտ է փաստագրել մարդու իրավունքների խախտման դեպքեր ներբեռնելով կամ պատճենելով այնպիսի գրառումներ, որոնք սոցիալական մեդիան ակտիվորեն ջնջում է, փորձեք կապվել [Mnemonic-ի](https://mnemonic.org/en/our-work) նման կազմակերպության հետ, որը կարող է ձեզ օգնել պահպանել այդ հրապարակումները։

## Ինչպես պահպանել ապացույցներ օրինական ու թվային անվտանգության զեկույցների համար

Բացի էկրանի նկարներ պահելուց, կան նաև այլ ձևեր, թե ինչպես ապացույց հավաքել։ Ահա մի քանի մեթոդ, որոնց հետևելով կարող եք օգնել ձեր դատական ու տեխնիկական խորհրդատուներին։

- **Զանգերի տեղեկամատյաններ։** Մուտքային և ելքային զանգերի համարները պահվում են ձեր բջջային հեռախոսի տվյալների բազայում:
  - Կարող եք այդ մուտքային ու ելքային համարների մատյանները նաև սքրինշոթեր անել։
  - Հնարավոր է նաև պահպանել այս տեղեկամատյանները կրկնօրինակելով ձեր հեռախոսի համակարգը (backup անելով) կամ օգտագործելով հատուկ ծրագրեր՝ տեղեկամատյանները ներբեռնելու համար։
- **Հաղորդագրություններ (SMS)**: Բջջային հեռախոսների մեծ մասը թույլ է տալիս կրկնօրինակել ձեր SMS հաղորդագրությունները ամպում (cloud-ում)։ Այլապես կարող եք հաղորդագրությունների սքրինշոթեր անել։ Այս մեթոդներից ոչ մեկը չի համարվում օրինական ապացույց առանց լրացուցիչ ստուգման։ Այնուամենայնիվ, դրանք կարևոր են հարձակումները դոկումենտացնելու համար։
  - Android. Եթե ձեր հեռախոսը չունի SMS հաղորդագրությունները պահուստավորելու հնարավորություն, կարող եք օգտագործել [SMS Backup & restore-ի](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore) պես հավելվածներ։
  - iOS. Միացրեք հաղորդագրությունների կրկնօրինակումը գնալով. iCloud՝ Կարգավորումներ > [ձեր օգտվողի անունը] > Կառավարել պահեստը (Manage storage) > Պահուստավորում (Backup)։ Ակտիվացրեք հաղորդագրությունների կրկնօրինակումը՝ ձեր սարքի անվան վրա սեղմելուց հետո։
- **Զանգերի ձայնագրում**. Ավելի նոր բջջային հեռախոսներն ունեն զանգեր ձայնագրելու ֆունկցիա։ Ստուգեք, թե ինչ իրավական սահմանափակումներ կան ձեր երկրում կամ տեղական իրավասության մեջ՝ զանգերի ձայնագրման վերաբերյալ. երկու կողմերի համաձայնությամբ կամ առանց դրա։ Եթե դա օրինական է, ապա կարող եք հեռախոսը կարգաբերել այնպես, որ զանգի ձայնագրիչն ավտոմատ ակտիվանա պատասխանելիս՝ առանց զանգողին զգուշացնելու, որ նա ձայնագրվում է։
  - Android․ Կարգավորումներ > Հեռախոս (Phone) > Զանգերի ձայնագրման կարգավորումներում (Call recording settings)։ Կարող եք ակտիվացնել բոլոր զանգերի, անհայտ համարներից զանգերի, կամ կոնկրետ համարներից զանգերի համար։ Եթե այս տարբերակը հասանելի չէ ձեր սարքում, կարող եք ներբեռնել [Call Recorder-ի](https://play.google.com/store/apps/details?id=com.lma.callrecorder) պես հավելված։
  - iOS. Apple-ն ավելի սահմանափակ է զանգերի ձայնագրման հարցում։ iOS օպերացիոն համակարգում այդ ֆունկցիան լռեյլալյն անջատված է։ Զանգ զայնձագրելու համար, օգտվեք հավելվածներից, ինչպիսիք են՝ [RecMe-ն](https://apps.apple.com/us/app/call-recorder-recme/id1455818490)։
- **Էլ.փոստեր**. Նույն տրամաբանությամբ, ինչպես թղթային նամակներն ունեն փոստային կնիք և ուղարկողի հասցե, յուրաքանչյուր էլ.նամակ նույնպես ունի իր «ենթագիրը» մեյլի վերևի հատվածում։ Այն ցույց է տալիս, թե ով է ուղարկել նամակը և ինչպես։
  - Էլ.փոստի ենթագրերը դիտելու և պահելու համար հետևեք [այս ուղեցույցին՝ Computer Incident Response Center Luxembourg (CIRCL)-ի կողմից](https://www.circl.lu/pub/tr-07/).
  - Եթե ավելի շատ ժամանակ ունեք, կարող եք նաև կարգավորել ձեր էլ.փոստն այնպես, որ այն ավտոմատ կարգով ներբեռնի բոլոր հաղորդագրությունները դեսքթոփի հավելվածի վրա, ինչպիսին է Thunderbird-ը [POP3](https://support.mozilla.org/en-US/kb/difference-between-imap-and-pop3#w_changing-your-account)։ [Mozilla-յի այս էջում](https://support.mozilla.org/en-US/kb/manual-account-configuration) կգտնեք Thunderbird-ին էլ.փոստի հաշիվ կցելու ցուցումները։
- **Լուսանկարներ**. Բոլոր նկարներն ունեն «EXIF պիտակներ»՝ մետատվյալներ, որոնք ցույց են տալիս, թե որտեղ և երբ է արվել լուսանկարը։
  - Որոշ դեպերում հնարավոր է վեբկայքերից կամ ստացված հաղորդագրություններից նկարները պահել այնպես, որ պահպանվեն EXIF պիտակները, որոնք կկորչեին, եթե պարզապես սքրինշոթ անեիք։ Երկար սեղմեք պատկերի վրա շարժական սարք օգտագործելիս, կամ «աջ-քլիք» արեք պատկերի վրա եթե օգտագործում եք համակարգիչ (Mac-ի դեպքում՝ նաև control-քլիք, Windows-ի դեպքում՝ menu-ի ստեղնով)։
- **Վեբկայքեր**. Վեբկայքերի ամբողջական ներբեռնումը կամ կրկնօրինակումը կարող է օգնել ձեզ աջակցողներին։
  - Եթե այն էջը, որտեղ եղել է ոտնձգությունը, հանրային է, (այլ կերպ ասած, եթե կարիք չկա այդ էջ մուտք գործել՝ այն տեսնելու համար), ապա կարող եք էջի հասցեն գրանցել [Wayback Machine-ի](https://web.archive.org/) ինտերնետային արխիվում՝ դրա ներկա տարբերակը պահելու համար։ Գրանցեք օրը՝ թե երբ եք էջը կրկնօրինակել, և այնուհետև կարող եք Wayback Machine-ով գտնել այդ էջի այդ օրվա տարբերակը։
  - Սքրինշոթը ավելի քիչ դետալավորված է, բայց կարող է նույնպես պարունակել կարևոր ինֆորմացիա։
- **WhatsApp**. WhatsApp-ի խոսակցությունները կարելի է ներբեռնել տեքստային ձևաչափով կամ կցված մեդիայի միջոցով. Դրանք նաև լռելյայն պահուստավորվում են iCloud-ում կամ Google Drive-ում:
  - [Այստեղ](https://faq.whatsapp.com/1180414079177245/?helpref=uf_share) կգտնեք անհատական կամ խմբային զրույցի պատմությունը պատճենելու հրահանգները։
- **Telegram**. Telegram-ի խոսակցությունները պատճենելու համար հարկավոր է օգտագործել աշխատասեղանի (դեսքթոփ) հավելվածը։ Ընտրեք ձևաչափը որով ցանկանում եք խոսակցությունները պահել և ժամանակային հատվածը (երբվանից՝ երբ)։ Telegram-ը՝ այս երկուսի հիման վրա, ձեզ համար կստեղծի HTML ֆայլ։ Հրահանգներն՝ [այստեղ](https://telegram.org/blog/export-and-more)։
- **Facebook Messenger**. Մուտք գործեք ձեր Facebook հաշիվ: Գնացեք Կարգավորումներ > Ձեր Facebook-ի տվյալները > Ներբեռնել ձեր տվյալները։ Կհայտնվեն բազմաթիվ տարբերակներ՝ ընտրեք «Հաղորդագրություններ»։ Facebook-ը ժամանակ է պահանջում ֆայլը պատրաստելիս։ Ձեզ կտեղեկացնեն, երբ այն հասանելի կլինի ներբեռնման համար։
- Եթե զեզ անհրաժեշտ է պահպանել **տեսանյութեր** որպես ապացույց, ապա օգտագործեք արտաքին սկավառակ (կոշտ սկավառակ, USB կրիչ կամ SD քարտ)՝ բավարար տարողությամբ։ Կարող է պետք լինի նաև ներբեռնել տեսաէկրան նկարելու հավելված։ Տեսախցիկի (կամերայի) հավելվածը, XBox Game Bar-ը կամ Snipping Tool-ը կարող են օգտագործվել Windows-ում, իսկ Mac-ում կարող եք օգտվել Quicktime-ից, կամ դիտարկիչի հավելվածներից, օրինակ՝ [Video DownloadHelper-ից](https://www.downloadhelper.net/)։
- Լայնածավալ կազմակերպված հարձակումների դեպքում, օրինակ՝ անընդհատ կրկնվող **հատուկ հեշթեգով կամ մեծ թվով առցանց մեկնաբանություններով** հարձակումների ժամանակ, կարող է պետք լինի անվտանգության թիմի, թվային անվտանգության լաբորատորիայի կամ որևէ ինստիտուցիայի օգնությունը՝ տվյալների այդ բազմությունը հավաքագրելու և դոկումենտացնելու համար։ Կազմակերպությունները, որոնց կարող եք դիմել օգնության համար, ներառում են [Citizen Lab-ը](https://citizenlab.ca/about/), [Fundacion Karisma’s K-Lab-ը](https://web.karisma.org.co/klab/) (իսպաներենով), [Meedan-ը](https://meedan.com/programs/digital-health-lab), the Stanford Internet Observatory-ն և the Oxford Internet Institute-ը։
- **Կասկածելի հաղորդագրություններին կցված ֆայլերը** արժեքավոր ապացույցներ են։ Ոչ մի դեպքում չպետք է սեղմեք դրանց վրա կամ բացեք դրանք։ Վստահելի տեխնիկական խորհրդատուն ձեզ կօգնի ապահով կերպով այդ ամենը հավաքել և ուղարկել մարդկանց, ովքեր կարող են այդ լինքերը վերլուծել։
  - Կարող եք օգտագործել [Danger Zone-ը](https://dangerzone.rocks/)՝ հնարավոր վտանգ պարունակող PDF-ներից, պատկերներից և տեքստային ֆայլերից ապահով PDF-ներ ստանալու համար։
- Ավելի նպատակաուղղված սպառնալիքների դեմ, ինչպիսիք են **լրտեսող ծրագրերը** ձեր սարքերում կամ **ձեզ թիրախավորող նամակները**, անհրաժեշտ է փաստագրման է՛լ ավելի ճշգրիտ մեթոդ ընտրել։ Կարևոր է գիտակցել, թե երբ է ճիշտ ապացույց հավաքել ինքնուրույն, և երբ դա թողնել փորձագետներին։ Եթե վստահ չեք, ապա դիմեք [վստահելի խորհրդատուի](/hy/support)։
  - **Եթե ձեր սարքը միացված է, ապա թողեք այն միացված: Եթե այն անջատված է՝ անջատված։** Անջատելով կամ միացնելով ձեր սարքը, դուք վտանգում եք կորցնել կարևոր դատա։ Խիստ զգայուն և օրինական հետաքննություների ժամանակ նախընտրելի է ներգրավել թվային անվտանգության փորձագետների՝ սարքն անջատելուց կամ նորից միացնելուց առաջ։
  - Լուսանկարեք ձեր սարքը, որի մեջ ըստ ձեզ տեղադրվել են վտանգավոր ծրագրեր կամ որը այս կամ այլ կերպ խաթարվել է։ Դոկումենտացրեք դրա ֆիզիկական վիճակը և վայրը, որտեղ այն գտել եք, այն պահից երբ սկսեցիք կասկածել, որ սարքի հետ մի բան այն չէ։ Սարքի վրա կա՞ն փորվածքներ կամ քերծվածքներ։ Այն թրջվա՞ծ է։ Մոտակայքում կա՞ն որևէ գործիքներ, որոնք կարող էին օգտագործվել սարքը խաթարելու համար։
  - Պահպանեք և՛ սարքը, և՛ ձեր գրանցած տվյալներն ապահով վայրում։
  - Սարքից տվյալների արդյունահանման ճիշտ մեթոդը կախված է սարքից։ Նոթբուքից տվյալներ հանելը տարբերվում է սմարթֆոնից տվյալներ հանելուց։ Յուրաքանչյուր սարք պահանջում է հատուկ գործիքներ և գիտելիքներ։
- Թիրախային սպառնալիքների ժամանակ տվյալների հավաքագրումը հաճախ ներառում է **համակարգի մատյանի (log) ֆայլերի** պատճենում։ Հեռախոսը կամ համակարգիչը այս ֆայլերն ավտոմատ կերպով հավաքում են։ Տեխնիկական փորձագետներին, ովքեր ձեզ կօգնեն տվյալների հավաքագրման հարցում, կարող են այդ մատյանները պետք գալ։ Նրանց նպատակը պետք է լինի համակարգչից ապահով կերպով արտահանել մետատվյալները, հավելվածները և ուղարկել դրանք մասնագետներին, ովքեր այդ ամենը կարող են վերլուծել։
  - Այս մետատվյալները պահպանելու համար սարքը պահեք այլ պահեստային համակարգերից առանձին, անջատեք Wi-Fi-ն ու Bluetooth-ը, անջատեք լարային ցանցային միացումները և երբեք մի խմբագրեք գրանցամատյանների ֆայլերը։
  - Մի փորձեք փոխարկել գրանցամատյանի ֆայլերը USB կրիչի վրա։
  - Մատյալը (log-ը) կարող է ինֆորմացնել սարքի ներքին ֆայլերի վիճակի մասին (օրինակ, թե որ ֆայլերը և ինչպես են բացվել) կամ սարքի մասին (օրինակ՝ արդյո՞ք տրվել է անջատման կամ ջնջման հրահանգ։ Արդյո՞ք ինչ-որ մեկը փորձել է ֆայլերը պատճենել մեկ այլ վայրում)։
