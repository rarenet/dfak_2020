---
language: hy
layout: organisation
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: "աշխատանքային ժամեր, UTC+2"
response_time: "4 ժամ"
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL-ը թվային անվտանգության արագ արձագանքման թիմ է, Լյուքսեմբուրգում մասնավոր ձեռնարկությունների, համայնքների և հասարակական կազմակերպությունների համար։

Եթե ​​դուք օգտատեր եք, ընկերություն կամ կազմակերպություն Լյուքսեմբուրգում և ենթարկվում եք հարձակման, CIRCL-ը կարող է լինել ձեր վստահելի գործընկերը։ CIRCL-ի փորձագետների թիմն աշխատում է հրշեջ բրիգադի պես. արագ և արդյունավետ արձագանքելով ինչպես տեղի ունեցած, այնպես էլ սպասվող սպառնալիքներին և միջադեպերին։

CIRCL-ի նպատակն է համակարգված և օպերատիվ կերպով հավաքել, վերանայել, զեկուցել և արձագանքել կիբեր սպառնալիքները։
