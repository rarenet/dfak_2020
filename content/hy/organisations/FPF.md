---
language: hy
layout: organisation
name: Freedom of the Press Foundation (Մամուլի ազատության հիմնադրամը)
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: "երկուշաբթիից ուրբաթ, աշխատանքային ժամերին, ԱՄՆ-ի Արևելյան ժամային գոտի"
response_time: "1 օր"
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

Մամուլի ազատության հիմնադրամը (FPF-ը) 501(c)3-ի մաս հանդիսացող շահույթ չհետապնդող կազմակերպություն է։ Այն խրախուսում ու պաշտպանում է հանրային շահերի լրագրությունը (Public Interest Journalism) 21-րդ դարում։ FPF-ը պատրաստ է բոլորին՝ խոշոր մեդիա կազմակերպություններից մինչև անկախ լրագրողներին, վերապատրաստել անվտանգության ու գաղտնիության տարբեր գործիքների և տեխնիկաների վերաբերյալ. իրենց, իրենց աղբյուրներն ու կազմակերպություններն ավելի լավ պաշտպանելու համար։
