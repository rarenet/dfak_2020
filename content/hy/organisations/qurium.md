---
language: hy
layout: organisation
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: "8:00-18:00, երկուշաբթիից կիրակի, CET"
response_time: "4 ժամ"
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation-ը անկախ մեդիաների, մարդու իրավունքների պաշտպան կազմակերպությունների, հետաքննող լրագրողների ու ակտիվիստների համար անվտանգության լուծումների մատակարար է։ Qurium-ն առաջարկում է պրոֆեսիոնալ, անհատականացված և ապահով լուծումներ ռիսկի տակ գտնվող կազմակերպությունների ու անհատների համար։ Նման ծառայությունների օրինակներ են՝

- Անվտանգ հոսթինգ ռիսկային կայքերի համար, DDoS-ի դեմ քայլերի ձեռնարկում
- Արագ արձագանքման աջակցություն անմիջական սպառնալիքի տակ գտնվող կազմակերպություններին ու անհատներին
- Վեբ ծառայությունների և բջջային հավելվածների անվտանգության ստուգումներ
- Կայքերի արգելափակման շրջանցում
- Թվային հարձակումների, խարդախ հավելվածների, վնասակար ծրագրերի ու ապատեղեկատվության հետաքննություններ
