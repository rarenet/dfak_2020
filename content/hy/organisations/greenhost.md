---
language: hy
layout: organisation
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: "24 ժամ, ՀԿԺ+2"
response_time: "4 ժամ"
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost-ն առաջարկում է էթիկական ու հուսալի ՏՏ ծառայություններ։ Ծառայությունների շարքը ներառում է վեբ հոսթինգ, ամպային պահեստավորում, տեղեկատվական անվտանգության ոլորտում ֆունկցիոնալ նիշային լուծումներ։ Greenhost-ն ակտիվորեն ներգրավված է բաց կոդով ծրագրերի ստեղծման, ինչպես նաև տարբեր նախագծերում տեխնոլոգիայի, լրագրության, մշակույթի, կրթության, կայունության և ինտերնետի ազատության հարցերում։
