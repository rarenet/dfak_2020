---
language: hy
layout: organisation
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship, physical_sec
beneficiaries: hrds, hros
hours: "շտապ գիծ 24/7, աշխարհով մեկ; կանոնավոր աշխատանք երկուշաբթիից ուրբաթ՝ աշխատանքային ժամերին, IST (ՀԿԺ+1), անձնակազմը տեղակայված է տարբեր շրջաններում և տարբեր ժամային գոտիներում"
response_time: "նույն կամ հաջորդ օրը հրատապ հարցի դեպքում"
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

Front Line Defenders-ը միջազգային կազմակերպություն է, որը հիմնված է Իռլանդիայում։ Կազմակերպությունը արագ ու գործնական աջակցություն է ցուցաբերում անմիջական վտանգի տակ գտնվող մարդու իրավունքի պաշտպաններին՝ դրամաշնորհների, ֆիզիկական և թվային անվտանգության ուսուցման, շահերի պաշտպանության ու քարոզարշավների միջոցով։

Front Line Defenders-ի շտապ օգնության թեժ գիծը գործում է 24 ժամ, +353-121-00489 հեռախոսահամարով՝ արաբերեն, անգլերեն, ֆրանսերեն, ռուսերեն կամ իսպաներեն լեզուներով։ Երբ մարդու իրավունքի պաշտպանները բախվում են իրենց կյանքին սպառնացող անմիջական վտանգի հետ, Front Line Defenders-ը կարող է օգնել նրանց ժամանակավոր վերատեղակայման հարցում։ Կազմակերպությունը տրամադրում է ֆիզիկական ու թվային անվտանգության դասընթացներ։ Այն նաև հրապարակում է ռիսկի տակ գտնվող մարդու իրավունքի պաշտպանների դեպքերն ու քարոզարշավներ է անցկացնում միջազգային մակարդակով՝ ուղղակիորեն աշխատելով ԵՄ-ի, ՄԱԿ-ի, միջտարածաշրջանային գործակալությունների և կառավարությունների հետ՝ մարդու իրավունքների համար պայքարողների պաշտպանության նպատակով։
