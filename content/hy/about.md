---
layout: page
title: “Մեր մասին“
language: hy
summary: “Թվային առաջին օգնության հավաքածուի մասին։”
date: 2023-05
permalink: about
parent: Home
---

# Մեր մասին

«Թվային առաջին օգնության հավաքածուն» [RaReNet-ի (Արագ արձագանքման ցանցի)](https://www.rarenet.org/)-ի և [CiviCERT-ի](https://www.civicert.org/) համատեղ աշխատանքի արդյունքն է։

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

«Արագացված պատասխանի համակարգը» թվային անվտանգության մասնագետների և արագ արձագանքման ոլորտում աշխատող անհատների ու հզոր կազմակերպությունների միջազգային ցանց է։ Դրանցից են Access Now-ը, Amnesty Tech-ը, Center for Digital Resilience, CIRCL, EFF, Fembloc, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund և Virtualroad կազմակերպությունները։ Համակարգի մաս են կազմում նաև թվային անվտանգության և արագ արձագանքման ոլորտում մասնագիտացած անհատներ։

Տվյալ կազմակերպություններից ու անհատներից ոմանք նաև CiviCERT-ի՝ թվային անվտանգության հարցերով աջակցող և ենթակառուցվածքներ առաջարկող միջազգային ցանցի մաս են կազմում։ CiviCERT-ն առաջին հերթին աջակցում է մարդկանց և խմբերին, որոնք աշխատում են սոցիալական արդարության, մարդու իրավունքների ու թվային իրավունքների պաշտպանության համար։ CiviCERT-ը պրոֆեսիոնալ հարթակ է արագ արձագանքման համայնքի՝ CERT-ի (Թվային արտակարգ իրավիճակների արձագանքման թիմի) կողմից։ Այն հավատարմագրված է համակարգչային արտակարգ իրավիճակների արձագանքման թիմերի եվրոպական ցանցի՝ Trusted Introducer-ի կողմից։

Թվային առաջին օգնության հավաքածուն նաև [բաց աղբյուր է, որին կարելի է նվիրաբերում կատարել](https://gitlab.com/rarenet/dfak_2020)։

[Metamorphosis Foundation-ի](https://metamorphosis.org.mk) շնորհիվ [կայացել է ծրագրի ալբաներեն լոկալիզացիան](https://digitalfirstaid.org/sq/), [EngageMedia-ի](https://engagemedia.org/) շնորհիվ՝ [բիրմայերենի](https://digitalfirstaid.org/my/), [ինդոնեզերենի](https://digitalfirstaid.org/id/) ու [թայերենի](https://digitalfirstaid.org/th/), և շնորհիվ [ԶԼՄ-ների բազմազանության ինստիտուտ](https://mdi.am/en/home) համար [հայերեն](https://digitalfirstaid.org/hy/) Թվային առաջին օգնության հավաքածուն տեղայնացման ընթացքի մեջ է։

Կարող եք նաև ներբեռնել առաջին օգնության հավաքածուի [օֆլայն տարբերակը](https://digitalfirstaid.org/dfak-offline.zip), եթե ցանկանում եք այն օգտագործել սահմանափակ կապի պայմաններում։

Մեկնաբանություններ, առաջարկներ կամ հարցեր կարող եք ուղղել dfak @ digitaldefenders . org հասցեով։

GPG Մատնահետք. 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
