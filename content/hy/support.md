---
layout: support
language: hy
permalink: support
type: support
---

Կայքեր, որոնք տրամադրում են տարբեր տեսակի աջակցություն։ Ավելին իմանալու համար, սեղմեք կայքի անվանման վրա։
