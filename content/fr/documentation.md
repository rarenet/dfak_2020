---
layout: page
title: "Documenter des attaques numériques"
author: Constanza Figueroa, Patricia Musomba, Candy Rodríguez, Gus Andrews, Alexandra Hache, Nina
language: fr
summary: "Conseils pour documenter différents types d'urgences numériques."
date: 2023-05
permalink: documentation
parent: /fr/
sidebar: >
  <h3>En savoir plus sur la manière de documenter les urgences numériques:</h3>
  <ul>
    <li><a href="https://acoso.online/barbados/report-to-law-enforcement/#general-tips">Acoso.online general tips, report a case and how to keep evidence</a></li>
    <li><a href="https://www.techsafety.org/documentationtips">National Network to End Domestic Violence: Documentation Tips for Survivors of Technology Abuse & Stalking</a></li>
    <li><a href="https://chayn.gitbook.io/how-to-build-a-domestic-abuse-case-without-a-lawye/english">Chayn: How to Build a Domestic Abuse Case Without a Lawyer</a></li>
    </ul>
---

# Documenter des attaques numériques

Documenter les attaques numériques peut servir plusieurs objectifs. L'enregistrement de ce qui se passe lors d'une attaque peut vous aider à :

- mieux comprendre la situation et prendre des mesures pour vous protéger.
- fournir des preuves dont les techniciens ou les juristes auront besoin pour vous aider.
- mieux comprendre le schéma des attaques et identifier d'autres menaces.
- vous assurer une plus grande tranquillité d'esprit et comprendre l'impact émotionnel des attaques sur vous.

Qu'il s'agisse d'une documentation pour votre propre compréhension ou pour obtenir une aide juridique ou technique, un enregistrement structuré des attaques peut vous aider à mieux comprendre :

- L'ampleur et la portée de l'attaque : s'il s'agit d'une attaque unique ou d'un schéma répété, qui vous vise ou qui vise un groupe de personnes plus important.
- Les habitudes de l'attaquant : s'il est isolé ou s'il fait partie d'un groupe organisé, s'il utilise des informations disponibles sur l'internet ou accède à vos informations personnelles privées, les tactiques et technologies qu'il peut utiliser, etc.
- Si les réponses que vous envisagez de mettre en œuvre réduiront les menaces physiques ou en ligne qui pèsent sur vous, ou les aggraveront.

Lorsque vous commencez à rassembler des informations, tenez compte des points suivants :

- Est-ce que vous documentez principalement pour votre propre tranquillité d'esprit et/ou pour comprendre ce qui se passe ?
- Souhaitez-vous obtenir une assistance technique pour mettre fin aux attaques ? Veuillez envisager de visiter la section ["Trouver de l'aide" de la trousse de premiers soins numériques](/fr/support/) pour voir quelles organisations CiviCERT peuvent vous aider.
- Souhaitez-vous également intenter une action en justice ?

En fonction de vos objectifs, vous devrez peut-être documenter les attaques de manières légèrement différentes. Si vous avez besoin de preuves pour une action en justice, vous devrez rassembler des éléments de preuve spécifiques qui sont légalement admissibles devant un tribunal, tels que des numéros de téléphone et des horodatages. Les experts techniques auront besoin d'autres preuves, comme des adresses web (URL), des noms d'utilisateurs et des captures d'écran. Voir les sections ci-dessous pour plus de détails sur les informations à collecter pour chacun de ces chemins possibles.

## Protégez votre bien-être mental et émotionnel tout en documentant

Le traitement des attaques numériques est stressant, et votre bien-être physique et émotionnel doit être votre première priorité, quel que soit le type de documentation que vous souhaitez obtenir. La documentation ne doit pas ajouter à votre stress. Tenez compte des éléments suivants lorsque vous commencez à documenter une attaque :

- Il existe de nombreuses façons de documenter et d'enregistrer. Choisissez celles avec lesquelles vous vous sentez à l'aise et qui correspondent à vos objectifs de documentation.
- Documenter les attaques numériques peut être émotionnellement exigeant et déclencher des souvenirs traumatisants. Si vous vous sentez dépassé·e, envisagez de déléguer la documentation à un·e collègue, un·e ami·e, une autre personne de confiance ou un groupe de surveillance qui pourra tenir à jour la documentation sur les attaques sans être à nouveau traumatisé·e.
- Si l'attaque est collective, envisagez de répartir les responsabilités en matière de documentation entre plusieurs personnes.
- Le consentement est essentiel. La documentation doit se poursuivre tant que la personne ou le groupe concerné est d'accord.
- [Voici d'autres conseils pour inciter vos amis ou votre famille à vous aider](https://onlineharassmentfieldmanual.pen.org/fr/parler-du-cyberharcelement-a-vos-proches/)

Documenter les attaques numériques ou la violence sexiste en ligne signifie rassembler des informations sur ce à quoi vous ou votre collectif êtes confrontés. Cette documentation ne doit pas seulement être rationnelle et technique. Elle peut également vous aider à traiter la violence en enregistrant ce que vous ressentez à propos de chaque attaque sous forme de texte, d'image, de son, de vidéo ou même d'expression artistique. Nous vous recommandons de le faire hors ligne et de prendre les mesures décrites ci-dessous pour protéger votre vie privée pendant que vous traitez ces sentiments.

## Préparez-vous à documenter une attaque

Après vous être assuré·e de votre propre bien-être, mais avant de rassembler des éléments de preuve individuels, **la réalisation d'une "carte" de toutes les informations pertinentes** concernant l'agression peut vous aider à identifier tous les éléments de preuve que vous pourriez vouloir rassembler.

La partie la plus importante de la documentation est **la tenue d'un registre organisé (ou "log")**. Dans le cas d'une documentation juridique ou technique, il est important que le journal ne contienne que des éléments clés pour aider à mettre en évidence des schémas d'attaque clairs. Lorsque vous documentez pour votre propre tranquillité d'esprit, un document texte ordinaire moins structuré peut vous suffire. Un journal plus structuré peut également vous aider à identifier des schémas dans les attaques auxquelles vous êtes confronté·e. Votre journal numérique peut être un document texte ou une feuille de calcul. Vous pouvez également tenir un journal sur papier. Le choix vous appartient.

**Toutes les interactions numériques laissent des traces (métadonnées) qui les décrivent** : Heure, durée, adresses, comptes d'envoi et de réception, etc. Ces traces sont enregistrées dans des journaux par vos propres appareils, par les sociétés de téléphonie et de médias sociaux, par les fournisseurs d'accès à internet et par d'autres. Leur analyse peut fournir aux experts juridiques et techniques des informations essentielles sur les auteurs d'une attaque. Par exemple, un enregistrement du ou des numéros de téléphone utilisés lors de 20 appels au cours d'une période donnée pourrait étayer un rapport de harcèlement.

La tenue d'un registre organisé et structuré de ces données vous aidera à démontrer les **modèles d'attaques** de manière à étayer un dossier juridique ou à aider un expert en sécurité numérique à bloquer les attaques et à protéger vos appareils.

Les informations suivantes seront utiles dans la plupart des cas, mais chaque cas peut être différent. Vous pouvez copier et coller les champs ci-dessous en haut d'une feuille de calcul ou d'un tableau pour structurer vos rapports sur chaque attaque, en ajoutant des champs supplémentaires si vous le jugez nécessaire.

| Date | Heure | Adresses électroniques ou numéros de téléphone utilisés par l'attaquant | Liens liés à l'attaque | Noms (y compris les noms de comptes) utilisés par l'attaquant | Types d'attaques numériques, de harcèlement, de désinformation, etc. | Techniques utilisées par l'auteur de l'attaque |
| ---- | ----- | ----------------------------------------------------------------------- | ---------------------- | ------------------------------------------------------------- | -------------------------------------------------------------------- | ---------------------------------------------- |

Voici quelques exemples d'autres modèles de journaux de notre communauté que vous pouvez utiliser :

- [Modèle de journal d'incident du service d'assistance téléphonique sur la sécurité numérique d'Access Now](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/tree/master/templates/incident_log_template.md)
- [Modèle de violence sexiste d'Acoso.online](https://acoso.online/site2022/wp-content/uploads/2019/01/reactingNCP.pdf)

En plus d'un fichier journal, **conservez tout ce qui concerne l'événement ou l'incident** dans un ou plusieurs dossiers - y compris votre document journal et toute preuve numérique supplémentaire que vous avez exportée, téléchargée ou capturée par le biais d'une capture d'écran. Même les preuves qui n'ont pas de valeur juridique peuvent être utiles pour montrer l'ampleur d'une attaque et vous aider à planifier des stratégies de réponse.

**Stockez les informations que vous recueillez en toute sécurité**. Il est plus sûr de stocker les sauvegardes sur vos propres appareils (et pas seulement en ligne ou "dans le nuage"). Protégez ces fichiers à l'aide d'un système de chiffrement et dans des dossiers cachés ou des partitions de disque si possible. Vous ne voulez pas perdre ces dossiers ou, pire, les voir exposés.

Sur les médias sociaux, **évitez de bloquer, de mettre en sourdine ou de signaler les comptes qui vous attaquent** jusqu'à ce que vous ayez rassemblé la documentation dont vous avez besoin, car cela pourrait vous empêcher de sauvegarder des preuves provenant de ces comptes. Si vous avez déjà bloqué, mis en sourdine ou signalé des comptes sur les médias sociaux, ne vous inquiétez pas. Il se peut que vous puissiez débloquer des comptes que vous ne pouvez plus voir, ou que vous puissiez encore vous documenter en consultant ce compte à partir du compte d'un ami ou d'un collègue.

## Prendre des captures d'écran

Ne sous-estimez pas l'importance de faire des captures d'écran de toutes les attaques (ou de demander à une personne de confiance de le faire pour vous). De nombreux messages numériques sont faciles à supprimer ou à perdre de vue. D'autres, comme les tweets ou les messages WhatsApp, ne peuvent être récupérés qu'avec l'aide des plateformes, qui mettent souvent beaucoup de temps à répondre aux signalements d'attaques, quand elles y répondent.

Si vous ne savez pas comment faire une capture d'écran sur votre appareil, effectuez une recherche en ligne sur "comment faire une capture d'écran" en indiquant la marque, le modèle et le système d'exploitation de votre appareil (par exemple, "Samsung Galaxy S21 Android comment faire une capture d'écran").

Si vous faites une capture d'écran d'un message dans un navigateur web, il est important **d'inclure l'adresse (URL) de la page dans l'image capturée**. Cette adresse se trouve dans la barre d'adresse en haut de la fenêtre du navigateur ; parfois, les navigateurs la cachent et l'affichent au fur et à mesure que vous faites défiler l'écran. Les adresses permettent de vérifier où le harcèlement s'est produit et aident les professionnels techniques et juridiques à le localiser plus facilement. Vous trouverez ci-dessous un exemple de capture d'écran montrant l'URL.

<img src="/images/screenshot_with_URL.png" alt="Voici un exemple de capture d'écran montrant l'URL." width="80%" title="Voici un exemple de capture d'écran montrant l'URL.">

<a name="legal"></a>

## Documentation pour une affaire judiciaire

Réfléchissez à l'opportunité d'engager une action en justice. Cela vous exposera-t-il à un risque supplémentaire ? Pouvez-vous vous permettre le temps et les efforts nécessaires ? Il n'est pas toujours obligatoire ou nécessaire d'intenter une action en justice. Demandez-vous également si une action en justice vous semble réparatrice.

Si vous décidez de porter l'affaire devant les tribunaux, demandez conseil à un avocat ou à une organisation juridique en qui vous avez confiance. Un mauvais conseil juridique peut être stressant et préjudiciable. N'entreprenez pas d'action en justice de votre propre chef si vous ne savez pas vraiment ce que vous faites.

Dans la plupart des juridictions, un avocat devra démontrer que les preuves sont pertinentes pour l'affaire, comment elles ont été obtenues et vérifiées, qu'elles ont été collectées de manière légale, que leur collecte était nécessaire à la constitution du dossier, et d'autres faits qui rendent les preuves acceptables pour un tribunal. Une fois que l'avocat a démontré cela, les preuves seront évaluées par un juge ou un tribunal.

Éléments à prendre en compte lors de la documentation en vue d'une procédure judiciare :

- Il est essentiel de conserver les preuves rapidement, dès le début de l'agression, car le contenu peut être rapidement supprimé, ce qui rend plus difficile la sauvegarde des preuves en vue d'une action en justice ultérieure.
- Même si l'agression semble mineure, il est important de conserver toutes les preuves. Elle peut faire partie d'un schéma qui ne s'avérera criminel que lorsque vous l'examinerez du début à la fin.
- Vous pouvez demander aux plateformes ou aux fournisseurs d'accès à Internet de conserver pour vous les informations relatives à l'attaque numérique. Il se peut que cela ne soit possible que pendant une courte période (quelques semaines ou un mois) après l'événement. Voir [les informations de Without My Consent sur le dépôt d'une demande de mise en suspens](https://withoutmyconsent.org/resources/something-can-be-done-guide/evidence-preservation/#consider-whether-to-include-a-litigation-hold-request) pour plus d'informations sur la manière de procéder.
- Les horodatages et les adresses électroniques et web sont essentiels pour que les preuves soient acceptées par les tribunaux. Les avocats doivent démontrer qu'un message est passé d'un appareil à un autre un jour donné à une heure donnée en utilisant ces outils.
- Les avocats doivent également vérifier que les preuves n'ont pas été altérées, qu'elles sont restées entre de bonnes mains depuis que vous les avez stockées et qu'elles sont authentiques. Les captures d'écran et les impressions d'e-mails ne sont pas considérées comme des preuves suffisamment solides à cet égard.

Pour ces raisons juridiques, il peut être préférable de faire appel à un expert, tel qu'un notaire ou une société de certification numérique, qui pourra témoigner devant un tribunal si nécessaire. Les sociétés de certification numérique sont souvent moins chères que les certificats notariés. Le notaire ou la société de certification que vous engagez doit disposer d'un bagage technique suffisant pour effectuer des tâches telles que les suivantes, le cas échéant :

- confirmer l'horodatage
- vérifier l'existence ou la non-existence de certains contenus, par exemple en comparant vos captures d'écran et d'autres preuves recueillies à la source originale, afin de confirmer que vos preuves n'ont pas été altérées
- fournir la preuve que les certificats numériques correspondent aux URL
- vérifier les identités, par exemple en vérifiant si une personne apparaît dans une liste de profils sur une plateforme de médias sociaux et si elle possède un profil avec le surnom qui vous attaque
- confirmer la propriété d'un numéro de téléphone dans une conversation WhatsApp.

Sachez que toutes les preuves que vous avez recueillies ne seront pas acceptées par le tribunal. Renseignez-vous sur les procédures juridiques relatives aux menaces numériques dans votre pays et votre région pour décider de ce que vous allez rassembler.

### Documenter les violations des droits de l’homme

Si vous avez besoin de documenter des violations de droits de l'homme et de télécharger ou de copier des documents que les plateformes de médias sociaux suppriment activement, vous pouvez contacter des organisations telles que [Mnemonic](https://mnemonic.org/en/our-work) qui peuvent aider à préserver ces documents.

## Comment enregistrer des preuves pour les rapports médico-légaux juridiques et numériques

Au delà de l'enregistrement de captures d'écran, voici quelques instructions supplémentaires sur la manière d'enregistrer des preuves plus complètes pour étayer votre dossier et faciliter le travail d'une personne vous apportant une assistance juridique ou technique.

- **Historique d'appels.** Les numéros des appels entrants et sortants sont stockés dans une base de données sur votre téléphone portable.
  - Vous pouvez faire des captures d'écran de cet historique.
  - Il est également possible de sauvegarder ces journaux en sauvegardant le système de votre téléphone ou en utilisant un logiciel spécifique pour télécharger ces historiques.
- **Messages textuels (SMS)** : La plupart des téléphones mobiles vous permettent de sauvegarder vos SMS dans le cloud. Vous pouvez également en faire des captures d'écran. Aucune de ces méthodes ne constitue une preuve légale sans vérification supplémentaire, mais elles sont néanmoins importantes pour documenter les attaques.
  - Android : si votre téléphone ne permet pas de sauvegarder les SMS, vous pouvez utiliser une application telle que [SMS Backup & restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore).
  - iOS : Activez la sauvegarde des messages ici : iCloud dans Réglages > [votre nom d'utilisateur] > Gérer le stockage > Sauvegarde. Appuyez sur le nom de l'appareil que vous utilisez et activez la sauvegarde des messages.
- **Enregistrement des appels**. Les téléphones portables les plus récents sont équipés d'un enregistreur d'appels. Vérifiez les règles légales en vigueur dans votre pays ou votre juridiction locale concernant l'enregistrement d'appels avec ou sans le consentement des deux parties ; dans certains cas, cela peut être illégal. Si cela est légal, vous pouvez configurer l'enregistreur d'appels pour qu'il s'active lorsque vous répondez à l'appel, sans avertir l'appelant qu'il est enregistré.
  - Android : dans Paramètres > Téléphone > Paramètres d'enregistrement des appels, vous pouvez activer l'enregistrement automatique de tous les appels, des appels provenant de numéros inconnus ou de numéros spécifiques. Si cette option n'est pas disponible sur votre appareil, vous pouvez télécharger une application telle que [Call Recorder](https://play.google.com/store/apps/details?id=com.lma.callrecorder).
  - iOs : Apple est plus restrictif en ce qui concerne l'enregistrement des appels et a bloqué l'option par défaut. Vous devez installer une application telle que [RecMe](https://apps.apple.com/fr/app/call-recorder-recme/id1455818490).
- **Emails**. Chaque courriel comporte un "en-tête" qui contient des informations sur l'expéditeur du message et sur la manière dont il a été envoyé, comme les adresses et le cachet de la poste sur une lettre papier.
  - Pour obtenir des instructions sur l'affichage et l'enregistrement des en-têtes de courrier électronique, vous pouvez utiliser [ce guide du Computer Incident Response Center Luxembourg (CIRCL)](https://www.circl.lu/pub/tr-07/).
  - Si vous disposez de plus de temps, vous pouvez également configurer votre messagerie de manière à ce que tous les messages soient téléchargés vers un client de bureau tel que Thunderbird, à l'aide de [POP3](https://support.mozilla.org/fr/kb/differences-imap-pop3#w_modifier-votre-compte). Les instructions relatives à la création d'un compte de messagerie sur Thunderbird se trouvent dans la [documentation officielle](https://support.mozilla.org/fr/kb/configurer-un-compte-manuellement#).
- **Photos**. Toutes les photos ont des "balises EXIF" - des métadonnées qui peuvent vous indiquer où et quand une photo a été prise.
  - Vous pouvez enregistrer des images à partir d'un site web ou de messages que vous avez reçus de manière à sauvegarder les balises EXIF qui seraient perdues si vous faisiez simplement une capture d'écran. Appuyez sur l'image et maintenez-la appuyée sur un appareil mobile, ou faites un clic droit sur l'image sur un ordinateur (contrôle-clic sur un Mac, utilisez la touche menu sous Windows).
- **Sites web**. Le téléchargement ou la sauvegarde de pages web complètes peut s'avérer utile pour les personnes qui tentent de vous aider.
  - Si la page où le harcèlement s'est produit est publique (en d'autres termes, vous n'avez pas besoin de vous connecter pour la voir), vous pouvez saisir l'adresse de la page dans la [Wayback Machine de l'Internet Archive](https://web.archive.org/) pour sauvegarder la page, enregistrer la date à laquelle vous l'avez sauvegardée et revenir plus tard à la page sauvegardée à cette date.
  - Les captures d'écran sont moins détaillées, mais peuvent également permettre de conserver des informations importantes.
- **WhatsApp**. Les conversations WhatsApp peuvent être téléchargées au format texte ou avec des fichiers multimédias joints ; elles sont également sauvegardées par défaut sur iCloud ou Google Drive.
  - Suivez les instructions pour exporter l'historique d'une conversation individuelle ou collective [ici](https://faq.whatsapp.com/1180414079177245/?helpref=uf_share).
- **Telegram**. Pour exporter des conversations dans Telegram, vous devez utiliser l'application de bureau. Vous pouvez choisir le format et la période de temps que vous souhaitez exporter, et Telegram générera un fichier HTML. Voir les instructions [ici](https://telegram.org/blog/export-and-more?setln=fr).
- **Facebook Messenger**. Connectez-vous à votre compte Facebook. Allez dans Paramètres > Vos informations sur Facebook > Télécharger une copie de vos informations. Plusieurs options s'affichent : sélectionnez "Messages". Facebook prend du temps pour traiter le fichier, mais il vous informera lorsqu'il sera disponible pour le téléchargement.
- Si vous devez enregistrer des **vidéos** à titre de preuve, prévoyez un disque externe (disque dur, clé USB ou carte SD) disposant d'un espace de stockage suffisant. Vous devrez peut-être utiliser un outil de capture d'écran vidéo. L'application Camera, la barre de jeu XBox ou l'outil Snipping peuvent être utilisés à cette fin sous Windows, Quicktime sous Mac, ou vous pouvez utiliser un plugin de navigateur comme [Video DownloadHelper](https://www.downloadhelper.net/) pour sauvegarder les vidéos.
- Dans le cas d'attaques organisées à grande échelle - par exemple, tout ce qui est posté avec **un hashtag spécifique ou un grand nombre de commentaires en ligne** - vous pouvez avoir besoin de l'aide d'une équipe de sécurité, d'un laboratoire de criminalistique numérique ou d'une université pour collecter et traiter un grand nombre de données. Parmi les organisations susceptibles de vous aider, citons [Citizen Lab](https://citizenlab.ca/about/), [Fundacion Karisma's K-Lab](https://web.karisma.org.co/klab/) (en espagnol), [Meedan](https://meedan.com/programs/digital-health-lab), l'Observatoire de l'Internet de Stanford et l'Institut de l'Internet d'Oxford.
- Les **fichiers joints aux messages malveillants** sont des preuves précieuses. Vous ne devez en aucun cas cliquer sur ces fichiers ou les ouvrir. Un conseiller technique de confiance devrait pouvoir vous aider à contenir ces pièces jointes en toute sécurité et à les envoyer à des personnes capables de les analyser.
  - Vous pouvez utiliser [Danger Zone](https://dangerzone.rocks/) pour convertir des PDF, des documents bureautiques ou des images potentiellement dangereux en PDF sûrs.
- Pour les menaces plus ciblées, telles que les **logiciels espions** sur vos appareils ou les **courriels malveillants** qui vous sont spécifiquement adressés, la collecte de preuves techniques plus approfondies peut vous aider. Il est important de savoir quand rassembler des preuves soi-même et quand laisser cette tâche à des experts. Si vous n'êtes pas sûr de la marche à suivre, contactez un [conseiller de confiance](/fr/support).
  - **Si l'appareil est allumé, laissez-le allumé. S'il est éteint, laissez-le éteint**. En l'éteignant ou en le rallumant, vous risquez de perdre des informations importantes. Dans le cas d'enquêtes très sensibles et juridiques, il est préférable de faire appel à des experts en criminalistique avant d'éteindre ou de rallumer l'appareil.
  - Prenez des photos de l'appareil dont vous pensez qu'il contient un logiciel malveillant. Documentez son état physique et l'endroit où vous l'avez trouvé lorsque vous avez commencé à soupçonner qu'il avait été infecté. Y a-t-il des bosses ou des rayures ? Est-il mouillé ? Y a-t-il des outils à proximité qui auraient pu être utilisés pour le manipuler ?
  - Conservez l'appareil et les données que vous avez copiées dans un endroit sûr.
  - La méthode exacte d'extraction des données dépend de l'appareil. Extraire des données d'un ordinateur portable est différent d'extraire des données d'un smartphone. Chaque appareil nécessite des outils et des connaissances spécifiques.
- La collecte de données pour les menaces ciblées implique souvent de copier les **fichiers journaux du système** que votre téléphone ou votre ordinateur collectent automatiquement. Les experts techniques qui vous aident à collecter des données peuvent vous demander ces fichiers. Ils doivent vous aider à préserver en toute sécurité les métadonnées et les pièces jointes et à les envoyer à des personnes capables de les analyser.
  - Pour préserver ces métadonnées, isolez l'appareil des autres systèmes de stockage, désactivez le Wi-Fi et le Bluetooth, débranchez les connexions réseau filaires et ne modifiez jamais les fichiers journaux.
  - Ne branchez pas une clé USB et n'essayez pas d'y copier des fichiers journaux.
  - Ces fichiers peuvent contenir des informations sur l'état des fichiers sur l'appareil (comme la manière avec laquelle les fichiers ont été consultés) ou sur l'appareil (comme l'émission d'une commande d'arrêt ou de suppression, ou la tentative de copie de fichiers sur un autre appareil).
