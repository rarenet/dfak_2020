---
layout: contact-method
title: "Formulaire de contact"
author: mfc
language: fr
summary: "Méthodes de contact"
date: 2021-01-27
permalink: /fr/contact-methods/contact-form.md
parent: /fr/
published: true
---

L’utilisation d’un formulaire de contact préservera certainement la confidentialité du message que vous envoyez, afin que seul vous même et l’organisation contactée puissiez le lire. Ceci ne sera le cas que si le site internet hébergeant le formulaire de contact a mis en place des mesures de sécurité comme [TLS/SSL](https://ssd.eff.org/en/glossary/secure-sockets-layer-ssl) le chiffrement entre autres, ce qui est le cas avec les organisations du CiviCERT.

Par contre, le fait que vous ayez visité le site de l’organisation où est hébergé le formulaire de contact sera certainement traçable par les gouvernements, les agences des forces de l’ordre, et les autres partis ayant accès à des infrastructures de surveillances locales, régionales ou globales. Le fait que vous ayez visité le site de cette organisation pourrait suggérer que vous l’ayez contactée.

Si vous souhaitez que la visite de ce site reste privée et secrète (et le fait que vous les ayez potentiellement contacté), il est préférable d’accéder au site au travers du [navigateur Tor](https://www.torproject.org/fr/) ou d’un VPN ou proxy de confiance. Avant de faire cela, veuillez prendre en considération le contexte légal de l’endroit où vous résidez et si vous avez besoin de cacher et brouiller votre utilisation du navigateur Tor en [le configurant](https://tb-manual.torproject.org/fr/running-tor-browser/) avec un [outil de transport enfichable](https://tb-manual.torproject.org/fr/circumvention/). Si vous envisagez l’utilisation d’un VPN ou d’un proxy, [vérifiez où le VPN ou proxy](https://protonvpn.com/blog/vpn-servers-high-risk-countries/) est basé et [si vous faites confiance au fournisseur du VPN](https://ssd.eff.org/fr/module/choisir-le-rpv-qui-vous-convient).
