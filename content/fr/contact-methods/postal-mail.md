---
layout: contact-method
title: Courrier postal
author: mfc
language: fr
summary: "Méthodes de contact"
date: 2018-09
permalink: /fr/contact-methods/postal-mail.md
parent: /fr/
published: true
---

L'envoi du courrier est une méthode de communication lente si vous faites face à une situation d'urgence. Selon les juridictions où le courrier voyage, les autorités peuvent ouvrir le courrier, et elles pistent souvent l'expéditeur, le lieu d'envoi, le destinataire et le lieu de destination.

