---
layout: contact-method
title: Signal
author: mfc
language: fr
summary: "Méthodes de contact"
date: 2018-09
permalink: /fr/contact-methods/signal.md
parent: /fr/
published: true
---

L'utilisation de Signal garantit que le contenu de votre message est chiffré spécifiquement pour l'organisation destinataire et que seuls vous et votre destinataire êtes informés de l'envoi des communications. Sachez que Signal utilise votre numéro de téléphone comme nom d'utilisateur et que vous partagerez votre numéro de téléphone avec l'organisation avec laquelle vous communiquez.

Ressources : [Comment utiliser Signal pour Android](https://ssd.eff.org/fr/module/guide-pratique-utiliser-signal-pour-android), [Comment utiliser Signal pour iOS](https://ssd.eff.org/fr/module/guide-pratique-utiliser-signal-pour-ios), [Comment utiliser Signal sans donner votre numéro de téléphone](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)

