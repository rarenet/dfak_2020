---
layout: contact-method
title: PGP
author: mfc
language: fr
summary: "Méthodes de contact"
date: 2021-01-27
permalink: /fr/contact-methods/pgp.md
parent: /fr/
published: true
---

PGP (ou Pretty Good Privacy) et son équivalent open source, GPG (Gnu Privacy Guard), vous permet de chiffrer le contenu des emails pour protéger votre message du regard de votre fournisseur de messagerie ou de toute autre partie qui pourrait avoir accès au message. Toutefois, le fait que vous ayez envoyé un message à l'organisme destinataire peut être accessible par les gouvernements ou les organismes d'application de la loi. Pour éviter cela, vous pouvez créer une adresse e-mail alternative non associée à votre identité.


Ressources: [La documentation publique d’assistance à la sécurité numérique d’Access Now : Email sûr](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

[Chiffrer des e-mails avec Mailvelope: guide du débutant](https://www.mailvelope.com/fr/help)

[Outils de confidentialité: Fournisseurs indépendants d’Email](https://privacytools.dreads-unlock.fr/#email)
