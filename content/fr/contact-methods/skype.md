---
layout: contact-method
title: Skype
author: mfc
language: fr
summary: "Méthodes de contact"
date: 2018-09
permalink: /fr/contact-methods/skype.md
parent: /fr/
published: true
---

Le contenu de votre message ainsi que le fait que vous avez communiqué avec l'organisation peuvent être accessibles par les gouvernements ou les forces de l'ordre.
