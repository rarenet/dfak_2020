---
layout: topic
title: "J'ai reçu des messages suspects"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: fr
summary: "J'ai reçu un message, un lien ou un e-mail suspect, que dois-je faire ?"
date: 2023-05
permalink: topics/suspicious-messages
parent: /fr/
order: 4
---

# J'ai reçu des messages suspects

Vous pouvez recevoir des messages suspicieux dans votre boîte de réception d'emails, vos comptes de médias sociaux et/ou vos applications de messagerie. La forme la plus courante de courriels suspects est l'hameçonnage. Les courriels d'hameçonnage visent à vous piéger et à vous amener à donner des renseignements personnels, financiers ou sur vos comptes en ligne. Ils peuvent vous demander de visiter un faux site Web ou d'appeler un faux numéro de service clientèle. Les courriels d'hameçonnage peuvent également contenir des pièces jointes qui installent des logiciels malveillants sur votre ordinateur lorsqu'elles sont ouvertes.

La forme la plus courante de messages électroniques suspects est le phishing, mais vous pouvez également recevoir des messages suspects sur des comptes de médias sociaux et/ou via des applications de messagerie. Les messages d'hameçonnage (phising) visent à vous inciter à fournir des informations personnelles, financières ou sur vos comptes en ligne. Ils peuvent vous demander de visiter un faux site web ou d'appeler un faux numéro de service clientèle. Les messages d'hameçonnage peuvent également contenir des pièces jointes qui installent des logiciels malveillants sur votre ordinateur lorsqu'elles sont ouvertes.

Si vous n'êtes pas certain de l'authenticité du message que vous avez reçu ou de ce qu'il faut faire à son sujet, vous pouvez utiliser le questionnaire suivant comme outil pour diagnostiquer la situation ou pour partager le message avec des organisations externes de confiance qui vous fourniront une analyse plus détaillée de celui-ci.

N'oubliez pas que le fait de recevoir un courriel suspect ne signifie pas nécessairement que votre compte a été compromis. Si vous pensez qu'un courriel ou un message est suspect, ne l'ouvrez pas. Ne répondez pas au message, ne cliquez sur aucun lien et ne téléchargez aucune pièce jointe.

## Workflow

### intro

#### Avez-vous effectué une action sur le message ou un lien ?

##### Options

- [J'ai cliqué sur un lien](#link-clicked)
- [J'ai entré des informations d'identification](#account-security_end)
- [J'ai téléchargé un fichier](#device-security_end)
- [J'ai répondu avec des informations](#reply-personal-info)
- [Je n'ai encore rien fait](#do-you-know-sender)

### do-you-know-sender

#### Je n'ai encore rien fait

Reconnaissez-vous l'expéditeur·rice du message ? Sachez que l'identité de l'expéditeur·rice du message peut être [usurpée](https://fr.wikipedia.org/wiki/Usurpation_de_courrier_%C3%A9lectronique) pour donner l'impression d'être quelqu'un·e en qui vous avez confiance.

##### Options

- [C'est une personne ou une organisation que je connais](#known-sender)
- [Il s'agit d'un fournisseur de services (tel qu'un fournisseur de courriel, un hébergeur, un fournisseur de médias sociaux ou une banque)](#service-provider)
- [Le message a été envoyé par une personne ou une organisation inconnue](#share)

### known-sender

#### C'est une personne ou une organisation que je connais

> Pouvez-vous joindre l'expéditeur·rice en utilisant un autre canal de communication ? Par exemple, si vous avez reçu un courriel, pouvez-vous le vérifier directement auprès de l'expéditeur·rice par Signal, WhatsApp ou par téléphone ? Veillez à utiliser une méthode de contact déjà éprouvée. Vous ne pouvez pas nécessairement faire confiance à un numéro de téléphone figurant dans le message suspect.

Avez-vous confirmé que c'est l'expéditeur·rice qui vous a envoyé ce message ?

##### Options

- [Oui](#resolved_end)
- [Non](#share)

### service-provider

#### Il s'agit d'un fournisseur de services (tel qu'un fournisseur de courriel, un hébergeur, un fournisseur de médias sociaux ou une banque)

> Dans ce scénario, un fournisseur de services est toute entreprise ou marque qui fournit des services que vous utilisez ou auxquels vous êtes abonné. Cette liste peut contenir votre fournisseur de messagerie (Google, Yahoo, Microsoft, Prontonmail...), votre fournisseur de médias sociaux (Facebook, Twitter, Instagram...), ou des plateformes en ligne contenant vos informations financières (Paypal, Amazon, banques, Netflix...).
>
> Avez-vous un moyen de vérifier si le message est authentique ? De nombreux fournisseurs de services vous fourniront également des copies d'avis ou d'autres documents dans la page de votre compte. Par exemple, si le message provient de Facebook, il doit être inclus dans votre [liste des emails de notification](https://www.facebook.com/settings?tab=security&section=recent_emails=security&section=recent_emails), ou s'il provient de votre banque, vous pouvez appeler votre service clientèle.

Choisissez l'une des options ci-dessous :

##### Options

- [J'ai pu vérifier qu'il s'agit d'un message légitime de mon fournisseur de services](#resolved_end)
- [Je n'ai pas pu vérifier le message](#share)
- [Je ne suis pas abonné à ce service ni en attente d'un message de leur part](#share)

### link-clicked

#### J'ai cliqué sur un lien

> Dans certains messages suspects, les liens peuvent vous conduire à de fausses pages de connexion qui voleront vos identifiants ou à d'autres types de pages qui pourraient voler vos informations personnelles ou financières. Parfois, le lien peut vous demander de télécharger des pièces jointes qui installent des logiciels malveillants sur votre ordinateur lorsqu'elles sont ouvertes. Le lien peut également vous conduire à un site web spécialement préparé qui peut tenter d'infecter votre appareil avec un logiciel malveillant ou d'espionnage.

Pouvez-vous dire ce qui s'est passé après que vous ayez cliqué sur le lien ?

##### Options

- [Il m'a demandé d'entrer des informations d'identification](#account-security_end)
- [Un fichier a été téléchargé](#device-security_end)
- [Il ne s'est rien passé, mais je ne suis pas sûr·e](#clicked_but_nothing_happened)

### clicked-but-nothing-happened

#### Il ne s'est rien passé, mais je n'en suis pas sûr.e.

> Le fait que vous ayez cliqué sur un lien suspect et que vous n'ayez pas remarqué de comportement étrange ne signifie pas qu'aucune action malveillante n'ait eu lieu en arrière-plan. Vous devriez envisager différents scénarios. Le moins inquiétant est que le message que vous avez reçu est un spam utilisé à des fins publicitaires. Dans ce cas, des publicités peuvent apparaître. Dans certains cas, ces publicités peuvent également être malveillantes.

- [Des publicités sont apparues. Je ne suis pas sûr·e qu'elles soient malveillantes ou non](#suspicious_device_end)

> Dans le pire des cas, en cliquant sur le lien, vous avez déclenché l’exécution d’une commande malveillante sur votre système. Si cela se produit, cela peut être dû au fait que votre navigateur n'est pas à jour et présente une vulnérabilité permettant ce déclenchement. Dans les rares cas où votre navigateur est à jour et où ce scénario s'est produit, la vulnérabilité exploitée pourrait ne pas être connue. Dans les deux cas, votre appareil pourrait commencer à agir de manière suspecte.
>
> Dans d'autres scénarios, en visitant ce lien, vous avez peut-être été victime d'une [attaque dite de cross-site scripting (XSS)](https://fr.wikipedia.org/wiki/Cross-site_scripting). Le résultat de cette attaque sera le vol de votre cookie utilisé pour vous authentifier sur le site visité, de sorte que l'attaquant·e pourra se connecter au site avec votre nom d'utilisateur·rice. En fonction de la sécurité du site, l'attaquant·e pourra ou non modifier le mot de passe. Cela devient plus grave si le site web qui est vulnérable au XSS est un site que vous gérez, car dans ce cas, l'attaquant·e pourra s'authentifier en tant qu'administrateur·rice de votre site web. Afin d'identifier une attaque XSS, vérifiez si le lien sur lequel vous avez cliqué contient une [chaîne de script](https://owasp.org/www-community/attacks/xss/). Celle-ci peut également être encodée en HEX ou en Unicode.

##### Options

- [Des publicités sont apparues, je ne suis pas sûr qu'elles soient malveillantes ou non](#suspicious-device_end)
- [Oui, mon navigateur n'est pas à jour et/ou mon appareil a commencé à agir de manière suspecte après que j'ai cliqué sur le lien](#suspicious-device_end)
- [Il y a un script dans le lien ou il est partiellement encodé](#cross_site_script_1)
- [Aucun script n’a pu être identifié](#suspicious_device_end)

### cross-site-script_1

#### Est-ce que le site que vous visitiez permettait de se loguer avec un compte ?

##### Options

- [Oui](#account_security_end)
- [Non](#cross-site-script-2)

### cross-site-script-2

#### Est-ce que le lien vous conduit à un site web que vous administrez ?

##### Options

- [Oui](#cross_site_script_admin_compromised)
- [Non](#cross-site-script-3)

### cross-site-script-admin-compromised_end

#### Je gère le site web sur lequel le lien sur lequel j'ai cliqué mène

> Dans ce cas, l'attaquant·e peut avoir un cookie valide qui lui permet d'accéder à votre compte admin. La première chose à faire est de se connecter à votre interface d'administration et de supprimer toute session active ou simplement de changer le mot de passe. Vous devez également vérifier si l'attaquant·e a modifié des choses sur votre site et/ou a publié du contenu malveillant, et si c'est le cas, le supprimer.

Les organisations suivantes peuvent aider à enquêter et à réagir à cet incident :

[orgs](:organisations?services=forensic)

### cross-site-script-3

#### Non, je ne gère pas ce site web

> Cela devrait être ok. Cependant, dans de rares cas, le XSS pourrait servir à utiliser votre navigateur afin de lancer d'autres attaques.

##### Options

- [Je veux évaluer si mon appareil a été infecté](/fr/topics/device-acting-suspiciously)
- [Je pense que je suis ok](#final_tips)

### reply-personal-info

#### J'ai répondu avec des informations

> Selon le type d'informations que vous avez partagées, il se peut que vous deviez prendre des mesures immédiates.

Quel genre d'informations avez-vous partagées ?

##### Options

- [J'ai partagé des informations de compte confidentielles](#account-security_end)
- [J'ai partagé des informations publiques](#share)
- [Je ne suis pas sûr·e à quel point les informations étaient sensibles et j'ai besoin d'aide](#help_end)

### share

#### Partager vos informations ou le message suspect

> Partager votre message suspect peut contribuer à protéger vos collègues et la communauté autour de vous qui peut également être touchée. Vous pouvez aussi demander de l'aide à une personne de confiance pour vous confirmer si le message est dangereux. Envisagez de partager le message avec des organisations qui peuvent l'analyser.
>
> Pour partager votre message suspect, veillez à inclure le message lui-même ainsi que des informations sur l'expéditeur·rice. S'il s'agit d'un courrier électronique, veillez à inclure l'intégralité du message, y compris les en-têtes, en [suivant le guide du Computer Incident Response Center Luxembourg (CIRCL)](https://www.circl.lu/pub/tr-07/).

Avez-vous besoin d'aide supplémentaire ?

##### Options

- [Oui, j'ai besoin de plus d'aide](#help_end)
- [Non, j'ai résolu mon problème](#resolved_end)

### device-security_end

#### J'ai téléchargé un fichier

> Si certains fichiers ont été téléchargés sur votre appareil, la sécurité de votre appareil peut être compromise !

Veuillez communiquer avec les organismes ci-dessous qui peuvent vous aider. Par la suite, veuillez [partager votre message suspect](#share).

[orgs](:organisations?services=device_security)

### account-security_end

#### J'ai saisi ou fourni des informations sur mon compte

> Si vous avez saisi vos identifiants, ou si vous avez été victime d'une attaque de type cross-site scripting, vos comptes peuvent être en danger !
>
> Si vous pensez que votre compte est compromis, il est recommandé de suivre également le processus de travail de la trousse de premiers soins numériques [J'ai perdu l'accès à mes comptes](/fr/topics/account-access-issues).
>
> Il est suggéré que vous informiez également votre communauté de cette campagne d’hameçonnage, et que vous partagiez le message suspect avec les organisations qui peuvent l'analyser.

Souhaitez-vous partager des informations sur le message que vous avez reçu ou avez-vous besoin d'aide supplémentaire ?

##### Options

- [Je veux partager le message suspect](#share)
- [J'ai besoin de plus d'aide pour sécuriser mon compte](#account_end)
- [J'ai besoin de plus d'aide pour analyser le message](#analysis_end)

### suspicious-device_end

#### Je ne sais pas ce qui s'est passé

> Si vous avez cliqué sur un lien et que vous n'êtes pas sûr de ce qui s'est passé, votre appareil a peut-être été infecté sans que vous le remarquiez. Si vous voulez explorer cette possibilité, ou si vous avez le sentiment que votre appareil est infecté, suivez le processus ["Mon appareil agit de façon suspecte"](/fr/topics/device-acting-suspiciously).

Si vous avez besoin d'une aide immédiate parce que votre appareil a un comportement suspect, vous pouvez contacter les organisations ci-dessous qui peuvent vous aider. Ensuite, veuillez [partager votre message suspect](#share).

[orgs](:organisations?services=device_security)

### help_end

#### J'ai besoin de plus d'aide

> Vous devriez demander l'aide de vos collègues ou d'autres personnes pour mieux comprendre les risques associés à l'information que vous avez partagée. D'autres personnes de votre organisation ou de votre réseau peuvent également avoir reçu des demandes similaires.

Veuillez communiquer avec les organismes ci-dessous qui peuvent vous aider. Par la suite, veuillez [partager votre message suspect](#share).

[orgs](:organisations?services=24_7_digital_support)

### account_end

#### J'ai besoin d'aide pour sécuriser mon compte

Si votre compte a été compromis et que vous avez besoin d'aide pour le sécuriser, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

[orgs](:organisations?services=account)

### analysis_end

#### J'ai besoin d'aide pour analyser le message

Les organisations suivantes peuvent recevoir votre message suspect et enquêter pour vous :

[orgs](:organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

#### Oui, mon problème a été résolu

Nous espérons que ce guide du DFAK a été utile. Vous pouvez nous faire part de vos commentaires [par courrier électronique](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

### Final Tips

La première règle à retenir : Ne donnez jamais d'informations personnelles dans des courriels ou d'autres outils de messagerie. Aucune institution, banque ou autre, ne demandera jamais ces informations par courriel, sur les plateformes de réseaux sociaux ou les applications de messagerie. Il n'est pas toujours facile de savoir si un courriel, un message ou un site web est légitime, mais certains conseils peuvent vous aider à évaluer le courriel que vous avez reçu.

- Sentiment d'urgence : les emails suspects vous avertissent généralement d'un changement soudain sur un compte et vous demandent d'agir immédiatement pour vérifier ce compte.
- Dans le corps d'un courriel, vous pouvez voir des questions vous demandant de "vérifier" ou de "mettre à jour votre compte" ou "ne pas mettre à jour vos informations entraînera la suspension de votre compte". Il est généralement sûr de supposer qu'aucune organisation crédible à laquelle vous avez fourni vos renseignements ne vous demandera jamais de les entrer à nouveau, alors ne tombez pas dans ce piège.
- Méfiez-vous des messages non sollicités, des pièces jointes, des liens et des pages de connexion.
- Attention aux fautes d'orthographe et de grammaire.
- Dans les e-mails, cliquez pour afficher l'adresse complète de l'expéditeur·rice, pas seulement le nom affiché.
- Attention aux liens raccourcis - ils peuvent cacher un lien malveillant derrière eux - comme vous ne pouvez pas vérifier la destination finale, ils peuvent vous conduire à un site web malveillant.
- Lorsque vous passez votre souris sur un lien, l'URL vers laquelle vous êtes dirigé s'affiche dans un popup ou au bas de la fenêtre de votre navigateur.
- Les en-têtes de courriel comprenant la valeur "De :" pourraient être soigneusement conçus pour avoir l'air légitime. En examinant les en-têtes SPF et DKIM, vous pouvez dire si une adresse IP est (ou non) autorisée à envoyer des courriels au nom du domaine de l'expéditeur·rice, et si les en-têtes ou le contenu ont été modifiés en cours de route. Dans un courriel légitime, les valeurs [SPF](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-spf) et [DKIM](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-dkim) doivent toujours être 'PASS', sinon on ne peut pas faire confiance à un courriel. La raison en est que le courrier électronique est usurpé ou, dans de rares cas, que le serveur de messagerie n'est pas configuré correctement.
- Les [signatures numériques](https://www.gnupg.org/gph/en/manual/x135.html) peuvent nous dire si un courriel a été envoyé par l'expéditeur·rice légitime et s'il a été modifié ou non en cours de route. Si le courriel est signé avec OpenPGP, regardez si la signature est vérifiée ou non. Afin de vérifier une signature, vous aurez besoin d'installer OpenPGP ainsi que d'importer la clé publique associée à l'ID dans la signature du message. La plupart des clients de messagerie électronique qui prennent en charge les signatures numériques automatisent la vérification pour vous et vous indiquent par le biais de leur interface utilisateur·rice si une signature est vérifiée ou non.
- Un compte compromis pourrait émettre un courriel ou un message malveillant qui pourrait répondre à toutes les conditions ci-dessus et paraître légitime. Toutefois, le contenu du message sera généralement inhabituel. Si le contenu du message électronique semble étrange, il est toujours bon de vérifier avec l'expéditeur·rice légitime par un autre canal de communication avant de prendre une quelconque mesure.
- Il est toujours conseillé de lire et d'écrire vos e-mails en texte simple. Les courriers électroniques en HTML peuvent être transformés de manière à masquer du code ou des URL malveillants. Vous trouverez des instructions sur la manière de désactiver le HTML dans différents clients de messagerie dans [ce post](https://useplaintext.email/).
- Utilisez la dernière version du système d'exploitation de votre téléphone ou de votre ordinateur, vérifiez la version pour [Android](https://fr.wikipedia.org/wiki/Historique_des_versions_d%27Android), [iOS](https://fr.wikipedia.org/wiki/Historique_des_versions_d%27iOS), [Mac](https://fr.wikipedia.org/wiki/MacOS#Versions_principales) et [Windows](https://fr.wikipedia.org/wiki/Microsoft_Windows).
- Mettez à jour le plus rapidement possible votre système d'exploitation et tous les programmes/applications que vous avez installés, en particulier ceux qui reçoivent des informations (navigateurs, programmes/applications de messagerie et de chat, clients de messagerie électronique, etc.) Supprimez toutes les applications et tous les programmes dont vous n'avez pas besoin.
- Utilisez un navigateur fiable (par exemple Mozilla Firefox). Augmentez la sécurité de votre navigateur en examinant les extensions installées dans votre navigateur. Laissez seulement de côté ceux en qui vous avez confiance (par exemple : [Privacy Badger](https://privacybadger.org/), [uBlock Origin](https://ublockorigin.com), [Facebook Container](https://addons.mozilla.org/en-US/firefox/addon/facebook-container/), [Cookie AutoDelete](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete), [NoScript](https://noscript.net/)).
- Faites régulièrement des sauvegardes sécurisées de vos informations.
- Protégez vos comptes avec des mots de passe forts, une authentification à deux facteurs et des paramètres sécurisés.

### Resources

Voici un certain nombre de ressources pour repérer les messages suspects et éviter l'hameçonnage.

- [Citizen Lab: Communautés à risque - Menaces numériques ciblées contre la société civile](https://targetedthreats.net)
- [Guide pratique d’auto-défense: Comment éviter les attaques par hameçonnage](https://ssd.eff.org/fr/module/guide-pratique-%C3%A9viter-les-attaques-par-hame%C3%A7onnage)
- [Google message header analysis tool](https://toolbox.googleapps.com/apps/messageheader/)
