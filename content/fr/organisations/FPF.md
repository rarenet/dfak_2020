---
language: fr
layout: organisation
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: Du lundi au vendre aux heures de bureau, Eastern Time Zone, USA
response_time: 1 jour
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

La Freedom of the Press Foundation (FPF) est une organisation à but non lucratif (501(c)3) qui protège, défend et renforce le journalisme d'intérêt public au 21ème siècle. La FPF forme tout le monde, des grands médias aux journalistes indépendants, à une variété d'outils et de techniques de sécurité et de protection de la vie privée pour mieux se protéger eux-mêmes, leurs sources et leurs organisations.
