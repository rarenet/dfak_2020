---
language: fr
layout: organisation
name: Myan ICT for Development Organization (MIDO)
website: https://http://www.mido.ngo/
logo: MIDO_logo_blue_fill.png
languages: ဗမာ, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, browsing, account, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: 7 jours sur 7 de 9h à 17h UTC +6:30
response_time: 1 journée
contact_methods: email, phone, signal
email: help@mido.ngo
phone: +95(9)777788258 +95(9)777788246
whatsapp: +95(9)777788258
---

La mission de MIDO est d’être un catalyseur pour favoriser l’adoption de technologies pour promouvoir les progrès sociaux et politiques à Myanmar.
