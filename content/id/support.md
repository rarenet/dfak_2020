---
layout: support
language: id
permalink: support
type: support
---

Berikut adalah daftar organisasi yang menyediakan berbagai jenis bantuan. Klik tiap-tiap organisasi untuk melihat info selengkapnya mengenai layanan serta cara menghubungi mereka.
