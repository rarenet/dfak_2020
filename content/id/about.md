---
layout: page
title: "Tentang"
language: id
summary: "Tentang Pertolongan Pertama Darurat Digital."
date: 2020-11
permalink: about
parent: Home
---

Pertolongan Pertama Darurat Digital (P2D2) adalah upaya kolaboratif dari [RaReNet (Rapid Response Network)](https://www.rarenet.org/) dan [CiviCERT](https://www.civicert.org/).

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Rapid Response Network adalah jaringan internasional yang terdiri dari tim respons cepat dan penggiat keamanan digital yang mencakup Access Now, Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Fembloc, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtualroad, dan juga pakar keamanan perseorangan yang bekerja di bidang keamanan digital dan respons cepat.

Beberapa dari organisasi dan individu di sini adalah bagian dari CiviCERT, jaringan internasional pusat bantuan dan penyedia infrastruktur keamanan digital yang fokus utamanya adalah mendukung kelompok dan individu yang memperjuangkan keadilan sosial serta membela hak asasi manusia dan hak digital. CiviCERT adalah kerangka profesional untuk upaya CERT (Computer Emergency Response Team, atau Tim Respons Darurat Komputer) yang didistribusikan oleh komunitas respons cepat. CiviCERT diakreditasi oleh Trusted Introducer, jaringan tim respons darurat komputer tepercaya di Eropa.

Pertolongan Pertama Darurat Digital juga merupakan [proyek sumber-terbuka yang menerima kontribusi dari pihak eksternal](https://gitlab.com/rarenet/dfak_2020).

Terima kasih kepada [Metamorphosis Foundation](https://metamorphosis.org.mk) untuk [lokalisasi bahasa Albania](https://digitalfirstaid.org/sq/), [EngageMedia](https://engagemedia.org/) untuk lokalisasi Pertolongan Pertama Darurat Digital ke [bahasa Burma](https://digitalfirstaid.org/my/), [bahasa Indonesia](https://digitalfirstaid.org/id/), dan [bahasa Thai](https://digitalfirstaid.org/th/) serta [Media Diversity Institute](https://mdi.am/en/home) untuk [terjemahan Armenia](https://digitalfirstaid.org/hy/).

Jika Anda ingin menggunakan Pertolongan Pertama Darurat Digital dalam konteks di mana konektivitas terbatas, atau menemukan sambungan internet adalah hal yang sulit, Anda dapat mengunduh [versi luring](https://digitalfirstaid.org/dfak-offline.zip).

Jika Anda memiliki komentar, saran, atau pertanyaan apapun terkait Pertolongan Pertama Darurat Digital (P2D2), Anda dapat mengirimkannya ke: dfak @ digitaldefenders . org

GPG - Fingerprint: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
