---
layout: topic
title: "Seseorang membagikan informasi pribadi saya"
author: Ahmad Gharbeia, Michael Carbone, Gus Andrews, Flo Pagano, Constanza Figueroa
language: id
summary: "Informasi pribadi tentang saya atau media yang menampilkan kemiripan dengan saya sedang diedarkan. Hal tersebut menyusahkan saya atau berpotensi berbahaya"
date: 2023-05
permalink: topics/doxing
parent: Home
order: 10
---

# Seseorang membagikan informasi atau media pribadi saya tanpa persetujuan

Pelaku pelecehan terkadang dapat memublikasikan informasi yang mengidentifikasi Anda secara pribadi atau mengungkap aspek kehidupan pribadi Anda yang telah Anda pilih untuk dirahasiakan. Hal ini terkadang disebut "_doxxing_", atau "_doxing_" (istilah singkatan untuk "merilis dokumen").

Tujuan _doxxing_ adalah untuk mempermalukan target atau mengagitasi mereka. Hal ini dapat memiliki efek negatif yang parah pada kesejahteraan psiko-sosial, keamanan pribadi, relasi, serta pekerjaan Anda.

_Doxxing_ digunakan untuk mengintimidasi tokoh masyarakat, jurnalis, pembela HAM, dan aktivis, tetapi juga sering menargetkan orang-orang yang termasuk minoritas secara seksual atau populasi yang terpinggirkan. Hal ini terkait erat dengan bentuk-bentuk lain dari kekerasan daring dan gender.

Orang yang melakukan _doxxing_ pada Anda mungkin telah memperoleh dokumen Anda dari berbagai sumber. Ini bisa termasuk membobol akun dan peranti pribadi Anda. Namun, mungkin juga mereka mengungkap dan mengambil di luar konteks dari informasi atau media yang Anda pikir Anda bagikan secara pribadi dengan teman, mitra, kenalan, atau rekan kerja Anda. Dalam beberapa kasus, pelaku pelecehan dapat mengumpulkan informasi yang tersedia secara publik tentang Anda dari media sosial atau catatan publik. Menyatukan informasi ini dengan cara-cara baru yang disalahartikan secara berbahaya juga bisa menjadi bagian dari _doxxing_.

Jika Anda telah di-_doxx_, Anda dapat menggunakan kuesioner berikut untuk mengidentifikasi di mana pelaku pelecehan menemukan informasi Anda, dan mencari cara untuk membatasi kerusakan, termasuk menghapus dokumen dari situs web.

## Workflow

### be_supported

#### Apakah Anda merasa memiliki seseorang yang Anda percayai dapat membantu Anda jika Anda membutuhkannya?

> Dokumentasi sangat penting untuk melakukan asesmen terhadap dampak, mengidentifikasi asal serangan ini, dan mengembangkan respons yang memulihkan keselamatan Anda. Bergantung pada apa yang terjadi, dengan melalui proses dokumentasi dapat meningkatkan dampak emosional dari bentuk agresi ini terhadap Anda. Untuk mengurangi beban pekerjaan ini serta mengurangi potensi stres emosional pada diri Anda sendiri, Anda mungkin memerlukan bantuan dari teman dekat atau orang tepercaya lainnya saat Anda mendokumentasikan serangan tersebut.

Apakah Anda merasa memiliki seseorang yang Anda percayai dapat membantu Anda jika Anda membutuhkannya?

##### Options

- [Ya, orang tepercaya tersebut siap membantu saya](#physical_risk)

### physical_risk

#### Ya, orang terpercaya siap membantu saya

> Renungkan: Menurut Anda siapa yang mungkin bertanggung jawab atas serangan-serangan ini? Seperti apa kemampuan mereka? Seberapa besar niat mereka untuk menyakiti Anda? Pihak lain mana saja yang mungkin termotivasi untuk merugikan Anda? Pertanyaan-pertanyaan ini akan membantu Anda melakukan asesmen terhadap ancaman yang sedang berlangsung dan menilai apakah Anda terpapar risiko fisik.

Apakah Anda mengkhawatirkan integritas atau kesejahteraan fisik Anda atau bahwa Anda mungkin menghadapi risiko hukum?

##### Options

- [Ya, saya pikir saya terkena risiko fisik](#yes_physical_risk)
- [Saya pikir saya mungkin menghadapi risiko hukum](#legal_risk)
- [Tidak, saya ingin menyelesaikan masalah saya di bidang digital](#takedown_content)
- [Saya tidak yakin. Saya butuh bantuan untuk menilai risiko saya](#assessment_end)

### yes_physical_risk

#### Ya, saya pikir saya terkena risiko fisik

> Jika Anda merasa bahwa Anda dapat menjadi target serangan langsung, atau bahwa integritas atau kesejahteraan fisik Anda terancam, Anda dapat merujuk ke beberapa sumber berikut untuk memikirkan kebutuhan segera mengenai keamanan fisik Anda.
>
> - [Front Line Defenders - Buku Kerja tentang Keamanan](https://www.frontlinedefenders.org/en/workbook-security) (dalam bahasa Inggris)
> - [_Hotline_ Kekerasan Dalam Rumah Tangga Nasional - Buat Rencana Keselamatan](https://www.thehotline.org/plan-for-safety/create-a-safety-plan/) (dalam bahasa Inggris)
> - [Koalisi melawan Kekerasan Daring - Bantuan Keamanan Fisik](https://onlineviolenceresponsehub.org/physical-security-support) (dalam bahasa Inggris)
> - [Forum Resiliensi Cheshire - Bagaimana Bersiap untuk Keadaan Darurat](https://cheshireresilience.org.uk/how-to-prepare-for-an-emergency/) (dalam bahasa Inggris)
> - [FEMA Form P-1094 Buat Rencana Komunikasi Darurat Keluarga Anda](https://www.templateroller.com/group/12262/create-your-family-emergency-communication-plan.html) (dalam bahasa Inggris)
> - [Reolink - Cara Cerdik Mengamankan Jendela Rumah Anda — 9 Solusi Keamanan Terbaik yang Termudah](https://reolink.com/blog/top-7-easy-diy-ways-to-secure-your-home-windows/) (dalam bahasa Inggris)
> - [Reolink - Cara Membuat Rumah Anda Aman dari Pembobolan](https://reolink.com/blog/make-home-safe-from-break-ins/) (dalam bahasa Inggris)
> - [Keamanan komprehensif untuk jurnalis - Keamanan fisik](https://seguridadintegral.articulo19.org/categorias-prevencion/seguridad-fisica/) (dalam bahasa Spanyol)

Apakah Anda memerlukan lebih banyak bantuan untuk mengamankan integritas dan kesejahteraan fisik Anda?

##### Options

- [Ya](#physical-risk_end)
- [Tidak, tapi saya pikir saya juga menghadapi risiko hukum](#legal_risk)
- [Tidak, saya ingin menyelesaikan masalah saya di bidang digital](#takedown_content)
- [Saya tidak yakin. Saya butuh bantuan untuk menilai risiko saya](#assessment_end)

### legal_risk

#### Saya pikir saya juga menghadapi risiko hukum

> Jika Anda mempertimbangkan untuk mengambil tindakan hukum, menyimpan bukti serangan yang Anda alami akan sangat penting. Oleh karena itu, sangat disarankan untuk mengikuti [rekomendasi di halaman Pertolongan Pertama Darurat Digital tentang merekam informasi terkait serangan](/id/documentation).

Apakah Anda memerlukan bantuan hukum segera?

##### Options

- [Ya](#legal_end)
- [Tidak, saya ingin menyelesaikan masalah saya di bidang digital](#takedown_content)
- [Tidak](#resolved_end)

### takedown_content

#### Saya ingin memecahkan masalah saya di bidang digital

Apakah Anda ingin mulai menghapus dokumen yang dibagikan secara tidak tepat dari tampilan publik?

##### Options

- [Ya](#documenting)
- [Tidak](#resolved_end)

### documenting

#### Hapus dokumen dari tampilan publik

> Menyimpan dokumentasi Anda sendiri tentang serangan yang Anda alami sangatlah penting. Hal tersebut dapat memudahkan tim bantuan hukum atau teknis untuk mendukung Anda. Silakan periksa [Panduan Pertolongan Pertama Darurat Digital tentang mendokumentasikan serangan daring](/id/documentation) untuk mempelajari cara terbaik mengumpulkan informasi untuk kebutuhan khusus Anda.
>
> Anda mungkin juga ingin meminta orang yang Anda percayai untuk membantu Anda mencari bukti serangan yang Anda alami. Hal ini dapat membantu memastikan Anda tidak mengalami trauma ulang dengan melihat kembali materi yang dipublikasikan.

Apakah Anda merasa siap untuk mendokumentasikan serangan dengan cara yang aman?

##### Options

- [Ya, saya mendokumentasikan serangan tersebut](#where_published)

### where_published

#### Saya mendokumentasikan serangan terhadap...

> Tergantung di mana si penyerang telah membagikan informasi Anda secara publik, Anda mungkin ingin mengambil tindakan yang berbeda.
>
> Jika informasi Anda telah dibagikan di platform media sosial yang berbasis di negara-negara dengan sistem hukum yang menentukan tanggung jawab hukum perusahaan terhadap orang yang menggunakan layanan mereka (misalnya, Uni Eropa atau Amerika Serikat), kemungkinan besar moderator perusahaan akan bersedia untuk membantu Anda. Mereka akan membutuhkan Anda untuk [mendokumentasikan](/id/documentation) bukti dari serangan tersebut. Anda mungkin dapat mencari negara tempat perusahaan berada di Wikipedia.
>
> Terkadang musuh dapat memublikasikan informasi Anda di situs yang lebih kecil dan kurang terkenal, dan menautkannya dari media sosial. Terkadang situs ini di-_hosting_ di negara yang tidak memiliki perlindungan hukum, atau dijalankan oleh individu atau kelompok yang mempraktekkan, mendukung, atau mudahnya, tidak menolak perilaku berbahaya. Dalam hal ini mungkin akan lebih sulit untuk menghubungi situs tempat materi dibagikan, atau mencari tahu siapa yang menjalankannya.
>
> Tetapi bahkan ketika si penyerang telah membagikan dokumen Anda di tempat lain selain media sosial populer (yang mana sering terjadi), mulai menghapus materi atau tautan ke sana dapat mengurangi sejumlah besar orang untuk melihat informasi Anda. Hal tersebut juga dapat mengurangi jumlah dan tingkat keparahan serangan yang mungkin Anda hadapi.

Informasi saya dibagikan di

##### Options

- [Platform atau layanan yang diatur berdasarkan hukum](#what_info_published)
- [Platform atau layanan yang tidak memiliki kemungkinan untuk menanggapi permintaan penghapusan](#legal_advocacy)

### what_info_published

#### Informasi apa yang telah dibagikan tanpa persetujuan Anda?

##### Options

- [Informasi pribadi yang mengidentifikasi saya atau bersifat pribadi: alamat, nomor telepon, NIK, rekening bank, dll.](#personal_info)
- [Media yang tidak saya setujui untuk dibagikan, termasuk materi intim (gambar, video, audio, teks)](#media)
- [Nama samaran yang saya gunakan telah dikaitkan dengan identitas kehidupan nyata saya](#linked_identities)

### linked_identities

#### Nama samaran yang saya gunakan telah dikaitkan dengan identitas kehidupan nyata saya

> Jika penyerang secara publik menautkan nama atau identitas asli Anda ke nama samaran, nama pendek, atau nama panggilan yang Anda gunakan saat mengekspresikan diri, memublikasikan pendapat Anda, mengorganisasi, atau terlibat dalam aktivisme, pertimbangkan untuk menutup akun yang terkait dengan identitas tersebut sebagai cara dalam mengurangi kerusakan.

Apakah Anda perlu tetap menggunakan identitas/persona ini untuk mencapai tujuan Anda, atau dapatkah Anda menutup akun??

##### Options

- [Saya harus tetap menggunakannya](#defamation_flow_end)
- [Saya dapat menutup akun](#close)

### close

#### Saya dapat menutup akun y

> Sebelum menutup akun yang terkait dengan identitas atau persona yang terdampak, pertimbangkan risiko penutupan akun. Anda mungkin kehilangan akses ke layanan atau data, menghadapi risiko terhadap reputasi Anda, kehilangan kontak dengan rekan online Anda, dll.

##### Options

- [Saya telah menutup akun yang relevan](#resolved_end)
- [Saya telah memutuskan untuk tidak menutup akun yang relevan sekarang](#resolved_end)
- [Saya butuh bantuan untuk memahami apa yang harus saya lakukan](#harassment_end)

### personal_info

#### Di mana informasi pribadi Anda dipublikasikan?

##### Options

- [Di platform jejaring sosial](#doxing-sn)
- [Di situs web](#doxing-web)

### doxing-sn

#### Di platform jejaring sosial

> Jika informasi pribadi Anda telah dipublikasikan di platform media sosial, Anda dapat melaporkan pelanggaran terhadap standar komunitas dengan mengikuti prosedur pelaporan yang diberikan kepada pengguna oleh situs jejaring sosial.
>
> **_Catatan:_** _Selalu [dokumentasikan](/id/documentation) sebelum mengambil tindakan seperti menghapus pesan, log percakapan, atau memblokir profil. Jika Anda sedang mempertimbangkan tindakan hukum, Anda harus berkonsultasi tentang informasi [dokumentasi hukum](/id/documentation#legal)._
>
> Anda akan menemukan instruksi untuk melaporkan pelanggaran standar komunitas ke beberapa platform utama dalam daftar berikut:
>
> - [Google](https://cybercivilrights.org/google-2/)
> - [Facebook](https://www.cybercivilrights.org/facebook)
> - [Twitter](https://www.cybercivilrights.org/twitter)
> - [Tumblr](https://www.cybercivilrights.org/tumblr)
> - [Instagram](https://www.cybercivilrights.org/instagram)
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons atas permintaan Anda. Simpan halaman ini di _bookmark_ Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah informasi tersebut telah dihapus?

##### Options

- [Ya](#harassment_end)
- [Tidak](#platform_help_end)

### doxing-web

#### Di sebuah situs web

> Anda dapat mencoba melaporkan situs web tersebut ke penyedia _hosting_ atau pendaftar domain, untuk meminta penghapusan.
>
> **_Catatan:_** _Selalu [dokumentasikan](/id/documentation) sebelum mengambil tindakan seperti menghapus pesan, log percakapan, atau memblokir profil. Jika Anda sedang mempertimbangkan tindakan hukum, Anda harus berkonsultasi tentang informasi [dokumentasi hukum](/id/documentation#legal)._
>
> Untuk mengirim permintaan penghapusan, Anda juga perlu mengumpulkan informasi di situs web tempat informasi Anda dipublikasikan:
>
> - Buka [layanan NSLookup dari Network Tools](https://network-tools.com/nslookup/) dan cari tahu alamat IP (atau alamat) dari situs web palsu dengan memasukkan URL-nya di formulir pencarian.
> - Tuliskan alamat IP atau alamat situs tersebut.
> - Buka [layanan Whois Lookup dari Domain Tools](https://whois.domaintools.com/) dan cari domain dan alamat IP dari situs web palsu.
> - Catat nama dan alamat surel untuk laporan milik penyedia _hosting_ dan layanan domain. Catat juga nama pemilik situs webnya, jika hal tersebut termasuk dalam hasil pencarian Anda.
> - Kirim pesan ke penyedia _hosting_ dan pendaftar domain dari situs web palsu tersebut untuk meminta penghapusannya. Dalam pesan tersebut, sertakan informasi mengenai alamat IP, URL dan pemilik situs web yang meniru identitas Anda, serta alasan mengapa situs tersebut merugikan.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di _bookmark_ Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah konten tersebut telah dihapus?

##### Options

- [Ya](#resolved_end)
- [Tidak, platform tidak merespons / membantu](#platform_help_end)
- [Saya butuh bantuan untuk mengetahui cara mengirim permintaan penghapusan](#platform_help_end)

### media

#### Media yang tidak saya izinkan untuk dibagikan, termasuk materi intim

> Jika Anda telah membuat media tersebut sendiri, biasanya Anda akan memiliki hak cipta atas media tersebut. Di beberapa yurisdiksi, tiap individu juga memiliki hak atas media di mana mereka muncul (dikenal sebagai hak publisitas).

Apakah Anda memiliki hak cipta atas media?

##### Options

- [Ya](#intellectual_property)
- [Tidak](#nude)

### intellectual_property

#### Saya memiliki hak cipta atas media tersebut

> Anda dapat menggunakan peraturan hak cipta, seperti [Digital Millennium Copyright Act (DMCA)](https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act), untuk menghapus informasi yang berhak cipta. Sebagian besar platform media sosial menyediakan formulir untuk melaporkan pelanggaran hak cipta dan mungkin lebih responsif terhadap jenis permintaan ini daripada pertimbangan penghapusan lainnya, karena implikasi hukumnya bagi mereka.
>
> **_Catatan:_** _Selalu [dokumentasikan](/id/documentation) sebelum meminta untuk menghapus konten. Jika Anda sedang mempertimbangkan tindakan hukum, Anda harus berkonsultasi tentang informasi [dokumentasi hukum](/id/documentation#legal)._
>
> Anda dapat menggunakan tautan ini untuk mengirim permintaan penghapusan karena pelanggaran hak cipta ke platform jejaring sosial utama:
>
> - [Facebook](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav&cms_id=1020633957973118)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://www.tiktok.com/legal/report/Copyright?lang=id-ID)
> - [Twitter](https://help.twitter.com/id/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622?sjid=1561215955637134274-SA&hl=id)
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di _bookmark_ Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah konten tersebut telah dihapus?

##### Options

- [Ya](#resolved_end)
- [Tidak, platform tidak merespons / membantu](#platform_help_end)
- [Saya tidak menemukan platform yang relevan di daftar sumber](#platform_help_end)

### nude

#### Saya tidak memiliki hak cipta atas media tersebut

> Media yang menampilkan tubuh telanjang atau detailnya terkadang dicakup oleh kebijakan platform tertentu.

Apakah media memuat tubuh telanjang atau rincian tubuh yang telanjang?

##### Options

- [Ya](#intimate_media)
- [Tidak](#nonconsensual_media)

### intimate_media

#### Ya, media tersebut berisi media intim

> Untuk mempelajari cara melaporkan pelanggaran aturan privasi atau berbagi media intim/konten berbahaya tanpa persetujuan ke platform paling populer, Anda dapat melihat sumber berikut:
>
> - [Stop NCII](https://stopncii.org/) (Facebook, Instagram, TikTok dan Bumble - di seluruh dunia)
> - [Saluran Bantuan _Revenge Porn_ - Bantuan untuk Korban di Luar UK](https://revengepornhelpline.org.uk/how-can-we-help/if-we-can-t-help-who-can/help-for-victims-outside-the-uk/)
>
> **_Catatan:_** _Selalu [dokumentasikan](/id/documentation) sebelum meminta untuk menghapus konten. Jika Anda sedang mempertimbangkan tindakan hukum, Anda harus berkonsultasi tentang informasi [dokumentasi hukum](/id/documentation#legal)._
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di _bookmark_ Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah konten tersebut telah dihapus?

##### Options

- [Ya](#resolved_end)
- [Tidak, platform tidak merespons / membantu](#platform_help_end)
- [Saya tidak menemukan platform yang relevan di daftar sumber](#platform_help_end)

### nonconsensual_media

#### Tidak, ini tidak mengandung media intim

Di mana media Anda telah dipublikasikan?

##### Options

- [Di platform jejaring sosial](#NCII-sn)
- [Di situs web](#NCII-web)

### NCII-sn

#### Di platform jejaring sosial

> Jika media Anda telah dipublikasikan di platform media sosial, Anda dapat melaporkan pelanggaran standar komunitas dengan mengikuti prosedur pelaporan yang diberikan kepada pengguna oleh situs jejaring sosial.
>
> **_Catatan:_** _Selalu [dokumentasikan](/id/documentation) sebelum mengambil tindakan seperti menghapus pesan, log percakapan, atau memblokir profil. Jika Anda sedang mempertimbangkan tindakan hukum, Anda harus berkonsultasi tentang informasi [dokumentasi hukum](/id/documentation#legal)._
>
> Anda akan menemukan instruksi untuk melaporkan pelanggaran standar komunitas ke beberapa platform utama dalam daftar berikut:
>
> - [Google](https://cybercivilrights.org/google-2/)
> - [Facebook](https://www.cybercivilrights.org/facebook)
> - [Twitter](https://www.cybercivilrights.org/twitter)
> - [Tumblr](https://www.cybercivilrights.org/tumblr)
> - [Instagram](https://www.cybercivilrights.org/instagram)
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di _bookmark_ Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah konten tersebut telah dihapus?

##### Options

- [Ya](#resolved_end)
- [Tidak, platform tidak merespons / membantu](#platform_help_end)
- [Saya tidak menemukan platform yang relevan di daftar sumber](#platform_help_end)

### NCII-web

#### Di sebuah situs web

> Ikuti instruksi di ["Without My Consent - Take Down" (dalam bahasa Inggris)](https://withoutmyconsent.org/resources/take-down) untuk menghapus konten dari situs web.
>
> **_Catatan:_** _Selalu [dokumentasikan](/id/documentation) sebelum mengambil tindakan seperti menghapus pesan, log percakapan, atau memblokir profil. Jika Anda sedang mempertimbangkan tindakan hukum, Anda harus berkonsultasi tentang informasi [dokumentasi hukum](/id/documentation#legal)._
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di _bookmark_ Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah konten tersebut telah dihapus?

##### Options

- [Ya](#resolved_end)
- [Tidak, platform tidak merespons / membantu](#platform_help_end)
- [Saya butuh bantuan untuk mengetahui cara mengirim permintaan penghapusan](#platform_help_end)

### legal_advocacy

#### Platform atau layanan yang tidak mungkin menanggapi permintaan penghapusan

> Dalam beberapa kasus, platform tempat terjadinya _doxxing_ atau publikasi media intim tanpa persetujuan tidak memiliki kebijakan atau proses moderasi yang cukup matang untuk menangani permintaan terkait _doxxing_, pelecehan, atau pencemaran nama baik.
>
> Terkadang materi yang menyinggung tersebut dipublikasikan atau di-_hosting_ di platform atau aplikasi yang dijalankan oleh individu atau kelompok yang mempraktikkan, mendukung, atau bahkan tidak menolak perilaku berbahaya.
>
> Ada juga tempat-tempat di dunia siber di mana undang-undang dan peraturan biasa hampir tidak dapat ditegakkan, karena dibuat secara khusus untuk beroperasi secara anonim dan tidak meninggalkan jejak. Salah satunya adalah apa yang dikenal sebagai _dark web_. Ada keuntungan dari menyediakan ruang anonim seperti ini, meskipun beberapa orang menyalahgunakannya.
>
> Dalam situasi seperti ini ketika tindakan hukum tidak tersedia, pertimbangkan apakah Anda bersedia menggunakan advokasi. Hal tersebut dapat mencakup mulai dari menjelaskan kasus Anda secara terbuka, menghasilkan debat publik tentangnya, dan mungkin memunculkan kasus serupa ke permukaan. Ada beberapa organisasi dan kelompok hak asasi yang dapat membantu Anda dalam hal itu. Mereka dapat membantu Anda menetapkan ekpetasi terhadap hasil, serta memberi Anda gambaran konsekuensinya.
>
> Mencari bantuan hukum mungkin merupakan upaya terakhir jika komunikasi dengan platform, aplikasi, atau penyedia layanan gagal atau tidak mungkin dilakukan.
>
> Jalur hukum biasanya membutuhkan waktu, biaya, dan mungkin mengharuskan Anda mempublikasikan bahwa _doxxing_ terjadi pada Anda atau media intim Anda dipublikasikan tanpa persetujuan Anda. Kasus hukum yang berhasil tergantung pada banyak faktor, termasuk [dokumentasi](/id/documentation#legal) atas serangan tersebut dengan cara yang dianggap dapat diterima oleh pengadilan, serta kerangka hukum yang mengatur kasus tersebut. Mengidentifikasi pelaku atau menentukan bahwa mereka bertanggung jawab bisa jadi juga sulit dibuktikan. Hal yang mungkin juga menantang adalah untuk menetapkan yurisdiksi di mana kasus tersebut harus diadili.

Apa yang ingin Anda lakukan?

##### Options

- [Saya membutuhkan bantuan untuk merencanakan kampanye advokasi](#advocacy_end)
- [Saya butuh bantuan hukum](#legal_end)
- [Saya mendapat bantuan dari komunitas lokal saya](#resolved_end)

### advocacy_end

#### Saya memerlukan dukungan untuk merencanakan kampanye advokasi

> Jika Anda ingin menangkal informasi tentang Anda yang telah tersebar secara daring tanpa kehendak Anda, kami sarankan Anda mengikuti rekomendasi di [Alur kerja Pertolongan Pertama Darurat Digital pada kampanye pencemaran nama baik](/id/topics/defamation).
>
> Jika Anda menginginkan bantuan untuk meluncurkan kampanye guna mengekspos penyerang Anda, Anda dapat menghubungi organisasi yang dapat membantu upaya advokasi:

[orgs](:organisations?services=advocacy)

### legal_end

#### Saya memerlukan dukungan hukum

> Jika Anda memerlukan bantuan hukum, silakan hubungi organisasi yang dapat mendukung Anda di bawah ini.
>
> Jika Anda berpikir untuk mengambil tindakan hukum, akan sangat penting untuk menyimpan bukti serangan yang Anda alami. Oleh karena itu, sangat disarankan untuk mengikuti [rekomendasi di halaman Pertolongan Pertama Darurat Digital tentang merekam informasi tentang serangan](/id/documentation).

[orgs](:organisations?services=legal)

### physical-risk_end

#### Saya membutuhkan dukungan untuk mengamankan integritas fisik dan kesejahteraan saya

> Jika Anda menghadapi risiko fisik dan memerlukan bantuan segera, silakan hubungi organisasi yang dapat membantu Anda di bawah ini.

[orgs](:organisations?services=physical_security)

### assessment_end

#### Saya memerlukan bantuan untuk menilai risiko saya

> Jika Anda merasa perlu bantuan dalam melakukan asesmen risiko yang Anda hadapi karena informasi atau media Anda telah dibagikan tanpa izin Anda, silakan hubungi organisasi yang dapat mendukung Anda di bawah ini.

[orgs](:organisations?services=harassment&services=assessment)

### resolved_end

#### Masalah saya telah teratasi

> Semoga panduan penyelesaian masalah ini bermanfaat. Silakan beri masukan [via surel](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### defamation_flow_end

#### Saya harus tetap menggunakan identitas itu

> Jika Anda ingin menangkal informasi tentang Anda yang telah tersebar secara daring di luar kehendak Anda, kami sarankan Anda untuk mengikuti rekomendasi di [Alur kerja Pertolongan Pertama Darurat Digital pada kampanye pencemaran nama baik](/id/topics/defamation).
>
> Jika Anda memerlukan bantuan khusus, daftar di bawah ini menyertakan organisasi yang dapat membantu Anda mengatasi kerusakan reputasi.

[orgs](:organisations?services=advocacy&services=legal)

### platform_help_end

#### Menghubungi platform tidak merespons atau membantu

> Di bawah ini adalah daftar organisasi yang dapat membantu Anda dalam pelaporan ke platform dan layanan.

[orgs](:organisations?services=harassment&services=legal)

### harassment_end

#### Saya memerlukan bantuan untuk memahami apa yang harus saya lakukan

> Jika Anda memerlukan bantuan untuk mengatasi situasi Anda, silakan hubungi organisasi yang dapat mendukung Anda di bawah ini.

[orgs](:organisations?services=harassment&services=triage)

### Final Tips

- Petakan keberadaan daring Anda. _Self-doxing_ terdiri dari mengeksplorasi kecerdasan buatan sumber terbuka pada diri sendiri untuk mencegah para aktor jahat menemukan dan menggunakan informasi tersebut untuk meniru identitas Anda. Pelajari lebih lanjut tentang cara mencari jejak daring Anda di [Panduan Saluran Bantuan Access Now untuk mencegah _doxing_ (dalam bahasa Inggris)](https://guides.accessnow.org/self-doxing.html)
- Jika _doxxing_ melibatkan informasi atau dokumen yang hanya Anda, atau beberapa orang tepercaya, yang seharusnya mengetahui atau memiliki akses terhadapnya, maka Anda mungkin juga ingin merenungkan bagaimana pelaku _doxxing_ dapat mengaksesnya. Tinjau praktik keamanan informasi pribadi Anda untuk meningkatkan keamanan peranti dan akun Anda.
- Anda mungkin ingin waspada dalam waktu dekat, seandainya materi yang sama, atau materi terkait, muncul kembali di internet. Salah satu cara untuk melindungi diri Anda sendiri adalah dengan mencari nama Anda atau nama samaran yang Anda gunakan di mesin penelusur, untuk melihat di mana lagi nama tersebut muncul. Anda dapat mengatur Peringatan Google untuk diri Anda sendiri, atau menggunakan Meltwater atau Mention. Layanan tersebut juga akan memberi tahu Anda saat nama atau nama samaran Anda muncul di internet.

### Resources

- [Crash Override Network - Jadi Anda Mengalami _Doxing_: Panduan untuk Praktik Terbaik (dalam bahasa Inggris)](https://crashoverridenetwork.tumblr.com/post/114270394687/so-youve-been-doxed-a-guide-to-best-practices)
- [Dokumentasi Komunitas Saluran Bantuan Access Now: Panduan untuk mencegah _doxing_ (dalam bahasa Inggris)](https://guides.accessnow.org/self-doxing.html)
- [Equality Labs: Panduan Anti-_doxing_ untuk Aktivis Menghadapi Serangan dari Kelompok Sayap Kanan (dalam bahasa Inggris)](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Mengunci Identitas Digital Anda (dalam bahasa Inggris)](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Ken Gagne, pertahanan terhadap _Doxxing_: Hapus info pribadi Anda dari broker data (dalam bahasa Inggris)](https://www.computerworld.com/article/2849263/doxxing-defense-remove-your-personal-info-from-data-brokers.html)
- [Totem Project - Menjaga kerahasiaan (dalam bahasa Inggris)](https://learn.totem-project.org/courses/course-v1:IWMF+IWMF_KP_EN+001/about)
- [Totem Project - Cara melindungi identitas daring Anda (dalam bahasa Inggris)](https://learn.totem-project.org/courses/course-v1:Totem+TP_IO_EN+001/about)
- [Koalisi Anti Kekerasan Daring - Aku mengalami _doxxing_ (dalam bahasa Inggris)](https://onlineviolenceresponsehub.org/for-journalists#doxxed)
- [PEN America: Manual Lapangan untuk Gangguan Daring (dalam bahasa Inggris)](https://onlineharassmentfieldmanual.pen.org/)
