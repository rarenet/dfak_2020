---
layout: topic
title: "Saya tidak bisa mengakses akun saya"
author: RaReNet
language: id
summary: "Apakah Anda mengalami masalah dalam mengakses surel, media sosial, atau akun web? Apakah salah satu akun Anda menunjukkan aktivitas yang tidak Anda kenali? Ada banyak hal yang dapat Anda lakukan untuk meringankan masalah ini."
date: 2020-11
permalink: topics/account-access-issues
parent: /id/
order: 2
---

# Saya kehilangan akses ke akun saya

Media sosial dan akun komunikasi banyak digunakan oleh anggota masyarakat sipil untuk berkomunikasi, berbagi pengetahuan, dan mengadvokasi pergerakan mereka. Konsekuensinya, akun-akun tersebut menjadi sasaran empuk oknum-oknum berniat jahat, yang sering kali mencoba menyalahgunakan akun-akun tersebut, yang menyebabkan kerugian bagi anggota masyarakat sipil dan kontak mereka.

Panduan ini hadir untuk membantu Anda jika Anda kehilangan akses ke salah satu akun Anda karena akun tersebut telah disalahgunakan.

Berikut adalah kuesioner untuk mengidentifikasi sifat masalah Anda dan menemukan solusi yang memungkinkan.

## Workflow

### Password-Typo

#### Apakah kata sandi Anda salah ketik?

> Kadang Anda mungkin tidak bisa masuk ke akun Anda karena Anda salah mengetik kata sandi, atau karena pengaturan bahasa di keyboard Anda bukanlah yang biasa Anda gunakan, atau CapsLock Anda menyala.
>
> Coba ketikkan nama pengguna dan kata sandi Anda di editor teks kemudian salinlah, lalu tempelkan pada formulir <i>login</i>.

Apakah saran di atas membantu Anda masuk ke akun Anda?

##### Options

- [Ya](#resolved_end)
- [Tidak](#account-disabled)

### Account-disabled

#### Akun dinonaktifkan

> Terkadang Anda tidak dapat masuk ke akun karena telah diblokir atau dinonaktifkan oleh platform mungkin karena terjadi pelanggaran Persyaratan Layanan atau peraturan platform. Hal ini dapat terjadi saat akun Anda dilaporkan secara masif, atau saat mekanisme pelaporan dan dukungan platform disalahgunakan dengan tujuan untuk menyensor konten daring.
>
> Jika Anda melihat pesan bahwa akun Anda dikunci, dibatasi, dinonaktifkan atau ditangguhkan, dan Anda yakin ini adalah kesalahan. Ikuti mekanisme pengajuan banding apa pun yang disertakan dengan pesan tersebut. Anda dapat menemukan informasi tentang cara mengajukan banding di tautan berikut:
>
> - [Facebook](https://www.facebook.com/help/185747581553788)
> - [Instagram](https://help.instagram.com/366993040048856)
> - [Twitter](https://help.twitter.com/en/forms/account-access/appeals/redirect)
> - [Youtube](https://support.google.com/youtube/answer/2802168)

Apakah saran di atas membantu Anda masuk ke akun Anda?

##### Options

- [Ya](#resolved_end)
- [Tidak](#what-type-of-account-or-service)

### what-type-of-account-or-service

#### Tipe akun atau layanan apakah yang hilang aksesnya?

##### Options

- [Facebook](#Facebook)
- [Facebook Page](#Facebook-Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#ProtonMail)
- [Instagram](#Instagram)
- [TikTok](#Tiktok)
  <!--- - [AddOtherServiceLink](#service-Name) -->

### Facebook-Page

#### Facebook Page

Apakah halaman tersebut memiliki admin lain selain Anda?

##### Options

- [Ya](#Other-admins-exist)
- [Tidak](#Facebook-Page-recovery-form)

### Other-admins-exist

#### Admin lain ada

Apakah admin lainnya mengalami masalah yang sama?

##### Options

- [Yes](#Facebook-Page-recovery-form)
- [No](#Other-admin-can-help)

### Other-admin-can-help

#### Admin lain bisa membantu

> Silakan minta admin lainnya untuk menambahkan Anda sebagai admin halaman lagi.

Apakah ini menyelesaikan masalahnya?

##### Options

- [Ya](#Fb-Page_end)
- [Tidak](#account_end)

### Facebook-Page-recovery-form

#### Admin lain juga mengalami hal yang sama

> Silakan masuk ke Facebook dan gunakan [formulir Facebook untuk memulihkan halaman](https://www.facebook.com/help/contact/164405897002583). Jika Anda tidak dapat masuk ke akun Facebook Anda, silakan mengikuti [alur kerja pemulihan akun Facebook](#Facebook)
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

##### Options

- [Ya](#resolved_end)
- [Tidak](#account_end)

<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

#### Google

Apakah Anda memiliki akses ke surel/nomor ponsel pemulihan yang terhubung ke akun tersebut?

##### Options

- [Ya](#I-have-access-to-recovery-email-google)
- [Tidak](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

#### Saya memiliki akses ke email pemulihan untuk Google

> Periksa kotak masuk surel pemulihan Anda untuk melihat apakah Anda menerima surel atau SMS "Peringatan keamanan serius untuk Akun Google Anda yang tertaut" dari Google.
>
> Saat memeriksa surel, selalu waspada terhadap upaya phishing. Jika Anda tidak yakin dengan keabsahan suatu pesan, silakan tinjau [Alur Kerja Pesan Mencurigakan](/id/topics/suspicious-messages/).

Apakah Anda menerima surel atau SMS "Peringatan keamanan serius untuk Akun Google Anda yang tertaut" dari Google?

##### Options

- [Ya](#Email-received-google)
- [Tidak](#Recovery-Form-google)

### Email-received-google

#### Saya telah menerima email pemulihan

Setelah Anda memverifikasi keabsahan pesan, tinjau informasi yang diberikan dalam surel. Periksa apakah ada tautan "pulihkan akun Anda". Apakah tautannya ada?

##### Options

- [Ya](#Recovery-Link-Found-google)
- [Tidak](#Recovery-Form-google)

### Recovery-Link-Found-google

#### Saya memiliki tautan pemulihan

> Silakan gunakan tautan "pulihkan akun Anda" untuk memulihkan akun Anda. Saat Anda mengikuti tautan, periksa kembali bahwa URL yang Anda kunjungi adalah alamat "google.com" yang benar.

Apakah Anda dapat memulihkan akun Anda?

##### Options

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-google)

### Recovery-Form-google

#### Formulir pemulihan Google

> Silakan ikuti instruksi [“Cara memulihkan Akun Google atau Gmail Anda”](https://support.google.com/accounts/answer/7682439?hl=id).
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

##### Options

- [Ya](#resolved_end)
- [Tidak](#account_end)

<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

#### Yahoo

Apakah Anda memiliki akses ke surel/nomor ponsel pemulihan yang terhubung ke akun tersebut?

##### Options

- [Ya](#I-have-access-to-recovery-email-yahoo)
- [Tidak](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

#### Saya memiliki akses ke email pemulihan

> Periksa kotak masuk surel pemulihan Anda untuk melihat apakah Anda menerima surel "Perubahan kata sandi untuk akun Yahoo Anda" dari Yahoo.
>
> Saat memeriksa surel, selalu waspada terhadap upaya phishing. Jika Anda tidak yakin dengan keabsahan suatu pesan, silakan tinjau [Alur Kerja Pesan Mencurigakan](/id/topics/suspicious-messages/).

Apakah Anda menerima surel "Perubahan kata sandi untuk akun Yahoo Anda" dari Yahoo?

##### Options

- [Ya](#Email-received-yahoo)
- [Tidak](#Recovery-Form-Yahoo)

### Email-received-yahoo

#### Saya telah menerima email pemulihan

Setelah Anda memverifikasi keabsahan pesan, tinjau informasi yang diberikan dalam surel. Periksa apakah ada tautan “Pulihkan akun Anda di sini”. Apakah tautannya ada?

##### Options

- [Ya](#Recovery-Link-Found-Yahoo)
- [Tidak](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

#### Saya memiliki tautan pemulihan

> Silakan gunakan tautan “Pulihkan akun Anda di sini” tersebut untuk memulihkan akun Anda.

Apakah Anda berhasil memulihkan akun Anda?

##### Options

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

#### Formulir pemulihan Yahoo

> Silakan ikuti instruksi di [“Memperbaiki masalah masuk ke akun Yahoo”](https://id.bantuan.yahoo.com/kb/Memperbaiki-masalah-masuk-ke-akun-Yahoo-sln2051.html) untuk memulihkan akun Anda.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

##### Options

- [Yes](#resolved_end)
- [No](#account_end)

<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

#### Twitter

Apakah Anda memiliki akses ke surel/nomor ponsel pemulihan yang terhubung ke akun tersebut?

##### Options

- [Ya](#I-have-access-to-recovery-email-Twitter)
- [Tidak](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

#### Saya memiliki akses ke email pemulihan

> Periksa kotak masuk surel pemulihan Anda untuk melihat apakah Anda menerima surel "Kata sandi Twitter Anda telah diubah" dari Twitter.
>
> Saat memeriksa surel, selalu waspada terhadap upaya phishing. Jika Anda tidak yakin dengan keabsahan suatu pesan, silakan tinjau [Alur Kerja Pesan Mencurigakan](/id/topics/suspicious-messages/).

Apakah Anda menerima surel "Kata sandi Twitter Anda telah diubah" dari Twitter?

##### Options

- [Ya](#Email-received-Twitter)
- [Tidak](#Recovery-Form-Twitter)

### Email-received-Twitter

#### Saya telah menerima email pemulihan

Setelah Anda memverifikasi keabsahan pesan, tinjau informasi yang diberikan dalam surel. Silakan periksa apakah pesan tersebut berisi tautan "pulihkan akun Anda". Apakah tautannya ada?

##### Options

- [Ya](#Recovery-Link-Found-Twitter)
- [Tidak](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

#### Saya memiliki tautan pemulihan

> Silakan gunakan tautan “pulihkan akun Anda” tersebut untuk memulihkan akun Anda.

Apakah Anda dapat memulihkan akun Anda?

##### Options

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

#### Formulir pemulihan Twitter

> Jika Anda yakin akun Twitter Anda telah disalahgunakan, coba ikuti langkah-langkah di [Bantuan terkait akun saya yang telah disalahgunakan](https://help.twitter.com/id/safety-and-security/twitter-account-compromised).
>
> Jika akun Anda tidak disalahgunakan, atau Anda memiliki masalah akses akun lainnya, Anda dapat mengikuti langkah-langkah di ["Minta bantuan memulihkan akun Anda"](https://help.twitter.com/id/forms/account-access/reactivate-my-account).
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

##### Options

- [Ya](#resolved_end)
- [Tidak](#account_end)

<!---=========================================================
//Protonmail
//========================================================= -->

### ProtonMail

#### Protonmail

> Silakan ikuti [panduan (dalam bahasa Inggris) untuk mengatur ulang kata sandi Anda](https://protonmail.com/support/knowledge-base/reset-password/) untuk memulihkan akun Anda.
>
> Harap perhatikan bahwa jika Anda mengatur ulang kata sandi, Anda tidak akan dapat membaca surel dan kontak yang sudah ada, karena hal tersebut dienkripsi dengan menggunakan kata sandi sebagai kunci enkripsi. Data lama dapat dipulihkan jika Anda memiliki akses ke berkas pemulihan atau frasa pemulihan dengan mengikuti langkah-langkah di [Pulihkan Pesan dan File yang Terenkripsi (dalam bahasa Inggris)](https://proton.me/support/recover-encrypted-messages-files).

Apakah prosedur pemulihannya berhasil?

##### Options

- [Ya](#resolved_end)
- [Tidak](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

#### Hotmail

Apakah Anda memiliki akses ke surel/nomor ponsel pemulihan yang terhubung ke akun tersebut?

##### Options

- [Ya](#I-have-access-to-recovery-email-Hotmail)
- [Tidak](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

#### Saya memiliki akses ke email pemulihan

> Periksa kotak masuk surel pemulihan Anda untuk melihat apakah Anda menerima surel "Perubahan kata sandi akun Microsoft" dari Microsoft.
>
> Saat memeriksa surel, selalu waspada terhadap upaya phishing. Jika Anda tidak yakin dengan keabsahan suatu pesan, silakan tinjau [Alur Kerja Pesan Mencurigakan](/id/topics/suspicious-messages/).

Apakah Anda menerima surel "Perubahan kata sandi akun Microsoft" dari Microsoft?

##### Options

- [Ya](#Email-received-Hotmail)
- [Tidak](#Recovery-Form-Hotmail)

### Email-received-Hotmail

#### Saya telah menerima email pemulihan

Setelah Anda memverifikasi keabsahan pesan, tinjau informasi yang diberikan dalam surel. Silakan periksa apakah pesan tersebut berisi tautan “Atur ulang kata sandi Anda”. Apakah tautannya ada?

##### Options

- [Ya](#Recovery-Link-Found-Hotmail)
- [Tidak](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

#### Saya memiliki tautan pemulihan

> Silakan gunakan tautan "Atur ulang kata sandi Anda" untuk mengatur kata sandi baru dan memulihkan akun Anda.

Apakah Anda dapat memulihkan akun Anda dengan tautan “Atur ulang kata sandi Anda” tersebut?

##### Options

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

#### Formulir pemulihan Hotmail

> Silakan coba ["Sarana Bantuan Masuk Akun" (dalam bahasa Inggris)](https://go.microsoft.com/fwlink/?linkid=2214157). Ikuti petunjuk pada sarana ini, termasuk menambahkan akun yang ingin Anda pulihkan dan menjawab pertanyaan tentang informasi yang tersedia untuk memulihkan.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda via formulir web. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

##### Options

- [Ya](#resolved_end)
- [Tidak](#account_end)

### Facebook

#### Facebook

Apakah Anda memiliki akses ke surel/nomor ponsel pemulihan yang terhubung ke akun tersebut?

##### Options

- [Ya](#I-have-access-to-recovery-email-Facebook)
- [Tidak](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

#### Saya memiliki akses ke email pemulihan

> Periksa kotak masuk surel pemulihan Anda untuk melihat apakah Anda menerima surel "Apakah Anda baru saja mengatur ulang kata sandi Anda?" dari Facebook.
>
> Saat memeriksa surel, selalu waspada terhadap upaya phishing. Jika Anda tidak yakin dengan keabsahan suatu pesan, silakan tinjau [Alur Kerja Pesan Mencurigakan](/id/topics/suspicious-messages/).

Apakah Anda menerima surel "Apakah Anda baru saja mengatur ulang kata sandi Anda?" dari Facebook?

##### Options

- [Ya](#Email-received-Facebook)
- [Tidak](#Recovery-Form-Facebook)

### Email-received-Facebook

#### Saya telah menerima email pemulihan

Setelah Anda memverifikasi keabsahan pesan, tinjau informasi yang diberikan dalam surel. Apakah surel berisi pesan dengan tautan yang mengatakan "Ini bukan saya"?

##### Options

- [Ya](#Recovery-Link-Found-Facebook)
- [Tidak](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

#### Saya memiliki tautan pemulihan

> Silakan gunakan tautan "Ini bukan saya" dalam pesan untuk memulihkan akun Anda.

Apakah Anda berhasil memulihkan akun Anda dengan mengklik tautan tersebut?

##### Options

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

#### Formulir pemulihan Facebook

> Silakan isi [formulir untuk memulihkan akun Anda](https://id-id.facebook.com/login/identify).
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda via formulir web. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

##### Options

- [Ya](#resolved_end)
- [Tidak](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

#### Instagram

Apakah Anda memiliki akses ke surel/nomor ponsel pemulihan yang terhubung ke akun tersebut?

##### Options

- [Ya](#I-have-access-to-recovery-email-Instagram)
- [Tidak](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

#### Saya memiliki akses ke email pemulihan

> Periksa kotak masuk surel pemulihan Anda untuk melihat apakah Anda menerima surel "Kata sandi Instagram Anda telah diubah" dari Instagram.
>
> Saat memeriksa surel, selalu waspada terhadap upaya phishing. Jika Anda tidak yakin dengan keabsahan suatu pesan, silakan tinjau [Alur Kerja Pesan Mencurigakan](/id/topics/suspicious-messages/).

Apakah Anda menerima surel "Kata sandi Instagram Anda telah diubah" dari Instagram?

##### Options

- [Ya](#Email-received-Instagram)
- [Tidak](#Recovery-Form-Instagram)

### Email-received-Instagram

#### Saya telah menerima email pemulihan

Setelah Anda memverifikasi keabsahan pesan, tinjau informasi yang diberikan dalam surel. Silakan periksa apakah ada tautan "amankan akun Anda di sini" dalam pesan tersebut. Apakah tautannya ada?

##### Options

- [Ya](#Recovery-Link-Found-Instagram)
- [Tidak](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

#### Saya menemukan tautan pemulihan

> Silakan gunakan tautan "amankan akun Anda di sini" untuk memulihkan akun Anda.

Apakah Anda dapat memulihkan akun Anda?

##### Options

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

#### Formulir pemulihan Instagram

> Silakan ikuti instruksi di [“Sepertinya akun Instagram saya diretas”](https://id-id.facebook.com/help/instagram/149494825257596?helpref=related) untuk memulihkan akun Anda.
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda via formulir web. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

##### Options

- [Ya](#resolved_end)
- [Tidak](#account_end)

### Tiktok

#### TikTok

Apakah Anda memiliki akses ke surel/nomor ponsel pemulihan yang terhubung ke akun tersebut?

##### Options

- [Ya](#I-have-access-to-recovery-email-Tiktok)
- [Tidak](#Recovery-Form-Tiktok)

### I-have-access-to-recovery-email-Tiktok

#### Saya memiliki akses ke email pemulihan

> Jika Anda memiliki akses ke surel pemulihan, silakan coba atur ulang kata sandi Anda dengan mengikuti [proses atur ulang kata sandi Tiktok](https://www.tiktok.com/login/email/forget-password?lang=id-ID).

Apakah Anda dapat memulihkan akun Anda?

##### Options

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-Tiktok)

### Recovery-Form-Tiktok

#### Ya, saya memiliki TikTok pemulihan

> Silakan coba ikuti instruksi di ["Akun saya telah diretas"](https://support.tiktok.com/id/log-in-troubleshoot/log-in/my-account-has-been-hacked) untuk memulihkan akun Anda.

Apakah prosedur pemulihan berhasil?

##### Options

- [Ya](#resolved_end)
- [Tidak](#account_end)

### Fb-Page_end

#### Ya, masalah saya telah teratasi

Kami ikut senang masalah Anda telah terselesaikan! Silakan baca rekomendasi kami berikut ini untuk membantu meminimalkan kemungkinan kehilangan akses ke halaman Anda di masa mendatang:

- Aktifkan autentikasi dua langkah (2FA) untuk semua admin halaman.
- Tetapkan peran admin hanya pada orang-orang yang Anda percayai dan yang responsif.
- Jika ada seseorang yang dapat Anda percayai, pertimbangkan untuk memiliki lebih dari satu akun admin. Perlu diingat Anda harus mengaktifkan 2FA untuk semua akun admin.
- Tinjau secara teratur hak istimewa dan izin pada halaman. Selalu tetapkan tingkat hak istimewa minimum yang diperlukan bagi pengguna untuk melakukan pekerjaan mereka.

### account_end

#### Masalah saya belum terselesaikan

Jika prosedur yang disarankan dalam alur kerja ini tidak membantu memulihkan akses ke akun Anda, Anda dapat menghubungi organisasi-organisasi berikut untuk meminta bantuan lebih lanjut:

[orgs](:organisations?services=account)

### resolved_end

#### Ya, masalah saya telah teratasi

Semoga panduan Pertolongan Pertama Darurat Digital ini bermanfaat. Silakan beri masukan [melalui surel](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

Silakan baca rekomendasi kami berikut ini untuk membantu meminimalkan kemungkinan kehilangan akses ke akun Anda di masa mendatang.

- Sangat disarankan untuk mengaktifkan autentikasi 2 langkah (2FA) untuk semua akun Anda di semua platform yang mendukungnya.
- Jangan pernah menggunakan kata sandi yang sama untuk lebih dari satu akun. Jika Anda masih melakukannya, Anda sebaiknya mengubahnya dengan kata sandi unik untuk setiap akun Anda.
- Menggunakan pengelola kata sandi dapat membantu Anda membuat dan mengingat kata sandi yang unik dan kuat untuk semua akun Anda.
- Waspadalah saat menggunakan jaringan Wi-Fi publik terbuka yang tidak tepercaya, dan sebisa mungkin gunakan VPN atau Tor saat terhubung dengan jaringan tersebut.

### Resources

- [Dokumentasi Komunitas Access Now Helpline: Rekomendasi Pengelola Kata Sandi Tim](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Surveillance Self-Defense: Melindungi Diri Anda di Jejaring Sosial](https://ssd.eff.org/en/module/protecting-yourself-social-networks)​​​​​​​
- [Surveillance Self-Defense: Membuat Kata Sandi yang Kuat Menggunakan Pengelola Kata Sandi](https://ssd.eff.org/en/module/creating-strong-passwords#0)

<!--- Edit the following to add another service recovery workflow:
#### service-name

Apakah Anda memiliki akses ke surel/nomor ponsel pemulihan yang terhubung ke akun tersebut?

- [Ya](#I-have-access-to-recovery-surel-google)
- [Tidak](#Recovery-Form-google)

### I-have-access-to-recovery-surel-google

Periksa apakah Anda menerima surel “[Judul Surel Perubahan Kata Sandi]” dari nama_layanan. Apakah Anda menerimanya?

- [Ya](#Surel-received-service-name)
- [Tidak](#Recovery-Form-service-name

### Email-received-service-name

> Silakan periksa apakah ada tautan “pulihkan akun Anda” dalam pesan tersebut. Apakah tautannya ada?

- [Ya](#Recovery-Link-Found-service-name)
- [Tidak](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> Silakan gunakan tautan [Deskripsi Tautan Pemulihan](URL) tersebut untuk memulihkan akun Anda.
Apakah Anda dapat memulihkan akun Anda dengan tautan “[Deskripsi Tautan Pemulihan]” tersebut?

- [Ya](#resolved_end)
- [Tidak](#Recovery-Form-service-name)

### Recovery-Form-service-name

> Silakan isi formulir pemulihan berikut ini untuk memulihkan akun ini: [Tautan ke formulir pemulihan standar].
>
> Harap perhatikan bahwa mungkin perlu beberapa saat untuk menerima respons dari permintaan Anda. Simpan halaman ini di <i>bookmark</i> Anda dan kembali ke alur kerja ini beberapa hari kemudian.

Apakah prosedur pemulihannya berhasil?

- [Ya](#resolved_end)
- [Tidak](#account_end)

-->
