---
layout: topic
title: "Situs web saya tidak berfungsi, apa yang terjadi?"
author: Rarenet
language: id
summary: "Ancaman yang dihadapi oleh banyak LSM, media independen, dan *blogger* adalah terbungkamnya suara mereka karena situs web mereka tidak berfungsi atau dirusak."
date: 2020-11
permalink: topics/website-not-working
parent: /id/
order: 5
---

# Situs web saya tidak berfungsi, apa yang terjadi?

Ancaman yang dihadapi oleh banyak LSM, media independen, dan _blogger_ adalah terbungkamnya suara mereka karena situs web mereka _down_ (tidak berfungsi) atau dirusak. Ini adalah masalah yang membuat frustasi dan dapat disebabkan oleh berbagai macam hal, seperti pemeliharaan situs web yang buruk, _hosting_ yang tidak andal, [script-kiddies](https://en.wikipedia.org/wiki/Script_kiddie), serangan _denial of service_ (DOS), atau pengambilalihan situs web. Di bagian Pertolongan Pertama Darurat Digital ini akan memandu Anda melalui beberapa langkah dasar untuk mendiagnosis kemungkinan masalah dengan menggunakan materi dari [_My Website is Down_](https://github.com/OpenInternet/MyWebsiteIsDown/blob/master/MyWebsiteIsDown.md).

Penting untuk diketahui bahwa ada banyak alasan mengapa situs web Anda tidak berfungsi. Mulai dari masalah teknis di perusahaan yang meng-_hosting_ situs web hingga Sistem Manajemen Konten (CMS) seperti Joomla atau Wordpress yang belum diperbarui. Menemukan masalahnya serta solusi yang memungkinkan untuk permasalahan situs web Anda bisa jadi rumit. Sebaiknya **hubungi _webmaster_ dan _host_ situs web Anda** setelah mendiagnosis masalah-masalah umum di bawah ini. Jika tidak ada satupun dari pilihan di bawah ini yang sesuai dengan Anda, carilah [bantuan dari organisasi yang Anda percaya](#website_down_end).

## Consider

Sebagai permulaan, pertimbangkan:

- Siapa yang membangun situs web Anda? Apakah mereka bisa membantu?
- Apakah situs web tersebut dibangun dengan WordPress atau platform CMS populer lainnya?
- Siapa penyedia _hosting_ web Anda? Jika Anda tidak tahu, Anda dapat menggunakan [layanan online WHOIS](http://www.whoishostingthis.com/) untuk membantu.

## Workflow

### error-message

#### Apakah Anda melihat pesan galat (_error_)?

##### Options

- [Ya, saya melihat pesan galat](#error-message-yes)
- [Tidak](#hosting-message)

### hosting-message

#### Apakah Anda melihat pesan dari penyedia _hosting_ web Anda?

##### Options

- [Ya, saya melihat pesan dari penyedia hosting saya](#hosting-message-yes)
- [Tidak](#site-not-loading)

### site-not-loading

#### Apakah situs Anda tidak memuat (_loading_) sama sekali?

##### Options

- [Ya, situsnya tidak memuat sama sekali](#site-not-loading-yes)
- [Tidak, situsnya memuat](#hosting-working)

### hosting-working

#### Apakah situs web penyedia _hosting_ Anda berfungsi, tetapi situs web Anda tidak?

##### Options

- [Ya, saya bisa mengakses situs web penyedia _hosting_ saya](#hosting-working-yes)
- [Tidak](#similar-content-censored)

### similar-content-censored

#### Apakah Anda bisa mengunjungi situs lain yang kontennya sama dengan situs Anda?

##### Options

- [Saya juga tidak bisa mengunjungi situs lain dengan konten serupa.](#similar-content-censored-yes)
- [Situs lain bekerja dengan baik. Saya hanya tidak bisa mengunjungi situs saya.](#loading-intermittently)

### loading-intermittently

#### Apakah pemuatan situs Anda terputus-putus, atau lebih lambat dari biasanya?

##### Options

- [Ya, pemuatan situs saya terputus-putus atau lambat](#loading-intermittently-yes)
- [Tidak, situs web saya termuat seperti biasa, tapi mungkin telah diretas](#website-defaced)

### website-defaced

#### Apakah situs web Anda termuat tetapi tampilan dan kontennya tidak seperti yang Anda harapkan?

##### Options

- [Ya, konten/tampilan situs saya tidak seperti yang diharapkan](#defaced-attack-yes)
- [Tidak](#website-down_end)

### error-message-yes

#### Ya, saya melihat pesan galat

> Bisa jadi ini adalah **_masalah perangkat lunak_** -- Anda harus merenungkan perubahan terbaru apapun yang mungkin telah Anda atau tim Anda lakukan, kemudian hubungi _webmaster_ Anda. Mengirimi _webmaster_ Anda tangkapan layar, tautan ke halaman yang bermasalah, dan pesan galat apapun yang Anda temukan akan membantu mereka mencari tahu penyebab masalahnya. Anda juga dapat menyalin pesan galat yang Anda dapatkan ke mesin pencari untuk mencari tahu apakah ada solusi mudahnya.

Apakah cara ini membantu?

##### Options

- [Ya](#resolved_end)
- [Tidak](#website-down_end)

### hosting-message-yes

#### Ya, saya melihat pesan dari penyedia hosting saya

> Situs web Anda bisa berada di luar jaringan (_offline_) karena alasan hukum, [hak cipta](https://www.eff.org/issues/bloggers/legal/liability/IP), penagihan, atau alasan lainnya. Hubungi penyedia _hosting_ Anda untuk detail lebih lanjut mengenai mengapa mereka menangguhkan _hosting_ situs web Anda.

Apakah cara ini membantu?

##### Options

- [Ya](#resolved_end)
- [Tidak, saya butuh bantuan hukum](#legal_end)
- [Tidak, saya butuh bantuan teknis](#website-down_end)

### site-not-loading-yes

#### Ya, situsnya tidak memuat sama sekali

> Perusahaan _hosting_ Anda mungkin sedang bermasalah, yang mana dalam hal ini mungkin Anda mengalami **_masalah hosting_**. Dapatkah Anda mengunjungi situs web perusahaan _hosting_ Anda? Ingat, **bukan** bagian admin dari situs Anda, melainkan perusahaan atau organisasi yang bekerja dengan Anda untuk meng-_hosting_ situs Anda.
>
> Cari atau selidiki “status” blog (misalnya status.dreamhost.com), dan juga cari di twitter.com untuk melihat diskusi dari pengguna lain mengenai _downtime_ di perusahaan _hosting_ tersebut - pencarian sederhana seperti “(nama perusahaan) _down_” seringkali dapat mengungkap apakah banyak orang mengalami masalah yang sama.

Apakah cara ini membantu?

##### Options

- [Ya](#resolved_end)
- [Tidak, saya butuh bantuan hukum](#hosting-working-yes)
- [Tidak, saya butuh bantuan teknis](#website-down_end)

### hosting-working-yes

#### Ya, Saya bisa mengakses situs web penyedia

> Periksa apakah situs web berfungsi dengan menggunakan [Down for Everyone or Just Me](https://downforeveryoneorjustme.com/) - situs Anda mungkin berfungsi tapi Anda tidak bisa melihatnya.
>
> Jika situs Anda berfungsi tapi Anda tidak bisa melihatnya, bisa jadi itu adalah **_masalah jaringan_**: koneksi internet Anda bisa jadi bermasalah atau memblokir akses ke situs Anda.

Apakah Anda membutuhkan bantuan lebih lanjut?

##### Options

- [Tidak, saya baik-baik saja](#resolved_end)
- [Ya, saya membutuhkan bantuan untuk memulihkan koneksi internet saya](#website-down_end)
- [Ya, ini bukanlah masalah jaringan dan situs web saya tidak berfungsi bagi siapa pun](#similar-content-censored)

### similar-content-censored-yes

#### Ya, ini bukanlah masalah jaringan dan situs web saya tidak berfungsi bagi siapa pun

> Coba kunjungi situs-situs web dengan konten yang serupa dengan situs web Anda. Coba juga untuk menggunakan [Tor](https://www.torproject.org/projects/gettor.html), [Psiphon](https://psiphon.ca/en/index.html), atau VPN untuk mengakses situs Anda.
>
> Jika Anda bisa mengunjungi situs Anda melalui Tor, Psiphon, atau VPN, Anda mengalami **_masalah sensor_** - situs Anda masih berfungsi bagi belahan dunia lainnya, tapi disensor di negara Anda sendiri.

Apakah Anda ingin melakukan sesuatu tentang penyensoran ini?

##### Options

- [Ya, saya ingin melaporkan hal ini secara terbuka dan butuh bantuan untuk kampanye advokasi saya](#advocacy_end)
- [Ya, saya ingin mencari solusi untuk membuat situs web saya dapat diakses](#website-down_end)
- [Tidak](#resolved_end)

### loading-intermittently-yes

#### Ya, pemuatan situs saya terputus-putus atau lambat

> Situs Anda mungkin kewalahan oleh jumlah dan kecepatan permintaan halaman yang diterimanya - ini adalah **_masalah kinerja_**.
>
> Hal tersebut bisa jadi “baik” karena situs Anda populer dan hanya perlu beberapa perbaikan untuk merespons pembaca yang lebih banyak - periksa analisis situs Anda untuk melihat pola pertumbuhan jangka panjang. Hubungi _webmaster_ atau penyedia _hosting_ Anda untuk panduannya. Banyak platform _blogging_ dan sistem pengelola konten (CMS) terkenal (Joomla, Wordpress, Drupal...) memiliki _plugin_ untuk membantu proses _cache_ situs web Anda secara lokal dan mengintegrasikan [Jaringan Pengiriman Konten](https://en.wikipedia.org/wiki/Content_delivery_network), yang dapat memperbaiki kinerja dan resiliensi situs secara drastis. Solusi-solusi di bawah ini juga dapat membantu permasalahan kinerja.
>
> Jika Anda mengalami **masalah kinerja** yang parah, situs Anda mungkin telah menjadi korban dari serangan [**"distributed denial of service"**](https://ssd.eff.org/en/glossary/distributed-denial-service-attack) (atau DDoS). Ikuti langkah-langkah di bawah ini untuk menanggulangi serangan tersebut:
>
> - Langkah 1: Hubungi orang tepercaya yang dapat membantu menangani situs web Anda (_webmaster_, orang yang membantu mengatur situs, staf internal, atau penyedia _hosting_).
>
> - Langkah 2: Bekerja sama dengan perusahaan tempat Anda membeli nama domain dan ubahlah “Time to Live” atau TTL menjadi 1 jam (Anda bisa menemukan instruksi mengenai cara melakukannya di beberapa situs web penyedia layanan, misalnya [GoDaddy](https://id.godaddy.com/help/kelola-catatan-dns-680) atau [Gandi.net](https://docs.gandi.net/en/domain_names/faq/dns_records.html)). Hal ini dapat membantu Anda mengarahkan ulang situs Anda dengan lebih cepat begitu diserang (standarnya adalah 72 jam atau tiga hari). Pengaturan ini seringkali ditemukan di bagian “lanjutan” dari _properties_ di domain Anda, kadang menjadi bagian dari SRV atau catatan layanan.
>
> - Langkah 3: Pindahkan situs Anda ke layanan penanggulangan DDoS. Sebagai permulaan:
>
>   - [Deflect.ca](https://deflect.ca/)
>   - [Project Shield dari Google](https://projectshield.withgoogle.com/landing)
>   - [Projek Galileo dari Cloudflare](https://www.cloudflare.com/galileo)
>
> Untuk daftar lengkap organisasi tepercaya yang dapat membantu menanggulangi serangan DDoS, kunjungi [bagian terakhir dari alur kerja ini yang membahas tentang DDoS](#ddos_end).
>
> - Langkah 4: Segera setelah Anda mendapatkan kendali kembali, tinjau ulang kebutuhan Anda dan putuskan apakah Anda butuh penyedia _hosting_ yang aman atau hanya melanjutkan dengan layanan penanggulangan DDoS Anda.

##### Options

- [Untuk daftar lengkap organisasi tepercaya yang dapat menyediakan _hosting_ aman, kunjungi bagian terakhir dari alur kerja ini yang membahas tentang persoalan hosting web](#web-hosting_end).

### defaced-attack-yes

#### Ya, konten/tampilan situs saya tidak seperti yang diharapkan

> Perusakan situs web adalah praktik di mana si penyerang mengganti konten atau tampilan visual situs web dengan konten mereka sendiri. Serangan ini biasanya dilakukan dengan mengeksploitasi kerentanan di platform CMS yang tidak terpelihara tanpa pembaruan keamanan terbaru, atau dengan menggunakan nama pengguna/sandi akun _hosting_ yang dicuri.
>
> - Langkah 1: Verifikasi bahwa hal ini adalah pengambilalihan berbahaya dari situs web Anda. Praktik yang disayangkan tetapi legal adalah membeli nama domain yang baru saja kedaluwarsa demi "mengambil alih" lalu lintas yang mereka miliki untuk tujuan periklanan. Sangat penting halnya untuk menjaga pembayaran untuk nama domain Anda dengan teratur.
> - Langkah 2: Jika situs web Anda telah dirusak, pertama-tama dapatkanlah kembali kendali atas akun log masuk situs web Anda dan atur ulang kata sandinya, lihat bagian [Pertolongan Pertama Darurat Digital pada Masalah Akses Akun](/id/topics/account-access-issues) untuk mendapatkan bantuan.
> - Langkah 3: Buat cadangan dari situs yang dirusak, yang nantinya dapat digunakan untuk menyelidiki kerusakannya.
> - Langkah 4: Nonaktifkan situs web Anda sementara waktu - gunakan _landing page_ sederhana atau halaman "parkir".
> - Langkah 5: Pastikan bagaimana situs Anda bisa diretas. Penyedia _hosting_ Anda mungkin bisa membantu. Masalah yang umum adalah di bagian lama situs Anda dengan skrip/alat khusus yang berjalan di dalamnya, Sistem Manajemen Konten yang kedaluwarsa, dan pemrograman khusus dengan kecacatan keamanan.
> - Langkah 6: Pulihkan situs asli Anda dengan menggunakan cadangan data. Jika Anda atau perusahaan _hosting_ Anda sama-sama tidak memiliki cadangan data, Anda mungkin harus membangun ulang situs web Anda dari awal! Perhatikan juga bahwa jika satu-satunya cadangan data ada di penyedia _hosting_ Anda, si penyerang mungkin bisa menghapusnya saat mereka mengambil alih kendali situs Anda!

Apakah rekomendasi ini membantu?

##### Options

- [Ya](#resolved_end)
- [Tidak](#website-down_end)

### website-down_end

#### Saya masih membutuhkan bantuan

> Jika Anda masih membutuhkan bantuan setelah semua pertanyaan yang Anda jawab, Anda dapat menghubungi organisasi tepercaya dan meminta bantuan mereka.
>
> Sebelum Anda menghubungi mereka, silakan tanyakan pada diri Anda sendiri pertanyaan-pertanyaan berikut ini:
>
> - Bagaimana struktur perusahaan/organisasi tersebut, dan bagaimana kelanjutannya? Jenis pemeriksaan atau pelaporan seperti apa yang harus mereka lakukan, jika ada?
> - Pertimbangkan di negara mana organisasi tersebut didaftarkan secara hukum serta penegakan hukum dan permintaan hukum lainnya yang harus mereka patuhi.
> - Log apa yang dibuat, dan untuk berapa lama tersedia?
> - Adakah batasan terkait jenis konten yang akan di-_hosting_/di-_proxy_ oleh layanan tersebut, dan bisakah hal tersebut berdampak pada situs Anda?
> - Adakah batasan di negara tempat mereka dapat menyediakan layanan?
> - Apakah mereka menerima bentuk pembayaran yang bisa Anda gunakan? Apakah Anda mampu membayar layanan mereka?
> - Komunikasi yang aman - Anda harus bisa masuk (_log in_) dengan aman dan berkomunikasi dengan penyedia layanan secara pribadi.
> - Apakah ada opsi untuk autentikasi dua-langkah, untuk meningkatkan keamanan akses administrator? Hal ini atau kebijakan akses aman yang terkait dapat membantu mengurangi ancaman bentuk serangan lainnya terhadap situs web Anda.
> - Jenis bantuan berkelanjutan seperti apa yang bisa Anda akses? Apakah ada biaya tambahan untuk bantuan tersebut, dan/atau akankah Anda menerima bantuan yang cukup jika Anda menggunakan layanan ‘gratis’?
> - Bisakah Anda "menguji coba" situs web Anda sebelum Anda melanjutkan melalui situs _staging_?

Berikut adalah daftar organisasi yang dapat membantu masalah Anda:

[orgs](:organisations?services=web_protection)

### legal_end

#### Tidak, saya memerlukan dukungan hukum

> Jika situs web Anda tidak berfungsi karena alasan hukum dan Anda membutuhkan bantuan hukum, silakan hubungi organisasi yang dapat membantu:

[orgs](:organisations?services=legal)

### advocacy_end

#### Ya, saya ingin melaporkan hal ini secara terbuka dan memerlukan dukungan untuk kampanye advokasi saya

> Jika Anda menginginkan bantuan untuk meluncurkan kampanye melawan penyensoran, silakan hubungi organisasi yang dapat membantu dengan upaya advokasi:

[orgs](:organisations?services=advocacy)

### ddos_end

#### Saya butuh bantuan dengan serangan DDoS

> Jika Anda membutuhkan bantuan untuk menanggulangi serangan DDoS terhadap situs web Anda, silakan mengacu pada organisasi yang berspesialisasi pada penanggulangan DDoS:

[orgs](:organisations?services=ddos)

### web-hosting_end

#### Saya butuh bantuan dengan hosting situs web

> Jika Anda mencari organisasi tepercaya untuk meng-_hosting_ situs web Anda di _server_ yang aman, silakan lihat daftar di bawah ini.
>
> Sebelum Anda menghubungi organisasi tersebut, silakan pikirkan pertanyaan-pertanyaan berikut:
>
> - Apakah mereka menawarkan bantuan penuh dalam memindahkan situs Anda ke layanan mereka?
> - Apakah layanannya setara atau lebih baik dari _host_ Anda yang sekarang, setidaknya untuk peralatan/layanan yang Anda gunakan? Hal-hal terpenting yang harus Anda periksa adalah:
>   - Pengelolaan dasbor, misalnya cPanel
>   - Akun surel (berapa banyak, kuota, akses melalui SMTP, IMAP)
>   - Basis data (berapa banyak, tipe, akses)
>   - Akses jarak jauh melalui SFTP/SSH
>   - Dukungan untuk bahasa pemrograman (PHP, Perl, Ruby, akses cgi-bin…) atau CMS (Drupal, Joomla, Wordpress…) yang digunakan situs Anda

Berikut adalah daftar organisasi yang dapat membantu Anda dengan urusan _hosting_ web:

[orgs](:organisations?services=web_hosting)

### resolved_end

#### Masalah saya telah teratasi

Kami harap panduan Pertolongan Pertama Darurat Digital ini bermanfaat. Silakan berikan masukan [via surel](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

- **Cadangan data** - Selain layanan dan saran di bawah ini, ada baiknya Anda memastikan bahwa Anda memiliki cadangan data (yang Anda simpan di suatu tempat selain tempat yang sama di mana Anda menyimpan situs web Anda!). Banyak _host_ dan platform situs web yang menyertakannya, tapi lebih baik Anda juga memiliki salinan luring tambahan.

- **Selalu perbarui perangkat lunak** - Jika Anda menggunakan Sistem Pengelolaan Konten (CMS) seperti WordPress atau Drupal, pastikan teknologi situs web Anda diperbarui ke perangkat lunak terbaru, terutama jika ada pembaruan keamanan.

- **Pemantauan** - Ada banyak layanan yang dapat senantiasa memeriksa situs Anda dan mengirim surel atau pesan kepada Anda jika situs Anda tidak berfungsi. [Posting ini](https://www.websiteplanet.com/blog/website-down-top-website-monitoring-tools/) membuat daftar 9 layanan terpopuler. Waspadalah bahwa surel atau nomor telepon yang Anda gunakan untuk pemantauan tentunya akan dikaitkan dengan pengelolaan situs web.

### Resources

- [EFF: Menjaga situs Anda tetap hidup (dalam bahasa Inggris)](https://www.eff.org/keeping-your-site-alive)
- [CERT.be: Tindakan proaktif dan reaktif DDoS (dalam bahasa Inggris)](https://ccb.belgium.be/sites/default/files/DDoS-proactive-reactive.pdf)
- [Sucuri: Apa itu Serangan DDoS? (dalam bahasa Inggris)](https://sucuri.net/guides/what-is-a-ddos-attack/)
