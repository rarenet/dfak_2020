---
layout: topic
title: "Saya Menerima Pesan Mencurigakan"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: id
summary: "Saya menerima pesan, tautan atau surel mencurigakan, apa yang harus saya lakukan terhadapnya?"
date: 2023-04
permalink: topics/suspicious-messages
parent: Home
order: 4
---

# Saya Menerima Pesan Mencurigakan

Bentuk email mencurigakan yang paling umum adalah surel _phishing_, tetapi Anda juga dapat menerima pesan _mencurigakan_ di akun media sosial dan/atau aplikasi perpesanan. Pesan _phishing_ bertujuan mengelabui Anda agar memberikan informasi pribadi, keuangan, atau akun Anda. Mereka mungkin meminta Anda mengunjungi situs web palsu atau menghubungi nomor layanan pelanggan palsu. Pesan _phishing_ juga dapat berisi lampiran yang akan memasang perangkat lunak berbahaya di komputer Anda saat dibuka.

Jika Anda tidak yakin tentang keaslian pesan yang Anda terima, atau apa yang harus dilakukan, Anda dapat menggunakan kuesioner berikut sebagai alat panduan untuk mendiagnosis situasi lebih lanjut atau untuk membagikan pesan tersebut dengan organisasi tepercaya eksternal yang akan memberi Anda analisis yang lebih rinci dari pesan Anda tersebut.

Ingatlah bahwa menerima surel yang mencurigakan tidak berarti akun Anda telah disalahgunakan. Jika menurut Anda surel atau pesan yang diterima mencurigakan, jangan dibuka. Jangan balas pesan, jangan klik tautan apa pun, dan jangan mengunduh lampiran apa pun.

## Workflow

### intro

#### Apakah Anda telah melakukan sesuatu pada pesan atau tautan yang ada?

##### Options

- [Saya mengeklik tautannya](#link-clicked)
- [Saya memasukkan kredensial](#account-security_end)
- [Saya mengunduh berkas](#device-security_end)
- [Saya membalas dengan informasi](#reply-personal-info)
- [Saya belum melakukan tindakan apapun](#do-you-know-sender)

### do-you-know-sender

#### Saya belum melakukan tindakan apapun

Apakah Anda mengenali si pengirim pesan? Ketahuilah bahwa pengirim pesan mungkin [dipalsukan](https://en.wikipedia.org/wiki/Email_spoofing) supaya tampak seperti seseorang yang Anda percaya.

##### Options

- [Pengirim adalah seseorang atau organisasi yang saya kenal](#known-sender)
- [Pengirim adalah penyedia layanan (seperti penyedia layanan surel, _hosting_, media sosial, atau bank)](#service-provider)
- [Pesan tersebut dikirim oleh orang atau organisasi yang tidak saya kenal](#share)

### known-sender

#### Pengirim adalah seseorang atau organisasi yang saya kenal

> Bisakah Anda menghubungi pengirim menggunakan saluran komunikasi lain? Misalnya, jika Anda menerima pesannya melalui surel, bisakah Anda memverifikasinya secara langsung dengan pengirim melalui Signal, WhatsApp, atau telepon? Pastikan Anda menggunakan metode kontak yang sudah ada. Anda tidak semestinya mempercayai nomor telepon yang tercantum di pesan yang mencurigakan.

Apakah Anda telah mengonfirmasi bahwa si pengirim adalah orang yang benar-benar mengirim pesan tersebut?

##### Options

- [Ya](#resolved_end)
- [Tidak](#share)

### service-provider

#### Pengirim adalah penyedia layanan (seperti penyedia layanan surel, _hosting_, media sosial, atau bank)

> Pada skenario ini, penyedia layanan adalah perusahaan atau merek apa pun yang menyediakan layanan yang Anda gunakan atau berlangganan. Daftar ini dapat berisi penyedia layanan surel Anda (Google, Yahoo, Microsoft, ProtonMail...), penyedia layanan media sosial Anda (Facebook, Twitter, Instagram...), atau platform daring yang memiliki informasi finansial Anda (Paypal, Amazon, bank, Netflix...).
>
> Apakah Anda memiliki cara untuk memeriksa apakah pesan tersebut asli? Banyak penyedia layanan yang juga akan menyediakan salinan pemberitahuan atau dokumen lain di halaman akun Anda. Contohnya, jika pesannya dari Facebook, seharusnya pesan tersebut termuat di [daftar surel pemberitahuan](https://www.facebook.com/settings?tab=security&section=recent_emails), atau jika itu dari bank Anda, Anda bisa menghubungi layanan pelanggannya.

Pilih salah satu dari pilihan di bawah ini:

##### Options

- [Saya dapat memverifikasi bahwa pesan tersebut resmi dari penyedia layanan saya](#resolved_end)
- [Saya tidak dapat memverifikasi pesan tersebut](#share)
- [Saya tidak berlangganan layanan ini dan/atau mengharapkan pesan dari mereka](#share)

### link-clicked

#### Saya mengeklik tautannya

> Dalam beberapa pesan mencurigakan, tautan yang ada dapat membawa Anda ke halaman log masuk palsu yang akan mencuri kredensial Anda atau tipe halaman lain yang mungkin mencuri informasi pribadi atau finansial Anda. Kadang, tautan tersebut akan meminta Anda untuk mengunduh lampiran yang akan memasang perangkat lunak berbahaya pada komputer Anda saat dibuka. Tautan tersebut juga bisa membawa Anda ke situs web yang disediakan secara khusus yang mungkin mencoba untuk menginfeksi peranti Anda dengan perangkat lunak berbahaya atau pengintai.

Bisakah Anda ceritakan apa yang terjadi setelah Anda mengeklik tautan tersebut?

##### Options

- [Tautan meminta saya untuk memasukkan kredensial](#account-security_end)
- [Tautan mengunduh sebuah berkas](#device-security_end)
- [Tak ada yang terjadi, tapi saya tidak yakin](#clicked-but-nothing-happened)

### clicked-but-nothing-happened

#### Tak ada yang terjadi, tapi saya tidak yakin

> Fakta bahwa Anda telah mengeklik tautan mencurigakan dan Anda tidak menyadari perilaku aneh apapun bukan berarti tidak ada tindakan berbahaya yang terjadi di balik layar. Ada sejumlah skenario yang harus Anda pikirkan. Yang paling tidak mengkhawatirkan adalah bahwa pesan yang Anda terima adalah spam yang digunakan untuk keperluan iklan. Pada kasus ini, beberapa iklan akan muncul. Pada beberapa kasus, iklan-iklan tersebut dapat juga menjadi berbahaya.
>
> Dalam skenario terburuk, dengan mengeklik tautan, eksploitasi sedang terjadi untuk mengeksekusi perintah berbahaya pada sistem Anda. Jika hal tersebut terjadi, ini mungkin karena _browser_ Anda tidak menggunakan versi terbaru dan memiliki kerentanan yang memungkinkan eksploitasi ini. Dalam kasus yang jarang terjadi di mana _browser_ Anda menggunakan versi terbaru dan skenario tersebut terjadi, kerentanan yang tereksploitasi bisa jadi tak diketahui. Pada kedua kasus, peranti Anda dapat mulai tampak mencurigakan.
>
> Pada skenario lain, dengan mengunjungi tautan tersebut Anda mungkin telah menjadi korban [serangan skrip lintas-situs (atau XSS)](https://en.wikipedia.org/wiki/Cross-site_scripting). Hasil dari serangan ini adalah pencurian _cookie_ Anda yang digunakan untuk mengautentikasi Anda pada situs web yang Anda kunjungi, jadi penyerang akan dapat masuk ke situs tersebut menggunakan nama pengguna Anda. Tergantung dari keamanan situs tersebut, penyerang mungkin bisa atau tidak bisa mengganti kata sandinya. Hal ini menjadi semakin serius jika situs web yang rentan terhadap XSS adalah situs yang Anda kelola, karena dalam kasus demikian penyerang akan dapat mengautentikasi sebagai admin situs web Anda. Untuk mengidentifikasi serangan XSS, periksa apakah tautan yang Anda klik mengandung [_script string_](https://owasp.org/www-community/attacks/xss/). Hal tersebut bisa juga dikodekan dalam HEX atau Unicode.

##### Options

- [Ada beberapa iklan yang muncul. Saya tidak yakin apakah mereka berbahaya atau tidak](#suspicious-device_end)
- [Ya, _browser_ saya tidak menggunakan versi terbaru dan/atau peranti saya mulai tampak mencurigakan setelah saya mengeklik tautan](#suspicious-device_end)
- [Ada skrip di tautan atau sebagian dikodekan](#cross-site-script)
- [Tidak ada skrip yang dapat diidentifikasi](#suspicious-device_end)

### cross-site-script_1

#### Apakah Anda memiliki akun pada situs yang Anda kunjungi?

##### Options

- [Ya](#account-security_end)
- [Tidak](#cross-site-script-2)

### cross-site-script-2

#### Apakah situs yang Anda kunjungi terakhir adalah situs web yang Anda kelola?

##### Options

- [Ya](#cross-site-script-admin-compromised_end)
- [Tidak](#cross-site-script-3)

### cross-site-script-admin-compromised_end

#### Organisasi berikut dapat membantu menyelidiki dan merespons insiden ini:

> Dalam kasus ini, si penyerang mungkin memiliki _cookie_ yang valid yang memungkinkan mereka untuk mengakses akun admin Anda. Hal pertama yang harus dilakukan adalah masuk ke antarmuka administrasi dan mematikan sesi apa pun yang aktif, atau cukup ubah kata sandinya. Anda juga harus memeriksa apakah si penyerang mengunggah artifak apa pun ke situs Anda dan/atau membuat posting konten berbahaya, dan jika ada maka hapuslah.

[orgs](:organisations?services=forensic)

### cross-site-script-3

#### Tidak, saya tidak mengelola situs ini

> Seharusnya Anda baik-baik saja. Namun, dalam beberapa kasus yang jarang terjadi, XSS dapat digunakan untuk "meminjam" _browser_ Anda untuk meluncurkan serangan lain.

##### Options

- [Saya ingin membuat asesmen apakah peranti saya terinfeksi](/id/topics/device-acting-suspiciously)
- [Saya merasa baik-baik saja](#final_tips)

### reply-personal-info

#### Saya membalas dengan informasi

> Tergantung dari tipe informasi yang Anda bagikan, Anda mungkin perlu mengambil tindakan segera.

Jenis informasi seperti apa yang Anda bagikan?

##### Options

- [Saya membagikan informasi akun yang bersifat rahasia](#account-security_end)
- [Saya membagikan informasi publik](#share)
- [Saya tidak yakin seberapa sensitif informasi tersebut dan saya membutuhkan bantuan](#help_end)

### share

#### Berbagi informasi Anda atau pesan mencurigakan

> Membagikan pesan mencurigakan yang Anda terima dapat membantu melindungi kolega dan komunitas Anda yang mungkin juga terdampak. Anda mungkin juga ingin meminta bantuan kepada seseorang yang Anda percaya untuk memberitahu Anda jika pesan tersebut berbahaya. Pertimbangkan untuk membagi pesan tersebut dengan organisasi yang dapat menganalisisnya.
>
> Untuk membagikan pesan mencurigakan yang Anda terima, pastikan untuk menyertakan pesan itu sendiri berikut informasi tentang si pengirim. Jika pesan tersebut berupa surel, harap pastikan untuk menyertakan surel lengkap termasuk _header_-nya menggunakan [panduan Computer Incident Response Center Luxembourg (CIRCL) berikut ini](https://www.circl.lu/pub/tr-07/).

Apakah Anda memerlukan bantuan lebih lanjut?

##### Options

- [Ya, saya butuh bantuan lebih lanjut](#help_end)
- [Tidak, saya sudah mengatasi masalah saya](#resolved_end)

### device-security_end

#### Saya mengunduh berkas

> Jika beberapa berkas telah terunduh ke peranti Anda, bisa jadi peranti Anda berisiko!
> Silakan hubungi organisasi yang dapat membantu Anda di bawah ini. Setelahnya, silakan [bagikan pesan mencurigakan yang Anda terima](#share).

[orgs](:organisations?services=device_security)

### account-security_end

#### Saya memasukkan atau memberikan informasi akun

> Jika Anda memasukkan kredensial Anda, atau Anda telah menjadi korban serangan skrip lintas-situs, akun Anda bisa jadi berisiko!
>
> Jika Anda merasa akun Anda telah disalahgunakan, Anda disarankan juga untuk mengikuti alur kerja Pertolongan Pertama Darurat Digital pada [akun yang disalahgunakan](/id/topics/account-access-issues).
>
> Anda juga harus memberi tahu komunitas Anda tentang kampanye _phishing_ ini, dan membagikan pesan mencurigakan tersebut kepada organisasi yang dapat menganalisisnya.

Apakah Anda ingin berbagi informasi tentang pesan yang Anda terima atau Anda butuh bantuan lebih lanjut sebelumnya?

##### Options

- [Saya ingin membagikan pesan mencurigakan tersebut](#share)
- [Saya butuh bantuan lebih lanjut untuk mengamankan akun saya](#account_end)
- [Saya butuh bantuan lebih lanjut untuk menganalisis pesan tersebut](#analysis_end)

### suspicious-device_end

#### Saya tidak yakin apa yang terjadi

> Jika Anda mengeklik sebuah tautan dan tidak yakin apa yang terjadi, peranti Anda mungkin telah terinfeksi tanpa Anda sadari. Jika Anda ingin mengeksplorasi kemungkinan ini, atau merasa peranti Anda mungkin terinfeksi, kunjungi alur kerja ["Peranti saya tampak mencurigakan"](/id/topics/device-acting-suspiciously).

Jika Anda memerlukan bantuan segera karena peranti Anda tampak mencurigakan, Anda dapat menghubungi organisasi yang dapat mendukung Anda di bawah ini. Setelahnya, silakan [bagikan pesan mencurigakan yang Anda terima](#share).

[orgs](:organisations?services=device_security)

### help_end

#### Saya butuh bantuan lebih lanjut

> Anda harus mencari bantuan dari kolega Anda atau orang lain untuk lebih memahami risiko dari informasi yang Anda bagikan. Orang lain di organisasi atau jaringan Anda mungkin juga pernah menerima permintaan yang sama.

Silakan hubungi organisasi yang dapat membantu Anda di bawah ini. Setelahnya, Setelahnya, silakan [bagikan pesan mencurigakan yang Anda terima](#share).

[orgs](:organisations?services=digital_support)

### account_end

#### Saya memerlukan bantuan lebih lanjut untuk mengamankan akun saya

Jika akun Anda telah disalahgunakan dan Anda membutuhkan bantuan untuk mengamankannya, silakan hubungi organisasi yang dapat membantu Anda di bawah ini.

[orgs](:organisations?services=account)

### analysis_end

#### Saya memerlukan bantuan lebih lanjut untuk menganalisis pesan tersebut

Organisasi berikut dapat menerima pesan mencurigakan yang Anda terima dan menyelidikinya lebih jauh untuk Anda:

[orgs](:organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

#### Ya, masalah saya telah teratasi

Kami harap panduan Pertolongan Pertama pada Darurat Digital (P2D2) ini bermanfaat. Silakan beri masukan [via surel](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

### Final Tips

Peraturan pertama yang perlu diingat: jangan pernah memberikan informasi pribadi apapun melalui surel atau alat perpesanan lainnya. Tidak ada institusi, bank, dll. yang akan meminta informasi ini melalui surel, platform jejaring sosial atau aplikasi perpesanan. Mungkin tidak semudah itu mengetahui apakah sebuah surel atau situs web tersebut resmi atau tidak, tetapi ada beberapa tip yang dapat membantu Anda menilai surel yang Anda terima.

- Perihal mendesak: surel mencurigakan biasanya memperingatkan tentang perubahan mendadak pada sebuah akun dan meminta Anda segera bertindak untuk memverifikasi akun Anda.
- Di badan surel, Anda mungkin menemukan pertanyaan yang meminta Anda untuk “memverifikasi” atau “memperbarui akun Anda” atau “kegagalan memperbarui catatan Anda akan mengakibatkan penangguhan akun”. Biasanya aman untuk berasumsi bahwa tak ada organisasi kredibel, yang mana telah Anda sediakan informasi tentang Anda, akan meminta Anda untuk memasukkan kembali informasi tersebut, jadi jangan jatuh ke perangkap ini
- Waspada terhadap pesan, lampiran, tautan, dan halaman log masuk yang muncul tanpa diminta.
- Perhatikan kesalahan pengejaan dan tata bahasa.
- Pada surel, klik untuk melihat alamat lengkap si pengirim, tidak hanya nama yang ditampilkan.
- Berhati-hatilah dengan tautan yang disingkat - karena Anda tidak dapat memeriksa tujuan akhir, tautan tersebut dapat membawa Anda ke situs web berbahaya
- Saat Anda mengarahkan _mouse_ ke suatu tautan, URL sebenarnya yang akan Anda tuju akan ditampilkan berupa _popup_ atau di bagian bawah jendela _browser_ Anda.
- _Header_ surel yang menyertakan kata “dari:” dapat dibuat sedemikian rupa untuk terlihat resmi. Dengan memeriksa _header_ SPF dan DKIM, Anda dapat mengetahui apakah tiap-tiap alamat IP diizinkan (atau tidak) untuk mengirim surel atas nama domain si pengirim, dan apakah _header_ atau kontennya telah diubah saat transit. Pada surel yang resmi, bagian [SPF](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-spf) dan [DKIM](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-dkim) harus berstatus ‘PASS’, jika tidak maka sebaiknya jangan mempercayai surel tersebut. Alasannya karena surel tersebut dipalsukan, atau dalam kasus yang langka, _server_ surel tidak terkonfigurasi dengan benar.
- [Tanda tangan digital](https://www.gnupg.org/gph/en/manual/x135.html) dapat memberi tahu kita apakah suatu surel dikirim dari pengirim yang resmi dan apakah surel tersebut telah dimodifikasi dalam prosesnya atau tidak. Jika surel tersebut ditandatangani dengan OpenPGP, periksa apakah tanda tangan tersebut sudah diverifikasi atau belum. Untuk memverifikasi tanda tangan, Anda perlu memasang OpenPGP dan mengimpor kunci publik yang terkait dengan ID di tanda tangan pesan. Sebagian besar klien surel modern yang mendukung tanda tangan digital akan mengotomatiskan verifikasi untuk Anda dan memberi tahu Anda melalui antarmuka pengguna mereka jika tanda tangan tersebut diverifikasi atau tidak.
- Akun yang disalahgunakan dapat mengirimkan surel atau pesan berbahaya yang dapat memverifikasi semua kondisi di atas dan terlihat resmi. Namun, biasanya isi pesannya tampak tak biasa. Jika isi pesan surel tersebut tampak aneh, selalu ada baiknya untuk memeriksa dengan pengirim yang sah melalui kanal komunikasi lain sebelum mengambil tindakan apapun.
- Ada baiknya mempraktikkan untuk membaca dan menulis surel Anda dalam format teks biasa. Surel berbasis HTML dapat diubah sedemikian rupa untuk menyembunyikan kode atau URL berbahaya. Anda dapat menemukan instruksi untuk menonaktifkan HTML pada berbagai klien surel dalam [situs ini](https://useplaintext.email/).
- Gunakan sistem operasi versi terbaru di ponsel atau komputer Anda (periksa versi untuk [Android](https://id.wikipedia.org/wiki/Android_version_history), [iOS](https://en.wikipedia.org/wiki/IOS_version_history), [macOS](https://en.wikipedia.org/wiki/MacOS_version_history) dan [Windows](https://id.wikipedia.org/wiki/Microsoft_Windows)).
- Segera perbarui sistem operasi Anda dan semua aplikasi/program yang telah Anda pasang, terutama yang menerima informasi (_browser_, aplikasi/program perpesanan dan obrolan, klien surel, dll.). Hapus semua aplikasi/program yang tidak Anda butuhkan.
- Gunakan _browser_ yang bisa diandalkan (misalnya Mozilla Firefox). Tingkatkan keamanan _browser_ Anda dengan meninjau ulang ekstensi/_add-on_ yang terpasang di _browser_ Anda. Biarkan ekstensi/_add-on_ yang Anda percaya (misalnya: [Privacy Badger](https://privacybadger.org/), [uBlock Origin](https://ublockorigin.com), [Facebook Container](https://addons.mozilla.org/en-US/firefox/addon/facebook-container/), [Cookie AutoDelete](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete), [NoScript](https://noscript.net/)).
- Buat cadangan aman secara teratur untuk informasi Anda.
- Lindungi akun Anda dengan kata sandi yang kuat, autentikasi 2-langkah, dan pengaturan yang aman.

### Resources

Berikut adalah sejumlah sumber informasi untuk menemukan pesan mencurigakan dan menghindari _phishing_.

- [Citizen Lab: Komunitas berisiko - Ancaman Digital yang Ditargetkan ke Masyarakat Sipil (dalam bahasa Inggris)](https://targetedthreats.net)
- [Surveillance Self-Defense: Cara Menghindari Serangan _Phising_](https://ssd.eff.org/en/module/how-avoid-phishing-attacks)
- [Alat analisis _header_ pesan Google](https://toolbox.googleapps.com/apps/messageheader/)
