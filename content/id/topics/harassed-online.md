---
layout: topic
title: "Apakah Anda menjadi sasaran pelecehan daring?"
author: Flo Pagano, Natalia Krapiva
language: id
summary: "Apakah Anda menjadi sasaran pelecehan daring?"
date: 2023-04
permalink: topics/harassed-online
parent: Home
order: 7
---

# Apakah Anda menjadi sasaran pelecehan daring?

Internet, dan khususnya platform media sosial, telah menjadi ruang yang kritis bagi anggota dan organisasi masyarakat sipil, terutama untuk perempuan, kelompok LGBTQIA+, dan kelompok minoritas lainnya, untuk mengekspresikan diri dan membuat suara mereka didengar. Tapi di saat yang bersamaan, internet juga telah menjadi ruang di mana kelompok-kelompok tersebut dengan mudah menjadi sasaran ketika mengekspresikan pandangan mereka. Kekerasan dan pelecehan daring menyangkal hak perempuan, kelompok LGBTQIA+, dan banyak orang kurang beruntung lainnya untuk mengekspresikan diri mereka secara setara, bebas, dan tanpa rasa takut.

Kekerasan dan pelecehan daring memiliki banyak bentuk, dan entitas jahat seringkali mengandalkan impunitas, juga karena kurangnya undang-undang yang melindungi korban pelecehan di banyak negara, dan terutama karena tidak ada strategi perlindungan yang universal: mereka perlu disesuaikan secara kreatif tergantung pada jenis serangan apa yang diluncurkan. Oleh karena itu, penting untuk mengidentifikasi tipologi serangan yang menarget Anda untuk kemudian memutuskan langkah apa yang dapat diambil.

Bagian Pertolongan Pertama Darurat Digital ini akan memandu Anda melalui beberapa langkah dasar untuk memahami jenis serangan yang Anda alami dan merencanakan cara melindungi dari serangan tersebut, atau menemukan bantuan keamanan digital yang dapat mendukung Anda.

Jika Anda menjadi sasaran pelecehan daring, kami sarankan Anda membaca rekomendasi tentang [cara menjaga diri sendiri](/id/self-care) dan [cara mendokumentasikan serangan](/id/documentation), kemudian ikuti kuesioner ini untuk mengidentifikasi jenis masalah Anda dan menemukan solusi yang sesuai.

## Workflow

### physical-wellbeing

#### Apakah Anda mengkhawatirkan integritas atau kesejahteraan fisik Anda?

##### Options

- [Ya](#physical-risk_end)
- [Tidak](#location)

### location

#### Apakah si penyerang tampaknya mengetahui lokasi fisik Anda?

##### Options

- [Ya](#location_tips)
- [Tidak](#device)

### location_tips

#### Penyerang tampaknya mengetahui lokasi fisik saya

> Periksa posting terbaru Anda di media sosial: apakah media sosial tersebut menyertakan lokasi persis Anda? Jika demikian, nonaktifkan akses GPS untuk aplikasi media sosial dan layanan lain di ponsel Anda sehingga saat Anda memposting pembaruan, lokasi Anda tidak ditampilkan.
>
> - [Cara menonaktifkan layanan lokasi di iOS](https://support.apple.com/id-id/HT207092)
> - [Kelola pengaturan lokasi di Android](https://support.google.com/accounts/answer/3467281?hl=id)
>
> Periksa foto diri Anda yang Anda posting secara daring: apakah foto tersebut menyertakan detail tempat yang dapat dikenali dengan jelas dan dapat menunjukkan lokasi Anda berada dengan jelas? Untuk melindungi diri Anda dari calon penguntit, sebaiknya jangan tunjukkan lokasi persis Anda saat memposting foto atau video diri Anda.
>
> Sebagai tindakan pencegahan tambahan, sebaiknya matikan GPS setiap saat – kecuali hanya sebentar saat Anda benar-benar perlu menemukan posisi Anda di peta.

Mungkinkah si penyerang telah mengikuti Anda melalui informasi yang Anda publikasikan secara daring? Atau apakah Anda masih merasa si penyerang mengetahui lokasi fisik Anda melalui cara lain?

##### Options

- [Tidak, sepertinya saya telah menyelesaikan masalah saya](#resolved_end)
- [Ya, saya masih berpikir si penyerang tahu keberadaan saya](#device)
- [Tidak, tapi saya punya masalah lain](#account)

### device

#### Apakah menurut Anda si penyerang telah mengakses atau sedang mengakses peranti Anda?

##### Options

- [Ya](#device-compromised)
- [Tidak](#account)

### device-compromised

#### Penyerang memiliki akses ke perangkat saya

> Ubah kata sandi akses ke peranti Anda menjadi kata sandi yang unik, panjang, dan rumit:
>
> - [Mac OS](https://support.apple.com/id-id/HT202860)
> - [Windows](https://support.microsoft.com/id-id/help/4490115/windows-change-or-reset-your-password)
> - [iOS - ID Apple](https://support.apple.com/id-id/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=id)

Apakah Anda masih merasa bahwa si penyerang mungkin mengendalikan peranti Anda?

##### Options

- [Tidak, sepertinya saya telah menyelesaikan masalah saya](#resolved_end)
- [Tidak, tapi saya punya masalah lain](#account)
- [Tidak](#info_stalkerware)

### info_stalkerware

#### Penyerang tampaknya tidak mengontrol perangkat saya

> [Perangkat penguntit (_Stalkerware_)](https://en.wikipedia.org/wiki/Stalkerware) adalah perangkat lunak apa pun yang digunakan untuk memantau aktivitas atau lokasi seseorang dengan tujuan menguntit atau mengendalikan mereka.

Jika menurut Anda seseorang mungkin memata-matai Anda melalui aplikasi yang mereka pasang di peranti seluler Anda, [Alur kerja Pertolongan Pertama Darurat Digital ini akan membantu Anda memutuskan apakah peranti Anda terinfeksi perangkat lunak berbahaya, serta cara mengambil langkah untuk membersihkannya](/id/topics/device-acting-suspiciously).

Apakah Anda memerlukan lebih banyak bantuan untuk memahami serangan yang Anda alami?

##### Options

- [Tidak, sepertinya saya telah menyelesaikan masalah saya](#resolved_end)
- [Ya, saya masih punya beberapa masalah yang ingin saya selesaikan](#account)

### account

#### Tidak, tapi saya punya masalah lain

> Apakah seseorang telah mendapatkan akses ke peranti Anda atau tidak, ada kemungkinan bahwa mereka telah mengakses akun daring Anda dengan meretasnya atau karena mereka mengetahui atau memecahkan kata sandi Anda.
>
> Jika seseorang memiliki akses ke satu atau beberapa akun daring Anda, mereka dapat membaca pesan pribadi Anda, mengidentifikasi kontak Anda, memublikasi posting, foto, atau video pribadi Anda, atau mulai meniru identitas Anda.
>
> Tinjau aktivitas akun daring dan kotak surel Anda (termasuk folder Terkirim dan Sampah) untuk kemungkinan aktivitas yang mencurigakan.

Pernahkah Anda memperhatikan ada posting atau pesan yang menghilang, atau aktivitas lain yang memberi Anda alasan kuat untuk berpikir bahwa akun Anda mungkin telah disalahgunakan?

##### Options

- [Ya](#change_passwords)
- [Tidak](#private-contact)

### change_passwords

#### Ubah kata sandi

> Coba ganti kata sandi untuk setiap akun daring Anda dengan kata sandi yang kuat dan unik.
>
> Anda dapat mempelajari lebih lanjut tentang cara membuat dan mengelola kata sandi di [Panduan Bela Diri terhadap Pengawasan](https://ssd.eff.org/module/creating-strong-passwords).
>
> Sangat disarankan untuk menambahkan lapisan perlindungan kedua ke akun daring Anda dengan mengaktifkan autentikasi 2-langkah (2FA) jika memungkinkan.
>
> Cari tahu cara mengaktifkan autentikasi 2-langkah di [Panduan Bela Diri terhadap Pengawasan](https://ssd.eff.org/module/how-enable-two-factor-authentication).

Apakah Anda masih merasa bahwa seseorang mungkin memiliki akses ke akun Anda?

##### Options

- [Ya](#hacked-account)
- [Tidak](#private-contact)

### hacked-account

#### Ya, menurut saya seseorang mungkin masih memiliki akses ke akun saya

> Jika Anda tidak dapat mengakses akun Anda, ada kemungkinan akun Anda diretas dan si peretas mengubah kata sandi Anda.

Jika menurut Anda akun Anda mungkin telah diretas, coba ikuti alur kerja Pertolongan Pertama Darurat Digital yang dapat membantu Anda menyelesaikan masalah akses akun.

##### Options

- [Bawa saya ke alur kerja untuk menyelesaikan masalah akses akun](/id/topics/account-access-issues)
- [Sepertinya saya telah menyelesaikan masalah saya](#resolved_end)
- [Akun saya baik-baik saja, tetapi saya punya masalah lain](#private-contact)

### private-contact

#### Tidak, menurut saya tidak ada orang yang memiliki akses ke akun saya

Apakah Anda menerima panggilan telepon atau pesan yang tidak diinginkan di aplikasi perpesanan?

##### Options

- [Ya](#change_contact)
- [Tidak](#threats)

### change_contact

#### Saya masih menerima panggilan telepon atau pesan yang tidak diinginkan di aplikasi perpesanan

> Jika Anda menerima panggilan telepon, SMS, atau pesan yang tidak diinginkan di aplikasi yang terkait dengan nomor telepon, surel, atau informasi kontak pribadi lainnya, Anda dapat mencoba mengubah nomor, kartu SIM, surel, atau informasi kontak lainnya yang terkait dengan akun tersebut .
>
> Anda juga harus mempertimbangkan untuk melaporkan dan memblokir pesan dan akun terkait ke platform yang relevan.

Apakah Anda berhasil menghentikan panggilan atau pesan yang tidak diinginkan?

##### Options

- [Ya](#legal)
- [Tidak, saya punya masalah lain](#threats)
- [Tidak, saya butuh bantuan](#harassment_end)

### threats

#### Saya tidak menerima panggilan telepon atau pesan yang tidak diinginkan di aplikasi perpesanan

Apakah Anda sedang diperas atau menerima ancaman melalui surel atau pesan di akun media sosial?

##### Options

- [Ya](#threats_tips)
- [Tidak](#impersonation)

### threats_tips

#### Saya sedang diperas atau menerima ancaman

> Jika Anda menerima pesan yang berisi ancaman, termasuk ancaman kekerasan fisik atau seksual, atau pemerasan, Anda harus mendokumentasikan apa yang terjadi sebanyak mungkin; termasuk merekam tautan dan tangkapan layar apa pun, melaporkan orang tersebut ke platform atau penyedia layanan terkait, dan memblokir penyerang.

Apakah Anda berhasil menghentikan ancaman tersebut?

##### Options

- [Ya](#legal)
- [Ya, tapi saya punya lebih banyak masalah](#impersonation)
- [Tidak, saya butuh bantuan hukum](#legal-warning)

### impersonation

#### Saya mungkin akan ditiru

> Bentuk pelecehan lain yang mungkin Anda alami adalah peniruan identitas.
>
> Misalnya, seseorang mungkin telah membuat akun atas nama Anda dan mengirimkan pesan pribadi atau secara terbuka menyerang seseorang di platform media sosial, menyebarkan disinformasi atau ujaran kebencian, atau bertindak dengan cara lain untuk membuat Anda, organisasi Anda, atau orang lain yang dekat dengan Anda rentan terhadap risiko reputasi dan keamanan.
>
> Jika menurut Anda seseorang sedang meniru identitas Anda atau organisasi Anda, Anda dapat mengikuti alur kerja Pertolongan Pertama Darurat Digital ini, yang memandu Anda melalui berbagai bentuk peniruan untuk mengidentifikasi kemungkinan strategi terbaik untuk menghadapi masalah Anda.

Apa yang ingin Anda lakukan?

##### Options

- [Identitas saya ditiru dan ingin mencari solusi](/id/topics/impersonated)
- [Saya menghadapi bentuk pelecehan yang berbeda](#defamation)

### defamation

#### Apakah seseorang mencoba merusak reputasi Anda dengan menyebarkan informasi palsu?

##### Options

- [Ya](#defamation-yes)
- [Saya menghadapi bentuk pelecehan yang berbeda](#doxing)

### defamation-yes

#### Seseorang mencoba merusak reputasi saya dengan menyebarkan informasi palsu

> Pencemaran nama baik secara umum didefinisikan sebagai membuat pernyataan yang mencederai reputasi seseorang. Pencemaran nama baik bisa dalam bentuk diucapkan (_slander_) atau tertulis (_libel_). Hukum di tiap-tiap negara mungkin berbeda dalam menetapkan batasan yang merupakan pencemaran nama baik untuk ditindak secara hukum. Misalnya, beberapa negara memiliki undang-undang yang sangat melindungi kebebasan berekspresi yang memungkinkan media dan individu pribadi untuk mengatakan hal-hal yang memalukan atau merugikan pejabat publik tertentu selama mereka percaya bahwa informasi tersebut benar. Negara lain mungkin mengizinkan Anda untuk menuntut orang lain dengan lebih mudah karena menyebarkan informasi tentang Anda yang tidak Anda sukai.
>
> Jika seseorang mencoba merusak reputasi Anda atau organisasi Anda, Anda dapat mengikuti alur kerja Pertolongan Pertama Darurat Digital tentang pencemaran nama baik, yang memandu Anda melalui berbagai strategi untuk menangani kampanye pencemaran nama baik.

Apa yang ingin Anda lakukan?

##### Options

- [Saya ingin menemukan strategi melawan kampanye pencemaran nama baik](/id/topics/defamation)
- [Saya menghadapi bentuk pelecehan yang berbeda](#doxing)

### doxing

#### Saya menghadapi bentuk pelecehan yang berbeda

Apakah seseorang memublikasikan informasi atau media pribadi Anda tanpa persetujuan Anda?

##### Options

- [Ya](#doxing-yes)
- [Tidak](#hate-speech)

### doxing-yes

#### Seseorang mempublikasikan informasi pribadi atau media tentang saya tanpa persetujuan saya

> Jika seseorang telah memublikasikan informasi pribadi tentang Anda atau menyebarkan video, foto, atau media lain tentang Anda, Anda dapat mengikuti alur kerja Pertolongan Pertama Darurat Digital yang dapat membantu Anda memahami apa yang terjadi dan bagaimana menanggapi serangan tersebut.

Apa yang ingin Anda lakukan?

##### Options

- [Saya ingin memahami apa yang terjadi dan apa yang dapat saya lakukan](/id/topics/doxing)
- [Saya ingin menerima bantuan](#harassment_end)

### hate-speech

#### Seseorang menyebarkan pesan kebencian terhadap saya

Apakah seseorang menyebarkan pesan kebencian terhadap Anda berdasarkan atribut seperti ras, gender, atau agama?

##### Options

- [Ya](#one-more-persons)
- [Tidak](#harassment_end)

### one-more-persons

#### Satu atau lebih orang menyebarkan pesan kebencian terhadap saya

Apakah Anda diserang oleh satu orang atau lebih?

##### Options

- [Saya telah diserang oleh satu orang](#one-person)
- [Saya telah diserang oleh lebih dari satu orang](#campaign)

### one-person

#### Saya telah diserang oleh satu orang

> Jika ujaran kebencian tersebut berasal dari satu orang, cara termudah dan tercepat untuk membatasi serangan dan mencegah pengguna terus mengirimkan pesan kebencian kepada Anda adalah dengan melaporkan dan memblokirnya. Ingatlah bahwa jika Anda memblokir pengguna tersebut, Anda tidak dapat mengakses kontennya untuk didokumentasikan. Sebelum memblokir, baca [tip mendokumentasikan serangan digital](/id/documentation).
>
> Tidak masalah apakah Anda mengenal si pengganggu atau tidak, selalu disarankan untuk memblokir mereka di platform jejaring sosial jika memungkinkan.
>
> - [Facebook](https://www.facebook.com/help/168009843260943?cms_id=168009843260943)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=id)
> - [Instagram](https://help.instagram.com/426700567389543?cms_id=426700567389543)
> - [TikTok](https://support.tiktok.com/id/using-tiktok/followers-and-following/blocking-the-users)
> - [Twitter](https://help.twitter.com/id/using-twitter/blocking-and-unblocking-accounts)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885/?cms_platform=android&cms_id=1142481766359885&draft=false)

Sudahkah Anda memblokir pengganggu Anda secara efektif?

##### Options

- [Sudah](#legal)
- [Belum](#campaign)

### legal

#### Saya memerlukan bantuan hukum

> Jika Anda mengetahui siapa yang mengganggu Anda, Anda dapat melaporkannya ke otoritas negara Anda jika Anda menganggapnya aman dan sesuai dengan konteks Anda. Setiap negara memiliki undang-undang yang berbeda untuk melindungi orang dari pelecehan daring, dan Anda harus mempelajari undang-undang di negara Anda atau meminta nasihat hukum untuk membantu Anda memutuskan apa yang harus dilakukan.
>
> Jika Anda tidak tahu siapa yang mengganggu Anda, dalam beberapa kasus, Anda dapat melacak kembali identitas penyerang melalui analisis forensik jejak yang mungkin mereka tinggalkan.
>
> Bagaimanapun, jika Anda mempertimbangkan untuk mengambil tindakan hukum, menyimpan bukti serangan yang Anda alami akan sangat penting. Oleh karena itu, sangat disarankan untuk mengikuti [rekomendasi di halaman Pertolongan Pertama Darurat Digital dalam merekam informasi tentang serangan](/id/documentation).

Apa yang ingin Anda lakukan?

##### Options

- [Saya ingin menerima bantuan hukum untuk menuntut penyerang saya](#legal_end)
- [Sepertinya saya telah menyelesaikan masalah saya](#resolved_end)

### campaign

#### Saya memerlukan bantuan untuk berkampanye menghentikan pelecehan tersebut

> Jika Anda diserang oleh lebih dari satu orang, Anda mungkin menjadi sasaran ujaran kebencian atau kampanye pelecehan, dan Anda perlu merenungkan strategi terbaik apa yang berlaku untuk kasus Anda.
>
> Untuk mempelajari tentang semua strategi yang mungkin, baca [Halaman Take Back The Tech tentang strategi melawan ujaran kebencian](https://www.takebackthetech.net/be-safe/hate-speech-strategies).

Sudahkah Anda mengidentifikasi strategi terbaik untuk Anda?

##### Options

- [Sudah](#resolved_end)
- [Belum, saya butuh bantuan untuk menyelesaikan lebih lanjut](#harassment_end)
- [Belum, saya butuh bantuan hukum](#legal_end)

### harassment_end

#### Saya memerlukan bantuan untuk memecahkan masalah lebih lanjut

> Jika Anda masih diganggu dan membutuhkan solusi khusus, silakan hubungi organisasi yang dapat membantu Anda di bawah ini .

[orgs](:organisations?services=harassment)

### physical-risk_end

#### Saya mengkhawatirkan integritas fisik dan kesejahteraan saya

> Jika Anda berada dalam risiko fisik, silakan hubungi organisasi yang dapat membantu Anda di bawah ini.

[orgs](:organisations?services=physical_security)

### legal_end

#### Saya memerlukan bantuan hukum

> Jika Anda memerlukan bantuan hukum, silakan hubungi organisasi yang dapat membantu Anda di bawah ini .
>
> Jika Anda mempertimbangkan untuk mengambil tindakan hukum, sangat penting untuk menyimpan bukti serangan yang Anda alami. Oleh karena itu, sangat disarankan untuk mengikuti [rekomendasi di halaman Pertolongan Pertama Darurat Digital dalam merekam informasi tentang serangan](/id/documentation).

[orgs](:organisations?services=legal)

### resolved_end

#### Masalah saya telah teratasi

Semoga panduan penyelesaian masalah ini bermanfaat. Silakan beri masukan melalui [surel](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

- **Dokumentasikan pelecehan:** Mendokumentasikan serangan atau insiden lainnya yang mungkin Anda saksikan dapat membantu di kemudian hari: ambil tangkapan layar, simpan pesan yang Anda terima dari pelaku pelecehan, dll. Jika memungkinkan, buat jurnal untuk menyusun dokumentasi ini secara sistematis; mencatat tanggal, waktu, platform dan URL, ID pengguna, tangkapan layar, deskripsi tentang apa yang terjadi, dll. Jurnal dapat membantu Anda mendeteksi kemungkinan pola dan indikasi mengenai kemungkinan siapa penyerang Anda. Jika Anda merasa kewalahan, pikirkan seseorang tepercaya yang dapat mendokumentasikan insidennya untuk Anda sementara waktu. Anda harus sangat mempercayai orang yang akan mengelola dokumentasi ini, karena Anda harus memberi mereka kredensial akun pribadi Anda. Sebelum membagi kata sandi Anda dengan orang tersebut, ubahlah sandinya menjadi berbeda dan bagikan melalui cara yang aman - misalnya menggunakan alat dengan [enkripsi ujung-ke-ujung](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools). Begitu Anda merasa Anda dapat memperolah kembali kendali akun tersebut, ingatlah untuk mengubah kata sandi Anda kembali menjadi sesuatu yang unik, [aman](https://ssd.eff.org/en/module/creating-strong-passwords), dan hanya diketahui oleh Anda.

  - Ikuti [rekomendasi di halaman Pertolongan Pertama Darurat Digital dalam merekam informasi tentang serangan](/id/documentation) untuk menyimpan informasi mengenai serangan yang Anda alami dengan cara terbaik.

- **Pasang autentikasi 2-langkah** pada semua akun Anda. Autentikasi 2-langkah bisa menjadi sangat efektif untuk menghentikan seseorang mengakses akun Anda tanpa persetujuan Anda. Jika Anda dapat memilih, jangan menggunakan autentikasi 2-langkah yang berbasis SMS dan pilihlah opsi lainnya, yaitu yang berbasis aplikasi ponsel atau menggunakan kunci keamanan.

  - Jika Anda tidak tahu solusi mana yang terbaik untuk Anda, Anda bisa melihat [infografis “Tipe verifikasi multifaktor apa yang paling sesuai untuk saya?” dari Access Now](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) dan [“Panduan Tipe-tipe Umum Autentikasi Dua-Langkah di Web” dari EFF](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
  - Anda bisa menemukan instruksi untuk mengatur autentikasi 2-langkah pada platform-platform besar di [12 Hari 2FA: Cara Mengaktifkan Autentikasi Dua-Langkah untuk Akun Daring Anda](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Petakan keberadaan daring Anda**. _Self-doxing_ terdiri dari mengeksplorasi kecerdasan buatan sumber-terbuka pada diri sendiri untuk mencegah para aktor jahat menemukan dan menggunakan informasi tersebut untuk meniru identitas Anda. Pelajari lebih lanjut tentang cara mencari jejak daring Anda di [Panduan Saluran Bantuan Access Now untuk mencegah _doxing_](https://guides.accessnow.org/self-doxing.html)

- **Jangan terima pesan dari pengirim yang tidak dikenal.** Beberapa platform perpesanan, seperti WhatsApp, Signal, atau Facebook Messenger, memungkinkan Anda melakukan pratinjau pesan sebelum menerima pengirim tersebut sebagai tepercaya. iMessage Apple juga memungkinkan Anda mengubah pengaturan untuk menyaring pesan dari pengirim yang tidak dikenal. Jangan pernah menerima pesan atau kontak jika terlihat mencurigakan atau Anda tidak mengenal pengirimnya.

### Resources

- [Dokumentasi Komunitas Saluran Bantuan Access Now: Panduan untuk mencegah _doxing_ (dalam bahasa Inggris)](https://guides.accessnow.org/self-doxing.html)
- [Dokumentasi Komunitas Saluran Bantuan Access Now: Pertanyaan Umum - pelecehan Daring yang Menargetkan Anggota Masyarakat Sipil (dalam bahasa Inggris)](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)​​​​​​​
- [PEN America: Manual di Lapangan untuk pelecehan Daring (dalam bahasa Inggris)](https://onlineharassmentfieldmanual.pen.org/)
- [Equality Labs: Panduan _Anti-doxing_ untuk Aktivis untuk Menghadapi Serangan dari Kelompok Ultra Kanan (dalam bahasa Inggris)](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Mengamankan Identitas Digital Anda (dalam bahasa Inggris)](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Jaringan Nasional untuk Akhiri Kekerasan Dalam Rumah Tangga: Tip Dokumentasi untuk Penyintas Penyalahgunaan dan Penguntitan Teknologi (dalam bahasa Inggris)](https://www.techsafety.org/documentationtips)
