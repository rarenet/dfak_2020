---
layout: contact-method
title: PGP
author: mfc
language: id
summary: Metode kontak
date: 2020-11
permalink: /id/contact-methods/pgp.md
parent: /id/
published: true
---

PGP (atau Pretty Good Privacy) serta padanan sumber terbuka lainnya, GPG (Gnu Privacy Guard), memungkinkan Anda mengenkripsi konten surel untuk melindungi pesan Anda agar tidak dilihat oleh penyedia surel Anda atau pihak lain mana pun yang mungkin memiliki akses ke surel tersebut. Namun fakta bahwa Anda telah mengirim pesan ke organisasi penerima tersebut dapat diakses oleh pemerintah atau lembaga penegak hukum. Untuk mencegahnya, Anda dapat membuat alamat surel alternatif yang tidak terkait dengan identitas Anda.

Sumber: [Dokumentasi Komunitas Saluran Bantuan Access Now: Amankan Surel (dalam bahasa Inggris)](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

[Freedom of the Press Foundation: Mengenkripsi Surel dengan Mailvelope: Panduan Pemula (dalam bahasa Inggris)](https://freedom.press/training/encrypting-email-mailvelope-guide/)

[Privacy Tools: Penyedia Surel Privat (dalam bahasa Inggris)](https://www.privacytools.io/providers/email/)
