---
layout: contact-method
title: Surel
author: mfc
language: id
summary: Metode kontak
date: 2018-09
permalink: /id/contact-methods/email.md
parent: /id/
published: true
---

Isi dari pesan Anda serta fakta bahwa Anda telah menghubungi organisasi tersebut dapat diakses oleh pemerintah atau lembaga penegak hukum.