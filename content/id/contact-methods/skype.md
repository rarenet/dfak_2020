---
layout: contact-method
title: Skype
author: mfc
language: id
summary: Metode kontak
date: 2018-09
permalink: /id/contact-methods/skype.md
parent: /id/
published: true
---

Isi pesan Anda serta fakta bahwa Anda telah menghubungi organisasi tersebut dapat diakses oleh pemerintah atau lembaga penegak hukum.