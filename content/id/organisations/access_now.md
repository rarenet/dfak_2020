---
language: id
layout: organisation
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano, Українська, тоҷикӣ
services: grants_funding, in_person_training, org_security, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, harassment, forensic, advocacy
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, global
response_time: 2 jam
contact_methods: email, pgp
email: help@accessnow.org
pgp_key: https://keys.accessnow.org/help.asc
pgp_key_fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC
initial_intake: yes
---

Saluran Bantuan Keamanan Digital Access Now bekerja dengan individu dan organisasi di seluruh dunia untuk menjaga mereka tetap aman saat daring. Jika Anda sedang berisiko, kami dapat membantu Anda meningkatkan praktik keamanan digital agar terhindar dari bahaya. Jika Anda sedang diserang, kami menyediakan bantuan darurat respons cepat.
