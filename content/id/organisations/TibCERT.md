---
language: id
layout: organisation
name: TibCERT - Tibetan Computer Emergency Readiness Team
website: https://tibcert.org/
logo: tibcert-logo.png
languages: English, Tibetan
services: in_person_training, org_security, assessment, digital_support, secure_comms, device_security, advocacy
beneficiaries: journalists, hrds, activists, cso
hours: Senin-Jumat, GMT+5.30
response_time: 24 jam ketika libur, 2 jam pada waktu kerja
contact_methods: email, pgp, phone, whatsapp, signal, mail
email: info@tibcert.org
pgp: 0xF34C6C41A569F186
pgp_key_fingerprint: D1C5 8DE6 E45B 4DD7 92EF  F970 F34C 6C41 A569 F186
mail: Dhangshar House, Temple Road, McleodGanj, Distt. Kangra, HP - 176219 - India
phone: "Mobile: +919816170738 Office: +911892292177"
whatsapp: +919816170738
signal: +919816170738
initial_intake: yes
---

Tibetan Computer Emergency Readiness Team (TibCERT) berusaha untuk menciptakan struktur formal berbasis koalisi untuk mengurangi dan memitigasi ancaman daring di komunitas Tibet, serta memperluas kapasitas penelitian teknis warga Tibet tentang ancaman terhadap diaspora dan pengawasan serta penyensoran di dalam Tibet, yang pada akhirnya memastikan kebebasan dan keamanan daring yang lebih besar bagi masyarakat Tibet secara keseluruhan.

Misi TibCERT meliputi:

- Menciptakan dan mempertahankan platform untuk kolaborasi jangka panjang antara pemangku kepentingan di komunitas Tibet tentang masalah dan kebutuhan keamanan digital,
- Memperdalam koneksi dan mengembangkan proses formal untuk kolaborasi antara warga Tibet dan peneliti *malware* dan keamanan siber global untuk memastikan berbagi yang saling menguntungkan,
- Meningkatkan sumber daya yang tersedia bagi warga Tibet untuk mempertahankan diri dan mengurangi serangan daring dengan secara teratur memublikasikan informasi dan rekomendasi tentang ancaman yang dihadapi komunitas,
- Membantu warga Tibet di Tibet dalam menghindari penyensoran dan pengawasan dengan memberikan informasi dan analisis yang teratur dan terperinci, serta solusi potensial.
