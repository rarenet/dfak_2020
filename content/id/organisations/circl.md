---
language: id
layout: organisation
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: hari kerja, UTC+2
response_time: 4 jam
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL merupakan CERT (computer emergency response team) atau tim respons cepat darurat komputer untuk sektor privat, komune, dan entitas nonpemerintah di Luxembourg.

CIRCL menyediakan titik kontak yang bisa diandalkan dan tepercaya untuk semua pengguna, perusahaan dan organisasi yang berbasis di Luxembourgh, untuk penanganan serangan dan insiden. Tim ahli mereka bertindak seperti pemadam kebakaran dengan kemampuan untuk bereaksi dengan cepat dan efisien setiap kali ancaman dicurigai, terdeteksi atau insiden terjadi.

Tujuan CIRCL adalah untuk mengumpulkan, meninjau, melaporkan dan merespons ancaman siber secara sistematis dan cepat.
