---
language: uk
layout: organisation
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship, physical_sec
beneficiaries: hrds, hros
hours: оперативна допомога 24/7, глобально; звичайна діяльність понеділок-п'ятниця в робочі години, IST (UTC+1), персонал перебуває в різних часових зонах у різних регіонах
response_time: той же або наступний день у випадку надзвичайної ситуації
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

Front Line Defenders - це міжнародна організація, штаб-квартира якої знаходиться в Ірландії. Ця організація працює над комплексним захистом правозахисників, які перебувають у групі ризику. Front Line Defenders надає оперативну та практичну підтримку правозахисникам, які безпосередньо ризикують, у вигляді грантів для підвищення безпеки, тренінгів із фізичної та цифрової безпеки, агітаційно-просвітницької роботи та проведення кампаній.

Front Line Defenders мають свою цілодобову (24/7) гарячу лінію для нагальних питань за номером +353-121-00489 для правозахисників із групи ризику арабською, англійською, французькою, російською або іспанською мовою. Коли правозахисники стикаються з безпосередньою загрозою для свого життя, представники Front Line Defenders можуть допомогти з тимчасовою релокацією. Front Line Defenders проводять тренінги з питань фізичної та цифрової безпеки. Також організація широко висвітлює історії правозахисників із групи ризику, проводить кампанії та захищає інтереси активістів на міжнародному рівні, зокрема, в ЄС і ООН, використовуючи для їхнього захисту міжрегіональні механізми та безпосередню взаємодію з урядами.
