---
language: uk
layout: organisation
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, глобально
response_time: 12 годин
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Окрім основної роботи, Internews також працює з окремими особами, організаціями та спільнотами у всьому світі для підвищення обізнаності щодо цифрової безпеки, захисту доступу до відкритого і нецензурованого інтернету та вдосконалення практик цифрової безпеки. Організація Internews провела тренінги для журналістів та правозахисників у 80+ країнах і має сильні мережі місцевих та регіональних тренерів та аудиторів, які працюють у сфері цифрової безпеки та обізнані в питаннях методології SAFETAG (Security Auditing Framework and Evaluation Template for Advocacy Groups) ([https://safetag.org](https://safetag.org)), розробку якої очолювала Internews. Організація Internews будує міцне відповідальне партнерство і з громадянським суспільством, і з приватними компаніями, що займаються збором та аналізом інформації про загрози, і може надавати своїм партнерам безпосередню підтримку безпечної та нецензурованої онлайн присутності. Internews пропонує технічні і нетехнічні рішення - від базової оцінки безпеки за допомогою методології SAFETAG до оцінки організаційної політики та перевірки стратегій пом'якшення ризиків на основі вивчення загроз. Організація надає безпосередню підтримку в питаннях аналізу фішингу та зловмисного програмного забезпечення на прохання правозахисників та медіагруп, які зазнають таргетованих цифрових атак. 
