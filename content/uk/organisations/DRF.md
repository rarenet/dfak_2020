---
language: uk
layout: organisation
name: Digital Rights Foundation
website: https://digitalrightsfoundation.pk/
logo: DRF.png
languages: English, اردو
services: grants_funding, in_person_training, org_security, web_hosting, web_protection, digital_support, relocation, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: 7 днів на тиждень, 9 - 17 UTC+5 (PST)
response_time: 56 годин
contact_methods: email, pgp, mail, phone, whatsapp
email: helpdesk@digitalrightsfoundation.pk
pgp_key_fingerprint: 4D02 8B68 866F E2AD F26F  D7CC 3283 B8DA 6782 7590
mail: 180A, Garden Block, Garden Town, Lahore, Pakistan
phone: +9280039393
whatsapp: +923323939312
---

Digital Rights Foundation (DRF) - це зареєстрована дослідницька правозахисна неурядова організація з Пакистану. DRF, заснована у 2012 році, зосереджена на тому, як інформаційно-комунікаційні технології підтримують права людей, інклюзивність, демократичні процеси та цифрове управління. DRF працює над питаннями онлайн свободи слова, приватності, захисту даних та протидії онлайн насиллю щодо жінок.