---
language: uk
layout: organisation
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: робочі години, UTC+2
response_time: 4 години
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL - це CERT для приватного сектора, общин та неурядових установ у Люксембурзі.

CIRCL - це надійний та довірений контактний центр для будь-яких користувачів, компаній та організацій в Люксембурзі, який займається атаками та інцидентами. Команда експертів центру діє як бригада пожежників: вони здатні швидко та ефективно реагувати, щойно з'являється підозра про загрозу, виявлено загрозу або стався інцидент.

Завдання CIRCL - це систематичний та оперативний збір та аналіз інформації про цифрові загрози, інформування про них та належне реагування на них.
