---
language: uk
layout: organisation
name: Myan ICT for Development Organization (MIDO)
website: https://http://www.mido.ngo/
logo: MIDO_logo_blue_fill.png
languages: ဗမာ, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, browsing, account, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: 7 днів на тиждень, 9 - 17 UTC +6:30
response_time: 1 день
contact_methods: email, phone, signal
email: help@mido.ngo
phone: +95(9)777788258 +95(9)777788246
whatsapp: +95(9)777788258
---

Місія MIDO полягає у тому, щоб бути каталізатором прийняття технологій, які є рушійною силою соціального та політичного прогресу в М'янмі.