---
language: uk
layout: organisation
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: 24/7, UTC+2
response_time: 4 години
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost пропонує ІТ-послуги, ключовими характеристиками яких є етичність та надійність. До спектру їхніх послуг належать веб-хостинг, хмарне зберігання та потужні нішові рішення у сфері інформаційної безпеки. Greenhost бере активну участь у розвитку програм із відкритим кодом та в різних проєктах у сферах технологій, журналістики, культури, освіти, сталого розвитку та свободи в інтернеті.
