---
language: uk
layout: organisation
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano, Українська, тоҷикӣ
services: grants_funding, in_person_training, org_security, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, harassment, forensic, advocacy
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, у всьому світі
response_time: 2 години
contact_methods: email, pgp
email: help@accessnow.org
pgp_key: https://keys.accessnow.org/help.asc
pgp_key_fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC
initial_intake: yes
---

Гаряча лінія з цифрової безпеки Access Now допомагає окремим особам та організаціям зі всього світу й намагається забезпечити їхню онлайн-безпеку. Якщо ви перебуваєте у зоні ризику, ми можемо допомогти вам удосконалити свої навички цифрової безпеки, щоб уникнути можливої небезпеки. Якщо на вас уже здійснюється атака, ми надамо вам негайну оперативну підтримку.
