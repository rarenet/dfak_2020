---
layout: contact-method
title: PGP
author: mfc
language: uk
summary: Способи зв'язку
date: 2020-11
permalink: /uk/contact-methods/pgp.md
parent: /uk/
published: true
---

PGP (або Pretty Good Privacy) та її еквівалент у відкритому програмному забезпеченні, GPG (Gnu Privacy Guard), дозволяє зашифрувати вміст електронних листів, щоб захистити ваше повідомлення від перегляду провайдером вашої електронної пошти чи будь-якою іншою стороною, яка може мати доступ до електронного листа. Однак факт вашого надсилання повідомлення організації-одержувачу може бути відомим для органів державної влади чи правоохоронних органів. Щоб уникнути цього, можна створити альтернативну адресу електронної пошти, не пов'язану з вашою особистістю.

Ресурси: [Access Now Helpline Community Documentation: Secure Email](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

[Freedom of the Press Foundation: Encrypting Email with Mailvelope: A Beginner's Guide](https://freedom.press/training/encrypting-email-mailvelope-guide/)

[Privacy Tools: Private Email Providers](https://www.privacytools.io/providers/email/)
