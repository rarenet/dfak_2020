---
layout: contact-method
title: WhatsApp
author: mfc
language: uk
summary: Способи зв'язку
date: 2018-09
permalink: /uk/contact-methods/whatsapp.md
parent: /uk/
published: true
---

Використання месенджера WhatsApp забезпечить захист вашої розмови з одержувачем так, що лише ви та одержувач зможете читати повідомлення, однак факт вашого спілкування з одержувачем може стати відомим органам державної влади чи правоохоронним органам.

Ресурси: [How to: Use WhatsApp on Android](https://ssd.eff.org/en/module/how-use-whatsapp-android) [How to: Use WhatsApp on iOS](https://ssd.eff.org/en/module/how-use-whatsapp-ios).
