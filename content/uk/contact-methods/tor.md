---
layout: contact-method
title: Tor
author: mfc
language: uk
summary: Способи зв'язку
date: 2018-09
permalink: /uk/contact-methods/tor.md
parent: /uk/
published: true
---

Вебпереглядач Tor - це вебпереглядач, який забезпечує приватність і дозволяє вам анонімно взаємодіяти з вебсайтами завдяки тому, що ви не ділитесь своїм місцезнаходженням (через свою IP адресу), коли заходите на вебсайт.

Ресурси: [Overview of Tor](https://www.torproject.org/about/overview.html.en).