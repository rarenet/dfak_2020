---
layout: contact-method
title: Signal
author: mfc
language: uk
summary: Способи зв'язку
date: 2018-09
permalink: /uk/contact-methods/signal.md
parent: /uk/
published: true
---

Використання месенджера Signal забезпечить шифрування змісту вашого повідомлення лише для організації-одержувача, і лише ви та ваш одержувач знатимете про факт вашого спілкування. Пам'ятайте, що Signal використовує ваш телефонний номер в якості вашого імені користувача, тому ви ділитесь своїм телефонним номером із організацією, з якою ви контактуєте.

Ресурси: [How to: Use Signal for Android](https://ssd.eff.org/en/module/how-use-signal-android), [How to: Use Signal for iOS](https://ssd.eff.org/en/module/how-use-signal-ios), [How to Use Signal Without Giving Out Your Phone Number](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)