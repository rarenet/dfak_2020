---
#layout: content-method
title: อีเมล
author: mfc
language: th
summary: วิธีการติดต่อ
date: 2021-09
permalink: /th/contact-methods/email.md
parent: /th/
published: true
---

รัฐบาลหรือหน่วยงานผู้บังคับใช้กฎหมายอาจสามารถเข้าถึงเนื้อหาในข้อความของคุณและข้อมูลว่าคุณได้ติดต่อกับองค์กรได้ 

