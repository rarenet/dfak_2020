---
layout: topic
title: "ฉันได้รับข้อความที่ไม่ชอบมาพากล"
author: Abir Ghattas, Donncha Ó Cearbhaill, Claudio Guarnieri, Michael Carbone
language: th
summary: "ฉันได้รับข้อความ ลิงก์ หรืออีเมลที่ไม่ชอบมาพากล ฉันควรรับมืออย่างไร?"
date: 2021-09
permalink: topics/suspicious-messages
parent: Home
order: 4
---

# ฉันได้รับข้อความที่ไม่ชอบมาพากล

คุณอาจได้รับข้อความที่ไม่ชอบมาพากลเข้ามาในกล่องข้อความในอีเมล บัญชีโซเชียลมีเดีย หรือแอปพลิเคชันส่งข้อความของคุณ รูปแบบของอีเมลต้องสงสัยที่พบเจอบ่อยที่สุดคือ อีเมลหลอกล่อ (Phishing emails) อีเมลหลอกล่อเหล่านี้มีเป้าหมายในการลวงเอาข้อมูลส่วนตัว ข้อมูลด้านการเงิน และข้อมูลบัญชีของคุณไป ด้วยวิธีการขอให้คุณเข้าไปเยี่ยมชมเว็บไซต์ปลอมหรือโทรหาศูนย์บริการข้อมูลลูกค้าปลอม นอกจากนี้ อีเมลหลอกล่ออาจมีสิ่งที่แนบมาซึ่งจะติดตั้งมัลแวร์ในคอมพิวเตอร์ของคุณเมื่อถูกเปิดขึ้นมาอีกด้วย

หากคุณไม่แน่ใจว่าข้อความที่ได้รับมาเป็นของจริงหรือไม่ หรือไม่แน่ใจว่าควรรับมืออย่างไร คุณสามารถทำแบบสอบถามต่อไปนี้เพื่อเป็นเครื่องมือชี้นำในการวินิจฉัยสถานการณ์ต่อไป หรือส่งต่อข้อความนั้นให้กับองค์กรภายนอกที่น่าเชื่อถือที่สามารถช่วยคุณวิเคราะห์ข้อความอย่างละเอียดยิ่งขึ้นได้
โปรดระลึกไว้ว่า การได้รับอีเมลที่ไม่ชอบมาพากลไม่ได้หมายความว่า บัญชีของคุณถูกรุกล้ำ หากคุณคิดว่าอีเมลหรือข้อความใดมีความไม่ชอบมาพากล อย่าเปิดมัน อย่าตอบอีเมลนั้น อย่าคลิ๊กลิงก์ใดๆ และอย่าดาวน์โหลดสิ่งที่แนบมาใดๆ

## Workflow

### intro

#### คุณได้ดำเนินการใดกับข้อความหรือลิงก์ที่ได้รับมาหรือไม่?

##### Options

- [ฉันคลิ๊กเข้าไปในลิงก์](#link-clicked)
- [ฉันใส่ข้อมูลที่บ่งบอกตัวตนของฉัน](#account-security_end)
- [ฉันดาวน์โหลดไฟล์](#device-security_end)
- [ฉันตอบข้อความนั้นด้วยการให้ข้อมูลส่วนตัว](#reply-personal-info)
- [ฉันไม่ได้ดำเนินการใดๆ](#do-you-know-sender)

### do-you-know-sender

#### ฉันไม่ได้ดำเนินการใดๆ

คุณรู้จักผู้ที่ส่งข้อความนี้หรือไม่? โปรดทราบว่าผู้ส่งข้อความอาจกำลัง [ถูกแอบอ้างชื่อ](https://en.wikipedia.org/wiki/Email_spoofing) โดยสวมรอยเป็นคนที่คุณเชื่อถือ

##### Options

- [ผู้ส่งเป็นบุคคลหรือองค์กรที่ฉันรู้จัก](#known-sender)
- [ผู้ส่งเป็นผู้ให้บริการ (เช่น ผู้ให้บริการอีเมล โฮสติ้ง โซเชียลมีเดีย หรือธนาคาร)](#service-provider)
- [ข้อความนี้ส่งมาจากบุคคลหรือองค์กรที่ฉันไม่รู้จัก](#share)

### known-sender

#### ผู้ส่งเป็นบุคคลหรือองค์กรที่ฉันรู้จัก

> คุณสามารถติดต่อผู้ส่งโดยใช้ช่องทางการสื่อสารอื่นๆ ได้หรือไม่? ยกตัวอย่างเช่น หากคุณได้รับอีเมล คุณสามารถตรวจสอบโดยตรงกับผู้ส่งผ่านทาง Signal, WhatsApp หรือโทรศัพท์ได้หรือไม่? ขอให้คุณใช้วิธีการติดต่อที่เคยใช้มาก่อนแล้ว คุณห้ามเชื่อเบอร์โทรศัพท์ที่ระบุไว้ในข้อความต้องสงสัย

คุณยืนยันได้ว่าผู้ส่งนั้นเป็นคนที่ส่งข้อความมาหรือไม่?

##### Options

- [ได้](#resolved_end)
- [ไม่ได้](#share)

### service-provider

#### ู้ส่งเป็นผู้ให้บริการ (เช่น ผู้ให้บริการอีเมล โฮสติ้ง โซเชียลมีเดีย หรือธนาคาร)

> ในสถานการณ์นี้ ผู้ให้บริการคือบริษัทหรือแบรนด์ใดๆ ที่ให้บริการที่คุณกำลังใช้งานหรือเป็นสมาชิกประจำ รายชื่อต่อไปนี้อาจรวมไปถึงผู้ให้บริการอีเมลของคุณ (Google, Yahoo, Microsoft, ProtonMail...), ผู้ให้บริการโซเชียลมีเดียของคุณ (Facebook, Twitter, Instagram...) หรือแพลตฟอร์มออนไลน์ที่มีข้อมูลทางการเงินของคุณ (Paypal, Amazon, ธนาคารต่างๆ, Netflix...).
>
> มีทางที่คุณสามารถตรวจสอบว่าข้อความเป็นของจริงหรือไม่? ผู้ให้บริการจำนวนมากจะให้สำเนาข้อความแจ้งเตือนหรือเอกสารอื่นๆในหน้าบัญชีของคุณ ยกตัวอย่างเช่น หากข้อความนั้นมาจาก Facebook มันควรอยู่ใน [ลิสต์อีเมลแจ้งเตือน] https://www.facebook.com/settings?tab=security&section=recent_emails) หรือหากมาจากธนาคารของคุณ คุณก็สามารถโทรไปตรวจสอบกับศูนย์บริการข้อมูลลูกค้าได้

เลือกหนึ่งในตัวเลือกด้านล่าง:

##### Options

- [ฉันสามารถพิสูจน์ได้ว่าข้อความมาจากผู้ให้บริการของฉันจริงๆ](#resolved_end)
- [ฉันไม่สามารถพิสูจน์ได้ว่าข้อความเป็นของจริง](#share)
- [ฉันไม่ได้เป็นสมาชิกประจำของบริการนี้ และ/หรือคาดคิดว่าจะได้ข้อความนี้จากเขา](#share)

### link-clicked

#### ฉันคลิ๊กเข้าไปในลิงก์

> ลิงก์ในข้อความที่ไม่ชอบมาพากลอาจนำคุณไปสู่หน้าเข้าสู่ระบบของปลอม ซึ่งจะขโมยข้อมูลที่บ่งบอกตัวตนของคุณหรือหน้าเว็บประเภทอื่นๆจะอาจขโมยข้อมูลส่วนตัวหรือข้อมูลทางการเงินของคุณ บางครั้ง ลิงก์นี้อาจขอให้คุณดาวน์โหลดสิ่งที่แนบมาเพื่อติดตั้งมัลแวร์ในคอมพิวเตอร์ของคุณเมื่อเปิดซอฟท์แวร์นั้นขึ้นมา ลิงก์นี้สามารถพาคุณไปยังเว็บไซต์ที่เตรียมไว้เป็นพิเศษเพื่อพยายามทำให้อุปกรณ์ของคุณติดมัลแวร์หรือสปายแวร์
> คุณสามารถเล่าว่าเกิดอะไรขึ้นบ้างหลังจากที่คลิ๊กเข้าไปในลิงก์ได้ไหม?

##### Options

- [มีการขอให้ฉันใส่ข้อมูลที่บ่งบอกตัวตน](#account-security_end)
- [มีการดาวน์โหลดไฟล์อัตโนมัติ](#device-security_end)
- [ไม่มีอะไรเกิดขึ้น แต่ฉันไม่แน่ใจ](#clicked-but-nothing-happened)

### clicked-but-nothing-happened

#### ไม่มีอะไรเกิดขึ้น แต่ฉันไม่แน่ใจ

> แม้ว่าคุณคลิ๊กเข้าไปในลิงก์ที่ไม่ชอบมาพากลแล้วคุณไม่ได้สังเกตเห็นพฤติกรรมผิดปกติใดๆ นั่นไม่ได้หมายความว่าไม่มีปฏิบัติการที่เป็นอันตรายเกิดขึ้นอยู่เบื้องหลัง มีความเป็นไปได้บางประการที่คุณควรพิจารณา กรณีที่น่าเป็นห่วงน้อยที่สุดคือว่า ข้อความที่คุณได้รับเป็นเพียงข้อความอันไม่พึงประสงค์ (Spam) เพื่อเป้าหมายในการโฆษณา ในกรณีนี้ จะมีโฆษณาเด้งขึ้นมา ในบางครั้ง โฆษณาเหล่านี้อาจสามารถมีอันตรายได้เช่นกัน

- [มีโฆษณาเด้งขึ้นมา ฉันไม่แน่ใจว่ามันมีอันตรายหรือไม่](#suspicious-device_end)
  > ในสถานการณ์อันเป็นไปได้ที่เลวร้ายที่สุด การคลิ๊กทำให้เกิดช่องโหว่ให้เกิดคำสั่งที่มุ่งร้ายต่อระบบของคุณ หากสิ่งนี้เกิดขึ้น มันอาจเป็นเพราะว่าบราวเซอร์ของคุณไม่ทันสมัยและมีจุดเปราะบางที่ทำให้เกิดการใช้ประโยชน์จากช่องโหว่ได้ ในกรณีที่หายาก บราวเซอร์ของคุณอาจทันสมัยแต่ยังเกิดสถานการณ์เช่นนี้ขึ้น จุดเปราะบางที่ถูกใช้ประโยชน์อาจไม่เป็นที่รู้จักโดยทั่วไป ในทั้งสองกรณี อุปกรณ์ของคุณอาจได้เริ่มทำงานอย่างไม่ชอบมาพากลแล้ว
- [ใช่ บราวเซอร์ของฉันไม่ทันสมัย และ/หรืออุปกรณ์ของฉันเริ่มทำงานอย่างไม่ชอบมาพากลหลังจากที่ฉันคลิ๊กเข้าไปในลิงก์](/th/topics/device-acting-suspiciously)
  > ในกรณีอื่นๆ การเข้าเยี่ยมชมลิงก์อาจทำให้คุณตกเป็นเหยื่อของการโจมตีแบบ [cross-site script (หรือ XSS)](https://en.wikipedia.org/wiki/Cross-site_scripting) ผลลัพธ์ของการโจมตีรูปแบบนี้คือ การขโมยคุกกี้ของคุณที่ใช้เพื่อยืนยันตัวตนคุณเมื่อเข้าชมเว็บไซต์ ดังนั้นผู้โจมตีจะสามารถเข้าสู่ระบบในเว็บไซต์ได้ด้วยชื่อผู้ใช้ของคุณ ผู้โจมตีอาจจะสามารถหรือไม่สามารถเปลี่ยนรหัสผ่านของคุณ ทั้งนี้ขึ้นอยู่กับความปลอดภัยของเว็บไซต์ กรณีนี้จะมีความร้ายแรงขึ้นหากเว็บไซต์ที่คุณเป็นผู้จัดการนั้นยังเปราะบางต่อการโจมตีแบบ XSS เพราะในกรณีเช่นนี้ ผู้โจมตีจะสามารถยืนยันตัวตนเป็นผู้จัดการเว็บไซต์ได้ หากต้องการตรวจค้นการโจมตีแบบ XXS ให้คุณตรวจสอบว่าลิงก์ที่ได้คลิ๊กเข้าไปมี [script string](https://owasp.org/www-community/attacks/xss/) หรือไม่ โดยสิ่งนี้อาจถูกใส่รหัสไว้ใน HEX หรือ Unicode

##### Options

- [มีสคริปต์อยู่ในลิงก์หรือสคริปต์นั้นถูกใส่รหัสเอาไว้บางส่วน](#cross-site-script)
- [ไม่พบสคริปต์](#suspicious-device_end)

### cross-site-script

#### เว็บไซต์ที่คุณเข้าไปเยี่ยมชมเป็นเว็บไซต์ที่คุณมีบัญชีอยู่ด้วยหรือไม่?

##### Options

- [ใช่](#account-security_end)
- [ไม่](#cross-site-script-2)

### cross-site-script-2

#### คุณถูกนำไปยังเว็บไซต์ที่คุณเป็นผู้จัดการในตอนสุดท้ายหรือไม่?

##### Options

- [ใช่](#cross-site-script-admin-compromised_end)
- [ไม่](#cross-site-script-3)

### cross-site-script-admin-compromised_end

#### ลิงก์ที่ฉันคลิกนำไปสู่เว็บไซต์ที่ฉันจัดการ

> ในกรณีนี้ ผู้โจมตีอาจมีคุกกี้ที่ใช้ได้ ซึ่งช่วยให้เขาเข้าถึงบัญชีที่คุณใช้ในฐานะผู้จัดการ สิ่งแรกที่ต้องทำคือการเข้าสู่ระบบในส่วนต่อประสานสำหรับการบริหารจัดการ และหยุดการใช้งานที่ดำเนินอยู่หรือเพียงแค่เปลี่ยนรหัสผ่าน นอกจากนี้ คุณควรตรวจสอบว่าผู้โจมตีได้อัพโหลดสิ่งใดลงไปในเว็บไซต์ของคุณ และ/หรือโพสต์เนื้อหาที่มุ่งร้ายต่อคุณหรือไม่ ถ้าเขาได้ทำสิ่งเหล่านี้ ให้คุณลบมันทิ้งออกจากระบบ
> องค์กรต่อไปนี้สามารถช่วยสืบสวนและตอบสนองต่อเหตุการณ์นี้ได้:

[orgs](:organisations?services=forensic)

### cross-site-script-3

#### ไม่ ฉันไม่ได้จัดการเว็บไซต์นี้

> น่าจะไม่เป็นอะไร อย่างไรก็ตาม ในกรณีที่หาพบยาก XXS อาจถูกใช้เพื่อนำบราวเซอร์ของคุณมากระทำการจู่โจมอื่นๆ

##### Options

- [ฉันต้องการประเมินว่าอุปกรณ์ของฉันติดมัลแวร์หรือไม่](/th/topics/device-acting-suspiciously)
- [ฉันคิดว่าฉันไม่เป็นอะไร](#final_tips)

### reply-personal-info

#### ฉันตอบข้อความนั้นด้วยการให้ข้อมูลส่วนตัว

> คุณอาจต้องดำเนินการอย่างฉับพลัน โดยขึ้นอยู่กับว่าคุณได้ให้ข้อมูลประเภทไหนไป
> คุณได้ให้ข้อมูลประเภทไหนไป?

##### Options

- [ฉันให้ข้อมูลบัญชีที่เป็นความลับ](#account-security_end)
- [ฉันให้ข้อมูลสาธารณะ](#share)
- [ฉันไม่แน่ใจว่าข้อมูลที่ให้ไปอ่อนไหวขนาดไหน และฉันต้องการความช่วยเหลือ](#help_end)

### share

#### การแบ่งปันข้อมูลของคุณหรือข้อความที่น่าสงสัย

> การแชร์ข้อความที่ไม่ชอบมาพากลสามารถช่วยปกป้องเพื่อนร่วมงานและชุมชนของคุณซึ่งอาจได้รับผลกระทบเช่นเดียวกัน นอกจากนี้ คุณอาจจะอยากลองขอความช่วยเหลือจากคนที่คุณเชื่อถือเพื่อขอคำแนะนำว่าข้อความนี้เป็นอันตรายหรือไม่ ลองพิจารณาที่จะแชร์ข้อความนี้กับองค์กรที่สามารถช่วยวิเคราะห์ให้ได้
>
> ก่อนแชร์ข้อความที่ไม่ชอบมาพากล โปรดตรวจสอบให้แน่ใจว่าได้ใส่ตัวข้อความนั้นและข้อมูลเกี่ยวกับผู้ส่งไว้แล้ว หากข้อความนั้นเป็นอีเมล โปรดตรวจสอบว่าได้แนบอีเมลฉบับเต็ม ซึ่งรวมไปถึงหัวเรื่อง โดยใช้ [คู่มือดังต่อไปนี้ที่จัดทำโดย Computer Incident Response Center Luxembourg (CIRCL)](https://www.circl.lu/pub/tr-07/)

คุณต้องการความช่วยเหลือเพิ่มเติมหรือไม่?

##### Options

- [ใช่ ฉันต้องการความช่วยเหลือเพิ่มเติม](#help_end)
- [ไม่ ฉันแก้ปัญหาของฉันสำเร็จแล้ว](#resolved_end)

### device-security_end

#### ฉันดาวน์โหลดไฟล์

> ในกรณีที่มีการดาวน์โหลดไฟล์เข้ามาในอุปกรณ์ของคุณ อุปกรณ์ของคุณอาจกำลังตกอยู่ในความเสี่ยง!
> โปรดติดต่อองค์กรด้านล่างที่สามารถช่วยเหลือคุณได้ หลังจากนั้น โปรด [ส่งข้อความไม่ชอบมาพากลที่คุณได้รับ](#share) ไปให้องค์กรนั้น

[orgs](:organisations?services=device_security)

### account-security_end

####  ันใส่ข้อมูลที่บ่งบอกตัวตนของฉัน

> หากคุณได้ให้ข้อมูลที่บ่งบอกตัวตน หรือคุณตกเป็นเหยื่อของการโจมตีแบบ cross-site script attack บัญชีของคุณอาจกำลังตกอยู่ในความเสี่ยง!
>
> หากคุณคิดว่าบัญชีของคุณถูกรุกล้ำ คุณอาจควรลองปฏิบัติตาม workflow ของคู่มือการปฐมพยาบาลทางดิจิทัลนี้เกี่ยวกับเรื่อง [บัญชีที่ถูกรุกล้ำ](/th/topics/account-access-issues).
>
> เราแนะนำว่าให้คุณแจ้งเตือนชุมชนเกี่ยวกับปฏิบัติการหลอกล่อทางออนไลน์ (Phishing) และแชร์ข้อความที่ไม่ชอบมาพากลให้กับองค์กรที่สามารถช่วยวิเคราะห์ได้
> คุณต้องการแชร์ข้อความที่คุณได้รับหรือต้องการความช่วยเหลือเพิ่มเติมก่อน?

##### Options

- [ฉันต้องการแชร์ข้อความที่ไม่ชอบมาพากล](#share)
- [ฉันต้องการความช่วยเหลือเพิ่มเติมในการทำให้บัญชีของฉันปลอดภัย](#account_end)
- [ฉันต้องการความช่วยเหลือเพิ่มเติมในการวิเคราะห์ข้อความ](#analysis_end)

### suspicious-device_end

#### ฉันไม่แน่ใจว่าเกิดอะไรขึ้น

> หากคุณคลิ๊กที่ลิงก์แล้ว และยังไม่แน่ใจว่าเกิดอะไรขึ้น อุปกรณ์ของคุณอาจติดมัลแวร์ไปแล้วโดยที่คุณไม่รู้ตัว คุณอาจควรลองพิจารณาความเป็นไปได้นี้ หรือ หากคุณคิดว่าอุปกรณ์ของคุณอาจจะติดมัลแวร์ ให้ไปที่ workflow ["อุปกรณ์ของฉันกำลังทำงานอย่างไม่ชอบมาพากล"](/th/topics/device-acting-suspiciously)

### help_end

#### ฉันต้องการความช่วยเหลือเพิ่มเติม

> คุณควรขอความช่วยเหลือจากเพื่อนร่วมงานหรือผู้อื่น เพื่อให้เข้าใจความเสี่ยงจากข้อมูลที่คุณส่งไปมากขึ้น คนอื่นๆในองค์กรหรือเครือข่ายของคุณก็อาจได้รับคำขอในลักษณะคล้ายกัน
> โปรดติดต่อองค์กรด้านล่างที่สามารถให้ความช่วยเหลือแก่คุณ หลังจากนั้น โปรด [ส่งข้อความไม่ชอบมาพากลที่คุณได้รับ](#share) ให้กับองค์กรนั้น

[orgs](:organisations?services=digital_support)

### account_end

#### ฉันต้องการความช่วยเหลือเพิ่มเติมในการรักษาความปลอดภัยบัญชีของฉัน

หากบัญชีของคุณถูกรุกล้ำ และคุณต้องการความช่วยเหลือในการทำให้มันปลอดภัย โปรดติดต่อองค์กรด้านล่างที่สามารถให้ความช่วยเหลือแก่คุณ

[orgs](:organisations?services=account)

### analysis_end

#### ฉันต้องการความช่วยเหลือเพิ่มเติมในการวิเคราะห์ข้อความ

องค์กรต่อไปนี้สามารถรับเอาข้อความที่ไม่ชอบมาพากลของคุณไปสืบสวนต่อยอดให้คุณได้

[orgs](:organisations?services=forensic&services=vulnerabilities_malware)

### resolved_end

#### ใช่ ปัญหาของฉันได้รับการแก้ไขแล้ว

เราหวังว่า แนวทางการแก้ไขปัญหานี้จะเป็นประโยชน์ โปรดให้ข้อติชมกับเรา [ผ่านทางอีเมล](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### Final Tips

กฎข้อแรกที่ต้องจำไว้: อย่าให้ข้อมูลส่วนตัวใดๆในอีเมลเด็ดขาด ไม่มีสถาบัน ธนาคาร หรือใครก็ตามที่จะขอข้อมูลเหล่านี้ผ่านทางอีเมล มันอาจไม่ใช่เรื่องง่ายเสมอไปที่จะระบุว่าอีเมลหรือเว็บไซต์เป็นของจริงหรือไม่ แต่ต่อไปนี้คือเคล็ดลับบางข้อที่สามารถช่วยคุณประเมินอีเมลที่คุณได้รับมาได้

- ความเร่งด่วน: โดยปกติ อีเมลที่ไม่ชอบมาพากลมักแจ้งเตือนเกี่ยวกับการเปลี่ยนแปลงของบัญชีอย่างกระทันหันและขอให้คุณดำเนินการโดยทันทีเพื่อยืนยันตัวตนสำหรับบัญชีของคุณ
- ในเนื้อความของอีเมล คุณอาจพบคำถามที่ขอให้คุณ “ยืนยันตัวตน” หรือ “อัพเดทบัญชีของคุณ” หรือแจ้งว่า “การไม่อัพเดทข้อมูลของคุณจะทำให้บัญชีถูกระงับชั่วคราว” ตามปกติ คุณมักสามารถทึกทักไปได้เลยว่า ไม่มีองค์กรที่น่าเชื่อถือใดๆที่คุณได้ให้ข้อมูลของคุณไปแล้วที่จะมาขอให้คุณกรอกข้อมูลซ้ำอีกครั้ง ดังนั้น อย่าตกหลุมพรางนี้
- ระวังข้อความ สิ่งที่แนบมา ลิงก์ หรือหน้าเว็บเข้าสู่ระบบอันไม่พึงประสงค์
- สังเกตดูจุดผิดพลาดในการสะกดคำหรือไวยากรณ์ให้ดี
- คลิ๊กเพื่อดูชื่อเต็มของที่อยู่ผู้ส่ง ไม่ใช่แค่ชื่อที่ใช้แสดง
- ระวังลิงก์ที่ถูกย่อให้สั้น - อาจมีลิงก์ซ้อนที่อันตรายอยู่เบื้องหลัง
- เมื่อคุณเคลื่อนเม้าส์ไปที่ลิงก์ URL ของจริงซึ่งจะเป็นหน้าเว็บที่มันจะพาคุณไปจะปรากฏ โดยเด้งขึ้นมาหรืออยู่ด้านล่างหน้าต่างบราวเซอร์ของคุณ
- หัวเรื่องอีเมล รวมไปถึงค่าของส่วนที่เขียนว่า “จาก:” อาจถูกจัดฉากขึ้นอย่างระมัดระวังเพื่อให้ดูเหมือนเป็นของจริง คุณสามารถตรวจสอบหัวเรื่องของ SPF และ DKIM ได้เพื่อให้ทราบว่า ที่อยู่ IP นั้นได้รับอนุญาต (หรือไม่ได้รับอนุญาต) ให้ส่งอีเมลในนามของโดเมนผู้ส่ง และตรวจสอบว่าหัวเรื่องหรือเนื้อหาถูกเปลี่ยนระหว่างทางหรือไม่ ในอีเมลของจริง ค่าของ [SPF](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-spf) และ [DKIM](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide#what-is-dkim) ควรจะขึ้นเป็น 'PASS' เสมอ มิฉะนั้น อีเมลนั้นถือว่าไม่น่าเชื่อถือ เหตุผลคือว่า อีเมลนั้นมีถูกปลอมแปลงขึ้น หรือในกรณีที่หายาก เซิร์ฟเวอร์ของอีเมลอาจไม่ได้รับการกำหนดค่าอย่างถูกต้อง
- [ลายเซ็นดิจิทัล](https://www.gnupg.org/gph/en/manual/x135.html) สามารถบอกเราได้ว่าอีเมลนั้นส่งมาจากผู้ส่งตัวจริงและว่าอีเมลนั้นถูกดังแปลงระหว่างทางหรือไม่ หากอีเมลนั้นมีการเซ็นชื่อลงท้าย ให้ตรวจสอบว่าลายเซ็นนั้นได้รับการรับรองว่าเป็นของแท้จริงหรือไม่ คุณจำเป็นต้องใช้ OpenPGP และกุญแจสาธารณะที่เชื่อมต่อกับ ID ในลายเซ็นในข้อความนั้นเพื่อตรวจสอบลายเซ็น โปรแกรมจัดการอีเมลสมัยใหม่ส่วนใหญ่ที่สามารถรองรับลายเซ็นดิจิทัลจะช่วยตรวจสอบให้คุณโดยอัตโนมัติและบอกคุณผ่านส่วนต่อประสานกับผู้ใช้งานว่าลายเซ็นนั้นเป็นของแท้หรือไม่
- บัญชีที่ถูกรุกล้ำสามารถปล่อยอีเมลหรือข้อความอันตราย ที่อาจสามารถหลุดรอดการตรวจสอบเงื่อนไขดังที่กล่าวไปด้านบนและดูเหมือนเป็นของจริง อย่างไรก็ตาม เนื้อหาของข้อความก็จะยังไม่ชอบมาพากล หากเนื้อความของอีเมลดูแปลกประหลาด คุณอาจควรตรวจสอบกับผู้ส่งตัวจริงผ่านช่องทางการสื่อสารอื่นก่อนจะดำเนินการใดๆ
- หนึ่งในแนวทางการปฏิบัติที่ดีคือ การอ่านและเขียนอีเมลแบบ Plain Text อีเมลแบบ HTML อาจมีการซ่อนรหัสหรือ URL ที่อันตรายเอาไว้ คุณสามารถหาวิธีการหยุดการใช้การ HTML ในโปรแกรมจัดการอีเมลต่างๆ ได้ที่ [โพสต์นี้](https://www.maketecheasier.com/read-email-in-plain-text/)
- ใช้ระบบปฏิบัติการแบบใหม่ล่าสุดในโทรศัพท์มือถือหรือคอมพิวเตอร์ของคุณ ตรวจสอบเวอร์ชั่นต่างๆสำหรับ:
  [Android](https://en.wikipedia.org/wiki/Android_version_history), [iOS](https://en.wikipedia.org/wiki/IOS_version_history), [macOS](https://en.wikipedia.org/wiki/MacOS_version_history) and [Windows](https://en.wikipedia.org/wiki/Microsoft_Windows)).
- อัพเดทระบบปฏิบัติการของคุณและแอปพลิเคชัน/โปรแกรมทั้งหมดที่คุณติดตั้ง โดยเฉพาะอันที่เป็นเครื่องมือรับข้อมูลข่าวสาร (บราวเซอร์ การส่งข้อความ หรือแอปพลิเคชัน/โปรแกรมสำหรับแชท) ให้เร็วที่สุดเท่าที่ทำได้ ลบแอปพลิเคชัน/โปรแกรมทั้งหมดที่คุณไม่จำเป็นต้องใช้
- ใช้บราวเซอร์ที่น่าไว้วางใจ (เช่น Mozilla Firefox) เพิ่มความปลอดภัยของบราวเซอร์ของคุณด้วยการตรวจสอบส่วนต่อขยาย (Extensions) หรือส่วนเสริม (Add-ons) ที่ติดตั้งอยู่ในบราวเซอร์ของคุณ เก็บไว้เพียงโปรแกรมที่คุณเชื่อ (ยกตัวอย่างเช่น HTTPS Everywhere, Privacy Badger, uBlock Origin, Facebook Container, Cookie AutoDelete, NoScript).
- บันทึกข้อมูลของคุณเพื่อสำรองไว้อย่างปลอดภัยอยู่เป็นประจำ
- ปกป้องบัญชีของคุณด้วยรหัสผ่านที่แข็งแกร่ง ระบบยืนยันตัวตนสองขั้นตอน และการตั้งค่าที่ปลอดภัย

### Resources

นี่เป็นแหล่งข้อมูลเพิ่มเติมเพื่อช่วยให้บ่งชี้ข้อความที่ไม่ชอบมาพากลและหลีกเลี่ยงการถูกหลอกล่อ

- [Citizen Lab: ชุมชนภายใต้ความเสี่ยง – ภัยทางดิจิทัลที่พุ่งเป้าไปยังภาคประชาสังคม](https://targetedthreats.net)
- [Surveillance Self-Defense: วิธีการหลีกเลี่ยงการจู่โจมด้วยวิธีการหลอกล่อทางออนไลน์ (Phishing) ](https://ssd.eff.org/en/module/how-avoid-phishing-attacks)
- [Security Without Borders: แนวทางการรับมือกับการหลอกล่อทางออนไลน์(Phishing)](https://guides.securitywithoutborders.org/guide-to-phishing/)
