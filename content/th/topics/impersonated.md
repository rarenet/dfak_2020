---
layout: topic
title: "มีคนกำลังแอบอ้างเป็นฉันในโลกออนไลน์ "
author: Floriana Pagano, Alexandra Hache
language: th
summary: "มีคนกำลังแอบอ้างเป็นฉันด้วยบัญชีโซเชียลมีเดีย, ที่อยู่อีเมล, กุญแจ PGP, เว็บปลอม, หรือแอปพลิเคชัน"
date: 2021-09
permalink: topics/impersonated
parent: Home
order: 6
---

# มีคนกำลังแอบอ้างเป็นฉันในโลกออนไลน์

การถูกแอบอ้างเป็นภัยคุกคามที่นักเคลื่อนไหว นักปกป้องสิทธิมนุษยชน องค์กรที่ไม่ใช่หน่วยงานภาครัฐ สื่ออิสระ และนักเขียนบล็อกจำนวนมากต้องเผชิญ โดยฝ่ายตรงข้ามจะสร้างโปรไฟล์ เว็บไซต์ หรืออีเมลปลอมด้วยชื่อของพวกเขา ปฏิบัติการนี้มุ่งเป้าเพื่อทำให้เสื่อมเสียชื่อเสียง เผยแพร่ข้อมูลที่ทำให้เข้าใจผิด โจมตีด้วยวิธีการวิศวกรรมทางสังคม หรือการขโมยตัวตนเพื่อสร้างเรื่องอื้อฉาว ก่อให้เกิดความหวาดระแวง และละเมิดข้อมูลส่วนตัว ซึ่งล้วนส่งผลต่อชื่อเสียงของบุคคลหรือกลุ่มที่ถูกแอบอ้าง นอกจากนี้ ในกรณีอื่นๆ ฝ่ายตรงข้ามอาจแอบอ้างเป็นใครบางคนในโลกออนไลน์ โดยมีเป้าหมายเกี่ยวกับเรื่องการเงิน เช่น การเรี่ยไรทุน ขโมยข้อมูลลับสำหรับการชำระเงิน การรับการชำระเงิน ฯลฯ เป็นต้น

ปัญหานี้เป็นเรื่องน่าหงุดหงิดที่อาจส่งผลต่อความสามารถของคุณในการสื่อสารและให้ข้อมูลได้ในหลายระดับ นอกจากนี้ ปัญหาดังกล่าวยังอาจเกิดได้จากหลายสาเหตุ โดยขึ้นอยู่กับว่าคุณถูกแอบอ้างที่ไหนและอย่างไร

สิ่งสำคัญคือว่า คุณควรทราบว่ามีวิธีการหลายแบบในการแอบอ้างตัวบุคคล (เช่น สร้างโปรไฟล์ปลอมในโซเชียลมีเดีย, ลอกเลียนแบบเว็บไซต์, ส่งอีเมลโดยปลอมแปลงชื่อผู้ส่ง, เผยแพร่ภาพหรือวิดีโอส่วนตัวโดยไม่ได้รับความยินยอม) กลยุทธ์ต่างๆในการตอบโต้อาจรวมไปถึงการส่งคำร้องขอให้ลบข้อมูล, พิสูจน์ความเป็นเจ้าของที่แท้จริง, อ้างลิขสิทธิ์ของเว็บไซต์หรือข้อมูลที่แท้จริง, หรือเตือนเครือข่ายและผู้ที่ติดต่อกันเป็นการส่วนตัวผ่านช่องทางการสื่อสารสาธารณะหรือทางลับ การวินิจฉัยปัญหาและหาทางออกที่เป็นไปได้จากการถูกแอบอ้างอาจเป็นเรื่องซับซ้อน บางครั้ง มันอาจแทบเป็นไปไม่ได้ที่จะผลักดันให้บริษัทผู้ให้บริการโฮสติ้งขนาดเล็กลบเว็บไซต์ออก และอาจจำเป็นต้องดำเนินการทางกฎหมาย แนวทางการปฏิบัติที่ดีคือการตั้งค่าเตือนและติดตามเฝ้าระวังอินเทอร์เน็ตเพื่อตรวจสอบว่าคุณหรือองค์กรของคุณกำลังถูกแอบอ้างหรือไม่

ส่วนนี้ของคู่มือการปฐมพยาบาลทางดิจิทัลจะพาคุณไปศึกษาขั้นตอนเบื้องต้นในการวินิจฉัยวิธีการที่สามารถใช้แอบอ้างและกลยุทธ์ที่สามารถใช้บรรเทาปัญหาโดยการลบบัญชี เว็บไซต์ และอีเมลที่แอบอ้างคุณหรือองค์กรของคุณ
หากคุณกำลังถูกแอบอ้าง ให้ทำแบบสอบถามนี้เพื่อบ่งชี้รูปแบบปัญหาของคุณและหาทางออกที่เป็นไปได้

## Workflow

### urgent-question

#### คุณรู้สึกหวาดกลัวเกี่ยวกับสมรรถภาพหรือสวัสดิภาพทางกายของคุณหรือไม่?

##### Options

- [ใช่](#physical-sec_end)
- [ไม่](#diagnostic-start1)

### diagnostic-start1

#### ไม่ ฉันไม่กลัวสุขภาพร่างกายของตัวเอง

การแอบอ้างนี้ส่งผลกับตัวคุณในฐานะปัจเจกบุคคล (มีคนนำชื่อ-สกุลทางกฎหมายหรือชื่อเล่นที่ผูกติดกับชื่อเสียงของคุณไปใช้) หรือในฐานะองค์กร/กลุ่ม?

##### Options

- [ในฐานะปัจเจกบุคคล](#individual)
- [ในฐานะองค์กร](#organization)

### individual

#### ในฐานะปัจเจกบุคคล

> หากคุณได้รับผลกระทบในฐานะปัจเจกบุคคล คุณอาจควรแจ้งเตือนผู้ที่คุณติดต่อด้วย โปรดดำเนินการตามขั้นตอนนี้โดยการใช้บัญชีอีเมล โปรไฟล์ หรือเว็บไซต์ที่คุณยังสามารถควบคุมการใช้งานได้อย่างเต็มที่

##### Options

- [เมื่อคุณแจ้งผู้ที่คุณติดต่อว่ากำลังถูกแอบอ้าง ให้เดินหน้าดำเนินการ ในขั้นตอนต่อไป](#diagnostic-start2)

### organization

#### ในฐานะองค์กร

> หากคุณได้รับผลกระทบในฐานะกลุ่มบุคคล ให้ลองสื่อสารทางสาธารณะ โปรดดำเนินการตามขั้นตอนนี้โดยการใช้บัญชีอีเมล โปรไฟล์ หรือเว็บไซต์ที่คุณยังสามารถควบคุมการใช้งานได้อย่างเต็มที่

##### Options

- [เมื่อคุณแจ้งชุมชนของคุณแล้วว่ากำลังถูกแอบอ้าง ให้เดินหน้าดำเนินการ ในขั้นตอนต่อไป](#diagnostic-start2)

### diagnostic-start2

#### เมื่อคุณแจ้งชุมชนของคุณแล้วว่ากำลังถูกแอบอ้าง ให้เดินหน้าดำเนินการ ในขั้นตอนต่อไป

คุณถูกแอบอ้างอย่างไร?

##### Options

- [มีเว็บไซต์ปลอมกำลังแอบอ้างเป็นฉันหรือกลุ่มของฉัน](#fake-website)
- [ผ่านบัญชีโซเชียลมีเดีย](#social-network)
- [ผ่านการแชร์วิดีโอหรือภาพโดยไม่ได้รับความยินยอม](#doxing)
- [ผ่านอีเมลของฉันหรืออีเมลที่คล้ายกัน](#spoofed-email1)
- [ผ่านกุญแจ PGP ที่เชื่อมต่อกับอีเมลของฉัน](#PGP)
- [ผ่านแอปพลิเคชันปลอมที่เลียนแบบแอปพลิเคชันของฉัน](#app1)

### social-network

#### ผ่านบัญชีโซเชียลมีเดีย

> คุณสามารถร้องเรียนบัญชีหรือเนื้อหาที่แอบอ้างไปยังแพลตฟอร์มที่เกี่ยวข้องได้
>
> **_หมายเหตุ:_** _ควร [บันทึก](/th/documentation) ก่อนดำเนินการต่างๆ เช่น ลบข้อความหรือลบบันทึกการสนทนา หรือบล็อกโปรไฟล์ หากคุณกำลังพิจารณาการดำเนินการทางกฎหมาย คุณควรศึกษาข้อมูลเกี่ยวกับ[การจัดทำเอกสารทางกฎหมาย](/th/documentation#legal)_

คุณถูกแอบอ้างในแพลตฟอร์มโซเชียลมีเดียใด?

##### Options

- [Facebook](#facebook)
- [Instagram](#instagram)
- [TikTok](#tiktok)
- [Twitch](#twitch)
- [Twitter](#twitter)
- [YouTube](#youtube)

### facebook

#### Facebook

> โปรดดำเนินการตามวิธีการที่หน้าเว็บ ["ฉันจะรายงานบัญชีหรือเพจที่แอบอ้างว่าเป็นฉันหรือเป็นผู้อื่นได้อย่างไร?"](https://www.facebook.com/help/174210519303259) เพื่อยื่นคำร้องให้ลบบัญชีที่กระทำการแอบอ้าง
>
> โปรดทราบว่า การตอบรับคำขอของคุณอาจใช้เวลาสักพักหนึ่ง บันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน
> ได้ผลหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#account_end)

### twitter

#### Twitter

> โปรดดำเนินการตามวิธีการที่หน้า ["รายงานบัญชีผู้ใช้เกี่ยวกับเรื่องการแอบอ้าง”](https://help.twitter.com/forms/impersonation) เพื่อยื่นคำร้องให้ลบบัญชีที่กระทำการแอบอ้าง
>
> โปรดทราบว่า การตอบรับคำขอของคุณอาจใช้เวลาสักพักหนึ่ง บันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน

ได้ผลหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#account_end)

### instagram

#### Instagram

> โปรดดำเนินการตามวิธีการที่หน้า ["บัญชีที่กระทำการแอบอ้าง"](https://help.instagram.com/446663175382270) เพื่อยื่นคำร้องให้ลบบัญชีที่กระทำการแอบอ้าง
>
> โปรดทราบว่า การตอบรับคำขอของคุณอาจใช้เวลาสักพักหนึ่ง บันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน

ได้ผลหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#account_end)

### tiktok

#### TikTok

> ทำตามคำแนะนำใน ["ร้องเรียนบัญชีที่กระทำการแอบอ้าง"](https://support.tiktok.com/en/safety-hc/report-a-problem/report-an-impersonation-account) เพื่อขอให้ลบบัญชีที่กระทำการแอบอ้าง
>
> โปรดทราบว่าการตอบรับคำขอของคุณอาจใช้เวลาสักพักหนึ่ง บันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน

ได้ผลหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#account_end)

### youtube

#### YouTube

> โปรดทำตามคำแนะนำใน ["ร้องเรียนวิดีโอ ช่อง และเนื้อหาอื่นๆ ที่ไม่เหมาะสมบน YouTube"](https://support.google.com/youtube/answer/2802027) เพื่อร้องเรียนบัญชีที่กระทำการแอบอ้าง ให้เลือกเหตุผลในการร้องเรียนเป็น "การแอบอ้างเป็นบุคคลอื่น"
>
> โปรดทราบว่าการตอบรับคำขอของคุณอาจใช้เวลาสักพักหนึ่ง บันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน

ได้ผลหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#account_end)

### twitch

#### Twitch

> โปรดทำตามคำแนะนำใน ["วิธียื่นร้องเรียนผู้ใช้"](https://help.twitch.tv/s/article/how-to-file-a-user-report) เพื่อร้องเรียนให้ลบบัญชีที่กระทำการแอบอ้าง ให้เลือกหมวดหมู่ในแบบฟอร์มเป็น "การแอบอ้างเป็นบุคคลอื่น"
>
> โปรดทราบว่าการตอบรับคำขอของคุณอาจใช้เวลาสักพักหนึ่ง บันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน

ได้ผลหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#account_end)

### fake-website

#### เว็บไซต์ปลอมกำลังแอบอ้างเป็นฉันหรือกลุ่มของฉัน

> ตรวจสอบว่าเว็บไซต์นี้ขึ้นชื่อว่าอันตรายหรือไม่ โดยการตรวจ URL ผ่านบริการทางออนไลน์ดังต่อไปนี้:
>
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)

โดเมนนี้ขึ้นชื่อว่าเป็นอันตรายหรือไม่?

##### Options

- [ใช่](#malicious-website)
- [ไม่](#non-malicious-website)

### malicious-website

#### เป็นที่รู้กันว่าโดเมนนี้เป็นอันตราย

> ร้องเรียน URL นี้กับบริการ Safe Browsing ของ Google โดยกรอก [แบบฟอร์ม "ร้องเรียนมัลแวร์"](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> โปรดทราบว่า การตรวจสอบว่าความสำเร็จของการร้องเรียนของคุณอาจต้องใช้เวลาสักพักหนึ่ง ระหว่างนี้ คุณสามารถดำเนินขั้นตอนถัดไปโดยการส่งคำขอให้ผู้ให้บริการโฮสติ้งและผู้ขึ้นทะเบียนโดเมนลบเว็บไซต์ออกจากระบบ หรือบันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน

ได้ผลหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#non-malicious-website)

### non-malicious-website

#### โดเมนไม่เป็นที่รู้จักว่าเป็นอันตราย

> คุณสามารถลองร้องเรียนเกี่ยวกับเว็บไซต์ไปยังผู้ให้บริการโฮสติ้งและผู้รับจดทะเบียนโดเมน โดยขอให้ลบเว็บไซต์ออกจากระบบ
>
> **_หมายเหตุ:_** _ควร [บันทึก](/th/documentation) ก่อนดำเนินการต่างๆ เช่น ลบข้อความหรือลบบันทึกการสนทนา หรือบล็อกโปรไฟล์ หากคุณกำลังพิจารณาการดำเนินการทางกฎหมาย คุณควรศึกษาข้อมูลเกี่ยวกับ[การจัดทำเอกสารทางกฎหมาย](/th/documentation#legal)_
>
> หากเว็บไซต์ที่คุณต้องการร้องเรียนกำลังใช้เนื้อหาของคุณอยู่ คุณจำเป็นต้องพิสูจน์ว่า คุณเป็นเจ้าของเนื้อหาที่แท้จริงตั้งแต่ต้น คุณสามารถพิสูจน์สิ่งนี้ได้โดยการแสดงสัญญาตั้งต้นให้ผู้รับจดทะเบียนโดเมนและ/หรือผู้ให้บริการโฮสติ้งดู อีกทั้ง คุณยังสามารถค้นหาทั้ง URL ของเว็บไซต์ของคุณและเว็บไซต์ปลอมได้ผ่าน [Wayback Machine](https://archive.org/web/) เช่นกัน หากเว็บไซต์ได้รับการขึ้นทะเบียนในบัญชีดัชนีในเว็บนี้ คุณจะสามารถหาประวัติที่สามารถแสดงให้เห็นได้ว่าเว็บไซต์ของคุณมีตัวตนอยู่ก่อนที่เว็บไซต์ปลอมจะได้รับการเผยแพร่
>
> หากต้องการส่งคำขอให้ลบเนื้อหา คุณจะต้องรวบรวมข้อมูลเกี่ยวกับเว็บไซต์ปลอม:
>
> - ไปยัง [บริการ NSLookup ของ Network Tools](https://network-tools.com/nslookup/) และค้นหาที่อยู่ IP (ซึ่งอาจมีมากกว่าหนึ่งที่อยู่) ของเว็บไซต์ปลอมด้วยการใส่ URL เข้าไปในแบบฟอร์มสำหรับการสืบค้น
> - จดที่อยู่ IP นั้นไว้.
> - ไปยัง [บริการ Whois Lookup ของเว็บ Domain Tools](https://whois.domaintools.com/) และสืบค้นทั้งโดเมนและที่อยู่ IP ของเว็บไซต์ปลอม
> - บันทึกชื่อและอีเมลสำหรับร้องเรียนการล่วงละเมิดของผู้ให้บริการโฮสติ้งและโดเมน นอกจากนี้ ให้บันทึกชื่อของเจ้าของเว็บไซต์ หากพบข้อมูลดังกล่าวในผลลัพธ์จากการสืบค้น
> - เขียนข้อความเพื่อติดต่อไปยังผู้ให้บริการโฮสติ้งและผู้รับลงทะเบียนโดเมนเว็บไซต์ปลอมเพื่อขอให้ลบออก ข้อความของคุณควรระบุข้อมูลที่อยู่ IP, URL, และเจ้าของเว็บไซต์ที่แอบอ้าง รวมถึงชี้แจงเหตุผลว่าทำไมการกระทำนี้ถึงเป็นการล่วงละเมิด
> - คุณสามารถใช้ [แบบฟอร์มในการร้องเรียนเว็บไซต์ลอกเลียนแบบกับผู้ให้บริการโฮสติ้งของ Access Now Helpline](https://communitydocs.accessnow.org/352-Report_Fake_Domain_Hosting_Provider.html) สำหรับการเขียนข้อความเพื่อติดต่อผู้ให้บริการโฮสติ้ง
> - คุณสามารถใช้ [แบบฟอร์มในการร้องเรียนเหตุแอบอ้างหรือการลอกเลียนแบบกับผู้รับจดทะเบียนโดเมนของ Access Now Helpline](https://communitydocs.accessnow.org/343-Report_Domain_Impersonation_Cloning.html) เพื่อเขียนข้อความติดต่อผู้รับจดทะเบียนโดเมน
>
> โปรดทราบว่า การตอบรับคำขอของคุณอาจใช้เวลาสักพักหนึ่ง บันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน

ได้ผลหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#web-protection_end)

### spoofed-email1

#### ผ่านที่อยู่อีเมลของฉันหรือที่อยู่ที่คล้ายกัน

> เนื่องด้วยเหตุผลพื้นฐานทางเทคนิค การพิสูจน์ว่าอีเมลเป็นของจริงถือเป็นเรื่องที่ค่อนข้างยุ่งยาก ดังนั้น การปลอมแปลงที่อยู่ของผู้ส่งและส่งอีเมลเพื่อแอบอ้างจึงสามารถเกิดขึ้นอย่างง่ายดาย

คุณกำลังถูกแอบอ้างด้วยที่อยู่อีเมลของคุณ หรือที่อยู่อีเมลที่คล้ายกัน เช่น มีชื่อผู้ใช้เดียวกันแต่คนละชื่อโดเมน หรือไม่?

##### Options

- [ฉันกำลังถูกแอบอ้างด้วยที่อยู่อีเมลของฉัน](#spoofed-email2)
- [ฉันกำลังถูกแอบอ้างด้วยที่อยู่อีเมลที่คล้ายกัน](#similar-email)

### spoofed-email2

#### ฉันถูกแอบอ้างผ่านที่อยู่อีเมลของฉัน

> ผู้ที่กำลังแอบอ้างเป็นคุณอาจได้แฮ็กเข้ามาในบัญชีอีเมลของคุณแล้ว ลองเปลี่ยนรหัสผ่านเพื่อตรวจสอบความเป็นไปได้นี้
> คุณเปลี่ยนรหัสผ่านได้หรือไม่

##### Options

- [ได้](#spoofed-email3)
- [ไม่ได้](#hacked-account)

### hacked-account

#### ฉันไม่สามารถเปลี่ยนรหัสผ่านของฉันได้

> หากคุณไม่สามารถเปลี่ยนรหัสผ่านได้ แสดงว่าบัญชีอีเมลของคุณอาจถูกแฮ็ก คุณสามารถดำเนินการตาม [Workflow "ฉันไม่สามารถเข้าถึงบัญชีของฉันได้ "](/th/topics/account-access-issues) เพื่อแก้ไขปัญหานี้

กระบานการนี้ช่วยแก้ปัญหาของคุณหรือไม่?

##### Options

- [ได้](#resolved_end)
- [ไม่ได้](#account_end)

### spoofed-email3

#### ฉันสามารถเปลี่ยนรหัสผ่านของฉันได้

> การแอบอ้างตัวตนทางอีเมลเกิดขึ้นผ่านการส่งข้อความทางอีเมลด้วยที่อยู่ของผู้ส่งที่ถูกปลอมแปลงขึ้นมา ข้อความนั้นเขียนขึ้นโดยบางคนหรือบางแหล่งที่มิใช่ตัวจริง
>
> การแอบอ้างตัวตนผ่านอีเมลถือเป็นวิธีการที่พบเจอได้ทั่วไปในขบวนการหลอกล่อทางออนไลน์ (Phishing) และการส่งข้อความที่ผู้รับมิได้ร้องขอ (spam) เนื่องจากคนทั่วไปมักมีแนวโน้มที่จะเปิดอ่านอีเมล หากพวกเขาคิดว่าอีเมลนั้นมาจากแหล่งที่น่าเชื่อถือ
>
> หากใครบางคนกำลังแอบอ้างตัวตนด้วยอีเมลของคุณ คุณควรแจ้งผู้ที่ติดต่อกับคุณเพื่อเตือนเกี่ยวกับอันตรายของการหลอกล่อทางออนไลน์ (เตือนจากบัญชีอีเมล โปรไฟล์ หรือเว็บไซต์ที่คุณยังสามารถควบคุมได้เต็มที่)
>
> หากคุณคิดว่าการแอบอ้างมีจุดหมายในการหลอกล่อหรือเจตนาอันตรายอื่นๆ คุณอาจควรอ่านเรื่อง[ฉันได้รับข้อความที่ไม่ชอบมาพากล](/th/topics/suspicious_messages) ของคู่มือนี้

อีเมลเหล่านี้หยุดถูกส่งมาหลังจากที่คุณได้เปลี่ยนรหัสผ่านบัญชีอีเมลของคุณหรือไม่?

##### Options

- [ใช่](#compromised-account)
- [ไม่](#secure-comms_end)

### compromised-account

#### ข้อความอีเมลหยุดหลังจากฉันเปลี่ยนรหัสผ่าน

> มีความเป็นไปได้ว่าบัญชีของคุณอาจถูกแฮ็กโดยใครบางคน ซึ่งเป็นคนที่ใช้บัญชีส่งอีเมลออกไปโดยแอบอ้างเป็นตัวคุณ เนื่องจากบัญชีของคุณถูกรุกล้ำ คุณอาจควรอ่านเรื่อง [ฉันไม่สามารถเข้าถึงบัญชีของฉันได้](/th/topics/account-access-issues/) ในคู่มือนี้

วิธีนี้ช่วยแก้ไขปัญหาของคุณได้สำเร็จหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#account_end)

### similar-email

#### ฉันถูกปลอมตัวผ่านทางที่อยู่อีเมลที่คล้ายกัน

> หากผู้แอบอ้างใช้ที่อยู่อีเมลที่คล้ายคลึงกับของคุณ แต่มีโดเมนหรือชื่อผู้ใช้ที่แตกต่าง คุณอาจควรเตือนผู้ที่ติดต่อกับคุณโดยแจ้งว่ามีพยายามในการแอบอ้างตัวตนของคุณ (เตือนจากบัญชีอีเมล โปรไฟล์ หรือเว็บไซต์ที่คุณยังสามารถควบคุมได้เต็มที่)
>
> คุณอาจควรอ่านเรื่อง [ฉันได้รับข้อความที่ไม่ชอบมาพากล](/th/topics/suspicious-messages) ในคู่มือฉบับนี้ เนื่องจากการแอบอ้างอาจมีเป้าหมายในการหลอกล่อ

วิธีนี้ช่วยแก้ไขปัญหาของคุณได้สำเร็จหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#secure-comms_end)

### PGP

#### ผ่านคีย์ PGP ที่เชื่อมต่อกับที่อยู่อีเมลของฉัน

คุณคิดว่า กุญแจส่วนตัว (private key) สำหรับ PGP ของคุณถูกรุกล้ำ เช่น คุณไม่สามารถควบคุมอุปกรณ์ในสถานที่ที่มันถูกเก็บไว้ได้?

##### Options

- [ใช่](#PGP-compromised)
- [ไม่](#PGP-spoofed)

### PGP-compromised

#### คีย์ PGP ของฉันอาจถูกบุกรุก

> - สร้างคู่กุญแจ (key pair) ใหม่ซึ่งเซ็น (sign) โดยคนที่คุณไว้วางใจ
> - แจ้งผู้ที่ติดต่อกับคุณผ่านช่องทางที่เชื่อถือได้และควบคุมโดยคุณ (เช่น Signal หรือ [เครื่องมือเข้ารหัสแบบครบวงจร](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools)) ว่าควรใช้คีย์ใหม่ของคุณและหยุดใช้คีย์เก่า โดยบอกพวกเขาว่าพวกเขาจะรู้ว่าเป็นรหัสจริงของคุณได้จากลายนิ้วมือของรหัสจริงของคุณ นอกจากนี้ คุณยังสามารถส่งรหัสสาธารณะใหม่ของคุณโดยตรงผ่านช่องทางที่เชื่อถือได้เช่นเดียวกับการแจ้งให้ทราบเรื่องคีย์ใหม่ของคุณ

คุณต้องการความช่วยเหลือเพิ่มเติมในการแก้ปัญหาหรือไม่?

##### Options

- [ต้องการ](#secure-comms_end)
- [ไม่ต้องการ](#resolved_end)

### access-to-PGP

#### การเข้าถึงพีจีพี

> - เพิกถอนกุญแจของคุณ
> - [วิธีการสำหรับ Enigmail](https://www.enigmail.net/index.php/en/user-manual/key-management#Revoking_your_key_pair)
> - สร้างคู่กุญแจ (key pair) ใหม่ และให้คนที่คุณเชื่อใจเป็นคนเซ็นรับรอง
> - สื่อสารผ่านช่องทางที่น่าเชื่อถือและคุณสามารถควบคุมได้ เช่น Signal หรือ [เครื่องมืออื่นที่ได้รับการเข้ารหัสตั้งแต่ต้นทางถึงปลายทาง](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools), แจ้งผู้ที่ติดต่อกับคุณว่าคุณได้เพิกถอนกุญแจและสร้างอันใหม่ขึ้นมาแล้ว

คุณต้องการความช่วยเหลือเพิ่มเติมในการแก้ไขปัญหาของคุณหรือไม่?

##### Options

- [ใช่](#secure-comms_end)
- [ไม่](#resolved_end)

### lost-PGP

#### ฉันทำคีย์ PGP หาย

คุณมีใบรับรองการเพิกถอนหรือไม่?

##### Options

- [มี](#access-to-PGP)
- [ไม่มี](#no-revocation-cert)

### no-revocation-cert

#### ฉันไม่มีใบรับรองการเพิกถอน

> - สร้างคู่กุญแจใหม่และให้คนที่คุณเชื่อใจเป็นผู้เซ็นรับรอง
>   -สื่อสารผ่านช่องทางที่น่าเชื่อถือและคุณสามารถควบคุมได้ เช่น Signal หรือ [เครื่องมืออื่นที่ได้รับการเข้ารหัสตั้งแต่ต้นทางถึงปลายทาง](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools) ว่าพวกเขาควรใช้กุญแจใหม่ของคุณและหยุดใช้อันเก่า

คุณต้องการความช่วยเหลือเพิ่มเติมในการแก้ไขปัญหาของคุณหรือไม่?

##### Options

- [ใช่](#secure-comms_end)
- [ไม่](#resolved_end)

### PGP-spoofed

#### คีย์ PGP ของฉันอาจถูกปลอมแปลง

> - แจ้งผู้ที่ติดต่อกับคุณผ่านช่องทางที่เชื่อถือได้และควบคุมโดยคุณ เช่น Signal หรือ[เครื่องมือเข้ารหัสแบบครบวงจร](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools) ว่ามีคนกำลังพยายามแอบอ้างเป็นคุณ โดยบอกพวกเขาว่าพวกเขาจะรู้ว่าเป็นรหัสจริงของคุณได้จากลายนิ้วมือของรหัสจริงของคุณ นอกจากนี้ คุณยังสามารถส่งรหัสสาธารณะของคุณโดยตรงผ่านช่องทางที่เชื่อถือได้เช่นเดียวกับการแจ้งให้ทราบเรื่องคีย์ใหม่ของคุณ

คุณต้องการความช่วยเหลือเพิ่มเติมในการแก้ปัญหาหรือไม่?

##### Options

- [ต้องการ](#secure-comms_end)
- [ไม่ต้องการ](#resolved_end)

### doxing

#### ผ่านการแชร์วิดีโอหรือรูปภาพโดยไม่ได้รับความยินยอม

> หากมีคนเผยแพร่ข้อมูลส่วนบุคคลหรือวิดีโอหรือรูปภาพส่วนตัวของคุณ เราขอแนะนำให้คุณปฏิบัติตาม workflow ชุดปฐมพยาบาลดิจิทัลเกี่ยวกับ [Doxing และการเผยแพร่สื่อส่วนตัวโดยไม่ได้รับความยินยอม](/th/topics/doxing)

คุณต้องการจะทำอะไร?

##### Options

- [พาฉันไปที่ชุดปฐมพยาบาลดิจิทัล ส่วน Doxing และการเผยแพร่สื่อส่วนตัวโดยไม่ได้รับความยินยอม](/th/topics/doxing)
- [ฉันต้องการความช่วยเหลือเพื่อแก้ไขปัญหาของฉัน](#harassment_end)

### app1

#### ผ่านแอปปลอมที่เลียนแบบแอปของฉัน

> หากมีบุคคลกำลังกระจายแอปพลิเคชันที่ลอกเลียนแบบแอปพลิเคชันของคุณหรือซอฟท์แวร์อื่นๆ คุณควรสื่อสารทางสาธารณะเพื่อแจ้งเตือนผู้ใช้ให้ดาวน์โหลดเวอร์ชั่นของแท้เท่านั้น
>
> นอกจากนี้ คุณควรร้องเรียนแอปพลิเคชันที่ลอกเลียนแบบโดยมีจุดประสงค์มุ่งร้ายนี้ และยื่นคำขอให้ลบออกจากระบบด้วย

แอปพลิเคชันที่ลอกเลียบแบบแอปพลิเคชันของคุณโดยมีจุดประสงค์มุ่งร้ายได้รับการเผยแพร่ผ่านช่องทางใด?

##### Options

- [ใน Github](#github)
- [ใน Gitlab.com](#gitlab)
- [ใน Google Play Store](#playstore)
- [ใน Apple App Store](#apple-store)
- [ในเว็บไซต์อื่น](#fake-website)

### github

#### Github

> หากมัลแวร์อยู่ใน Github, ให้คุณอ่าน [คู่มือการส่งคำร้องให้ลบออกจากระบบตามรัฐบัญญัติลิขสิทธิ์แห่งสหัสวรรษดิจิทัล (DMCA)](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) ของ Github เพื่อลบเนื้อหาที่ละเมิดลิขสิทธิ์
>
> การตอบรับคำขอของคุณอาจใช้เวลาสักพักหนึ่ง บันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน

วิธีนี้ช่วยแก้ไขปัญหาของคุณได้สำเร็จหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#app_end)

### gitlab

#### GitLab

> หากมัลแวร์อยู่ใน Gitlab.com ให้คุณอ่าน [ข้อบังคับสำหรับคำร้องขอให้ลบเนื้อหาตามรัฐบัญญัติลิขสิทธิ์แห่งสหัสวรรษดิจิทัล (DMCA)](https://about.gitlab.com/handbook/dmca/) ของ Gitlab เพื่อลบเนื้อหาที่ละเมิดลิขสิทธิ์
>
> การตอบรับคำขอของคุณอาจใช้เวลาสักพักหนึ่ง บันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน

วิธีนี้ช่วยแก้ไขปัญหาของคุณได้สำเร็จหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#app_end)

### playstore

#### PlayStore

> หากแอปพลิเคชันที่มุ่งร้ายต่อตัวคุณอยู่ใน Google Play Store ให้คุณดำเนินการตามวิธีในหน้า ["การลบเนื้อหาออกจาก Google"](https://support.google.com/legal/troubleshooter/1114905) เพื่อลบเนื้อหาที่ละเมิดลิขสิทธิ์
>
> การตอบรับคำขอของคุณอาจใช้เวลาสักพักหนึ่ง บันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน

วิธีนี้ช่วยแก้ไขปัญหาของคุณได้สำเร็จหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#app_end)

### apple-store

#### Apple Store

> หากแอปพลิเคชันที่มุ่งร้ายต่อตัวคุณอยู่ใน App Store ให้คุณกรอก [แบบฟอร์ม "ข้อพิพาทเรื่องเนื้อหาใน App Store ของ Apple"](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts?lang=en) เพื่อลบเนื้อหาที่ละเมิดลิขสิทธิ์
>
> การตอบรับคำขอของคุณอาจใช้เวลาสักพักหนึ่ง บันทึกหน้านี้ไว้ในที่คั่นหน้าและกลับมาสู่ Workflow นี้หลังจากผ่านไปแล้วหลายวัน

วิธีนี้ช่วยแก้ไขปัญหาของคุณได้สำเร็จหรือไม่?

##### Options

- [ได้ผล](#resolved_end)
- [ไม่ได้ผล](#app_end)

### physical-sec_end

#### ฉันกลัวความเป็นอยู่ที่ดีทางร่างกายของฉัน

> หากคุณกำลังรู้สึกหวาดกลัวเกี่ยวกับสวัสดิภาพทางกายของคุณ โปรดติดต่อองค์กรด้านล่างซึ่งสามารถให้ความช่วยเหลือคุณได้
> [orgs](:organisations?services=physical_sec)

### harassment_end

#### ฉันต้องการความช่วยเหลือในการแก้ไขปัญหาของฉัน

> หากมีการเผยแพร่ข้อมูลหรือสื่อส่วนตัวของคุณโดยไม่ได้รับความยินยอมจากคุณ และคุณต้องการการสนับสนุนเพื่อแก้ไขปัญหา คุณสามารถติดต่อกับองค์กรที่สามารถสนับสนุนคุณได้ตามข้อมูลด้านล่าง
>
> ก่อนที่คุณจะติดต่อองค์กร เราขอแนะนำอย่างยิ่งให้คุณปฏิบัติตามชุดปฐมพยาบาลดิจิทัล ในส่วน [เครื่องมือแก้ปัญหาเกี่ยวกับการเผยแพร่สื่อส่วนตัวโดยไม่ได้รับอนุญาต](/th/topics/doxing) เนื่องจากจะช่วยให้คุณเข้าใจในปัญหาที่คุณกำลังเผชิญ

[orgs](:organisations?services=harassment)

### account_end

#### ฉันยังคงประสบปัญหาเกี่ยวกับการแอบอ้างบุคคลอื่นในบัญชีของฉัน

> หากคุณยังต้องเผชิญการแอบอ้างตัวตนหรือบัญชีของคุณยังคงถูกรุกล้ำ โปรดติดต่อองค์กรด้านล่างซึ่งสามารถให้ความช่วยเหลือคุณได้
> [orgs](:organisations?services=account&services=legal)

### app_end

#### ฉันต้องการความช่วยเหลือเพิ่มเติมเกี่ยวกับแอปปลอม

> หากแอปพลิเคชันปลอมยังไม่ถูกลบออก โปรดติดต่อองค์กรด้านล่างซึ่งสามารถให้ความช่วยเหลือคุณได้
> [orgs](:organisations?services=account&services=legal)

### web-protection_end

#### ฉันต้องการความช่วยเหลือเพิ่มเติมเกี่ยวกับคำขอให้ลบออก

> หากคำร้องของคุณไม่ประสบความสำเร็จในการขอให้ลบเนื้อหาออก คุณสามารถติดต่อองค์กรต่างๆด้านล่างเพื่อขอความช่วยเหลือเพิ่มเติม
> [orgs](:organisations?services=web_protection)

### secure-comms_end

#### ฉันต้องการการสนับสนุนเพิ่มเติมด้วยการสื่อสารที่ปลอดภัย

> หากคุณต้องการความช่วยเหลือหรือข้อแนะนำเกี่ยวกับเรื่องการหลอกล่อทางออนไลน์ ความปลอดภัยและการเข้ารหัสอีเมล และภาพรวมการสื่อสารเรื่องความปลอดภัย คุณสามารถติดต่อองค์กรเหล่านี้:
> [orgs](:organisations?services=secure_comms)

### resolved_end

#### ปัญหาของฉันได้รับการแก้ไขแล้ว

เราหวังว่า แนวทางการแก้ไขปัญหานี้จะเป็นประโยชน์ โปรดให้ข้อติชมกับเรา [ผ่านทางอีเมล](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com) โปรดอ่านเคล็ดลับด้านล่างเพื่อป้องกันความพยายามใดๆในการแอบอ้างตัวคุณในอนาคต

### Final Tips

- สร้างรหัสผ่านที่แข็งแกร่ง ซับซ้อน และมีเอกลักษณ์สำหรับบัญชีทั้งหมดของคุณ
- พิจารณาใช้เครื่องมือจัดการรหัสผ่านสำหรับการสร้างและเก็บรหัสผ่าน เพื่อให้คุณสามารถใช้รหัสผ่านที่หลากหลายในแต่ละเว็บไซต์และบริการโดยที่ไม่จำเป็นต้องจดจำเองทั้งหมด
- เปิดใช้ระบบการยืนยันตัวตนสองขั้นตอน (2FA) ในบัญชีต่างๆที่มีความสำคัญมากที่สุดสำหรับคุณ ระบบ 2FA สามารถช่วยเพิ่มความปลอดภัยให้แก่บัญชีด้วยการบังคับให้ใช้วิธีการมากกว่าหนึ่งวิธีในการเข้าสู่ระบบ นี่หมายความว่า หากใครสามารถเข้าถึงรหัสผ่านหลักของคุณ เขาก็มิอาจเข้าถึงบัญชีของคุณได้ถ้าเขาไม่ได้มีโทรศัพท์มือถือของคุณหรือวิธีการลำดับรองที่ใช้ในการยืนยันตัวตน
- ยืนยันตัวตนของคุณในโปรไฟล์โซเชียลมีเดีย แพลตฟอร์มบางแห่งมีฟีเจอร์ให้คุณสามารถยืนยันตัวตนและช่วยเชื่อมต่อเข้ากับบัญชีของคุณ
- ตั้งค่าเตือนใน Google คุณสามารถได้รับอีเมลแจ้งเตือนเวลามีผลลัพธ์การสืบค้นใหม่จากการค้นข้อมูลทาง Google ยกตัวอย่างเช่น คุณสามารถได้รับข้อมูลเมื่อมีการกล่าวถึงชื่อคุณ หรือชื่อองค์กร/กลุ่มของคุณ
- บันทึกหน้าเว็บของคุณดังที่ปรากฏในปัจจุบันไว้เพื่อเป็นหลักฐานในอนาคต หากเว็บไซต์ของคุณอนุญาตให้ใช้ Crawler คุณสามารถใช้ Wayback Machine ซึ่งเป็นของเว็บ archive.org เข้าไปที่ [Internet Archive Wayback Machine](https://archive.org/web/) ใส่ชื่อเว็บไซต์ในช่องด้านล่างหัวเรื่อง "บันทึกหน้าเว็บตอนนี้" และคลิ๊กที่ปุ่ม "บันทึกหน้าเว็บตอนนี้

### Resources

- [Security Self-Defense: สร้างรหัสผ่านที่แข็งแกร่งและไม่เหมือนใคร](https://ssd.eff.org/en/module/creating-strong-passwords)
- [Security Self-Defense: ข้อมูลเบื้องต้นเกี่ยวกับการใช้เครื่องมือจัดการรหัสผ่านในรูปแบบอะนิเมชัน](https://ssd.eff.org/en/module/animated-overview-using-password-managers-stay-safe-online)
- [Surveillance Self-Defense: วิธีการยืนยันตัวตนแบบ 2 ขั้นตอน (2FA)](https://ssd.eff.org/module/how-enable-two-factor-authentication)
- [Archive.org: บันทึกเว็บไซต์ของคุณเข้าแฟ้มข้อมูล](https://archive.org/web/)
- [Security Self-Defense: วิธีการใช้ KeePassXC เครื่องมือจัดการรหัสผ่านอย่างปลอดภัยในรูปแบบโอเพนซอร์ส](https://ssd.eff.org/en/module/how-use-keepassxc)
- [โครงการบันทึกข้อมูลสายด่วนชุมชน องค์กร Access Now: เลือกเครื่องมือจัดการรหัสผ่าน](https://communitydocs.accessnow.org/295-Password_managers.html)
