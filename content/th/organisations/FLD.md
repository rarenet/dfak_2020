---
language: th
layout: organisation
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship, physical_sec
beneficiaries: hrds, hros
hours: กรณีฉุกเฉิน 24 ชั่วโมง 7 วันต่อสัปดาห์, ทั่วโลก; เวลาทำการปกติ วันจันทร์ - วันศุกร์ ในเวลาทำการ, โซนเวลามาตรฐานอินเดีย (IST หรือ UTC+1), เจ้าหน้าที่ประจำอยู่ในหลายโซนเวลาในหลากหลายภูมิภาค
response_time: ภายในวันเดียวกันหรือวันต่อไปในกรณีที่เป็นเหตุฉุกเฉิน
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 สำหรับเหตุฉุกเฉิน; เบอร์โทรศัพท์สำนักงาน +353-1-212-3750
skype: front-line-emergency?call
email: info@frontlinedefenders.org สำหรับข้อติชมหรือคำถาม
initial_intake: yes
---

Front Line Defenders เป็นองค์กรระหว่างประเทศที่ตั้งอยู่ ณ ประเทศไอร์แลนด์และทำงานเกี่ยวกับการคุ้มครองนักปกป้องสิทธิมนุษยชนที่ตกอยู่ในความเสี่ยงในรูปแบบบูรณาการ Front Line Defenders ให้ความช่วยเหลืออย่างฉับพลันและจับต้องได้แก่นักปกป้องสิทธิมนุษยชนที่ตกอยู่ในความเสี่ยงซึ่งหน้า โดยให้ทุนสนับสนุนด้านความปลอดภัย จัดการอบรมเกี่ยวกับความปลอดภัยทางกายภาพและดิจิทัล และดำเนินการรณรงค์เคลื่อนไหว

Front Line Defenders มีบริการสายด่วนเพื่อช่วยเหลือกรณีฉุกเฉินตลอด 24 ชั่วโมงของทั้ง 7 วันในสัปดาห์ โดยสามารถนักปกป้องสิทธิมนุษยชนที่ตกอยู่ในความเสี่ยงซึ่งหน้าสามารถโทรไปที่ +353-121-00489 เพื่อรับบริการในภาษาอาหรับ อังกฤษ ฝรั่งเศส รัสเซีย หรือสเปนได้ เมื่อชีวิตของนักปกป้องสิทธิมนุษยชนตกอยู่ในความเสี่ยงซึ่งหน้า Front Line Defenders สามารถช่วยเขาย้ายถิ่นฐานชั่วคราวได้ Front Line Defenders จัดอบรมเกี่ยวกับความปลอดภัยทางกายภาพและทางดิจิทัล อีกทั้ง Front Line Defenders ยังเผยแพร่ข้อมูลเกี่ยวกับกรณีต่างๆที่นักปกป้องสิทธิมนุษยชนตกอยู่ในความเสี่ยงและรณรงค์เคลื่อนไหวในระดับนานาชาติ ซึ่งรวมไปถึงสหภาพยุโรป สหประชาชาติ กลไกระหว่างภูมิภาค และรัฐบาลต่างๆ โดยตรงเพื่อขอความคุ้มครองจากกลไกเหล่านี้
