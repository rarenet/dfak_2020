---
language: th
layout: organisation
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano, Українська, тоҷикӣ
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: ตลอด 24 ชั่วโมง 7 วันต่อสัปดาห์, ทั่วโลก
response_time: 2 ชั่วโมง
contact_methods: email, pgp
email: help@accessnow.org
pgp_key: https://keys.accessnow.org/help.asc
pgp_key_fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC
initial_intake: yes
---

สายด่วนเรื่องความปลอดภัยทางดิจิทัลของ Access Now ทำงานร่วมกับบุคคลและองค์กรทั่วโลกเพื่อให้อยู่ในโลกออนไลน์ได้อย่างปลอดภัย หากคุณกำลังตกอยู่ในความเสี่ยง เราสามารถช่วยพัฒนาแนวปฏิบัติของคุณเพื่อเพิ่มความปลอดภัยทางดิจิทัลและหลีกเลี่ยงอันตรายไม่ให้เกิดขึ้นกับคุณ หากคุณกำลังถูกจู่โจม เราสามารถให้ความช่วยเหลือฉุกเฉินแบบตอบสนองทันทีได้
