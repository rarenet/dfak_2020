---
language: th
layout: organisation
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: วันจันทร์ - วันศุกร์ ในเวลาทำการ, โซนเวลาตะวันออก สหรัฐอเมริกา
response_time: 1 วัน
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

Freedom of the Press Foundation (FPF) เป็นองค์กรไม่แสวงหาผลกำไรที่จัดตั้งขึ้นตามมาตรา 501(c)3 โดยทำงานด้านการปกป้อง คุ้มครอง และเสริมพลังให้กับงานสื่อที่ทำเพื่อประโยชน์สาธารณะในศตวรรษที่ 21 ทั้งนี้ องค์กร FPF จัดการอบรมให้คนทุกกลุ่ม ตั้งแต่องค์กรสื่อขนาดใหญ่ไปจนถึงนักข่าวอิสระ เกี่ยวกับเครื่องมือและเทคนิคต่างๆ ด้านความปลอดภัยและความเป็นส่วนตัว เพื่อช่วยให้พวกเขาปกป้องตัวเอง แหล่งช้อมูล และองค์กรของพวกเขา
