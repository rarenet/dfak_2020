---
language: th
layout: organisation
name: Deflect
website: https://www.deflect.ca
logo: deflect.png
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino
services: ddos, web_hosting, web_protection
beneficiaries: hrds, cso
hours: วันจันทร์ – วันศุกร์, ตลอด 24 ชั่วโมงทั้ง 5 วัน โซนเวลา UTC-4)
response_time: 6 ชั่วโมง
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

Deflect เป็นเว็บไซต์ให้บริการฟรีเพื่อคุ้มครองความปลอดภัยขององค์กรภาคประชาสังคมและกลุ่มสิทธิมนุษยชนจากการจู่โจมทางดิจิทัล
