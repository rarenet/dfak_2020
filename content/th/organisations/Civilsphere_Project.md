---
language: th
layout: organisation
name: Civilsphere Project
website: https://www.civilsphereproject.org/
logo: Civilsphere.png
languages: English, Español
services: assessment, vulnerabilities_malware, forensic
beneficiaries: journalists, hrds, cso, activists, lawyers
hours: วันจันทร์ – วันศุกร์ เวลา 9:00 น. – 17:00 น. CET (เวลายุโรปกลาง)
response_time: 3 วันทำการ
contact_methods: web_form, email, pgp, mail, telegram
web_form: www.civilsphereproject.org
email: civilsphere@aic.fel.cvut.cz
pgp_key: https://www.civilsphereproject.org/s/Civilsphere_Project_pub.asc
pgp_key_fingerprint: C1FD 513E D50F 00FE 6CBF C72F 52F7 76AD 9B72 6C6D
mail: "Stratosphere Lab (KN-E313), Karlovo náměstí 13, Praha 2, 121 35, Prague, Czech Republic"
telegram: "@civilsphereproject"
initial_intake: no
---

โครงการ Civilsphere มีภารกิจในการให้บริการและเครื่องมือเพื่อช่วยให้นักข่าว นักเคลื่อนไหว และผู้ถูกละเมิดสิทธิมนุษยชนรับรู้ถึงภัยคุกคามทางดิจิทัลที่เป็นอันตรายต่อชีวิตและการทำงานได้ตั้งแต่เนิ่นๆ
