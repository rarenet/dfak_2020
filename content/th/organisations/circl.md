---
language: th
layout: organisation
name: Computer Incident Response Center Luxembourg (CIRCL)
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: เวลาทำการของสำนักงาน, โซนเวลา UTC+2)
response_time: 4 ชั่วโมง
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

Computer Incident Response Center Luxembourg (CIRCL) เป็นทีมตอบสนองเหตุฉุกเฉินในชุมชน (Community Emergency Response Team หรือ CERT) สำหรับภาคเอกชน ชุมชน และหน่วยงานที่ไม่ใช่รัฐในประเทศลักเซมเบิร์ก

CIRCL สรรหาผู้ติดต่อประสานงานที่น่าเชื่อถือและเป็นที่ไว้วางใจให้กับผู้ใช้ บริษัท และองค์กรใดๆที่ตั้งอยู่ที่ลักเซมเบิร์ก โดยจะช่วยจัดการรับมือกับเหตุจู่โจมและเหตุการณ์ต่างๆ ทีมผู้เชี่ยวชาญของศูนย์ฯ ทำหน้าที่เสมือนคณะนักดับเพลิงที่สามารถช่วยตอบสนองอย่าฉับพลันและมีประสิทธิภาพทันทีที่มีการสงสัยหรือตรวจพบภัยคุกคามหรือมีเหตุการณ์เกิดขึ้น

CIRCL มีเป้าหมายในการเก็บรวบรวม รายงาน และตอบสนองต่อภัยทางไซเบอร์ด้วยวิธีการที่เป็นระบบและรวดเร็วฉับพลัน
