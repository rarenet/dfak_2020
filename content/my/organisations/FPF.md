---
language: my
layout: organisation
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: "တနင်္လာမှသောကြာ ၊ ရုံးချိန်အတွင်း (Eastern Time Zone, USA)"
response_time: "၁​ ရက်"
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

Freedom of the Press Foundation (FPF) သည် ၂၁ ရာစုရှိ အများပြည်သူ အကျိုးစီးပွားသက်ရောက်သော သတင်းစာပညာကို ကာကွယ်စောင့်ရှောက်ပေးသော ၅၀၁(ဂ)၃ မှတ်ပုံတင်ထားသည့် အမြတ်မယူသော အဖွဲ့အစည်းဖြစ်ပါသည်။ FPF သည် မိမိကိုယ်တိုင်၊ မိမိ၏ ပစ္စည်းအရာများ နှင့် အဖွဲ့အစည်းများကို ပိုမိုကာကွယ်နိုင်ရန်အတွက် လုံခြုံရေးကိရိယာများ နှင့် နည်းစနစ်များကို မီဒီယာအဖွဲ့အစည်းအကြီးများ နှင့် လွပ်လပ်သော စာနယ်ဇင်းသမားများသို့ သင်ကြားပေးပါသည်။
