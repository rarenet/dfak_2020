---
language: my
layout: organisation
name: Nothing2Hide
website: https://nothing2hide.org
logo: nothing2hide.png
languages: Français, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: "တနင်္လာမှသောကြာ ၊ 9am to 6pm CET"
response_time: "၁ ရက်"
contact_methods: email, pgp, signal, web_form, wire
web_form: https://vault.tech4press.org/#/ (online secured form - based on Globaleaks)
email: help@tech4press.org
pgp_key:  http://tech4press.org/fr/contact-mail/
pgp_key_fingerprint: 2DEB 9957 8F91 AE85 9915 DE94 1B9E 0F13 4D46 224B
signal: +33 7 81 37 80 08 <http://tech4press.org/fr/#signal>
wire: tech4press
---

Nothing2Hide (N2H) သည် စာနယ်ဇင်းသမားများ၊ ရှေ့နေများ၊ လူ့အခွင့်အရေး တက်ကြွလှုပ်ရှားသူများနှင့် "သာမန်" ပြည်သူလူထုတို့အနေဖြင့် ၎င်းတို့၏ ဒေတာနှင့် ဆက်သွယ်မှုများကို ကာကွယ်နိုင်သည့် နည်းလမ်းများ သိရှိထားနိုင်ရန် ရည်ရွယ်၍ သက်ဆိုင်ရာအခြေအနေအလိုက် သင့်လျော်သော နည်းပညာဆိုင်ရာ ဖြေရှင်းနည်းများနှင့် သင်တန်းများ‌ပေးသည့် အဖွဲ့အစည်းဖြစ်ပါသည်။ ကျွန်ုပ်တို့၏ မျှော်မှန်းချက်မှာ ကမ္ဘာတစ်ဝှမ်းတွင် ဒီမိုကရေစီစနစ်များ ခိုင်မာစေရန် သတင်းအချက်အလက်မျှဝေခြင်းနှင့် ကာကွယ်စောင့်ရှောက်ခြင်းတို့ဆောင်ရွက်ရာတွင် နည်းပညာကိုအသုံးချနိုင်စေရန်ဖြစ်သည်။
