---
language: my
layout: organisation
name: Digital Rights Foundation
website: https://digitalrightsfoundation.pk/
logo: DRF.png
languages: English, اردو
services: grants_funding, in_person_training, org_security, web_hosting, web_protection, digital_support, relocation, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: ပိတ်ရက်မရှိ, 9 AM-5 PM UTC+5 (PST)
response_time: ၅၆ နာရီ
contact_methods: email, pgp, mail, phone, whatsapp
email: helpdesk@digitalrightsfoundation.pk
pgp_key_fingerprint: 4D02 8B68 866F E2AD F26F  D7CC 3283 B8DA 6782 7590
mail: 180A, Garden Block, Garden Town, Lahore, Pakistan
phone: +9280039393
whatsapp: +923323939312
---

Digital Rights Foundation (DRF) သည် ပါကစ္စတန်အခြေစိုက် သုတေသနအခြေပြု စည်းရုံးထောက်ပံ့ပေးပြီး မှတ်ပုံတင်ထားသည့် အစိုးရမဟုတ်သော အဖွဲ့အစည်းတစ်ခုဖြစ်ပါသည်။ ၂၀၁၂ ခုနှစ်တွင် စတင်တည်ထောင်ခဲ့ပြီး DRF သည် လူ့အခွင်းအရေးများ၊ အားလုံးအကျုံးဝင်မှု၊ ဒီမိုကရေစီလုပ်စဥ်များ နှင့် ဒီဂျစ်တယ်အုပ်ချုပ်မှု တို့ကို ပံ့ပိုးပေးရန် သတင်းအချက်အလက် နှင့် ဆက်သွယ်ရေးနည်းပညာများ (ICT) ကို အာရုံစိုက်ပေးပါသည်။ DRF သည် အွန်လိုင်းပေါ်တွင် လွတ်လပ်စွာပြောဆိုနိုင်ခွင့်၊ ပုဂ္ဂိုလ်လုံခြုံရေး၊ ဒေတာကာကွယ်မှု နှင့် အမျိုးသမီးများအပေါ် အွန်လိုင်းအကြမ်းဖက်မှု တားဆီးခြင်း ဆိုင်ရာ ကိစ္စရပ်များတွင် အလုပ်လုပ်ဆောင်ပါသည်။ 
