---
language: my
layout: organisation
name: SHARE CERT
website: https://www.sharecert.rs/
logo: SHARECERT_Logo.png
languages: Srpski, Македонски, English, Español
services: in_person_training, org_security, assessment, secure_comms, vulnerabilities_malware, browsing, account, harassment, forensic, legal, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: "တနင်္လာမှသောကြာ​၊ 9 AM - 5 PM, GMT+1/GMT+2"
response_time: "တစ်ရက်အတွင်း"
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.sharecert.rs/prijava-incidenta/
email: info@sharecert.rs, emergency@sharecert.rs
pgp_key_fingerprint: info@sharecert.rs - 3B89 7A55 8C36 2337 CBC2 C6E9 A268 31E2 0441 0C10
mail: Kapetan Mišina 6A, Office 31, 11000 Belgrade, Serbia
phone: +381 64 089 70 67
initial_intake: yes
---

SHARE CERT ၏မစ်ရှင်သည် ၎င်းတို့၏ အသိုင်းအဝိုင်းအား တိုက်ခိုက်မှုများကို ထိရောက်သော နည်းလမ်းဖြင့် တုံ့ပြန်နိုင်စေရန် စွမ်းဆောင်ရည်မြှင့်ပေးရန် နှင့် အနာဂတ်တွင် ဆိုက်ဘာတိုက်ခိုက်မှုများကို ကာကွယ်ရန် ပိုမိုခံနိုင်ရည်ရှိစေရန် ဖြစ်ပါသည်။
