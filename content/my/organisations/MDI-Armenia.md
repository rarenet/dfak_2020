---
language: my
layout: organisation
name: Media Diversity Institute - Armenia
website: https://mdi.am/
logo: MDI_Armenia_Logo.png
languages: հայերեն, Русский, English
services: in_person_training, org_security, web_hosting, web_protection, digital_support, triage, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, censorship, physical_sec
beneficiaries: journalists, hrds, activists, lgbti, women, cso
hours: "၂၄/၇ ၊ GMT+4"
response_time: "၃ နာရီ"
contact_methods: email, pgp, phone, whatsapp, signal, telegram
email: help@mdi.am
pgp_key_fingerprint: D63C EC9C D3AD 51BE EFCB  683F F2B8 6042 F9D9 23A8
phone: "+37494938363"
whatsapp: "+37494938363"
signal: "+37494938363"
telegram: "+37494938363"
initial_intake: yes
---

Media Diversity Institute - Armenia သည် အမြတ်အစွန်းမယူသော၊ အစိုးရမဟုတ်သော အဖွဲ့အစည်းတစ်ခုဖြစ်ပါသည်။ ၎င်းသည် ရိုးရာမီဒီယာ၊ လူမှုမီဒီယာ နှင့် နည်းပညာအသစ်များကို အသုံးပြု၍ လူ့အခွင့်အရေးများကို ကာကွယ်စေရန်၊ ဒီမိုကရေစီ လိုက်နာသော အရပ်ဘက် အသိုင်းအဝိုင်း တည်ဆောက်ရန်၊ လူနည်းစုများသို့ အသံပေးရန် နှင့် မတူကွဲပြားမှု အမျိုးမျိုး၏ စုပေါင်းနားလည်မှုကို နက်ရှိုင်းစေရန် လုပ်ဆောင်ပါသည်။

MDI Armenia သည် လန်ဒန်အခြေစိုက် Media Diversity Institute နှင့် ဆက်နွယ်သော်လည်း လွတ်လပ်သော အဖွဲ့အစည်းတစ်ခုဖြစ်ပါသည်။
