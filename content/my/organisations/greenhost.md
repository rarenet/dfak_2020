---
language: my
layout: organisation
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: "၂၄/၇, UTC+2"
response_time: "၄ နာရီ"
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost သည် ကျင့်ဝတ်ရှိစွာ နှင့် ရေရှည်တည်တံ့နိုင်သော နည်းဖြင့် အိုင်တီဝန်ဆောင်မှုများကို ပေးဆောင်ပါသည်။ ၎င်းတို့၏ ဝန်ဆောင်မှုများတွင် ဝဘ်ဆိုဒ်ထောင်ခြင်း၊ ကလောက် (cloud) ဝန်ဆောင်မှုများ နှင့် အချက်အလက်လုံခြုံရေးအတွက် အစွမ်းထက်သော အထူးပြု  ဝန်ဆောင်မှုများ ပါဝင်ပါသည်။ Greenhost သည် အများသုံးနိုင်သော ကုဒ်အရင်းမြစ်၏ (open source) ဖွံ့ဖြိုးမှုတွင် တက်ကြွစွာ ပါဝင်ပြီး နည်းပညာ၊ သတင်းစာပညာ၊ ရိုးရာယဥ်ကျေးမှု၊ ပညာရေး၊ ရေရှည်တည်တံ့ခိုင်မြဲရေး နှင့် အင်တာနက်လွတ်လပ်ခွင့် ဆိုင်ရာ စီမံကိန်းများတွင် အလုပ်လုပ်ဆောင်ပါသည်။
