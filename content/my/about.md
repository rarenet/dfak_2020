---
layout: page
title: "About"
language: my
summary: "Digital First Aid Kit(ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန်) အကြောင်း"
date: 2020-11
permalink: about
parent: Home
---

ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန်သည် [RaReNet (Rapid Response Network)](https://www.rarenet.org/) နှင့် [CiviCERT](https://www.civicert.org/) တို့ ပူးပေါင်းဆောင်ရွက်ထားပါသည်။

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Rapid Response Network ဆိုသည်မှာ လျင်မြန်စွာတုံ့ပြန်သူများ နှင့် ဒစ်ဂျစ်တယ်လုံခြုံရေးချန်ပီယံများဖြစ်သည့် Access Now, Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Fembloc, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtualroad အပြင် ထိုနယ်ပယ်တွင်လုပ်ဆောင်နေကြသော ကျွမ်းကျင်သူများအားလုံးအတူလုပ်ကိုင်နေသော အပြည်ပြည်ဆိုင်ရာကွန်ယက်တစ်ခုဖြစ်ပါသည်။

ယင်းတို့မှ အချို့သောအဖွဲ့အစည်းများနှင့် လူပုဂ္ဂိုလ်များသည် CiviCERT တွင် ပါဝင်သူများဖြစ်သည်။ CiviCERT သည် လူ့အခွင့်အရေးနှင့်ဒစ်ဂျစ်တယ်အခွင့်အရေးများကိုကာကွယ်ရန်ကြိုးပမ်းနေသည့်အဖွဲ့များနှင့် လူပုဂ္ဂိုလ်များကို ကူညီထောက်ပံ့ပေးရန် အဓိက အာရုံစိုက်သည့် ဒစ်ဂျစ်တယ်လုံခြုံရေးအကူအညီပေးနေသည့်နေရာများနှင့် အခြေခံဝန်ဆောင်မှုပေးသူများ၏ အပြည်ပြည်ဆိုင်ရာကွန်ယက်တစ်ခုဖြစ်ပါသည်။ CiviCERT သည် လျင်မြန်စွာတုန့်ပြန်အသိုင်းအဝိုင်းရဲ့ဖြန့်ဝေသည့် CERT (ကွန်ပျူတာအရေးပေါ်တုံ့ပြန်မှုအဖွဲ့) အားထုတ်မှုတစ်ခုဖြစ်ပါသည်။ CiviCERT သည်ယုံကြည်စိတ်ချရသောကွန်ပျူတာ အရေးပေါ်တုံ့ပြန်မှုအဖွဲ့များ၏ဥရောပကွန်ယက် မှအသိအမှတ်ပြုထားသည်။

ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန်သည် [ပြင်ပပံ့ပိုးမှုများကိုလက်ခံသော open-source စီမံကိန်း](https://gitlab.com/rarenet/dfak_2020) တစ်ခုဖြစ်သည်။

ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန်ကို ဒေသဆိုင်ရာ လျော်ညီအောင် ဘာသာပြန်ပြုစုပေးသော အဖွဲ့အစည်းများဖြစ်သည့် [Armenian ဘာသာစကား](https://digitalfirstaid.org/hy/) တို့အတွက် [Media Diversity Institute](https://mdi.am/en/home) နှင့် [Albanian ဘာသာစကား](https://digitalfirstaid.org/sq/) အတွက် [Metamorphosis Foundation](https://metamorphosis.org.mk)၊ [ဗမာဘာသာစကား](https://digitalfirstaid.org/my/)၊ [အင်ဒိုနီးရှားဘာသာစကား](https://digitalfirstaid.org/id/) နှင့် [ထိုင်းဘာသာစကား](https://digitalfirstaid.org/th/) တို့အတွက် [EngageMedia](https://engagemedia.org/) ကိုကျေးဇူးတင်ပါသည်။

အကယ်၍ သင်ဆက်သွယ်မှုအကန့်အသတ်ရှိသည့်အခြေအနေမျိုးတွင် ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန်ကိုအသုံးပြုလိုပါက သို့မဟုတ် ဆက်သွယ်မှုတစ်ခုခက်ခဲခြင်းကိုအသုံးပြုလိုပါက [an offline version](https://digitalfirstaid.org/dfak-offline.zip) ကိုဒေါင်းလော့ဆွဲနိုင်သည်။

ဒစ်ဂျစ်တယ်ရှေးဦးသူနာပြုလမ်းညွှန်ကို မှတ်ချက်ပေးခြင်း၊ အကြုံပြုချက်ပေးခြင်း သို့မဟုတ် မေးစရာမေးခွန်းများ ရှိပါက dfak @ digitaldefenders.org သို့ အီးမေးလ်ပေးပို့နိုင်ပါသည်။

GPG - Fingerprint: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
