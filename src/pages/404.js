import React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"
// import "../css/dfak.scss"

const NotFoundPage = () => (
  <Layout>
    <Seo title="404: Not found" />
    <h1>NOT FOUND</h1>
    <p>
      You just hit a route that doesn&#39;t exist...{" "}
      <span
        role="button"
        tabIndex="0"
        onKeyUp={null}
        onClick={() => window.history.back()}
        className="link dim underline b dark-blue"
      >
        Go Back
      </span>
    </p>
  </Layout>
)

export default NotFoundPage
