import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"
import _ from "lodash"
import map from "lodash/fp/map"
import compose from "lodash/fp/compose"
import concat from "lodash/fp/concat"
import Layout from "../components/layout"
import Seo from "../components/seo"
import { Link, useI18next } from "gatsby-plugin-react-i18next"

function IndexPage({ siteTitle, data }) {
  const { t, language } = useI18next()
  const [topics, setTopics] = useState([])
  const dir = data.dir.siteMetadata.rtlLanguages

  useEffect(() => {
    const markdownTopics = compose(map("node"))(data.allMarkdownRemark.edges)
    setTopics(markdownTopics.concat(data.arrested))
  }, [])
  return (
    <Layout>
      <Seo title={t("home_welcome_2")} />
      <div className="flex flex-col w-full md:w-10/12 m-auto mt-20">
        <div className="m-auto p-2 bg-white uppercase text-3xl font-['Serif'] font-bold  text-center">
          {t("home_welcome_1")}
        </div>
        <div className="m-auto p-2 bg-white uppercase text-3xl font-['Serif'] font-bold text-center">
          {t("home_welcome_2")}
        </div>
        <span className="m-auto py-2 px-4 text-white bg-black text-xl font-['Serif'] text-center">
          {t("home_byline")}
        </span>
      </div>
      <section className="mt-12 bg-white">
        <div className="font-serif text-[14pt] leading-7 px-10 py-12">
          {t("home_intro")}
        </div>
        <div className="bg-dfak-blue">
          <div className="m-auto -mt-5 text-3xl font-['Serif'] font-semibold uppercase border-t-8 border-black w-11/12 md:w-7/12 p-6 text-center">
            {t("home_topics_title")}
          </div>
          <div className="m-auto w-11/12 md:w-8/12 flex flex-col items-center sm:grid sm:grid-cols-2 md:grid-cols-3 gap-12 py-12">
            {topics.length > 0 &&
              _.map(topics, (topic, i) => {
                return (
                  <Link
                    to={`/${topic.frontmatter.permalink}`}
                    className="flex flex-col items-center"
                    key={i}
                    language={language}
                  >
                    <img
                      style={{ width: "12em" }}
                      alt={topic.frontmatter.title}
                      src={`/images/topics/${topic.fileAbsolutePath
                        .split("/")
                        .reverse()[0]
                        .replace(".md", ".svg")}`}
                    />
                    <h3
                      className={`font-['Serif'] text-lg mt-2 font-bold ${
                        dir.includes(language) ? "text-right" : "text-center"
                      }`}
                    >
                      {topic.frontmatter.title}
                    </h3>
                  </Link>
                )
              })}
          </div>
          <p className="m-auto w-11/12 md:w-8/12 text-lg font-['Serif'] pb-12">
            {t("home_topics_description")}
          </p>
        </div>
      </section>
      <section className="bg-white flex flex-col px-12 sm:px-24 md:px-32 py-12 md:py-20 md:grid md:grid-cols-3 gap-12 font-['Serif']">
        {[1, 2, 3].map(i => (
          <div key={i}>
            <h3 className="text-2xl font-sans font-bold mb-2">
              {t(`home_bottom_col_${i}_title`)}
            </h3>
            <p
              className="prose text-black"
              dangerouslySetInnerHTML={{
                __html: t(`home_bottom_col_${i}_text`),
              }}
            />
          </div>
        ))}
      </section>
    </Layout>
  )
}

export const homeQuery = graphql`
  query ($language: String!) {
    dir: site {
      siteMetadata {
        rtlLanguages
      }
    }
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    allMarkdownRemark(
      filter: {
        frontmatter: { layout: { eq: "topic" }, language: { eq: $language } }
      }
      sort: { frontmatter: { order: ASC } }
    ) {
      edges {
        node {
          fields {
            slug
          }
          id
          fileAbsolutePath
          frontmatter {
            permalink
            title
            language
            summary
          }
        }
      }
    }
    arrested: markdownRemark(
      fields: { slug: { glob: "**/**arrested" } }
      frontmatter: { language: { eq: $language } }
    ) {
      fields {
        slug
      }
      id
      fileAbsolutePath
      frontmatter {
        permalink
        title
        language
        summary
      }
    }
  }
`

export default IndexPage
