import React from "react"
import { graphql } from "gatsby"
import _ from "lodash"
import map from "lodash/fp/map"
import filter from "lodash/fp/filter"
import sortBy from "lodash/fp/sortBy"
import uniqBy from "lodash/fp/uniqBy"
import split from "lodash/fp/split"
import compose from "lodash/fp/compose"
import Layout from "../components/layout"
import Seo from "../components/seo"
import Org from "../components/organisation"
import Filters from "../components/filters"
import { withTranslation } from "gatsby-plugin-react-i18next"
import { Transition } from "@headlessui/react"

class SupportPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      viewStyle: "list",
      showFilters: false,
      selectedFilters: [],
      orgs: [],
      filteredOrgs: [],
      allFilters: [],
      filters: {},
      page: {},
    }
    this.updateFilter = this.updateFilter.bind(this)
    this.toggleFilters = this.toggleFilters.bind(this)
    this.toggleViewStyle = this.toggleViewStyle.bind(this)
  }

  componentDidMount() {
    const { data, pageContext } = this.props
    const language = pageContext.language
    const page = data.markdownRemark

    // Filter all organisations by current language
    const nodes = compose(
      filter(node => node.frontmatter.language === language),
      map("node")
    )(data.allMarkdownRemark.edges)
    // create the orgfilters array of all filters for each organisation.
    _.map(nodes, node => {
      const f = _.concat(
        [],
        node.frontmatter.services.split(","),
        node.frontmatter.languages.split(","),
        node.frontmatter.beneficiaries.split(",")
      )
      _.merge(node, {
        orgfilters: _.map(f, fi => _.trim(_.kebabCase(fi))),
      })
    })

    // create the filters object for the filters section
    let filters = {}
    _.each(["languages", "services", "beneficiaries"], k => {
      _.merge(filters, {
        [k]: compose(
          sortBy(n => n),
          uniqBy(n => n),
          map(n => _.trimStart(n)),
          split(","),
          map(`frontmatter.${k}`)
        )(nodes),
      })
    })

    // store an array of all currently available filters.
    const allFilters = _.uniq(_.flatten(_.map(nodes, "orgfilters")))

    this.setState({
      page,
      orgs: nodes,
      filteredOrgs: nodes,
      filters,
      allFilters,
    })
  }

  updateFilter(f) {
    if (f === null) {
      // check for resetting filters
      this.setState({
        selectedFilters: [],
        filteredOrgs: this.state.orgs,
      })
    } else if (_.includes(this.state.selectedFilters, f)) {
      // remove a filter from the selected filters
      const selectedFilters = _.without(this.state.selectedFilters, f)

      this.setState({
        selectedFilters: _.without(this.state.selectedFilters, f),
        filteredOrgs:
          selectedFilters.length > 0
            ? _.filter(
                this.state.orgs,
                org =>
                  _.intersection(org.orgfilters, selectedFilters).length > 0
              )
            : this.state.orgs,
      })
    } else {
      // add a filter to the selected filters
      const selectedFilters = this.state.selectedFilters
      selectedFilters.push(f)
      // filter filtered orgs.
      const filteredOrgs =
        this.state.selectedFilters.length === 1
          ? _.filter(
              this.state.orgs,
              org => _.intersection(selectedFilters, org.orgfilters).length > 0
            )
          : _.remove(this.state.filteredOrgs, org =>
              _.includes(org.orgfilters, f)
            )

      const allFilters = _.uniq(_.flatten(_.map(filteredOrgs, "orgfilters")))
      this.setState({
        selectedFilters: _.uniq(_.concat(this.state.selectedFilters, f)),
        filteredOrgs,
        allFilters,
      })
    }
  }

  toggleFilters(f) {
    this.setState({
      showFilters: !this.state.showFilters,
    })
  }

  toggleViewStyle(view) {
    this.setState({
      viewStyle: { list: "grid", grid: "list" }[view],
    })
  }

  render() {
    const { t } = this.props
    const {
      page,
      filters,
      showFilters,
      selectedFilters,
      viewStyle,
      filteredOrgs,
    } = this.state
    const buttonClasses =
      "flex align-bottom bg-dfak-blue rounded-sm self-start hover:bg-white hover:dfak-blue hover:ring-2"
    return (
      <Layout>
        <Seo title={t("navbar_support")} />
        <article className="px-[5px] sm:px-5 md:p-7 flex flex-col">
          <h1> {t("navbar_support")} </h1>
          <div
            className="mt-5 mb-10 prose-md max-w-none"
            dangerouslySetInnerHTML={{
              __html: page.html,
            }}
          />
          <div className="flex flex-col mb-10">
            <div className="flex justify-between p-2 bg-dfak-blue">
              <div className="flex relative">
                {selectedFilters.length > 0 && (
                  <div className="absolute w-2 h-2 rounded-full bg-red-600 top-0 right-0"></div>
                )}
                <button
                  onClick={() => this.toggleFilters()}
                  className={buttonClasses}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-5 w-5 rotate-90"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M6 13.5V3.75m0 9.75a1.5 1.5 0 010 3m0-3a1.5 1.5 0 000 3m0 3.75V16.5m12-3V3.75m0 9.75a1.5 1.5 0 010 3m0-3a1.5 1.5 0 000 3m0 3.75V16.5m-6-9V3.75m0 3.75a1.5 1.5 0 010 3m0-3a1.5 1.5 0 000 3m0 9.75V10.5"
                    />
                    <title>{t("strings_filter")}</title>
                  </svg>
                </button>
              </div>
              <div className="text-sm">
                {t("strings_filters_count", {
                  count: filteredOrgs.length,
                })}
              </div>
              <button
                onClick={() => this.toggleViewStyle(viewStyle)}
                className={buttonClasses}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 16 16"
                  strokeWidth={0.5}
                  stroke="currentColor"
                  className="w-5 h-5"
                >
                  {viewStyle === "list" ? (
                    <path
                      fillRule="evenodd"
                      d="M1 2.5A1.5 1.5 0 0 1 2.5 1h3A1.5 1.5 0 0 1 7 2.5v3A1.5 1.5 0 0 1 5.5 7h-3A1.5 1.5 0 0 1 1 5.5v-3zM2.5 2a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 1h3A1.5 1.5 0 0 1 15 2.5v3A1.5 1.5 0 0 1 13.5 7h-3A1.5 1.5 0 0 1 9 5.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zM1 10.5A1.5 1.5 0 0 1 2.5 9h3A1.5 1.5 0 0 1 7 10.5v3A1.5 1.5 0 0 1 5.5 15h-3A1.5 1.5 0 0 1 1 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 9h3a1.5 1.5 0 0 1 1.5 1.5v3a1.5 1.5 0 0 1-1.5 1.5h-3A1.5 1.5 0 0 1 9 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3z"
                      clipRule="evenodd"
                    />
                  ) : (
                    <path
                      fillRule="evenodd"
                      d="M3 4.5h10a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-3a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1H3zM1 2a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13A.5.5 0 0 1 1 2zm0 12a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13A.5.5 0 0 1 1 14z"
                      clipRule="evenodd"
                    />
                  )}
                </svg>
              </button>
            </div>

            <Transition
              show={showFilters}
              enter="transition duration-[.5s] ease-in"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="transition duration-[.4s]"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
              className="w-full relative"
            >
              <Filters
                filters={filters}
                selectedFilters={selectedFilters}
                updateFilter={this.updateFilter}
                toggleFilters={this.toggleFilters}
                showFilters={showFilters}
                buttonClasses={buttonClasses}
              />
            </Transition>
          </div>
          <div
            className={`gap-5 grid grid-cols-1 ${
              viewStyle === "grid"
                ? " sm:grid-cols-2 md:grid-cols-3 m-auto w-full"
                : ""
            }`}
          >
            {filteredOrgs.map((node, idx) => (
              <Org
                key={idx}
                organisation={node}
                viewStyle={viewStyle}
                selectedFilters={selectedFilters}
              />
            ))}
          </div>
        </article>
      </Layout>
    )
  }
}

export const supportQuery = graphql`
  query ($slug: String!, $language: String!) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    markdownRemark(
      frontmatter: { language: { eq: $language }, permalink: { eq: $slug } }
    ) {
      html
      frontmatter {
        title
      }
    }
    allMarkdownRemark(
      filter: {
        frontmatter: {
          layout: { eq: "organisation" }
          language: { eq: $language }
        }
      }
    ) {
      edges {
        node {
          id
          html
          frontmatter {
            language
            name
            website
            logo
            languages
            services
            beneficiaries
            hours
            response_time
            contact_methods
            hours
            response_time
            contact_methods
            email
            pgp_key
            pgp_key_fingerprint
            mail
            web_form
            signal
            phone
            whatsapp
            telegram
            skype
            mobile_app
          }
        }
      }
    }
  }
`

export default withTranslation()(SupportPage)
