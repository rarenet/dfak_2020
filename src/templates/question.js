import React from "react"
import { useI18next } from "gatsby-plugin-react-i18next"

import Layout from "../components/layout"
import Seo from "../components/seo"
import Breadcrumb from "../components/breadcrumb"

export default function Question({ pageContext: page }) {
  const { t } = useI18next()
  return (
    <Layout>
      <Seo title={page.page.title} />
      <article>
        <Breadcrumb
          parentLink={page.page.parentLink}
          parentTitle={page.page.parentTitle}
        />

        <div className="page-content w-100 center">
          <div dangerouslySetInnerHTML={{ __html: page.page.html }} />
        </div>
        <button className="topic-button" onClick={() => window.history.back()}>
          {t("strings_previous_question")}
        </button>
      </article>
    </Layout>
  )
}
