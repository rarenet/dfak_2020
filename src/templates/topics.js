import React from "react"
import { graphql } from "gatsby"
import { withTranslation } from "gatsby-plugin-react-i18next"
import visit from "unist-util-visit"
import find from "unist-util-find"
import findAllAfter from "unist-util-find-all-after"
import findAllBefore from "unist-util-find-all-before"
import { findAfter } from "unist-util-find-after"
import filter from "unist-util-filter"
import { remove } from "unist-util-remove"
import between from "unist-util-find-all-between"
import u from "unist-builder"
import toHTML from "hast-util-to-html"
import _ from "lodash"
import { FaUndo, FaCheckCircle } from "react-icons/fa"

import Layout from "../components/layout"
import Seo from "../components/seo"
import Breadcrumb from "../components/breadcrumb"
import Org from "../components/organisation"

class Topics extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      intro: "",
      sections: {},
      currentSection: null,
      previousSections: [],
      organisations: [],
      finalTips: null,
      resources: null,
      frontmatter: null,
      loading: false,
    }
    this.changeSection = this.changeSection.bind(this)
    this.listOrganisations = this.listOrganisations.bind(this)
  }

  componentDidMount() {
    const { htmlAst: ast, frontmatter, fields } = this.props.data.markdownRemark
    const organisations = this.props.data.allMarkdownRemark
      ? this.props.data.allMarkdownRemark.edges.map(o => o.node)
      : []
    const { i18n } = this.props

    const astSteps = [],
      topic = {},
      sections = {}
    const resourcesIndex = find(ast, node => {
      return (
        node.tagName === "h3" &&
        node.children[0].value.toLowerCase() === "resources"
      )
    })
    const finalTipsIndex = find(ast, node => {
      return (
        node.tagName === "h3" &&
        node.children[0].value.toLowerCase() === "final tips"
      )
    })
    const finalTips = between(ast, finalTipsIndex, resourcesIndex)
    const resources = findAllAfter(ast, resourcesIndex)
    visit(ast, "element", wNode => {
      if (wNode.tagName === "a") {
        if (wNode.properties.href && wNode.properties.href.startsWith("#")) {
          const hashValue = _.kebabCase(wNode.properties.href.slice(1))
          wNode.properties.href = `#${hashValue}`
        } else if (
          wNode.properties.href &&
          wNode.properties.href.startsWith("http")
        ) {
          _.merge(wNode.properties, {
            target: "_blank",
            rel: "noreferrer",
          })
        } else if (
          wNode.properties.href &&
          wNode.properties.href.indexOf("/topics") !== -1
        ) {
          wNode.properties.href.replace(/\.\.\//g, "")
          wNode.properties.href = `${
            frontmatter.language === i18n.language
              ? ""
              : `/${frontmatter.language}`
          }${wNode.properties.href}`
          _.merge(wNode.properties, {
            target: "_blank",
            rel: "noreferrer",
          })
        }
      }
      if (
        wNode.tagName === "h3" &&
        !["resources", "final tips"].includes(
          wNode.children[0].value.toLowerCase()
        )
      ) {
        astSteps.push(wNode)
      }
    })
    const withoutTitle = filter(ast, wNode => wNode.tagName !== "h1"),
      h2Pos = find(withoutTitle, {
        tagName: "h2",
      }),
      intro = u("root", findAllBefore(withoutTitle, h2Pos).reverse())
    _.merge(topic, {
      html: toHTML(intro),
      path: fields.slug,
      frontmatter: frontmatter,
      slug: fields.slug,
    })

    astSteps.map((step, idx) => {
      let to = {},
        stepBlock = {}

      const from = u("root", findAllAfter(ast, step)),
        title = _.kebabCase(step.children[0].value),
        questionPath = `${fields.slug}/question/${title}`

      if (idx === 0) {
        _.merge(topic, {
          start: `#${title}`,
        })
      }

      if (astSteps[idx + 1]) {
        const nextNode = find(from, node => node === astSteps[idx + 1])
        to = findAllBefore(from, nextNode).reverse()
        stepBlock = u("root", to)
      } else {
        stepBlock = from
      }
      const header = find(stepBlock, { tagName: "h4" })
      const optionHeader = find(stepBlock, { tagName: "h5" })
      remove(stepBlock, header)
      let optionsLinks = []
      const stepOptionsBlock =
        optionHeader &&
        findAfter(stepBlock, find(stepBlock, { tagName: "h5" }), "element")
      stepOptionsBlock &&
        visit(stepBlock, stepOptionsBlock, s => {
          s.properties = {
            class: "not-prose step-options m-0 p-0 pb-20 flex flex-col w-full",
          }
          s.children.map(child => {
            if (child.tagName) {
              return (
                child.tagName === "li" &&
                optionsLinks.push({
                  link: child.children[0].properties.href,
                  text: child.children[0].children[0].value,
                })
              )
            }
          })
        })
      const orgsFilter = find(
        stepBlock,
        org =>
          org.tagName === "a" &&
          org.properties.href.startsWith(":organisations")
      )
      remove(stepBlock, optionHeader)
      remove(stepBlock, orgsFilter)
      remove(stepBlock, resourcesIndex)
      remove(stepBlock, finalTipsIndex)
      resources.map(resource => remove(stepBlock, resource))
      finalTips.map(tip => remove(stepBlock, tip))
      const ste = {
        id: idx,
        html: toHTML(stepBlock),
        title,
        header: toHTML(header || ""),
        parentLink: fields.slug,
        parentTitle: frontmatter.title,
        path: questionPath,
        options: optionsLinks,
        orgsFilter:
          orgsFilter && orgsFilter.properties.href.split("=").reverse()[0],
      }
      return _.merge(sections, {
        [title]: ste,
      })
    })

    const currentSection = _.includes(
      _.keys(sections),
      window.location.hash.slice(1)
    )
      ? sections[window.location.hash.slice(1)]
      : null

    this.setState({
      currentSection,
      sections,
      intro: topic,
      organisations,
      finalTips: toHTML(finalTips),
      resources: toHTML(resources),
      frontmatter,
      loading: true,
    })

    window !== undefined &&
      window.addEventListener(
        "hashchange",
        e => this.changeSection(window.location.hash.slice(1)),
        true
      )
  }

  componentWillUnmount() {
    window !== undefined &&
      window.removeEventListener(
        "hashchange",
        this.changeSection(window.location.hash.slice(1)),
        false
      )
  }

  changeSection(s) {
    const sections = this.state.sections
    const previousSections = this.state.previousSections
    if (window !== undefined) {
      window.location.hash = s
    }
    const sIndex = previousSections.indexOf(s)
    const updatedPreviousSections =
      sIndex !== -1
        ? _.dropRight(previousSections, previousSections.length - (sIndex + 1))
        : _.concat(previousSections, s)
    this.setState({
      currentSection: sections[s],
      previousSections: s === "" ? [] : updatedPreviousSections,
    })
  }

  listOrganisations(orgsFilter) {
    const { organisations } = this.state
    const orgs = organisations.filter(organisation =>
      organisation.frontmatter.services.includes(orgsFilter)
    )
    return orgs.map((org, orgIdx) => (
      <div className="mt-5">
        <Org
          key={orgIdx}
          organisation={org}
          viewStyle="list"
          selectedFilters={[]}
        />
      </div>
    ))
  }

  render() {
    const {
      intro,
      currentSection,
      previousSections,
      sections,
      finalTips,
      resources,
      frontmatter,
      loading,
    } = this.state
    const { t } = this.props
    return (
      <Layout>
        {loading ? (
          <>
            <Seo title={frontmatter && frontmatter.title} />
            <article className="topic px-[5px] sm:px-5 md:p-7 flex flex-col m-auto">
              <Breadcrumb
                parentLink={frontmatter && frontmatter.permalink}
                parentTitle={frontmatter && frontmatter.title}
                changeSection={this.changeSection}
              />
              {!currentSection ? (
                // {COMMENT: Intro section to start the workflow}
                <>
                  <div className="flex md:flex-row flex-col">
                    <div
                      className="title md:w-4/12 w-full pt-5 px-3 pb-2 flex flex-col items-center"
                      style={{
                        backgroundColor: "#d0f1f2",
                      }}
                    >
                      <img
                        style={{
                          width: "11em",
                        }}
                        alt={frontmatter && frontmatter.title}
                        src={`/images/${
                          frontmatter && frontmatter.permalink
                        }.svg`}
                      />
                      <h2 className="text-center text-3xl">
                        {" "}
                        {frontmatter && frontmatter.title}{" "}
                      </h2>
                    </div>
                    <div
                      className={`${
                        frontmatter && frontmatter.title
                      }  bg-white md:w-8/12 w-full p-12 pt-10 prose-xl prose-p:leading-6 prose-li:list-disc prose-a:underline font-serif`}
                      dangerouslySetInnerHTML={{
                        __html: intro.html,
                      }}
                    />
                  </div>
                  <div
                    className="w-full flex justify-between items-center py-12 px-10 mt-10 font-serif text-2xl"
                    style={{
                      backgroundColor: "#d0f1f2",
                    }}
                  >
                    <div className="mr-10"> {t("strings_answer")} </div>
                    <a
                      href={`#${Object.keys(sections)[0]}`}
                      className={`px-12 py-1 text-white uppercase text-xl font-sans font-semibold bg-dfak-dark-blue ${intro.start}`}
                    >
                      {t("strings_start")}
                    </a>
                  </div>
                </>
              ) : (
                // {COMMENT: display workflow steps history }
                <>
                  {(previousSections.length === 0 &&
                    !window.location.hash.split("#").reverse()[0] !== "") ||
                  previousSections.length > 0 ? (
                    <div className="">
                      <div className="bg-dfak-blue my-2 rounded-sm group hover:scale-[105%] transition-all duration-200 ease-in-out hover:shadow-lg">
                        <a
                          href="#"
                          className="bg-white-900 pointer text-xl b-1 p-5 rounded-sm items-center flex flex-row justify-start"
                        >
                          <FaUndo className="bg-green-800 rounded-full text-white p-2 w-9 h-9 mr-4" />
                          <h4 className="text-green-800">{t("start_again")}</h4>
                        </a>
                      </div>
                      {_.map(
                        _.dropRight(previousSections),
                        (preSection, preSectionIndex) => (
                          <div
                            className="bg-dfak-blue my-2 rounded-sm group hover:scale-[105%] transition-all duration-200 ease-in-out hover:shadow-lg p-5"
                            key={preSectionIndex}
                          >
                            <a
                              href={`#${sections[preSection].title}`}
                              key={sections[preSection].id}
                              className="bg-white-900 pointer text-2xl block text-green-800"
                              label={t("return_to_chapter")}
                            >
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: sections[preSection].header,
                                }}
                                className="prose-lg prose-headings:text-2xl prose-headings:font-semibold prose-headings:"
                              />
                            </a>
                            {sections[preSection].options ? (
                              // {COMMENT: display options in workflow steps history }
                              <div className="flex flex-wrap justify-start pt-2">
                                {sections[preSection].options.map(
                                  ({ link, text }, idx) => (
                                    <div
                                      className={`flex items-center ${
                                        previousSections.includes(
                                          link.replace("#", "")
                                        )
                                          ? "text-green-600 font-semibold"
                                          : "text-red-800 line-through"
                                      } p-1 mr-2 text-normal`}
                                      key={idx}
                                    >
                                      {previousSections.includes(
                                        link.replace("#", "")
                                      ) ? (
                                        <FaCheckCircle className="mr-1" />
                                      ) : (
                                        ""
                                      )}
                                      <>{text}</>
                                    </div>
                                  )
                                )}
                              </div>
                            ) : (
                              ""
                            )}
                          </div>
                        )
                      )}
                    </div>
                  ) : (
                    ""
                  )}
                  <div
                    // {COMMENT: display normal workflow step }
                    className={`${currentSection.title} bg-white w-full pb-10`}
                  >
                    <div
                      className="text-3xl font-semibold mt-2 p-12"
                      dangerouslySetInnerHTML={{
                        __html: currentSection.header,
                      }}
                    />
                    <div
                      className="px-12 prose prose-xl font-serif max-w-none"
                      dangerouslySetInnerHTML={{
                        __html: currentSection.html,
                      }}
                    />
                  </div>
                  {currentSection.orgsFilter && (
                    // {COMMENT: Show organisations lists in workflow step }
                    <div className="pt-1">
                      {this.listOrganisations(currentSection.orgsFilter)}
                    </div>
                  )}
                  {currentSection.title.split("-").reverse()[0] === "end" && (
                    // {COMMENT: display Final Tips and Resources block to _end workflow steps }
                    <div className="flex flex-col lg:flex-row justify-start font-serif prose-xl prose-a:underline mt-2">
                      <div
                        className="final-tips px-10 pb-10 w-full lg:w-8/12"
                        style={{ backgroundColor: "#d0f1f2" }}
                      >
                        <h4 className="text-3xl font-semibold mb-2">
                          {t("strings_more_tips")}
                        </h4>
                        <div
                          dangerouslySetInnerHTML={{ __html: finalTips }}
                          className="prose-li:list-disc prose-li:leading-6"
                        />
                      </div>
                      <div className="resources bg-white px-10 w-full lg:w-4/12 font-serif pb-10">
                        <h4 className="text-3xl font-semibold mb-2">
                          {t("strings_resources")}
                        </h4>
                        <div
                          dangerouslySetInnerHTML={{ __html: resources }}
                          className="prose-li:list-disc prose-li:leading-6"
                        />
                      </div>
                    </div>
                  )}
                </>
              )}
            </article>
          </>
        ) : (
          <>Loading...</>
        )}
      </Layout>
    )
  }
}

export default withTranslation()(Topics)

export const query = graphql`
  query ($slug: String!, $language: String!) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    markdownRemark(
      frontmatter: { language: { eq: $language }, permalink: { eq: $slug } }
    ) {
      html
      htmlAst
      frontmatter {
        title
        permalink
        language
      }
      fields {
        slug
      }
    }
    allMarkdownRemark(
      filter: {
        frontmatter: {
          layout: { eq: "organisation" }
          language: { eq: $language }
        }
      }
    ) {
      edges {
        node {
          frontmatter {
            language
            name
            website
            logo
            languages
            services
            beneficiaries
            hours
            response_time
            contact_methods
            hours
            response_time
            contact_methods
            email
            pgp_key
            pgp_key_fingerprint
            mail
            web_form
            signal
            phone
            whatsapp
          }
        }
      }
    }
  }
`
