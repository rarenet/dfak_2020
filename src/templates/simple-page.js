import React, { useEffect, useState } from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import Seo from "../components/seo"

export default function SimplePage({ data }) {
  const [html, setHtml] = useState("")
  const [title, setTitle] = useState("")
  const [sidebar, setSidebar] = useState(null)

  useEffect(() => {
    const page = data.markdownRemark
    setTitle(page.frontmatter.title)
    setSidebar(page.frontmatter.sidebar || null)
    setHtml(page.html || "")
  }, [data])

  return (
    <Layout>
      <Seo title={title} />
      <article className="pt-10 md:p-20 flex md:flex-row flex-col">
        <div
          dangerouslySetInnerHTML={{ __html: html }}
          className={`font-serif prose prose-li:leading-5 prose-p:leading-5 prose-headings:leading-6 max-w-none bg-${
            sidebar ? "dfak-blue w-full md:w-8/12" : "white w-full"
          } p-6 md:p-12`}
        />
        <div
          dangerouslySetInnerHTML={{ __html: sidebar }}
          className="w-full md:w-4/12 font-serif prose prose-li:leading-5 prose-headings:leading-6 bg-white p-6 md:p-12 max-w-none"
        />
      </article>
    </Layout>
  )
}

export const query = graphql`
  query ($slug: String!, $language: String!) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    markdownRemark(
      frontmatter: { language: { eq: $language }, permalink: { eq: $slug } }
    ) {
      html
      frontmatter {
        title
        sidebar
      }
    }
  }
`
