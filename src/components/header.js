import React, { useState, useRef, useEffect } from "react"
import { Link, useI18next } from "gatsby-plugin-react-i18next"
import logo from "../../static/images/logo.svg"

export default function Header({ siteTitle, data }) {
  const { languages, originalPath, language, t } = useI18next()
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const [panelHeight, setPanelHeight] = useState(600)
  const navPanel = useRef()
  useEffect(() => {
    setPanelHeight(navPanel.current.clientHeight)
  }, [panelHeight])
  return (
    <header className="bg-white flex h-[70px] sticky top-0 z-20 w-full">
      <div className="md:w-10/12 m-auto w-full h-full flex items-center justify-between  max-w-[1024px]">
        <Link to="/" className="min-w-[300px]">
          <img src="/images/logo.svg" alt="Logo" />
        </Link>
        <button
          className="block md:hidden mr-5"
          onClick={() => setIsSidebarOpen(!isSidebarOpen)}
          onKeyUp={() => null}
          tabIndex="0"
          role="button"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className="w-6 h-6"
          >
            {isSidebarOpen ? (
              <path
                fillRule="evenodd"
                d="M5.47 5.47a.75.75 0 011.06 0L12 10.94l5.47-5.47a.75.75 0 111.06 1.06L13.06 12l5.47 5.47a.75.75 0 11-1.06 1.06L12 13.06l-5.47 5.47a.75.75 0 01-1.06-1.06L10.94 12 5.47 6.53a.75.75 0 010-1.06z"
                clipRule="evenodd"
              />
            ) : (
              <path
                fillRule="evenodd"
                d="M3 6.75A.75.75 0 013.75 6h16.5a.75.75 0 010 1.5H3.75A.75.75 0 013 6.75zM3 12a.75.75 0 01.75-.75h16.5a.75.75 0 010 1.5H3.75A.75.75 0 013 12zm0 5.25a.75.75 0 01.75-.75h16.5a.75.75 0 010 1.5H3.75a.75.75 0 01-.75-.75z"
                clipRule="evenodd"
              />
            )}
          </svg>
        </button>
        <nav
          className={`md:relative absolute transition-all ease-in-out duration-600 md:flex md:flex-row flex-col md:transform-none md:items-center items-start md:right-auto md:top-auto right-0 top-[65px]  h-auto md:h-full md:w-auto w-full pb-16 md:p-0  ${
            isSidebarOpen
              ? "bg-white translate-x-[0vw] border-b-2 border-gray-500 shadow-md"
              : "translate-x-[100vw]"
          }`}
          ref={navPanel}
        >
          <div className="flex justify-between flex-col md:flex-row">
            {["about", "support", "documentation", "self-care"].map((l, i) => {
              return (
                <Link
                  to={`/${l}`}
                  key={i}
                  language={language}
                  className="md:pt-0 md:pb-0 pt-4 pb-4 md:pr-3 md:pl-3 pl-12 md:pl-0 w-full md:w-auto text-[1.2em] font-semibold  border-t-2 border-gray-800 last-of-type:border-b-2 md:border-0 last-of-type:md:border-0 text-gray-900 hover:bg-gray-200 leading-20 flex justify-center"
                >
                  {t(`navbar_${l}`)}
                </Link>
              )
            })}
            <div className="flex flex-col relative group ml-6 text-[1.2em] font-semibold">
              <div className="hidden md:block pb-3 cursor-pointer px-3 -mb-3">
                [{language}]
              </div>
              <div className="md:w-[300px] w-full p-6 right-0 grid-cols-3 gap-4 z-20 md:absolute relative md:group-hover:grid md:hidden top-[40px] bg-white">
                {languages.map(lng => {
                  return (
                    <Link
                      language={lng}
                      to={originalPath}
                      key={lng}
                      className={`${lng === language ? "md:hidden" : ""} mx-3`}
                    >
                      [{lng}]
                    </Link>
                  )
                })}
              </div>
            </div>
          </div>
        </nav>
      </div>
    </header>
  )
}
