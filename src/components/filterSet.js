import React, { useState } from "react"
import _ from "lodash"
import { useI18next } from "gatsby-plugin-react-i18next"
import { Transition } from "@headlessui/react"

const FilterSet = ({ filterSet, idx, updateFilter, selectedFilters }) => {
  const { t } = useI18next()
  const [isOpen, setIsOpen] = useState(false)

  return (
    <div className="flex flex-col border-t-2 border-gray-300 my-3 pt-2">
      <div className="flex items-center ">
        <button
          className={`flex justify-around items-center`}
          role="button"
          onClick={() => {
            setIsOpen(!isOpen)
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className={`w-3 h-3  bg-gray-100 rounded-sm mr-2 p-[1px] transition-all ${
              isOpen && "-rotate-180"
            }`}
          >
            <path
              fillRule="evenodd"
              d="M12.53 16.28a.75.75 0 01-1.06 0l-7.5-7.5a.75.75 0 011.06-1.06L12 14.69l6.97-6.97a.75.75 0 111.06 1.06l-7.5 7.5z"
              clipRule="evenodd"
            />
          </svg>
          <span className="font-semibold uppercase text-xs text-gray-800 ">
            {t("strings_" + idx)}
          </span>
        </button>
      </div>
      <Transition
        show={isOpen}
        enter="transition duration-[.5s] ease-in"
        enterFrom="opacity-0"
        enterTo="opacity-100"
        leave="transition duration-[.4s]"
        leaveFrom="opacity-100"
        leaveTo="opacity-0"
      >
        <div className="flex flex-wrap gap-2 p-2 ">
          {_.map(filterSet, (filter, i) => (
            <div
              className={`text-[9pt] ring-1 rounded-sm py-1 px-2 hover:bg-dfak-blue ${
                _.includes(selectedFilters, _.kebabCase(filter))
                  ? "bg-gray-900 text-gray-100 ring-purple-500  hover:bg-gray-600"
                  : "text-gray-800 bg-gray-100"
              }`}
              key={i}
              id={`${idx}-${i}`}
              data-filter={_.kebabCase(filter)}
              onClick={e => updateFilter(e.target.dataset["filter"])}
              onKeyUp={() => null}
              role="button"
              tabIndex="0"
            >
              {idx === "languages" ? filter : t(`${idx}_${filter}`)}
            </div>
          ))}
        </div>
      </Transition>
    </div>
  )
}

export default FilterSet
