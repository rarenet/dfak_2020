import React from "react"
import { useI18next, Link } from "gatsby-plugin-react-i18next"

export default function Breadcrumb({ parentLink, parentTitle, changeSection }) {
  const { language } = useI18next()
  return (
    <div className="breadcrumb mt-10 mb-5 text-xl">
      <Link to={`/`} language={language} className="link black">
        {`DFAK / `}
      </Link>
      <button
        className="bn bg-transparent pointer f4"
        onClick={() => {
          changeSection("")
        }}
      >
        {parentTitle}
      </button>
    </div>
  )
}
