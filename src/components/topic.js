import React, { Component } from "react"
import PropTypes from "prop-types"
import visit from "unist-util-visit"
import find from "unist-util-find"
import findAllAfter from "unist-util-find-all-after"
import findAllBefore from "unist-util-find-all-before"
import filter from "unist-util-filter"
import u from "unist-builder"
import toHTML from "hast-util-to-html"
import { withTranslation } from "gatsby-plugin-react-i18next"
import _ from "lodash"

class Topic extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: props.page,
      currentStep: {},
      stepsHistory: [],
      steps: {},
    }
  }

  componentDidMount() {
    let workflowSteps = {}

    const ast = this.state.page.htmlAst,
      astSteps = []

    visit(ast, "element", node => {
      if (node.tagName === "a") {
        if (node.properties.href.startsWith("#")) {
          _.merge(node.properties, {
            onClick: `{(e) => this.changeQestion(e)}`,
          })
        } else if (node.properties.href.startsWith("http")) {
          _.merge(node.properties, { target: "_blank", rel: "noreferrer" })
        } else if (node.properties.href.startsWith("../")) {
          node.properties.href.replaceAll("../", "")
          node.properties.href = `/${node.properties.href}`
        }
      }
      if (node.tagName === "h3") {
        astSteps.push(node)
      }
    })
    const withoutTitle = filter(ast, node => node.tagName !== "h1"),
      h2Pos = find(withoutTitle, { tagName: "h2" }),
      intro = u("root", findAllBefore(withoutTitle, h2Pos).reverse())

    astSteps.map((step, idx) => {
      let to = {},
        stepBlock = {}
      const from = u("root", findAllAfter(ast, step)),
        title = step.children[0].value
      if (astSteps[idx + 1]) {
        const nextNode = find(from, node => node === astSteps[idx + 1])
        to = findAllBefore(from, nextNode).reverse()
        stepBlock = u("root", to)
      } else {
        stepBlock = from
      }
      return _.merge(workflowSteps, { [title]: { html: toHTML(stepBlock) } })
    })

    _.merge(workflowSteps, {
      start: { start: 0, end: 0, html: toHTML(intro), title: "start" },
    })

    const currentStep = window.location.hash
      ? window.location.hash.slice(1)
      : "start"
    this.setState({
      steps: workflowSteps,
      currentStep: workflowSteps[currentStep],
    })
  }

  render() {
    const { currentStep } = this.state
    const { page, t } = this.props
    return (
      <>
        {currentStep.title === "start" ? (
          <>
            <div className="flex flex-row-l flex-column ">
              <div className="title w-40-l w-100 pa2 flex flex-column items-center">
                <img
                  style={{ width: "12em" }}
                  alt={page.frontmatter.title}
                  src={`/images/topics/${
                    page.fields.slug.split("/").reverse()[1]
                  }.svg`}
                />
                <h1 className="tc">{page.frontmatter.title}</h1>
              </div>
              <div
                className={`${currentStep.title} s bg-white-50 w-60-l w-100`}
                dangerouslySetInnerHTML={{ __html: currentStep.html }}
              />
            </div>
            <div className="w-100 flex justify-between pa5 bg-blue-30">
              <div>{t("strings_answer")}</div>
              <button>{t("strings_start")}</button>
            </div>
          </>
        ) : (
          <div
            className={`${currentStep.title} s bg-white-50 pa2 ma3`}
            dangerouslySetInnerHTML={{ __html: currentStep.html }}
          />
        )}
      </>
    )
  }
}

Topic.propTypes = {
  page: PropTypes.any,
}
export default withTranslation()(Topic)
