import { Link, useI18next } from "gatsby-plugin-react-i18next"
import React from "react"
import cc from "../../static/images/cc.png"

function Footer() {
  const { t, language, defaultLanguage } = useI18next()
  const classNames = "mr-5 py-3 text-white font-bold uppercase "
  const links = [
    {
      title: t("navbar_about"),
      href: `/about`,
      target: "_self",
    },
    {
      title: t("navbar_download"),
      href: "/dfak-offline.zip",
      target: "_blank",
    },
    {
      title: "Gitlab",
      href: "https://gitlab.com/rarenet/dfak_2020",
      target: "_blank",
    },
  ]
  return (
    <footer className="w-full mt-10 bg-black">
      <div className="w-11/12 md:w-9/12 p-5 m-auto flex justify-between flex-col md:flex-row  max-w-[1024px]">
        <a
          className="md:py-3 py-10 md:order-none order-1 flex justify-around"
          target="_blank"
          rel="noreferrer"
          href="https://creativecommons.org/licenses/by/4.0/"
          title="Licence: CC by 4.0"
        >
          <img src={cc} alt="Licence: CC by 4.0" className="self-center" />
        </a>
        <nav className="flex justify-end w-50 items-center flex-col md:flex-row ">
          {links.map(({ href, target, title }, index) => (
            <div key={index}>
              {target === "_self" ? (
                <Link to={href} language={language} className={classNames}>
                  {title}
                </Link>
              ) : (
                <a
                  key={index}
                  href={href}
                  target={target}
                  className={classNames}
                >
                  {title}
                </a>
              )}
            </div>
          ))}
        </nav>
      </div>
    </footer>
  )
}

export default Footer
