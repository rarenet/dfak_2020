import React, { useRef, useEffect } from "react"
import { useI18next } from "gatsby-plugin-react-i18next"
import _ from "lodash"
import OutsideClickHandler from "react-outside-click-handler"

import FilterSet from "./filterSet"

function Filters({
  filters,
  selectedFilters,
  updateFilter,
  toggleFilters,
  showFilters,
  buttonClasses,
}) {
  const { t } = useI18next()
  return (
    <OutsideClickHandler onOutsideClick={() => toggleFilters(false)}>
      <div className="flex flex-col bg-white p-2 z-10 w-full shadow-lg rounded-b-md">
        <div className="flex w-full justify-between">
          <h3 className="text-lg font-sans">
            {t("strings_filters_title")}
            {selectedFilters.length > 0 && (
              <>
                <span className="bg-red-600 rounded-sm text-white ml-1 text-[8pt] py-[2px] px-1 rounded-lg font-medium">
                  {selectedFilters.length}
                </span>
                <button
                  className="text-[8pt] ml-1 font-normal"
                  onClick={() => updateFilter(null)}
                  onKeyUp={() => null}
                  role="button"
                  tabIndex="0"
                >
                  {t("reset")}
                </button>
              </>
            )}
          </h3>
          <button
            onClick={() => toggleFilters(false)}
            className={buttonClasses}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="currentColor"
              className="w-5 h-5"
            >
              <path
                fillRule="evenodd"
                d="M5.47 5.47a.75.75 0 011.06 0L12 10.94l5.47-5.47a.75.75 0 111.06 1.06L13.06 12l5.47 5.47a.75.75 0 11-1.06 1.06L12 13.06l-5.47 5.47a.75.75 0 01-1.06-1.06L10.94 12 5.47 6.53a.75.75 0 010-1.06z"
                clipRule="evenodd"
              />
            </svg>
          </button>
        </div>
        {_.map(filters, (filterSet, idx) => (
          <FilterSet
            key={idx}
            idx={idx}
            filterSet={filterSet}
            updateFilter={updateFilter}
            selectedFilters={selectedFilters}
          />
        ))}
      </div>
    </OutsideClickHandler>
  )
}

export default Filters
