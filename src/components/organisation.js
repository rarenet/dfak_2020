/* eslint no-restricted-globals: 0*/
import React, { useState } from "react"
import { useI18next } from "gatsby-plugin-react-i18next"
import _ from "lodash"

function Org({ organisation, viewStyle, selectedFilters }) {
  const [isOrganisationOpen, openOrganisation] = useState(false)
  const { t } = useI18next()
  function toggleOrganisation(e) {
    openOrganisation(!isOrganisationOpen)
  }

  return (
    <div
      id={_.kebabCase(organisation.frontmatter.name)}
      className={`organisation relative ${
        isOrganisationOpen
          ? "sm:col-span-2 md:col-span-3 px-5"
          : `${viewStyle === "grid" ? "px-2" : "px-5"}`
      } bg-white py-3 transition-all ease-in-out duration-300`}
    >
      {viewStyle === "grid" ? (
        <button
          data-anchor={organisation.frontmatter.name}
          className="absolute top-0 right-0 bg-dfak-blue text-xs px-[10px] py-[3px] hover:ring-1"
          onClick={e =>
            toggleOrganisation(_.kebabCase(e.target.dataset["anchor"]))
          }
        >
          {isOrganisationOpen ? t("strings_fold") : t("strings_expand_info")}
        </button>
      ) : (
        ""
      )}
      <div
        className={`flex w-full ${
          viewStyle === "grid"
            ? "flex-col items-center justify-start"
            : "md:flex-row flex-col"
        }`}
      >
        <div
          className={`${
            viewStyle === "grid" ? "w-auto" : "w=3/12"
          } min-w-[200px] max-w-[200px] max-h-[250px] h-auto p-4`}
        >
          <img
            src={`/images/logos/${organisation.frontmatter.logo}`}
            alt={organisation.frontmatter.name}
          />
        </div>
        <div
          className={`${viewStyle === "grid" ? "w-full" : "w=9/12"} px-2 py-5`}
        >
          <h3
            className={`font-sans text-3xl m-0 ${
              viewStyle === "grid" ? "text-xl" : "text-3xl"
            }`}
          >
            {organisation.frontmatter.name}
          </h3>
          <div className={`section ${isOrganisationOpen ? "oneline" : ""}`}>
            <label>{t("strings_website")}:</label>
            <a
              href={organisation.frontmatter.website}
              target="_blank"
              rel="noreferrer nofollow"
              className="break-all"
            >
              <span className="underline">
                {organisation.frontmatter.website}
              </span>
            </a>
          </div>
          {viewStyle === "list" || isOrganisationOpen ? (
            <>
              <div
                className="section html prose max-w-none prose-li:leading-5 prose-li:m-0 prose-ul:m-0 prose-p:my-[2px] prose-p:leading-5"
                dangerouslySetInnerHTML={{ __html: organisation.html }}
              />
              <div className="section">
                <label>{t("strings_services")}: </label>
                <ul>
                  {_.map(
                    _.toArray(organisation.frontmatter.services.split(",")),
                    (service, sid) => {
                      const serv = `services_${service.replace(" ", "")}`
                      return <li key={sid}>{t(serv)}</li>
                    }
                  )}
                </ul>
              </div>
              <div className="section">
                <label>{t("strings_who_they_help")}: </label>
                <ul>
                  {_.map(
                    _.toArray(
                      organisation.frontmatter.beneficiaries.split(",")
                    ),
                    (beneficiary, bid) => {
                      const bene = `beneficiaries_${beneficiary.replace(
                        " ",
                        ""
                      )}`
                      return (
                        <li className="beneficiary list-item" key={bid}>
                          {t(bene)}
                        </li>
                      )
                    }
                  )}
                </ul>
              </div>
              <div className="section">
                <label>{t("strings_available_in_languages")}: </label>
                <ul>
                  {_.map(
                    _.toArray(organisation.frontmatter.languages.split(",")),
                    (lan, lid) => {
                      return (
                        <li className="language list-item" key={lid}>
                          {lan}
                        </li>
                      )
                    }
                  )}
                </ul>
              </div>
              <div className="section">
                <label>{t("strings_how_to_contact")}: </label>
                <ul className="list">
                  {organisation.frontmatter.contact_methods &&
                    organisation.frontmatter.contact_methods
                      .split(",")
                      .map((method, mIdx) => {
                        if (method === " pgp") {
                          return (
                            <li key={mIdx}>
                              {organisation.frontmatter.pgp_key && (
                                <span key={mIdx + 1000}>
                                  <span className="mr-2">
                                    {t("methods_pgp_key")}:{`  `}
                                  </span>
                                  <a
                                    href={organisation.frontmatter.pgp_key}
                                    rel="nofollow noreferrer"
                                    target="_blank"
                                    className="font-semibold"
                                  >
                                    {organisation.frontmatter.pgp_key}
                                  </a>
                                </span>
                              )}
                              <span key={mIdx}>
                                <span className="mr-2">
                                  {t("methods_pgp_key_fingerprint")}:{`  `}
                                </span>
                                <span className="font-semibold">
                                  {organisation.frontmatter.pgp_key_fingerprint}
                                </span>
                              </span>
                            </li>
                          )
                        } else {
                          return (
                            <li key={mIdx}>
                              <span className="mr-2">
                                {t("methods_" + method.replace(" ", ""))}:{`  `}
                              </span>
                              {method.replace(" ", "") === "web_form" ? (
                                <a
                                  href={`https://${organisation.frontmatter.web_form.replace(
                                    "https://",
                                    ""
                                  )}`}
                                  rel="nofollow noreferrer"
                                  target="_blank"
                                  className="font-semibold"
                                >
                                  {organisation.frontmatter.web_form}
                                </a>
                              ) : (
                                <span className="font-semibold">
                                  {
                                    organisation.frontmatter[
                                      method.replace(" ", "")
                                    ]
                                  }
                                </span>
                              )}
                            </li>
                          )
                        }
                      })}
                </ul>
              </div>
              {organisation.frontmatter.hours && (
                <div className="section oneline">
                  <label>{t("strings_working_hours")}: </label>
                  <div>{organisation.frontmatter.hours}</div>
                </div>
              )}
              {organisation.frontmatter.response_time && (
                <div className="section oneline">
                  <label>{t("strings_expected_response_time")}: </label>
                  <div>{organisation.frontmatter.response_time}</div>
                </div>
              )}
            </>
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  )
}

export default Org
