module.exports = {
  siteMetadata: {
    title: "Digital First Aid Kit",
    description: "A kit designed to get you to the right CiviCERT team",
    author: "DFAK <dfak@civicert.org>",
    rtlLanguages: ["ar", "fa"],
  },
  plugins: [
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-postcss",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: `${__dirname}/src/images`,
      },
      options: {
        name: "static_images",
        path: `${__dirname}/static/images`,
      },
    },
    "gatsby-transformer-sharp",
    "gatsby-plugin-sharp",
    {
      resolve: "gatsby-transformer-remark",
      options: {
        commonmark: true,
        footnotes: true,
        pedantic: true,
        gfm: true,
        plugins: [],
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "content",
        path: `${__dirname}/content/`,
        ignore: ["**/.*"],
      },
    },
    {
      resolve: "gatsby-plugin-react-i18next",
      options: {
        localeJsonSourceName: "content",
        languages: [
          "en",
          "es",
          "fr",
          "pt",
          "ru",
          "ar",
          "sq",
          "my",
          "id",
          "th",
          "hy",
          "ky",
          "uk",
          "fa",
        ],
        defaultLanguage: "en",
        redirect: true,
        i18nextOptions: {
          interpolation: {
            escapeValue: false,
          },
          keySeparator: false,
          nsSeparator: false,
        },
        pages: [
          {
            matchPath: "/:language?/:slug",
            getLanguageFromPath: true,
            excludeLanguages: ["en"],
          },
          {
            matchPath: "/:slug",
            getLanguageFromPath: true,
            excludeLanguages: ["en"],
          },
        ],
      },
    },
    {
      resolve: "gatsby-plugin-webfonts",
      options: {
        fonts: {
          google: [
            {
              family: "Roboto",
              variants: ["300", "400", "500", "900"],
              strategy: "selfHosted",
            },
          ],
        },
        formats: ["woff2", "woff"],
        useMinify: true,
        usePreload: true,
        usePreconnect: false,
      },
    },
    {
      resolve: "gatsby-plugin-matomo",
      options: {
        siteId: "2",
        matomoUrl: "https://trends.digitaldefenders.org",
        siteUrl: "https://digitalfirstaid.org",
      },
    },
    "gatsby-plugin-smoothscroll",
  ],
}
