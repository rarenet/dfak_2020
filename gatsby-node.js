const path = require(`path`)
const visit = require("unist-util-visit")
const find = require("unist-util-find")
const findAllAfter = require("unist-util-find-all-after")
const findAllBefore = require("unist-util-find-all-before")
const filter = require("unist-util-filter")
const u = require("unist-builder")
const toHTML = require("hast-util-to-html")
const { v4: uuid } = require("uuid")

const { createFilePath } = require(`gatsby-source-filesystem`)
const _ = require("lodash")

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  const typeDefs = `
    type MarkdownRemark implements Node {
      frontmatter: Frontmatter
    }
    type Frontmatter {
      hours: String
      response_time: String
      contact_methods: String
      email: String
      pgp_key: String
      pgp_key_fingerprint: String
      mail: String
      web_form: String
      signal: String
      phone: String
      whatsapp: String
      telegram: String
      skype: String
    }
  `
  createTypes(typeDefs)
}

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if (
    node.internal.type === `MarkdownRemark` &&
    _.includes(["page", "support", "topic"], node.frontmatter.layout)
  ) {
    const slug = createFilePath({ node, getNode, basePath: `pages` })
      .slice(0, -1)
      .slice(1)
    createNodeField({
      node,
      name: `slug`,
      value: node.frontmatter.language === "en" ? slug.slice(3) : slug,
    })
  }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage, createNode, createContentDigest } = actions
  const pageResult = await graphql(`
    query {
      allMarkdownRemark(filter: { frontmatter: { layout: { eq: "page" } } }) {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              language
              permalink
            }
          }
        }
      }
    }
  `)

  pageResult.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve(`./src/templates/simple-page.js`),
      context: {
        page: node,
        permalink: node.frontmatter.permalink,
        language: node.frontmatter.language,
        slug: node.frontmatter.permalink,
      },
    })
  })

  const supportResult = await graphql(`
    query {
      allMarkdownRemark(
        filter: { frontmatter: { layout: { eq: "support" } } }
      ) {
        edges {
          node {
            frontmatter {
              language
            }
            fields {
              slug
            }
          }
        }
      }
    }
  `)

  supportResult.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: path.resolve(`./src/templates/support.js`),
      context: {
        slug: node.fields.slug,
      },
    })
  })

  const topicResult = await graphql(`
    query {
      allMarkdownRemark(filter: { frontmatter: { layout: { eq: "topic" } } }) {
        edges {
          node {
            html
            htmlAst
            fields {
              slug
            }
            fileAbsolutePath
            frontmatter {
              title
              language
              summary
              permalink
            }
          }
        }
      }
    }
  `)

  topicResult.data.allMarkdownRemark.edges.forEach(({ node }) => {
    // Create the topic page.
    createPage({
      path: node.fields.slug,
      component: path.resolve(`./src/templates/topics.js`),
      context: {
        slug: node.frontmatter.permalink,
      },
    })
  })
}
