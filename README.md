# Welcome to the rewrite of the DFAK website.

This project is built using [gatsby](https://gatsbyjs.org) a static site generator. Gatsby a `react`/`node` based static site generator.

To build and develop, manange content or modify code you need to have the following.

We recommend you use `nvm` to manage node and its versions.

# How to run the DFAK website locally

## 1. Install dependencies

_You will need to follow the instructions in steps 1 and 2 only once - after you've installed the dependencies and cloned the repository, you can skip directly to step 3 every time you need to run the DFAK locally._

**First install git**, below for Linux Debian/Ubuntu, [see here for other OS](https://git-scm.com/downloads) :

```bash
sudo apt install git
```

**Install curl**, below for Linux Debian/Ubuntu, [see here for other OS](https://curl.haxx.se/download.html) :

```bash
sudo apt-get install curl
```

**Install nvm** [a version manager for node.js](https://github.com/nvm-sh/nvm#install--update-script)

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash
```

Close and reopen your terminal to start using nvm or run the following to use it now:

```
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
```

## 2. Clone the repository

**Clone the DFAK repository** : _(has to be changed with the definitive repository)_

```bash
git clone https://gitlab.com/rarenet/dfak_2020.git dfak
```

## 3. Run the website locally

**Go the the DFAK folder** :

```bash
cd dfak
```

_Please note that if this repository was cloned some time ago, you need to synchronize it with the current version by launching the following command:_

```bash
git pull origin main
```

**Install and use nodejs** with nvm

```bash
nvm install
nvm use
```

**Install gatsby**, a [static site generator command line tool](https://www.gatsbyjs.com/docs/gatsby-cli/) and **yarn**, [a package manager for your code](https://yarnpkg.com/getting-started/install) and the corresponding packages

```bash
npm i -g gatsby-cli yarn
```

**Install dependencies**

```bash
yarn install
```

**Start the development server**. Watches files, rebuilds, and hot reloads if something changes

```bash
gatsby develop
```

**You should now be able to visit DFAK website on your computer**, go to http://localhost:8000/
